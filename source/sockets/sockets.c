/* A minimal wrapper for socket communication.

Copyright (C) 2013, Joshua More and Michele Ceriotti

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Contains both the functions that transmit data to the socket and read the data
back out again once finished, and the function which opens the socket initially.
Can be linked to a FORTRAN code that does not support sockets natively.

Functions:
   error: Prints an error message and then exits.
   open_socket_: Opens a socket with the required host server, socket type and
      port number.
   write_buffer_: Writes a string to the socket.
   read_buffer_: Reads data from the socket.
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
//#include <time.h>
//#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <netdb.h>
#include <math.h>
#include <signal.h>
#include <sys/select.h>

void open_socket(int *psockfd, int* inet, int* port, char* host, int *slots)
/* Opens a socket.

Note that fortran passes an extra argument for the string length, but this is
ignored here for C compatibility.

Args:
   psockfd: The id of the socket that will be created.
   inet: An integer that determines whether the socket will be an inet or unix
      domain socket. Gives unix if 0, inet otherwise.
   port: The port number for the socket to be created. Low numbers are often
      reserved for important channels, so use of numbers of 4 or more digits is
      recommended.
   host: The name of the host server.
*/

{
   int sockfd, ai_err;

   signal(SIGPIPE, SIG_IGN);

   if (*inet>0)
   {  // creates an internet socket
      
      // fetches information on the host      
      struct addrinfo hints, *res;  
      char service[256];
   
      memset(&hints, 0, sizeof(hints));
      hints.ai_socktype = SOCK_STREAM;
      hints.ai_family = AF_INET;
      hints.ai_flags = AI_PASSIVE;

      sprintf(service,"%d",*port); // convert the port number to a string
      ai_err = getaddrinfo(host, service, &hints, &res); 
      if (ai_err!=0) { perror("  @SOCKET: Error fetching host data. Wrong host name?\n"); exit(-1); }

      // creates socket
      sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
      if (sockfd < 0) { perror("  @SOCKET: Error opening socket"); exit(-1); }
    
      // makes connection
      if (bind(sockfd, res->ai_addr, res->ai_addrlen) < 0) 
      { perror("  @SOCKET: Error opening INET socket: wrong port or server unreachable\n"); exit(-1); }
      freeaddrinfo(res);
   }
   else
   {  
      struct sockaddr_un serv_addr;

      // fills up details of the socket addres
      memset(&serv_addr, 0, sizeof(serv_addr));
      serv_addr.sun_family = AF_UNIX;
      strcpy(serv_addr.sun_path, "/tmp/ipi_");
      strcpy(serv_addr.sun_path+9, host);
      // creates a unix socket
  
      // creates the socket
      sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
      // connects
     unlink(serv_addr.sun_path);
      if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
      { perror("  @SOCKET: Error opening UNIX socket: path unavailable, or already existing\n"); exit(-1); }
      
   }
   
   listen(sockfd,*slots);
   *psockfd=sockfd;
}

void check_for_new_client(int *psockfd, int* client_id)
{
   fd_set set;
   FD_ZERO(&set);
   FD_SET(*psockfd,&set);
   
   if(*client_id==0){
      struct timeval tv;
      tv.tv_sec=0.;
      tv.tv_usec=0.;
      select(*psockfd+1, &set, NULL, NULL, &tv);
   } else {
      select(*psockfd+1, &set, NULL, NULL, NULL);
   }
   if(FD_ISSET(*psockfd,&set)){
      *client_id=accept(*psockfd,NULL,NULL);
     // printf("  @SOCKET: new client connected.\n");
   } else {
      *client_id=-1;
   }
  
}

void poll_clients(int * n, int ids[*n], int busy[*n],int * latency)
{
   fd_set set;
   int max_id=ids[0],i;
   int info;
   FD_ZERO(&set);
   for(i=0;i<*n;i++){
      FD_SET(ids[i],&set);
      if(ids[i]>max_id){ max_id=ids[i]; }
   }
   //fprintf(stderr,"%d\n",max_id);
   // for(i=0;i<*n;i++){
   //   if(FD_ISSET(ids[i],&set)){
   //      fprintf(stderr,"%d in set\n",ids[i]);
   //   }
   // }
   struct timeval tv;
   if(*latency<0) {
       info=select(max_id+1, &set, NULL, NULL, NULL);
   } else{
      tv.tv_sec=0.;
      tv.tv_usec=1000.*(*latency);
      info=select(max_id+1, &set, NULL, NULL, &tv);
   }  
   if(info<0) { perror("  @SOCKET: select error.");}

   for(i=0;i<*n;i++){
     //    fprintf(stderr,"%d %d %d\n",FD_ISSET(ids[i],&set),FD_ISSET(ids[i],&wset),FD_ISSET(ids[i],&eset));

      if(FD_ISSET(ids[i],&set)){        
         busy[i]=0;
      } else {
         busy[i]=1;
       //  fprintf(stderr,"%d not ok\n",ids[i]);
      }
   }
  
}

void writebuffer(int *psockfd, const char *data, int* plen,int * info)
/* Writes to a socket.

Args:
   psockfd: The id of the socket that will be written to.
   data: The data to be written to the socket.
   plen: The length of the data in bytes.
*/

{
   int sockfd=*psockfd;
   int len=*plen;
   int n;

   n = write(sockfd,data,len);
  
   if (n < 0) { perror("  @SOCKET: Error writing to socket: server has quit or connection broke\n"); }
   if(n<0){
      *info=-1;
   } else {
      *info=0;
   }
}


void readbuffer(int *psockfd, char *data, int* plen,int *info)
/* Reads from a socket.

Args:
   psockfd: The id of the socket that will be read from.
   data: The storage array for data read from the socket.
   plen: The length of the data in bytes.
*/

{
   int n, nr;
   int sockfd=*psockfd;
   int len=*plen;

   n = nr = read(sockfd,data,len);

   while (nr>0 && n<len )
   {  nr=read(sockfd,&data[n],len-n); n+=nr; }

   if (n == 0) { perror("  @SOCKET: Error reading from socket: server has quit or connection broke.\n"); }
   if(n==0){
      *info=-1;
   } else {
      *info=0;
   }
   
}

void close_socket(int *psockfd)
{
	int sockfd=*psockfd;
	int info;
	info=shutdown(sockfd,2);
	if(info==0){
		//printf("  @SOCKET: closed successfully.\n");
	} else {
		perror("  @SOCKET: Error closing socket.\n"); exit(-1);
	}
}

