!! MODULE THAT ALLOWS TO COMMUNICATE WITH EXTERNAL PROGRAMS
!! TO COMPUTE THE POTENTIAL AND FORCES.
!! THE SOCKET INTERFACE IS THE SAME AS (AND TAKEN FROM) i-PI (M. Ceriotti)
!! AND AIMS TO BE FULLY COMPATIBLE WITH IT

MODULE socket_potential
	USE kinds
	USE nested_dictionaries	
	USE string_operations
	USE matrix_operations
	USE f90sockets
	USE basic_types
	IMPLICIT NONE

	INTEGER, PARAMETER :: MAX_UNEXP=2
	
	!STATUS CODES
	TYPE STATUS_TYPE
		INTEGER :: Disconnected = 0
		INTEGER :: Up = 1
		INTEGER :: Ready = 2
		INTEGER :: NeedsInit = 4
		INTEGER :: HasData = 8
		INTEGER :: Busy = 16
		INTEGER :: Timeout = 32
	END TYPE

	TYPE WORKER_CHAIN
		INTEGER :: id ! -2=head  ;  -1=disconnected 
		INTEGER :: current_status
		INTEGER :: current_job=-1
		INTEGER :: n_workers
		LOGICAL :: status_request=.FALSE.
		TYPE(WORKER_CHAIN), pointer :: next=>NULL()
		TYPE(WORKER_CHAIN), pointer :: prev=>NULL()
	CONTAINS
		PROCEDURE :: update_status=>worker_update_status
		PROCEDURE :: feed=>worker_feed
		PROCEDURE :: send_init=>worker_send_init
		PROCEDURE :: recolt => worker_recolt
		PROCEDURE :: put_next => worker_put_next
	END TYPE WORKER_CHAIN
	
	TYPE(STATUS_TYPE), SAVE :: state
	LOGICAL, SAVE :: wait_driver
	
	!STANDARD LENGTH OF A MESSAGE
	INTEGER, PARAMETER :: MSGLEN=12
	
	!SOCKET INFO
	TYPE SOCKET_TYPE
		CHARACTER(:), ALLOCATABLE :: address
		INTEGER :: port
		INTEGER :: slots
		CHARACTER(:), ALLOCATABLE :: mode
		REAL :: timeout
		INTEGER :: latency
		INTEGER :: socket_ID
		INTEGER :: server_ID

		TYPE(WORKER_CHAIN) :: worker_pool
	CONTAINS
		PROCEDURE :: add_new_worker => add_new_worker
		PROCEDURE :: wait_for_a_worker => wait_for_a_worker
		PROCEDURE :: shutdown_workers => shutdown_workers
		PROCEDURE :: clean_up_workers => clean_up_workers
		PROCEDURE :: reinitialize_workers_jobs => reinitialize_workers_jobs
	END TYPE
	TYPE(SOCKET_TYPE), ALLOCATABLE, SAVE :: sockets(:)
	
	!SYSTEM INFO
	INTEGER, SAVE :: n_atoms
	INTEGER, SAVE :: n_sockets
	
	PRIVATE
	PUBLIC :: initialize_socket, socket_shutdown, parallel_socket_pot_info,hessian_socket
	
	
CONTAINS

	SUBROUTINE initialize_socket(param_library,n_dim,nAt)
		!$ USE OMP_LIB
		IMPLICIT NONE
		TYPE(DICT_STRUCT), INTENT(IN) :: param_library
		INTEGER, INTENT(IN) :: nAt,n_dim
		TYPE(DICT_STRUCT), pointer :: pot_lib
		CHARACTER(:), ALLOCATABLE :: address
		INTEGER :: port
		INTEGER :: slots,i
		CHARACTER(:), ALLOCATABLE :: mode
		REAL :: timeout
		INTEGER :: latency
		
		if(n_dim /= 3) stop "Socket potentials require that n_dim=3. Execution stopped."
		!ASSIGN get_pot_info
    get_pot_info => get_socket_pot_info
		Pot => Pot_socket
		dPot => dPot_socket
		compute_hessian => hessian_socket

		n_atoms=nAt
		pot_lib => param_library%get_child("POTENTIAL")
		
		!GET SOCKET INFO
		address=pot_lib%get("address",default=("localhost"))
		!! @input_file POTENTIAL/address (socket_potential, default="localhost")
		port=pot_lib%get("port",default=(31415))
		!! @input_file POTENTIAL/port (socket_potential, default=31415)
		slots=pot_lib%get("slots",default=(4))
		!! @input_file POTENTIAL/slots (socket_potential, default=4)
		mode=pot_lib%get("mode",default=("unix"))
		!! @input_file POTENTIAL/mode (socket_potential, default="unix")
		timeout=pot_lib%get("timeout",default=(1.0))
		!! @input_file POTENTIAL/timeout (socket_potential, default=1.)
		latency=pot_lib%get("latency",default=(-1.))
		!! @input_file POTENTIAL/latency (socket_potential, default=0. (ms))

		n_sockets=1
		!$ n_sockets=param_library%get("PARAMETERS/omp_num_threads")
		write(*,*) "Initializing ",n_sockets,"sockets."
		
		allocate(sockets(n_sockets))

		DO i=1,n_sockets
			sockets(i)%worker_pool%n_workers=0
			sockets(i)%worker_pool%id=-2

			sockets(i)%port = port
			sockets(i)%slots = slots
			sockets(i)%mode = mode
			sockets(i)%timeout = timeout
			sockets(i)%latency = latency
			! if(n_sockets==1) then
			! 	sockets(i)%address = address
			! else
				sockets(i)%address = address//"."//int_to_str(i)
			! endif
		
			!OPEN SOCKET
			sockets(i)%server_ID=0
			SELECT CASE(mode)
			CASE("unix")
				call open_socket(sockets(i)%server_ID,0,sockets(i)%port,sockets(i)%address,sockets(i)%slots)
			CASE("inet")
				call open_socket(sockets(i)%server_ID,1,sockets(i)%port,sockets(i)%address,sockets(i)%slots)
			CASE DEFAULT
				STOP "Error: socket mode not implemented (should be unix/inet)!"
			END SELECT

			call sockets(i)%add_new_worker(1)
			
			!WAIT FOR THE DRIVER TO BE READY
			WRITE(*,*) "  @SOCKET:	sending handshaking request."
			CALL sockets(i)%worker_pool%next%update_status(-1)
			IF(sockets(i)%worker_pool%next%current_status==state%HasData) THEN
				WRITE(0,*) "  @SOCKET:	Unexpected status ",sockets(i)%worker_pool%next%current_status
				STOP "Execution stopped."
			ENDIF
			WRITE(*,*) "  @SOCKET:	 Handshaking was successful."
		ENDDO
		
							
	END SUBROUTINE initialize_socket	


	SUBROUTINE get_socket_pot_info(X,Pot,Forces,hessian,vir)
		IMPLICIT NONE
		real(wp), intent(in)  :: X(:,:)
		real(wp), intent(out) :: Forces(size(X,1),size(X,2))
		real(wp), intent(out) :: Pot
		real(wp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
		real(wp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
		real(wp) :: vir_tmp(size(X,2),size(X,2))
		TYPE(WORKER_CHAIN), pointer :: worker
		INTEGER :: s,info,n_unexp,is
		LOGICAL :: normal,cleanup

		if(present(hessian)) hessian=hessian_socket(X)
	
		is=1

		n_unexp=0	
		call sockets(is)%reinitialize_workers_jobs()
		!call update_workers_status()

		worker=>sockets(is)%worker_pool%next
		DO		
			cleanup=.FALSE.
			call worker%update_status(-1)
			normal=.TRUE.	
			s=worker%current_status
			if(s==state%Ready) then
				call worker%feed(1,X,info)
			elseif(s==state%NeedsInit) then
				call worker%send_init()
			elseif(s==state%HasData) then
				if(worker%current_job==-1) then
					WRITE(0,*) "  @SOCKET: Unexpected 'HasData' status. Flushing buffer."
					normal=.FALSE.
				endif
				call worker%recolt(Pot,Forces,vir_tmp,info)
				if(info>=0 .AND. normal) exit
			elseif(s==state%busy) then
				!WRITE(0,*) "  @SOCKET: worker is busy."
			elseif(s==state%Disconnected) then
				cleanup=.TRUE.
			else
				WRITE(0,*) "  @SOCKET: Unexpected status ",s
				n_unexp=n_unexp+1
				if(n_unexp>MAX_UNEXP) STOP "  @SOCKET: Reached max number of unexpected status in a row."
			endif

			if(cleanup) &
				call sockets(is)%clean_up_workers()
			!call update_workers_status()			
		ENDDO

		if(present(vir)) vir=vir_tmp
		
	END SUBROUTINE get_socket_pot_info 

	real(wp) function Pot_socket(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: pot, F(size(X,1),size(X,2))
		
		call get_socket_pot_info(X,pot,F)
		Pot_socket=pot
		
	end function Pot_socket

	function dPot_socket(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: pot

		F=0
		call get_socket_pot_info(X,pot,F)
		F=-F
		
	end function dPot_socket
	

	SUBROUTINE parallel_socket_pot_info(X,Forces,Pot,vir,i_socket)
		IMPLICIT NONE
		real(wp), intent(in)  			:: X(:,:,:)
		real(wp), intent(out) :: Forces(size(X,1),size(X,2),size(X,3))
		real(wp), intent(out), OPTIONAL :: Pot(size(X,1))
		real(wp), intent(out), OPTIONAL :: vir(size(X,1),size(X,3),size(X,3))
		INTEGER, intent(in), OPTIONAL :: i_socket
		real(wp) :: vir_tmp(size(X,1),size(X,3),size(X,3)),Pot_tmp(size(X,1))
		INTEGER :: nu,c_in,c_out,s,i,info,c,is
		TYPE(WORKER_CHAIN), pointer :: worker
		INTEGER :: job_pool(size(X,1)), n_unexp, n_workers_disconnected, n_workers_busy
		REAL(wp) :: flush1,flush2(size(X,2),size(X,3)),flush3(size(X,3),size(X,3))
		LOGICAL :: normal, resend_to_pool, first

		if(present(i_socket)) then
			if(i_socket > n_sockets) STOP "Error: asked for a socket not present!"
			is=i_socket
		else
			is=1
		endif

		call sockets(is)%reinitialize_workers_jobs()
		!call update_workers_status(.TRUE.)
		!call add_new_worker(0)		
		nu=size(X,1) ; c_in=nu ; c_out=0
		!do i=1,nu ; job_pool(i)=i ; enddo
		!WRITE(0,*) worker_pool%n_workers, "active worker(s)."
		job_pool=-1

		first=.TRUE.

		n_unexp=0
		DO while(ANY(job_pool /= 1))	

			!write(0,*) COUNT(job_pool==-1)
			normal=.TRUE.
			resend_to_pool=.FALSE.
			n_workers_disconnected=0
			n_workers_busy=0
			
			call sockets(is)%add_new_worker(0)
			!call update_workers_status()			
			!WRITE(0,*) worker_pool%n_workers, "active worker(s)."
			
			worker=>sockets(is)%worker_pool%next
			DO while(associated(worker))

			

				call worker%update_status(10)	
				!write(0,*) job_pool
				s=worker%current_status
				
				if(s==state%Ready) then
					c=get_first(-1,job_pool)
					!if(c<0) c=get_first(0,job_pool)
					if(c>0) then
						!write(0,*) " @SOCKET: s",c,"->",worker%id
						call worker%feed(c,X(c,:,:),info)
						if(info<0) resend_to_pool=.TRUE.
						job_pool(c)=0
					endif
				elseif(s==state%NeedsInit) then
					call worker%send_init()	
					! c=get_first(-1,job_pool)
					! if(c<0) c=get_first(0,job_pool)
					! if(c>0) then
					! 	call worker%send_init(c)	
					! 	call worker%update_status(-1)
					! 	call worker%feed(c,X(c,:,:),info)
					! 	if(info<0) resend_to_pool=.TRUE.
					! 	job_pool(c)=0
					! endif				
				elseif(s==state%HasData) then
					if(worker%current_job==-1) then
						WRITE(0,*) " @SOCKET: Unexpected 'HasData' status. Flushing buffer."
						write(0,*) " @SOCKET: job -1 for",worker%id
						call worker%recolt(flush1,flush2,flush3,info)
					else						
						i=worker%current_job
						!write(0,*) " @SOCKET: r",i,"<-",worker%id
						call worker%recolt(Pot_tmp(i),Forces(i,:,:),vir_tmp(i,:,:),info)
						if(info>=0) then
							job_pool(i)=1
						else
							resend_to_pool=.TRUE.
						endif
					endif
				elseif(s==state%busy) then
					!WRITE(0,*) "  @SOCKET: worker", worker%id," is busy."
					n_workers_busy=n_workers_busy+1
					!STOP
				elseif(s==state%Disconnected) then	
					n_workers_disconnected=n_workers_disconnected+1
					if(worker%current_job/=-1) resend_to_pool=.TRUE.
				else
					WRITE(0,*) "  @SOCKET: Unexpected status ",s
					n_unexp=n_unexp+1
					normal=.FALSE.
					if(worker%current_job/=-1) resend_to_pool=.TRUE.					
				endif

				if(n_unexp>MAX_UNEXP) STOP "  @SOCKET: Reached max number of unexpected status in a row."
				if(resend_to_pool) then
					WRITE(0,*) "  @SOCKET: Error while getting forces. Sending again in the job_pool."
					job_pool(worker%current_job)=-1
					worker%current_job=-1
				endif

				worker=>worker%next
			ENDDO

			if(n_workers_busy==sockets(is)%worker_pool%n_workers) then
				write(0,*) "  @SOCKET: all workers busy. Waiting for a worker..."
				call sockets(is)%wait_for_a_worker()		
			elseif(n_workers_disconnected>0) then
				call sockets(is)%clean_up_workers()
				write(*,*) "  @SOCKET: Some worker(s) disconnected. Now have ",sockets(is)%worker_pool%n_workers," active worker(s)."
			endif
			!call update_workers_status()
			!first=.FALSE.

		ENDDO	
		IF(present(vir)) vir=vir_tmp
		IF(present(pot)) pot=pot_tmp

	END SUBROUTINE parallel_socket_pot_info 

	FUNCTION get_first(n,tab)
		IMPLICIT NONE
		INTEGER, INTENT(in) :: n, tab(:)
		INTEGER :: i
		INTEGER :: get_first
		DO i=1,size(tab)
			if(tab(i)==n) then
				get_first=i
				return
			endif
		ENDDO
		get_first=-1
		
	END FUNCTION

!--------------------------------------------------------
!--------------------------------------------------------
! WORKERS SUBROUTINES

	SUBROUTINE clean_up_workers(socket)
		IMPLICIT NONE
		CLASS(SOCKET_TYPE), INTENT(inout) :: socket
		TYPE(WORKER_CHAIN), pointer :: worker, tmp

		worker=>socket%worker_pool%next
		DO while(associated(worker))
			if(worker%current_status==state%Disconnected) then
				worker%prev%next=>worker%next
				if(associated(worker%next)) &
					worker%next%prev=>worker%prev
				tmp=>worker%next
				call close_socket(worker%id)
				deallocate(worker)
				worker=>tmp
				socket%worker_pool%n_workers=socket%worker_pool%n_workers-1
			else
				worker=>worker%next
			endif
		ENDDO		

	END SUBROUTINE clean_up_workers

	SUBROUTINE reinitialize_workers_jobs(socket)
		IMPLICIT NONE
		CLASS(SOCKET_TYPE), INTENT(inout) :: socket
		TYPE(WORKER_CHAIN), pointer :: worker, tmp

		worker=>socket%worker_pool%next
		DO while(associated(worker))
			worker%current_job=-1
			worker=>worker%next
		ENDDO		

	END SUBROUTINE reinitialize_workers_jobs

	SUBROUTINE remove_worker(socket,id)
		IMPLICIT NONE
		CLASS(SOCKET_TYPE), INTENT(inout) :: socket
		INTEGER, INTENT(in) :: id
		TYPE(WORKER_CHAIN), pointer :: worker, tmp

		worker=>socket%worker_pool%next
		DO while(associated(worker))
			if(worker%id==id) then
				worker%prev%next=>worker%next
				if(associated(worker%next)) &
					worker%next%prev=>worker%prev
				deallocate(worker)
				socket%worker_pool%n_workers=socket%worker_pool%n_workers-1
				return
			endif
		ENDDO

	END SUBROUTINE remove_worker


	SUBROUTINE add_new_worker(socket,wait)
		IMPLICIT NONE
		CLASS(SOCKET_TYPE), intent(inout) :: socket
		INTEGER, intent(in) :: wait
		CLASS(WORKER_CHAIN), POINTER :: new_worker
		INTEGER :: id

		if(socket%worker_pool%n_workers >= socket%slots) return
		
		id=wait
		if(socket%worker_pool%n_workers==0) THEN
			WRITE(*,*) "  @SOCKET: No driver connected. Waiting for a driver on '"//socket%address//"'"
			id=1 !BLOCKING IF NO WORKER
		ENDIF
		call check_for_new_client(socket%server_ID,id)
		if(id<=0) return
		

		new_worker=>create_worker(id,socket%worker_pool)
		new_worker%status_request=.FALSE.
		new_worker%current_job=-1
		call new_worker%update_status(-1)
		call socket%worker_pool%put_next(new_worker)	

		socket%worker_pool%n_workers=socket%worker_pool%n_workers+1
		write(*,*) "  @SOCKET: new client (",new_worker%id,") connected. Now have " &
									,socket%worker_pool%n_workers," active worker(s)."

	END SUBROUTINE add_new_worker

	SUBROUTINE worker_put_next(self,new_worker)
		IMPLICIT NONE
		CLASS(WORKER_CHAIN), TARGET :: self
		CLASS(WORKER_CHAIN), POINTER :: new_worker

		new_worker%next=>self%next
		new_worker%prev=>self
		if(associated(new_worker%next)) &
			new_worker%next%prev=>new_worker
		self%next=>new_worker
	END SUBROUTINE

	SUBROUTINE worker_update_status(self,wait)
		IMPLICIT NONE
		CLASS(WORKER_CHAIN) :: self
		INTEGER, INTENT(in) :: wait
		INTEGER :: info, busy(1)
		CHARACTER(MSGLEN) :: reply
		
		if(.NOT. self%status_request) then ! SEND A STATUS REQUEST IF NECESSARY
			call writebuffer(self%id,msg("status"),MSGLEN,info)	
			self%status_request=.TRUE.
		endif

		if(self%current_status==state%busy) then
			call poll_clients((/self%id/),busy,wait)
			if(busy(1)==1) then
				self%current_status=state%Busy
				return
			endif
		endif

		call readbuffer(self%id,reply,MSGLEN,info)	
		self%status_request=.FALSE.

		!write(0,*) "  @SOCKET: worker ",self%id," => "//reply
		if(trim(reply)=="" .OR. info<0) then
			!write(0,*) "  @SOCKET: no response from client!"
			self%current_status=state%Disconnected
		elseif(reply == msg("ready")) then
			self%current_status=state%Ready
		elseif(reply == msg("needinit")) then
			self%current_status=state%NeedsInit
		elseif(reply == msg("havedata")) then
			self%current_status=state%HasData
		else
			WRITE(0,*) "  @SOCKET: Unrecognized reply: "//reply
			self%current_status=state%Disconnected
		endif
		
	END SUBROUTINE worker_update_status

	SUBROUTINE wait_for_a_worker(socket)
		IMPLICIT NONE
		CLASS(SOCKET_TYPE), intent(inout) :: socket
		TYPE(WORKER_CHAIN), POINTER :: worker
		INTEGER :: ids(socket%worker_pool%n_workers)
		INTEGER :: busy(socket%worker_pool%n_workers)
		INTEGER :: n_workers_busy
		INTEGER :: i,s,c

		IF(socket%worker_pool%n_workers==0) THEN
			call socket%add_new_worker(1)
			return
		ENDIF		

		worker=>socket%worker_pool%next
		DO i=1,socket%worker_pool%n_workers
			ids(i)=worker%id
			worker=>worker%next
		ENDDO
		CALL poll_clients(ids,busy,-1)

	END SUBROUTINE wait_for_a_worker

	SUBROUTINE worker_feed(self,job,X,info)
		IMPLICIT NONE
		CLASS(WORKER_CHAIN) :: self
		INTEGER, INTENT(in) :: job
		REAL(wp), INTENT(in) :: X(:,:)
		INTEGER,INTENT(out) :: info
		
		self%current_status=state%Up
		self%current_job=job
		call send_pos(self%id,X,simulation_box%hcell,simulation_box%hcell_inv,info)
		if(info<0) write(0,*) "Error while sending pos"	
		!call writebuffer(self%id,msg("status"),MSGLEN,info)	
		!call worker%ask_status()

	END SUBROUTINE worker_feed

	SUBROUTINE worker_recolt(self,pot,Force,vir,info)
		IMPLICIT NONE
		CLASS(WORKER_CHAIN) :: self
		REAL(wp), INTENT(inout) :: Force(:,:),pot,vir(:,:)
		REAL(wp) :: Pot_p
		REAL(wp), DIMENSION(:), ALLOCATABLE :: Force_p, vir_p
		INTEGER :: nAt, extra_str_len,i
		CHARACTER(:), ALLOCATABLE :: extra_str
		CHARACTER(MSGLEN) :: reply
		INTEGER, INTENT(out) :: info

		
		IF(self%current_status /= state%HasData) THEN
			WRITE(0,*) "  @SOCKET: Error: status in get_force was",self%current_status
			STOP "Execution stopped."
		ENDIF
		self%current_status=state%Up
		self%current_job=-1
		
		call writebuffer(self%id,msg("getforce"),MSGLEN,info)
		if(info<0) return

		DO
			call readbuffer(self%id,reply,MSGLEN,info)
			if(info<0) return

			if( reply == msg("forceready") ) then
				exit
			else
				WRITE(0,*) "  @SOCKET: Unexpected getforce reply: "//reply
			endif
			
			if(trim(reply)=="") &
				WRITE(0,*) "  @SOCKET: driver probably disconnected!"			
		ENDDO
		
		IF(.not. allocated(Force_p)) &
			ALLOCATE(Force_p(3*n_atoms))
		IF(.not. allocated(vir_p)) &
			ALLOCATE(vir_p(9))
	
		call readbuffer(self%id,Pot_p,info)
		if(info<0) return

		call readbuffer(self%id,nAt,info)
		if(info<0) return

		if(nAt /= n_atoms) &
			STOP "Error: n_atoms doesn't match between server and driver!"
		call readbuffer(self%id,Force_p,3*nAt,info)
		if(info<0) return

		call readbuffer(self%id,vir_p,9,info)
		if(info<0) return
		
		
		!RECEIVE EXTRA STRING (NOT USED YET)
		call readbuffer(self%id,extra_str_len,info)
		if(extra_str_len > 0) then
			if(allocated(extra_str)) deallocate(extra_str)
			allocate(CHARACTER(len=extra_str_len) :: extra_str)
			call readbuffer(self%id,extra_str,extra_str_len,info)
		endif
		
		Pot=Pot_p
		!DRIVER GIVES ROW MAJOR
		DO i=1,n_atoms
			Force(i,:)=Force_p(3*(i-1)+1:3*i)
		ENDDO
		vir=transpose(RESHAPE(vir_p,(/3,3/)))

		info=0

	END SUBROUTINE worker_recolt

	SUBROUTINE worker_send_init(self,n)
		IMPLICIT NONE
		CLASS(WORKER_CHAIN) :: self
		INTEGER, INTENT(in), OPTIONAL :: n
		INTEGER :: i
		if(present(n)) then
			i=n
		else 
			i=1
		endif
		call send_init(self%id,i,"hello")
	END SUBROUTINE worker_send_init

	FUNCTION create_worker(id,parent) RESULT(worker)
		IMPLICIT NONE
		INTEGER, INTENT(in) :: id
		TYPE(WORKER_CHAIN), INTENT(in), TARGET :: parent
		TYPE(WORKER_CHAIN), POINTER :: worker

		ALLOCATE(worker)
		worker%id=id
		worker%current_job=-1
		worker%current_status=0

		worker%next=>null()
		worker%prev=>parent
		
	END FUNCTION


!--------------------------------------------------------
!--------------------------------------------------------

	SUBROUTINE send_init(driver,rid,pars)
		!> SEND INITITIALIZATION MESSAGE TO DRIVER
		!> (COMPATIBILITY WITH i-PI)
		!> rid: The index of the request, i.e. the replica that
        !>       the force calculation is for.
        !> pars: The parameter string to be sent to the driver.
		IMPLICIT NONE
		INTEGER, INTENT(IN) :: driver,rid
		CHARACTER(*), INTENT(in) :: pars
		INTEGER :: info
		
		!WRITE(*,*) "  @SOCKET: Sending initialization message."
		
		call writebuffer(driver,msg("init"),MSGLEN,info)
		call writebuffer(driver,rid,info)
		call writebuffer(driver,len(pars),info)
		call writebuffer(driver,pars,len(pars),info)		
	
	END SUBROUTINE send_init
	
	SUBROUTINE send_pos(driver,pos,cell,i_cell,info)
		!> SEND POSITION AND CELL DATA
		!> pos: An array containing the atom positions.
        !> cell: The parameter string to be sent to the driver.
		IMPLICIT NONE
		INTEGER, INTENT(IN) :: driver
		REAL(wp), INTENT(IN) :: pos(:,:), cell(:,:), i_cell(:,:)
		REAL(wp), DIMENSION(:), ALLOCATABLE :: pos_p, cell_p, i_cell_p
		INTEGER :: i
		INTEGER, INTENT(out) :: info
		IF(.not. allocated(pos_p)) &
			ALLOCATE(	pos_p(3*n_atoms), &
						cell_p(9), i_cell_p(9)	)
						
		do i=1,n_atoms !ROW MAJOR FOR DRIVER
			pos_p(3*(i-1)+1:3*i)=pos(i,:)
		enddo		
		cell_p=RESHAPE(transpose(cell), (/9/))
		i_cell_p=RESHAPE(transpose(i_cell), (/9/))
	
		call writebuffer(driver,msg("posdata"),MSGLEN,info)
		call writebuffer(driver,cell_p,9,info)
		call writebuffer(driver,i_cell_p,9,info)
		call writebuffer(driver,n_atoms,info)
		call writebuffer(driver,pos_p,3*n_atoms,info)
		!write(0,*) "  @SOCKET: positions sent to", driver
	
	END SUBROUTINE send_pos
	
	! SUBROUTINE get_forces(driver,Pot,Force,vir)
	! 	!> Gets the potential energy, force (and virial) from the driver.
	! 	IMPLICIT NONE
	! 	INTEGER, INTENT(in) :: driver
	! 	REAL(wp), INTENT(INOUT) :: Pot, Force(n_atoms,3)
	! 	REAL(wp), INTENT(INOUT), OPTIONAL :: vir(3,3)
	! 	REAL(8) :: Pot_p
	! 	REAL(8), DIMENSION(:), ALLOCATABLE, SAVE :: Force_p, vir_p
	! 	INTEGER :: nAt, extra_str_len,i
	! 	CHARACTER(:), ALLOCATABLE :: extra_str
	! 	CHARACTER(MSGLEN) :: reply
	! 	INTEGER :: info
		
	! 	IF(current_status /= state%HasData) THEN
	! 		WRITE(0,*) "  @SOCKET: Error: status in get_force was",current_status
	! 		STOP "Execution stopped."
	! 	ENDIF
		
	! 	call writebuffer(driver,msg("getforce"),MSGLEN,info)
	! 	DO
	! 		call readbuffer(driver,reply,MSGLEN,info)
	! 		if( reply == msg("forceready") ) then
	! 			exit
	! 		else
	! 			WRITE(0,*) "  @SOCKET: Unexpected getforce reply: "//reply
	! 		endif
			
	! 		if(trim(reply)=="") &
	! 			WRITE(0,*) "  @SOCKET: driver probably disconnected!"			
	! 	ENDDO
		
	! 	IF(.not. allocated(Force_p)) &
	! 		ALLOCATE(Force_p(3*n_atoms), vir_p(9))
	
	! 	call readbuffer(driver,Pot_p,info)
	! 	call readbuffer(driver,nAt,info)
	! 	if(nAt /= n_atoms) &
	! 		STOP "Error: n_atoms doesn't match between server and driver!"
	! 	call readbuffer(driver,Force_p,3*nAt,info)
	! 	call readbuffer(driver,vir_p,9,info)
		
	! 	!RECEIVE EXTRA STRING (NOT USED YET)
	! 	call readbuffer(driver,extra_str_len,info)
	! 	if(extra_str_len > 0) then
	! 		if(allocated(extra_str)) deallocate(extra_str)
	! 		allocate(CHARACTER(len=extra_str_len) :: extra_str)
	! 		call readbuffer(driver,extra_str,extra_str_len,info)
	! 	endif
		
	! 	Pot=Pot_p
	! 	!DRIVER GIVES ROW MAJOR
	! 	DO i=1,n_atoms
	! 		Force(i,:)=Force_p(3*(i-1)+1:3*i)
	! 	ENDDO
	! 	IF(present(vir)) vir=transpose(RESHAPE(vir_p,(/3,3/)))
	
	! END SUBROUTINE get_forces
	
	! FUNCTION get_status(driver) RESULT(status)
	! 	!> GET DRIVER STATUS
	! 	!> AND UPDATE current_status
	! 	IMPLICIT NONE
	! 	INTEGER, INTENT(INOUT) :: driver
	! 	CHARACTER(MSGLEN) :: reply
	! 	INTEGER :: info
	! 	INTEGER :: status
		
	! 	status=state%Disconnected
		
	! 	if(driver==-1) return

	! 	call writebuffer(driver,msg("status"),MSGLEN,info)	
	! 	call readbuffer(driver,reply,MSGLEN,info)			
	! 	!write(0,*) "  @SOCKET: ",reply, msg("havedata")

	! 	if(trim(reply)=="" .OR. info<0) then
	! 		write(0,*) "  @SOCKET: no response from client!"
	! 		driver=-1
	! 		status=state%Disconnected
	! 	elseif(reply == msg("ready")) then
	! 		status=state%Ready
	! 	elseif(reply == msg("needinit")) then
	! 		status=state%NeedsInit
	! 	elseif(reply == msg("havedata")) then
	! 		status=state%HasData
	! 	else
	! 		WRITE(0,*) "  @SOCKET: Unrecognized reply: "//reply
	! 		status=state%Up 
	! 		!driver=-1
	! 	endif
	
	! END FUNCTION get_status
	
	SUBROUTINE socket_shutdown()
		IMPLICIT NONE
		INTEGER :: info,i
		
		!call writebuffer(server_ID,msg("exit"),MSGLEN,info)
		!current_status=state%Disconnected
		do i=1,n_sockets
			call sockets(i)%shutdown_workers()
			call close_socket(sockets(i)%server_ID)
			write(*,*) "  @SOCKET: closed server",i
		enddo
		
	END SUBROUTINE socket_shutdown

	SUBROUTINE shutdown_workers(socket)
		IMPLICIT NONE
		CLASS(SOCKET_TYPE), INTENT(INOUT) :: socket
		TYPE(WORKER_CHAIN), POINTER :: worker
		INTEGER :: info

		worker=>socket%worker_pool%next
		DO while(associated(worker))
			call writebuffer(worker%id,msg("exit"),MSGLEN,info)
			call close_socket(worker%id)
			worker%current_status=state%Disconnected
			worker=>worker%next
		ENDDO

	END SUBROUTINE shutdown_workers
	
	FUNCTION msg(str)
		IMPLICIT NONE
		CHARACTER(*), INTENT(in) :: str
		CHARACTER(MSGLEN) :: msg
		CHARACTER(:), ALLOCATABLE :: tmp
		
		tmp=to_upper_case(trim(str))
		
		if(len(tmp) > MSGLEN) then
			msg=tmp(1:MSGLEN)
		else
			msg=tmp
		endif
		
	END FUNCTION msg

	! SUBROUTINE poll_loop()
	! 	IMPLICIT NONE
		
	! 	wait_driver=.FALSE.
	! 	DO
	! 		current_status=get_status(socket_ID)
	! 		if(current_status==state%Disconnected) then
	! 			if(.not. wait_driver) then
	! 				wait_driver=.TRUE.
	! 				WRITE(*,*) "  @SOCKET: No driver connected. Waiting for a driver."
	! 				socket_ID=-1
	! 				call check_for_new_client(server_ID,socket_ID)
	! 			endif
	! 		elseif(current_status==state%NeedsInit) then
	! 			wait_driver=.FALSE.
	! 			call send_init(socket_ID,1,"hello")
	! 		elseif(current_status==state%Ready &
	! 				.OR. current_status==state%HasData) then
	! 			wait_driver=.FALSE.			
	! 			EXIT
	! 		elseif(current_status==state%Up) then
	! 			wait_driver=.FALSE.	
	! 			WRITE(0,*) "  @SOCKET: Unexpected 'Up' status. Polling again."
	! 		else
	! 			WRITE(0,*) "  @SOCKET: Unexpected status ",current_status
	! 			!call socket_shutdown()
	! 		endif		
	! 	ENDDO
		
	! END SUBROUTINE poll_loop

	! SUBROUTINE get_socket_pot_info(X,Pot,Forces,vir)
	! 	IMPLICIT NONE
	! 	real(wp), intent(in)  			:: X(:,:)
	! 	real(wp), intent(out), OPTIONAL :: Forces(size(X,1),size(X,2))
	! 	real(wp), intent(out), OPTIONAL :: Pot
	! 	real(wp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
		
	! 	DO while(current_status/=state%ready)
	! 		call poll_loop()
	! 		if(current_status==state%HasData) then
	! 			WRITE(0,*) "  @SOCKET:	Unexpected 'HasData' status. Flushing buffer."
	! 			call get_forces(socket_ID,Pot,Forces)
	! 		endif
	! 	ENDDO
	! 	if(current_status==state%ready) then
	! 		DO
	! 			call send_pos(socket_ID,X,simulation_box%hcell,simulation_box%hcell_inv)
	! 			call poll_loop()
	! 			if(current_status==state%HasData) then
	! 				if(present(Forces) .AND. present(Pot)) then
	! 					if(present(vir)) then
	! 						call get_forces(socket_ID,Pot,Forces,vir) 
	! 					else
	! 						call get_forces(socket_ID,Pot,Forces)
	! 					endif
	! 					exit
	! 				else
	! 					STOP "Error: both 'Forces' and 'Pot' must be present for socket potentials!"
	! 				endif
	! 			elseif(current_status==state%ready) then
	! 				WRITE(0,*) "  @SOCKET:	Unexpected 'ready' status. Probably, client just recovered."
	! 				WRITE(0,*) "            Sending positions again."
	! 			else
	! 				WRITE(0,*) "  @SOCKET:	Unexpected status ",current_status
	! 				STOP "Execution stopped."
	! 			endif
	! 		ENDDO
	! 	else
	! 		WRITE(0,*) "  @SOCKET:	Unexpected status ",current_status
	! 		STOP "Execution stopped."
	! 	endif		
		
	! END SUBROUTINE get_socket_pot_info 	

	function hessian_socket(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

		STOP "Hessian not compatible with socket potential"
		
	end function hessian_socket

END MODULE socket_potential
