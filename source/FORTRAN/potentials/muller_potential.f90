module muller_potential
    use kinds
    IMPLICIT NONE

    real(wp), dimension(4) ::  A_p = (/-200._wp, -100._wp, -170._wp, 15._wp/)
    real(wp), dimension(4) ::  aa_p = (/-1._wp, -1._wp, -6.5_wp, 0.7_wp/)
    real(wp), dimension(4) ::  bb_p = (/0._wp, 0._wp, 11._wp, 0.6_wp/)
    real(wp), dimension(4) ::  cc_p = (/-10._wp, -10._wp, -6.5_wp, 0.7_wp/)
    real(wp), dimension(4) ::  xx_p = (/1._wp, 0._wp, -0.5_wp, -1._wp/)
    real(wp), dimension(4) ::  yy_p = (/0._wp, 0.5_wp, 1.5_wp, 1._wp/)

    PRIVATE
    PUBLIC :: Pot_muller,dPot_muller, hessian_muller

CONTAINS

    function Pot_muller(X) result(U)
		implicit none
		real(wp),intent(in)  :: X(:,:)
        REAL(wp) :: U
        INTEGER :: it
		
		U  = 0._wp
        do it = 1, 4
            U = U + A_p(it)*exp( aa_p(it)*(X(1,1)-xx_p(it))**2 + bb_p(it)*(X(1,1) - xx_p(it))*(X(2,1)-yy_p(it)) + &
                cc_p(it)*(X(2,1)-yy_p(it))**2 )
        enddo
		
	end function Pot_muller

	function dPot_muller(X) result(dU)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: dU(size(X,1),size(X,2))
        INTEGER :: it
        REAL(wp) :: alpha_x,alpha_y

		dU = 0._wp
        do it = 1, 4
            alpha_x = 2*aa_p(it)*(X(1,1)-xx_p(it)) + bb_p(it)*(X(2,1)-yy_p(it))
            dU(1,1) = dU(1,1) + alpha_x*A_p(it)*exp( aa_p(it)*(X(1,1)-xx_p(it))**2 + &
                bb_p(it)*(X(1,1) - xx_p(it))*(X(2,1)-yy_p(it)) +cc_p(it)*(X(2,1)-yy_p(it))**2 )

            alpha_y = bb_p(it)*(X(1,1) - xx_p(it)) + 2*cc_p(it)*(X(2,1)-yy_p(it))
            dU(2,1) = dU(2,1) + alpha_y*A_p(it)*exp( aa_p(it)*(X(1,1)-xx_p(it))**2 + bb_p(it)*(X(1,1) - &
                xx_p(it))*(X(2,1)-yy_p(it)) + cc_p(it)*(X(2,1)-yy_p(it))**2 )
        enddo
		
	end function dPot_muller

    function hessian_muller(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

		STOP "Hessian not implemented for Muller potential"
		
	end function hessian_muller


end module muller_potential