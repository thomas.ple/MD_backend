MODULE model_potentials
	USE kinds
	USE nested_dictionaries
	USE basic_types
	USE muller_potential
	USE atomic_units
	IMPLICIT NONE
	
	CHARACTER(*), PARAMETER :: POT_CAT="potential"

	!MORSE AND DOUBLE MORSE POTENTIAL
	REAL(wp),SAVE :: D
	REAL(wp),SAVE :: alpha
		!MORSE
		REAL(wp),SAVE ::	xmax
		!DOUBLE MORSE
		REAL(wp),SAVE :: dAB
		REAL(wp),SAVE :: d0
		REAL(wp),SAVE :: ADM
		REAL(wp),SAVE :: BDM
		REAL(wp),SAVE :: CDM
		REAL(wp),SAVE :: asym
	!HARMONIC OSCILLATOR
	REAL(wp), ALLOCATABLE,SAVE ::  Omega0(:)
	REAL(wp), ALLOCATABLE,SAVE ::  mass_HO(:)
	REAL(wp), ALLOCATABLE,SAVE :: k_HO(:)
	!QUARTIC OSCILLATOR
	REAL(wp),SAVE :: QO
	!COUPLED OSCILLATORS
	REAL(wp),SAVE :: c3
	REAL(wp),SAVE :: c3_CO2,c3_CO2_sym
	REAL(wp),SAVE :: c4
	!LENNARD-JONES
	REAL(wp),SAVE :: sigma6,r_refl,epsil

	REAL(wp), SAVE :: refpos
	LOGICAL, SAVE :: symmetrize_potential

	PRIVATE
	PUBLIC initialize_model_potentials, POT_CAT, k_HO,c3_CO2

CONTAINS

	subroutine initialize_model_potentials(param_library,n_dim,n_atoms,pot_name)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), INTENT(IN) :: param_library
		CHARACTER(*), INTENT(IN) :: pot_name
		TYPE(DICT_STRUCT), pointer :: pot_lib
		INTEGER, INTENT(in) :: n_dim,n_atoms
		REAL(wp) :: sigma

		!GET POTENTIAL PARAMETERS
		pot_lib => param_library%get_child(POT_CAT)

		!ASSIGN Pot AND dPot
		SELECT CASE(pot_name)
		CASE("DM")
			if(n_atoms/=2 .or. n_dim/=1) stop "required: n_atoms=2, n_dim=1 for DM potential. Stopping execution..."
			Pot=>Pot_DM
			dPot=>dPot_DM	
			compute_hessian => hessian_DM		
		CASE("HO")
			if(n_dim/=1) stop "required: n_dim=1 for HO potential. Stopping execution..."
			Pot=>Pot_HO
			dPot=>dPot_HO
			compute_hessian => hessian_HO
		CASE("HO+C3")
			if(n_dim/=1) stop "required: n_dim=1 for HO+c3 potential. Stopping execution..."
			Pot=>Pot_HOc3
			dPot=>dPot_HOc3
			compute_hessian => hessian_HOc3
		CASE("MO")
			if(n_dim/=1) stop "required: n_dim=1 for MO potential. Stopping execution..."
			Pot=>Pot_Mo
			dPot=>dPot_Mo
			compute_hessian => hessian_Mo
		CASE("MO_CENTRAL")
			if(n_dim/=1) stop "required: n_dim=1 for MO_CENTRAL potential. Stopping execution..."
			Pot=>Pot_Mo_central
			dPot=>dPot_Mo_central
			compute_hessian => hessian_Mo_central
		CASE("CO")
			if(n_atoms/=2 .or. n_dim/=1) stop "required: n_atoms=2, n_dim=1 for CO potential. Stopping execution..."
			Pot=>Pot_CO
			dPot=>dPot_CO
			compute_hessian => hessian_CO
		CASE("DM1D")
			if(n_atoms/=1 .or. n_dim/=1) stop "required: n_atoms=1, n_dim=1 for DM1D potential. Stopping execution..."
			Pot=>Pot_DM1D
			dPot=>dPot_DM1D
			compute_hessian => hessian_DM1D
		CASE("DMCH")
			if(n_atoms/=1 .or. n_dim/=1) stop "required: n_atoms=1, n_dim=1 for DMCH potential. Stopping execution..."
			Pot=>Pot_DMCH
			dPot=>dPot_DMCH
			compute_hessian => hessian_DM1D
		CASE("DMJOH")
			if(n_atoms/=1 .or. n_dim/=1) stop "required: n_atoms=1, n_dim=1 for DMJOH potential. Stopping execution..."
			Pot=>Pot_DMJOH
			dPot=>dPot_DMJOH
			compute_hessian => hessian_DMJOH
		CASE("CO2")
			if(n_atoms/=3 .or. n_dim/=1) stop "required: n_atoms=3, n_dim=1 for CO2 potential. Stopping execution..."
			Pot=>Pot_CO2
			dPot=>dPot_CO2
			compute_hessian => hessian_CO2
		CASE("QO")
			if(n_dim/=1) stop "required: n_dim=1 for this potential. Stopping execution..."
			Pot=>Pot_QO
      dPot=>dPot_QO
			compute_hessian => hessian_QO
		CASE("MULLER")
			if(n_atoms /=2 .or. n_dim/=1) stop "required: n_atoms=2, n_dim=1 for MULLER potential. Stopping execution..."
			Pot=>Pot_muller
			dPot=>dPot_muller
			compute_hessian => hessian_muller
		CASE("LJ")
			if(n_atoms <2 .or. n_dim/=3) stop "required: n_atoms>2, n_dim=3 for LJ potential. Stopping execution..."
			Pot=>Pot_LJ
			dPot=>dPot_LJ
			compute_hessian => hessian_LJ
		CASE DEFAULT
			write(0,*) "Error: '"//pot_name//"' is not a correct potential."
			stop
		END SELECT
		!ASSIGN get_pot_info

		symmetrize_potential=pot_lib%get("symmetrize_potential",default=.FALSE.)
		if(symmetrize_potential) then
			refpos=pot_lib%get("symmetrize_ref_position")
			get_pot_info => get_model_pot_info_symmetrized
			write(*,*) "Warning: symmetrized potential around X=",refpos
		else
			get_pot_info => get_model_pot_info
		endif
		
		!GET MASS 
		if(param_library%has_key("parameters/mass")) then
			mass_HO=param_library%get("parameters/mass")
			!! @input_file PARAMETERS/mass (potentials)
			if(size(mass_HO) /= n_atoms) &
				STOP "Error: specified 'mass' size does not match 'n_atoms'"			
		else
			write(0,*) "Warning: 'mass' not specified, using default of 1 a.m.u. for all dimensions."
			allocate(mass_HO(n_atoms))
			mass_HO=Mprot
		endif

		
		

		SELECT CASE(pot_name)
		CASE("HO","HO+C3","CO","CO2")
			allocate(Omega0(n_atoms),k_HO(n_atoms)) ; Omega0=1500._wp/cm1
			Omega0 = pot_lib%get("omega0", default=Omega0)
			!! @input_file POTENTIAL/omega0 (potentials, default=1500./cm1, if pot_name="HO","CO","CO2")
			if(size(Omega0) /= n_atoms) &
				STOP "Error: specified 'Omega0' size does not match 'n_atoms'"
			k_HO(:) = mass_HO(:)*Omega0(:)**2
		END SELECT

		D=pot_lib%get("d",default=(20._wp/kcalperMol))
		!! @input_file POTENTIAL/d (potentials, default=20./kcalperMol, if pot_name="DM","DM1D","MO")
		alpha=pot_lib%get("alpha",default=(2.5_wp*bohr))
		!! @input_file POTENTIAL/alpha (potentials, default=2.5*bohr, if pot_name="DM","DM1D","MO")
		xmax=pot_lib%get("xmax",default=(2.5_wp/bohr))
		!! @input_file POTENTIAL/xmax (potentials, default=2.5/bohr, if pot_name="MO")
		dAB=pot_lib%get("dab",default=(2.6_wp/bohr))
		!! @input_file POTENTIAL/dab (potentials, default=2.6/bohr, if pot_name="DM1D")
		d0=pot_lib%get("d0",default=(0.96_wp/bohr))
		!! @input_file POTENTIAL/d0 (potentials, default=0.96/bohr, if pot_name="DM","DM1D")
		ADM=pot_lib%get("adm",default=(2.32e5_wp/kcalperMol))
		!! @input_file POTENTIAL/adm (potentials, default=2.32e5/kcalperMol, if pot_name="DM","DM1D")
		BDM=pot_lib%get("bdm",default=(3.15_wp*bohr))
		!! @input_file POTENTIAL/bdm (potentials, default=3.15*bohr, if pot_name="DM","DM1D")
		CDM=pot_lib%get("cdm",default=(2.31e4_wp/(kcalperMol*bohr**6)))
		!! @input_file POTENTIAL/cdm (potentials, default=2.31e4/(kcalperMol*bohr**6), if pot_name="DM","DM1D")
		asym=pot_lib%get("asym",default=(1._wp))
		!! @input_file POTENTIAL/asym (potentials, default=1., if pot_name="DM","DM1D")
		QO=pot_lib%get("qo",default=(0.1_wp))
		!! @input_file POTENTIAL/qo (potentials, default=0.1, if pot_name="QO")
		c3=pot_lib%get("c3",default=(0.1_wp))
		!! @input_file POTENTIAL/c3 (potentials, default=0.1, if pot_name="CO","CO2")
		c4=pot_lib%get("c4",default=(0.0_wp))
		!! @input_file POTENTIAL/c4 (potentials, default=0., if pot_name="CO")
		epsil=pot_lib%get("epsil",default=34.9_wp/kelvin) 
		!! @input_file POTENTIAL/epsil (potentials, default=34.9_wp/kelvin, if pot_name="LJ")			
		sigma=pot_lib%get("sigma",default=2.78_wp/bohr) ; sigma6=sigma**6 
		!! @input_file POTENTIAL/sigma (potentials, default=2.78_wp/bohr, if pot_name="LJ")
		r_refl=pot_lib%get("r_refl",default=10._wp/bohr)
		!! @input_file POTENTIAL/r_refl (potentials, default=10._wp/bohr, if pot_name="LJ")

		if(pot_name=="CO2") then
			if(mass_HO(2) /= mass_HO(3))  &
				STOP "Error: mass of atoms 2 and 3 must be the same for CO2"
			c3_CO2=asym*c3*sqrt(mass_HO(1))*mass_HO(2)
			c3_CO2_sym=(1.-asym)*c3*sqrt(mass_HO(2))*mass_HO(1)
			write(*,*) Omega0*cm1, c3
		endif

	end subroutine initialize_model_potentials

	SUBROUTINE get_model_pot_info(X,Potential,Forces,hessian,vir)
		IMPLICIT NONE
		real(wp), intent(in)  			:: X(:,:)
		real(wp), intent(out) :: Forces(size(X,1),size(X,2))
		real(wp), intent(out) :: Potential
		real(wp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
		real(wp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))		
		
		Forces=-dPot(X)
		Potential=Pot(X)
		if(present(hessian)) hessian=compute_hessian(X)
		
	END SUBROUTINE get_model_pot_info

	SUBROUTINE get_model_pot_info_symmetrized(X,Potential,Forces,hessian,vir)
		IMPLICIT NONE
		real(wp), intent(in)  			:: X(:,:)
		real(wp), intent(out) :: Forces(size(X,1),size(X,2))
		real(wp), intent(out) :: Potential
		real(wp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
		real(wp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))		
		real(wp) :: lamb

		lamb=1.0_wp

		Forces=-0.5*lamb*(dPot(refpos+lamb*X)-dPot(refpos-lamb*X))
		Potential=0.5*(Pot(refpos+lamb*X)+Pot(refpos-lamb*X))
		

		if(present(hessian)) hessian=0.5*lamb*lamb &
				*(compute_hessian(refpos+lamb*X)+compute_hessian(refpos-lamb*X))
		
	END SUBROUTINE get_model_pot_info_symmetrized

	real(wp) function Pot_DM(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: exp_sum, exp_diff,sig

		sig=BDM/X(2,1)
		exp_sum=exp(-alpha*(X(2,1)/2+X(1,1)-d0))
		exp_diff=exp(-alpha*(X(2,1)/2-X(1,1)-d0)/asym)
		
		Pot_DM=D*( &
					exp_sum*exp_sum &
					-2*exp_sum + 1 &
					+(asym**2)*( &
						exp_diff*exp_diff &
						-2*exp_diff &
					) &
				) &!+ ADM*exp(-BDM*X(2,1))-CDM/(X(2,1)**6)
				+ ADM*(sig**12-2.*sig**6) &
				+ 0.5*CDM*(X(2,1)-BDM)**2

				
		
	end function Pot_DM

	function dPot_DM(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: exp_sum, exp_diff,sig

		sig=BDM/X(2,1)
		exp_sum=exp(-alpha*(X(2,1)/2+X(1,1)-d0))
		exp_diff=exp(-alpha*(X(2,1)/2-X(1,1)-d0)/asym)

		F=0
		F(1,1)=D*2*alpha*( &
				-exp_sum*exp_sum &
				+ exp_sum &
				+ asym*( &
					exp_diff*exp_diff &
					- exp_diff &
				) &
			  )

		F(2,1)=D*alpha*( &
				-exp_sum*exp_sum &
				+exp_sum &
				+asym*( &
					-exp_diff*exp_diff &
					+exp_diff &
				) &
			) & !- ADM*BDM*exp(-BDM*X(2,1))+6*CDM/(X(2,1)**7)
			-12.*(ADM/BDM)*(sig**13-sig**7) &
			+CDM*(X(2,1)-BDM)
		
	end function dPot_DM

	function hessian_DM(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		real(wp) :: exp_sum, exp_diff,sig

		sig=BDM/X(2,1)
		exp_sum=exp(-alpha*(X(2,1)/2+X(1,1)-d0))
		exp_diff=exp(-alpha*(X(2,1)/2-X(1,1)-d0)/asym)

		H=0
		H(1,1)=D*2*alpha*alpha*( &
				+2*exp_sum*exp_sum &
				- exp_sum &
				+ ( &
					2*exp_diff*exp_diff &
					- exp_diff &
				) &
			  )

		H(2,2)=D*alpha*alpha*( &
				+exp_sum*exp_sum &
				-0.5_wp*exp_sum &
				+( &
					exp_diff*exp_diff &
					-0.5_wp*exp_diff &
				) &
			) & !+ ADM*BDM*BDM*exp(-BDM*X(2,1))-42*CDM/(X(2,1)**8)
			+12._wp*(ADM/(BDM*BDM))*(13._wp*sig**14-7*sig**8) &
			+CDM


		H(1,2)=D*2*alpha*alpha*( &
				+exp_sum*exp_sum &
				-0.5_wp* exp_sum &
				+( &
					-exp_diff*exp_diff &
					+0.5_wp*exp_diff &
				) &
			  )
		H(2,1)=H(1,2)
		
	end function hessian_DM

	real(wp) function Pot_DM1D(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: exp_sum, exp_diff

		exp_sum=exp(-alpha*(dAB/2+X(1,1)-d0))
		exp_diff=exp(-alpha*(dAB/2-X(1,1)-d0)/asym)
		
		Pot_DM1D=D*( &
					exp_sum*exp_sum &
					-2*exp_sum + 1 &
					+(asym**2)*( &
						exp_diff*exp_diff &
						-2*exp_diff &
					) &
				) + ADM*exp(-BDM*dAB)-CDM/(dAB**6)
		
	end function Pot_DM1D

	function dPot_DM1D(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: exp_sum, exp_diff

		exp_sum=exp(-alpha*(dAB/2+X(1,1)-d0))
		exp_diff=exp(-alpha*(dAB/2-X(1,1)-d0)/asym)

		F=0
		F(1,1)=D*2*alpha*( &
				-exp_sum*exp_sum &
				+exp_sum &
				+asym*( &
					exp_diff*exp_diff &
					-exp_diff &
				) &
			  )
		
	end function dPot_DM1D

	function hessian_DM1D(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		real(wp) :: exp_sum, exp_diff

		exp_sum=exp(-alpha*(dAB/2+X(1,1)-d0))
		exp_diff=exp(-alpha*(dAB/2-X(1,1)-d0)/asym)

		H=0
		H(1,1)=D*2*alpha*alpha*( &
				+2*exp_sum*exp_sum &
				-exp_sum &
				+( &
					+2*exp_diff*exp_diff &
					-exp_diff &
				) &
			  )
		
	end function hessian_DM1D

	real(wp) function Pot_DMCH(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: exp_sum, exp_diff
		
		Pot_DMCH=sum(D*((d0*cosh(alpha*X)-1._wp)/(d0-1._wp))**2)
		
	end function Pot_DMCH

	function dPot_DMCH(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: exp_sum, exp_diff

		F=2*D*alpha*d0*(d0*cosh(alpha*x)-1._wp)*sinh(alpha*x)/(d0-1._wp)**2
		
	end function dPot_DMCH
	

	function hessian_DMCH(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		real(wp) :: exp_sum, exp_diff

		STOP "Hessian not implemented for DMCH potential"
		
	end function hessian_DMCH

	real(wp) function Pot_DMJOH(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp), parameter :: aa=7.10*bohr, bb=2.00*bohr  ! Ang-1
		real(wp), parameter :: u0=3.35/eV  ! eV
		real(wp) :: usum,udiff
		real(wp) :: exp_suma, exp_diffa
		real(wp) :: exp_sumb, exp_diffb
		real(wp) :: deq

		deq=dAB/2-d0

		exp_suma=exp(aa*(deq+X(1,1)))
		exp_diffa=exp(aa*(deq-X(1,1)))
		exp_sumb=exp(-bb*(deq+X(1,1)))
		exp_diffb=exp(-bb*(deq-X(1,1)))
		
		usum=(aa*(exp_sumb-1)+bb*(exp_suma-1))/(aa+bb*exp_suma)
		udiff=(aa*(exp_diffb-1)+bb*(exp_diffa-1))/(aa+bb*exp_diffa)

		Pot_DMJOH=u0*(usum+udiff-2._wp)
		
	end function Pot_DMJOH

	function dPot_DMJOH(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp), parameter :: aa=7.10*bohr, bb=2.00*bohr  ! Ang-1
		real(wp), parameter :: u0=3.35/eV  ! eV
		real(wp) :: esuma, ediffa,esumb,ediffb
		real(wp) :: deq
		real(wp) :: den_sum,den_diff

		deq=dAB/2-d0

		esuma=exp(aa*(deq+X(1,1)))
		ediffa=exp(aa*(deq-X(1,1)))
		esumb=exp(-bb*(deq+X(1,1)))
		ediffb=exp(-bb*(deq-X(1,1)))

		den_sum=aa+bb*esuma
		den_diff=aa+bb*ediffa

		F=0
		F(1,1)=aa*bb*u0*( &
			(esuma-esumb)/den_sum &
			-(ediffa-ediffb)/den_diff &
			-(bb*(esuma-1)+aa*(esumb-1))*esuma/den_sum**2 &
			+(bb*(ediffa-1)+aa*(ediffb-1))*ediffa/den_diff**2 &
		)
		
	end function dPot_DMJOH
	

	function hessian_DMJOH(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		real(wp) :: exp_sum, exp_diff

		STOP "Hessian not implemented for DMDMJOH potential"
		
	end function hessian_DMJOH

	real(wp) function Pot_HO(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)

		Pot_HO=0.5*sum(k_HO(:)*X(:,1)**2)
		
	end function Pot_HO

	function dPot_HO(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))

		F=0
		F(:,1)=k_HO(:)*X(:,1)
		
	end function dPot_HO

	function hessian_HO(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i
		H=0
		DO i=1,size(X,1)
			H(i,i)=k_HO(i)
		ENDDO
		
	end function hessian_HO

	real(wp) function Pot_HOc3(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)

		Pot_HOc3=SUM(0.5_wp*k_HO(:)*X(:,1)**2 &
				+c3*X(:,1)**3/3._wp &
				+c4*X(:,1)**4/4._wp)
		
	end function Pot_HOc3

	function dPot_HOc3(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))

		F=0
		F(:,1)=k_HO(:)*X(:,1) &
				+c3*X(:,1)**2 &
				+c4*X(:,1)**3
		
	end function dPot_HOc3

	function hessian_HOc3(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i

		H=0
		DO i=1,size(X,1)*size(X,2)
			H(i,i)=k_HO(i) + 2._wp*c3*X(i,1) + 3._wp*c4*X(i,1)**2
		ENDDO
		
	end function hessian_HOc3

	real(wp) function Pot_CO(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		
		Pot_CO=0.5*sum(k_HO(:)*X(:,1)**2) &
				+c3*X(1,1)*X(2,1)**2
				!+c3*(X(1,1)-X(2,1))**3 !&
				!+c4*(X(1,1)-X(2,1))**4
		
	end function Pot_CO

	function dPot_CO(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: dC

		F=0
		F(1,1)=k_HO(1)*X(1,1)+c3*X(2,1)**2
		F(2,1)=k_HO(2)*X(2,1)+2.*c3*X(1,1)*X(2,1)

		!dC=3*c3*(X(1,1)-X(2,1))**2 &
		!		+4*c4*(X(1,1)-X(2,1))**3

		!F(1,1)=F(1,1)+dC
		!F(2,1)=F(2,1)-dC
		
	end function dPot_CO

	function hessian_CO(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i
		real(wp) :: ddc

		! ddc=6*c3*(X(1,1)-X(2,1)) &
		! 	+12*c4*(X(1,1)-X(2,1))**2

		! H=0
		! H(1,1) = k_HO(1) + ddc
		! H(2,2) = k_HO(2) + ddc
		! H(1,2) = -ddc
		! H(2,1) = -ddc

		H=0
		H(1,1) = k_HO(1)
		H(2,2) = k_HO(2) + 2.*c3*X(1,1)
		H(1,2) = 2.*c3*X(2,1) ; H(2,1)=H(1,2)
		
	end function hessian_CO
    
	real(wp) function Pot_CO2(X)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		
		Pot_CO2=0.5_wp*sum(k_HO(:)*X(:,1)**2) &
				+0.5_wp*c3_CO2*X(1,1)*(X(2,1)**2+X(3,1)**2) &
				+0.5_wp*c3_CO2_sym*(X(1,1)**2)*(X(2,1)+X(3,1))

		
	end function Pot_CO2

	function dPot_CO2(X) result(F)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))

		F=0
		F(:,1)=k_HO(:)*X(:,1)
		F(1,1)=F(1,1)+0.5_wp*c3_CO2*(X(2,1)**2+X(3,1)**2) &
							+c3_CO2_sym*X(1,1)*(X(2,1)+X(3,1))
		F(2,1)=F(2,1)+c3_CO2*X(2,1)*X(1,1) &
							+0.5_wp*c3_CO2_sym*X(1,1)**2
		F(3,1)=F(3,1)+c3_CO2*X(3,1)*X(1,1) &
							+0.5_wp*c3_CO2_sym*X(1,1)**2
		
	end function dPot_CO2

	function hessian_CO2(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i
		real(wp) :: ddc3, ddc4

		H=0
		H(1,1) = k_HO(1) + c3_CO2_sym*(X(2,1)+X(3,1))
		H(2,2) = k_HO(2) + c3_CO2*X(1,1)
		H(3,3) = k_HO(3) + c3_CO2*X(1,1)

		H(1,2) = c3_CO2*X(2,1)+c3_CO2_sym*X(1,1) ; H(2,1) = H(1,2)
		H(1,3) = c3_CO2*X(3,1)+c3_CO2_sym*X(1,1) ; H(3,1) = H(1,3)

		!STOP "Hessian for CO2 not available"
		
	end function hessian_CO2

	real(wp) function Pot_Mo(X)
		implicit none
		real(wp), intent(in) :: X(:,:)
    !real(wp) :: alp,alp2
		
		 if(sqrt(sum(X(:,1)**2))<=xmax) then
		 	Pot_Mo=sum(D*(exp(-2*alpha*X(:,1))-2*exp(-alpha*X(:,1))+1))
		 else
		 	Pot_Mo=sum(D*(exp(-2*alpha*X(:,1))-2*exp(-alpha*X(:,1)) &
		 			+exp(1._wp*(X(:,1)-xmax))))
		 endif

		! alp=alpha*(X(1,1)-d0)
		! alp2=alp*alp
		! Pot_Mo=D*alp2*(1._wp-alp+(7._wp/12._wp)*alp2)
		
	end function Pot_Mo

	function dPot_Mo(X) result(F)
		implicit none
		real(wp),intent(in) :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		!real(wp) :: alp,alp2
		
		 F=0
		 if(sqrt(sum(X(:,1)**2))<=xmax) then
		 	F(:,1)=D*2*alpha*(-exp(-2*alpha*X(:,1)) &
		 			+exp(-alpha*X(:,1)) )
		 else
		 	F(:,1)=D*2*alpha*(-exp(-2*alpha*X(:,1)) &
		 			+exp(-alpha*X(:,1)))+D*1._wp*exp(1._wp*(X(:,1)-xmax))
		 endif

		! alp=alpha*(X(1,1)-d0)
		! alp2=alp*alp
    ! F=0.
		! F(1,1)=D*alpha*alp*(2._wp-3._wp*alp+(7._wp/3._wp)*alp2)
		
	end function dPot_Mo

	function hessian_Mo(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i

		H=0
		if(sqrt(sum(X(:,1)**2))<=xmax) then
			DO i=1,size(X,1)
				H(i,i)=D*2*alpha*alpha*(2*exp(-2*alpha*X(i,1))   &
						-exp(-alpha*X(i,1)))
			ENDDO
		else
			DO i=1,size(X,1)
				H(i,i)=D*2*alpha*alpha*(2*exp(-2*alpha*X(i,1))   &
						-exp(-alpha*X(i,1)))+D*1._wp*exp(1._wp*(X(i,1)-xmax))
			ENDDO
		endif
		
	end function hessian_Mo

	real(wp) function Pot_Mo_central(X)
		implicit none
		real(wp), intent(in) :: X(:,:)
		real(wp) :: r,alp,alp2

		r=NORM2(X(:,1))
		alp=alpha*(r-d0)
		alp2=alp*alp

		Pot_Mo_central = D*alp2*(1._wp-alp+(7._wp/12._wp)*alp2)
	
	end function Pot_Mo_central

	function dPot_Mo_central(X) RESULT(F)
		implicit none
		real(wp), intent(in) :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: r,alp,alp2,dvr

		F=0._wp
		r=NORM2(X(:,1))
		alp=alpha*(r-d0)
		alp2=alp*alp

		dvr = D*alpha*alp*(2._wp-3._wp*alp+(7._wp/3._wp)*alp2)
		F(:,1) = dvr*X(:,1)/r
	
	end function dPot_Mo_central

	function hessian_Mo_central(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i

		STOP "Hessian not implemented for MO_CENTRAL potential"
		
	end function hessian_Mo_central

	real(wp) function Pot_QO(X)
		implicit none
		real(wp), intent(in) :: X(:,:)
		real(wp) :: r2
		r2=sum(X(:,1)**2)
		Pot_QO=QO*r2*r2
		
	end function Pot_QO

	function dPot_QO(X) result(F)
		implicit none
		real(wp),intent(in) :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: r2
		r2=sum(X(:,1)**2)
		F=0
		F(:,1)=4*QO*r2*X(:,1)
		
	end function dPot_QO

	function hessian_QO(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i,j
		real(wp) :: r2
		r2=sum(X(:,1)**2)

		H=0
		DO i=1,size(X,1)
			H(i,i)=4*r2 + 8*X(i,1)**2
			DO j=i+1,size(X,1)
				H(i,j)=8*X(i,1)*X(j,1)
				H(j,i)=H(i,j)
			ENDDO
		ENDDO
		
	end function hessian_QO

	real(wp) function Pot_LJ(X)
		! Lennard-Jones potential
		! V = epsil*((sigma/r)**12-2*(sigma/r)**6)
		! sigma => Al 2.54d0 !Ne 2.78d0 !Ar 3.4d0  ! angströms
		! epsil => Al 1450.6d0 ! Ne 34.9d0 ! Ar 120.d0! K 
		! parameters : sigma6, r_refl, epsil
		implicit none
		real(wp),intent(in) :: X(:,:)
		real(wp) :: rkl(3)
		real(wp) :: dsq, d6, fkl
		integer :: k,l,nat

		nat = size(X,1)
		pot_LJ    = 0.d0		

		!sigma6 = pot_param%sigma**6
		do k = 1, nat-1 ; do l = k+1, nat
			rkl = X(k,:)-X(l,:) 
			dsq = rkl(1)**2 + rkl(2)**2 + rkl(3)**2
			d6 = sigma6/(dsq*dsq*dsq)
			pot_LJ = pot_LJ + epsil*d6*(d6-2._wp)

			if (sqrt(dsq) > r_refl) then
				pot_LJ = pot_LJ + epsil *( sqrt(dsq) - r_refl)**4
			endif			
		enddo ; enddo

	end function Pot_LJ

	function dPot_LJ(X) result(F)
		implicit none
		real(wp),intent(in) :: X(:,:)
		real(wp) :: F(size(X,1),size(X,2))
		real(wp) :: rkl(3), fklx(3)
		real(wp) :: dsq, d6, fkl
		integer :: k, l, nat

		nat = size(X,1)
		F(:,:) = 0._wp

		do k = 1, nat-1 ; do l = k+1, nat
			
			rkl = X(k,:)-X(l,:) 
			dsq = rkl(1)**2 + rkl(2)**2 + rkl(3)**2
			d6 = sigma6/(dsq*dsq*dsq)
			fkl = 12._wp*epsil*d6*(d6-1._wp)/dsq

			if (sqrt(dsq) > r_refl) then
				fkl = fkl - epsil* 4._wp * (sqrt(dsq) - r_refl)**3
			endif

			fklx = rkl*fkl
			F(k,:) = F(k,:) - fklx
			F(l,:) = F(l,:) + fklx

		enddo ; enddo

	end function dPot_LJ

	function hessian_LJ(X) result(H)
		implicit none
		real(wp),intent(in)  :: X(:,:)
		real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		INTEGER :: i
		real(wp) :: rkl(3), fklx(3)
		real(wp) :: dsq, d6, fkl
		integer :: k, l, nat

		STOP "Hessian not implemented for LJ potential"
		
	end function hessian_LJ


END MODULE model_potentials