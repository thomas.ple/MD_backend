!
!====================================================================================
!
! Implementation of the CO2 force field from
! dx.doi.org/10.1021/jp3007574 | J. Phys. Chem. C 2012, 116, 13079−13091
! for a single CO2 molecule 
!  => harmonic potential for the stretching and bending angles
! expects sequence C O O ...
!
!====================================================================================
!

module ff_co2
  use kinds
  USE basic_types
  IMPLICIT NONE

  PRIVATE
  PUBLIC :: get_ffco2_pot_info, Pot_ffco2, dPot_ffco2,hessian_ffco2

  CONTAINS

SUBROUTINE get_ffco2_pot_info(X,Pot,Forces,hessian,vir)
   IMPLICIT NONE
   real(wp), intent(in)  :: X(:,:)
   real(wp), intent(out) :: Forces(size(X,1),size(X,2))
   real(wp), intent(out) :: Pot
   real(wp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
   real(wp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
   real(wp) :: vir_tmp(size(X,2),size(X,2))
   real(wp) :: box(3)
   INTEGER :: na

   call ffco2(simulation_box%box_lengths,X,size(X,1),Forces,Pot,vir_tmp)
   if(present(vir)) vir = vir_tmp
   if(present(hessian)) hessian = hessian_ffco2(X)

END SUBROUTINE get_ffco2_pot_info

function Pot_ffco2(X) result(U)
   implicit none
   real(wp),intent(in)  :: X(:,:)
   REAL(wp) :: U
   INTEGER :: it
   REAL(wp) :: F(size(X,1),size(X,2))
   real(wp) :: vir_tmp(3,3)

   call ffco2(simulation_box%box_lengths,X,size(X,1),F,U,vir_tmp)

end function Pot_ffco2

function dPot_ffco2(X) result(dU)
   implicit none
   real(wp),intent(in)  :: X(:,:)
   real(wp) :: dU(size(X,1),size(X,2))
   INTEGER :: it
   REAL(wp) :: alpha_x,alpha_y
   real(wp) :: vir_tmp(3,3), U

   call ffco2(simulation_box%box_lengths,X,size(X,1),dU,U,vir_tmp)

   dU=-dU

end function dPot_ffco2

function hessian_ffco2(X) result(H)
   implicit none
   real(wp),intent(in)  :: X(:,:)
   real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

   STOP "Hessian not implemented for ffco2 potential"
   
end function hessian_ffco2

subroutine ffco2(box,rt,na,dvdrt,v, vir)
  use atomic_units
  implicit none  
  integer :: i,j,na,nm,ic,njump
  real(wp) :: r(3,na), dvdr(3,na), box(3), rt(na,3), dvdrt(na,3)
  real(wp) :: oo_eps, oo_sig, rcut,v,vlj,vint
  real(wp) :: ks,kb, angstrom,alp,de
  real(wp) :: qc,qo,theta,reco,vir(3,3), virlj(3,3), virint(3,3)

  ! Set and check number of molecules.
  ! 
  nm = na/3
  if (3*nm .ne. na) stop 'ERROR 1 in POTENTIAL'

  angstrom=1./0.52917721_wp
  !
  ! Set potential parameters - ATOMIC UNITS!
  !
  qc = 0.6512_wp
  qo = -0.3256_wp
  theta = acos(-1.d0)
  reco = 1.162_wp *angstrom ! 1.162 A (CO equilibrium distance)
  !ks = 3.2158 / (angstrom*angstrom) ! 8443 kJ/mol A^-2 (stretching harmonic force constant)
  !kb = 0.17212 ! 451.9 kJ/mol rad^-2 (bending harmonic force constant)

  !! TWEAKS !! 
  kb = 0.179_wp
  ks = 3.85_wp / (angstrom*angstrom)
 ! alp=2._wp
 ! de=ks/(2._wp*alp*alp)


  ! Zero-out potential and derivatives.
  !
  v = 0.d0
  vir = 0.d0
  dvdr(:,:) = 0.d0

  do i=1, na 
    r(:,i)=rt(i,:)
  enddo

  !
  ! *** INTRAMOLECULAR CALCULATION ***
  !
  Call intra_harm_harm(r,dvdr,na,vint,virint,theta,reco,0.5_wp*ks,0.5_wp*kb)  
  !Call intra_morse_harm(r,dvdr,na,vint,virint,theta,reco,de,0.5_wp*kb,alp)  
  v = v + vint
  vir = vir + virint

  ! write(*,*) "force="
  ! write(*,*) dvdr

  ! ....and we're done...
  !
  vir = -1.0d0 *vir
  do i=1, na 
    dvdrt(i,:) = -dvdr(:,i)
  enddo
  return
end Subroutine ffco2

!
!======================================================================
!
! Intramolecular contribution to CO2 energy and derivatives
! (based on q-TIP4P/F )
!
! Harmonic bend and stretch
!
!======================================================================
!
Subroutine intra_harm_harm(r,dvdr,na,v,vir,theta,reoh,apot,bpot)
  USE matrix_operations, only: cross_product
  implicit none
  integer :: na,j
  real(wp), intent(in) :: apot,bpot, theta,reoh
  real(wp) :: r(3,na), dvdr(3,na), vir(3,3)
  real(wp) :: dr1,dr2,dr3
  real(wp) :: dx1,dy1,dz1,v,dx2,dy2,dz2,dr1sq,dr2sq,dr3dx3
  real(wp) :: dx3,dy3,dz3,dr3sq,dr1dx1,dr1dy1,dr1dz1
  real(wp) :: dr2dx2,dr2dy2,dr2dz2,dr3dy3,dr3dz3,u,vv,v2,arg,ang
  real(wp) :: dang,uprime,vprime,grad
  real(wp) :: dvdx1,dvdx2,dvdx3,dvdy1,dvdy2,dvdy3
  real(wp) :: dvdz1,dvdz2,dvdz3,dvdr1,dvdr2,dvdr3
  real(wp) :: dthetadr1,dthetadr2,dthetadr3
  real(wp) :: darg,dr1a,dr2a,dr3a
  real(wp) :: de,drasq,drbsq,cp
  real(wp) :: f1,f2,deb,a1,a2,a3,xx,yy,zz,xy,xz,yz
  real(wp) :: ra(3),rb(3)

  de = apot
  deb = bpot

  v = 0.d0
  vir = 0.0d0
  ! Loop over molecules

  do j = 1,na,3
    ra = r(:,j+1) - r(:,j)
    dx1 = ra(1)
    dy1 = ra(2)
    dz1 = ra(3)
    dr1sq = dx1*dx1 + dy1*dy1 + dz1*dz1
    dr1 = sqrt(dr1sq)
    dr1a = dr1
    dr1dx1 = dx1/dr1
    dr1dy1 = dy1/dr1
    dr1dz1 = dz1/dr1
    dr1 = dr1-reoh
    drasq=dr1*dr1

    rb = r(:,j+2) - r(:,j)
    dx2 = rb(1)
    dy2 = rb(2)
    dz2 = rb(3)
    dr2sq = dx2*dx2 + dy2*dy2 + dz2*dz2
    dr2 = sqrt(dr2sq)
    dr2a = dr2
    dr2dx2 = dx2/dr2
    dr2dy2 = dy2/dr2
    dr2dz2 = dz2/dr2
    dr2 = dr2-reoh
    drbsq=dr2*dr2
    
    dx3 = r(1,j+2)-r(1,j+1)
    dy3 = r(2,j+2)-r(2,j+1)
    dz3 = r(3,j+2)-r(3,j+1)
    dr3sq = dx3*dx3 + dy3*dy3 + dz3*dz3
    dr3 = sqrt(dr3sq)
    dr3a = dr3
    dr3dx3 = dx3/dr3
    dr3dy3 = dy3/dr3
    dr3dz3 = dz3/dr3
    
    u = (dr1sq + dr2sq - dr3sq)
    vv = (2.d0 * dr1a * dr2a)         
    v2 = 1.d0/(vv * vv)
    arg = u / vv
    darg = -1.d0/sqrt(1.d0 - arg*arg)
    ang = acos( arg )
    !cp = dot_product(cross_product(ra,rb)
    dang = (ang - theta)
    ! write(*,*) ang * 180./acos(-1.), dang*180./acos(-1.)
    if(abs(dang)<=TINY(dang)) then
      dthetadr1=0._wp
      dthetadr2=0._wp
      dthetadr3=0._wp
    else
      uprime = 2.d0 * dr1a
      vprime = 2.d0 * dr2a
      grad = (uprime*vv - vprime*u) * v2
      dthetadr1= darg * grad
      uprime = 2.d0 * dr2a
      vprime = 2.d0 * dr1a
      grad = (uprime*vv - vprime*u) * v2
      dthetadr2= darg * grad
      uprime = -2.d0*dr3a
      grad = (uprime*vv) * v2
      dthetadr3= darg * grad
    endif

  

    v = v+de*drasq
    v = v+de*drbsq
    v = v+deb*dang**2

    a1 = 2.d0*de*dr1
    a2 = 2.d0*de*dr2
    a3 = 2.d0*deb*dang

    dvdr1 = a1+a3*dthetadr1
    dvdr2 = a2+a3*dthetadr2
    dvdr3 = a3*dthetadr3
    dvdx1 = dvdr1*dr1dx1
    dvdy1 = dvdr1*dr1dy1
    dvdz1 = dvdr1*dr1dz1 
    dvdx2 = dvdr2*dr2dx2
    dvdy2 = dvdr2*dr2dy2
    dvdz2 = dvdr2*dr2dz2
    dvdx3 = dvdr3*dr3dx3
    dvdy3 = dvdr3*dr3dy3
    dvdz3 = dvdr3*dr3dz3
    dvdr(1,j) = dvdr(1,j) - dvdx1 - dvdx2
    dvdr(2,j) = dvdr(2,j) - dvdy1 - dvdy2
    dvdr(3,j) = dvdr(3,j) - dvdz1 - dvdz2
    dvdr(1,j+1) = dvdr(1,j+1) + dvdx1 - dvdx3
    dvdr(2,j+1) = dvdr(2,j+1) + dvdy1 - dvdy3
    dvdr(3,j+1) = dvdr(3,j+1) + dvdz1 - dvdz3
    dvdr(1,j+2) = dvdr(1,j+2) + dvdx2 + dvdx3
    dvdr(2,j+2) = dvdr(2,j+2) + dvdy2 + dvdy3
    dvdr(3,j+2) = dvdr(3,j+2) + dvdz2 + dvdz3

    xx = dx1*dvdx1 + dx2*dvdx2 + dx3*dvdx3
    xy = dx1*dvdy1 + dx2*dvdy2 + dx3*dvdy3
    xz = dx1*dvdz1 + dx2*dvdz2 + dx3*dvdz3
    yy = dy1*dvdy1 + dy2*dvdy2 + dy3*dvdy3
    yz = dy1*dvdz1 + dy2*dvdz2 + dy3*dvdz3
    zz = dz1*dvdz1 + dz2*dvdz2 + dz3*dvdz3
    vir(1,1) = vir(1,1) + xx
    vir(1,2) = vir(1,2) + xy
    vir(1,3) = vir(1,3) + xz
    vir(2,1) = vir(2,1) + xy
    vir(2,2) = vir(2,2) + yy
    vir(2,3) = vir(2,3) + yz
    vir(3,1) = vir(3,1) + xz
    vir(3,2) = vir(3,2) + yz
    vir(3,3) = vir(3,3) + zz
  enddo
  
  return 
end subroutine intra_harm_harm

Subroutine intra_morse_harm(r,dvdr,na,v,vir,theta,reoh,apot,bpot,alp)
  USE matrix_operations, only: cross_product
  implicit none
  integer :: na,j
  real(wp), intent(in) :: apot,bpot, theta,reoh,alp
  real(wp) :: r(3,na), dvdr(3,na), vir(3,3)
  real(wp) :: dr1,dr2,dr3
  real(wp) :: dx1,dy1,dz1,v,dx2,dy2,dz2,dr1sq,dr2sq,dr3dx3
  real(wp) :: dx3,dy3,dz3,dr3sq,dr1dx1,dr1dy1,dr1dz1
  real(wp) :: dr2dx2,dr2dy2,dr2dz2,dr3dy3,dr3dz3,u,vv,v2,arg,ang
  real(wp) :: dang,uprime,vprime,grad
  real(wp) :: dvdx1,dvdx2,dvdx3,dvdy1,dvdy2,dvdy3
  real(wp) :: dvdz1,dvdz2,dvdz3,dvdr1,dvdr2,dvdr3
  real(wp) :: dthetadr1,dthetadr2,dthetadr3
  real(wp) :: darg,dr1a,dr2a,dr3a
  real(wp) :: de,drasq,drbsq,cp,alp2,alp3,alp4
  real(wp) :: f1,f2,deb,a1,a2,a3,xx,yy,zz,xy,xz,yz
  real(wp) :: ra(3),rb(3)

  de = apot
  deb = bpot

  alp2  = alp*alp
  alp3  = alp*alp2
  alp4  = alp3*alp
  f1 = 7.d0 / 12.d0
  f2 = 7.d0 / 3.d0

  v = 0.d0
  vir = 0.0d0
  ! Loop over molecules

  do j = 1,na,3
    ra = r(:,j+1) - r(:,j)
    dx1 = ra(1)
    dy1 = ra(2)
    dz1 = ra(3)
    dr1sq = dx1*dx1 + dy1*dy1 + dz1*dz1
    dr1 = sqrt(dr1sq)
    dr1a = dr1
    dr1dx1 = dx1/dr1
    dr1dy1 = dy1/dr1
    dr1dz1 = dz1/dr1
    dr1 = dr1-reoh
    drasq=dr1*dr1

    rb = r(:,j+2) - r(:,j)
    dx2 = rb(1)
    dy2 = rb(2)
    dz2 = rb(3)
    dr2sq = dx2*dx2 + dy2*dy2 + dz2*dz2
    dr2 = sqrt(dr2sq)
    dr2a = dr2
    dr2dx2 = dx2/dr2
    dr2dy2 = dy2/dr2
    dr2dz2 = dz2/dr2
    dr2 = dr2-reoh
    drbsq=dr2*dr2
    
    dx3 = r(1,j+2)-r(1,j+1)
    dy3 = r(2,j+2)-r(2,j+1)
    dz3 = r(3,j+2)-r(3,j+1)
    dr3sq = dx3*dx3 + dy3*dy3 + dz3*dz3
    dr3 = sqrt(dr3sq)
    dr3a = dr3
    dr3dx3 = dx3/dr3
    dr3dy3 = dy3/dr3
    dr3dz3 = dz3/dr3
    
    u = (dr1sq + dr2sq - dr3sq)
    vv = (2.d0 * dr1a * dr2a)         
    v2 = 1.d0/(vv * vv)
    arg = u / vv
    darg = -1.d0/sqrt(1.d0 - arg*arg)
    ang = acos( arg )
    !cp = dot_product(cross_product(ra,rb)
    dang = (ang - theta)
    ! write(*,*) ang * 180./acos(-1.), dang*180./acos(-1.)
    if(abs(dang)<=TINY(dang)) then
      dthetadr1=0._wp
      dthetadr2=0._wp
      dthetadr3=0._wp
    else
      uprime = 2.d0 * dr1a
      vprime = 2.d0 * dr2a
      grad = (uprime*vv - vprime*u) * v2
      dthetadr1= darg * grad
      uprime = 2.d0 * dr2a
      vprime = 2.d0 * dr1a
      grad = (uprime*vv - vprime*u) * v2
      dthetadr2= darg * grad
      uprime = -2.d0*dr3a
      grad = (uprime*vv) * v2
      dthetadr3= darg * grad
    endif

    v = v+de*(alp2*drasq-alp3*dr1*drasq+f1*alp4*drasq*drasq)
    v = v+de*(alp2*drbsq-alp3*dr2*drbsq+f1*alp4*drbsq*drbsq)
    v = v+deb*dang**2

    a1 = de*(2.d0*alp2*dr1-3.d0*alp3*drasq+f2*alp4*dr1*drasq)
    a2 = de*(2.d0*alp2*dr2-3.d0*alp3*drbsq+f2*alp4*dr2*drbsq)
    a3 = 2.d0*deb*dang  

    dvdr1 = a1+a3*dthetadr1
    dvdr2 = a2+a3*dthetadr2
    dvdr3 = a3*dthetadr3
    dvdx1 = dvdr1*dr1dx1
    dvdy1 = dvdr1*dr1dy1
    dvdz1 = dvdr1*dr1dz1 
    dvdx2 = dvdr2*dr2dx2
    dvdy2 = dvdr2*dr2dy2
    dvdz2 = dvdr2*dr2dz2
    dvdx3 = dvdr3*dr3dx3
    dvdy3 = dvdr3*dr3dy3
    dvdz3 = dvdr3*dr3dz3
    dvdr(1,j) = dvdr(1,j) - dvdx1 - dvdx2
    dvdr(2,j) = dvdr(2,j) - dvdy1 - dvdy2
    dvdr(3,j) = dvdr(3,j) - dvdz1 - dvdz2
    dvdr(1,j+1) = dvdr(1,j+1) + dvdx1 - dvdx3
    dvdr(2,j+1) = dvdr(2,j+1) + dvdy1 - dvdy3
    dvdr(3,j+1) = dvdr(3,j+1) + dvdz1 - dvdz3
    dvdr(1,j+2) = dvdr(1,j+2) + dvdx2 + dvdx3
    dvdr(2,j+2) = dvdr(2,j+2) + dvdy2 + dvdy3
    dvdr(3,j+2) = dvdr(3,j+2) + dvdz2 + dvdz3

    xx = dx1*dvdx1 + dx2*dvdx2 + dx3*dvdx3
    xy = dx1*dvdy1 + dx2*dvdy2 + dx3*dvdy3
    xz = dx1*dvdz1 + dx2*dvdz2 + dx3*dvdz3
    yy = dy1*dvdy1 + dy2*dvdy2 + dy3*dvdy3
    yz = dy1*dvdz1 + dy2*dvdz2 + dy3*dvdz3
    zz = dz1*dvdz1 + dz2*dvdz2 + dz3*dvdz3
    vir(1,1) = vir(1,1) + xx
    vir(1,2) = vir(1,2) + xy
    vir(1,3) = vir(1,3) + xz
    vir(2,1) = vir(2,1) + xy
    vir(2,2) = vir(2,2) + yy
    vir(2,3) = vir(2,3) + yz
    vir(3,1) = vir(3,1) + xz
    vir(3,2) = vir(3,2) + yz
    vir(3,3) = vir(3,3) + zz
  enddo
  
  return 
end subroutine intra_morse_harm


end module ff_co2