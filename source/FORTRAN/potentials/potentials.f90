module potentials
	USE kinds
	USE nested_dictionaries
	USE basic_types
	USE string_operations
	USE socket_potential
	USE model_potentials
	USE qtip4pf_potential
	USE ff_co2
	USE ff_cso2h3
	USE file_handler, only: output
	USE diatomic
	IMPLICIT NONE

	CHARACTER(:), allocatable, SAVE :: pot_name
	INTEGER, SAVE :: n_threads

	PRIVATE
	PUBLIC :: initialize_potential, finalize_potential
	
contains

	subroutine initialize_potential(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), INTENT(IN) :: param_library
		INTEGER :: n_dim, n_atoms,i
		CHARACTER(:), allocatable :: xyz_file
		LOGICAL :: file_exists
		
		pot_name = param_library%get("POTENTIAL/pot_name")
		!! @input_file POTENTIAL/pot_name (potentials)
		pot_name=to_upper_case(trim(pot_name))

		n_dim=param_library%get("parameters/n_dim",default=n_dim_default)
		!! @input_file PARAMETERS/n_dim (potentials, default=n_dim_default)
		if(n_dim<=0) STOP "Error: 'n_dim' is not properly defined!"
		n_atoms=param_library%get("PARAMETERS/n_atoms")
		!! @input_file PARAMETERS/n_atoms (potentials)
		if(n_atoms<=0) STOP "Error: 'n_atoms' is not properly defined!"

		SELECT CASE(pot_name)
    CASE("SOCKET")
			call initialize_socket(param_library,n_dim,n_atoms)			
		CASE("QTIP4PF")
			write_pot_components =param_library%get("POTENTIAL/write_pot_components",default=.FALSE.) 
			if(write_pot_components) then
				n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
				write_stride=param_library%get("POTENTIAL/write_stride",default=1) 
				allocate(write_now(n_threads) &
								,unit_pot(n_threads) &
								,write_count(n_threads) &
								,vlj_mean(n_threads) &
								,vewald_mean(n_threads) &
								,vstretch_mean(n_threads) &
								,vbend_mean(n_threads) &
				)
				vlj_mean=0; vewald_mean=0; vstretch_mean=0; vbend_mean=0
				write_count=0
				write_now=.FALSE.
				DO i=1,n_threads
					open(newunit=unit_pot(i),file=output%working_directory//"/pot_components_"//int_to_str(i)//".out")
					if(n_atoms>3) then					
						write(unit_pot(i),*) "#V_lj   V_coulomb   V_stretching    V_bending"
					else
						write(unit_pot(i),*) "#V_stretching    V_bending"
					endif
				ENDDO
			endif
			get_pot_info => get_qtip4pf_pot_info
			Pot => Pot_qtip4pf
			dPot => dPot_qtip4pf
			compute_hessian => hessian_qtip4pf
		CASE("FFCO2")
			get_pot_info => get_ffco2_pot_info
			Pot => Pot_ffco2
			dPot => dPot_ffco2
			compute_hessian => hessian_ffco2
		CASE("CSO2H3")
			get_pot_info => get_cso2h3_pot_info
			Pot => Pot_cso2h3
			dPot => dPot_cso2h3
			compute_hessian => hessian_cso2h3
			call initialize_cso2h3(param_library)
		CASE("DIATOMIC")
			get_pot_info => get_diatomic_pot_info
			Pot => Pot_diatomic
			dPot => dPot_diatomic
			compute_hessian => hessian_diatomic
			call initialize_diatomic(param_library)
		CASE DEFAULT
			call initialize_model_potentials(param_library,n_dim,n_atoms,pot_name)
		END SELECT

	end subroutine initialize_potential


	subroutine finalize_potential()
		IMPLICIT NONE
		INTEGER :: i

		SELECT CASE(pot_name)
    CASE("SOCKET")
			CALL socket_shutdown()
		CASE("QTIP4PF")
			if(write_pot_components) then
				DO i=1,n_threads
			 		close(unit_pot(i))
				ENDDO
			endif
		END SELECT

	end subroutine finalize_potential
    
end module potentials
