MODULE lgclwild_integrators
    USE lgclwild_types
		USE random
    USE thermostats, only : apply_thermostat
    USE basic_types
		USE atomic_units
		USE matrix_operations, ONLY: sym_mat_diag
    IMPLICIT NONE

	PRIVATE :: apply_A,apply_B, lgclwild_compute_forces_full

	PROCEDURE(lgclwild_sub), pointer :: lgclwild_compute_forces => null()

CONTAINS

    ! INTEGRATORS

	subroutine lgclwild_BAOAB(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), intent(inout) :: system
		CLASS(LGCLWILD_PARAM), intent(inout) :: parameters


	!	CALL apply_B(system,parameters,parameters%dt/2._wp)
		CALL apply_A(system,parameters,parameters%dt/2._wp)

		!CALL apply_thermostat(system,parameters)
		CALL apply_O(system,parameters,parameters%dt)

	!	call lgclwild_compute_forces(system,parameters)
    call lgclwild_update_P_mod_from_P(system,parameters)
    
		CALL apply_A(system,parameters,parameters%dt/2._wp)

		call lgclwild_compute_forces(system,parameters)
		CALL apply_B(system,parameters,parameters%dt)
    call lgclwild_update_P_mod_from_P(system,parameters)

	end subroutine lgclwild_BAOAB


	subroutine apply_A(system,parameters,tau)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), intent(inout) :: system
		CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + tau*system%P_mod(:,i)/system%mass(:)
		enddo

	end subroutine apply_A

	subroutine apply_B(system,parameters,tau)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), intent(inout) :: system
		CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau

		system%P=system%P + tau*system%Forces
		
	end subroutine apply_B

	subroutine apply_O(system,parameters,tau)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), intent(inout) :: system
		CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: gamma_exp,sigma,R(system%n_atoms)
		INTEGER :: i

		gamma_exp=exp(-parameters%gamma0*parameters%dt)
		sigma=SQRT( parameters%temperature &
										*( 1._wp-gamma_exp*gamma_exp ) )

		do i=1,system%n_dim		
			call randGaussN(R)
			system%P(:,i)=gamma_exp*system%P(:,i) &
							+ sigma*R(:)
		enddo
		
	end subroutine apply_O
!---------------------------------------------
! FORCES CALCULATORS

	subroutine lgclwild_initialize_forces(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters

		! ASSIGN FORCES CALCULATOR	
		lgclwild_compute_forces => lgclwild_compute_forces_full

		if(.not. allocated(system%Forces)) &
			allocate(system%Forces(system%n_atoms,system%n_dim))
		
		call lgclwild_compute_forces(system,parameters)

	end subroutine lgclwild_initialize_forces

	subroutine lgclwild_compute_forces_full(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), intent(inout) :: system
		CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
		REAL(wp) :: u,omega2,Q,sqrtQ, eigtranspose(system%n_dof,system%n_dof)
		INTEGER :: i, INFO,c,j,k

		call get_pot_info(system%X,system%E_pot,system%Forces &
												,hessian=system%k2)

		system%F_cl=RESHAPE(system%Forces,(/system%n_dof/))		

    !COMPUTE MASS WEIGHTED HESSIAN
		c=0
		DO j=1,system%n_dim; do i=1,system%n_atoms
				c=c+1
				system%F_cl(c)=system%F_cl(c)/sqrt(system%mass(i))
				DO k=1,system%n_dim
						system%k2((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
								=system%k2((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
										/sqrt(system%mass(i)*system%mass(:))
				ENDDO
		ENDDO ; ENDDO

		!DIAGONALIZE HESSIAN
    call sym_mat_diag(system%k2,system%k2EigVals,system%k2EigMat,INFO)            
    IF(INFO/=0) STOP "Error: could not diagonalize mass weighted hessian !"

		!COMPUTE QUANTUM CORRECTION TO KINETIC ENERGY IN NORMAL MODES
		system%sqrtk2inv=0
		system%sqrtk2=0
		system%k2=0
		system%sqrtk2inv_det=1._wp
		system%k2_det=1._wp
		DO i=1,system%n_dof
			omega2=system%k2EigVals(i)
			if(omega2>=0) then 
				! HARMONIC WHEN REAL FREQUENCY
				u=0.5_wp*parameters%beta*sqrt(omega2)
				Q=tanh(u)/u
			else 
				! LIU & MILLER ANSATZ WHEN IMAGINARY FREQUENCY
				u=0.5_wp*parameters%beta*sqrt(-omega2)
				Q=u/tanh(u) 
			endif
			system%k2(i,i)=Q
			system%k2_det=system%k2_det*Q
			sqrtQ=sqrt(Q)
			system%sqrtk2(i,i)=sqrtQ			
			system%sqrtk2inv(i,i)=1._wp/sqrtQ
			system%sqrtk2inv_det=system%sqrtk2inv_det/sqrtQ
		ENDDO

		! TRANSFORM BACK
		eigtranspose=transpose(system%k2EigMat)
		system%k2 = matmul(system%k2EigMat,matmul(system%k2,eigtranspose))       
		system%sqrtk2inv = matmul(system%k2EigMat,matmul(system%sqrtk2inv,eigtranspose))
		system%sqrtk2 = matmul(system%k2EigMat,matmul(system%sqrtk2,eigtranspose))

		! MODIFIED FORCE IS sqrt(k2)*F_classical
		system%Forces=RESHAPE( matmul(system%sqrtk2,system%F_cl) &
													,(/ system%n_atoms,system%n_dim/) )

	end subroutine lgclwild_compute_forces_full

END MODULE lgclwild_integrators    
