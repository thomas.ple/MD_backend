MODULE lgclwild
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE random
	USE lgclwild_types
	USE lgclwild_integrators
	USE classical_md, only: classical_MD_get_parameters
	USE thermostats, only : initialize_thermostat, finalize_thermostat, thermostat_end_thermalization
	IMPLICIT NONE	

	TYPE(LGCLWILD_SYS), save, target :: lgclwild_system
	TYPE(LGCLWILD_PARAM), save, target :: lgclwild_parameters

	PROCEDURE(lgclwild_sub), pointer :: lgclwild_integrator

	PRIVATE ::lgclwild_system, lgclwild_parameters

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine lgclwild_loop()
		IMPLICIT NONE
		INTEGER :: i,n_dof,n_dim,n_atoms,u
		REAL :: time_start, time_finish


		if(lgclwild_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",lgclwild_parameters%n_steps_therm," steps &
						&of thermalization"
			call cpu_time(time_start)
			DO i=1,lgclwild_parameters%n_steps_therm
				if(lgclwild_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				call lgclwild_integrator(lgclwild_system,lgclwild_parameters)
				call lgclwild_print_system_info(lgclwild_system,lgclwild_parameters,i)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			call lgclwild_write_restart_file(lgclwild_system,lgclwild_parameters,0)
		endif

		write(*,*)
		write(*,*) "Starting ",lgclwild_parameters%n_steps," steps &
							&of LGclWiLD"
		call cpu_time(time_start)
		DO i=1,lgclwild_parameters%n_steps

			if(lgclwild_parameters%check_exit_file()) then
				call lgclwild_write_restart_file(lgclwild_system,lgclwild_parameters &
						,lgclwild_parameters%n_steps_prev+i)
				write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
				RETURN
			endif

			lgclwild_system%time=lgclwild_system%time &
								+lgclwild_parameters%dt

			
			call lgclwild_integrator(lgclwild_system,lgclwild_parameters)
			call lgclwild_print_system_info(lgclwild_system,lgclwild_parameters,i)
			

			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(lgclwild_parameters%write_restart) then
				if(mod(i,lgclwild_parameters%restart_stride)==0) &
					call lgclwild_write_restart_file(lgclwild_system,lgclwild_parameters &
						,lgclwild_parameters%n_steps_prev+i)
			endif

		ENDDO
		call cpu_time(time_finish)
		write(*,*) "Simulation done in ",time_finish-time_start,"s."

		call lgclwild_write_restart_file(lgclwild_system,lgclwild_parameters &
					,lgclwild_parameters%n_steps_prev+lgclwild_parameters%n_steps)	
		

	end subroutine lgclwild_loop
!-----------------------------------------------------------------

	subroutine initialize_lgclwild(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart,qtb_thermalization

		load_restart=.FALSE.
		lgclwild_parameters%engine="lgclwild"

		call lgclwild_initialize_restart_file(lgclwild_system,lgclwild_parameters,param_library)

		!ASSOCIATE classicla_MD_loop
		simulation_loop => lgclwild_loop

		!GET ALGORITHM
		lgclwild_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (lgclwild, default="nvt")
		lgclwild_parameters%algorithm=to_lower_case(lgclwild_parameters%algorithm)

		call lgclwild_get_parameters(lgclwild_system,lgclwild_parameters,param_library)

		call lgclwild_get_integrator_parameters(lgclwild_system,lgclwild_parameters,param_library)	

		call lgclwild_initialize_momenta(lgclwild_system,lgclwild_parameters)	
			
		! LOAD RESTART FILE IS NEEDED
		lgclwild_parameters%n_steps_prev=0
		load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
		if(load_restart) then
			call lgclwild_load_restart_file(lgclwild_system,lgclwild_parameters)
			lgclwild_parameters%n_steps_therm=0
		endif

		call lgclwild_initialize_forces(lgclwild_system,lgclwild_parameters)
		call lgclwild_update_P_mod_from_P(lgclwild_system,lgclwild_parameters)

		call lgclwild_initialize_output(lgclwild_system,lgclwild_parameters,param_library)

		
		! if(lgclwild_parameters%compute_fdt) &
		! 	call spectrum_analyser%initialize(lgclwild_system,lgclwild_parameters,param_library)	
		

	end subroutine initialize_lgclwild

	subroutine finalize_lgclwild()
		IMPLICIT NONE

		call lgclwild_deallocate(lgclwild_system,lgclwild_parameters)

	end subroutine finalize_lgclwild

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine lgclwild_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dof,i,j,n

		call classical_MD_get_parameters(system,parameters,param_library)

		n_dof=system%n_dof
    allocate(system%F_cl(n_dof) &
            ,system%k2(n_dof,n_dof) &
            ,system%sqrtk2inv(n_dof,n_dof) &
						,system%sqrtk2(n_dof,n_dof) &
						,system%k2EigVals(n_dof) &
						,system%k2EigMat(n_dof,n_dof) &
    )

	end subroutine lgclwild_get_parameters

	subroutine lgclwild_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i

		allocate(system%P(system%n_atoms,system%n_dim), &
				system%P_mod(system%n_atoms,system%n_dim))		

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt")

			!classical initialization for the moment
			do i=1,system%n_dim
				call randGaussN(system%P(:,i))
				system%P(:,i)=system%P(:,i)*SQRT(parameters%temperature)
			enddo
      system%P_mod=system%P

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine lgclwild_initialize_momenta


!-----------------------------------------------------------------

	subroutine lgclwild_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: i
		CHARACTER(:), ALLOCATABLE :: default_integrator

		default_integrator = "baoab"

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default=default_integrator)
		!! @input_file PARAMETERS/integrator (lgclwild, default="baoab")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt")

      !call initialize_thermostat(system,parameters,param_library)
			parameters%gamma0=param_library%get("THERMOSTAT/damping")
			!! @input_file THERMOSTAT/damping (lgclwild)

			SELECT CASE(parameters%integrator_type)
			CASE("baoab")
				lgclwild_integrator => lgclwild_BAOAB
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT		

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine lgclwild_get_integrator_parameters

!-----------------------------------------------------------------

	subroutine lgclwild_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name
		INTEGER :: i_file, i,k
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("position","positions")
			!! @input_file OUTPUT/position (lgclwild)
				if(system%n_dim==1) then
					default_name="X.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X(:,1),index=i_file,name="positions")
				else
					default_name="position.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,name="positions")
				endif

			CASE("momentum","momenta")		
			!! @input_file OUTPUT/momentum (lgclwild)	
				description="trajectory of momenta"
				if(system%n_dim==1) then
					default_name="P.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					
					call output%add_array_to_file(system%P_mod(:,1),index=i_file,name="momenta")
				else
					default_name="momentum.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%P_mod,system%element_symbols,index=i_file,name="momenta")
				endif
			
			CASE("k2_det")
      !! @input_file OUTPUT/k2_det (lgclwild)		
				description="trajectory of k2 determinant"
				default_name="k2_det.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%k2_det,index=i_file,name="k2_det")
				call output%add_column_to_file(system%sqrtk2inv_det,index=i_file,name="sqrtk2inv_det")
			
			CASE("k2")		
			!! @input_file OUTPUT/k2 (lgclwild)		
				description="trajectory of k2 (only the lower triangular part in col-major)"
				default_name="k2.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%k2(k:system%n_dof,k),index=i_file,name="k2")
				ENDDO
			
			CASE("k2_eigvals")		
			!! @input_file OUTPUT/k2_eigvals (lgclwild)		
				description="trajectory of k2 eigenvalues"
				default_name="k2_eigvals.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%k2EigVals(:),index=i_file,name="k2_eigvals")

			CASE("force_lgclwild")	
			!! @input_file OUTPUT/force_lgclwild (lgclwild)	
				description="trajectory of LGclWiLD force "
				default_name="F_LGclWiLD.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dim
					call output%add_array_to_file(system%Forces(:,k),index=i_file,name="F_LGclWiLD")
				ENDDO
			
			CASE("force_cl")	
			!! @input_file OUTPUT/force_cl (lgclwild)			
				description="trajectory of classical force"
				default_name="F_classical.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%F_cl,index=i_file,name="F_cl")			
			
			CASE("print_system_info")
			!! @input_file OUTPUT/print_system_info (lgclwild)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
				parameters%keep_potential=.TRUE.
				parameters%print_temperature_stride=dict%get("stride",default=100)

			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO


		IF(parameters%compute_E_kin) THEN
			ALLOCATE(system%E_kin_mean(system%n_dof) &
							,system%E_kin(system%n_dof))
			system%E_kin_mean=0
			system%E_kin=0
			system%temp_estimated=0
			system%temp_k2=0
			system%E_kin_k2=0
			system%E_kin_k2_mean=0
			system%pressure_tensor_mean=0
			system%E_pot_mean=0
		ENDIF



	end subroutine lgclwild_initialize_output

END MODULE lgclwild