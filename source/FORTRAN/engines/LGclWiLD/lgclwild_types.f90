MODULE lgclwild_types
	USE kinds
	USE classical_md_types
	USE nested_dictionaries
	USE file_handler, only: output
	USE atomic_units
	USE string_operations
  IMPLICIT NONE

  TYPE, EXTENDS(CLASS_MD_SYS) :: LGCLWILD_SYS

		REAL(wp), allocatable :: P_mod(:,:)

		REAL(wp) :: k2_det,sqrtk2inv_det

		REAL(wp), ALLOCATABLE :: F_cl(:)
		REAL(wp), ALLOCATABLE :: k2(:,:)
		REAL(wp), ALLOCATABLE :: sqrtk2inv(:,:)
    REAL(wp), ALLOCATABLE :: sqrtk2(:,:)		

		REAL(wp), ALLOCATABLE :: k2EigMat(:,:)
		REAL(wp), ALLOCATABLE :: k2EigVals(:)

		REAL(wp) :: temp_k2, E_kin_k2, E_kin_k2_mean

	END TYPE

	TYPE, EXTENDS(CLASS_MD_PARAM) :: LGCLWILD_PARAM
		REAL(wp) :: gamma0
	END TYPE	

	ABSTRACT INTERFACE
		subroutine lgclwild_sub(system,parameters)
			import :: LGCLWILD_SYS, LGCLWILD_PARAM
			IMPLICIT NONE
			CLASS(LGCLWILD_SYS), intent(inout) :: system
			CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
		end subroutine lgclwild_sub

		subroutine lgclwild_sub_int(system,parameters,n)
			import :: LGCLWILD_SYS, LGCLWILD_PARAM
			IMPLICIT NONE
			CLASS(LGCLWILD_SYS), intent(inout) :: system
			CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
			INTEGER, INTENT(in) :: n
		end subroutine lgclwild_sub_int
	END INTERFACE

CONTAINS

	subroutine lgclwild_update_P_mod_from_P(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), intent(inout) :: parameters
		INTEGER :: i,j
    REAL(wp), ALLOCATABLE :: P_packed(:)

    allocate(P_packed(system%n_dof))
		P_packed=RESHAPE(system%P,(/system%n_dof/))
		system%P_mod=RESHAPE(matmul(system%sqrtk2inv,P_packed) &
													,(/system%n_atoms,system%n_dim/))
		DO j=1,system%n_dim
			system%P_mod(:,j)=system%P_mod(:,j)*sqrt(system%mass(:))
		ENDDO
    deallocate(P_packed)
		
	end subroutine lgclwild_update_P_mod_from_P

	subroutine lgclwild_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters

		call classical_MD_deallocate(system,parameters)
    if(allocated(system%P_mod)) deallocate(system%P_mod)
    if(allocated(system%k2)) deallocate(system%k2)
    if(allocated(system%sqrtk2)) deallocate(system%sqrtk2)
    if(allocated(system%sqrtk2inv)) deallocate(system%sqrtk2inv)
		

	end subroutine lgclwild_deallocate

	! subroutine lgclwild_compute_temp_estimated(system,parameters)
	! 	IMPLICIT NONE
	! 	CLASS(LGCLWILD_SYS), INTENT(inout) :: system
	! 	CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters

	! 	system%temp_estimated=parameters%temperature*dot_product(system%P_packed &
	! 			,matmul(system%k2,system%P_packed))/real(system%n_dof,wp)
		
	! 	system%temp_natural=(system%P_packed &
	! 			,matmul(system%k2,system%P_packed))/real(system%n_dof,wp)

	! end subroutine lgclwild_compute_temp_estimated

	subroutine lgclwild_compute_kinetic_energy(system,parameters)
		IMPLICIT NONE
    CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j,n

		n=0
		DO j=1,system%n_dim ; do i=1,system%n_atoms
			n=n+1
			system%E_kin(n)=0.5*system%P(i,j)**2
		ENDDO ; ENDDO

		system%E_kin_k2=0
		DO j=1,system%n_dim ; do i=1,system%n_atoms
			system%E_kin_k2=system%E_kin_k2+system%P_mod(i,j)**2/(2._wp*system%mass(i))
		ENDDO ; ENDDO

	end subroutine lgclwild_compute_kinetic_energy

	subroutine lgclwild_update_mean_energies(system,parameters,n)
		IMPLICIT NONE
    CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n

		system%E_kin_mean=system%E_kin_mean+(system%E_kin-system%E_kin_mean)/real(n,wp)
		system%E_kin_k2_mean=system%E_kin_k2_mean+(system%E_kin_k2-system%E_kin_k2_mean)/real(n,wp)
		system%temp_estimated = SUM(system%E_kin_mean)*2._wp/system%n_dof
		system%temp_k2 = system%E_kin_k2_mean*2._wp/system%n_dof
		system%E_pot_mean = system%E_pot_mean +(system%E_pot - system%E_pot_mean)/real(n,wp)

	end subroutine lgclwild_update_mean_energies

	subroutine lgclwild_print_system_info(system,parameters,i)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: i		

		if(parameters%write_mean_energy_info) then
				call lgclwild_compute_kinetic_energy(system,parameters)

				call lgclwild_update_mean_energies(system,parameters &
								,parameters%counter_mean_energy_info)
				parameters%counter_mean_energy_info= &
						parameters%counter_mean_energy_info+1

				if(parameters%print_temperature_stride>0) then
					if(parameters%counter_mean_energy_info &
							>parameters%print_temperature_stride) then
						write(*,*)
						write(*,*) "----------------------------------------------"
						write(*,*) "STEP "//int_to_str(i)
						write(*,*) "SYSTEM INFO (last "//int_to_str( &
													parameters%print_temperature_stride)//" steps):"
						write(*,*) "  temperature_natural: " &
												,system%temp_estimated*kelvin, "K"
						write(*,*) "  temperature_k2: " &
												,system%temp_k2*kelvin, "K"
						write(*,*) "  kinetic_energy_natural: " &
												,SUM(system%E_kin_mean)*kcalpermol, "kCal/mol"
						write(*,*) "  kinetic_energy_k2: " &
												,system%E_kin_k2_mean*kcalpermol, "kCal/mol"
						write(*,*) "  potential_energy: " &
												,system%E_pot_mean*kcalpermol, "kCal/mol"
						write(*,*)

						parameters%counter_mean_energy_info=1
						system%E_kin_k2_mean=0
						system%E_kin_mean=0
						system%temp_k2=0
						system%temp_estimated=0
						system%E_pot_mean=0
					endif					
				endif

			endif
	end subroutine lgclwild_print_system_info

!----------------------------------------------------------
! RESTART FILE

	subroutine lgclwild_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_initialize_restart_file(system,parameters,param_library)
		
	end subroutine lgclwild_initialize_restart_file

	subroutine lgclwild_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(in) :: system
		CLASS(LGCLWILD_PARAM), INTENT(in) :: parameters
		INTEGER, intent(in) :: n_steps
		INTEGER :: u

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			WRITE(u,*) parameters%engine
			WRITE(u,*) system%time
			WRITE(u,*) n_steps
			WRITE(u,*) system%X
			WRITE(u,*) system%P		
		close(u)

		write(*,*) "I/O : save config OK."

	end subroutine lgclwild_write_restart_file

	subroutine lgclwild_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(LGCLWILD_SYS), INTENT(inout) :: system
		CLASS(LGCLWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u
		CHARACTER(100) :: engine

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			READ(u,*) engine
			if(engine/=parameters%engine) then
				write(0,*) "Error: tried to restart engine '",parameters%engine &
							,"' with a file from '",engine,"' !"
				STOP "Execution stopped"
			endif
			READ(u,*) system%time
			READ(u,*) parameters%n_steps_prev
			READ(u,*) system%X
			READ(u,*) system%P		
		close(u)

		write(*,*) "I/O : configuration retrieved successfully."

	end subroutine lgclwild_load_restart_file

END MODULE lgclwild_types