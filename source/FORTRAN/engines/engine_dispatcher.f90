MODULE engine_dispatcher
	USE kinds
	USE nested_dictionaries
	USE basic_types
	USE string_operations
	USE classical_md, ONLY: initialize_classical_MD, finalize_classical_MD
	USE classical_mc, ONLY: initialize_classical_MC, finalize_classical_MC
	USE ring_polymer_md, ONLY: initialize_RPMD, finalize_RPMD
	USE wigner_md, ONLY: initialize_wigner_MD, finalize_wigner_MD
	USE wigner_bayes, ONLY: initialize_wb_MD, finalize_wb_MD
	USE centroid_md, ONLY: initialize_cmd, finalize_cmd
	USE schrodinger, ONLY: initialize_schrodinger, finalize_schrodinger
	USE ivr, ONLY : initialize_IVR,finalize_IVR
	USE lgclwild, ONLY: initialize_lgclwild, finalize_lgclwild
	USE gwild, ONLY: initialize_gwild, finalize_gwild
	USE analytic_wild_1d, ONLY: initialize_awild
	USE gxx_module, ONLY: initialize_gxx
	USE autok2, ONLY: initialize_autok2
	USE spectrum_deconvolution, ONLY: initialize_deconvolution, finalize_deconvolution
	IMPLICIT NONE

	CHARACTER(:), ALLOCATABLE, SAVE :: engine

	PRIVATE :: engine
CONTAINS

	subroutine initialize_engine(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT) :: param_library

		engine = param_library%get("PARAMETERS/engine")
		!! @input_file PARAMETERS/engine (main)
		engine=to_lower_case(engine)
		write(*,*) "ENGINE : ", engine

		SELECT CASE(engine)
		CASE("classical_md")
			call initialize_classical_MD(param_library)
		CASE("classical_mc")
			call initialize_classical_MC(param_library)
		CASE("rpmd")
			call initialize_RPMD(param_library)
		CASE("wigner","wigner_md","wild")
			call initialize_wigner_MD(param_library)
		CASE("gwild","g-wild")
			call initialize_gwild(param_library)
		CASE("lgclwild")
			call initialize_lgclwild(param_library)
		CASE("wigner_bayes")
			call initialize_wb_MD(param_library)
		CASE("cmd","centroid_md")
			call initialize_cmd(param_library)
		CASE("schrodinger","schroedinger")
			call initialize_schrodinger(param_library)
		CASE("ivr")
			call initialize_IVR(param_library)
		CASE("awild")
			call initialize_awild(param_library)
		CASE("gxx")
			call initialize_gxx(param_library)
		CASE("autok2")
			call initialize_autok2(param_library)
		CASE("deconvolution")
			call initialize_deconvolution(param_library)
		CASE DEFAULT
			write(0,*) "Error: Unknown engine '"//engine//"'!"
			STOP "Execution stopped."
		END SELECT

	end subroutine initialize_engine

	subroutine finalize_engine()
		IMPLICIT NONE

		SELECT CASE(engine)
		CASE("classical_md")
			call finalize_classical_MD()
		CASE("classical_mc")
			call finalize_classical_MC()
		CASE("rpmd")
			call finalize_RPMD()
		CASE("wigner","wigner_md","wild")
			call finalize_wigner_MD()
		CASE("gwild","g-wild")
			call finalize_gwild()
		CASE("lgclwild")
			call finalize_lgclwild()
		CASE("wigner_bayes")
			call finalize_wb_MD()
		CASE("cmd","centroid_md")
			call finalize_cmd()
		CASE("schrodinger","schroedinger")
			call finalize_schrodinger()
		CASE("ivr")
			call finalize_IVR()
		CASE("awild")

		CASE("gxx")

		CASE("autok2")

		CASE("deconvolution")
			call finalize_deconvolution()
		CASE DEFAULT
			write(0,*) "Error: Unknown engine '"//engine//"'!"
			STOP "Execution stopped."
		END SELECT

	end subroutine finalize_engine

END MODULE engine_dispatcher