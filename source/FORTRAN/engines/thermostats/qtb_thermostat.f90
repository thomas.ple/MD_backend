module qtb_thermostat
    USE kinds
	USE random
	USE classical_md_types
    USE nested_dictionaries
    USE qtb_types
    USE file_handler, only : output
	implicit none

    TYPE(QTB_TYPE) :: qtb
    LOGICAL, SAVE :: write_spectra

    PRIVATE
    PUBLIC :: QTB_initialize, apply_QTB_thermostat &
                ,QTB_finalize, qtb_end_thermalization
	
contains

    subroutine QTB_initialize(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

        call qtb%initialize(system,parameters,param_library)
        write(*,*) "QTB : nom=",qtb%nom
        if(qtb%OTF_generation) then
            write(*,*) "QTB OTF : qtb_n_smooth=",qtb%n_smooth
			write(*,*) "QTB OTF : domega_OTF=",qtb%domega_OTF
        endif

    end subroutine QTB_initialize

    subroutine qtb_end_thermalization()
        IMPLICIT NONE        
        if(qtb%save_average_spectra) &
            call qtb%write_average_spectra("QTB_spectra.thermalization.out",restart_average=.TRUE.)
    end subroutine qtb_end_thermalization

    subroutine qtb_finalize()
        IMPLICIT NONE        
        if(qtb%save_average_spectra) call qtb%write_average_spectra("QTB_spectra.out")
        call qtb%deallocate()
    end subroutine qtb_finalize

    subroutine apply_QTB_thermostat(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
        REAL(wp), allocatable, save :: QTB_force(:,:)
		INTEGER :: i

		if(.not. allocated(QTB_force)) allocate(QTB_force(system%n_atoms,system%n_dim))

        QTB_force=qtb%get_force(system,parameters)

       ! do i=1,system%n_dim		
			system%P(:,:)=qtb%gamma_exp*system%P(:,:) &
							+ qtb%dt*QTB_force(:,:)
		!enddo

    end subroutine apply_QTB_thermostat

end module qtb_thermostat