MODULE thermostats
    USE kinds
    USE string_operations
    USE nested_dictionaries
    USE classical_md_types
    USE rpmd_types
    USE qtb_thermostat
    USE lgv_thermostat
    USE pi_lgv_thermostat
    USE pi_qtb_thermostat
    IMPLICIT NONE

    ABSTRACT INTERFACE
        subroutine classical_thermostat_type(system,parameters)
            import :: CLASS_MD_SYS, CLASS_MD_PARAM, wp
            implicit none
            CLASS(CLASS_MD_SYS), INTENT(inout) :: system
            CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
        end subroutine classical_thermostat_type

        subroutine PI_thermostat_type(system,parameters)
            import :: POLYMER_TYPE, RPMD_PARAM
			IMPLICIT NONE
			CLASS(POLYMER_TYPE), intent(inout) :: system
			CLASS(RPMD_PARAM), intent(inout) :: parameters
        end subroutine PI_thermostat_type

        function PI_thermostat_get_EigP_type(system,parameters,at,dim) RESULT(EigP)
            import :: POLYMER_TYPE, RPMD_PARAM, wp
			IMPLICIT NONE
			CLASS(POLYMER_TYPE), intent(inout) :: system
			CLASS(RPMD_PARAM), intent(inout) :: parameters
            INTEGER, INTENT(in) :: at, dim
            REAL(wp) :: EigP(system%n_beads)
        end function PI_thermostat_get_EigP_type
    END INTERFACE

    PROCEDURE(classical_thermostat_type), pointer :: apply_thermostat => null()
    PROCEDURE(PI_thermostat_type), pointer :: apply_PI_thermostat => null()
    PROCEDURE(PI_thermostat_get_EigP_type), pointer :: PI_thermostat_get_evolved_P => null()

    LOGICAL, SAVE :: fast_forward_langevin

    PRIVATE
    PUBLIC :: apply_thermostat, apply_PI_thermostat, PI_thermostat_get_evolved_P &
                , initialize_thermostat, finalize_thermostat, thermostat_end_thermalization &
                ,fast_forward_langevin

CONTAINS

    subroutine initialize_thermostat(system,parameters,param_library)
        IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

        parameters%thermostat_type = param_library%get("THERMOSTAT/thermostat_type",DEFAULT="langevin")
        !! @input_file THERMOSTAT/thermostat_type (thermostats, default="langevin")
        parameters%thermostat_type = to_lower_case(parameters%thermostat_type)

        fast_forward_langevin = param_library%get("THERMOSTAT/fast_forward_langevin",DEFAULT=.FALSE.)
        if(fast_forward_langevin) WRITE(*,*) "FAST FORWARD LANGEVIN"

        write(*,*) "Thermostat : "//parameters%thermostat_type
        SELECT CASE(parameters%thermostat_type)
        CASE("langevin")
            SELECT TYPE(system)
            CLASS IS (POLYMER_TYPE)
                SELECT TYPE(parameters)
                CLASS IS (RPMD_PARAM)
                    call pi_lgv_initialize(system,parameters,param_library)
                    apply_PI_thermostat => apply_pi_lgv_thermostat
                    PI_thermostat_get_evolved_P => pi_lgv_get_evolved_P
                CLASS DEFAULT
                    STOP "Error: class error in thermostat initialization!"
                END SELECT
            CLASS DEFAULT
                call lgv_initialize(system,parameters,param_library)
                apply_thermostat => apply_lgv_thermostat
            END SELECT
        CASE("qtb")
            SELECT TYPE(system)
            CLASS IS (POLYMER_TYPE)
                SELECT TYPE(parameters)
                CLASS IS (RPMD_PARAM)
                    call pi_qtb_initialize(system,parameters,param_library)
                    apply_PI_thermostat => apply_pi_qtb_thermostat
                    PI_thermostat_get_evolved_P => pi_qtb_get_evolved_P
                CLASS DEFAULT
                    STOP "Error: class error in thermostat initialization!"
                END SELECT
            CLASS DEFAULT
                call QTB_initialize(system,parameters,param_library)
                apply_thermostat => apply_QTB_thermostat
            END SELECT            
        CASE DEFAULT
            write(0,*) "Error: unknown thermostat '"//parameters%thermostat_type//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_thermostat

    subroutine finalize_thermostat(system,parameters)
        IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters

        SELECT CASE(parameters%thermostat_type)
        CASE("langevin")
            SELECT TYPE(system)
            CLASS IS (POLYMER_TYPE)
                SELECT TYPE(parameters)
                CLASS IS (RPMD_PARAM)
                    call pi_lgv_finalize()
                END SELECT
            CLASS DEFAULT
                call lgv_finalize()
            END SELECT
        CASE("qtb")
            SELECT TYPE(system)
            CLASS IS (POLYMER_TYPE)
                SELECT TYPE(parameters)
                CLASS IS (RPMD_PARAM)
                   call pi_qtb_finalize()
                END SELECT
            CLASS DEFAULT
                call QTB_finalize()
            END SELECT
        END SELECT

    end subroutine finalize_thermostat

    subroutine thermostat_end_thermalization(system,parameters)
        IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters

        SELECT CASE(parameters%thermostat_type)
        CASE("langevin")
            SELECT TYPE(system)
            CLASS IS (POLYMER_TYPE)
                SELECT TYPE(parameters)
                CLASS IS (RPMD_PARAM)
                    call pi_lgv_end_thermalization()
                END SELECT
            CLASS DEFAULT
                call lgv_end_thermalization()
            END SELECT
        CASE("qtb")
            SELECT TYPE(system)
            CLASS IS (POLYMER_TYPE)
                SELECT TYPE(parameters)
                CLASS IS (RPMD_PARAM)
                   call pi_qtb_end_thermalization()
                END SELECT
            CLASS DEFAULT
                call QTB_end_thermalization()
            END SELECT
        END SELECT

    end subroutine thermostat_end_thermalization


END MODULE thermostats