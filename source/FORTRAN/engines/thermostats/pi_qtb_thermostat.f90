MODULE pi_qtb_thermostat
    USE kinds
    USE random
    USE rpmd_types
    USE nested_dictionaries
	USE pi_lgv_thermostat, only: PI_LGV_TYPE
	USE qtb_types
    IMPLICIT NONE

    TYPE, EXTENDS(PI_LGV_TYPE) :: PI_QTB_TYPE

		TYPE(QTB_TYPE), allocatable :: qtb(:)
		REAL(wp), allocatable :: current_forces(:,:,:)
		LOGICAL :: save_average_spectra
		LOGICAL :: centroid_only

    END TYPE PI_QTB_TYPE

    TYPE(PI_QTB_TYPE) :: piqtb

    PRIVATE
    PUBLIC :: PI_QTB_TYPE, pi_qtb_initialize, apply_pi_qtb_thermostat,pi_qtb_get_evolved_P &
                ,pi_qtb_finalize, pi_qtb_update_forces, pi_qtb_end_thermalization

CONTAINS

    function pi_qtb_get_evolved_P(system,parameters,at,dim) result(EigP)
		!! NEED QTB FORCES TO BE UPDATED !!
        IMPLICIT NONE
        CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
        INTEGER, INTENT(in) :: at, dim
        REAL(wp) :: EigP(system%n_beads)
		INTEGER :: i
		REAL(wp) :: dt

		dt=piqtb%qtb(1)%dt

		if(piqtb%save_average_spectra) then
			DO i = 1 , system%n_beads
				call piqtb%qtb(i)%store_velocity_one_dof(SQRT(piqtb%qtb(i)%gamma_exp)*system%EigP(i,:,:) &
										+ 0.5_wp*dt*piqtb%current_forces(i,:,:) , at, dim)
			ENDDO
		endif
									

		EigP=system%EigP(:,at,dim)*piqtb%qtb(:)%gamma_exp &
				+ dt*piqtb%current_forces(:,at,dim)

    end function pi_qtb_get_evolved_P

    subroutine apply_pi_qtb_thermostat(polymer,parameters)
		implicit none
		CLASS(POLYMER_TYPE), intent(inout) :: polymer
		CLASS(RPMD_PARAM), intent(inout) :: parameters
		INTEGER :: i,j,k
		REAL(wp) :: dt

		dt=piqtb%qtb(1)%dt
		
		call pi_qtb_update_forces(polymer,parameters)

		! UPDATE NORMAL MODES
		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms
			polymer%EigP(:,j,k)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
		enddo ; enddo

		! SAVE (HALF PROPAGATED EIGEN) VELOCITIES
		if(piqtb%save_average_spectra) then
			DO i = 1 , polymer%n_beads
				call piqtb%qtb(i)%store_velocities(SQRT(piqtb%qtb(i)%gamma_exp)*polymer%EigP(i,:,:) &
							        				+ 0.5_wp*dt*piqtb%current_forces(i,:,:))
			ENDDO
		endif
		
		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms
			! PROPAGATE
			DO i=1,polymer%n_beads
				polymer%EigP(i,j,k)=polymer%EigP(i,j,k)*piqtb%qtb(i)%gamma_exp &
									+ dt*piqtb%current_forces(i,j,k)
			ENDDO

			! TRANSFORM BACK TO BEADS
			polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))

		enddo ; enddo
		
	end subroutine apply_pi_qtb_thermostat

	subroutine pi_qtb_update_forces(polymer,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(in) :: polymer
		CLASS(RPMD_PARAM), intent(in) :: parameters
		integer :: i

		do i=1,polymer%n_beads
			piqtb%current_forces(i,:,:)=piqtb%qtb(i)%get_force(polymer,parameters)
		enddo

	end subroutine pi_qtb_update_forces

    subroutine pi_qtb_initialize(system,parameters,param_library)
        IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		REAL(wp), allocatable :: power_spectrum_in(:)
        INTEGER :: nu, i

		if(parameters%algorithm=="mean_force") &
			STOP "Error: cannot compute mean_force with PI-QTB yet!"

        nu=system%n_beads

        piqtb%gamma0=param_library%get("THERMOSTAT/damping")
		!! @input_file THERMOSTAT/damping (pi_qtb_thermostat)
		if(piqtb%gamma0<=0) STOP "Error: 'damping' is not properly defined!"

        piqtb%is_TRPMD=param_library%get("THERMOSTAT/trpmd",default=.FALSE.)
		!! @input_file THERMOSTAT/trpmd (pi_qtb_thermostat, default=.FALSE.)
		piqtb%TRPMD_lambda=param_library%get("THERMOSTAT/trpmd_lambda",default=1._wp)
		!! @input_file THERMOSTAT/trpmd_lambda (pi_qtb_thermostat, default=1.)

		piqtb%centroid_only = param_library%get("THERMOSTAT/piqtb_centroid_only",default=.FALSE.)
		!! @input_file THERMOSTAT/piqtb_centroid_only (pi_qtb_thermostat, default=1.)

		allocate(piqtb%gamma(nu))
		if(piqtb%is_TRPMD) then		
			do i=1,nu
				piqtb%gamma(i)=max(piqtb%TRPMD_lambda*parameters%Omk(i) &
										,piqtb%gamma0)
			enddo		
		else
			piqtb%gamma(:)=piqtb%gamma0
		endif

		allocate(piqtb%current_forces(nu,system%n_atoms,system%n_dim))
		piqtb%current_forces=0._wp

		allocate(piqtb%qtb(nu))
		if(piqtb%centroid_only) then
			call piqtb%qtb(1)%initialize(system,parameters,param_library,piqtb%gamma(1))
		else
			call piqtb%qtb(1)%initialize(system,parameters,param_library,piqtb%gamma(1) &
					,OmK=parameters%OmK(nu),bead_index=0)
		endif
		
		
		write(*,*) 'PI-QTB cutoff set to ',piqtb%qtb(1)%omegacut*THz/(2._wp*pi),"THz"
		write(*,*) "QTB : nom=",piqtb%qtb(1)%nom
		if(piqtb%qtb(1)%OTF_generation) then    
            write(*,*) "QTB OTF : qtb_n_smooth=",piqtb%qtb(1)%n_smooth
			write(*,*) "QTB OTF : domega_OTF=",piqtb%qtb(1)%domega_OTF
        endif	

		allocate(power_spectrum_in(size(piqtb%qtb(1)%power_spectrum)))
		if(piqtb%centroid_only) then
			do i=2,nu
				call piqtb%qtb(i)%initialize(system,parameters,param_library &
						,piqtb%gamma(i), OmK=parameters%OmK(nu),classical_kernel=.TRUE.)
			enddo
		else
			do i=2,nu
				call piqtb%qtb(i)%initialize(system,parameters,param_library &
						,piqtb%gamma(i), OmK=parameters%OmK(nu) &
						,power_spectrum_in=piqtb%qtb(1)%power_spectrum &
						, bead_index=i-1)

			enddo
		endif
		
		if(piqtb%qtb(1)%save_average_spectra) then
			piqtb%save_average_spectra=.TRUE.
		else
			piqtb%save_average_spectra=.TRUE.
		endif
		
		
		write(*,*) 'PI-QTB: initialization done.'


    end subroutine pi_qtb_initialize

	subroutine pi_qtb_finalize()
        IMPLICIT NONE        
		INTEGER :: i
		CHARACTER(3) :: i_char

		DO i=1,piqtb%qtb(1)%n_beads
			write(i_char,'(i3.3)') i
            call piqtb%qtb(i)%write_average_spectra("PIQTB_spectra_"//i_char//".out")
			call piqtb%qtb(i)%deallocate()
		ENDDO

    end subroutine pi_qtb_finalize

	subroutine pi_qtb_end_thermalization()
        IMPLICIT NONE        
		INTEGER :: i
		CHARACTER(3) :: i_char

		DO i=1,piqtb%qtb(1)%n_beads
			write(i_char,'(i3.3)') i
            call piqtb%qtb(i)%write_average_spectra("PIQTB_spectra_"//i_char//".thermalization.out",restart_average=.TRUE.)
		ENDDO
    end subroutine pi_qtb_end_thermalization

END MODULE pi_qtb_thermostat