MODULE qtb_types
    USE kinds
    USE atomic_units
    USE random
    USE file_handler, only : output
    USE nested_dictionaries
    USE classical_md_types
    USE rpmd_types, only : POLYMER_TYPE, RPMD_PARAM
    USE string_operations
    USE matrix_operations, only : linear_interpolation, linear_fit &
                                    ,sym_mat_inverse
    IMPLICIT NONE

    complex(wp), parameter :: eye = cmplx (0._wp,1._wp,wp) 

    TYPE  QTB_TYPE

        INTEGER :: n_atoms,n_dim,n_dof
        REAL(wp) :: dt, kBT
        REAL(wp), allocatable :: mass(:)

        CHARACTER(2), allocatable :: element_symbols(:)
        CHARACTER(2), allocatable :: element_set(:)
        TYPE(DICT_STRUCT), pointer :: element_dict
        INTEGER :: n_elements
        LOGICAL :: write_spectra_by_element
        LOGICAL :: adapt_by_element

        LOGICAL :: mass_normalized

        REAL(wp) :: gamma0
        REAL(wp) :: gamma_exp

        CHARACTER(:), allocatable :: method
        LOGICAL :: adQTBr , adQTBf

        INTEGER :: n_rand
        INTEGER :: selector
        REAL(wp) :: omegacut
        REAL(wp) :: domega
        REAL(wp) :: omega_smear
        integer :: nom

        LOGICAL :: OU_integrator

        INTEGER :: i_traj
        LOGICAL :: save_average_spectra

    !FOR BARRAT
        INTEGER :: n_OTF, n_smooth
        REAL(wp) :: domega_OTF
    
    !FOR TESTING : white noise
        LOGICAL :: noQTB

    !FOR PI-QTB
        LOGICAL :: PI_QTB
        INTEGER :: n_beads
        REAL(wp) :: kernel_mixing
        REAL(wp) :: kernel_iter_prec
        INTEGER :: kernel_max_iter
        REAL(wp), ALLOCATABLE :: power_spectrum(:)
        REAL(wp), ALLOCATABLE :: kernel_weights(:)
        REAL(wp) :: kernel_sigma
        LOGICAL :: centroid_only
        LOGICAL :: beads_same_temperature
        INTEGER :: bead_index
        REAL(wp), ALLOCATABLE :: omK2(:)


    ! For QTB analysis
        real(wp), allocatable :: vfspec_aver(:,:,:)
        real(wp), allocatable :: vvspec_aver(:,:,:)
        real(wp), allocatable :: ffspec_aver(:,:,:)
        real(wp), allocatable :: mcvvgammar_aver(:,:,:)
        real(wp), allocatable :: vfspec(:,:,:)
        real(wp), allocatable :: vvspec(:,:,:)
        real(wp), allocatable :: ffspec(:,:,:)
        real(wp), allocatable :: mcvvgammar(:,:,:)

        real(wp), allocatable :: vv2spec(:,:,:)
        real(wp), allocatable :: v2fspec(:,:,:)
        real(wp), allocatable :: vv2spec_aver(:,:,:)
        real(wp), allocatable :: v2fspec_aver(:,:,:)
        ! Store random force
        ! real(wp), allocatable :: FRand_store(:,:,:) 
        ! Store velocities
        real(wp), allocatable :: veloc_store(:,:,:)  

        real(wp), allocatable :: vffspec(:)
        real(wp), allocatable :: vffspec_aver(:)
        real(wp), allocatable :: vvvspec(:)
        real(wp), allocatable :: vvvspec_aver(:)
        LOGICAL :: save_cvff
        INTEGER :: stride_cvff, counter_cvff


    ! For QTB calculation
        real(wp), allocatable :: fr(:),fi(:)
        real(wp), allocatable :: vr(:),vi(:)
        real(wp), allocatable :: work(:)

        ! QTB FORCE VECTOR
        real(wp), allocatable :: Force(:,:,:) 
        !real(wp), allocatable :: Forcex(:,:,:) 
        ! gamma_random vector
        real(wp), allocatable :: gammar(:,:,:)
        !real(wp), allocatable :: gammax(:,:,:)

        !FOR BARRAT METHOD
        LOGICAL :: OTF_generation
        REAL(wp), allocatable :: white_memory(:,:,:)
        REAL(wp), allocatable :: coloration_kernel(:,:,:)
        REAL(wp), allocatable :: OTF_current(:,:)
        ! INTEGER :: M_noise, OTF_i
        ! REAL(wp) :: dt_n
        
    ! For adQTB 
        REAL(wp) :: Agammas!,Agammax
        REAL(wp) :: alpha

        INTEGER :: n_slices
        INTEGER :: n_start_adapt

        real(wp), allocatable   :: d_FDR(:)
        real(wp), allocatable   :: d_FDR_typ(:,:)
        real(wp), allocatable   :: d_FDR_typ_prev(:,:)
        real(wp), allocatable   :: d_FDR_mean(:)
        real(wp), allocatable   :: d_FDR_mean_aver(:)

        INTEGER, ALLOCATABLE :: gamma_io_unit(:)
        LOGICAL :: save_every_gamma
    ! For adQTB-r
        

    ! For adQTB-f
        ! gamma_friction vector
        real(wp), allocatable :: gammaf(:,:,:)
        ! Generalized Langevin equation Memory kernel                                       
        real(wp), allocatable :: moment_mem_kern(:,:)
        ! Auxiliary velocities                  
        complex(wp), allocatable :: veloc_aux(:,:,:)
        ! Auxiliary random noise             
        complex(wp), allocatable :: QTBF_aux (:,:,:)

    CONTAINS
        PROCEDURE :: initialize => QTB_status_initialize
        PROCEDURE :: allocate => QTB_allocate
        PROCEDURE :: deallocate => QTB_deallocate
        PROCEDURE :: store_velocities => QTB_store_velocities
        PROCEDURE :: store_velocity_one_dof => QTB_store_velocity_one_dof
        ! PROCEDURE :: store_randForce => qtb_store_randForce        
        PROCEDURE :: fill_random_vector => QTB_fill_random_vector
        PROCEDURE :: compute_spectra => QTB_compute_vv_vf_ff_spec
        PROCEDURE :: average_spectra => QTB_average_vv_vf_ff_spec
        PROCEDURE :: write_average_spectra => QTB_write_ffspec_aver 
        PROCEDURE :: corr_colored_OU => QTB_corr_colored_OU
        PROCEDURE :: ff_theo => QTB_ff_theo
        PROCEDURE :: get_force => QTB_random_force
        PROCEDURE :: read_gammar => QTB_read_gammar
        PROCEDURE :: init_OTF_coloration_kernel => QTB_init_OTF_coloration_kernel
    END TYPE

    PRIVATE
    PUBLIC :: QTB_TYPE, QTB_theta

CONTAINS

    subroutine QTB_status_initialize(qtb,system,parameters,param_library &
                    ,gamma_in,power_spectrum_in,OmK,classical_kernel,bead_index)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
        CLASS(QTB_type), INTENT(inout) :: qtb
		TYPE(DICT_STRUCT), intent(in) :: param_library
        TYPE(DICT_STRUCT), pointer :: th_lib
        REAL(wp), INTENT(in), OPTIONAL :: gamma_in
        REAL(wp), INTENT(in), OPTIONAL :: power_spectrum_in(:)
        REAL(wp), INTENT(in), OPTIONAL :: OmK
        LOGICAL, INTENT(in), OPTIONAL :: classical_kernel
        INTEGER, INTENT(in), OPTIONAL :: bead_index
        REAL(wp) :: mp
        CHARACTER(3) :: bead_char
        CHARACTER(2) :: el
        INTEGER :: k

        qtb%n_atoms=system%n_atoms
        qtb%n_dim=system%n_dim
        qtb%n_dof=system%n_dof
        qtb%dt=parameters%dt
        qtb%kBT=parameters%temperature
        

        allocate(qtb%element_symbols(qtb%n_atoms) &
                ,qtb%element_set(system%n_elements))
        qtb%element_symbols=system%element_symbols
        qtb%element_set=system%element_set
        qtb%n_elements = system%n_elements
        qtb%element_dict => system%element_dict

        qtb%n_slices=1
        

        th_lib => param_library%get_child("THERMOSTAT")

        allocate(qtb%mass(size(system%mass)))
        
        qtb%mass_normalized = th_lib%get("mass_normalized",default=.FALSE.)
        if(qtb%mass_normalized) then
            qtb%mass(:)=1.
        else
            qtb%mass = system%mass
        endif

        qtb%save_average_spectra=th_lib%get("qtb_write_spectra" &
                                                    ,default=.TRUE.)
        if(qtb%save_average_spectra) then
            qtb%write_spectra_by_element=th_lib%get("qtb_write_spectra_by_element" &
                                                    ,default=.FALSE.)
                                                
            qtb%save_cvff=th_lib%get("qtb_save_cvff",default=.FALSE.)
            if(qtb%save_cvff) then
                qtb%stride_cvff=th_lib%get("qtb_stride_cvff",default=1)
                qtb%counter_cvff=0
            endif
        endif

        !CHECK IF PATH INTEGRALS CALCULATION
        SELECT TYPE(system)
        CLASS is (POLYMER_TYPE)
           
            qtb%PI_QTB=.TRUE.
            qtb%n_beads=system%n_beads
            mp=th_lib%get("piqtb_mixing_power" &
                            ,default=1._wp )
            !! @input_file THERMOSTAT/piqtb_mixing_power (pi_qtb_thermostat, default=1.)
            qtb%kernel_mixing=1._wp/real(qtb%n_beads,wp)**mp
            qtb%kernel_max_iter=th_lib%get("piqtb_kernel_max_iter" &
                                            ,default=10000 )
            !! @input_file THERMOSTAT/piqtb_kernel_max_iter (pi_qtb_thermostat, default=10000)
            qtb%kernel_iter_prec=th_lib%get("piqtb_kernel_iter_prec" &
                                            ,default=0.001_wp )
            !! @input_file THERMOSTAT/piqtb_kernel_iter_prec (pi_qtb_thermostat, default=0.001)
            qtb%kernel_sigma=th_lib%get("piqtb_corr_prop" &
                                        ,default=0.2_wp )
            !! @input_file THERMOSTAT/piqtb_corr_prop (pi_qtb_thermostat, default=0.2)
            qtb%centroid_only = param_library%get("THERMOSTAT/piqtb_centroid_only",default=.FALSE.)

            qtb%beads_same_temperature = param_library%get("THERMOSTAT/piqtb_beads_same_temperature",default=.FALSE.)

            allocate(qtb%omK2(qtb%n_beads))
            SELECT TYPE(parameters)
            CLASS is (RPMD_PARAM)
                qtb%omK2 = parameters%OmK**2
            END SELECT

            if(qtb%centroid_only .AND. qtb%beads_same_temperature) &
                STOP "Error: 'piqtb_centroid_only' and 'piqtb_beads_same_temperature' are not compatible!"
            
            if(qtb%beads_same_temperature) then
                if(present(bead_index)) then
                    qtb%bead_index=bead_index
                else
                    STOP "Error: bead_index not passed when initializing &
                            & 'piqtb_beads_same_temperature'!"
                endif
            endif

        CLASS DEFAULT
            qtb%PI_QTB=.FALSE.
        END SELECT

        qtb%noQTB=th_lib%get("noqtb", default=.FALSE.)
        if(qtb%noQTB) then
            write(*,*) "Using noQTB (white noise). ONLY FOR TESTING!"
        endif

        !GET QTB method
        qtb%adQTBr=.FALSE.
        qtb%adQTBf=.FALSE.
        qtb%adapt_by_element=.FALSE.
        !if(qtb%PI_QTB) then 
         !   qtb%method="qtb_basic" ! NO ADAPTIVE FOR PATH INTEGRALS
            !write(*,*) "Initialize PI-QTB."
        !else
            qtb%method = th_lib%get("qtb_method",DEFAULT="qtb_basic")
            !! @input_file THERMOSTAT/qtb_method (qtb_types, default="qtb_basic")
            qtb%method = to_lower_case(qtb%method)

            SELECT CASE(qtb%method)
            CASE("qtb_basic")
                write(*,*) "Initialize basic QTB."               
            CASE("adqtb-r","adqtbr")
                write(*,*) "Initialize adQTB-r."
                qtb%save_average_spectra=.TRUE.
                qtb%adQTBr=.TRUE.
                qtb%Agammas = th_lib%get("adqtb_agammas",default=10._wp)
                !! @input_file THERMOSTAT/adqtb_agammas (qtb_types, default=10., if qtb_method=adqtb-r)
                !qtb%Agammax = th_lib%get("adqtb_agammax",default=1.0e-13)
                !write(*,*) "agammax=",qtb%Agammax

                qtb%n_start_adapt = th_lib%get("adqtb_n_start_adapt", default=5)
                !! @input_file THERMOSTAT/adqtb_n_start_adapt (qtb_types, default=5, if qtb_method=adqtb-r or adqtb-f)
                qtb%adapt_by_element = th_lib%get("adqtb_adapt_by_element",default=.FALSE.)
                !! @input_file THERMOSTAT/adqtb_adapt_by_element (qtb_types, default=.FALSE., if qtb_method=adqtb-r)
                if(qtb%adapt_by_element) write(*,*) "adQTB-r: adaptation by element"
            CASE("adqtb-f","adqtbf")
                write(*,*) "Initialize adQTB-f."
                qtb%save_average_spectra=.TRUE.
                qtb%adQTBf=.TRUE.
                qtb%Agammas = th_lib%get("adqtb_agammas",default=0.02/cm1)
                !! @input_file THERMOSTAT/adqtb_agammas (qtb_types, default=10., if qtb_method=adqtb-f)
                qtb%n_start_adapt = th_lib%get("adqtb_n_start_adapt", default=5)
            CASE DEFAULT
                write(0,*) "Error: unkown QTB method '"//qtb%method//"' !"
                STOP "Execution stopped"
            END SELECT 
        !endif

        if(present(classical_kernel)) then
            if(classical_kernel) qtb%noQTB=.TRUE.
        endif

        !! GET CHOICE OF NOISE GENERATION
        qtb%OTF_generation  = th_lib%get("qtb_on_the_fly" &
                                        ,default=.FALSE.)
        !! @input_file THERMOSTAT/qtb_on_the_fly (qtb_types, default=.FALSE.)

        !! GET gamma0
        if(present(gamma_in)) then
            qtb%gamma0=gamma_in
        else
            qtb%gamma0=th_lib%get("damping")
            !! @input_file THERMOSTAT/damping (qtb_types)
        endif
		if(qtb%gamma0<=0) STOP "Error: 'damping' is not properly defined!"
        write(*,*) "qtb damping=",qtb%gamma0*THz,"THz"
        qtb%gamma_exp=exp(-qtb%gamma0*qtb%dt)

        !! GET omegacut
        qtb%omegacut  = th_lib%get("qtb_cutoff")
        !! @input_file THERMOSTAT/qtb_cutoff (qtb_types)
        if(present(OmK)) then
            qtb%omegacut=qtb%omegacut + 1.5*OmK
        endif
        if ( qtb%omegacut >= pi/parameters%dt ) &
            STOP 'Error: the QTB cutoff frequency should be significantly &
                    &less than 1/dt !'        

        !GET QTB PARAMETERS
        if(qtb%OTF_generation) then
            qtb%n_OTF = th_lib%get("qtb_n_otf")
            !! @input_file THERMOSTAT/qtb_n_otf (qtb_types, if QTB_OTF)
            if(qtb%n_OTF<=0) STOP "Error: 'qtb_n_otf' must be positive"
            qtb%n_smooth = th_lib%get("qtb_n_smooth", default=2)
            if(qtb%n_smooth<=0) STOP "Error: 'qtb_n_smooth' must be positive"
            !! @input_file THERMOSTAT/qtb_n_smooth (qtb_types, if QTB_OTF, default=2)
            qtb%n_rand = (2*qtb%n_smooth+1)*qtb%n_OTF
            qtb%domega_OTF = pi/(qtb%n_OTF*qtb%dt)
            if(qtb%domega_OTF > qtb%omegacut/10._wp) then
                write(0,*) "Warning: domega_OTF=",qtb%domega_OTF*cm1,"cm^-1 is too large !"
                write(0,*) "         'qtb_n_otf' should be increased."
            endif
        else
            qtb%n_rand = th_lib%get("qtb_n_rand" &
                            ,default=parameters%n_steps+parameters%n_steps_therm)
            !! @input_file THERMOSTAT/qtb_n_rand (qtb_types, default=n_steps+n_steps_therm)
        endif
        qtb%omega_smear = qtb%omegacut/50._wp
		qtb%domega      = pi/(qtb%n_rand*qtb%dt) !2._wp*pi/(qtb%n_rand*qtb%dt)
		qtb%nom         = nint(qtb%omegacut/qtb%domega)
        ! if(qtb%OTF_generation) then
        !     qtb%dt_n = pi/qtb%omegacut            
        !     qtb%M_noise = nint(qtb%dt_n / qtb%dt)
        ! endif

        if(qtb%adQTBf) then
            qtb%alpha   = th_lib%get("adqtb_alpha",default=qtb%domega)
            !! @input_file THERMOSTAT/adqtb_alpha (qtb_types, default=domega, if qtb_method=adqtb-f)
            IF(qtb%alpha > 0.5_wp*qtb%gamma0) then
                STOP "Error: adqtb_alpha should be at least smaller than half of gamma"
            ELSE
                write(*,*) "adQTB-f: alpha=",qtb%alpha*THz,"THz"
            ENDIF
        endif
        

        !! ALLOCATE QTB ARRAYS
        call qtb%allocate()
        
        if(th_lib%has_key("gammar_file")) then
            call qtb%read_gammar(th_lib%get("gammar_file"))
        else
            qtb%gammar(:,:,:) = qtb%gamma0
        endif

        !if(th_lib%has_key("gammax_file")) then
        !    call QTB_read_gammax(qtb,th_lib%get("gammax_file"))
        !else
        !    qtb%gammax(:,:,:) = 0._wp
        !endif

        qtb%i_traj=0

        ! If the integrator contains an Ornstein-Uhlenbeck process,
        ! correct the colored noise accordingly
        SELECT CASE(parameters%integrator_type)
        CASE("baoab","aboba","pile-l")
            qtb%OU_integrator = .true.
        CASE("stochastic_verlet")
            write(0,*) "Error: QTB not compatible with integrator &
                        &'"//parameters%integrator_type//"' !"
            STOP "Execution stopped."
        CASE DEFAULT
            qtb%OU_integrator = .false.
        END SELECT   
        !if(qtb%PI_QTB) qtb%OU_integrator = .false.   
        ! qtb%OU_integrator = th_lib%get("ou_correction_colored_noise" &
        !                                 ,default=.FALSE.)
        !! @input_file THERMOSTAT/ou_correction_colored_noise (qtb_types, default=.FALSE.)

        qtb%selector=0

        if(present(power_spectrum_in) .AND. (.NOT. qtb%beads_same_temperature)) then
            if(size(power_spectrum_in) /= qtb%nom) &
                STOP "Error: wrong size of input QTB power spectrum!"
            qtb%power_spectrum=power_spectrum_in
        else
            call compute_power_spectrum(qtb)
        endif


        if(qtb%OTF_generation) then
            call qtb%init_OTF_coloration_kernel(reset_memory=.TRUE.)
        else
            call qtb%fill_random_vector()
        endif

        if(qtb%adQTBf .OR. qtb%adQTBr) then
            qtb%save_every_gamma = th_lib%get("adqtb_save_every_gamma",default=.FALSE.)
            if(qtb%save_every_gamma) then
                if(qtb%adapt_by_element) then
                    allocate(qtb%gamma_io_unit(qtb%n_elements))
                    DO k=1,qtb%n_elements
                        el=qtb%element_set(k)
                        IF(qtb%PI_QTB) then
                            write(bead_char,'(i3.3)') qtb%bead_index+1
                            open(newunit=qtb%gamma_io_unit(k), file=output%working_directory//&
                                    &'/PIadQTB_gammas.'//bead_char//'.out.'//trim(el))
                        else
                            open(newunit=qtb%gamma_io_unit(k), file=output%working_directory//&
                                    &'/adQTB_gammas.out.'//trim(el))
                        endif
                    ENDDO
                else
                    allocate(qtb%gamma_io_unit(1))
                    IF(qtb%PI_QTB) then
                        write(bead_char,'(i3.3)') qtb%bead_index+1
                        open(newunit=qtb%gamma_io_unit(1), file=output%working_directory//&
                                &'/PIadQTB_gammas.'//bead_char//'.out')
                    else
                        open(newunit=qtb%gamma_io_unit(1), file=output%working_directory//&
                                &'/adQTB_gammas.out')
                    endif
                endif
            endif
        endif


        if(qtb%save_cvff) then
            call QTB_initialize_cvff_cvvv(qtb)
        endif

    end subroutine QTB_status_initialize

!---------------------------------------------------
! QTB FORCE CALCULATIONS

    !FILL THE FORCE VECTOR WITH RANDOM FORCES (QTB SPECTRUM)
	subroutine QTB_fill_random_vector(qtb)	       
        IMPLICIT NONE
        CLASS(QTB_type), INTENT(inout) :: qtb

		! local variables
		integer :: i,j,iom, ifail
		real(wp) :: omega, omega_range(qtb%nom)
		real(wp) :: fmax, aux, interp
        real(4) :: huge_lnr = log(HUGE(1.))
        real(wp) :: fr(2*qtb%n_rand), fi(2*qtb%n_rand), &
                    work(2*qtb%n_rand+1)

        !INITIALIZE POWER SPECTRUM RANGE
        do iom=1,qtb%nom
            omega_range(iom)=real(iom-1,wp)*qtb%domega 
        enddo

        ! n_ex=max(2,nint(0.2*qtb%nom))
        ! call linear_fit(omega_range(qtb%nom-n_ex+1:), qtb%power_spectrum(qtb%nom-n_ex+1:) &
        !                 ,0.0001_wp, beta)

        do j = 1,qtb%n_dim ;do i = 1, qtb%n_atoms         

            call randGaussN(fr)
            call randGaussN(fi)
            !Creates the FT of the force for positive frequencies:
            do  iom=2,qtb%n_rand
                omega = real(iom-1,wp)*qtb%domega
                
                if ( (omega-qtb%omegacut)/qtb%omega_smear  < huge_lnr ) then
                    fmax = 1._wp/(1._wp+exp((omega-qtb%omegacut)/qtb%omega_smear))  
                    !factor used to filter out high frequencies
                else
                    fmax = 0._wp
                endif

                aux = fmax!*QTB_theta(omega,qtb%kBT) 

                if (iom <= qtb%nom) then
                    aux = aux * qtb%gammar(i,j,iom) * qtb%power_spectrum(iom)
                else
                    interp=linear_interpolation( &
                          omega,qtb%power_spectrum,omega_range,prop_ex=0.05)
                    aux = aux * qtb%gamma0 * interp
                endif
                if (qtb%OU_integrator) then
                    aux = aux * qtb%corr_colored_OU(omega)
                endif 


                fr(iom) = sqrt(aux)*fr(iom)
                fi(iom) = sqrt(aux)*fi(iom)
            enddo
            
            !Creates the FT of the force for negative frequencies:
            do iom=qtb%n_rand+2,2*qtb%n_rand
                fr(iom) =  fr(2*qtb%n_rand-iom+2)  ! real part
                fi(iom) = -fi(2*qtb%n_rand-iom+2)  ! imag part
            enddo
            
            !Zero-frequency term:
            aux = qtb%kBT*qtb%gammar(i,j,1)    
            if(qtb%PI_QTB) aux=aux*real(qtb%n_beads,wp) 
            if (qtb%OU_integrator) then
                aux = aux * qtb%corr_colored_OU(0._wp)
            endif 
            fr(1) = sqrt(aux)*fr(1)!*WORK(qtb%n_rand)
            fi(1) = sqrt(aux)*fi(1)!*WORK(qtb%n_rand)
            fr(qtb%n_rand+1)=0._wp
            fi(qtb%n_rand+1)=0._wp

            call C06FCF(fr,fi,2*qtb%n_rand,WORK,ifail)    
            qtb%Force(i,j,1:qtb%n_rand) = fr(1:qtb%n_rand)   &
                                        * sqrt(qtb%mass(i)/qtb%dt)

        enddo ;enddo 

	end subroutine QTB_fill_random_vector

    !FILL THE FORCE VECTOR WITH RANDOM FORCES (QTB SPECTRUM)
	! subroutine QTB_fill_random_vector_pos(qtb)	       
    !     IMPLICIT NONE
    !     CLASS(QTB_type), INTENT(inout) :: qtb

	! 	! local variables
	! 	integer :: i,j,iom, ifail
	! 	real(wp) :: omega, omega_range(qtb%nom)
	! 	real(wp) :: fmax, aux, interp
    !     real(4) :: huge_lnr = log(HUGE(1.))
    !     real(wp) :: fr(2*qtb%n_rand), fi(2*qtb%n_rand), &
    !                 work(2*qtb%n_rand+1), cr,ci

    !     !INITIALIZE POWER SPECTRUM RANGE
    !     do iom=1,qtb%nom
    !         omega_range(iom)=real(iom-1,wp)*qtb%domega 
    !     enddo

    !     ! n_ex=max(2,nint(0.2*qtb%nom))
    !     ! call linear_fit(omega_range(qtb%nom-n_ex+1:), qtb%power_spectrum(qtb%nom-n_ex+1:) &
    !     !                 ,0.0001_wp, beta)

    !     do j = 1,qtb%n_dim ;do i = 1, qtb%n_atoms         

    !         ! call randGaussN(fr)
    !         ! call randGaussN(fi)
    !         call randGauss(cr)
    !         call randGauss(ci)
    !         !Creates the FT of the force for positive frequencies:
    !         do  iom=2,qtb%n_rand
    !             omega = real(iom-1,wp)*qtb%domega
                
    !             ! if ( (omega-qtb%omegacut)/qtb%omega_smear  < huge_lnr ) then
    !             !     fmax = 1._wp/(1._wp+exp((omega-qtb%omegacut)/qtb%omega_smear))  
    !             !     !factor used to filter out high frequencies
    !             ! else
    !             !     fmax = 0._wp
    !             ! endif

    !             ! aux = fmax!*QTB_theta(omega,qtb%kBT) 
    !             aux=1

    !             if (iom <= qtb%nom) then
    !                 aux = aux * qtb%gammax(i,j,iom)
    !             else
    !                 aux = 0
    !             endif
    !             ! if (qtb%OU_integrator) then
    !             !     aux = aux * qtb%corr_colored_OU(omega)
    !             ! endif 


    !             ! fr(iom) = sqrt(aux)*fr(iom)
    !             ! fi(iom) = sqrt(aux)*fi(iom)
    !             fr(iom) = sqrt(aux)*cr
    !             fi(iom) = sqrt(aux)*ci
    !         enddo
            
    !         !Creates the FT of the force for negative frequencies:
    !         do iom=qtb%n_rand+2,2*qtb%n_rand
    !             fr(iom) =  fr(2*qtb%n_rand-iom+2)  ! real part
    !             fi(iom) = -fi(2*qtb%n_rand-iom+2)  ! imag part
    !         enddo
            
    !         !Zero-frequency term:
    !         aux = qtb%gammax(i,j,1)    
    !         if(qtb%PI_QTB) aux=aux*real(qtb%n_beads,wp) 
    !         ! if (qtb%OU_integrator) then
    !         !     aux = aux * qtb%corr_colored_OU(0._wp)
    !         ! endif 
    !         ! fr(1) = sqrt(aux)*fr(1)!*WORK(qtb%n_rand)
    !         ! fi(1) = sqrt(aux)*fi(1)!*WORK(qtb%n_rand)
    !         fr(1) = sqrt(aux)*cr
    !         fi(1) = sqrt(aux)*ci
    !         fr(qtb%n_rand+1)=0._wp
    !         fi(qtb%n_rand+1)=0._wp

    !         call C06FCF(fr,fi,2*qtb%n_rand,WORK,ifail)    
    !         qtb%Forcex(i,j,1:qtb%n_rand) = fr(1:qtb%n_rand)

            

    !     enddo ;enddo
        
    !     ! do iom=1,qtb%n_rand
    !     !  write(75,* ) real(iom-1,wp)*qtb%domega*cm1,qtb%Forcex(1,1,iom)
    !     ! enddo

	! end subroutine QTB_fill_random_vector_pos

    subroutine QTB_init_OTF_coloration_kernel(qtb,reset_memory)
        IMPLICIT none
        CLASS(QTB_type), INTENT(inout) :: qtb
        LOGICAL, INTENT(in) :: reset_memory
        INTEGER :: i,j, Nf, iom,k,nk
        REAL(wp) :: aux,c,omega,fmax,omega_range(qtb%nom)
        REAL(wp), allocatable :: htilde(:)
        real(wp) :: huge_lnr = log(HUGE(1._wp))

        do iom=1,qtb%nom
            omega_range(iom)=real(iom-1,wp)*qtb%domega
        enddo

        Nf=qtb%n_OTF
        qtb%coloration_kernel=0._wp
        allocate(htilde(-Nf:Nf-1))

        DO j=1,qtb%n_dim ; DO i=1,qtb%n_atoms
            htilde=0._wp
            !Zero-frequency term:
            aux=qtb%kBT*SUM(qtb%gammar(i,j,1:qtb%n_smooth+1))/REAL(qtb%n_smooth+1,wp)
            if(qtb%PI_QTB) aux=aux*real(qtb%n_beads,wp) 
            if (qtb%OU_integrator) then
                aux = aux * qtb%corr_colored_OU(0._wp)
            endif 
            htilde(0)=sqrt(aux)

            ! omega=real(Nf,wp)*qtb%domega_OTF
            ! !c=0.5_wp*omega*qtb%dt_n
            ! aux=qtb%gammar(i,j,Nf)*qtb%power_spectrum(Nf)
            ! if (qtb%OU_integrator) then
            !     aux = aux * qtb%corr_colored_OU(omega)
            ! endif
            ! qtb%coloration_kernel(i,j,1)=sqrt(aux)!*c/sin(c)


            DO iom=1,Nf-1

                omega=real(iom,wp)*qtb%domega_OTF

                if ( (omega-qtb%omegacut)/qtb%omega_smear  < huge_lnr ) then
                    fmax = 1._wp/(1._wp+exp((omega-qtb%omegacut)/qtb%omega_smear))  
                    !factor used to filter out high frequencies
                else
                    fmax = 0._wp
                endif
                
                !c=0.5_wp*omega*qtb%dt_n
                nk = 1 + iom*(2*qtb%n_smooth+1)
                if(nk+qtb%n_smooth<=qtb%nom) then
                    aux=fmax*SUM(qtb%gammar(i,j,nk-qtb%n_smooth:nk+qtb%n_smooth) &
                                *qtb%power_spectrum(nk-qtb%n_smooth:nk+qtb%n_smooth) &
                            ) / REAL(2*qtb%n_smooth+1,wp)
                else
                    aux=fmax*qtb%gamma0*linear_interpolation( &
                          omega,qtb%power_spectrum,omega_range,prop_ex=0.2)
                endif

                if (qtb%OU_integrator) then
                    aux = aux * qtb%corr_colored_OU(omega)
                endif
                htilde(iom)=sqrt(aux)!*c/sin(c)
                htilde(-iom)=htilde(iom)
               ! qtb%coloration_kernel(i,j,2*Nf-iom+1)=qtb%coloration_kernel(i,j,iom)     
            ENDDO

            !qtb%coloration_kernel(i,j,Nf+2:2*Nf)=qtb%coloration_kernel(i,j,1:Nf-1:-1)

            ! pedestrian but safe Fourier transform to generate H(t).
            DO iom=-Nf,Nf-1
                c = real(iom,wp)*pi / real(Nf,wp)
                qtb%coloration_kernel(i,j,Nf+iom+1)=dot_product( htilde, (/ (cos(c*real(k,wp)) , k=-Nf,Nf-1) /) ) &
                                                *sqrt(2._wp*qtb%mass(i)/qtb%dt)/real(2*Nf,wp) 
            ENDDO

        ENDDO ; ENDDO

        ! call QTB_write_coloration_kernel(qtb)

        ! ! pedestrian but safe Fourier transform to generate H(t).
        ! DO j=1,qtb%n_dim ; DO i=1,qtb%n_atoms
        !     htilde(:)=qtb%coloration_kernel(i,j,:)
        !     DO iom=1,2*Nf
        !         c = real(iom-(Nf+1),wp)*pi / real(Nf,wp)
        !         qtb%coloration_kernel(i,j,iom)=dot_product( htilde, (/ (cos(c*real(k,wp)) , k=-Nf,Nf-1) /) ) &
        !                                         *qtb%dt*sqrt(2._wp*qtb%mass(i)/qtb%dt_n)/real(2*Nf,wp) 
        !     ENDDO
        ! ENDDO ; ENDDO

        if(reset_memory) then
            !FILL WHITE NOISE   
            DO j=1,qtb%n_dim ; DO i=1,qtb%n_atoms
                call randGaussN(qtb%white_memory(i,j,:))
            ENDDO ; ENDDO    

            ! qtb%OTF_i=qtb%M_noise
        endif

    end subroutine QTB_init_OTF_coloration_kernel

    subroutine QTB_write_coloration_kernel(qtb)
        IMPLICIT NONE
        CLASS(QTB_type), INTENT(inout) :: qtb
        INTEGER :: iom,u
        REAL(wp) :: omega

        open(newunit=u,file=output%working_directory//"/QTB_coloration_kernel.out")
        DO iom=0,qtb%nom-1
            omega=real(iom-qtb%nom,wp)*qtb%domega
            write(u,*) omega,qtb%coloration_kernel(:,:,iom+1)
        ENDDO
        DO iom=0,qtb%nom-1
            omega=real(iom,wp)*qtb%domega
            write(u,*) omega,qtb%coloration_kernel(:,:,qtb%nom+iom+1)
        ENDDO
        close(u)

    end subroutine QTB_write_coloration_kernel

    !returns a random force distributed with QTB
    function QTB_random_force(qtb,system,parameters) result(F)
        implicit none
        CLASS(QTB_type), INTENT(inout) :: qtb
        CLASS(CLASS_MD_SYS), INTENT(in) :: system
        CLASS(CLASS_MD_PARAM), INTENT(in) :: parameters
        real(wp) :: F(system%n_atoms,system%n_dim)

        qtb%selector=qtb%selector+1
        IF(qtb%selector>qtb%n_rand) THEN
            CALL QTB_reinit(qtb)
        ENDIF    

        if(qtb%OTF_generation) then
            ! qtb%OTF_i=qtb%OTF_i+1
            ! if(qtb%OTF_i > qtb%M_noise) &
                call QTB_update_OTF_current(qtb)
            F(:,:)=qtb%OTF_current(:,:)
            if(qtb%save_average_spectra) &
                qtb%Force(:,:,qtb%selector)=F(:,:)
        else
            F(:,:)=qtb%Force(:,:,qtb%selector)   
        endif     

        if(qtb%adQTBf) then
            call adapt_gamma_f(qtb)
            F(:,:)=F(:,:) - qtb%moment_mem_kern(:,:)
        endif

        if(qtb%save_average_spectra) then
            if(.NOT. qtb%PI_QTB) &
                !call qtb%store_velocities(system%P(:,:))
                call qtb%store_velocities(SQRT(qtb%gamma_exp)*system%P(:,:) &
				 			            + 0.5_wp*qtb%dt*F(:,:))
        endif

    end function QTB_random_force


    !REINITIALIZE FORCE VECTOR IF EVERYTHING USED
    subroutine QTB_reinit(qtb)
        implicit none
        CLASS(QTB_type), INTENT(inout) :: qtb
        INTEGER :: i,j

        qtb%n_slices=qtb%n_slices+1

        if(qtb%save_average_spectra) then
            call qtb%compute_spectra()
            ! call qtb%average_spectra()
            ! if(qtb%save_average_spectra) then
            !     if(qtb%PI_QTB) then
            !         call qtb%write_average_spectra("PIQTB_spectra_"&
            !             &//int_to_str(qtb%bead_index+1)//".tmp")
            !     else
            !         call qtb%write_average_spectra("QTB_spectra.tmp")
            !     endif
            ! endif
        endif

        if (qtb%adQTBr .AND. (qtb%n_slices >= qtb%n_start_adapt)) then
                call adapt_gamma_r(qtb)
                if(qtb%OTF_generation) &
                    call qtb%init_OTF_coloration_kernel(reset_memory=.FALSE.)            
        elseif(qtb%save_average_spectra .AND. (.NOT. qtb%adQTBf)) then
            qtb%d_FDR_mean=0
            do j = 1,qtb%n_dim ; do i=1,qtb%n_atoms            
                    qtb%d_FDR_mean(:) = qtb%d_FDR_mean(:) + qtb%vfspec(i,j,:) &
                                        -qtb%mcvvgammar(i,j,:)
            enddo ; enddo
            qtb%d_FDR_mean=qtb%d_FDR_mean/REAL(qtb%n_dof,wp)
        endif

 
        if(qtb%save_average_spectra) then
            call qtb%average_spectra()
            if(qtb%PI_QTB) then
                call qtb%write_average_spectra("PIQTB_spectra_"&
                    &//int_to_str(qtb%bead_index+1)//".tmp")
            else
                call qtb%write_average_spectra("QTB_spectra.tmp")
            endif

            if(qtb%save_cvff) call QTB_write_cvff_cvvv(qtb)
        endif

        if(.not. qtb%OTF_generation) then
            call qtb%fill_random_vector()
           ! call QTB_fill_random_vector_pos(qtb)
        endif

        if(qtb%adQTBf) then
            qtb%d_FDR_typ_prev=qtb%d_FDR_typ
            qtb%d_FDR_typ(:,:) = 0._wp
            qtb%d_FDR_mean(:) = 0._wp
        endif
        
        if(qtb%adQTBf .OR. qtb%adQTBr) then
            call write_gamma_adapt(qtb)
        endif
        
        qtb%selector=1

    end subroutine QTB_reinit

    subroutine QTB_update_OTF_current(qtb)
        IMPLICIT NONE
        CLASS(QTB_type), INTENT(inout) :: qtb
        INTEGER :: i,j
        REAL(wp) :: R(qtb%n_atoms)
        
        DO j=1,qtb%n_dim
            call randGaussN(R)     
            DO i=1,qtb%n_atoms                
                qtb%white_memory(i,j,:)= &
                    eoshift(qtb%white_memory(i,j,:),1,R(i))
                qtb%OTF_current(i,j)=dot_product( qtb%coloration_kernel(i,j,:) &
                                                , qtb%white_memory(i,j,:) )
            ENDDO
        ENDDO
        ! qtb%OTF_i=1

    end subroutine QTB_update_OTF_current

!---------------------------------------------------

    function QTB_corr_colored_OU(qtb,omega) result(r)
	   ! computes correction for a Ornstein-Uhlenbeck process
	   ! with colored noise
	    IMPLICIT NONE
        CLASS(QTB_type), INTENT(inout) :: qtb
	    real(wp) :: omega,r
	
        r = ( 1._wp - 2._wp*exp(-qtb%gamma0*qtb%dt) * cos(omega*qtb%dt) &
            + exp(-2._wp*qtb%gamma0 *qtb%dt) ) / (qtb%dt**2 * (qtb%gamma0 **2 + omega **2))
	
	end function QTB_corr_colored_OU

    function QTB_theta(omega,kBT) result(r)
        implicit none
        real(wp) :: r,omega,kBT
        real(wp), parameter :: omega_min = TINY(omega)

        if (omega  > omega_min) then
            r = hbar*omega*0.5_wp/tanh(0.5_wp*(hbar*omega/kBT))
        !    r =  hbar*omega*(0.5_wp+1._wp/(exp(hbar*omega/kBT)-1._wp))
        else
            r = kBT
        endif

    end function QTB_theta


    function QTB_ff_theo(qtb,omega,mass) result(ff_theo)
      implicit none
      CLASS(QTB_type), INTENT(inout) :: qtb
      real(wp) :: ff_theo, omega, mass

        ff_theo = 2._wp * mass * qtb%gamma0 * QTB_theta(omega,qtb%kBT)

    end function QTB_ff_theo

!----------------------------------------------------
! ADAPTATION ROUTINES

    ! Adapt the noise to limit Zero-point energy leakage
	! subroutine adapt_gamma_r(qtb)
    !     implicit none
    !     CLASS(QTB_type), INTENT(inout) :: qtb
    !     integer :: i, j,iom
    
    !     !call qtb%compute_spectra() !ALREADY DONE BEFORE
    
    !     do j = 1,qtb%n_dim ; do i=1,qtb%n_atoms            
    
    !         do iom = 1, qtb%nom
    !             qtb%d_FDR(iom) = qtb%vfspec(i,j,iom) - qtb%mass(i) &
    !                         * qtb%gammar(i,j,iom) * qtb%vvspec(i,j,iom)
    !         enddo

    !         qtb%d_FDR_typ(i,j) = SQRT(SUM(qtb%d_FDR**2)/REAL(qtb%nom,wp))

    !         qtb%gammar(i,j,:) = max(0._wp , qtb%gammar(i,j,:) + qtb%Agammas * qtb%gamma0 &
    !                         * real(qtb%n_rand,wp) * qtb%dt * qtb%d_FDR(:) / qtb%d_FDR_typ(i,j) )                        
    !     enddo ;enddo
    
    ! !    do iom = 1,nom
    ! !    write(77,'(4(E13.6,2X))') real(iom,wp) * domega, sum(gammar(:,:,iom))
    ! !    enddo
   
	! end subroutine adapt_gamma_r

    subroutine adapt_gamma_r(qtb)
        implicit none
        CLASS(QTB_type), INTENT(inout) :: qtb
        integer :: i,j,iom,iel,k,n_atoms_of
        INTEGER, ALLOCATABLE :: indices(:)
        REAL(wp) :: d_FDR_typ
        CHARACTER(2) :: el
    
        !call qtb%compute_spectra() !ALREADY DONE BEFORE        

        if(qtb%adapt_by_element) then
            qtb%d_FDR_mean=0._wp
            DO k=1,qtb%n_elements
                qtb%d_FDR=0
                el=qtb%element_set(k)
                n_atoms_of=qtb%element_dict%get(el//"/n")
                indices=qtb%element_dict%get(el//"/indices")
                DO i=1,n_atoms_of
                    iel=indices(i)
                    DO j=1,qtb%n_dim
                        qtb%d_FDR(:)=qtb%d_FDR(:)+qtb%vfspec(iel,j,:) &
                                            -qtb%mcvvgammar(iel,j,:)
                    ENDDO
                ENDDO
                d_FDR_typ = SQRT(SUM(qtb%d_FDR**2)/REAL(qtb%nom,wp))
                qtb%d_FDR_mean=qtb%d_FDR_mean+qtb%d_FDR/real(qtb%n_dof,wp)
                DO i=1,n_atoms_of
                    iel=indices(i)
                    DO j=1,qtb%n_dim
                        qtb%gammar(iel,j,:) = max(0._wp , qtb%gammar(iel,j,:) + qtb%Agammas * qtb%gamma0 &
                                * real(qtb%n_rand,wp) * qtb%dt * qtb%d_FDR(:) / d_FDR_typ )
                    ENDDO
                ENDDO
                deallocate(indices)
            ENDDO
        else
            qtb%d_FDR=0
            do j = 1,qtb%n_dim ; do i=1,qtb%n_atoms            
        
                do iom = 1, qtb%nom
                    qtb%d_FDR(iom) = qtb%d_FDR(iom) + qtb%vfspec(i,j,iom) &
                                        -qtb%mcvvgammar(i,j,iom)
                enddo

            enddo ; enddo

            d_FDR_typ = SQRT(SUM(qtb%d_FDR**2)/REAL(qtb%nom,wp))
            qtb%d_FDR_mean = qtb%d_FDR/REAL(qtb%n_dof)

            do j = 1,qtb%n_dim ; do i=1,qtb%n_atoms
                qtb%gammar(i,j,:) = max(0._wp , qtb%gammar(i,j,:) + qtb%Agammas * qtb%gamma0 &
                                * real(qtb%n_rand,wp) * qtb%dt * qtb%d_FDR(:) / d_FDR_typ )                        
            enddo ;enddo

            ! do j = 1,qtb%n_dim ; do i=1,qtb%n_atoms
            !     qtb%gammax(i,j,:) = max(0._wp , qtb%gammax(i,j,:) + qtb%Agammax &
            !                     * real(qtb%n_rand,wp) * qtb%dt * qtb%d_FDR(:) / d_FDR_typ )                        
            ! enddo ;enddo

        endif
    
    !    do iom = 1,nom
    !    write(77,'(4(E13.6,2X))') real(iom,wp) * domega, sum(gammar(:,:,iom))
    !    enddo
   
	end subroutine adapt_gamma_r


    ! Adapt the friction kernel to limit Zero-point energy leakage
	subroutine adapt_gamma_f(qtb)
        implicit none
        CLASS(QTB_type), INTENT(inout) :: qtb
        integer :: iom, i , j        

        call propagate_aux (qtb, qtb%veloc_store(:,:,qtb%selector), qtb%veloc_aux )
        call propagate_aux (qtb, qtb%Force(:,:,qtb%selector), qtb%QTBF_aux )      

        do j = 1, qtb%n_dim ; do i = 1, qtb%n_atoms    
        
            qtb%moment_mem_kern(i,j) =  qtb%mass(i) * & 
                    dot_product( qtb%gammaf(i,j,:) , real(qtb%veloc_aux(i,j,:),wp) )

        enddo ; enddo
            
        qtb%moment_mem_kern(:,:) = (2._wp*qtb%domega/pi) * qtb%moment_mem_kern(:,:)

        do j = 1, qtb%n_dim ; do i = 1, qtb%n_atoms

            do iom = 1, qtb%nom
                ! Difference from the Fluctuation-Dissipation relation  
                ! qtb%d_FDR(iom) = ( real(qtb%QTBF_aux(i,j,iom)*conjg(qtb%veloc_aux(i,j,iom)),wp)  &
                !                     + 0.5_wp * qtb%dt * abs(qtb%QTBF_aux(i,j,iom))**2 )          &
                !                 / (qtb%mass(i)*(1._wp + qtb%gamma0 * qtb%dt * 0.5_wp))
                
                ! qtb%d_FDR(iom) = real(qtb%QTBF_aux(i,j,iom)*conjg(qtb%veloc_aux(i,j,iom)),wp)  &
                !                         / qtb%mass(i)


                ! qtb%d_FDR(iom) =  qtb%d_FDR(iom) - qtb%gamma0 * qtb%mass(i)	&
                !                         * abs(qtb%veloc_aux(i,j,iom))**2      
                qtb%d_FDR(iom) = real(qtb%veloc_aux(i,j,iom)*conjg(qtb%QTBF_aux(i,j,iom)),wp) &
                                    - qtb%gamma0 * qtb%mass(i) * abs(qtb%veloc_aux(i,j,iom))**2

            enddo

            qtb%d_FDR(:)=qtb%d_FDR(:)*(2._wp*qtb%alpha)
        
            qtb%d_FDR_typ(i,j) = qtb%d_FDR_typ(i,j) + SQRT(SUM(qtb%d_FDR**2)/REAL(qtb%nom,wp))/real(qtb%n_rand,wp)
            qtb%d_FDR_mean=qtb%d_FDR_mean+qtb%d_FDR/REAL(qtb%n_dof*qtb%n_rand,wp)

            if(qtb%n_slices >= qtb%n_start_adapt) then
                qtb%gammaf(i,j,:) = qtb%gammaf(i,j,:) -  qtb%Agammas *qtb%gamma0 &
                                      * qtb%dt * qtb%d_FDR(:)  / qtb%d_FDR_typ_prev(i,j)
            endif

        enddo ; enddo

    end subroutine adapt_gamma_f

    subroutine propagate_aux(qtb,var,aux_var)
    	implicit none
        CLASS(QTB_type), INTENT(inout) :: qtb
    	real(wp), intent(in)       :: var(:,:)
    	complex(wp), intent(inout) :: aux_var(:,:,:)
    
    	! Local variables
    	real(wp) :: omega
    	integer  :: iom
    
          ! Auxiliary variables computation
    	do iom = 1 , qtb%nom
            omega = real(iom-1,wp) * qtb%domega
            aux_var(:,:,iom) = qtb%dt * var(:,:)  + aux_var(:,:,iom)*exp((eye*omega-qtb%alpha)*qtb%dt)
    	enddo 

    end subroutine propagate_aux


    subroutine write_gamma_adapt(qtb)
        implicit none
        CLASS(QTB_type), INTENT(inout) :: qtb
        integer :: iom,u,k,i,j,iel,n_atoms_of
        integer, allocatable :: indices(:)
        real(wp) :: omega,gamma_r, gamma_f ,n_dof_of 
        CHARACTER(2) :: el       

        if(qtb%adapt_by_element) then
            gamma_f=qtb%gamma0
            DO k=1,qtb%n_elements
                el=qtb%element_set(k)
                n_atoms_of=qtb%element_dict%get(el//"/n")
                n_dof_of=REAL(n_atoms_of*qtb%n_dim,wp)
                indices=qtb%element_dict%get(el//"/indices")

                open(newunit=u,file=output%working_directory//"/adQTB_gammas.last."//trim(el))
                DO iom=1,qtb%nom
                    omega = real(iom-1,wp) * qtb%domega 
                    if(qtb%adQTBr) then
                        gamma_r=0             
                        DO i=1,n_atoms_of
                            iel=indices(i)
                            gamma_r = gamma_r + SUM(qtb%gammar(iel,:,iom))/n_dof_of
                        ENDDO
                    endif
                    if(qtb%adQTBf) then
                        gamma_f=0             
                        DO i=1,n_atoms_of
                            iel=indices(i)
                            gamma_f = gamma_f + SUM(qtb%gammaf(iel,:,iom))/n_dof_of
                        ENDDO
                    endif
                    if(qtb%save_every_gamma)  write(qtb%gamma_io_unit(k),*) omega*cm1 , gamma_r, gamma_f                   
                    write(u,*) omega*cm1 , gamma_r, gamma_f
                ENDDO
                CLOSE(u)
                deallocate(indices)
            ENDDO

        else

            gamma_r=qtb%gamma0
            gamma_f=qtb%gamma0
            open(newunit=u,file=output%working_directory//"/adQTB_gammas.last")
            !    write(qtb%gamma_io_unit,*) "#gamma0",qtb%gamma0
            !   write(qtb%gamma_io_unit,*) "#omega     gamma_r     gamma_f"

            do iom = 1, qtb%nom
                omega = real(iom-1,wp) * qtb%domega 

                if(qtb%adQTBr) then
                    gamma_r = sum( qtb%gammar ( :,:,iom ) ) / real(qtb%n_dof,wp)
                    !gamma_f = sum( qtb%gammax ( :,:,iom ) ) / real(qtb%n_dof,wp)
                endif
                

                if(qtb%adQTBf) &
                    gamma_f = sum( qtb%gammaf ( :,:,iom ) ) / real(qtb%n_dof,wp)

                if(qtb%save_every_gamma) write(qtb%gamma_io_unit(1),*) omega*cm1 , gamma_r, gamma_f
                write(u,*) omega*cm1 , gamma_r, gamma_f

            enddo

            close(u)
        endif

    end subroutine write_gamma_adapt

    subroutine QTB_read_gammar(qtb,gammar_file)
        IMPLICIT NONE
        CLASS(QTB_type), INTENT(inout) :: qtb
        CHARACTER(*), INTENT(in) :: gammar_file
        INTEGER :: u,iostat,i,k,iom,j,iel,n_atoms_of
        REAL(wp) :: om
        LOGICAL :: file_exist
        CHARACTER(2) :: el
        CHARACTER(:), ALLOCATABLE :: filename
        REAL(wp) :: gammar0
        INTEGER, ALLOCATABLE :: indices(:)

        if(qtb%adapt_by_element) then
            DO k=1,qtb%n_elements
                el=qtb%element_set(k)
                filename=gammar_file//"."//trim(el)
                INQUIRE(file=filename,exist=file_exist)
                if(.NOT. file_exist) then
                    INQUIRE(file=output%working_directory//"/"//filename &
                                ,exist=file_exist)
                    if(.NOT. file_exist) then
                        write(0,*) "Error: specified gammar file '"//filename&
                                        &//"' does not exist!"
                        STOP 
                    else
                        open(newunit=u,file=output%working_directory//"/"//filename)
                        write(*,*) "QTB: recovering gammar from '"&
                                    &//output%working_directory//"/"//filename//"'"
                    endif
                else
                    open(newunit=u,file=filename)
                    write(*,*) "QTB: recovering gammar from '"&
                                    &//filename//"'"
                endif

                n_atoms_of=qtb%element_dict%get(el//"/n")
                indices=qtb%element_dict%get(el//"/indices")
                DO iom=1,qtb%nom
                    read(u,*) om,gammar0
                    DO i=1,n_atoms_of
                        iel=indices(i)
                        qtb%gammar(iel,:,iom)=gammar0
                    ENDDO
                ENDDO
                close(u) 
            ENDDO
        else
            INQUIRE(file=gammar_file,exist=file_exist)
            if(.NOT. file_exist) then
                INQUIRE(file=output%working_directory//"/"//gammar_file,exist=file_exist)
                if(.NOT. file_exist) then
                    STOP "Error: specified gammar_file does not exist!"
                else
                    open(newunit=u,file=output%working_directory//"/"//gammar_file)
                    write(*,*) "QTB: recovering gammar from '"&
                                    &//output%working_directory//"/"//gammar_file//"'"
                endif
            else
                open(newunit=u,file=gammar_file)
                write(*,*) "QTB: recovering gammar from '"&
                                    &//gammar_file//"'"
            endif
            DO i=1,qtb%nom
                read(u,*) om,gammar0
                qtb%gammar(:,:,i)=gammar0
            ENDDO 
            close(u)        
        endif

    end subroutine QTB_read_gammar

    ! subroutine QTB_read_gammax(qtb,gammar_file)
    !     IMPLICIT NONE
    !     CLASS(QTB_type), INTENT(inout) :: qtb
    !     CHARACTER(*), INTENT(in) :: gammar_file
    !     INTEGER :: u,iostat,i,k,iom,j,iel,n_atoms_of
    !     REAL(wp) :: om
    !     LOGICAL :: file_exist
    !     CHARACTER(2) :: el
    !     CHARACTER(:), ALLOCATABLE :: filename
    !     REAL(wp) :: gammar0,gammax0
    !     INTEGER, ALLOCATABLE :: indices(:)

    !     if(qtb%adapt_by_element) then
            
    !     else
    !         INQUIRE(file=gammar_file,exist=file_exist)
    !         if(.NOT. file_exist) then
    !             INQUIRE(file=output%working_directory//"/"//gammar_file,exist=file_exist)
    !             if(.NOT. file_exist) then
    !                 STOP "Error: specified gammax_file does not exist!"
    !             else
    !                 open(newunit=u,file=output%working_directory//"/"//gammar_file)
    !                 write(*,*) "QTB: recovering gammax from '"&
    !                                 &//output%working_directory//"/"//gammar_file//"'"
    !             endif
    !         else
    !             open(newunit=u,file=gammar_file)
    !             write(*,*) "QTB: recovering gammax from '"&
    !                                 &//gammar_file//"'"
    !         endif
    !         DO i=1,qtb%nom
    !             read(u,*) om,gammar0,gammax0
    !             qtb%gammax(:,:,i)=gammax0
    !         ENDDO 
    !         close(u)        
    !     endif

    ! end subroutine QTB_read_gammax


!----------------------------------------------------
! QTB ANALYSIS TOOLS
! Compute velocity autocorrelation and velocity random force cross-correlation
! spectra
! Mandatory for adQTB-r and highly recommended for QTB and adQTB-f 

    ! velocity storage
	subroutine QTB_store_velocities(qtb,momentum)
		implicit none
        CLASS(QTB_TYPE), INTENt(inout) :: qtb
        REAL(wp), INTENT(in) :: momentum(:,:)
        INTEGER             :: i 

		DO i=1,qtb%n_dim
		    qtb%veloc_store(:,i,qtb%selector) = momentum(:,i)/qtb%mass(:)
		ENDDO
 
	end subroutine QTB_store_velocities

    subroutine QTB_store_velocity_one_dof(qtb,momentum,at,dim)
		implicit none
        CLASS(QTB_TYPE), INTENt(inout) :: qtb
        REAL(wp), INTENT(in) :: momentum(:,:)
        INTEGER, INTENT(in) :: at, dim
        INTEGER             :: i 

		qtb%veloc_store(at,dim,qtb%selector) = momentum(at,dim)/qtb%mass(at)
 
	end subroutine QTB_store_velocity_one_dof


    !random force storage
    ! subroutine  QTB_store_randForce(qtb) 
    !     implicit none
    !     CLASS(QTB_TYPE), INTENt(inout) :: qtb

    !     FRand_store(:,:,qtb%selector) = qtb%Force(:,:) / qtb%dt       

    ! end subroutine QTB_store_randForce


	subroutine QTB_compute_vv_vf_ff_spec(qtb)
        implicit none
        CLASS(QTB_TYPE), INTENt(inout) :: qtb
        integer :: i, j, istep,jstep, ifail,count
        real(wp) :: fr(2*qtb%n_rand), fi(2*qtb%n_rand), &
                    vr(2*qtb%n_rand), vi(2*qtb%n_rand), &
                    v2r(2*qtb%n_rand), v2i(2*qtb%n_rand), &
                    work(2*qtb%n_rand)
        real(wp) :: norm
              
        IF(qtb%save_cvff) then
            qtb%vffspec=0._wp
            qtb%vvvspec=0._wp
        ENDIF
        norm=2._wp*qtb%dt
        do j = 1,qtb%n_dim ;do i=1,qtb%n_atoms
            
            fr = 0._wp
            fr(1:qtb%n_rand) = qtb%Force(i,j,:)
            fi(:) = 0._wp
            work(:) = 0._wp
            vr = 0._wp
            vr(1:qtb%n_rand) = qtb%veloc_store(i,j,:)
            vi(:) = 0._wp
            v2r = 0._wp
            v2r(1:qtb%n_rand) = qtb%veloc_store(i,j,:)**2
            v2i(:) = 0._wp
    
            ifail=0
            call C06FCF(fr,fi,2*qtb%n_rand,work,ifail)    ! FFT
            ifail=0
            call C06FCF(vr,vi,2*qtb%n_rand,work,ifail)    ! FFT	 
            ifail=0
            call C06FCF(v2r,v2i,2*qtb%n_rand,work,ifail)    ! FFT	 
    
    
            do istep = 1, qtb%nom
                qtb%vfspec(i,j,istep) = norm*(vr(istep)*fr(istep) + vi(istep)*fi(istep))
                qtb%v2fspec(i,j,istep) = norm*(v2r(istep)*fi(istep) - v2i(istep)*fr(istep))
                qtb%vvspec(i,j,istep) = norm*(vr(istep)**2 + vi(istep)**2)
                !qtb%vv2spec(i,j,istep) = norm*(v2r(istep)*fi(istep) - v2i(istep)*fr(istep))
                qtb%vv2spec(i,j,istep) = norm*(v2r(istep)*vi(istep) - v2i(istep)*vr(istep))
                qtb%ffspec(i,j,istep) = norm*(fr(istep)**2 + fi(istep)**2)

                ! qtb%vfspec(i,j,istep)  = ( qtb%vfspec(i,j,istep)	   &
                !         + 0.5_wp *qtb%dt * qtb%ffspec(i,j,istep) / qtb%mass(i) )    &
                !     / (1._wp + qtb%gamma0 * qtb%dt * 0.5_wp)
            enddo

            qtb%mcvvgammar(i,j,:) = qtb%mass(i)*qtb%vvspec(i,j,:)*qtb%gammar(i,j,:)

            IF(qtb%save_cvff) THEN
                count=0
                do jstep = 1, qtb%nom/2; do istep = jstep, qtb%nom-jstep
                    count=count+1
                    qtb%vffspec(count)=qtb%vffspec(count) + (&
                             fr(istep)*fr(jstep)*vi(istep+jstep-1) &
                            +fi(istep)*fi(jstep)*vr(istep+jstep-1) &
                        )/qtb%n_dof
                    qtb%vvvspec(count)=qtb%vvvspec(count) + (qtb%mass(i)*qtb%gamma0)**2*(&
                             vr(istep)*vr(jstep)*vi(istep+jstep-1) &
                            +vi(istep)*vi(jstep)*vr(istep+jstep-1) &
                        )/qtb%n_dof
                enddo ; enddo
            ENDIF

        enddo ; enddo        


    end subroutine QTB_compute_vv_vf_ff_spec


    subroutine QTB_average_vv_vf_ff_spec(qtb)
        implicit none   
        CLASS(QTB_TYPE), INTENT(inout) :: qtb

        qtb%i_traj=qtb%i_traj+1

        qtb%ffspec_aver = qtb%ffspec_aver &
            + (qtb%ffspec(:,:,:)-qtb%ffspec_aver)/REAL(qtb%i_traj,wp)
        qtb%vvspec_aver = qtb%vvspec_aver &
            + (qtb%vvspec(:,:,:)-qtb%vvspec_aver)/REAL(qtb%i_traj,wp)
        qtb%vfspec_aver = qtb%vfspec_aver &
            + (qtb%vfspec(:,:,:)-qtb%vfspec_aver)/REAL(qtb%i_traj,wp)
        qtb%mcvvgammar_aver = qtb%mcvvgammar_aver &
            + (qtb%mcvvgammar(:,:,:)-qtb%mcvvgammar_aver)/REAL(qtb%i_traj,wp)
        qtb%d_FDR_mean_aver = qtb%d_FDR_mean_aver &
            + (qtb%d_FDR_mean(:)-qtb%d_FDR_mean_aver)/REAL(qtb%i_traj,wp)
        
        qtb%vv2spec_aver = qtb%vv2spec_aver &
            + (qtb%vv2spec(:,:,:)-qtb%vv2spec_aver)/REAL(qtb%i_traj,wp)
        qtb%v2fspec_aver = qtb%v2fspec_aver &
            + (qtb%v2fspec(:,:,:)-qtb%v2fspec_aver)/REAL(qtb%i_traj,wp)
        
        IF(qtb%save_cvff) THEN
            qtb%vffspec_aver = qtb%vffspec_aver &
                + (qtb%vffspec-qtb%vffspec_aver)/REAL(qtb%i_traj,wp)
            qtb%vvvspec_aver = qtb%vvvspec_aver &
                + (qtb%vvvspec-qtb%vvvspec_aver)/REAL(qtb%i_traj,wp)
        ENDIF

   
	end subroutine QTB_average_vv_vf_ff_spec

   
    subroutine QTB_write_ffspec_aver(qtb,filename,restart_average)
        implicit none
        integer   :: l_step,istep,jstep,u,u1,i,j,k,n_atoms_of,iel,count
        real(wp)  :: omega,n_dof_of,om1,om2
        CLASS(QTB_TYPE), INTENT(inout) :: qtb
        LOGICAL, INTENT(in), OPTIONAL :: restart_average
        CHARACTER(*), INTENT(in) :: filename
        REAL(wp) ,allocatable :: vfspec(:),mcvvgammar(:),mcvv(:),mcvv_aver(:),mcvv2gammar(:)
        INTEGER, allocatable :: indices(:)
        LOGICAL :: restart_after_thermalization
        CHARACTER(2) :: el

        allocate(mcvv(qtb%nom))
        allocate(mcvv2gammar(qtb%nom))

        restart_after_thermalization=.FALSE.

        ! qtb%ffspec_aver = qtb%ffspec_aver/real(qtb%i_traj,wp)
        ! qtb%vvspec_aver = qtb%vvspec_aver/real(qtb%i_traj,wp)
        ! qtb%vfspec_aver = qtb%vfspec_aver/real(qtb%i_traj,wp)

        open(newunit=u, file=output%working_directory//'/'//trim(filename)//".mean")
        write(u,*) "#omega(cm^-1)    mCvv   mCvv*gammar    Cvf  dFDR  Cff   Cff_theo"        
        mCvv=0._wp
        mcvv2gammar=0
        DO l_step = 1, qtb%nom
            DO j=1,qtb%n_dim ; DO i=1,qtb%n_atoms
                mCvv(l_step)=mCvv(l_step)+qtb%mass(i)*qtb%vvspec_aver(i,j,l_step)
                mcvv2gammar(l_step)=mcvv2gammar(l_step)+qtb%mass(i)*qtb%vv2spec_aver(i,j,l_step)*qtb%gammar(i,j,l_step)
            ENDDO ; ENDDO
            omega = real(l_step-1,wp) * qtb%domega
            write(u,*) omega* cm1 , mCvv(l_step) / real(qtb%n_dof , wp) &            
            , sum(qtb%mcvvgammar_aver(:,:,l_step)) / real(qtb%n_dof, wp) &
            , sum(qtb%vfspec_aver(:,:,l_step)) / real(qtb%n_dof , wp) &
            , qtb%d_FDR_mean_aver(l_step) &
            , sum(qtb%ffspec_aver(:,:,l_step)) / real(qtb%n_dof, wp) &
            , qtb%ff_theo(omega, sum(qtb%mass/real(qtb%n_atoms , wp)))  &
            , sum(qtb%v2fspec_aver(:,:,l_step)) / real(qtb%n_dof, wp) &
            , mcvv2gammar(l_step) / real(qtb%n_dof , wp)
        ENDDO        
        close(u)

        open(newunit=u, file=output%working_directory//'/'//trim(filename)//".last")
        write(u,*) "#omega(cm^-1)    mCvv    mCvv*gammar    Cvf    dFDR"
        mCvv=0._wp
        mcvv2gammar=0
        DO l_step = 1, qtb%nom
            DO j=1,qtb%n_dim ; DO i=1,qtb%n_atoms
                mCvv(l_step)=mCvv(l_step)+qtb%mass(i)*qtb%vvspec(i,j,l_step)
                mcvv2gammar(l_step)=mcvv2gammar(l_step)+qtb%mass(i)*qtb%vv2spec(i,j,l_step)*qtb%gammar(i,j,l_step)
            ENDDO ; ENDDO
            omega = real(l_step-1,wp) * qtb%domega
            write(u,*) omega* cm1 , mCvv(l_step) / real(qtb%n_dof , wp) &            
            , sum(qtb%mcvvgammar(:,:,l_step)) / real(qtb%n_dof, wp) &
            , sum(qtb%vfspec(:,:,l_step)) / real(qtb%n_dof , wp) &
            , qtb%d_FDR_mean(l_step) &
            , sum(qtb%v2fspec(:,:,l_step)) / real(qtb%n_dof, wp) &
            , mcvv2gammar(l_step) / real(qtb%n_dof , wp)
        ENDDO        
        close(u)

        if(qtb%write_spectra_by_element .or. qtb%adapt_by_element) then
            allocate(mcvvgammar(qtb%nom) &
                    ,vfspec(qtb%nom))
            DO k=1,qtb%n_elements
                mcvvgammar=0._wp
                mcvv=0._wp
                vfspec=0._wp
                el=qtb%element_set(k)
                n_atoms_of=qtb%element_dict%get(el//"/n")
                n_dof_of=REAL(n_atoms_of*qtb%n_dim,wp)
                indices=qtb%element_dict%get(el//"/indices")
                DO i=1,n_atoms_of
                    iel=indices(i)
                    DO j=1,qtb%n_dim
                        mcvv(:)=mcvv(:)+qtb%mass(iel)*qtb%vvspec_aver(iel,j,:)
                        mcvvgammar(:)=mcvvgammar(:)+qtb%mcvvgammar_aver(iel,j,:)
                        vfspec(:)=vfspec(:)+qtb%vfspec_aver(iel,j,:)
                    ENDDO
                ENDDO  
                deallocate(indices)              
                open(newunit=u, file=output%working_directory//'/'//trim(filename)//"."//trim(el))
                write(u,*) "#omega(cm^-1)  mCvv    mCvv*gammar    Cvf"
                DO l_step = 1, qtb%nom
                    omega = real(l_step-1,wp) * qtb%domega
                    write(u,*) omega* cm1 ,  mcvv(l_step)/n_dof_of &
                                , mcvvgammar(l_step)/n_dof_of &
                                , vfspec(l_step)/n_dof_of
                ENDDO        
                close(u)
            ENDDO
        endif

        if(present(restart_average)) then
            if(restart_average) then
                qtb%ffspec_aver=0._wp
                qtb%vvspec_aver=0._wp
                qtb%vfspec_aver=0._wp
                qtb%mcvvgammar_aver=0._wp
                qtb%d_FDR_mean_aver=0._wp
                qtb%i_traj=0
                if(qtb%save_cvff) then
                    qtb%vffspec_aver=0
                    qtb%vvvspec_aver=0
                    qtb%counter_cvff=0
                endif

                qtb%vv2spec_aver=0._wp
                qtb%v2fspec_aver=0._wp
            endif
        endif

    end subroutine QTB_write_ffspec_aver

!----------------------------------------------------------

    subroutine compute_power_spectrum(qtb)
        IMPLICIT NONE
        CLASS(QTB_TYPE), intent(inout) :: qtb
        REAL(wp) :: P,du,u,u0, omegamin2,bead_contrib,u02
        REAL(wp) ,ALLOCATABLE :: u_range(:),uk_range(:,:), Fp(:) &
                                    ,Fp_prev(:),Fp0(:),targ(:),mu(:) &
                                    , err_it(:), err_w(:)!, u0k(:)
        REAL(wp), DIMENSION(:,:), ALLOCATABLE :: A,gmat,gmat_inv
        INTEGER :: i,k,unit,unit_err,nom
        LOGICAL :: converged,converged_weights

        if(qtb%noQTB) then  !QTB with white noise for TESTING purpose            
            converged_weights=.TRUE.
            if(qtb%PI_QTB)then
                qtb%power_spectrum(:)=qtb%n_beads*qtb%kBT
            else
                qtb%power_spectrum(:)=qtb%kBT
            endif
        else if(qtb%PI_QTB) then 
            P=real(qtb%n_beads,wp)            
            IF(qtb%centroid_only) THEN
                converged_weights=.TRUE.
                u0=(2*P*qtb%kBT)**2
                do i=1,qtb%nom
                    u=(i-1)*qtb%domega
                    qtb%power_spectrum(i)=P*QTB_theta(u,qtb%kBT)
                    DO k=1,qtb%n_beads-1
                        qtb%power_spectrum(i) = qtb%power_spectrum(i) &
                            - P*qtb%kBT*u*u/(u*u + u0*(sin(k*pi/P))**2 )
                    ENDDO
                enddo
            ELSEIF(qtb%beads_same_temperature) THEN
                converged_weights=.TRUE.
                omegamin2 = qtb%omK2(qtb%bead_index+1)
                do i=1,qtb%nom
                    u=(i-1)*qtb%domega
                    u02 = u*u - omegamin2
                    if(u02 >= 0) then
                        u0 = sqrt(u02)
                        bead_contrib=0._wp
                        DO k=1,qtb%n_beads
                            bead_contrib = bead_contrib &
                               + 1._wp/(P*(u02 + qtb%omK2(k)))
                        ENDDO
                        qtb%power_spectrum(i) = 1./(2*u0*tanh(0.5*u0/qtb%kBT)*bead_contrib)
                    else
                        qtb%power_spectrum(i)=P*qtb%kBT
                    endif
                enddo
            ELSE
                !PI-QTB ITERATIVE SCHEME (cf. Fabien Brieuc)
                du=qtb%domega/(2*P*qtb%kBT)
                if(qtb%OTF_generation) then
                    nom=qtb%nom
                else
                    nom=nint(qtb%nom/10._wp)
                    du=10._wp*du
                endif
                write(*,*) "nom=",nom

                allocate(u_range(nom),Fp(nom),Fp0(nom),Fp_prev(nom),targ(nom))
                allocate(uk_range(nom,0:qtb%n_beads-1))!,u0k(0:qtb%n_beads-1))

                !initialize u_range and first guess for Fp
                do  i=1,nom
                    u=(i-1)*du
                    u_range(i)=u
                    do k=0,qtb%n_beads-1
                        uk_range(i,k)=SQRT(u**2 + (sin(k*pi/P))**2)
                    enddo
                    !write(0,*) u*(2*P*qtb%kBT)*THz/(2*pi),uk_range(i,:)*THz/(2*pi)*(2*P*qtb%kBT)
                enddo
                !INITIAL GUESS
                Fp0(:)=h(u_range(:))
                Fp=Fp0

                targ(:)=qtb%kernel_mixing*h(P*u_range(:))
                write(*,*) "PI-QTB: sigma=",qtb%kernel_sigma,"*u_range(qtb%nom)"
                qtb%kernel_sigma=qtb%kernel_sigma*u_range(nom)
                allocate(qtb%kernel_weights(nom))
                allocate(A(nom,nom) &
                        ,gmat(nom,nom) &
                        ,gmat_inv(nom,nom) &
                        ,mu(nom), err_it(nom), err_w(nom) )
                call compute_intermediate_matrices(qtb,A,gmat,gmat_inv,mu,uk_range,nom)
                qtb%kernel_weights=0._wp
                mu=matmul(gmat_inv, h(P*u_range)-mu)
                A=matmul(gmat_inv,A)

            ! n_ex=max(2,nint(0.2*qtb%nom))
                converged=.FALSE.
                converged_weights=.FALSE.
                do i=1,qtb%kernel_max_iter
                ! call linear_fit(u_range(qtb%nom-n_ex+1:),Fp(qtb%nom-n_ex+1:),0.0001_wp,beta)
                    if(.not. converged) then
                        Fp_prev=Fp
                        Fp=h(u_range*P)
                        DO k=1,qtb%n_beads-1
                            Fp(:)=Fp(:)-(u_range(:)/uk_range(:,k))**2 &
                                *linear_interpolation(uk_range(:,k),Fp_prev,u_range,prop_ex=0.05)
                        ENDDO
                        Fp=Fp*qtb%kernel_mixing + (1._wp-qtb%kernel_mixing)*Fp_prev
                        err_it=get_kernel_errors_iter(qtb,uk_range,Fp,nom)
                        !Fp=kernel_iteration(Fp_prev,u_range,uk_range,qtb%kernel_mixing,qtb%n_beads)
                        if( maxval(abs(Fp-Fp_prev)/targ) <= qtb%kernel_iter_prec ) then
                            write(*,*) "PI-QTB: kernel converged in",i,"iterations."
                            converged=.TRUE.
                        ! exit
                        endif
                    endif
                    if(.not. converged_weights) then
                        DO k=1,1000
                            qtb%kernel_weights=qtb%kernel_weights &
                                            +qtb%kernel_mixing*(mu-matmul(A,qtb%kernel_weights))
                        ENDDO
                        err_w=get_kernel_errors_weights(qtb,uk_range,nom)
                        if( maxval(abs(err_w)) <= qtb%kernel_iter_prec ) then
                            write(*,*) "PI-QTB: kernel weigths converged in",i*1000,"iterations."
                            converged_weights=.TRUE.
                        ! exit
                        endif
                    endif
                    if(converged .AND. converged_weights) exit           
                enddo
                !qtb%power_spectrum(:)=P*qtb%KBT*linear_interpolation(u_range(:)*sqrt(P),Fp,u_range,prop_ex=0.05)
                ! qtb%power_spectrum(:)=P*qtb%KBT*Fp(:)
                ! allocate(u_full(qtb%nom))
                ! do  i=1,qtb%nom
                !     u_full(i)=i*qtb%domega/(2*P*qtb%kBT)
                ! enddo
                Fp0=predict_F(qtb,u_range,u_range)
                qtb%power_spectrum(:)=P*qtb%KBT*predict_F(qtb &
                    ,(/ ((i-1)*qtb%domega/(2*P*qtb%kBT), i=1,qtb%nom) /),u_range)

                !COMPUTE KINETIC ENERGY HARMONIC OSCILLATOR
                ! u0=100._wp*(2._wp*pi/THz)/(2*P*qtb%kBT)
                ! do k=0,qtb%n_beads-1
                !     u0k(k)=SQRT(u0**2 + (sin(k*pi/P))**2)
                ! enddo
                !write(*,*) "Ec_100THz", qtb%n_beads ,0.5*qtb%kBT*sum(predict_F(qtb,u0k,u_range)*(u0/u0k(:))**2)

                open(newunit=unit,file=output%working_directory//"/PIQTB_kernel.out")
                open(newunit=unit_err,file=output%working_directory//"/PIQTB_kernel_errors.out")
                do  i=1,nom
                    u=(i-1)*du*(2*P*qtb%kBT)
                    write(unit,*) u,P*qtb%kBT*Fp0(i)*kelvin, P*qtb%kBT*Fp(i)*kelvin
                    write(unit_err,*) u,err_w(i),err_it(i)
                enddo
                write(*,*) "PI-QTB: kernel mean_errors=",sqrt(sum(err_w**2))/nom, sqrt(sum(err_it**2))/nom
                close(unit)
                close(unit_err)
            ENDIF
        else !STANDARD QTB
            do i=1,qtb%nom
                u=(i-1)*qtb%domega
                qtb%power_spectrum(i)=QTB_theta(u,qtb%kBT)
            enddo
            converged_weights=.TRUE.
        endif

        if(.not. converged_weights) then
            write(0,*) "Error: could not converge QTB kernel in ",qtb%kernel_max_iter,"iterations !"
            STOP "Execution stopped."
        endif

    end subroutine compute_power_spectrum

    subroutine compute_intermediate_matrices(qtb,A,gmat,gmat_inv,mu,uk,nom)
        IMPLICIT NONE
        CLASS(QTB_TYPE), intent(inout) :: qtb
        INTEGER, INTENT(in) :: nom
        REAL(wp), DIMENSION(:,:), INTENT(INOUT) :: A,gmat,gmat_inv
        REAL(wp), DIMENSION(:), INTENT(INOUT) :: mu
        REAL(wp), INTENT(in) :: uk(nom,0:qtb%n_beads-1)
        INTEGER :: k, INFO,i
        REAL(wp), allocatable :: gk(:,:),v(:),Id(:,:)
        REAL(wp) :: lamb

        allocate(Id(nom,nom))
        Id=0
        lamb=0.0001
        do i=1,nom ; Id(i,i)=1 ; enddo
        gmat=gauss_kernel(uk(:,0),uk(:,0),qtb%kernel_sigma)
        gmat_inv=sym_mat_inverse(gmat+lamb*Id,INFO)
        if(INFO/=0) &
            STOP "Error: could not inverse gaussian kernel matrix"
        write(*,*) "PI-QTB: gaussian kernel inversion error:",sum((matmul(gmat,gmat_inv)-Id)**2)
        
        allocate(gk(nom,nom),v(nom))
        mu=0._wp
        A=0._wp
        do k=0,qtb%n_beads-1
            gk=gauss_kernel(uk(:,k),uk(:,0),qtb%kernel_sigma)
            v=(uk(:,0)/uk(:,k))**2
            mu=mu+h(uk(:,k))*v(:)
            DO i=1,nom 
                A(i,:)=A(i,:)+gk(i,:)*v(i)
            ENDDO
        enddo

    end subroutine compute_intermediate_matrices

    function predict_F(qtb,u_n,u) result(F)
        CLASS(QTB_TYPE), intent(inout) :: qtb
        REAL(wp), INTENT(in) :: u(:),u_n(:)
        REAL(wp) :: F(size(u_n)), g(size(u_n),size(u))

        g=gauss_kernel(u_n,u,qtb%kernel_sigma)
        F=matmul(g,qtb%kernel_weights)+h(u_n)

    end function predict_F

    function gauss_kernel(u,v,width) result(g)
        IMPLICIT NONE
        REAL(wp), INTENT(in) :: u(:), v(:), width
        REAL(wp) :: g(size(u),size(v))
        INTEGER :: i,j

        do i=1,size(u) ; do j=1,size(v)
            g(i,j)=u(i)-v(j)            
        enddo ; enddo
        g=exp(-g**2/(2._wp*width**2))

    end function gauss_kernel

    function get_kernel_errors_weights(qtb,uk,nom) result(error)
        IMPLICIT NONE
        CLASS(QTB_TYPE), intent(inout) :: qtb
        INTEGER, INTENT(in) :: nom
        REAL(wp), INTENT(in) :: uk(nom,0:qtb%n_beads-1)
        REAL(wp) :: error(nom),F(nom,0:qtb%n_beads-1),hp(nom)
        INTEGER :: k ,iom

        DO k=0,qtb%n_beads-1
            F(:,k)=predict_F(qtb,uk(:,k),uk(:,0))
        ENDDO
        hp=h(real(qtb%n_beads,wp)*uk(:,0))

        DO iom=1,nom
            error(iom)=( hp(iom)-sum(F(iom,:)*(uk(iom,0)/uk(iom,:))**2) )/hp(iom)
        ENDDO        

    end function get_kernel_errors_weights

    function get_kernel_errors_iter(qtb,uk,F0,nom) result(error)
        IMPLICIT NONE
        CLASS(QTB_TYPE), intent(inout) :: qtb
        INTEGER, INTENT(in) :: nom
        REAL(wp), INTENT(in) :: uk(nom,0:qtb%n_beads-1),F0(:)
        REAL(wp) :: error(nom),F(nom,0:qtb%n_beads-1),hp(nom)
        INTEGER :: k ,iom

        DO k=0,qtb%n_beads-1
            F(:,k)=linear_interpolation(uk(:,k),F0,uk(:,0),prop_ex=0.05)
        ENDDO
        hp=h(real(qtb%n_beads,wp)*uk(:,0))

        DO iom=1,nom
            error(iom)=( hp(iom)-sum(F(iom,:)*(uk(iom,0)/uk(iom,:))**2) )/hp(iom)
        ENDDO        

    end function get_kernel_errors_iter

    function h(u)
        IMPLICIT NONE
        REAL(wp), intent(in) :: u(:)
        REAL(wp) :: h(size(u))
        integer :: i

        DO i=1,size(u)
            if(u(i)<=TINY(u(i))) then
                h=1._wp
            else
                h=u(i)/tanh(u(i))
            endif
        ENDDO
        

    end function h

    ! function kernel_iteration(Fp,u_range,uk_range,alpha,n_beads,beta) result(Fp_new)
    !     IMPLICIT NONE
    !     REAL(wp),intent(in):: Fp(:), u_range(:),uk_range(:,:), alpha
    !     REAL(wp), intent(in), OPTIONAL :: beta(2)
    !     INTEGER, intent(in) :: n_beads
    !     REAL(wp) :: Fp_new(size(Fp))
    !     INTEGER :: i,k
    !     REAL(wp) :: u,uk,P

    !     P=real(n_beads,wp)

    !     do i=1,size(Fp)
    !         u=u_range(i)
    !         Fp_new(i)=h(u)
    !         do k=1,n_beads-1
    !             uk=uk_range(i,k)
    !             Fp_new(i)=Fp_new(i)-((u/uk)**2)*linear_interpolation(uk*sqrt(P),Fp,u_range,prop_ex=0.05)
    !         enddo
    !       !  Fp_new(i)=Fp_new(i)*alpha/P + (1-alpha)*Fp(i)
    !     enddo

    !     !MIXING
    !     Fp_new=Fp_new*alpha/P + (1-alpha)*Fp

    ! end function kernel_iteration

!-------------------------------------------------------

    subroutine QTB_deallocate(qtb)
        IMPLICIT NONE
        CLASS(QTB_TYPE) :: qtb

        ! Deallocation of QTB variables
        IF ( allocated(qtb%Force) ) deallocate(qtb%Force)
       ! IF ( allocated(qtb%Forcex) ) deallocate(qtb%Forcex)
        IF ( allocated(qtb%gammar) ) deallocate(qtb%gammar)
       ! IF ( allocated(qtb%gammax) ) deallocate(qtb%gammax)
        IF ( allocated(qtb%power_spectrum) ) deallocate(qtb%power_spectrum)


        if ( qtb%adQTBr .OR. qtb%adQTBf) then
            IF ( allocated(qtb%d_FDR) ) deallocate( qtb%d_FDR )
            IF ( allocated( qtb%d_FDR_typ )) deallocate( qtb%d_FDR_typ )            
        endif

        if  (qtb%save_average_spectra) then
            IF ( allocated( qtb%d_FDR_mean )) deallocate( qtb%d_FDR_mean )
            IF ( allocated( qtb%d_FDR_mean_aver )) deallocate( qtb%d_FDR_mean_aver )
        endif
  
        ! Deallocation of adQTB-f variables 
        if ( qtb%adQTBf ) then
            IF ( allocated(qtb%gammaf) ) deallocate( qtb%gammaf )
            ! IF ( allocated(qtb%d_FDR) ) deallocate( qtb%d_FDR )
            ! IF ( allocated( qtb%d_FDR_typ ) ) deallocate( qtb%d_FDR_typ )
            IF ( allocated( qtb%veloc_aux ) ) deallocate( qtb%veloc_aux )
            IF ( allocated( qtb%QTBF_aux ) ) deallocate(qtb%QTBF_aux )
            IF ( allocated( qtb%moment_mem_kern ) ) deallocate( qtb%moment_mem_kern )
        endif

        IF ( qtb%OTF_generation ) THEN
            IF(allocated(qtb%coloration_kernel)) deallocate( qtb%coloration_kernel )
            IF(allocated(qtb%white_memory)) deallocate( qtb%white_memory )
            IF(allocated(qtb%OTF_current)) deallocate( qtb%OTF_current )
        ENDIF

        !QTB ANALYSIS
        if  (qtb%save_average_spectra) then
            IF ( allocated(qtb%vfspec) ) deallocate( qtb%vfspec )
            IF ( allocated(qtb%vvspec) ) deallocate( qtb%vvspec )
            IF ( allocated(qtb%ffspec) ) deallocate( qtb%ffspec )
            IF ( allocated(qtb%mcvvgammar) ) deallocate( qtb%mcvvgammar )
            ! IF ( allocated(qtb%FRand_store) ) deallocate( qtb%FRand_store )
            IF ( allocated(qtb%veloc_store) ) deallocate( qtb%veloc_store )  
            IF ( allocated(qtb%ffspec_aver) ) deallocate( qtb%ffspec_aver )
            IF ( allocated(qtb%vfspec_aver) ) deallocate( qtb%vfspec_aver )
            IF ( allocated(qtb%vvspec_aver) ) deallocate( qtb%vvspec_aver )
            IF ( allocated(qtb%mcvvgammar_aver) ) deallocate( qtb%mcvvgammar_aver )

            IF ( allocated(qtb%v2fspec_aver) ) deallocate( qtb%v2fspec_aver )
            IF ( allocated(qtb%vv2spec_aver) ) deallocate( qtb%vv2spec_aver )

            IF (qtb%save_cvff) then
                IF ( allocated(qtb%vffspec) ) deallocate( qtb%vffspec )
                IF ( allocated(qtb%vffspec_aver) ) deallocate( qtb%vffspec_aver )
                IF ( allocated(qtb%vvvspec) ) deallocate( qtb%vvvspec )
                IF ( allocated(qtb%vvvspec_aver) ) deallocate( qtb%vvvspec_aver )
            ENDIF
        endif

    end subroutine QTB_deallocate

    subroutine QTB_allocate(qtb)
        IMPLICIT NONE
        CLASS(QTB_TYPE) :: qtb

        call qtb%deallocate()

        ! Allocate mandatory QTB Variables
        allocate( qtb%Force(qtb%n_atoms,qtb%n_dim,qtb%n_rand))
        allocate( qtb%gammar(qtb%n_atoms,qtb%n_dim,qtb%nom))
       ! allocate( qtb%Forcex(qtb%n_atoms,qtb%n_dim,qtb%n_rand))
       ! allocate( qtb%gammax(qtb%n_atoms,qtb%n_dim,qtb%nom))
        allocate( qtb%power_spectrum(qtb%nom) )

        !qtb%gammax=0

       
        ! Allocate adQTB-r variables
		if  (qtb%adQTBr .OR. qtb%adQTBf) then
            allocate( qtb%d_FDR(qtb%nom) )
            allocate( qtb%d_FDR_typ(qtb%n_atoms,qtb%n_dim) )
            qtb%d_FDR_typ(:,:) = 0._wp
		endif

        if  (qtb%save_average_spectra) then
            allocate( qtb%d_FDR_mean(qtb%nom) )
            allocate( qtb%d_FDR_mean_aver(qtb%nom) )
            qtb%d_FDR_mean(:) = 0._wp
            qtb%d_FDR_mean_aver(:) = 0._wp
        endif

       ! Allocate adQTB-f variables
       if  ( qtb%adQTBf ) then  
            allocate( qtb%gammaf(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            ! allocate( qtb%d_FDR(qtb%nom) )
            allocate( qtb%moment_mem_kern(qtb%n_atoms,qtb%n_dim) ) 
            allocate( qtb%veloc_aux(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            allocate( qtb%QTBF_aux(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            ! allocate( qtb%d_FDR_typ(qtb%n_atoms,qtb%n_dim) )
            allocate( qtb%d_FDR_typ_prev(qtb%n_atoms,qtb%n_dim) )

            qtb%gammaf(:,:,:) = 0._wp
            qtb%moment_mem_kern(:,:) = 0._wp
            qtb%veloc_aux(:,:,:) = 0._wp
            qtb%QTBF_aux(:,:,:) = 0._wp
            qtb%d_FDR_typ(:,:) = 0._wp
            qtb%d_FDR_typ_prev(:,:) = 0._wp
		endif

        IF ( qtb%OTF_generation ) THEN
            allocate( qtb%coloration_kernel(qtb%n_atoms,qtb%n_dim,2*qtb%n_OTF) )
            allocate( qtb%white_memory(qtb%n_atoms,qtb%n_dim,2*qtb%n_OTF) )
            allocate( qtb%OTF_current(qtb%n_atoms,qtb%n_dim) )
        ENDIF

        !QTB analysis
        if  (qtb%save_average_spectra) then
            allocate( qtb%vfspec(qtb%n_atoms,qtb%n_dim,qtb%nom) )
           
            allocate( qtb%vvspec(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            allocate( qtb%ffspec(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            allocate( qtb%mcvvgammar(qtb%n_atoms,qtb%n_dim,qtb%nom)  )
            ! allocate( qtb%FRand_store(qtb%n_atoms,qtb%n_dim,qtb%n_rand) )
            allocate( qtb%veloc_store(qtb%n_atoms,qtb%n_dim,qtb%n_rand) )
            allocate( qtb%vfspec_aver(qtb%n_atoms,qtb%n_dim,qtb%nom)  )
            allocate( qtb%vvspec_aver(qtb%n_atoms,qtb%n_dim,qtb%nom)  )
            allocate( qtb%ffspec_aver(qtb%n_atoms,qtb%n_dim,qtb%nom)  )
            allocate( qtb%mcvvgammar_aver(qtb%n_atoms,qtb%n_dim,qtb%nom)  )
            qtb%vfspec_aver(:,:,:) = 0._wp
            qtb%vvspec_aver(:,:,:) = 0._wp
            qtb%ffspec_aver(:,:,:) = 0._wp
            qtb%mcvvgammar_aver(:,:,:) = 0._wp

            allocate( qtb%v2fspec(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            allocate( qtb%vv2spec(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            allocate( qtb%v2fspec_aver(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            allocate( qtb%vv2spec_aver(qtb%n_atoms,qtb%n_dim,qtb%nom) )
            qtb%v2fspec_aver(:,:,:) = 0._wp
            qtb%vv2spec_aver(:,:,:) = 0._wp
            
        endif

    end subroutine QTB_allocate

    subroutine QTB_initialize_cvff_cvvv(qtb)
        IMPLICIT NONE
        CLASS(QTB_TYPE) :: qtb
        INTEGER :: i,j,N

        if(allocated(qtb%vffspec)) deallocate(qtb%vffspec)
        if(allocated(qtb%vffspec_aver)) deallocate(qtb%vffspec_aver)
        if(allocated(qtb%vvvspec)) deallocate(qtb%vvvspec)
        if(allocated(qtb%vvvspec_aver)) deallocate(qtb%vvvspec_aver)

        N=0
        DO j=1,qtb%nom/2; do i=j,qtb%nom-j
            N=N+1
        ENDDO ; ENDDO

        allocate(qtb%vffspec(N))
        allocate(qtb%vffspec_aver(N))
        allocate(qtb%vvvspec(N))
        allocate(qtb%vvvspec_aver(N))

        qtb%vffspec=0
        qtb%vffspec_aver=0
        qtb%vvvspec=0
        qtb%vvvspec_aver=0

    end subroutine QTB_initialize_cvff_cvvv

    subroutine QTB_write_cvff_cvvv(qtb)
        IMPLICIT NONE
        CLASS(QTB_TYPE) :: qtb
        INTEGER :: u,u1,jstep,istep,count
        REAL(wp) :: om1,om2

        qtb%counter_cvff =  qtb%counter_cvff +1
        if(qtb%counter_cvff>=qtb%stride_cvff) then
            qtb%counter_cvff=0
            open(newunit=u, file=output%working_directory//'/QTB_cvvv_cvff.mean') 
            count=0
            DO jstep=1,qtb%nom/2
                om2=real(jstep-1,wp)*qtb%domega* cm1
                DO istep=jstep,qtb%nom-jstep
                    count=count+1
                    om1=real(istep-1,wp)*qtb%domega* cm1
                    write(u,*) om1,om2,qtb%vvvspec_aver(count),qtb%vffspec_aver(count)
                ENDDO
                write(u,*)
            ENDDO
            close(u)
        ENDIF

    end subroutine QTB_write_cvff_cvvv

END MODULE qtb_types
