MODULE pi_lgv_thermostat
    USE kinds
    USE random
    USE rpmd_types
    USE nested_dictionaries
    IMPLICIT NONE

    TYPE PI_LGV_TYPE

        REAL(wp) :: gamma0

        REAL(wp), allocatable :: gamma(:)
		REAL(wp), allocatable :: sigma(:,:)
        REAL(wp),allocatable :: gamma_exp(:)

        LOGICAL :: is_TRPMD,fast_forward
				REAL(wp) :: TRPMD_lambda
				

    END TYPE PI_LGV_TYPE

    TYPE(PI_LGV_TYPE) :: lgv

    PRIVATE
    PUBLIC :: PI_LGV_TYPE, pi_lgv_initialize, apply_pi_lgv_thermostat,pi_lgv_get_evolved_P &
                ,pi_lgv_finalize, pi_lgv_end_thermalization,apply_centroid_flip_ffl

CONTAINS

    function pi_lgv_get_evolved_P(system,parameters,at,dim) result(EigP)
        IMPLICIT NONE
        CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
        INTEGER, INTENT(in) :: at, dim
        REAL(wp) :: EigP(system%n_beads)
        REAL(wp) :: R(system%n_beads)

		!if(.not. allocated(R)) allocate(R(system%n_beads))
	
		call randGaussN(R)
		EigP=system%EigP(:,at,dim)*lgv%gamma_exp(:) &
				+ lgv%sigma(:,at)*R(:)

		end function pi_lgv_get_evolved_P
		
		subroutine apply_centroid_flip_ffl(polymer,Pc0)
			implicit none
			CLASS(POLYMER_TYPE), intent(inout) :: polymer
			REAL(wp), INTENT(in) :: Pc0(:,:)
			REAL(wp) :: norm0,norm_new

			norm0=NORM2(Pc0(:,:))
			norm_new=NORM2(polymer%EigP(1,:,:))

			polymer%EigP(1,:,:)=norm_new*Pc0(:,:)/norm0

		end subroutine apply_centroid_flip_ffl

    subroutine apply_pi_lgv_thermostat(polymer,parameters)
		implicit none
		CLASS(POLYMER_TYPE), intent(inout) :: polymer
		CLASS(RPMD_PARAM), intent(inout) :: parameters
		INTEGER :: i,j,k
		REAL(wp), allocatable :: R(:),Pc0(:,:)
		REAL(wp) :: norm0,norm_new
				

		!if(.not. allocated(R)) allocate(R(polymer%n_beads))

		!$OMP PARALLEL private(k,j,R)
		allocate(R(polymer%n_beads),Pc0(polymer%n_atoms,polymer%n_dim))
		!$OMP DO SCHEDULE(static) COLLAPSE(2)
		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms

			polymer%EigP(:,j,k)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
			Pc0(j,k)=polymer%EigP(1,j,k)

			call RandGaussN(R)
			polymer%EigP(:,j,k)=polymer%EigP(:,j,k)*lgv%gamma_exp(:) &
									+ lgv%sigma(:,j)*R(:)

		!	polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))

		enddo ; enddo
		!$OMP END DO
		!$OMP END PARALLEL

		if(lgv%fast_forward) call apply_centroid_flip_ffl(polymer,Pc0)

		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms
		  polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))
		enddo ; enddo
		
	end subroutine apply_pi_lgv_thermostat

    subroutine pi_lgv_initialize(system,parameters,param_library)
        IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
        INTEGER :: nu, i,j

        nu=system%n_beads

        lgv%gamma0=param_library%get("THERMOSTAT/damping")
		!! @input_file THERMOSTAT/damping (pi_lgv_thermostat)
		if(lgv%gamma0<=0) STOP "Error: 'damping' is not properly defined!"

		lgv%is_TRPMD=param_library%get("THERMOSTAT/trpmd",default=.TRUE.)
		lgv%fast_forward=param_library%get("THERMOSTAT/fast_forward_langevin",default=.FALSE.)
		!! @input_file THERMOSTAT/trpmd (pi_lgv_thermostat, default=.TRUE.)
		lgv%TRPMD_lambda=param_library%get("THERMOSTAT/trpmd_lambda",default=1._wp)
		!! @input_file THERMOSTAT/trpmd_lambda (pi_lgv_thermostat, default=1.)

		allocate(lgv%gamma(nu))
		if(lgv%is_TRPMD) then
			write(*,*) "TRPMD thermostat"		
			do i=1,nu
				lgv%gamma(i)=max(lgv%TRPMD_lambda*parameters%Omk(i) &
										,lgv%gamma0)
			enddo		
		else
			write(*,*) "standard RPMD thermostat"
			lgv%gamma(:)=lgv%gamma0
		endif

		allocate(lgv%gamma_exp(nu))
		lgv%gamma_exp(:)=exp(-lgv%gamma(:)*parameters%dt)
					
		allocate(lgv%sigma(nu,system%n_atoms))
		do j=1,system%n_atoms
			lgv%sigma(:,j)=sqrt( system%mass(j) &
						*(1-exp(-2*lgv%gamma(:)*parameters%dt)) )
		enddo
		if(parameters%algorithm=="mean_force") then
			lgv%sigma=lgv%sigma/sqrt(parameters%beta)
		else
			lgv%sigma=lgv%sigma/sqrt(parameters%dBeta)
		endif

    end subroutine pi_lgv_initialize

	subroutine pi_lgv_finalize()
        IMPLICIT NONE        
		!!!TO DO!!!
    end subroutine pi_lgv_finalize

	subroutine pi_lgv_end_thermalization()
        IMPLICIT NONE        
		!!!TO DO!!!
    end subroutine pi_lgv_end_thermalization

END MODULE pi_lgv_thermostat