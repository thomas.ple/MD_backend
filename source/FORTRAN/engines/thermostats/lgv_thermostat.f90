MODULE lgv_thermostat
    USE kinds
    USE random
    USE classical_md_types
    USE nested_dictionaries
    IMPLICIT NONE

    TYPE LGV_TYPE

        REAL(wp) :: gamma0

		REAL(wp), allocatable :: sigma(:)
        REAL(wp) :: gamma_exp

    END TYPE LGV_TYPE

    TYPE(LGV_TYPE) :: lgv

    PRIVATE
    PUBLIC :: LGV_TYPE, LGV_initialize, apply_lgv_thermostat &
                , lgv_finalize, lgv_end_thermalization

CONTAINS

    subroutine apply_lgv_thermostat(system,parameters)
        IMPLICIT NONE
        CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters

        REAL(wp), allocatable, save :: R(:)
		INTEGER :: i

		if(.not. allocated(R)) allocate(R(system%n_atoms))

		do i=1,system%n_dim		
			call randGaussN(R)
			system%P(:,i)=lgv%gamma_exp*system%P(:,i) &
							+ lgv%sigma(:)*R(:)
		enddo

    end subroutine apply_lgv_thermostat

    subroutine lgv_initialize(system,parameters,param_library)
        IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
        LOGICAL :: mass_normalized

        mass_normalized = param_library%get("THERMOSTAT/mass_normalized",default=.FALSE.)

        lgv%gamma0=param_library%get("THERMOSTAT/damping")
        !! @input_file THERMOSTAT/damping (lgv_thermostat)
		if(lgv%gamma0<0) STOP "Error: 'damping' is not properly defined!"
        allocate(lgv%sigma(system%n_atoms))

        SELECT CASE(parameters%integrator_type)
        CASE("baoab", "obabo","aboba")
            lgv%sigma(:)=SQRT( system%mass(:)*parameters%temperature* &
				( 1._wp-exp(-2._wp*lgv%gamma0*parameters%dt) ) )
            lgv%gamma_exp=exp(-lgv%gamma0*parameters%dt)
        CASE("stochastic_verlet")
            lgv%sigma(:)=SQRT( system%mass(:)*parameters%temperature* &
				( 1._wp-exp(-2._wp*lgv%gamma0*parameters%dt/2.) ) )
            lgv%gamma_exp=exp(-lgv%gamma0*parameters%dt/2.)
        CASE DEFAULT
            lgv%sigma(:)=SQRT(system%mass(:)*parameters%temperature*lgv%gamma0*parameters%dt)
        END SELECT

        if(mass_normalized) lgv%sigma(:)=lgv%sigma(:)/SQRT(system%mass(:))

    end subroutine lgv_initialize

    subroutine lgv_finalize()
        IMPLICIT NONE        
		!!!TO DO!!!
    end subroutine lgv_finalize

    subroutine lgv_end_thermalization()
        IMPLICIT NONE        
		!!!TO DO!!!
    end subroutine lgv_end_thermalization

END MODULE lgv_thermostat