MODULE analytic_gwild_1d
  USE kinds
  USE classical_md_types
  USE classical_md
  USE string_operations
  USE random
  IMPLICIT NONE

  TYPE, EXTENDS(CLASS_MD_SYS) :: AGWILD_SYS
    REAL(wp) :: k2,W,dU,dk2,Pmod
    REAL(wp), ALLOCATABLE :: k2_params(:), dU_params(:)
    INTEGER :: k2_poly_order, dU_poly_order
    REAL(wp) :: Qinv,dQinv,Q
  END TYPE

  TYPE, EXTENDS(CLASS_MD_PARAM) :: AGWILD_PARAM
    REAL(wp) :: lth2
    REAL(wp) :: gamma0, gexp,gsigma
    LOGICAL :: apply_p2_force
  END TYPE

  ABSTRACT INTERFACE
		subroutine agwild_sub(system,parameters)
			import :: AGWILD_SYS, AGWILD_PARAM
			IMPLICIT NONE
			CLASS(AGWILD_SYS), intent(inout) :: system
			CLASS(AGWILD_PARAM), intent(inout) :: parameters
		end subroutine agwild_sub
	END INTERFACE

  TYPE(AGWILD_SYS), save, target :: agwild_system
	TYPE(AGWILD_PARAM), save, target :: agwild_parameters

  PROCEDURE(agwild_sub), pointer :: agwild_integrator

	PRIVATE
  PUBLIC :: initialize_agwild

CONTAINS

  subroutine agwild_loop()
		IMPLICIT NONE
    INTEGER :: i

    if(agwild_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",agwild_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,agwild_parameters%n_steps_therm

        call agwild_integrator(agwild_system,agwild_parameters)

        if(agwild_parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(agwild_system,agwild_parameters)
				call classical_md_print_system_info(agwild_system,agwild_parameters,i)
			ENDDO
			write(*,*) "Thermalization done."
    ENDIF

    write(*,*) "Starting ",agwild_parameters%n_steps_therm," steps &
						&of Analytic WiLD"
			DO i=1,agwild_parameters%n_steps
        agwild_system%time=agwild_system%time &
								+agwild_parameters%dt

        call agwild_integrator(agwild_system,agwild_parameters)
        
        if(agwild_parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(agwild_system,agwild_parameters)
				call classical_md_print_system_info(agwild_system,agwild_parameters,i)

        ! WRITE TRAJECTORY
			  call output%write_all()
			ENDDO

      write(*,*) "Simulation done."    

  end subroutine agwild_loop

  subroutine initialize_agwild(param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library

    agwild_parameters%engine="agwild"

		!ASSOCIATE classical_MD_loop
		simulation_loop => agwild_loop

		!GET ALGORITHM
		agwild_parameters%algorithm="nvt"
    call param_library%store("THERMOSTAT/mass_normalized",.TRUE.)

		! GET PARAMETERS
		call classical_MD_get_parameters(agwild_system,agwild_parameters,param_library)

    if(agwild_system%n_dof /= 1 ) STOP "Error: agwild only implemented for n_dof=1"

    agwild_parameters%lth2 = 1./(agwild_system%mass(1)*agwild_parameters%temperature)
		call classical_MD_initialize_momenta(agwild_system,agwild_parameters)    

    call agwild_initialize_forces(agwild_system,agwild_parameters,param_library)

    call agwild_initialize_integrator(agwild_system,agwild_parameters,param_library)
		
		call classical_MD_initialize_output(agwild_system,agwild_parameters,param_library)

    agwild_system%Pmod=agwild_system%P
    agwild_system%P=agwild_system%P*sqrt(system%mass(1))

  end subroutine initialize_agwild

  subroutine agwild_P_from_Ptilde(system,parameters)
    IMPLICIT NONE
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
  
    agwild_system%P=agwild_system%Pmod*sqrt(system%Qinv)

  end subroutine agwild_P_from_Ptilde

  subroutine agwild_initialize_forces(system,parameters,param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
  
    if(allocated(system%Forces)) deallocate(system%Forces)
		allocate(system%Forces(system%n_atoms,system%n_dim))

    system%k2_params=param_library%get('agwild/k2_params')
    system%dU_params=param_library%get('agwild/du_params')

    system%k2_poly_order=size(system%k2_params)-1
    system%dU_poly_order=size(system%dU_params)-1

    call agwild_compute_forces(system,parameters)

  end subroutine agwild_initialize_forces

  subroutine agwild_compute_forces(system,parameters)
    IMPLICIT NONE
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
    REAL(wp) :: q,dok2,F
    INTEGER :: i

    q=system%X(1,1)

    call agwild_compute_k2(system,parameters)

    system%dk2=0
    DO i=1,system%k2_poly_order
      system%dk2=system%dk2 + i*system%k2_params(i+1)*(q**(i-1))
    ENDDO
    dok2=system%dk2/system%k2

    system%Qinv=system%k2/system%lth2
    system%Q=1._wp/system%Qinv
    system%dQinv=system%dk2/system%lth2

    system%dU=system%dU_params(1)
    DO i=1,system%dU_poly_order
      system%dU=system%dU + system%dU_params(i+1)*(q**i)
    ENDDO

    F=-(system%dU+system%Qinv*dPot(system%X)) &
        -parameters%temperature*system%Q*sqrt(system%Q)*system%dQinv

    system%Forces=*sqrt(system%Q/system%mass)*F

  end subroutine agwild_compute_forces

  subroutine agwild_compute_k2(system,parameters)
    IMPLICIT NONE
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
    INTEGER :: i
    REAL(wp) :: q

    q=system%X(1,1)
    system%k2=system%k2_params(1)
    DO i=1,system%k2_poly_order
      system%k2=system%k2 + system%k2_params(i+1)*(q**i)
    ENDDO

  END subroutine agwild_compute_k2

  subroutine agwild_initialize_integrator(system,parameters,param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
  
    parameters%integrator_type=param_library%get("PARAMETERS/integrator" &
										, default="baoab")
    parameters%integrator_type=to_lower_case(parameters%integrator_type)

    parameters%gamma0=param_library%get("THERMOSTAT/damping")
    parameters%gexp=exp(-parameters%gamma0*parameters%dt)
    parameters%gsigma=sqrt((1.0-parameters%gexp**2)*parameters%temperature)

    SELECT CASE(parameters%integrator_type)
    CASE("baoab")
      agwild_integrator => agwild_BAOAB
    CASE("aboba")
      agwild_integrator => agwild_ABOBA
    CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
		END SELECT		

  end subroutine agwild_initialize_integrator

  subroutine agwild_ABOBA(system,parameters)
    IMPLICIT NONE
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
    REAL(wp) :: dt2,R
    dt2=0.5*parameters%dt

    system%X = system%X + dt2*system%P/system%mass(1)
    call agwild_compute_forces(system,parameters)
    system%P = system%P + dt2*system%Forces

    call RandGauss(R)
    system%P = parameters%gexp*system%P &
      + R*parameters%gsigma

    system%P = system%P + dt2*system%Forces
    system%X = system%X + dt2*system%P/system%mass(1)

  end subroutine agwild_ABOBA

  subroutine agwild_BAOAB(system,parameters)
    IMPLICIT NONE
    CLASS(AGWILD_SYS), intent(inout) :: system
    CLASS(AGWILD_PARAM), intent(inout) :: parameters
    REAL(wp) :: dt2,R
    dt2=0.5*parameters%dt

    if(parameters%apply_p2_force) &
      system%P = system%P /(1._wp - dt2*system%W*system%P)
    
    system%P = system%P + dt2*system%Forces

    system%X = system%X + dt2*system%P/system%mass(1)
    call agwild_compute_k2(system,parameters)

    call RandGauss(R)
    system%P = parameters%gexp*system%P &
      + R*parameters%gsigma/sqrt(system%k2)

    system%X = system%X + dt2*system%P/system%mass(1)
    call agwild_compute_forces(system,parameters)
    
    system%P = system%P + dt2*system%Forces

    if(parameters%apply_p2_force) &
      system%P = system%P /(1._wp - dt2*system%W*system%P)
    

  end subroutine agwild_BAOAB

END MODULE analytic_gwild_1d