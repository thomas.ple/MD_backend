MODULE autok2
  USE kinds
  USE classical_md_types
  USE classical_md
  USE string_operations
  USE random
  USE matrix_operations
  USE qtb_types
  USE thermostats, only : initialize_thermostat &
                          ,finalize_thermostat &
                          ,thermostat_end_thermalization &
                          ,apply_thermostat
  IMPLICIT NONE

  CHARACTER(*), PARAMETER :: AK2_CAT="autok2"

  TYPE, EXTENDS(CLASS_MD_SYS) :: autok2_SYS
    REAL(wp), ALLOCATABLE :: delta(:,:,:), pdelta(:,:,:)
    REAL(wp), ALLOCATABLE :: Pmod(:,:),Xmod(:,:)
    REAL(wp), ALLOCATABLE :: massd(:)
    REAL(wp), ALLOCATABLE :: sqrtmass(:)
    REAL(wp) :: nose,nosed,mnose,mnosed
    REAL(wp), ALLOCATABLE :: Fmean(:,:)

    INTEGER :: n_delta

    REAL(wp) :: Tloc,Tdeltaloc

    REAL(wp), ALLOCATABLE :: k2(:,:), momega2(:,:), sqrtQ(:,:),sqrtQinv(:,:),Q(:,:)
    REAL(wp) :: sqrtQdet

    REAL(wp), ALLOCATABLE :: memory_sqrtQinv(:,:),memory_k2(:,:),memory_sqrtQ(:,:),memory_momega2(:,:)
  END TYPE

  TYPE, EXTENDS(CLASS_MD_PARAM) :: autok2_PARAM
    REAL(wp) :: gamma0, gexp,gsigma,gammad,gexpd,gsigmad
    INTEGER :: n_delta
    LOGICAL :: use_nose,use_nose_delta
    LOGICAL :: use_global_thermostat
    LOGICAL :: use_Fmean

    REAL(wp) :: memory_sum_weight
    REAL(wp) :: memory_tau
    LOGICAL :: memory_smoothing

    TYPE(QTB_TYPE) :: qtb
    LOGICAL :: use_change_var
    LOGICAL :: move_system
  END TYPE

  TYPE(autok2_SYS), save, target :: ak2_system
	TYPE(autok2_PARAM), save, target :: ak2_parameters

	PRIVATE
  PUBLIC :: initialize_autok2

CONTAINS

  subroutine autok2_loop()
		IMPLICIT NONE
    INTEGER :: i

    if(ak2_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",ak2_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,ak2_parameters%n_steps_therm

        call ak2_integrator(ak2_system,ak2_parameters)

        if(ak2_parameters%compute_E_kin) &
					call autok2_compute_kinetic_energy(ak2_system,ak2_parameters)
				call classical_md_print_system_info(ak2_system,ak2_parameters,i)
			ENDDO
			write(*,*) "Thermalization done."
      call thermostat_end_thermalization(ak2_system,ak2_parameters)
    ENDIF

    write(*,*) "Starting ",ak2_parameters%n_steps," steps &
						&of Analytic autok2"
			DO i=1,ak2_parameters%n_steps

        call ak2_integrator(ak2_system,ak2_parameters)
        
        if(ak2_parameters%compute_E_kin) &
					call autok2_compute_kinetic_energy(ak2_system,ak2_parameters)
				call classical_md_print_system_info(ak2_system,ak2_parameters,i)

        if(ak2_parameters%compute_dipole) then
				if(mod(i,ak2_parameters%compute_dipole_stride)==0) &
            call autok2_compute_dipole(ak2_system,ak2_parameters)
        endif

        ! WRITE TRAJECTORY
			  call output%write_all()
			ENDDO

      write(*,*) "Simulation done."    

  end subroutine autok2_loop

  subroutine initialize_autok2(param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    INTEGER :: i,j
    ak2_parameters%engine="autok2"

		!ASSOCIATE simulation loop
		simulation_loop => autok2_loop

		!GET ALGORITHM
		ak2_parameters%algorithm="nvt"

		! GET PARAMETERS
    ak2_parameters%use_change_var=param_library%get(AK2_CAT//"/use_change_var",default=.TRUE.)
    ak2_parameters%move_system=.NOT. param_library%get(AK2_CAT//"/only_move_delta",default=.FALSE.)

    if(ak2_parameters%use_change_var) then
      call param_library%store("THERMOSTAT/mass_normalized",.TRUE.)
    else
      call param_library%store("THERMOSTAT/mass_normalized",.FALSE.)
    endif
		call classical_MD_get_parameters(ak2_system,ak2_parameters,param_library)

		call classical_MD_initialize_momenta(ak2_system,ak2_parameters)
    allocate(ak2_system%Pmod(ak2_system%n_atoms,ak2_system%n_dim))
    allocate(ak2_system%Xmod(ak2_system%n_atoms,ak2_system%n_dim))
    ak2_system%Pmod=ak2_system%P
    DO i=1,ak2_system%n_dim
      ak2_system%Xmod(:,i)=sqrt(ak2_system%masskin(:))*ak2_system%X(:,i)
    ENDDO
    ! do i=1,ak2_system%n_dim
    !   ak2_system%P(:,i)=ak2_system%P(:,i)/SQRT(ak2_system%mass(:))
    ! enddo

    call get_autok2_parameters(ak2_system,ak2_parameters,param_library)

    call autok2_initialize_forces(ak2_system,ak2_parameters,param_library)

    call classical_MD_initialize_output(ak2_system,ak2_parameters,param_library)
    call autok2_initialize_output(ak2_system,ak2_parameters,param_library)

    if(ak2_parameters%compute_dipole) &
			 call classical_MD_initialize_dipole(ak2_system,ak2_parameters,param_library)

  end subroutine initialize_autok2

  subroutine finalize_autok2()
		IMPLICIT NONE

    if(ak2_parameters%use_global_thermostat) &
	  	call finalize_thermostat(ak2_system,ak2_parameters)
		call classical_MD_deallocate(ak2_system,ak2_parameters)

	end subroutine finalize_autok2

  subroutine get_autok2_parameters(system,parameters,param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: i, idelta,ndof
    REAL(wp) :: delta_mass_factor

    ndof=system%n_dof

    allocate(system%massd(system%n_atoms))
    delta_mass_factor = param_library%get(AK2_CAT//"/delta_mass_factor")
    system%massd=delta_mass_factor*system%mass

    allocate(system%sqrtmass(system%n_atoms))
    system%sqrtmass=sqrt(system%mass)

    parameters%n_delta=param_library%get(AK2_CAT//"/n_delta")
    system%n_delta=parameters%n_delta
    ALLOCATE(system%delta(system%n_atoms,system%n_dim,system%n_delta) &
      ,system%pdelta(system%n_atoms,system%n_dim,system%n_delta))
    system%delta=0._wp

    DO idelta=1,system%n_delta
      DO i=1,system%n_dim
        call randGaussN(system%pdelta(:,i,idelta))
        system%pdelta(:,i,idelta)=system%pdelta(:,i,idelta) &
            *SQRT(parameters%temperature*system%massd(:))
      ENDDO
    ENDDO

    parameters%use_Fmean=param_library%get(AK2_CAT//"/use_fmean",default=.FALSE.)
    

    parameters%use_global_thermostat= &
      param_library%get(AK2_CAT//"/use_global_thermostat",default=.TRUE.)
    parameters%gamma0=param_library%get("THERMOSTAT/damping")
    parameters%gexp=exp(-parameters%gamma0*parameters%dt)
    parameters%gsigma=sqrt((1.0-parameters%gexp**2)*parameters%temperature)

    if(parameters%use_global_thermostat) then
      !call initialize_thermostat(system,parameters,param_library)
      call parameters%qtb%initialize(system,parameters,param_library)
    endif

    parameters%gammad=param_library%get(AK2_CAT//"/damping_delta",default=parameters%gamma0)
    parameters%gexpd=exp(-parameters%gammad*parameters%dt)
    parameters%gsigmad=sqrt((1.0-parameters%gexpd**2)*parameters%temperature)

    system%nose=0. ; system%nosed=0.;
    parameters%use_nose=param_library%get(AK2_CAT//"/use_nose",default=.FALSE.)
    if(parameters%use_nose) then
      if(.NOT.parameters%use_change_var) STOP "Error:nose only compatible with changed var autok2"
      system%mnose = param_library%get(AK2_CAT//"/m_nose",default=1.0e5_wp)
    endif
    parameters%use_nose_delta=param_library%get(AK2_CAT//"/use_nose_delta",default=.FALSE.)
    if(parameters%use_nose_delta) then
      system%mnosed = param_library%get(AK2_CAT//"/m_nose_delta",default=1.0e5_wp)
    endif

    system%Tloc=system%n_dof*parameters%temperature
    system%Tdeltaloc=system%n_dof*system%n_delta*parameters%temperature

    parameters%memory_smoothing=param_library%get(AK2_CAT//"/memory_smoothing" &
                                ,default=.FALSE.)
    !! @input_file autok2/memory_smoothing (autok2, default=.FALSE.)
    if(parameters%memory_smoothing) then
        parameters%memory_tau=param_library%get(AK2_CAT//"/memory_tau" &
                                  ,default=5._wp*parameters%dt)
        !! @input_file autok2/memory_tau (autok2, default=5*dt)
        allocate(system%memory_sqrtQinv(ndof,ndof) &
                ,system%memory_k2(ndof,ndof) &
                ,system%memory_sqrtQ(ndof,ndof) &
                ,system%memory_momega2(ndof,ndof) &
            )
        system%memory_sqrtQinv=0
        system%memory_k2=0
        system%memory_sqrtQ=0
        system%memory_momega2=0
        parameters%memory_sum_weight=0
        write(*,*)
        write(*,*) "memory smoothing activated with tau=",parameters%memory_tau*fs,"fs"
    endif
  
  end subroutine get_autok2_parameters

  subroutine autok2_initialize_forces(system,parameters,param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: ndof,i,j,c
  
    if(allocated(system%Forces)) deallocate(system%Forces)
		allocate(system%Forces(system%n_atoms,system%n_dim))
    call get_pot_info(system%X,system%E_pot,system%Forces)

    ndof=system%n_dof
    allocate(system%k2(ndof,ndof) &
            ,system%Q(ndof,ndof) &
            ,system%momega2(ndof,ndof) &
            ,system%sqrtQ(ndof,ndof) &
            ,system%sqrtQinv(ndof,ndof) &
            ,system%Fmean(system%n_atoms,system%n_dim) &
    )
    system%Fmean=0.
    system%k2=0.
    system%Q=0.
    system%momega2=0.
    system%sqrtQ=0.
    system%sqrtQinv=0.
    c=0
    DO j=1,system%n_dim ; DO i=1,system%n_atoms
      c=c+1
      system%momega2(c,c)=system%mass(i)*parameters%temperature**2
      system%sqrtQ(c,c)=1._wp
      system%sqrtQinv(c,c)=1._wp
    ENDDO ; ENDDO

  end subroutine autok2_initialize_forces

  subroutine autok2_Pmod_from_P(system,parameters)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: i

    if(parameters%use_change_var) then

      system%Pmod(:,:) = RESHAPE( matmul(system%sqrtQ, &
                  RESHAPE(system%P,(/system%n_dof/)) &
            ), (/system%n_atoms,system%n_dim/) &
          )
      DO i=1,system%n_dim
        system%Pmod(:,i)=system%Pmod(:,i)*system%sqrtmass(:)
      ENDDO

    else
      system%Pmod(:,:)=system%P(:,:)
    endif

  end subroutine autok2_Pmod_from_P

  subroutine autok2_Xmod_from_X(system,parameters)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: i

    if(parameters%use_change_var) then
      DO i=1,system%n_dim
        system%Xmod(:,i)=system%X(:,i)*system%sqrtmass(:)
      ENDDO

      system%Xmod(:,:) = RESHAPE( matmul(system%sqrtQ, &
                  RESHAPE(system%Xmod,(/system%n_dof/)) &
            ), (/system%n_atoms,system%n_dim/) &
          )     

    else
      system%Xmod(:,:)=system%X(:,:)
    endif

  end subroutine autok2_Xmod_from_X

  subroutine autok2_X_from_Xmod(system,parameters)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: i

    if(parameters%use_change_var) then      

      system%X(:,:) = RESHAPE( matmul(system%sqrtQinv, &
                  RESHAPE(system%Xmod,(/system%n_dof/)) &
            ), (/system%n_atoms,system%n_dim/) &
          )  

      DO i=1,system%n_dim
        system%X(:,i)=system%X(:,i)/system%sqrtmass(:)
      ENDDO   

    else
      system%X(:,:)=system%Xmod(:,:)
    endif

  end subroutine autok2_X_from_Xmod


  subroutine autok2_compute_forces(system,parameters)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: i

    call get_pot_info(system%X,system%E_pot,system%Forces)

    if(parameters%use_change_var) then
      DO i=1,system%n_dim
        system%Forces(:,i)=system%Forces(:,i)/system%sqrtmass(:)
        system%Fmean(:,i)=system%Fmean(:,i)/system%sqrtmass(:)
      ENDDO

      system%Forces(:,:) = RESHAPE( matmul(system%sqrtQinv, &
                  RESHAPE(system%Forces,(/system%n_dof/)) &
            ), (/system%n_atoms,system%n_dim/) &
          )
      
      system%Fmean(:,:) = RESHAPE( matmul(system%sqrtQinv, &
                  RESHAPE(system%Fmean,(/system%n_dof/)) &
            ), (/system%n_atoms,system%n_dim/) &
          )
    endif
  
  end subroutine autok2_compute_forces

  subroutine autok2_compute_delta_properties(system,parameters)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), ALLOCATABLE :: Eigmat(:,:),EigmatTr(:,:) &
            , eval(:),k2inv(:,:),dv(:),f(:,:),mext(:),omega2(:,:),Fmean(:,:)
    INTEGER :: ndof,i1,j1,c1,i2,j2,c2,idelta,INFO,i,j
    REAL(wp) :: om2,u

    ndof=system%n_dof
    ALLOCATE(Eigmat(ndof,ndof),eval(ndof),k2inv(ndof,ndof) &
        ,EigmatTR(ndof,ndof))

    system%k2=0._wp
    DO idelta=1,parameters%n_delta
      c1=0
      DO j1=1,system%n_dim ; DO i1=1,system%n_atoms
        c1=c1+1
        c2=0
        DO j2=1,system%n_dim ; DO i2=1,system%n_atoms
          c2=c2+1
          system%k2(c2,c1)=system%k2(c2,c1) &
            +system%delta(i1,j1,idelta)*system%delta(i2,j2,idelta)
        ENDDO ; ENDDO
      ENDDO ; ENDDO
    ENDDO
    system%k2=system%k2/REAL(system%n_delta)
    call sym_mat_diag(system%k2,eval,eigmat,INFO)
    k2inv=0
    DO i=1,ndof
      k2inv(i,i)=1./eval(i)
    ENDDO
    k2inv=matmul(EigMat,matmul(k2inv,transpose(EigMat)))

    ALLOCATE(omega2(ndof,ndof),Fmean(system%n_atoms,system%n_dim))
    system%momega2=0._wp
    omega2=0._wp
    Fmean=0._wp
    !$OMP PARALLEL private(dv,c1,c2,idelta,j1,i1,i2,j2,u,f) REDUCTION(+:omega2,Fmean)
    ALLOCATE(dv(ndof),f(system%n_atoms,system%n_dim))
    !$OMP DO
    DO idelta=1,parameters%n_delta
      call get_pot_info(system%X+0.5*system%delta(:,:,idelta),u,f)
      Fmean=Fmean+f
      dv=-RESHAPE(f,(/ndof/))
      call get_pot_info(system%X-0.5*system%delta(:,:,idelta),u,f)
      Fmean=Fmean+f
      dv=dv+RESHAPE(f,(/ndof/))
      c1=0
      DO j1=1,system%n_dim ; DO i1=1,system%n_atoms
        c1=c1+1
        c2=0
        DO j2=1,system%n_dim ; DO i2=1,system%n_atoms
          c2=c2+1
          omega2(c2,c1)=omega2(c2,c1) &
            +dv(c1)*system%delta(i2,j2,idelta)
        ENDDO ; ENDDO
      ENDDO ; ENDDO
    ENDDO  
    !$OMP END DO
    DEALLOCATE(dv,f)
    !$OMP END PARALLEL
    system%Fmean=Fmean/REAL(2*system%n_delta)
    system%momega2=matmul(k2inv,omega2/REAL(system%n_delta))
    DEALLOCATE(k2inv,omega2)
    
    ALLOCATE(mext(ndof))
    c1=0
    DO j=1,system%n_dim ; DO i=1,system%n_atoms
      c1=c1+1
      mext(c1)=system%sqrtmass(i)
    ENDDO ; ENDDO

    DO j=1,ndof ; DO i=1,ndof
      system%momega2(i,j)=system%momega2(i,j)/(mext(i)*mext(j))
    ENDDO ; ENDDO
    system%momega2=0.5*(system%momega2+transpose(system%momega2))    


    call sym_mat_diag(system%momega2,eval,eigmat,INFO)
    EigmatTR=transpose(Eigmat)
    system%momega2=0.
    system%sqrtQ=0.
    system%Q=0.
    system%sqrtQinv=0.
    system%sqrtQdet=1.
    DO i=1,ndof
      om2=eval(i)
      if(om2>=0) then
        u=0.5*sqrt(om2)/parameters%temperature
        system%Q(i,i)=u/tanh(u)
      else
        u=0.5*sqrt(-om2)/parameters%temperature
        if(u<0.5*pi) then
          system%Q(i,i)=u/tanh(u)
        else
          system%Q(i,i)=tanh(u)/u
        endif
      endif
      system%sqrtQ(i,i)=sqrt(system%Q(i,i))
      system%sqrtQinv(i,i)=1./system%sqrtQ(i,i)
      system%sqrtQdet=system%sqrtQdet*system%sqrtQ(i,i)
    ENDDO
    system%Q=matmul(EigMat,matmul(system%Q,EigmatTR))
    system%sqrtQ=matmul(EigMat,matmul(system%sqrtQ,EigmatTR))
    system%sqrtQinv=matmul(EigMat,matmul(system%sqrtQinv,EigmatTR))
    system%momega2=system%Q*parameters%temperature**2

    DO j=1,ndof ; DO i=1,ndof
      system%momega2(i,j)=system%momega2(i,j)*mext(i)*mext(j)
    ENDDO ; ENDDO

    DEALLOCATE(Eigmat,EigmatTR,eval,mext)

    if(parameters%memory_smoothing) call memory_smoothing(system,parameters)

  end subroutine autok2_compute_delta_properties

  subroutine ak2_integrator(system,parameters)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    INTEGER :: i,j,idelta
    REAL(wp) :: dt2

    dt2=0.5_wp*parameters%dt

    if(parameters%move_system) then
      call apply_A_delta(system,parameters,dt2)
    endif

    call autok2_compute_delta_properties(system,parameters)

    if(parameters%move_system) then
      call autok2_Pmod_from_P(system,parameters)
      call apply_A(system,parameters,dt2)
    endif

    call autok2_compute_forces(system,parameters)

    if(parameters%move_system) then
      call apply_B(system,parameters,dt2)
    endif
    call apply_B_delta(system,parameters,dt2)

    call apply_N(system,parameters,dt2)
    if(parameters%use_change_var) then
      call apply_O_change_var(system,parameters,parameters%dt)
    else
      call apply_O(system,parameters,parameters%dt)
    endif
    call apply_N(system,parameters,dt2)

    call apply_B_delta(system,parameters,dt2)
    
    if(parameters%move_system) then
      call apply_B(system,parameters,dt2)

      call autok2_Pmod_from_P(system,parameters)
      call apply_A(system,parameters,dt2)
      call apply_A_delta(system,parameters,dt2)      
    endif
    !call autok2_compute_delta_properties(system,parameters)
    !call autok2_compute_forces(system,parameters)
    !call apply_B(system,parameters,parameters%dt)
    

  end subroutine ak2_integrator

  subroutine apply_A(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    INTEGER :: i,idelta

    ! if(parameters%use_change_var) then
    !   call autok2_Xmod_from_X(system,parameters)
    !   system%Xmod(:,:)=system%Xmod(:,:) + tau* RESHAPE( matmul(system%Q, &
    !                 RESHAPE(system%P,(/system%n_dof/)) &
    !           ), (/system%n_atoms,system%n_dim/) &
    !         )
    !   call autok2_X_from_Xmod(system,parameters)
    ! else
      DO i=1,system%n_dim
        system%X(:,i)=system%X(:,i) + tau*system%Pmod(:,i)/system%mass(:)
      ENDDO
    ! endif

  end subroutine apply_A

  subroutine apply_A_delta(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    INTEGER :: i,idelta

    DO idelta=1,system%n_delta
      DO i=1,system%n_dim
        system%delta(:,i,idelta)=system%delta(:,i,idelta) + tau*system%Pdelta(:,i,idelta)/system%massd(:)
      ENDDO
    ENDDO

  end subroutine apply_A_delta

  subroutine apply_B(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    INTEGER :: i

    if(parameters%use_Fmean) then
      system%P = system%P + tau*system%Fmean
    else
      system%P = system%P + tau*system%Forces
    endif
  
  end subroutine apply_B

  subroutine apply_B_delta(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    INTEGER :: idelta

    DO idelta=1,system%n_delta
      system%Pdelta(:,:,idelta) = system%Pdelta(:,:,idelta) &
        -tau*RESHAPE( matmul(system%momega2, &
                RESHAPE(system%delta(:,:,idelta),(/system%n_dof/)) &
          ), (/system%n_atoms,system%n_dim/) &
        )
    ENDDO
  
  end subroutine apply_B_delta

  subroutine apply_N(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    REAL(wp) :: targ   
    INTEGER :: idelta,i

    system%Tloc=0
    DO i=1,system%n_dim
      system%Tloc =  system%Tloc + SUM(system%P(:,i)**2/system%masskin(:))
    ENDDO

    system%Tdeltaloc=0._wp
    DO idelta=1,system%n_delta
      DO i=1,system%n_dim
        system%Tdeltaloc = system%Tdeltaloc &
          +SUM(system%Pdelta(:,i,idelta)**2/system%massd(:))
      ENDDO
    ENDDO

    if(parameters%use_nose) then
      targ = system%n_dof*parameters%temperature
      system%nose = system%nose + tau*(system%Tloc-targ)/system%mnose
      system%P=system%P*exp(-tau*system%nose)
    endif

    if(parameters%use_nose_delta) then
      targ = system%n_delta*system%n_dof*parameters%temperature
      system%nosed = system%nosed + tau*(system%Tdeltaloc-targ)/system%mnosed
      system%Pdelta=system%Pdelta*exp(-tau*system%nosed)
    endif

  end subroutine apply_N

  subroutine apply_O_change_var(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    INTEGER :: idelta,i
    REAL(wp) :: R(system%n_atoms)

    DO idelta=1,system%n_delta
      DO i=1,system%n_dim
        call randGaussN(R)
        system%Pdelta(:,i,idelta) = system%Pdelta(:,i,idelta)*parameters%gexpd &
            + parameters%gsigmad*R(:)*sqrt(system%massd(:))
      ENDDO
    ENDDO

    if(parameters%use_global_thermostat) then
      system%P(:,:)=parameters%qtb%gamma_exp*system%P(:,:) &
							+ tau*parameters%qtb%get_force(system,parameters)
    else
      DO i=1,system%n_dim
        call randGaussN(R)
        system%P(:,i) = system%P(:,i)*parameters%gexp &
            + parameters%gsigma*R(:)
      ENDDO   
    endif

  end subroutine apply_O_change_var

  subroutine apply_O(system,parameters,tau)
    IMPLICIT NONE
    CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
    REAL(wp), INTENT(in) :: tau
    INTEGER :: idelta,i,j,idof
    REAL(wp) :: R(system%n_dof)

    DO idelta=1,system%n_delta
      DO i=1,system%n_dim
        call randGaussN(R)
        system%Pdelta(:,i,idelta) = system%Pdelta(:,i,idelta)*parameters%gexpd &
            + parameters%gsigmad*R(:)*sqrt(system%massd(:))
      ENDDO
    ENDDO

    if(parameters%use_global_thermostat) then
      R=matmul(system%sqrtQ(:,:), &
        RESHAPE(parameters%qtb%get_force(system,parameters), (/system%n_dof/)) &
      )

      !UPDATE QTB STORE
      parameters%qtb%Force(:,:,parameters%qtb%selector)=RESHAPE(R,(/ system%n_atoms,system%n_dim /))
      call parameters%qtb%store_velocities(SQRT(parameters%qtb%gamma_exp)*system%P(:,:) &
				 			            + 0.5_wp*tau*RESHAPE(R(:), (/ system%n_atoms,system%n_dim /)) )

      system%P(:,:) = system%P(:,:)*parameters%qtb%gamma_exp &
          + tau*RESHAPE(R(:), (/ system%n_atoms,system%n_dim /)) 
    else
      call randGaussN(R)
      idof=0
      DO j=1,system%n_dim ; DO i=1,system%n_atoms
        idof=idof+1
        R(idof)=R(idof)*parameters%gsigma*system%sqrtmass(i)
      ENDDO ; ENDDO 
      R(:)=matmul(system%sqrtQ(:,:),R(:))
      system%P(:,:) = system%P(:,:)*parameters%gexp &
          + RESHAPE(R(:), (/ system%n_atoms,system%n_dim /)) 
    endif
    
    

  end subroutine apply_O


!-------------------------------------------------------------

  subroutine autok2_compute_dipole(system,parameters)
		IMPLICIT NONE
   	CLASS(autok2_SYS), intent(inout) :: system
    CLASS(autok2_PARAM), intent(inout) :: parameters
		INTEGER :: i,j

		system%dipole=0._wp
		system%dipole_velocity=0._wp	
		DO j=1,system%n_dim ; DO i=1,system%n_atoms
			system%dipole(j)=system%dipole(j) &
					+system%charges(i)*system%X(i,j)/system%n_atoms

			system%dipole_velocity(j)=system%dipole_velocity(j) &
					+system%charges(i)*system%Pmod(i,j)/(system%mass(i)*system%n_atoms)
		ENDDO ; ENDDO
	
	end subroutine autok2_compute_dipole

  subroutine autok2_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(autok2_SYS), INTENT(inout) :: system
		CLASS(autok2_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name,unit_tmp
		INTEGER :: i_file, i,k
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)			
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))


			CASE("momentum","momenta")        
			!! @input_file OUTPUT/momentum (autok2)	
        if(parameters%use_change_var) then		
          description="trajectory of momenta"
          if(system%n_dim==1) then
            i_file=output%get_file_index("P.traj")
            if(i_file>0) call output%files(i_file)%rename("P_tilde.traj")          

            default_name="P.traj"
            call output%create_file(dict,default_name,index=i_file)

            if(output%files(i_file)%has_time_column) &
              call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
            call output%add_array_to_file(system%Pmod(:,1),index=i_file,name="momenta")
          else
            i_file=output%get_file_index("momentum.traj.xyz")
            if(i_file>0) call output%files(i_file)%rename("P_tilde.traj.xyz")  

            default_name="momentum.traj.xyz"
            call output%create_file(dict,default_name,index=i_file)
            call output%add_xyz_to_file(system%Pmod,system%element_symbols,index=i_file,name="momenta")
          endif
          call output%files(i_file)%edit_description(description)
        endif
      
      CASE("fmean","f_mean")        
			!! @input_file OUTPUT/fmean (autok2)			
				description="trajectory of mean delta force"
				if(system%n_dim==1) then
          default_name="Fmean.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%Fmean(:,1),index=i_file,name="Fmean")
				else
					default_name="Fmean.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%Pmod,system%element_symbols,index=i_file,name="Fmean")
				endif
				call output%files(i_file)%edit_description(description)
             
			CASE("sqrtqinv")		
			!! @input_file OUTPUT/q_inv (gwild)		
				description="trajectory of sqrt(Q^-1) (only the lower triangular part in col-major)"
				default_name="sqrtQinv.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%sqrtQinv(k:system%n_dof,k),index=i_file,name="sqrtQinv")
				ENDDO
      
      CASE("sqrtq")		
			!! @input_file OUTPUT/q_inv (gwild)		
				description="trajectory of sqrt(Q) (only the lower triangular part in col-major)"
				default_name="sqrtQ.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%sqrtQ(k:system%n_dof,k),index=i_file,name="sqrtQinv")
				ENDDO
      
      CASE("sqrtqdet")		
			!! @input_file OUTPUT/q_inv (gwild)		
				description="trajectory of det(sqrt(Q))"
				default_name="sqrtQdet.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_column_to_file(system%sqrtQdet,index=i_file,name="sqrtQdet")
      
      CASE("k2")		
			!! @input_file OUTPUT/q_inv (gwild)		
				description="trajectory of k2 (only the lower triangular part in col-major)"
				default_name="k2.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%k2(k:system%n_dof,k),index=i_file,name="k2")
				ENDDO
      
      CASE("nose")		
			!! @input_file OUTPUT/q_inv (gwild)		
				description="trajectory of nose variables"
				default_name="nose.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_column_to_file(system%nose,index=i_file,name="nose")
          call output%add_column_to_file(parameters%gamma0,index=i_file,name="nose")
          call output%add_column_to_file(system%nosed,index=i_file,name="nose delta")
          call output%add_column_to_file(parameters%gammad,index=i_file,name="nose")

			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

	end subroutine autok2_initialize_output

  subroutine autok2_compute_kinetic_energy(system,parameters)
		IMPLICIT NONE
    CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j,n,k
		REAL(wp) :: vol

		n=0
		DO j=1,system%n_dim ; do i=1,system%n_atoms
			n=n+1
			system%E_kin(n)=system%P(i,j)**2/(2._wp*system%masskin(i))
		ENDDO ; ENDDO

		system%pressure_tensor=0._wp
		if(parameters%n_dim==3) then
			vol=product(simulation_box%box_lengths)		
      system%pressure_tensor=0.	
		endif

	end subroutine autok2_compute_kinetic_energy

  subroutine memory_smoothing(system,parameters)
   IMPLICIT NONE
		CLASS(autok2_SYS), INTENT(inout) :: system
		CLASS(autok2_PARAM), INTENT(inout) :: parameters
    REAL(wp) :: mem_exp
    
    mem_exp = exp(-parameters%dt/parameters%memory_tau)

    parameters%memory_sum_weight = 1._wp + parameters%memory_sum_weight*mem_exp

    system%memory_k2 = system%memory_k2 * mem_exp + system%k2
    system%memory_sqrtQ = system%memory_sqrtQ * mem_exp + system%sqrtQ
    system%memory_sqrtQinv  = system%memory_sqrtQinv  * mem_exp + system%sqrtQinv
    system%memory_momega2  = system%memory_momega2  * mem_exp + system%momega2

    system%sqrtQinv = system%memory_sqrtQinv / parameters%memory_sum_weight
    system%k2 = system%memory_k2 / parameters%memory_sum_weight
    system%sqrtQ  = system%memory_sqrtQ  / parameters%memory_sum_weight
    system%momega2  = system%memory_momega2  / parameters%memory_sum_weight
    
  end subroutine memory_smoothing

END MODULE autok2
