MODULE analytic_wild_1d
  USE kinds
  USE classical_md_types
  USE classical_md
  USE string_operations
  USE random
  IMPLICIT NONE

  TYPE, EXTENDS(CLASS_MD_SYS) :: AWILD_SYS
    REAL(wp) :: k2,W,dU,dk2
    REAL(wp), ALLOCATABLE :: k2_params(:), dU_params(:)
    INTEGER :: k2_poly_order, dU_poly_order
  END TYPE

  TYPE, EXTENDS(CLASS_MD_PARAM) :: AWILD_PARAM
    REAL(wp) :: lth2
    REAL(wp) :: gamma0, gexp,gsigma
    LOGICAL :: apply_p2_force
  END TYPE

  ABSTRACT INTERFACE
		subroutine awild_sub(system,parameters)
			import :: AWILD_SYS, AWILD_PARAM
			IMPLICIT NONE
			CLASS(AWILD_SYS), intent(inout) :: system
			CLASS(AWILD_PARAM), intent(inout) :: parameters
		end subroutine awild_sub
	END INTERFACE

  TYPE(AWILD_SYS), save, target :: awild_system
	TYPE(AWILD_PARAM), save, target :: awild_parameters

  PROCEDURE(awild_sub), pointer :: awild_integrator

	PRIVATE
  PUBLIC :: initialize_awild

CONTAINS

  subroutine awild_loop()
		IMPLICIT NONE
    INTEGER :: i

    if(awild_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",awild_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,awild_parameters%n_steps_therm

        call awild_integrator(awild_system,awild_parameters)

        if(awild_parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(awild_system,awild_parameters)
				call classical_md_print_system_info(awild_system,awild_parameters,i)
			ENDDO
			write(*,*) "Thermalization done."
    ENDIF

    write(*,*) "Starting ",awild_parameters%n_steps_therm," steps &
						&of Analytic WiLD"
			DO i=1,awild_parameters%n_steps
        awild_system%time=awild_system%time &
								+awild_parameters%dt

        call awild_integrator(awild_system,awild_parameters)
        
        if(awild_parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(awild_system,awild_parameters)
				call classical_md_print_system_info(awild_system,awild_parameters,i)

        ! WRITE TRAJECTORY
			  call output%write_all()
			ENDDO

      write(*,*) "Simulation done."    

  end subroutine awild_loop

  subroutine initialize_awild(param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library

    awild_parameters%engine="awild"

		!ASSOCIATE classical_MD_loop
		simulation_loop => awild_loop

		!GET ALGORITHM
		awild_parameters%algorithm="nvt"

		! GET PARAMETERS
		call classical_MD_get_parameters(awild_system,awild_parameters,param_library)

    if(awild_system%n_dof /= 1 ) STOP "Error: awild only implemented for n_dof=1"

    awild_parameters%lth2 = 1./(awild_system%mass(1)*awild_parameters%temperature)
		call classical_MD_initialize_momenta(awild_system,awild_parameters)

    call awild_initialize_forces(awild_system,awild_parameters,param_library)

    call awild_initialize_integrator(awild_system,awild_parameters,param_library)
		
		call classical_MD_initialize_output(awild_system,awild_parameters,param_library)

  end subroutine initialize_awild

  subroutine awild_initialize_forces(system,parameters,param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    CLASS(AWILD_SYS), intent(inout) :: system
    CLASS(AWILD_PARAM), intent(inout) :: parameters
  
    if(allocated(system%Forces)) deallocate(system%Forces)
		allocate(system%Forces(system%n_atoms,system%n_dim))

    system%k2_params=param_library%get('awild/k2_params')
    system%dU_params=param_library%get('awild/du_params')

    system%k2_poly_order=size(system%k2_params)-1
    system%dU_poly_order=size(system%dU_params)-1

    call awild_compute_forces(system,parameters)

  end subroutine awild_initialize_forces

  subroutine awild_compute_forces(system,parameters)
    IMPLICIT NONE
    CLASS(AWILD_SYS), intent(inout) :: system
    CLASS(AWILD_PARAM), intent(inout) :: parameters
    REAL(wp) :: q,dok2
    INTEGER :: i

    q=system%X(1,1)

    call awild_compute_k2(system,parameters)

    system%dk2=0
    DO i=1,system%k2_poly_order
      system%dk2=system%dk2 + i*system%k2_params(i+1)*(q**(i-1))
    ENDDO
    dok2=system%dk2/system%k2

    system%dU=system%dU_params(1)
    DO i=1,system%dU_poly_order
      system%dU=system%dU + system%dU_params(i+1)*(q**i)
    ENDDO

    system%Forces=-dPot(system%X) &
                  -(parameters%lth2/system%k2)*(&
                      system%dU &
                      +parameters%temperature*dok2 &
                  )
    system%W = -0.5_wp*dok2/system%mass(1)

  end subroutine awild_compute_forces

  subroutine awild_compute_k2(system,parameters)
    IMPLICIT NONE
    CLASS(AWILD_SYS), intent(inout) :: system
    CLASS(AWILD_PARAM), intent(inout) :: parameters
    INTEGER :: i
    REAL(wp) :: q

    q=system%X(1,1)
    system%k2=system%k2_params(1)
    DO i=1,system%k2_poly_order
      system%k2=system%k2 + system%k2_params(i+1)*(q**i)
    ENDDO

  END subroutine awild_compute_k2

  subroutine awild_initialize_integrator(system,parameters,param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
    CLASS(AWILD_SYS), intent(inout) :: system
    CLASS(AWILD_PARAM), intent(inout) :: parameters
  
    parameters%integrator_type=param_library%get("PARAMETERS/integrator" &
										, default="baoab")
    parameters%integrator_type=to_lower_case(parameters%integrator_type)

    parameters%gamma0=param_library%get("THERMOSTAT/damping")
    parameters%gexp=exp(-parameters%gamma0*parameters%dt)
    parameters%gsigma=sqrt(1.0-parameters%gexp**2)

    parameters%apply_p2_force = param_library%get("awild/apply_p2_force",default=.TRUE.)

    SELECT CASE(parameters%integrator_type)
    CASE("baoab")
      awild_integrator => awild_BAOAB
    CASE("aboba")
      awild_integrator => awild_ABOBA
    CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
		END SELECT		

  end subroutine awild_initialize_integrator

  subroutine awild_ABOBA(system,parameters)
    IMPLICIT NONE
    CLASS(AWILD_SYS), intent(inout) :: system
    CLASS(AWILD_PARAM), intent(inout) :: parameters
    REAL(wp) :: dt2,R
    dt2=0.5*parameters%dt

    system%X = system%X + dt2*system%P/system%mass(1)
    call awild_compute_forces(system,parameters)
    system%P = system%P + dt2*system%Forces

    if(parameters%apply_p2_force) &
      system%P = system%P /(1._wp - dt2*system%W*system%P)

    call RandGauss(R)
    system%P = parameters%gexp*system%P &
      + R*parameters%gsigma/sqrt(system%k2)

    if(parameters%apply_p2_force) &
      system%P = system%P /(1._wp - dt2*system%W*system%P)

    system%P = system%P + dt2*system%Forces
    system%X = system%X + dt2*system%P/system%mass(1)

  end subroutine awild_ABOBA

  subroutine awild_BAOAB(system,parameters)
    IMPLICIT NONE
    CLASS(AWILD_SYS), intent(inout) :: system
    CLASS(AWILD_PARAM), intent(inout) :: parameters
    REAL(wp) :: dt2,R
    dt2=0.5*parameters%dt

    if(parameters%apply_p2_force) &
      system%P = system%P /(1._wp - dt2*system%W*system%P)
    
    system%P = system%P + dt2*system%Forces

    system%X = system%X + dt2*system%P/system%mass(1)
    call awild_compute_k2(system,parameters)

    call RandGauss(R)
    system%P = parameters%gexp*system%P &
      + R*parameters%gsigma/sqrt(system%k2)

    system%X = system%X + dt2*system%P/system%mass(1)
    call awild_compute_forces(system,parameters)
    
    system%P = system%P + dt2*system%Forces

    if(parameters%apply_p2_force) &
      system%P = system%P /(1._wp - dt2*system%W*system%P)
    

  end subroutine awild_BAOAB

END MODULE analytic_wild_1d