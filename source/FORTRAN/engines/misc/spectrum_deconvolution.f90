MODULE spectrum_deconvolution
  use kinds
  USE basic_types
	USE nested_dictionaries
  use string_operations
  use atomic_units
  IMPLICIT NONE
  
  ABSTRACT INTERFACE
    FUNCTION convolution_kernel(omega,omega0,gamma)
      import wp
      IMPLICIT NONE
      REAL(wp), intent(in) :: omega,omega0,gamma
      REAL(wp) :: convolution_kernel
    END FUNCTION convolution_kernel
  END INTERFACE

  TYPE DECONV_TYPE
    INTEGER :: u,nom,INFO,niter
    REAL(wp), ALLOCATABLE :: s0(:),sout(:),omega(:),srec(:),omegasym(:),s0sym(:)
    REAL(wp) :: om,stmp,gamma,kT,ommin=TINY(1.),omega0
    LOGICAL :: symmetrize, freq0_exist,use_corr_pot,verbose
    REAL(wp) :: input_unit,omegacut,omega_smear,corrfact,threshold
    CHARACTER(:), ALLOCATABLE :: input_file,method
  END TYPE DECONV_TYPE

  TYPE(deconv_type), SAVE :: deconv_params
  
  PRIVATE
  PUBLIC :: DECONV_TYPE,initialize_deconvolution,deconvolute_spectrum &
            ,kernel_lorentz, kernel_lorentz_pot,finalize_deconvolution

CONTAINS

subroutine deconvolution_main()
  IMPLICIT NONE
  INTEGER :: nom,i,u
  REAL(wp), PARAMETER :: ommin=TINY(1.)
  REAL(wp) :: om

  nom=deconv_params%nom

  if (deconv_params%method=="qtb") then
    DO i=1,nom
      if(abs(deconv_params%omegasym(i))>ommin) then
        om=0.5*deconv_params%omegasym(i)/deconv_params%kT
        deconv_params%s0sym(i)=deconv_params%s0sym(i)*tanh(om)/om
      endif
    ENDDO
  endif

  call deconvolute_spectrum(deconv_params%s0sym,deconv_params%omegasym &
          ,deconv_params%niter,.false.,deconv_params%threshold,deconv_params%gamma &
          ,kernel_lorentz,deconv_params%sout,deconv_params%srec,verbose=deconv_params%verbose)
  
  if (deconv_params%method=="qtb") then
    DO i=1,nom
      if(abs(deconv_params%omegasym(i))>ommin) then
        om=0.5*deconv_params%omegasym(i)/deconv_params%kT
        deconv_params%s0sym(i)=deconv_params%s0sym(i)*om/tanh(om)
        deconv_params%sout(i)=deconv_params%sout(i)*om/tanh(om)
        deconv_params%srec(i)=deconv_params%srec(i)*om/tanh(om)
        endif
    ENDDO
  endif
  
  OPEN(newunit=u,file=deconv_params%input_file//".deconv")
  DO i=1,nom
     WRITE(u,*)  deconv_params%omegasym(i)* deconv_params%input_unit &
                      , deconv_params%sout(i)
  ENDDO
  CLOSE(u)
  write(*,*) " deconvoluted spectrum written to '"//deconv_params%input_file//".deconv'."

  OPEN(newunit=u,file=deconv_params%input_file//".reconv")
  DO i=1,nom
     WRITE(u,*)  deconv_params%omegasym(i)* deconv_params%input_unit &
                      , deconv_params%srec(i)
  ENDDO
  CLOSE(u)
  write(*,*) " deconvoluted spectrum written to '"//deconv_params%input_file//".reconv'."

  OPEN(newunit=u,file=deconv_params%input_file//".diff")
  DO i=1,nom
     WRITE(u,*)  deconv_params%omegasym(i)* deconv_params%input_unit &
                      , deconv_params%srec(i)-deconv_params%s0sym(i)
  ENDDO
  CLOSE(u)
  write(*,*) " deconvoluted spectrum written to '"//deconv_params%input_file//".diff'."

  write(*,*)
  write(*,*) "sum of the initial spectrum: ", sum(deconv_params%s0sym)
  write(*,*) "sum of the deconvoluted spectrum: ", sum(deconv_params%sout)
  write(*,*) "sum of the reconvoluted spectrum: ", sum(deconv_params%srec)
  write(*,*) "deconv to reconv ratio: ", sum(deconv_params%srec)/sum(deconv_params%sout)
  write(*,*) "deconv to initial ratio: ", sum(deconv_params%s0sym)/sum(deconv_params%sout)

end subroutine deconvolution_main

subroutine initialize_deconvolution(param_library)
  IMPLICIT NONE
  TYPE(DICT_STRUCT), intent(in) :: param_library
  INTEGER :: i,j

  !ASSOCIATE simulation loop
  simulation_loop => deconvolution_main

  call deconvolution_get_parameters(deconv_params,param_library)

  call deconvolution_read_input_spectrum(deconv_params)

end subroutine initialize_deconvolution

subroutine deconvolution_get_parameters(parameters,param_library)
  IMPLICIT NONE
  TYPE(DECONV_TYPE), intent(inout) :: parameters
  TYPE(DICT_STRUCT), intent(in) :: param_library
  CHARACTER(:), ALLOCATABLE :: input_unit

  parameters%gamma=param_library%get("THERMOSTAT/damping")
  parameters%freq0_exist=param_library%get("DECONVOLUTION/freq0_exist",default=.TRUE.)
  parameters%symmetrize=param_library%get("DECONVOLUTION/symmetrize",default=.FALSE.)

  parameters%verbose=param_library%get("DECONVOLUTION/verbose",default=.TRUE.)

  parameters%use_corr_pot=param_library%get("DECONVOLUTION/use_corr_pot",default=.FALSE.)
  if(parameters%use_corr_pot) STOP "Error: use_corr_pot not implemented yet!"

  parameters%method=to_lower_case(param_library%get("DECONVOLUTION/method",default="classical"))
  SELECT CASE(parameters%method)
  CASE("classical")
    write(*,*) "Deconvolution of "//parameters%method//" spectrum."    
  CASE("qtb")
    parameters%kT=param_library%get("PARAMETERS/temperature")
    write(*,*) "Deconvolution of "//parameters%method//" spectrum."
  CASE DEFAULT
    write(*,*) "Error: unknown deconvolution method '"//parameters%method//"'"
    STOP "Execution stopped."
  END SELECT

  parameters%niter=param_library%get("DECONVOLUTION/n_iter",default=100)
  parameters%threshold=1.0d-10
  parameters%threshold=param_library%get("DECONVOLUTION/threshold",default=parameters%threshold)

  input_unit=param_library%get("DECONVOLUTION/input_unit",default="cm-1")
  parameters%input_unit=atomic_units_get_multiplier(input_unit)
  parameters%input_file=param_library%get("DECONVOLUTION/input_file")

end subroutine deconvolution_get_parameters

subroutine deconvolution_read_input_spectrum(p)
  IMPLICIT NONE
  TYPE(DECONV_TYPE), intent(inout) :: p
  INTEGER :: u,i,INFO
  REAL(wp) :: om,stmp

  write(*,*) "Reading number of frequencies..."
  OPEN(newunit=u,file=p%input_file,action='READ')
  p%nom=0
  DO    
    READ(u,*,iostat=INFO) om,stmp
    IF(INFO/=0) exit
    p%nom=p%nom+1
  ENDDO
  write(*,*) "done!"
  
  write(*,*)
  write(*,*) "Storing input spectrum..."
  REWIND(u)

  ALLOCATE(p%s0(p%nom),p%omega(p%nom))  
  DO i=1,p%nom
    READ(u,*) p%omega(i),p%s0(i)
  ENDDO
  CLOSE(u)  
  p%omega=p%omega/p%input_unit  
  write(*,*) "done!"

  IF(p%symmetrize) then
    if (p%freq0_exist) then         
		  ALLOCATE(p%sout(2*p%nom-1),p%srec(2*p%nom-1))
		  ALLOCATE(p%s0sym(2*p%nom-1),p%omegasym(2*p%nom-1))
		  p%omegasym(p%nom)=p%omega(1)
		  p%s0sym(p%nom)=p%s0(1)
		  DO i=1,p%nom-1
		    p%omegasym(p%nom+i)=p%omega(1+i)	
		    p%omegasym(p%nom-i)=-p%omega(1+i)
		    p%s0sym(p%nom+i)=p%s0(1+i)
		    p%s0sym(p%nom-i)=p%s0(1+i)
		  ENDDO  
		  p%nom=2*p%nom-1
    else
		  ALLOCATE(p%sout(2*p%nom),p%srec(2*p%nom))
		  ALLOCATE(p%s0sym(2*p%nom),p%omegasym(2*p%nom))
		  DO i=1,p%nom
		    p%omegasym(p%nom+i)=p%omega(i)
		    p%omegasym(p%nom+1-i)=-p%omega(i)
		    p%s0sym(p%nom+i)=p%s0(i)
		    p%s0sym(p%nom+1-i)=p%s0(i)
		  ENDDO  
		  p%nom=2*p%nom
    endif
  ELSE
    ALLOCATE(p%sout(p%nom),p%srec(p%nom))
    ALLOCATE(p%s0sym(p%nom),p%omegasym(p%nom))    
    p%s0sym=p%s0
    p%omegasym=p%omega    
  ENDIF

  write(*,*) "nom=",p%nom

end subroutine deconvolution_read_input_spectrum


subroutine finalize_deconvolution()
  IMPLICIT NONE

end subroutine finalize_deconvolution

!! deconvolute the spectrum 's_in' using the method from ref:
!! J. Chem. Phys. 148, 102301 (2018); 
!! https://doi.org/10.1063/1.4990536@jcp.2018.NQE2018.issue-1
SUBROUTINE deconvolute_spectrum(s_in,omega,nIterations, &
                trans,thr,gamma,kernel,s_out,s_rec,verbose)
   use atomic_units 
  IMPLICIT NONE
  REAL(wp),INTENT(in) :: s_in(:),omega(:),gamma,thr
  INTEGER, INTENT(in) :: nIterations
  REAL(wp), INTENT(inout) :: s_out(:)
  REAL(wp), INTENT(inout), optional :: s_rec(:)
  LOGICAL, INTENT(in), optional :: verbose
  PROCEDURE(convolution_kernel):: kernel
  INTEGER :: nom,i,j,m,it
  REAL(wp), ALLOCATABLE :: D(:,:),K(:,:),h(:),s_next(:),Ktr(:,:)
  REAL(wp), ALLOCATABLE :: K_norm(:,:),omega_norm(:)  
  REAL(wp) :: domega,om,om0,den,rn,ln,diff,tmp
  LOGICAL :: verb,trans
  
  
  verb=.FALSE.
  if(present(verbose)) then
    if(verbose) verb=.TRUE.
  endif
  
  nom=size(omega)
  if(size(s_in)/=nom) STOP "Error: size(s_in)/=nom"
  if(size(s_out)/=nom) STOP "Error: size(s_out)/=nom"
  if(present(s_rec)) then
    if(size(s_rec)/=nom) STOP "Error: size(s_rec)/=nom"
  endif
  
  domega=omega(2)-omega(1)
  
  ALLOCATE(D(nom,nom),K(nom,nom),Ktr(nom,nom),h(nom),s_next(nom))
  if (trans) then 
		ALLOCATE(K_norm(nom,2*nom-1),omega_norm(2*nom-1))
		omega_norm(nom)=0.d0
		K_norm=0.d0
		do j=1,nom-1
		  omega_norm(nom+j)=j*domega
		  omega_norm(nom-j)=-j*domega
		enddo
		DO j=1,2*nom-1
		  om0=omega_norm(j)
		  DO i=1,nom
		    om=omega(i)  
		    K_norm(i,j)=kernel(om,om0,gamma)
		  ENDDO
		ENDDO
  endif

  if(verb) then
    write(*,*) "Initializing deconvolution..."
    write(*,*) "    Computing kernel matrix..."
  endif
  K=0.d0
  DO j=1,nom
    om0=omega(j)
    DO i=1,nom
      om=omega(i)  
      K(i,j)=kernel(om,om0,gamma)
    ENDDO
  ENDDO
  DO j=1,nom
    if (trans) then
      write(*,*) omega(j)*THz,sum(K(j,:))*domega
!      if (abs(omega(j))<maxval(omega)/5.0d0) K(j,:)=K(j,:)/sum(K(j,:))/domega 
      K(j,:)=K(j,:)/sum(K_norm(j,:))/domega 
    else
      K(:,j)=K(:,j)/sum(K(:,j))/domega 
    endif
  enddo

  if(verb) then
    write(*,*) "    done!"
    write(*,*) "    Computing double convolution matrix..."
  endif

  Ktr=transpose(K)
  !call DGEMM('T','N',nom,nom,nom,1._8,K,nom,K,nom,0._8,D,nom)
  D=matmul(Ktr,K)*domega

  if(verb) write(*,*) "    done!"  
  call convolution(Ktr,s_in,domega,h)
  if(verb) write(*,*) "done!"
  
  if(verb) write(*,*) "Starting iterative process"
  s_out=s_in
  s_next=0.d0
  DO it=1,nIterations
    if(verb) write(*,*) "Iteration ",it
    DO i=1,nom
      den=SUM(s_out*D(i,:))*domega
      s_next(i)=s_out(i)*h(i)/den
    ENDDO   
    diff=SUM((s_next-s_out)**2)/sum(s_out**2)
    if(verb) then
      write(*,*) "    relative squared difference: ",diff
      call compute_rn_ln(s_in,s_next,K,domega,rn,ln)
      write(*,*) "    rn=",rn,"ln=",ln
    endif
    s_out= s_next
    if(diff<=thr) exit  
  ENDDO  
  
  if(present(s_rec)) then
    call convolution(K,s_out,domega,s_rec)
  endif
  if(verb) write(*,*) "done!"
  
END SUBROUTINE deconvolute_spectrum

SUBROUTINE convolution(kernel,s_in,domega,s_out)
  IMPLICIT NONE
  REAL(wp), INTENT(in) :: kernel(:,:),s_in(:),domega
  REAL(wp), INTENT(inout) :: s_out(:)
  
  s_out=matmul(kernel,s_in)*domega

END SUBROUTINE convolution

SUBROUTINE compute_rn_ln(s0,sn,K,domega,rn,ln)
  IMPLICIT NONE
  REAL(wp), INTENT(in) :: s0(:),sn(:),K(:,:),domega
  REAL(wp), INTENT(out) :: rn,ln
  REAL(wp), ALLOCATABLE :: fc(:)
  INTEGER :: nom,i
  REAL(wp) :: domega3
  
  domega3=domega**3
  
  nom=size(s0)
  ALLOCATE(fc(nom))
  call convolution(K,sn,domega,fc)
  
  rn=SUM((fc-s0)**2)/SUM(s0**2)
  
  DEALLOCATE(fc)
  
  ln=0
  DO i=2,nom-1
    ! centered second derivative
    ln=ln+(sn(i+1)-2.d0*sn(i)+sn(i-1))**2/domega3
  ENDDO

END SUBROUTINE compute_rn_ln

FUNCTION kernel_lorentz(omega,omega0,gamma) result(kern)
  IMPLICIT NONE
  REAL(wp), intent(in) :: omega,omega0,gamma
  REAL(wp) :: kern
  REAL(wp), parameter :: pi=4.d0*atan(1.d0)
  REAL(wp) :: omegamin,omega2
  
  omegamin=TINY(omega)
  
  if(abs(omega)<=omegamin .AND. abs(omega0)<=omegamin) then
    kern = 1.d0/(pi*gamma)
  else  
    omega2=omega*omega
    kern = gamma*omega2/(pi*(omega2*gamma**2 + (omega2-omega0**2)**2))
  endif
  
END FUNCTION kernel_lorentz


FUNCTION kernel_lorentz_pot(omega,omega0,gamma) result(kern)
  IMPLICIT NONE
  REAL(wp), intent(in) :: omega,omega0,gamma
  REAL(wp) :: kern
  REAL(wp), parameter :: pi=4.d0*atan(1.d0)
  REAL(wp) :: omegamin,omega2
  
  omegamin=TINY(omega)
  
  if(abs(omega)<=omegamin .AND. abs(omega0)<=omegamin) then
    kern = 1.d0/(pi*gamma)
  else  
    omega2=omega*omega
    kern = gamma*omega2/(pi*(omega0**2*gamma**2 + (omega2-omega0**2)**2))
  endif
  
END FUNCTION kernel_lorentz_pot

END MODULE spectrum_deconvolution


