MODULE cmd_types
	USE kinds
	USE classical_md_types
	USE atomic_units, only : kelvin
	USE file_handler, only : output
	USE nested_dictionaries
    IMPLICIT NONE

	CHARACTER(*), PARAMETER :: CMD_CAT="CMD_AUX"

    TYPE, EXTENDS(CLASS_MD_SYS) :: CMD_SYS

		REAL(wp), allocatable :: X_prev(:,:)
		REAL(wp) :: T_est, T_est_mean

	END TYPE

	TYPE, EXTENDS(CLASS_MD_PARAM) :: CMD_PARAM
		CHARACTER(:), ALLOCATABLE :: move_generator

		LOGICAL :: save_beads_positions
		LOGICAL :: compute_temperature_estimation
		LOGICAL :: write_mean_temperature_info
	END TYPE	

	ABSTRACT INTERFACE
		subroutine cmd_sub(system,parameters)
			import :: CMD_SYS, CMD_PARAM
			IMPLICIT NONE
			CLASS(CMD_SYS), intent(inout) :: system
			CLASS(CMD_PARAM), intent(inout) :: parameters
		end subroutine cmd_sub

		subroutine cmd_sub_int(system,parameters,n)
			import :: CMD_SYS, CMD_PARAM
			IMPLICIT NONE
			CLASS(CMD_SYS), intent(inout) :: system
			CLASS(CMD_PARAM), intent(inout) :: parameters
			INTEGER, INTENT(in) :: n
		end subroutine cmd_sub_int
	END INTERFACE

CONTAINS

	subroutine cmd_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters

		call classical_MD_deallocate(system,parameters)
		!!! TO COMPLETE !!! 

	end subroutine cmd_deallocate

	subroutine update_T_est(system,parameters,n)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		INTEGER, intent(in) :: n
		INTEGER :: i

		system%T_est=0._wp
		DO i=1,system%n_atoms
			system%T_est=system%T_est + SUM(system%P(i,:)**2)/system%mass(i)
		ENDDO
		system%T_est=system%T_est/real(system%n_dof,wp)
		system%T_est_mean=system%T_est_mean + (system%T_est-system%T_est_mean)/real(n,wp)

	end subroutine update_T_est

	subroutine write_mean_temperature_info(system,parameters)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u

		OPEN(newunit=u,file=output%working_directory//"/mean_temperature.info")
			write(u,*) "T_est = ", system%T_est_mean*kelvin, "K"
			write(u,*) "T = ", parameters%temperature*kelvin, "K"
		close(u)
		write(*,*) "mean_temperature.info correctly written."

	end subroutine write_mean_temperature_info

!----------------------------------------------------------
! RESTART FILE

	subroutine cmd_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_initialize_restart_file(system,parameters,param_library)
		
	end subroutine cmd_initialize_restart_file

	subroutine cmd_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(in) :: system
		CLASS(CMD_PARAM), INTENT(in) :: parameters
		INTEGER, intent(in) :: n_steps

		call classical_MD_write_restart_file(system,parameters,n_steps)

	end subroutine cmd_write_restart_file

	subroutine cmd_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters

		call classical_MD_load_restart_file(system,parameters)

	end subroutine cmd_load_restart_file

END MODULE cmd_types