MODULE cmd_integrators
    USE cmd_types
	USE random
    USE thermostats, only : apply_thermostat
    USE basic_types
	USE cmd_forces, only : compute_cmd_forces, initialize_auxiliary_simulation
	USE nested_dictionaries

    IMPLICIT NONE

	PRIVATE :: apply_A,apply_B

	PROCEDURE(cmd_sub), pointer :: cmd_compute_forces => null()

CONTAINS

    ! INTEGRATORS

	subroutine cmd_ABOBA(system,parameters)
		IMPLICIT NONE
		CLASS(CMD_SYS), intent(inout) :: system
		CLASS(CMD_PARAM), intent(inout) :: parameters

		CALL cmd_compute_forces(system,parameters)

		CALL apply_B(system,parameters,parameters%dt/2._wp)		
		CALL apply_thermostat(system,parameters)
		CALL apply_B(system,parameters,parameters%dt/2._wp)	
		CALL apply_A(system,parameters,parameters%dt)

	end subroutine cmd_ABOBA

	subroutine cmd_BAOAB(system,parameters)
		IMPLICIT NONE
		CLASS(CMD_SYS), intent(inout) :: system
		CLASS(CMD_PARAM), intent(inout) :: parameters

		CALL apply_A(system,parameters,parameters%dt/2._wp)
		CALL apply_thermostat(system,parameters)
		CALL apply_A(system,parameters,parameters%dt/2._wp)

		CALL cmd_compute_forces(system,parameters)
		CALL apply_B(system,parameters,parameters%dt)		

	end subroutine cmd_BAOAB

	subroutine apply_A(system,parameters,tau)
		IMPLICIT NONE
		CLASS(CMD_SYS), intent(inout) :: system
		CLASS(CMD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + tau*system%P(:,i)/system%mass(:)
		enddo

	end subroutine apply_A

	subroutine apply_B(system,parameters,tau)
		IMPLICIT NONE
		CLASS(CMD_SYS), intent(inout) :: system
		CLASS(CMD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		
		system%P=system%P + tau*system%Forces
		
	end subroutine apply_B

!---------------------------------------------
! FORCES CALCULATORS

	subroutine cmd_initialize_forces(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		
		! ASSIGN FORCES CALCULATOR	
		cmd_compute_forces => compute_cmd_forces
		allocate(system%Forces(system%n_atoms,system%n_dim))
		call initialize_auxiliary_simulation(system,parameters,param_library)

		call cmd_compute_forces(system,parameters)

	end subroutine cmd_initialize_forces

END MODULE cmd_integrators    
