MODULE centroid_md
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE random
	USE cmd_types
	USE cmd_integrators
	USE thermostats, only : initialize_thermostat, finalize_thermostat
	USE classical_md, only: classical_MD_get_parameters, classical_md_initialize_momenta
	USE cmd_forces, only : close_beads_positions_file
	IMPLICIT NONE	

	TYPE(CMD_SYS), save, target :: cmd_system
	TYPE(CMD_PARAM), save, target :: cmd_parameters

	PROCEDURE(cmd_sub), pointer :: cmd_integrator

	PRIVATE :: cmd_system, cmd_parameters

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine cmd_loop()
		IMPLICIT NONE
		INTEGER :: i
		REAL :: time_start, time_finish
		

		if(cmd_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",cmd_parameters%n_steps_therm," steps &
						&of thermalization"
			call cpu_time(time_start)
			DO i=1,cmd_parameters%n_steps_therm
				call cmd_integrator(cmd_system,cmd_parameters)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			call cmd_write_restart_file(cmd_system,cmd_parameters &
						,cmd_parameters%n_steps_prev+cmd_parameters%n_steps_therm)
		endif

		write(*,*)
		write(*,*) "Starting ",cmd_parameters%n_steps," steps of CMD"
		call cpu_time(time_start)
		DO i=1,cmd_parameters%n_steps

			cmd_system%time=cmd_system%time &
								+cmd_parameters%dt

			call cmd_integrator(cmd_system,cmd_parameters)
			if(cmd_parameters%compute_temperature_estimation) &
				call update_T_est(cmd_system,cmd_parameters,i)

			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(cmd_parameters%write_restart) then
				if(mod(i,cmd_parameters%restart_stride)==0) &
					call cmd_write_restart_file(cmd_system,cmd_parameters &
						,cmd_parameters%n_steps_prev+i)
			endif

		ENDDO
		call cpu_time(time_finish)
		write(*,*) "Simulation done in ",time_finish-time_start,"s."

		if(cmd_parameters%write_mean_temperature_info) &
			call write_mean_temperature_info(cmd_system,cmd_parameters)

		call cmd_write_restart_file(cmd_system,cmd_parameters &
					,cmd_parameters%n_steps_prev+cmd_parameters%n_steps)	
		

	end subroutine cmd_loop
!-----------------------------------------------------------------

	subroutine initialize_cmd(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart

		cmd_parameters%engine="centroid_md"

		call cmd_initialize_restart_file(cmd_system,cmd_parameters,param_library)

		!ASSOCIATE classicla_MD_loop
		simulation_loop => cmd_loop

		!GET ALGORITHM
		cmd_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (CMD,default="nvt")
		cmd_parameters%algorithm=to_lower_case(cmd_parameters%algorithm)

		call cmd_get_parameters(cmd_system,cmd_parameters,param_library)

		call cmd_get_integrator_parameters(cmd_system,cmd_parameters,param_library)	

		call cmd_initialize_momenta(cmd_system,cmd_parameters)	

		! LOAD RESTART FILE IS NEEDED
		cmd_parameters%n_steps_prev=0
		load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
		if(load_restart) then
			call cmd_load_restart_file(cmd_system,cmd_parameters)
			cmd_parameters%n_steps_therm=0
		endif

		call cmd_initialize_forces(cmd_system,cmd_parameters, param_library)
		
		call cmd_initialize_output(cmd_system,cmd_parameters,param_library)
	end subroutine initialize_cmd

	subroutine finalize_cmd()
		IMPLICIT NONE

		call cmd_deallocate(cmd_system,cmd_parameters)
		call close_beads_positions_file()

	end subroutine finalize_cmd

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine cmd_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_get_parameters(system,parameters,param_library)

		allocate(system%X_prev(system%n_atoms,system%n_dim))
		system%X_prev=system%X

		system%T_est=0._wp ; system%T_est_mean=0._wp

	end subroutine cmd_get_parameters

	subroutine cmd_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters

		call classical_MD_initialize_momenta(system,parameters)

	end subroutine cmd_initialize_momenta

!-----------------------------------------------------------------

	subroutine cmd_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default="baoab")
		!! @input_file PARAMETERS/integrator (CMD,default="baoab")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt")
			
			call initialize_thermostat(system,parameters,param_library)

			SELECT CASE(parameters%integrator_type)
			CASE("aboba")
				cmd_integrator => cmd_ABOBA
			CASE("baoab")
				cmd_integrator => cmd_BAOAB
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT		

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine cmd_get_integrator_parameters

!-----------------------------------------------------------------

	subroutine cmd_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name
		INTEGER :: i_file, i
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%compute_temperature_estimation=.FALSE.
		parameters%write_mean_temperature_info=.FALSE.

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("centroid_position","position")
			!! @input_file OUTPUT/centroid_position (CMD)
				description="trajectory of centroid positions"
				default_name="X_centroid.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%X(:,1),index=i_file,name="centroid position")

			CASE("centroid_momentum","momentum")
			!! @input_file OUTPUT/centroid_momentum (CMD)			
				description="trajectory of centroid momenta"
				default_name="P_centroid.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%P(:,1),index=i_file,name="centroid momentum")
			
			CASE("temperature")
			!! @input_file OUTPUT/temperature (CMD)	
				parameters%compute_temperature_estimation=.TRUE.	
				description="trajectory of estimated temperature"
				default_name="temperature.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%T_est,index=i_file,name="estimated temperature")
			
			CASE("mean_temperature")
			!! @input_file OUTPUT/mean_temperature (CMD)	
				parameters%compute_temperature_estimation=.TRUE.
				parameters%write_mean_temperature_info=.TRUE.
			
			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

	end subroutine cmd_initialize_output

END MODULE centroid_md