MODULE cmd_forces
    USE kinds
    USE cmd_types
    USE rpmd_types
    USE nested_dictionaries
    USE string_operations
    USE atomic_units, only : convert_to_unit,bohr
    USE basic_types, only : Pot,dPot
    USE matrix_operations, only : sym_mat_inverse,fill_permutations
    USE random, only: RandGaussN
    USE file_handler, only: output
    USE rpmd_integrators, only:RPMD_potential_only
    IMPLICIT NONE


    TYPE, EXTENDS(RPMD_PARAM) :: CMD_FORCE_PARAM
        REAL(wp), ALLOCATABLE :: mu(:,:)
        REAL(wp), ALLOCATABLE :: OUsig(:,:,:)
        REAL(wp), ALLOCATABLE :: expOmk(:,:,:)

        REAL(wp), ALLOCATABLE :: dq(:,:)

        CHARACTER(:), ALLOCATABLE :: move_generator
        INTEGER :: n_samples
        REAL(wp), ALLOCATABLE :: sigp2_inv(:), sigp2(:)

        REAL(wp), ALLOCATABLE :: mass_expand(:)
        REAL(wp), ALLOCATABLE :: sqrt_mass(:)

        LOGICAL :: save_beads_positions

        INTEGER :: position_file_unit
    END TYPE

    TYPE WORK_VARIABLES
        REAL(wp), ALLOCATABLE :: F_s(:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:),EigV(:),tmp(:)
        REAL(wp), ALLOCATABLE :: R(:), R1(:), R2(:)
        REAL(wp), ALLOCATABLE :: muX(:,:,:),muP(:,:,:)
        REAL(wp), ALLOCATABLE :: X_new(:,:,:)
        REAL(wp) :: MC_energy_new, MC_Delta_energy_new
    END TYPE

    TYPE(POLYMER_TYPE), SAVE :: polymer
    TYPE(CMD_FORCE_PARAM), SAVE :: f_param
    TYPE(WORK_VARIABLES), SAVE :: WORK

    ABSTRACT INTERFACE
		subroutine move_proposal_type(system,polymer,move_accepted)
            IMPORT :: CMD_SYS,CMD_PARAM,POLYMER_TYPE
			IMPLICIT NONE
            TYPE(CMD_SYS), INTENT(IN) :: system
			TYPE(POLYMER_TYPE), INTENT(INOUT) :: polymer
			LOGICAL, INTENT(OUT) :: move_accepted
		end subroutine move_proposal_type
	END INTERFACE

    PROCEDURE(move_proposal_type), POINTER :: move_proposal => NULL()

    PRIVATE
    PUBLIC :: compute_cmd_forces, initialize_auxiliary_simulation, close_beads_positions_file

CONTAINS

!--------------------------------------------------------------
! FORCE CALCULATIONS

    subroutine compute_cmd_forces(system,parameters)
        IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters

        if(ANY(ISNAN(system%X))) STOP "ERROR: system diverged!"

        call sample_forces(system,parameters,polymer)
            
     !   write(*,*) "Forces=",system%Forces
        system%X_prev=system%X       

    end subroutine compute_cmd_forces
    
!--------------------------------------------------------------
! SAMPLING LOOP
    subroutine sample_forces(system,parameters,polymer)
        IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
        CLASS(POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: m,is,count
        LOGICAL :: move_accepted
        REAL(wp) :: P

        P=real(polymer%n_beads,wp)

        call reinitialize_polymer_config(system,polymer)
        WORK%F_s=0

        !THERMALIZATION
        DO m=1,f_param%n_steps_therm
            CALL move_proposal(system,polymer,move_accepted)
        ENDDO

        !PRODUCTION
        count=0
        DO is=1,f_param%n_samples ; DO m=1,f_param%n_steps

            count=count+1
            !GENERATE A NEW CONFIG
            CALL move_proposal(system,polymer,move_accepted)
            !UPDATE MEAN VALUES
            WORK%F_s(:,:,is)=WORK%F_s(:,:,is) + (SUM(polymer%F_beads,dim=1)/P-WORK%F_s(:,:,is))/m
            SYSTEM%E_pot = SYSTEM%E_pot + (polymer%E_pot - SYSTEM%E_pot)/count

        ENDDO ; ENDDO

        if(f_param%save_beads_positions) &
            call write_beads_positions()

        system%Forces=SUM(WORK%F_s,dim=3)/real(f_param%n_samples,wp)

    end subroutine sample_forces

!--------------------------------------------------------------
! MOVE GENERATORS
    
    !-----------------------------------
    ! PIOUD steps
    subroutine PIOUD_step(system,polymer,move_accepted)
		IMPLICIT NONE
        TYPE(CMD_SYS), INTENT(IN) :: system
		TYPE(POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
		integer :: i,j,nu

		nu=f_param%n_beads
        
       ! write(*,*) "before=",system%X, SUM(polymer%X_beads,dim=1)/real(f_param%n_beads,wp)
		do j=1,system%n_dim ; do i=1,system%n_atoms
			!TRANSFORM IN NORMAL MODES	
                WORK%EigX(:)=polymer%X_beads(1:nu,i,j)*f_param%sqrt_mass(i)
                ! WORK%EigX=matmul(f_param%EigMatTr,WORK%EigX)
                call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigX,1,0._8,WORK%tmp,1)
                WORK%EigX=WORK%tmp
                WORK%EigV=polymer%P_beads(1:nu,i,j)/f_param%sqrt_mass(i)
                ! EigV=matmul(EigMatTr,EigV)
                call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigV,1,0._8,WORK%tmp,1)
                WORK%EigV=WORK%tmp	

			!GENERATE RANDOM VECTORS
			CALL RandGaussN(WORK%R1)
			CALL RandGaussN(WORK%R2)

            polymer%P_beads(1,i,j)=0._wp
            polymer%X_beads(1,i,j)=WORK%EigX(1)

			!PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
			polymer%P_beads(2:,i,j)=WORK%EigV(2:)*f_param%expOmk(2:,1,1) &
                +WORK%EigX(2:)*f_param%expOmk(2:,1,2) &
                +f_param%OUsig(2:,1,1)*WORK%R1(:)

			polymer%X_beads(2:,i,j)=WORK%EigV(2:)*f_param%expOmk(2:,2,1) &
                +WORK%EigX(2:)*f_param%expOmk(2:,2,2) &
                +f_param%OUsig(2:,2,1)*WORK%R1(:)+f_param%OUsig(2:,2,2)*WORK%R2(:)
			
			!TRANSFORM BACK IN COORDINATES
                ! polymer%X_beads(1:nu,i,j)=matmul(f_param%EigMat,polymer%X_beads(1:nu,i,j))/f_param%sqrt_mass(i)
                call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%X_beads(1:nu,i,j),1,0._8,WORK%tmp,1)
                polymer%X_beads(1:nu,i,j)=WORK%tmp/f_param%sqrt_mass(i)
                ! polymer%P_beads(:,i)=matmul(f_param%EigMat,polymer%P_beads(:,i))*f_param%sqrt_mass(i)
                call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%P_beads(1:nu,i,j),1,0._8,WORK%tmp,1)
                polymer%P_beads(1:nu,i,j)=WORK%tmp*f_param%sqrt_mass(i)

		enddo ; enddo


		!APPLY FORCES	        
		CALL apply_forces(polymer,f_param%dt)
         !write(*,*) "after=",system%X, SUM(polymer%X_beads,dim=1)/real(f_param%n_beads,wp)


        ! CENTROID CONSTRAINT
        f_param%dq=system%X - sum(polymer%X_beads,dim=1)/real(nu,wp)
      !  write(*,*) system%X, sum(polymer%X_beads,dim=1)/real(nu,wp)
        !  DO i=1,nu
        !      polymer%X_beads(i,:,:)=polymer%X_beads(i,:,:)+f_param%dq
        !  ENDDO

		move_accepted=.TRUE.
	end subroutine PIOUD_step

	subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: tau
        INTEGER :: i

        call RPMD_potential_only(polymer,f_param)
        do i=1,f_param%n_beads
           ! polymer%F_beads(i,:,:)=-dPot(polymer%X_beads(i,:,:))
			polymer%P_beads(i,:,:)=polymer%P_beads(i,:,:)+tau*polymer%F_beads(i,:,:)
		enddo
        
	end subroutine apply_forces
!--------------------------------------------------------------
! VARIOUS INITIALIZATIONS

    subroutine reinitialize_polymer_config(system,polymer)
        IMPLICIT NONE
		CLASS(CMD_SYS), INTENT(inout) :: system
        CLASS(POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i

        f_param%dq=system%X-system%X_prev
       ! write(*,*) "dq=",f_param%dq
        DO i=1,f_param%n_beads
            polymer%X_beads(i,:,:)=polymer%X_beads(i,:,:)+f_param%dq(:,:)
        ENDDO  
        !write(*,*) "initial=",system%X, SUM(polymer%X_beads,dim=1)/real(f_param%n_beads,wp)

        SELECT CASE(f_param%move_generator)
        CASE("PIOUD")
            CALL apply_forces(polymer,0.5_wp*f_param%dt)
        END SELECT

    end subroutine reinitialize_polymer_config

    subroutine initialize_auxiliary_simulation(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
        INTEGER :: n_dof,n_beads,n_atoms,n_dim
        INTEGER :: i,j,k,n_therm_tmp

        n_dof=system%n_dof
        f_param%n_dof=n_dof
        n_atoms=system%n_atoms
        f_param%n_atoms=n_atoms
        n_dim=system%n_dim
        f_param%n_dim=n_dim

        allocate(f_param%mass_expand(n_dof) &
                ,f_param%sqrt_mass(n_atoms))
        DO i=1,n_dim
            f_param%mass_expand(n_atoms*(i-1)+1:n_atoms*i)=system%mass(:)
        ENDDO
        f_param%sqrt_mass=SQRT(system%mass)

        !GET NUMBER OF BEADS
        n_beads=param_library%get("PARAMETERS/n_beads")
        !! @input_file PARAMETERS/n_beads (CMD)
		if(n_beads<=0) STOP "Error: 'n_beads' of 'PARAMETERS' is not properly defined!"
			polymer%n_beads=n_beads
			f_param%n_beads=n_beads
        
        !GET NUMBER OF AUXILIARY SIMULATION STEPS
        f_param%n_steps=param_library%get(CMD_CAT//"/n_steps")
        !! @input_file CMD_AUX/n_steps (CMD)
        if(f_param%n_steps<=0) &
            STOP "Error: 'n_steps' of '"//CMD_CAT//"' is not properly defined!"

        f_param%n_steps_therm=param_library%get(CMD_CAT//"/n_steps_therm",default=0)
        !! @input_file CMD_AUX/n_steps_therm (CMD, default=0)
        if(f_param%n_steps_therm<0) &
            STOP "Error: 'n_steps_therm' of '"//CMD_CAT//"' is not properly defined!"
        
        f_param%n_samples=param_library%get(CMD_CAT//"/n_samples",default=1)
        !! @input_file CMD_AUX/n_samples (CMD, default=1)
        if(f_param%n_samples<=0) &
            STOP "Error: 'n_samples' of '"//CMD_CAT//"' is not properly defined!"

        !INITIALIZE FORCES PARAMETERS
        allocate( &
            f_param%EigMat(n_beads,n_beads), &
            f_param%EigMatTr(n_beads,n_beads), &
            f_param%OmK(n_beads), &
            f_param%dq(n_atoms,n_dim), &
            f_param%sigp2(n_atoms), &
            f_param%sigp2_inv(n_atoms) &
        )
        f_param%dq=0._wp
        f_param%dBeta=parameters%beta/REAL(f_param%n_beads,wp)
        f_param%sigp2(:)=f_param%dBeta/system%mass(:)
        f_param%sigp2_inv(:)=1._wp/f_param%sigp2

        !INITIALIZE WORK VARIABLES
        allocate( &
            WORK%F_s(n_atoms,n_dim,f_param%n_samples), &
            WORK%EigX(n_beads), &
            WORK%EigV(n_beads), &
            WORK%R(n_atoms), &
            WORK%R1(n_beads-1), &
            WORK%R2(n_beads-1), &
            WORK%tmp(n_beads) &
        )

        !INITIALIZE polymers
            allocate( &
                polymer%X_beads(n_beads,n_atoms,n_dim), &
                polymer%P_beads(n_beads,n_atoms,n_dim), &
                polymer%F_beads(n_beads,n_atoms,n_dim), &
                polymer%Epot_beads(n_beads) &
            )
            DO j=1,n_beads
                polymer%X_beads(j,:,:)=system%X(:,:)
                DO k=1,n_dim
                    call randGaussN(polymer%P_beads(j,:,k))
                    polymer%P_beads(j,:,k)=polymer%P_beads(j,:,k)*SQRT(f_param%sigp2_inv(:))
                ENDDO
            ENDDO
            system%X_prev=system%X

        call initialize_move_generator(system,parameters,param_library)

        n_therm_tmp = f_param%n_steps_therm
		f_param%n_steps_therm = 10* n_therm_tmp
		call compute_cmd_forces(system,parameters)
		f_param%n_steps_therm = n_therm_tmp
        
        f_param%save_beads_positions=param_library%get(CMD_CAT//"/save_beads_positions",default=.FALSE.)
        !! @input_file CMD_AUX/save_beads_positions (CMD, default=.FALSE.)
        if(f_param%save_beads_positions) &
            call open_beads_positions_file()

    end subroutine initialize_auxiliary_simulation

    subroutine initialize_move_generator(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(CMD_SYS), INTENT(inout) :: system
		CLASS(CMD_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library

        f_param%move_generator=param_library%get(CMD_CAT//"/move_generator" &
                    ,default="PIOUD")
        !! @input_file CMD_AUX/move_generator (CMD, default="PIOUD")
        f_param%move_generator=to_upper_case(f_param%move_generator)
        parameters%move_generator=f_param%move_generator

        SELECT CASE(f_param%move_generator)
        CASE ("PIOUD")

            move_proposal => PIOUD_step
            !INITIALIZE PIOUD
            f_param%dt=param_library%get(CMD_CAT//"/dt",default=parameters%dt)
            !! @input_file CMD_AUX/dt (CMD, if move_generator="PIOUD")
            CALL initialize_PIOUD(system)

        CASE ("DE_BROGLIE_MC")

            STOP "Error: DE_BROGLIE_MC not implemented for CMD yet..."
            
        CASE DEFAULT
            write(0,*) "Error: unknown move generator '"//f_param%move_generator//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_move_generator

    subroutine initialize_PIOUD(system)
        IMPLICIT NONE
        CLASS(CMD_SYS), INTENT(inout) :: system
        INTEGER :: i,nu,INFO
		REAL(wp), ALLOCATABLE :: TMP(:),OUsig2(:,:,:)
		REAL(wp) :: Id(2,2), Om0

        nu=f_param%n_beads

        Id=0
		Id(1,1)=1
		Id(2,2)=1

		Om0=1._wp/f_param%dBeta
        allocate(TMP(3*nu-1),OUsig2(nu,2,2))
		allocate( &
            f_param%OUsig(nu,2,2), &
            f_param%expOmk(nu,2,2) &
        )

        !INITIALIZE DYNAMICAL MATRIX
		f_param%EigMat=0
		do i=1,nu-1
			f_param%EigMat(i,i)=2
			f_param%EigMat(i+1,i)=-1
			f_param%EigMat(i,i+1)=-1
		enddo
		f_param%EigMat(nu,nu)=2
		f_param%EigMat(nu-1,nu)=-1
		f_param%EigMat(nu,nu-1)=-1
		f_param%EigMat(1,nu)=-1
		f_param%EigMat(nu,1)=-1

        !SOLVE EIGENPROBLEM
		CALL DSYEV('V','L',nu,f_param%EigMat,nu,f_param%Omk,TMP,3*nu-1,INFO)
		if(INFO/=0) then
			write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
			stop 
		endif

        f_param%Omk=Om0*SQRT(f_param%Omk)
		write(*,*)
		write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
		do i=1,nu
			write(*,*) i, f_param%Omk(i)*THz/(2*pi),"THz"
		enddo
		write(*,*)

        f_param%EigMatTr=transpose(f_param%EigMat)

		f_param%expOmk(:,1,1)=exp(-f_param%Omk(:)*f_param%dt) &
                                *(1-f_param%Omk(:)*f_param%dt)
		f_param%expOmk(:,1,2)=exp(-f_param%Omk(:)*f_param%dt) &
                                *(-f_param%dt*f_param%Omk(:)**2)
		f_param%expOmk(:,2,1)=exp(-f_param%Omk(:)*f_param%dt)*f_param%dt
		f_param%expOmk(:,2,2)=exp(-f_param%Omk(:)*f_param%dt) &
                                *(1+f_param%Omk(:)*f_param%dt)

        !COMPUTE COVARIANCE MATRIX
        OUsig2=0._wp
		OUsig2(2:nu,1,1)=(1._wp- (1._wp-2._wp*f_param%dt*f_param%Omk(2:) &
                            +2._wp*(f_param%dt*f_param%Omk(2:))**2 &
                          )*exp(-2._wp*f_param%Omk(2:)*f_param%dt) &
                        )/f_param%dBeta
		OUsig2(2:nu,2,2)=(1._wp- (1._wp+2._wp*f_param%dt*f_param%Omk(2:) &
                            +2._wp*(f_param%dt*f_param%Omk(2:))**2 &
                          )*exp(-2._wp*f_param%Omk(2:)*f_param%dt) &
                        )/(f_param%dBeta*f_param%Omk(2:)**2)
		OUsig2(2:nu,2,1)=2._wp*f_param%Omk(2:)*(f_param%dt**2) &
                        *exp(-2._wp*f_param%Omk(2:)*f_param%dt)/f_param%dBeta
		OUsig2(2:nu,1,2)=OUsig2(2:,2,1)

        !COMPUTE CHOLESKY DECOMPOSITION
        f_param%OUsig=0._wp
		f_param%OUsig(2:nu,1,1)=sqrt(OUsig2(2:nu,1,1))
		f_param%OUsig(2:nu,1,2)=0
		f_param%OUsig(2:nu,2,1)=OUsig2(2:nu,2,1)/f_param%OUsig(2:,1,1)
		f_param%OUsig(2:nu,2,2)=sqrt(OUsig2(2:nu,2,2)-f_param%OUsig(2:nu,2,1)**2)

		!CHECK CHOLESKY DECOMPOSITION
		! write(*,*) "sum of squared errors on cholesky decomposition:"
		! do i=1,nu
		! 	write(*,*) i, sum( (matmul(f_param%OUsig(i,:,:),transpose(f_param%OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
		! enddo

    end subroutine initialize_PIOUD

!--------------------------------------------------
! BEADS POSITIONS FILE

    subroutine open_beads_positions_file()
        IMPLICIT NONE

        OPEN(newunit=f_param%position_file_unit &
            ,file=output%working_directory//"/X_beads.out" &
            , form="unformatted", access="stream")
        write(*,*) "X_beads.out correctly opened"

    end subroutine open_beads_positions_file

    subroutine close_beads_positions_file()
        IMPLICIT NONE

        CLOSE(f_param%position_file_unit)
        write(*,*) "X_beads.out correctly closed"

    end subroutine close_beads_positions_file

    subroutine write_beads_positions()
        IMPLICIT NONE
        INTEGER :: i

        DO i=1,f_param%n_beads
            write(f_param%position_file_unit) polymer%X_beads(i,:,:)*bohr
        ENDDO

    end subroutine write_beads_positions

END MODULE cmd_forces