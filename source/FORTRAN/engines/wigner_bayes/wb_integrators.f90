MODULE wb_integrators
    USE wb_types
	USE random
   ! USE thermostats, only : apply_thermostat
    USE basic_types
	USE wb_forces, only : compute_wigner_forces, initialize_auxiliary_simulation, compute_EW
	USE wb_gaussian_forces
	USE nested_dictionaries
	USE wb_spectrum_analysis
    IMPLICIT NONE

	PRIVATE :: apply_A,apply_B, apply_C

	PROCEDURE(wig_sub), pointer :: wig_compute_forces => null()
	PROCEDURE(wig_sub), pointer :: wig_compute_EW => null()

CONTAINS

    ! INTEGRATORS

	subroutine wb_ABOBA(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters		
		
		call wig_compute_forces(system,parameters)
		CALL apply_C(system,parameters,0.5_wp*parameters%dt)	
		CALL apply_B(system,parameters,0.5_wp*parameters%dt)	

		CALL apply_O_alt(system,parameters,parameters%dt)

		CALL apply_B(system,parameters,0.5_wp*parameters%dt)	
		CALL apply_C(system,parameters,0.5_wp*parameters%dt)	
		CALL apply_A(system,parameters,parameters%dt)

	end subroutine wb_ABOBA

	subroutine apply_A(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		system%X=system%X + tau*system%P/system%mass

	end subroutine apply_A

	subroutine apply_B(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: P_half,W
		INTEGER :: k
		
		! W=system%P*system%C*system%P
		! P_half=system%P+0.5_wp*tau*(system%F+W)

		! W=P_half*system%C*P_half
		! system%P=system%P+tau*(system%F+W)
		system%P=system%P+tau*system%F
		
	end subroutine apply_B

	subroutine apply_C(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: P_half
		INTEGER :: k
		
		system%P=system%P/(1._wp-tau*system%C*system%P)
		
	end subroutine apply_C

	subroutine apply_O(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: R,expg,frict

		 call RandGauss(R)
		! system%P=system%P + tau*( &
		! 		-0.5_wp*system%k2*system%P*parameters%sigma**2 &
		! 		+parameters%sigma*R/sqrt(tau) &
		! 	)
		if(parameters%noise_correction) then
			frict=0.5_wp*system%k2*(parameters%sigma**2+system%sigf2)
		else
			frict=0.5_wp*system%k2*parameters%sigma**2
		endif

		
		expg=exp(-tau*frict)
		system%rand_force = parameters%sigma*sqrt( &
						(1._wp-expg*expg) / (2._wp*frict) &
					)*R
			
		if(parameters%compute_fdt) then
			system%P = system%P*sqrt(expg) + 0.5_wp*system%rand_force
			call spectrum_analyser%update(system,parameters)
			system%P = system%P*sqrt(expg) + 0.5_wp*system%rand_force
		else
			system%P = system%P*expg + system%rand_force
		endif
		
	end subroutine apply_O

	subroutine apply_O_alt(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: R,expg,frict

		 call RandGauss(R)
		! system%P=system%P + tau*( &
		! 		-0.5_wp*system%k2*system%P*parameters%sigma**2 &
		! 		+parameters%sigma*R/sqrt(tau) &
		! 	)

		expg=exp(-tau*parameters%gamma0)
		system%rand_force = sqrt(system%k2inv*(1._wp-expg*expg))*R
			
		if(parameters%compute_fdt) then
			system%P = system%P*sqrt(expg) + 0.5_wp*system%rand_force
			call spectrum_analyser%update(system,parameters)
			system%P = system%P*sqrt(expg) + 0.5_wp*system%rand_force
		else
			system%P = system%P*expg + system%rand_force
		endif
		
	end subroutine apply_O_alt

!---------------------------------------------
! FORCES CALCULATORS

	subroutine wb_initialize_forces(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		system%k2=0
		system%k4=0
		system%F=0
		system%k2inv=0
		
		! ASSIGN FORCES CALCULATOR	
		parameters%gaussian_forces=param_library%get(WIGNER_CAT//"/gaussian_forces",default=.FALSE.)
		if(parameters%gaussian_forces) then
			wig_compute_forces => compute_gaussian_forces
			wig_compute_EW => compute_EW_gaussian
			call initialize_gaussian_forces(system,parameters,param_library)
		else
			wig_compute_forces => compute_wigner_forces
			wig_compute_EW => compute_EW
			call initialize_auxiliary_simulation(system,parameters,param_library)
		endif

		!call wig_compute_forces(system,parameters)

	end subroutine wb_initialize_forces

END MODULE wb_integrators    
