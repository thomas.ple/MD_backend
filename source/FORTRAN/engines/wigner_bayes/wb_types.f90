MODULE wb_types
	USE kinds
	USE classical_md_types
	USE nested_dictionaries
	USE file_handler, only: output
	USE rpmd_types
    IMPLICIT NONE

	CHARACTER(*), PARAMETER :: WIGNER_CAT="WIGNER_AUX"

    TYPE :: WIG_SYS_1D

		REAL(wp) :: time, mass

		REAL(wp) :: X,P
		REAL(wp) :: X_prev

		REAL(wp) :: EW
		REAL(wp) :: EW6 !ONLY FOR 1D SYSTEMS

		REAL(wp) :: F,F_var
		REAL(wp) :: C
		REAL(wp) :: k2, k2_var, delta2_var
		REAL(wp) :: k2inv
		REAL(wp) :: k4
		REAL(wp) :: sigf2
		REAL(wp) :: rand_force
				
		!ONLY FOR 1D SYSTEMS
		REAL(wp) :: k6,beta_dU, dk2, dC4,C4_beta_dU,C4
		REAL(wp) :: acc_ratio, acc_ratio_delta, acc_ratio_staging
		REAL(wp) :: acc_ratio_mts
 
	END TYPE

	TYPE,EXTENDS(CLASS_MD_PARAM) :: WIG_PARAM_1D
		REAL(wp) :: gamma0
		REAL(wp) :: sigma
		INTEGER :: n_MTS
		CHARACTER(:), ALLOCATABLE :: move_generator
		LOGICAL :: only_auxiliary_simulation
		LOGICAL :: gaussian_forces
		LOGICAL :: noise_correction

		LOGICAL :: compute_fdt
		INTEGER :: fdt_stride
	END TYPE


	TYPE :: WIG_POLYMER_TYPE
        INTEGER :: n_beads

        REAL(wp), ALLOCATABLE :: X(:)
        REAL(wp), ALLOCATABLE :: P(:)

        REAL(wp) :: Delta
        REAL(wp) :: F_spring,F_delta,F_beads, F_beadsD2
        REAL(wp) :: F_springD2,F_deltaD2
        REAL(wp), ALLOCATABLE :: Pot(:)
        REAL(wp) :: dDelta2

        REAL(wp) :: k2
		REAL(wp) :: k2inv
		REAL(wp) :: k4
		REAL(wp) :: F
        REAL(wp) :: F_var
		REAL(wp) :: G
        REAL(wp) :: k2_var
        REAL(wp) :: delta2_var

        !ONLY FOR 1D SYSTEMS
        REAL(wp) :: k6 
        REAL(wp) :: G4

        REAL(wp) :: MC_energy
        REAL(wp) :: MC_Delta_energy, MC_beads_energy
        REAL(wp) :: acc_ratio, acc_ratio_delta, acc_ratio_staging
        INTEGER :: N_moves_delta, N_moves_staging
    END TYPE

    TYPE, EXTENDS(RPMD_PARAM) :: WIG_FORCE_PARAM
        REAL(wp), ALLOCATABLE :: mu(:,:)
        REAL(wp), ALLOCATABLE :: OUsig(:,:,:)
        REAL(wp), ALLOCATABLE :: expOmk(:,:,:)

        REAL(wp) :: k2_prev

        REAL(wp) :: sigp2
        REAL(wp) :: sigp2_inv
        REAL(wp) :: deBroglie
        REAL(wp) :: DeltaMult
        REAL(wp) :: deltaGaussCorr
        REAL(wp) :: a_delta, acc_ratio_target
        INTEGER :: L_staging
        REAL(wp) :: Delta_prob

        REAL(wp) :: dq

        CHARACTER(:), ALLOCATABLE :: move_generator
        INTEGER :: n_samples

        REAL(wp) :: mass_expand
        REAL(wp) :: sqrt_mass

        LOGICAL :: write_aux_traj
        INTEGER :: traj_unit

         LOGICAL :: write_beads_traj
        INTEGER :: beads_traj_unit

        LOGICAL :: spring_force

		REAL(wp) :: delta_mass_factor, sqrt_delta_mass_factor
    END TYPE

    TYPE WORK_VARIABLES
        REAL(wp) :: Forces1,Forces2
        REAL(wp) :: V
        REAL(wp) :: F
        REAL(wp), ALLOCATABLE :: F_s(:)
        REAL(wp) :: F_delta, F_beads, F_spring
        REAL(wp) :: Delta,Delta2
        REAL(wp) :: Delta4
        REAL(wp), ALLOCATABLE :: k2_s(:)
        REAL(wp), ALLOCATABLE :: k4_s(:)
        REAL(wp) :: dDelta,Delta_new
        REAL(wp), ALLOCATABLE :: EigX(:),EigV(:),tmp(:)
        REAL(wp), ALLOCATABLE :: R(:), R1(:), R2(:)
        REAL(wp), ALLOCATABLE :: muX(:),muP(:)
        REAL(wp), ALLOCATABLE :: X_new(:)
        REAL(wp) :: MC_energy_new, MC_Delta_energy_new
    END TYPE	

	ABSTRACT INTERFACE
		subroutine wig_sub(system,parameters)
			import :: WIG_SYS_1D, WIG_PARAM_1D
			IMPLICIT NONE
			CLASS(WIG_SYS_1D), intent(inout) :: system
			CLASS(WIG_PARAM_1D), intent(inout) :: parameters
		end subroutine wig_sub

		subroutine wig_sub_int(system,parameters,n)
			import :: WIG_SYS_1D, WIG_PARAM_1D
			IMPLICIT NONE
			CLASS(WIG_SYS_1D), intent(inout) :: system
			CLASS(WIG_PARAM_1D), intent(inout) :: parameters
			INTEGER, INTENT(in) :: n
		end subroutine wig_sub_int
	END INTERFACE

	TYPE(WIG_FORCE_PARAM), SAVE :: f_param
    TYPE(WORK_VARIABLES), SAVE :: WORK
	INTEGER :: NUM_AUX_SIM=3    

CONTAINS


	subroutine wb_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters

		!call classical_MD_deallocate(system,parameters)
		!!! TO COMPLETE !!! 

	end subroutine wb_deallocate

!----------------------------------------------------------
! RESTART FILE

	subroutine wb_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		!call classical_MD_initialize_restart_file(system,parameters,param_library)
		
	end subroutine wb_initialize_restart_file

	subroutine wb_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(in) :: system
		CLASS(WIG_PARAM_1D), INTENT(in) :: parameters
		INTEGER, intent(in) :: n_steps
		INTEGER :: u

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			WRITE(u,*) parameters%engine
			WRITE(u,*) system%time
			WRITE(u,*) n_steps
			WRITE(u,*) parameters%sigma
			WRITE(u,*) system%X
			WRITE(u,*) system%P		
		close(u)

		write(*,*) "I/O : save config OK."

	end subroutine wb_write_restart_file

	subroutine wb_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		INTEGER :: u
		CHARACTER(100) :: engine

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			READ(u,*) engine
			if(engine/=parameters%engine) then
				write(0,*) "Error: tried to restart engine '",parameters%engine &
							,"' with a file from '",engine,"' !"
				STOP "Execution stopped"
			endif
			READ(u,*) system%time
			READ(u,*) parameters%n_steps_prev
			READ(u,*) parameters%sigma
			READ(u,*) system%X
			READ(u,*) system%P		
		close(u)

		write(*,*) "I/O : configuration retrieved successfully."

	end subroutine wb_load_restart_file

	function mat(x)
        REAL(wp), INTENT(in) :: x
        REAL(wp) :: mat(1,1)
        mat=x
    end function mat

    function scal(x)
        REAL(wp), INTENT(in) :: x(:,:)
        REAL(wp) :: scal
        scal=x(1,1)
    end function scal

END MODULE wb_types