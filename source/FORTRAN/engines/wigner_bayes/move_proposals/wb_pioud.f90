MODULE wb_pioud
	USE kinds
	use wb_types
	USE basic_types, only : Pot,dPot
	USE nested_dictionaries
	USE random, only: RandGaussN, randGauss
	IMPLICIT NONE

	PRIVATE 
	PUBLIC :: PIOUD_step, initialize_PIOUD, reinitialize_PIOUD

CONTAINS

!-----------------------------------
    ! PIOUD steps
    subroutine PIOUD_step(system,polymer,move_accepted)
		IMPLICIT NONE
        TYPE(WIG_SYS_1D), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
		integer :: i,j,nu

		nu=f_param%n_beads

        !TRANSFORM IN NORMAL MODES	
            WORK%EigX(:)=polymer%X(1:nu)*f_param%sqrt_mass
            WORK%EigX(nu)=f_param%sqrt_delta_mass_factor*WORK%EigX(nu)
            ! WORK%EigX=matmul(f_param%EigMatTr,WORK%EigX)
            call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigX,1,0._8,WORK%tmp,1)
            WORK%EigX=WORK%tmp
            WORK%EigV=polymer%P(1:nu)/f_param%sqrt_mass
            WORK%EigV(nu)=WORK%EigV(nu)/f_param%sqrt_delta_mass_factor
            ! EigV=matmul(EigMatTr,EigV)
            call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigV,1,0._8,WORK%tmp,1)
            WORK%EigV=WORK%tmp	

        !GENERATE RANDOM VECTORS
        CALL RandGaussN(WORK%R1)
        CALL RandGaussN(WORK%R2)

        !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
        polymer%P(:)=WORK%EigV(:)*f_param%expOmk(:,1,1) &
            +WORK%EigX(:)*f_param%expOmk(:,1,2) &
            +f_param%OUsig(:,1,1)*WORK%R1(:) &
            +WORK%muP(:)

        polymer%X(1:nu)=WORK%EigV(:)*f_param%expOmk(:,2,1) &
            +WORK%EigX(:)*f_param%expOmk(:,2,2) &
            +f_param%OUsig(:,2,1)*WORK%R1(:)+f_param%OUsig(:,2,2)*WORK%R2(:) &
            +WORK%muX(:)
        
        !TRANSFORM BACK IN COORDINATES
            ! polymer%X(1:nu)=matmul(f_param%EigMat,polymer%X(1:nu))/f_param%sqrt_mass
            call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%X(1:nu),1,0._8,WORK%tmp,1)
            polymer%X(1:nu)=WORK%tmp/f_param%sqrt_mass
            polymer%X(nu)=polymer%X(nu)/f_param%sqrt_delta_mass_factor
            ! polymer%P(:)=matmul(f_param%EigMat,polymer%P(:))*f_param%sqrt_mass
            call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%P(1:nu),1,0._8,WORK%tmp,1)
            polymer%P(1:nu)=WORK%tmp*f_param%sqrt_mass
            polymer%P(nu)=f_param%sqrt_delta_mass_factor*polymer%P(nu)


		polymer%Delta=polymer%X(nu)

		!APPLY FORCES		
		CALL apply_forces(system%X,polymer,f_param%dt)

		move_accepted=.TRUE.
	end subroutine PIOUD_step

	subroutine apply_forces(q,polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,f_param%n_beads-1
			polymer%P(i)=polymer%P(i)-tau*scal(dPot(mat(polymer%X(i))))
		enddo
		polymer%P(f_param%n_beads)=polymer%P(f_param%n_beads) - 0.25_wp*tau* &
			( scal(dPot(mat(q+0.5_wp*polymer%Delta))) - scal(dPot(mat(q-0.5_wp*polymer%Delta))) )
	end subroutine apply_forces

	 subroutine initialize_PIOUD(polymers,system,parameters,wig_lib)
        IMPLICIT NONE
		CLASS(WIG_POLYMER_TYPE), DIMENSION(:), INTENT(INOUT) :: polymers
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,nu,INFO
		REAL(wp), ALLOCATABLE :: TMP(:),thetaInv(:,:,:),OUsig2(:,:,:)
		REAL(wp) :: Id(2,2), Om0

        f_param%dt=wig_lib%get("dt",default=parameters%dt)
        !! @input_file WIGNER_AUX/dt (wigner, default= PARAMETERS/dt, if move_generator="PIOUD")
		f_param%delta_mass_factor=wig_lib%get("delta_mass_factor",default=0.25_wp)
		!! @input_file WIGNER_AUX/delta_mass_factor (wigner, default= 0.25, if move_generator="PIOUD")
		f_param%sqrt_delta_mass_factor=SQRT(f_param%delta_mass_factor)

        nu=f_param%n_beads

        Id=0
		Id(1,1)=1
		Id(2,2)=1

		Om0=1._wp/f_param%dBeta
        allocate(TMP(3*nu-1),thetaInv(nu,2,2),OUsig2(nu,2,2))
		allocate( &
            f_param%mu(nu,2), &
            f_param%OUsig(nu,2,2), &
            f_param%expOmk(nu,2,2) &
        )
        allocate( &
            WORK%muX(nu), &
            WORK%muP(nu) &
        )

        !INITIALIZE DYNAMICAL MATRIX
		f_param%EigMat=0
		do i=1,nu-1
			f_param%EigMat(i,i)=2
			f_param%EigMat(i+1,i)=-1
			f_param%EigMat(i,i+1)=-1
		enddo
		f_param%EigMat(nu,nu)=0.5_wp/f_param%delta_mass_factor
		f_param%EigMat(nu-1,nu)=0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(nu,nu-1)=0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(1,nu)=-0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(nu,1)=-0.5_wp/f_param%sqrt_delta_mass_factor

        !SOLVE EIGENPROBLEM
		CALL DSYEV('V','L',nu,f_param%EigMat,nu,f_param%Omk,TMP,3*nu-1,INFO)
		if(INFO/=0) then
			write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
			stop 
		endif

        f_param%Omk=Om0*SQRT(f_param%Omk)
		write(*,*)
		write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
		do i=1,nu
			write(*,*) i, f_param%Omk(i)*THz/(2*pi),"THz"
		enddo
		write(*,*)

        f_param%EigMatTr=transpose(f_param%EigMat)

		f_param%expOmk(:,1,1)=exp(-f_param%Omk(:)*f_param%dt) &
                                *(1-f_param%Omk(:)*f_param%dt)
		f_param%expOmk(:,1,2)=exp(-f_param%Omk(:)*f_param%dt) &
                                *(-f_param%dt*f_param%Omk(:)**2)
		f_param%expOmk(:,2,1)=exp(-f_param%Omk(:)*f_param%dt)*f_param%dt
		f_param%expOmk(:,2,2)=exp(-f_param%Omk(:)*f_param%dt) &
                                *(1+f_param%Omk(:)*f_param%dt)

		thetaInv(:,1,1)=0._wp
		thetaInv(:,1,2)=-1._wp
		thetaInv(:,2,1)=1._wp/f_param%Omk(:)**2
		thetaInv(:,2,2)=2._wp/f_param%Omk(:)

        !COMPUTE MEAN VECTOR
		f_param%mu=0
		f_param%mu(1,1)=Om0**2
		f_param%mu(nu-1,1)=Om0**2
		f_param%mu(:,1)=matmul(f_param%EigMatTr,f_param%mu(:,1))
		f_param%mu(:,2)=0._wp
		do i=1,nu
			thetaInv(i,:,:)=matmul((Id(:,:)-f_param%expOmk(i,:,:)),thetaInv(i,:,:))
			f_param%mu(i,:)=matmul(thetaInv(i,:,:),f_param%mu(i,:))
		enddo

        !COMPUTE COVARIANCE MATRIX
		OUsig2(:,1,1)=(1._wp- (1._wp-2._wp*f_param%dt*f_param%Omk(:) &
                            +2._wp*(f_param%dt*f_param%Omk(:))**2 &
                          )*exp(-2._wp*f_param%Omk(:)*f_param%dt) &
                        )/f_param%dBeta
		OUsig2(:,2,2)=(1._wp- (1._wp+2._wp*f_param%dt*f_param%Omk(:) &
                            +2._wp*(f_param%dt*f_param%Omk(:))**2 &
                          )*exp(-2._wp*f_param%Omk(:)*f_param%dt) &
                        )/(f_param%dBeta*f_param%Omk(:)**2)
		OUsig2(:,2,1)=2._wp*f_param%Omk(:)*(f_param%dt**2) &
                        *exp(-2._wp*f_param%Omk(:)*f_param%dt)/f_param%dBeta
		OUsig2(:,1,2)=OUsig2(:,2,1)

        !COMPUTE CHOLESKY DECOMPOSITION
		f_param%OUsig(:,1,1)=sqrt(OUsig2(:,1,1))
		f_param%OUsig(:,1,2)=0
		f_param%OUsig(:,2,1)=OUsig2(:,2,1)/f_param%OUsig(:,1,1)
		f_param%OUsig(:,2,2)=sqrt(OUsig2(:,2,2)-f_param%OUsig(:,2,1)**2)

		!CHECK CHOLESKY DECOMPOSITION
		! write(*,*) "sum of squared errors on cholesky decomposition:"
		! do i=1,nu
		! 	write(*,*) i, sum( (matmul(f_param%OUsig(i,:,:),transpose(f_param%OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
		! enddo

         DO i=1,NUM_AUX_SIM
            polymers(i)%X(f_param%n_beads)=polymers(i)%Delta
        ENDDO

    end subroutine initialize_PIOUD

	subroutine reinitialize_PIOUD(system,polymer)
        IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer

		CALL apply_forces(system%X,polymer,0.5_wp*f_param%dt)
		WORK%muX(:)=f_param%sqrt_mass*system%X*f_param%mu(:,2)
		WORK%muP(:)=f_param%sqrt_mass*system%X*f_param%mu(:,1)

	end subroutine reinitialize_PIOUD

END MODULE wb_pioud