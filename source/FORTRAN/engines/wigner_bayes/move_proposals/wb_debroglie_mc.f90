MODULE wb_debroglie_mc
	USE kinds
	use wb_types
	USE basic_types, only : Pot,dPot
	USE nested_dictionaries
	USE random, only: RandGaussN, randGauss
	IMPLICIT NONE

	PUBLIC

CONTAINS

	    !-----------------------------------
    ! DE_BROGLIE_MC steps
    !perform a step which moves Delta (updates Xbeads and PotCurrent)
	subroutine deBroglie_step(system,polymer,move_accepted)
		IMPLICIT NONE
        TYPE(WIG_SYS_1D), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
		REAL(wp) :: P,dE
		INTEGER :: i
        
		move_accepted=.FALSE.

        !### DELTA MOVE ###
        call RandGauss(P)
        WORK%Delta_new=P*f_param%deBroglie*f_param%DeltaMult
        WORK%MC_Delta_energy_new=MC_compute_delta_energy(system%X,WORK%Delta_new)
        !FAKE ACCEPTANCE TEST
        dE=WORK%MC_Delta_energy_new - polymer%MC_Delta_energy
        call random_number(P)
		if(P < exp(-dE)) then 
            polymer%acc_ratio_delta=polymer%acc_ratio_delta+1
        endif

        !### STAGING MOVE ###
		WORK%X_new(0)=system%X+0.5_wp*WORK%Delta_new
		WORK%X_new(f_param%n_beads)=system%X-0.5_wp*WORK%Delta_new
		call staging_move(WORK%X_new,f_param%n_beads-1,SQRT(f_param%sigp2))

		WORK%MC_energy_new=MC_compute_energy(system%X,WORK%X_new(0:f_param%n_beads),WORK%Delta_new)

        !ACCEPTANCE TEST A=min(1,exp(-dE))
		dE=WORK%MC_energy_new-polymer%MC_energy		
		call random_number(P)
		if(P < exp(-dE)) then 
            polymer%Delta=WORK%Delta_new
			polymer%X=WORK%X_new
			polymer%MC_energy=WORK%MC_energy_new
            polymer%MC_delta_energy=WORK%MC_Delta_energy_new
			move_accepted=.TRUE.
		endif
		
	end subroutine deBroglie_step

	!PERFORMS A STAGING MOVE (Xnew(0) and Xnew(Lstaging+1) contain the fixed extremities)
	subroutine staging_move(Xnew,Lstaging,sigma)
		integer, intent(in) :: Lstaging
		REAL(wp), INTENT(inout) :: Xnew(0:Lstaging+1)
		REAL(wp), intent(in) :: sigma
        REAL(wp) :: R
		integer :: j,i

        do j=Lstaging,1,-1
            call RandGauss(R)
            Xnew(j)=R*sigma*sqrt(real(j,wp)/real(j+1,wp)) &
                        +(j*Xnew(j+1)+Xnew(0))/real(j+1,wp)
        enddo

	end subroutine staging_move

	function MC_compute_energy(q,X_beads,Delta) result(MC_ener)
		implicit none
		REAL(wp), INTENT(in) :: q,X_beads(0:),Delta
		REAL(wp) :: MC_ener
		INTEGER :: i

        MC_ener=0
        DO i=1,f_param%n_beads		
            MC_ener=MC_ener+Pot(mat(X_beads(i)))
        ENDDO
        MC_ener=f_param%dBeta*( MC_ener+0.5_wp*Pot(mat(q+0.5_wp*Delta)) &
                +0.5_wp*Pot(mat(q-0.5_wp*Delta)) ) &
                +f_param%deltaGaussCorr*Delta**2

        !MC_ener=MC_ener+f_param%deltaGaussCorr*Delta**2

	end function MC_compute_energy

    function MC_compute_delta_energy(q,Delta) result(MC_ener)
		implicit none
		REAL(wp), INTENT(in) :: q,Delta
		REAL(wp) :: MC_ener

        MC_ener=0.5_wp*f_param%dBeta*( Pot(mat(q+0.5_wp*Delta)) &
                    +Pot(mat(q-0.5_wp*Delta)) ) &
                +f_param%deltaGaussCorr*Delta**2

	end function MC_compute_delta_energy

    function MC_compute_beads_energy(X_beads) result(MC_ener)
		implicit none
		REAL(wp), INTENT(in) :: X_beads(:)
		REAL(wp) :: MC_ener
		INTEGER :: i

        MC_ener=0
        DO i=1,SIZE(X_beads,1)
            MC_ener=MC_ener+Pot(mat(X_beads(i)))
        ENDDO
        MC_ener=f_param%dBeta*MC_ener

	end function MC_compute_beads_energy

	subroutine update_deltaMult(acc_ratio)
        IMPLICIT NONE
        REAL(wp), INTENT(IN) :: acc_ratio
        INTEGER :: i,j,k

    !    f_param%deltaMult=f_param%deltaMult+f_param%a_delta*(acc_ratio-f_param%acc_ratio_target)
        f_param%deltaMult=f_param%a_delta*sqrt(f_param%k2_prev)/f_param%deBroglie

        f_param%deltaGaussCorr=(1._8-1._8/f_param%DeltaMult**2) &
                                    /(2._8*f_param%deBroglie**2)
        ! f_param%deltaGaussCorr(:,i)=1._8/(2._8*f_param%deBroglie(:)**2)

    end subroutine update_deltaMult

	subroutine initialize_debroglie_mc(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib

		!COMPUTE deBroglie WAVELENGTH
		f_param%deBroglie=SQRT(parameters%beta/system%mass)            
		
		!compute gaussian correction to Delta energy
		f_param%deltaGaussCorr=(1._wp-1._wp/f_param%DeltaMult**2) &
									/(2._wp*f_param%deBroglie**2)

		allocate(WORK%X_new(0:f_param%n_beads))

		f_param%a_delta=wig_lib%get("a_delta",default=0.1)
		!! @input_file WIGNER_AUX/a_delta (wigner, default=0.1, if move_generator="DE_BROGLIE_MC")
		f_param%acc_ratio_target=wig_lib%get("acc_ratio_target",default=0.5)
		!! @input_file WIGNER_AUX/acc_ratio_target (wigner, default=0.5, if move_generator="DE_BROGLIE_MC")

		system%k2=(f_param%DeltaMult*f_param%deBroglie)**2

		system%acc_ratio_mts=wig_lib%get("acc_ratio_mts", default=0.5)
		!! @input_file WIGNER_AUX/acc_ratio_mts (wigner, default=0.5, if move_generator="DE_BROGLIE_MC")

	end subroutine initialize_debroglie_mc

	subroutine reinitialize_debroglie_mc(system,polymer)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer

		polymer%X(0)=system%X + 0.5_wp*polymer%Delta
		polymer%X(f_param%n_beads)=system%X - 0.5_wp*polymer%Delta
		polymer%MC_energy=MC_compute_energy(system%X,polymer%X,polymer%Delta)
		polymer%MC_delta_energy=MC_compute_delta_energy(system%X,polymer%Delta)

	end subroutine reinitialize_debroglie_mc

END MODULE wb_debroglie_mc