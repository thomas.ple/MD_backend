MODULE wb_separate_mc
	USE kinds
	use wb_types
	USE basic_types, only : Pot,dPot
	USE nested_dictionaries
	USE random, only: RandGaussN, randGauss
	IMPLICIT NONE

	PRIVATE
	PUBLIC :: separate_mc_step,initialize_separate_mc,reinitialize_separate_mc

CONTAINS

!--------------------------------------------------------------
! SEPARATE_MC moves

    subroutine separate_mc_step(system,polymer,move_accepted)
        IMPLICIT NONE
        TYPE(WIG_SYS_1D), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
        LOGICAL :: move_accepted_loc
        real(wp) :: P
        INTEGER :: i

        move_accepted=.FALSE.
        ! call random_number(P)
        ! if(P<=f_param%Delta_prob) then
        !     call delta_step(system%X,polymer,move_accepted)
        ! else
        !     call staging_step(polymer,move_accepted)
        ! endif
        call delta_step(system%X,polymer,move_accepted_loc)
        if(move_accepted_loc) move_accepted=.TRUE.

        DO i=1,f_param%n_beads/f_param%l_staging
            call staging_step(polymer,move_accepted_loc)
            if(move_accepted_loc) move_accepted=.TRUE.
        ENDDO

    end subroutine separate_mc_step

    !perform a staging step (updates Xbeads and polymer%Pot)
	subroutine staging_step(polymer,move_accepted)
		implicit none
        TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, INTENT(out) :: move_accepted
		real(wp) :: PotNew(f_param%L_staging),Ust(f_param%L_staging),Xnew(f_param%L_staging)
		real(wp) :: P,R(f_param%L_staging),dE
		integer :: z,j
        REAL(wp) :: acc
		
        polymer%N_moves_staging=polymer%N_moves_staging+1
		!Take a random fixed starting point for staging
		call random_number(P)
		z=nint(P*(f_param%n_beads-1-f_param%L_staging))
		
		!sample staging variables
		!do j=1,f_param%L_staging
		!	call RandGauss(R)
		!	Ust(j)=R*sqrt(f_param%sigp2*j/(j+1))
		!enddo
		!transform back to coordinates
		call RandGaussN(R)
		Xnew(f_param%L_staging)=R(f_param%L_staging)*sqrt(f_param%sigp2*f_param%L_staging/real(f_param%L_staging+1,wp)) &
						+(f_param%L_staging*polymer%X(z+f_param%L_staging+1)+polymer%X(z))/real(f_param%L_staging+1,wp)
		do j=f_param%L_staging-1,1,-1
			Xnew(j)=R(j)*sqrt(f_param%sigp2*j/real(j+1,wp))+(j*Xnew(j+1)+polymer%X(z))/real(j+1,wp)
		enddo			
		
		!compute potential energy difference
		dE=0
		do j=1,f_param%L_staging
			PotNew(j)=Pot(mat(Xnew(j)))
			dE=dE+PotNew(j)-polymer%Pot(z+j)
		enddo
        dE=f_param%dBeta*dE
		
		!ACCEPTANCE TEST A=min(1,exp(-dE))
		call random_number(P)
		if(P < exp(-dE)) then 
			do j=1,f_param%L_staging
				polymer%Pot(z+j)=PotNew(j)
				polymer%X(z+j)=Xnew(j)
			enddo
            move_accepted=.TRUE.
            acc=1._wp
		else
            move_accepted=.FALSE.
            acc=0._wp
        endif
        polymer%acc_ratio_staging=polymer%acc_ratio_staging+(acc-polymer%acc_ratio_staging)/polymer%N_moves_staging
		
	end subroutine staging_step

	!perform a step which moves Delta (updates Xbeads and polymer%Pot)
	subroutine delta_step(q,polymer,move_accepted)
		implicit none
        TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		real(wp), INTENT(in) :: q
        LOGICAL, INTENT(out) :: move_accepted
		real(wp) :: R,P,Delta_new,PotNew_nu,PotNew_0,dE,dDelta		
        REAL(wp) :: acc

        polymer%N_moves_delta=polymer%N_moves_delta+1

		call RandGauss(R)
		dDelta=R*sqrt(2*f_param%sigp2)*f_param%DeltaMult
		Delta_new=polymer%X(1)-polymer%X(f_param%n_beads-1)+dDelta
		PotNew_nu=Pot(mat(q-0.5_wp*Delta_new))
		PotNew_0=Pot(mat(q+0.5_wp*Delta_new))
		
		dE=0.5_wp*f_param%dBeta*(PotNew_nu-polymer%Pot(f_param%n_beads)+PotNew_0-polymer%Pot(0)) &
			+f_param%deltaGaussCorr*(dDelta**2-(polymer%Delta-(polymer%X(1)-polymer%X(f_param%n_beads-1)))**2)
		!ACCEPTANCE TEST A=min(1,exp(-dE))
		call random_number(P)
		if(P < exp(-dE)) then 
			polymer%X(0)=q+0.5_wp*Delta_new
			polymer%X(f_param%n_beads)=q-0.5_wp*Delta_new
			polymer%Pot(0)=PotNew_0
			polymer%Pot(f_param%n_beads)=Potnew_nu
			polymer%Delta=Delta_new
            move_accepted=.TRUE.
            acc=1._wp
		else
            move_accepted=.FALSE.
            acc=0._wp
        endif
		polymer%acc_ratio_delta=polymer%acc_ratio_delta+(acc-polymer%acc_ratio_delta)/polymer%N_moves_delta

	end subroutine delta_step


	subroutine initialize_separate_mc(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib

		f_param%L_staging=wig_lib%get("l_staging",default=f_param%n_beads/2)
		!! @input_file WIGNER_AUX/l_staging (wigner, default=n_beads/2, if move_generator="SEPARATE_MC")
		if(f_param%L_staging<=0) f_param%L_staging=1
		if(f_param%L_staging>f_param%n_beads) f_param%L_staging=f_param%n_beads
		

		f_param%Delta_prob=wig_lib%get("delta_prob",default=0.2)
		!! @input_file WIGNER_AUX/delta_prob (wigner, default=0.2, if move_generator="SEPARATE_MC")

		!compute gaussian correction to Delta energy
		f_param%deltaGaussCorr=(1._wp-1._wp/f_param%DeltaMult**2)/(4*f_param%sigp2)


	end subroutine initialize_separate_mc

	subroutine reinitialize_separate_mc(system,polymer)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
		INTEGER :: i

		polymer%X(0)=system%X + 0.5_wp*polymer%Delta
		polymer%X(f_param%n_beads)=system%X - 0.5_wp*polymer%Delta
		DO i=0,f_param%n_beads
			polymer%Pot(i)=Pot(mat(polymer%X(i)))
		ENDDO

	end subroutine reinitialize_separate_mc

END MODULE wb_separate_mc