MODULE wb_rdelta_mc
	USE kinds
	USE wb_types
	USE basic_types, only : Pot,dPot
	USE nested_dictionaries
	USE random, only: RandGaussN, randGauss
	USE	wb_debroglie_mc, only: staging_move
	USE file_handler, only: output
	IMPLICIT NONE

	PRIVATE
	PUBLIC :: rdelta_mc_step, initialize_rdelta_mc, reinitialize_rdelta_mc

	REAL(wp), allocatable, save :: r_chain(:), delta_chain(:), pot_curr(:)
	INTEGER, save :: nu, L_staging_r, L_staging_delta, n_segments_delta, n_segments_r
	REAL(wp), save :: sigma_r, sigma_delta
	INTEGER, allocatable :: lpins_r(:), lpins_delta(:)

CONTAINS


	subroutine rdelta_mc_step(system,polymer,move_accepted)
		IMPLICIT NONE
        TYPE(WIG_SYS_1D), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
		LOGICAL :: move_accepted_loc
		REAL(wp) :: P	
		INTEGER :: i

		move_accepted=.TRUE.

		!DO i=1,n_segments_delta+n_segments_r
			CALL random_number(P)
			IF(P<=f_param%delta_prob) THEN
				call delta_move(system,polymer,move_accepted)
			ELSE
				call r_move(system,polymer,move_accepted)
			ENDIF	
		!ENDDO

		IF(move_accepted) call x_from_rdelta(polymer)	
		
	end subroutine rdelta_mc_step

	subroutine delta_move(system,polymer,move_accepted)
		IMPLICIT NONE
        TYPE(WIG_SYS_1D), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
		INTEGER :: i,seg_select, lpin
		REAL(wp) :: P,R,dE,acc
		REAL(wp) :: chain_new(0:L_staging_delta+1),pot_new(L_staging_delta), pot_new_0
		LOGICAL :: move_end

		CALL random_number(P)
		seg_select = INT( P * n_segments_delta ) + 1
		! IF(seg_select==n_segments_delta) THEN
		! 	lpin=nu-L_staging_delta-1
		! ELSE
		! 	lpin = INT( L_staging_delta / 2 ) * ( seg_select - 1 )
		! ENDIF
		lpin=lpins_delta(seg_select)

		chain_new(1:L_staging_delta)=0._wp
		chain_new(L_staging_delta+1)=delta_chain(lpin+L_staging_delta+1)
		IF(seg_select==1) THEN
			CALL randGauss(R) ! MOVE Delta_0 if needed
			chain_new(0)=chain_new(L_staging_delta+1) + R*SQRT(L_staging_delta+1._wp)*sigma_delta
			move_end=.TRUE.
		ELSE
			chain_new(0)=delta_chain(lpin)
			move_end=.FALSE.
		ENDIF		

		CALL staging_move(chain_new,L_staging_delta,sigma_delta)

		dE=0._wp
		DO i=1,L_staging_delta
			pot_new(i)=Pot(mat(r_chain(lpin+i)+0.5_wp*chain_new(i))) &
						+Pot(mat(r_chain(lpin+i)-0.5_wp*chain_new(i)))
			dE=dE + pot_new(i)-pot_curr(lpin+i)
		ENDDO
		If(move_end) THEN
			pot_new_0=0.5_wp*(Pot(mat(r_chain(0)+0.5_wp*chain_new(0))) &
							+Pot(mat(r_chain(0)-0.5_wp*chain_new(0))))
			dE=dE + pot_new_0 -pot_curr(0)
		ENDIF

		CALL random_number(P)
		IF(P <= exp(-f_param%dBeta*dE)) THEN
			pot_curr(lpin+1:lpin+L_staging_delta)=pot_new(:)
			delta_chain(lpin+1:lpin+L_staging_delta) = chain_new(1:L_staging_delta)
			IF(move_end) THEN
				pot_curr(0)=pot_new_0
				delta_chain(0)=chain_new(0)
			ENDIF
			move_accepted=.TRUE.
			acc=1._wp
		ELSE
			move_accepted=.FALSE.
			acc=0._wp
		ENDIF

		polymer%N_moves_delta=polymer%N_moves_delta+1
		polymer%acc_ratio_delta=polymer%acc_ratio_delta+(acc-polymer%acc_ratio_delta)/polymer%N_moves_delta		

		!write(200,*) seg_select, int(acc), polymer%acc_ratio_delta,polymer%N_moves_delta

	end subroutine delta_move

	subroutine r_move(system,polymer,move_accepted)
		IMPLICIT NONE
        TYPE(WIG_SYS_1D), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		LOGICAL, INTENT(OUT) :: move_accepted
		INTEGER :: i,seg_select, lpin,rpin
		REAL(wp) :: P,R,dE,acc
		REAL(wp) :: chain_new(0:L_staging_r+1),pot_new(L_staging_r), pot_new_0
		LOGICAL :: move_end

		CALL random_number(P)
		seg_select = INT( P * n_segments_r ) + 1
		! IF(seg_select==n_segments_r) THEN
		! 	lpin=nu-L_staging_r-1
		! ELSE
		! 	lpin = INT( L_staging_r / 2 ) * ( seg_select - 1 )
		! ENDIF
		lpin=lpins_r(seg_select)

		chain_new(1:L_staging_r)=0._wp
		chain_new(0)=r_chain(lpin)
		IF(seg_select==n_segments_r) THEN
			CALL randGauss(R) ! MOVE r_nu if needed
			chain_new(L_staging_r+1)=chain_new(0) + R*SQRT(L_staging_r+1._wp)*sigma_r
			move_end=.TRUE.
		ELSE
			chain_new(L_staging_r+1)=r_chain(lpin+L_staging_r+1)
			move_end=.FALSE.
		ENDIF		

		CALL staging_move(chain_new,L_staging_r,sigma_r)

		dE=0._wp
		DO i=1,L_staging_r
			pot_new(i)=Pot(mat(chain_new(i)+0.5_wp*delta_chain(lpin+i))) &
						+Pot(mat(chain_new(i)-0.5_wp*delta_chain(lpin+i)))
			dE=dE + pot_new(i)-pot_curr(lpin+i)
		ENDDO
		If(move_end) THEN
			pot_new_0=Pot(mat(chain_new(L_staging_r+1)))
			dE=dE + pot_new_0-pot_curr(nu)
		ENDIF


		CALL random_number(P)
		IF(P <= exp(-f_param%dBeta*dE)) THEN
			pot_curr(lpin+1:lpin+L_staging_r)=pot_new(:)
			r_chain(lpin+1:lpin+L_staging_r) = chain_new(1:L_staging_r)
			IF(move_end) THEN
				pot_curr(lpin+L_staging_r+1)=pot_new_0
				r_chain(lpin+L_staging_r+1)=chain_new(L_staging_r+1)
			ENDIF
			move_accepted=.TRUE.
			acc=1._wp
		ELSE
			move_accepted=.FALSE.
			acc=0._wp
		ENDIF

		polymer%N_moves_staging=polymer%N_moves_staging+1
		polymer%acc_ratio_staging=polymer%acc_ratio_staging+(acc-polymer%acc_ratio_staging)/polymer%N_moves_staging

	end subroutine r_move



	subroutine initialize_rdelta_mc(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
		INTEGER :: i, u

		if(mod(f_param%n_beads, 2) /= 0) then
			write(0,*) "Error: n_beads should be a multiple of 2 for 'rdelta_mc'."
			STOP "Execution stopped."
		endif
		nu=f_param%n_beads/2

		ALLOCATE(r_chain(0:nu), delta_chain(0:nu), pot_curr(0:nu))

		L_staging_r=wig_lib%get("l_staging",default=nu/2)
		!! @input_file WIGNER_AUX/l_staging_r (wigner, default=nu/2, if move_generator="RDELTA_MC")
		L_staging_r=wig_lib%get("l_staging_r",default=L_staging_r)
		!! @input_file WIGNER_AUX/l_staging_r (wigner, default=nu/2, if move_generator="RDELTA_MC")
		L_staging_delta=wig_lib%get("l_staging_delta",default=L_staging_r)
		!! @input_file WIGNER_AUX/l_staging_delta (wigner, default=L_staging_r, if move_generator="RDELTA_MC")
		if(L_staging_r<=0) L_staging_r=1
		if(L_staging_r>=nu) L_staging_r=nu-1
		if(L_staging_delta<=0) L_staging_delta=1
		if(L_staging_delta>=nu) L_staging_delta=nu-1

		! n_segments_r = 2 * MAX( nu/L_staging_r , 1 ) - 1
		! n_segments_delta = 2 * MAX( nu/L_staging_delta , 1 ) - 1
		OPEN(newunit=u,file=output%working_directory//"/rdelta_segments.info")
		write(u,*) "delta chain, l_staging_delta=",L_staging_delta
		call build_segments(lpins_delta,n_segments_delta,L_staging_delta,u)
		write(u,*)
		write(u,*) "r chain, l_staging_r=",L_staging_r
		call build_segments(lpins_r,n_segments_r,L_staging_r,u)
		CLOSE(u)

		f_param%Delta_prob=wig_lib%get("delta_prob",default=0.5)
		!! @input_file WIGNER_AUX/delta_prob (wigner, default=0.5, if move_generator="RDELTA_MC")

		sigma_r=SQRT(parameters%beta/(nu*4._wp*system%mass))
		sigma_delta=SQRT(parameters%beta/(nu*system%mass))
		

	end subroutine initialize_rdelta_mc

	subroutine build_segments(segments,n_seg,ls,u)
		INTEGER, allocatable, intent(out) :: segments(:)
		INTEGER, INTENT(out) :: n_seg
		INTEGER, intent(in), optional :: u
		INTEGER, intent(in) :: ls
		INTEGER :: lsegs(nu),rsegs(nu)
		INTEGER :: i,il,ir

		lsegs(1)=0
		rsegs(1)=nu-ls-1
		il=1
		DO
			if(lsegs(il)+ls+1 > rsegs(il)) exit
			il=il+1
			lsegs(il)=lsegs(il-1)+ls
			rsegs(il)=rsegs(il-1)-ls
		ENDDO
		ir=il
		if(rsegs(il)==lsegs(il)) ir=il-1

		n_seg=ir+il
		
		if(allocated(segments)) deallocate(segments)
		allocate(segments(n_seg))
		DO i=1,il
			segments(i)=lsegs(i)
		ENDDO
		DO i=1,ir
			segments(il+i)=rsegs(ir+1-i)
		ENDDO

		if(present(u)) then
			write(u,*) "n_seg=",n_seg
			DO i=1,n_seg
				write(u,*) segments(i),"->",segments(i)+ls+1
			ENDDO
		endif

	end subroutine build_segments

	subroutine reinitialize_rdelta_mc(system,polymer)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
		INTEGER :: i

		polymer%X(0)=system%X + 0.5_wp*polymer%Delta
		polymer%X(f_param%n_beads)=system%X - 0.5_wp*polymer%Delta
		call rdelta_from_x(polymer)

		DO i=0,nu-1
			pot_curr(i)=Pot(mat(polymer%X(i))) &
						+Pot(mat(polymer%X(2*nu-i)))
		ENDDO
		pot_curr(0)=0.5_wp*pot_curr(0)
		pot_curr(nu)=Pot(mat(polymer%X(nu)))

	end subroutine reinitialize_rdelta_mc

	subroutine rdelta_from_x(polymer)
		IMPLICIT NONE
		CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
		INTEGER :: i

		DO i=0,nu-1
			r_chain(i)=0.5_wp*(polymer%X(i)+polymer%X(2*nu-i))
			delta_chain(i)=polymer%X(i)-polymer%X(2*nu-i)
		ENDDO
		r_chain(nu)=polymer%X(nu)
		delta_chain(nu)=0._wp
	end subroutine rdelta_from_x

	subroutine x_from_rdelta(polymer)
		IMPLICIT NONE
		CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
		INTEGER :: i

		DO i=0,nu-1
			polymer%X(i)=r_chain(i)+0.5_wp*delta_chain(i)
			polymer%X(2*nu-i)=r_chain(i)-0.5_wp*delta_chain(i)
		ENDDO
		polymer%X(nu)=r_chain(nu)
		polymer%Delta=delta_chain(0)
	end subroutine x_from_rdelta

END MODULE wb_rdelta_mc