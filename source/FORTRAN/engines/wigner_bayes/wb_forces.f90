MODULE wb_forces
    USE kinds
    USE wb_types
    USE nested_dictionaries
    USE string_operations
    USE atomic_units, only : convert_to_unit
    USE basic_types, only : Pot,dPot
    USE matrix_operations, only : sym_mat_inverse,fill_permutations
    USE random, only: RandGaussN, randGauss
    USE unbiased_products, only: unbiased_product
    USE wb_separate_mc
    USE wb_debroglie_mc
    USE wb_pioud
    USE wb_rdelta_mc
    IMPLICIT NONE

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), ALLOCATABLE, SAVE :: polymers

    ABSTRACT INTERFACE
		subroutine move_proposal_type(system,polymer,move_accepted)
            IMPORT :: WIG_SYS_1D,WIG_PARAM_1D,WIG_POLYMER_TYPE
			IMPLICIT NONE
            TYPE(WIG_SYS_1D), INTENT(IN) :: system
			TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
			LOGICAL, INTENT(OUT) :: move_accepted
		end subroutine move_proposal_type

        subroutine reinit_type(system,polymer)
            IMPORT :: WIG_SYS_1D,WIG_PARAM_1D,WIG_POLYMER_TYPE
			IMPLICIT NONE
            CLASS(WIG_SYS_1D), INTENT(INOUT) :: system
			CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
		end subroutine reinit_type
	END INTERFACE

    PROCEDURE(move_proposal_type), POINTER :: move_proposal => NULL()
    PROCEDURE(reinit_type), POINTER :: reinitialize_move_generator => NULL()

    PRIVATE
    PUBLIC :: compute_wigner_forces, initialize_auxiliary_simulation &
                ,compute_EW

CONTAINS

!--------------------------------------------------------------
! FORCE CALCULATIONS

    subroutine compute_wigner_forces(system,parameters)
        IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        INTEGER :: i,u
        REAL(wp) :: k2_var, delta2_var,k2inv
        CHARACTER(5) :: i_char

        if(ISNAN(system%X)) STOP "ERROR: system diverged!"
        if(abs(system%X)>10) STOP "ERROR: system diverged!"


        f_param%k2_prev=system%k2
        if(parameters%move_generator=="DE_BROGLIE_MC") &
            call update_deltaMult(system%acc_ratio_delta)

        f_param%dq=system%X-system%X_prev
        
        system%k2=0
        system%k4=0
        system%k6=0
        k2_var=0
        delta2_var=0
        k2inv=0
        

        if(parameters%only_auxiliary_simulation) then
            open(newunit=u,file=output%working_directory//"/q_info")
            write(u,*) "q ",system%X
            write(u,*) "F_classical ",-dPot(mat(system%X))
            write(u,*) "Pot ",Pot(mat(system%X)) 
            write(u,*) "n_beads ",f_param%n_beads
            write(u,*) "gamma0 ",f_param%OmK(1)*THz/pi,"THz"
            write(u,*) "tau0 ",pi*fs/f_param%OmK(1),"fs"
            close(u)

            open(newunit=u &
                    ,file=output%working_directory//"/mean_values_aux.out" &
                    ,access="STREAM", form="unformatted")
        endif            
        
        DO i=1,NUM_AUX_SIM
            write(i_char,'(i5.5)') i
            IF(f_param%write_aux_traj) THEN                
                open(newunit=f_param%traj_unit &
                    ,file=output%working_directory//"/aux_traj_"//i_char//".out" &
                    ,access="STREAM", form="unformatted")
            ENDIF
            IF(f_param%write_beads_traj) THEN                
                open(newunit=f_param%beads_traj_unit &
                    ,file=output%working_directory//"/beads_traj_"//i_char//".out" &
                    ,access="STREAM", form="unformatted")
            ENDIF
            call sample_forces(system,parameters,polymers(i))
            system%k6=system%k6+polymers(i)%k6
            system%k4=system%k4+polymers(i)%k4
            system%k2=system%k2+polymers(i)%k2
            system%F_var=system%F_var+polymers(i)%F_var
            k2_var=k2_var+polymers(i)%k2_var
            delta2_var=delta2_var+polymers(i)%delta2_var
            k2inv=k2inv+polymers(i)%k2inv
            IF(f_param%write_aux_traj) close(f_param%traj_unit)
            IF(f_param%write_beads_traj) close(f_param%beads_traj_unit)
            
            if(parameters%only_auxiliary_simulation) then
                write(*,*) "run",i
                write(*,*) "acc_ratio_delta=", polymers(i)%acc_ratio_delta
                write(*,*) "acc_ratio_staging=", polymers(i)%acc_ratio_staging               
                write(u) polymers(i)%k2 &
                        ,polymers(i)%F_delta &
                        ,polymers(i)%F_beads &                        
                        ,polymers(i)%F_spring &
                        ,polymers(i)%F_deltaD2 &
                        ,polymers(i)%F_beadsD2 &
                        ,polymers(i)%F_springD2 &   
                        ,polymers(i)%k4 &
                        ,polymers(i)%k6                  
            endif
        ENDDO

        system%k6=system%k6/REAL(NUM_AUX_SIM, wp)
        system%k4=system%k4/REAL(NUM_AUX_SIM, wp)
        system%k2=system%k2/REAL(NUM_AUX_SIM, wp)
        system%F_var=system%F_var/REAL(NUM_AUX_SIM, wp)

        k2_var=k2_var/REAL(NUM_AUX_SIM, wp)
        delta2_var=delta2_var/REAL(NUM_AUX_SIM, wp)
       
        system%delta2_var=delta2_var
        

        system%acc_ratio=SUM(polymers(:)%acc_ratio/REAL(NUM_AUX_SIM, wp))
        system%acc_ratio_delta=SUM(polymers(:)%acc_ratio_delta/REAL(NUM_AUX_SIM, wp))
        system%acc_ratio_staging=SUM(polymers(:)%acc_ratio_staging/REAL(NUM_AUX_SIM, wp))

        if(parameters%only_auxiliary_simulation) then
            close(u)
            return
        endif
        CALL compute_forces_elements(system,parameters)

        IF(NUM_AUX_SIM<=5) THEN
            system%k2inv=k2inv/REAL(NUM_AUX_SIM, wp)
            system%k2_var=k2_var
        ELSE
            system%k2_var=SUM((polymers(:)%k2-system%k2)**2/REAL(NUM_AUX_SIM*(NUM_AUX_SIM-1), wp))
            system%k2inv=1._wp/system%k2 - system%k2_var/system%k2**3
            !write(200,*) system%k2, system%k2_var, sqrt(system%k2_var)/system%k2 &
             !           , system%k2inv, 1._wp/system%k2**2 - 3*system%k2_var/system%k2**4
        ENDIF
       
        system%X_prev=system%X
    end subroutine compute_wigner_forces

    subroutine compute_forces_elements(system,parameters)
        IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        REAL(wp) :: tmp,tmp2,var,mean,k2k2F,F(NUM_AUX_SIM),k2(NUM_AUX_SIM),G2(NUM_AUX_SIM) &
                    ,k4(NUM_AUX_SIM),tab(NUM_AUX_SIM),tab2(NUM_AUX_SIM-1)
        REAL(wp) :: F_var,V_var,deltax,x,deltay,y,FV_covar
        INTEGER :: i,j,k
        

        DO i=1,NUM_AUX_SIM
            F(i)=polymers(i)%F
            k2(i)=polymers(i)%k2
            k4(i)=polymers(i)%k4
            G2(i)=polymers(i)%G
        ENDDO

        system%beta_dU=-SUM(F(:))/real(NUM_AUX_SIM,wp)

        system%dk2= SUM(G2(:))/real(NUM_AUX_SIM,wp) &
                    -unbiased_product(k2(:),F(:))

        system%C4=system%k4 - 3._wp*unbiased_product(k2(:),k2(:))

        k2k2F=unbiased_product(k2(:),k2(:),F(:))
        system%C4_beta_dU=-unbiased_product(k4(:),F(:)) &
                            +3._wp*k2k2F
        system%dC4= SUM(polymers(:)%G4)/real(NUM_AUX_SIM,wp) &
                    +system%C4_beta_dU + 3._wp*k2k2F &                        
                    -6._wp*unbiased_product(k2(:),G2(:))

        if(NUM_AUX_SIM<=5) then

            system%C=-0.5_wp*(unbiased_product(polymers(:)%k2inv,G2) &
                               ! -SUM(F/REAL(NUM_AUX_SIM,wp)) )/f_param%mass_expand
                            -unbiased_product(polymers(:)%k2inv,k2,F))/f_param%mass_expand

            WORK%V=-unbiased_product(polymers(:)%k2inv,G2,polymers(:)%k2inv)/f_param%mass_expand

            system%F=WORK%V+2._wp*unbiased_product(polymers%k2inv,F)/f_param%mass_expand

        else
            system%C=0
            system%F=0
            WORK%V=0
            tab=k2
            tmp=0
            F_var=0
            V_var=0
            FV_covar=0
            DO i=1,NUM_AUX_SIM
                tab=CSHIFT(tab,1)
                mean=sum(tab(1:NUM_AUX_SIM-1)/REAL(NUM_AUX_SIM-1,wp))
                var=sum((tab(1:NUM_AUX_SIM-1)-mean)**2/REAL((NUM_AUX_SIM-1)*(NUM_AUX_SIM-2),wp))
                tmp=(1._wp/mean-var/mean**3)
                system%C=system%C + (G2(i)*tmp-system%C)/real(i,wp)

                x=F(i)*tmp
                deltax= x-system%F
                system%F=system%F + deltax/real(i,wp)
                F_var=F_var+ (x-system%F)*deltax

                y=G2(i)*(1._wp/mean**2-3._wp*var/mean**4)
                deltay= y-WORK%V
                WORK%V=WORK%V + deltay/real(i,wp)
                V_var=V_var+(y-WORK%V)*deltay

                FV_covar=FV_covar + deltax*(y-WORK%V)
            ENDDO
            system%C=-0.5_wp*( system%C - SUM(F/REAL(NUM_AUX_SIM,wp)) )/f_param%mass_expand
            system%F=(2._wp*system%F - WORK%V)/f_param%mass_expand

            F_var=F_var/REAL(NUM_AUX_SIM*(NUM_AUX_SIM-1),wp)
            V_var=V_var/REAL(NUM_AUX_SIM*(NUM_AUX_SIM-1),wp)
            FV_covar=FV_covar/REAL(NUM_AUX_SIM*(NUM_AUX_SIM-1),wp)
            system%sigf2=parameters%dt*(4._wp*F_var+V_var-2*FV_covar)/f_param%mass_expand**2
            !system%sigf2=parameters%dt*(4._wp*F_var+V_var)/f_param%mass_expand**2
            !write(200,*) 4._wp*F_var, V_var, -2*FV_covar, system%sigf2, parameters%sigma**2
        endif

    end subroutine compute_forces_elements

    function mean_inverse_k2(k2s) RESULT(mean)
        IMPLICIT NONE
        REAL(wp), INTENT(in) :: k2s(:)
        REAL(wp) :: mean
        REAL(wp) :: var
        INTEGER :: n
        n=size(k2s)
        mean=sum(k2s/REAL(n,wp))
        var=sum((k2s-mean)**2/REAL(n-1,wp))
        mean=(1._wp/mean-var/mean**3)
    end function mean_inverse_k2

!--------------------------------------------------------------
! SAMPLING LOOP
    subroutine sample_forces(system,parameters,polymer)
        IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: m,is,count,n_dof
        INTEGER :: i,j,k,l
        LOGICAL :: move_accepted

        call reinitialize_polymer_config(system,polymer)
        ! do is=1,f_param%n_samples
        !     WORK%k2_s(is)=f_param%k2_prev
        ! enddo
        WORK%k2_s=0
        WORK%k4_s=0
        WORK%F_s=0


        !THERMALIZATION
        DO m=1,f_param%n_steps_therm
            CALL move_proposal(system,polymer,move_accepted)
        ENDDO

        !INITIALIZE OBSERVABLES
        SELECT CASE(f_param%move_generator)
        CASE("DE_BROGLIE_MC","SEPARATE_MC")
            call compute_observables(system,polymer)
        END SELECT

        polymer%acc_ratio=0
        polymer%acc_ratio_delta=0
        polymer%acc_ratio_staging=0

        !PRODUCTION
        count=0
        DO is=1,f_param%n_samples ; DO m=1,f_param%n_steps

            count=count+1
            !GENERATE A NEW CONFIG
            CALL move_proposal(system,polymer,move_accepted)

            !COMPUTE OBSERVABLES
            IF(move_accepted) THEN
                polymer%acc_ratio=polymer%acc_ratio+1
                call compute_observables(system,polymer)
            ENDIF

            !UPDATE MEAN VALUES
            CALL update_mean_values(polymer,count)
            WORK%dDelta=WORK%Delta2-WORK%k2_s(is)
            WORK%k2_s(is)=WORK%k2_s(is) + WORK%dDelta/(m+1)
            WORK%k4_s(is)=WORK%k4_s(is) + (WORK%Delta2-WORK%k2_s(is))*WORK%dDelta
            WORK%F_s(is)=WORK%F_s(is) + (WORK%F-WORK%F_s(is))/m

            if(f_param%write_aux_traj) &
                write(f_param%traj_unit) &
                            polymer%Delta &
                            ,WORK%F_delta &
                            ,WORK%F_beads &
                            ,WORK%F_spring
            
            if(f_param%write_beads_traj) &
                write(f_param%beads_traj_unit) &
                            system%X+0.5*polymer%Delta &
                            ,polymer%X(1:f_param%n_beads-1) &
                            ,system%X-0.5*polymer%Delta
                            

        ENDDO ; ENDDO
        CALL compute_k2_k2inv(polymer)

        ! polymer%acc_ratio=polymer%acc_ratio/(f_param%n_samples*f_param%n_steps)
        ! polymer%acc_ratio_delta=polymer%acc_ratio_delta/(f_param%n_samples*f_param%n_steps)

    end subroutine sample_forces

!-------------------------------------------------
! UTILITY SUBROUTINES

    subroutine compute_observables(system,polymer)
        IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i,j,k,l
        REAL(wp) :: P

        P=real(f_param%n_beads,wp)

        WORK%Forces1=-scal(dPot(mat(system%X+0.5_wp*polymer%Delta)))
        WORK%Forces2=-scal(dPot(mat(system%X-0.5_wp*polymer%Delta)))
         WORK%F_delta= 0.5*(WORK%Forces1 + WORK%Forces2)/P
 
        WORK%F_spring= -2._wp*f_param%sigp2_inv/f_param%beta &
            *(system%X - 0.5_wp*(polymer%X(1)+polymer%X(f_param%n_beads-1)))

        WORK%F_beads=0        
        DO k=1,f_param%n_beads-1
            WORK%F_beads=WORK%F_beads &
                    -scal(dPot(mat(polymer%X(k))))/P
        ENDDO

        if(f_param%spring_force) then
            ! WORK%F = -2._wp*f_param%sigp2_inv * ( &
            !             system%X-WORK%Rnu &
            !     )+f_param%beta*WORK%F_pot
            WORK%F = f_param%beta*(WORK%F_delta + WORK%F_spring)
        else
            WORK%F = f_param%beta*(WORK%F_delta + WORK%F_beads)
        endif

        WORK%Delta2=polymer%Delta**2
        WORK%Delta4=polymer%Delta**4

    end subroutine compute_observables

    subroutine update_mean_values(polymer,count)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER, INTENT(in) :: count
        INTEGER :: k
        

        polymer%k6=polymer%k6 + (polymer%Delta**6-polymer%k6)/count
		polymer%k4=polymer%k4 + (WORK%Delta4-polymer%k4)/count			
		polymer%F=polymer%F + (WORK%F-polymer%F)/count
		polymer%G=polymer%G + (WORK%Delta2*WORK%F-polymer%G)/count
        polymer%G4=polymer%G4 + (WORK%F*WORK%Delta4-polymer%G4)/count

        polymer%F_spring=polymer%F_spring + (WORK%F_spring-polymer%F_spring)/count
        polymer%F_springD2=polymer%F_springD2 + (WORK%F_spring*WORK%Delta2-polymer%F_springD2)/count

        polymer%F_delta=polymer%F_delta + (WORK%F_delta-polymer%F_delta)/count
        polymer%F_deltaD2=polymer%F_deltaD2 + (WORK%F_delta*WORK%Delta2-polymer%F_deltaD2)/count
        
        polymer%F_beads=polymer%F_beads + (WORK%F_beads-polymer%F_beads)/count
        polymer%F_beadsD2=polymer%F_beadsD2 + (WORK%F_beads*WORK%Delta2-polymer%F_beadsD2)/count

    end subroutine update_mean_values

    subroutine compute_k2_k2inv(polymer)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i,j,INFO,is

        !COMPUTE MEAN k2
        polymer%k2=0
        DO is=1,f_param%n_samples
            polymer%k2=polymer%k2+WORK%k2_s(is)
        ENDDO
        polymer%k2=polymer%k2/f_param%n_samples

        !COMPUTE k2_var
        polymer%k2_var=0
        do is=1,f_param%n_samples
            polymer%k2_var=polymer%k2_var+(WORK%k2_s(is)-polymer%k2)**2
        enddo
        polymer%k2_var=polymer%k2_var/((f_param%n_samples-1)*f_param%n_samples)

        !COMPUTE F_var
        polymer%F_var=0
        do is=1,f_param%n_samples
            polymer%F_var=polymer%F_var+(WORK%F_s(is)-polymer%F)**2
        enddo
        polymer%F_var=polymer%F_var/((f_param%n_samples-1)*f_param%n_samples)

        !INVERSE k2
        WORK%Delta2=1._wp/polymer%k2
       ! IF(INFO/=0) STOP "Error: could not compute k2inv !"
        !COMPUTE CORRECTED k2inv
        IF(NUM_AUX_SIM<=5 .AND. f_param%n_samples>=5) THEN
            polymer%k2inv=0
            do is=1,f_param%n_samples
                polymer%k2inv=polymer%k2inv+WORK%k2_s(is)*WORK%Delta2*WORK%k2_s(is)
            enddo
            polymer%k2inv=( real(f_param%n_samples,wp) &
                            /real(f_param%n_samples-1,wp) ) * WORK%Delta2 &
                        -WORK%Delta2**2 &
                            *polymer%k2inv/real(f_param%n_samples,wp) &
                            /real(f_param%n_samples-1,wp)    
        ELSE
            polymer%k2inv=1._wp/polymer%k2
        ENDIF
       ! write(*,*) polymer%k2, polymer%k2inv, 1._wp/polymer%k2 
       ! polymer%k2inv=1._wp/polymer%k2

    end subroutine compute_k2_k2inv

!--------------------------------------------------------------
! VARIOUS INITIALIZATIONS

    subroutine reinitialize_polymer_config(system,polymer)
        IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i,j

        polymer%acc_ratio=0
        polymer%acc_ratio_delta=0
        polymer%acc_ratio_staging=0
        polymer%N_moves_delta=0
        polymer%N_moves_staging=0

        polymer%k2=0
        polymer%k2inv=0
        polymer%k4=0
        polymer%k6=0
        polymer%G4=0
        polymer%F=0
        polymer%G=0

        polymer%F_delta=0
        polymer%F_deltaD2=0
        polymer%F_spring=0
        polymer%F_springD2=0
        polymer%F_beads=0
        polymer%F_beadsD2=0

        DO i=1,f_param%n_beads-1
            polymer%X(i)=polymer%X(i)+f_param%dq
        ENDDO       

        call reinitialize_move_generator(system,polymer)

    end subroutine reinitialize_polymer_config

    subroutine initialize_auxiliary_simulation(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
        TYPE(DICT_STRUCT), pointer :: wig_lib
        INTEGER :: n_beads,min_aux_sim
        INTEGER :: i,j,k,n_therm_tmp

        f_param%mass_expand=system%mass
        f_param%sqrt_mass=SQRT(system%mass)

        min_aux_sim=3
        if(parameters%only_auxiliary_simulation) min_aux_sim=1

        NUM_AUX_SIM=param_library%get(WIGNER_CAT//"/num_aux_sim",default=3)
        allocate(polymers(NUM_AUX_SIM))

        wig_lib => param_library%get_child(WIGNER_CAT)
        !GET NUMBER OF BEADS
        n_beads=wig_lib%get("n_beads")
        !! @input_file WIGNER_AUX/n_beads (wigner_bayes)
		if(n_beads<=0) STOP "Error: 'n_beads' of '"//WIGNER_CAT//"' is not properly defined!"
			polymers(:)%n_beads=n_beads
			f_param%n_beads=n_beads
        
        !GET NUMBER OF AUXILIARY SIMULATION STEPS
        f_param%n_steps=wig_lib%get("n_steps")
        !! @input_file WIGNER_AUX/n_steps (wigner_bayes)
        if(f_param%n_steps<=0) &
            STOP "Error: 'n_steps' of '"//WIGNER_CAT//"' is not properly defined!"

        f_param%n_steps_therm=wig_lib%get("n_steps_therm")
        !! @input_file WIGNER_AUX/n_steps_therm (wigner_bayes)
        if(f_param%n_steps_therm<=0) &
            STOP "Error: 'n_steps_therm' of '"//WIGNER_CAT//"' is not properly defined!"
        
        f_param%n_samples=wig_lib%get("n_samples")
        !! @input_file WIGNER_AUX/n_samples (wigner_bayes)
        if(f_param%n_samples<=0) &
            STOP "Error: 'n_samples' of '"//WIGNER_CAT//"' is not properly defined!"

        f_param%spring_force=wig_lib%get("spring_force",default=.FALSE.)
        !! @input_file WIGNER_AUX/spring_force (wigner_bayes, default=.FALSE.)

        !INITIALIZE FORCES PARAMETERS
        allocate( &
            f_param%EigMat(n_beads,n_beads), &
            f_param%EigMatTr(n_beads,n_beads), &
            f_param%OmK(n_beads) &
        )
        f_param%dq=0._wp
        f_param%beta=parameters%beta
        f_param%dBeta=parameters%beta/REAL(f_param%n_beads,wp)
        f_param%sigp2=f_param%dBeta/system%mass
        f_param%sigp2_inv=1._wp/f_param%sigp2
        ! GET delta_mult
        f_param%DeltaMult=wig_lib%get("delta_mult",default=1._wp)
        !! @input_file WIGNER_AUX/delta_mult (wigner_bayes, default=1., if move_generator="DE_BROGLIE_MC","SEPARATE_MC") 

        !INITIALIZE WORK VARIABLES
        allocate( &
            WORK%F_s(f_param%n_samples), &
            WORK%k2_s(f_param%n_samples), &
            WORK%k4_s(f_param%n_samples), &
            WORK%EigX(n_beads), &
            WORK%EigV(n_beads), &
            WORK%R1(n_beads), &
            WORK%R2(n_beads), &
            WORK%tmp(n_beads) &
        )

        !INITIALIZE polymers
        DO i=1,NUM_AUX_SIM
            allocate( &
                polymers(i)%X(0:n_beads), &
                polymers(i)%P(n_beads), &
                polymers(i)%Pot(0:n_beads) &
            )
            polymers(i)%Delta=0._wp
            polymers(i)%X(:)=system%X
            call randGaussN(polymers(i)%P(:))
            polymers(i)%P(:)=polymers(i)%P(:)*SQRT(f_param%sigp2_inv)
            polymers(i)%Pot(:)=Pot(mat(system%X))
        ENDDO

        call initialize_move_generator(system,parameters,wig_lib)

        f_param%write_aux_traj=wig_lib%get("write_aux_traj",default=.FALSE.)
        f_param%write_beads_traj=wig_lib%get("write_beads_traj",default=.FALSE.)
        if(.not. parameters%only_auxiliary_simulation) then
            if(f_param%write_aux_traj) then
                f_param%write_aux_traj=.FALSE.
                write(0,*) "Warning: cannot save auxiliary trajectory when &
                    &not in mode 'only_auxiliary_simulation'."
            endif
            if(f_param%write_beads_traj) then
                f_param%write_beads_traj=.FALSE.
                write(0,*) "Warning: cannot save auxiliary trajectory when &
                    &not in mode 'only_auxiliary_simulation'."
            endif
            n_therm_tmp = f_param%n_steps_therm
            f_param%n_steps_therm = 100000+10* n_therm_tmp
            call compute_wigner_forces(system,parameters)
            f_param%n_steps_therm = n_therm_tmp
        endif

    end subroutine initialize_auxiliary_simulation

    subroutine initialize_move_generator(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,j,k
        REAL(wp), allocatable :: DeltaMult_tmp(:)

        f_param%move_generator=wig_lib%get("move_generator" &
                    ,default="PIOUD")
        !! @input_file WIGNER_AUX/move_generator (wigner_bayes, default="PIOUD")
        f_param%move_generator=to_upper_case(f_param%move_generator)
        parameters%move_generator=f_param%move_generator

        SELECT CASE(f_param%move_generator)
        CASE ("PIOUD")

            move_proposal => PIOUD_step
            reinitialize_move_generator => reinitialize_pioud
            CALL initialize_PIOUD(polymers,system,parameters,wig_lib)

        CASE ("DE_BROGLIE_MC")

            move_proposal => deBroglie_step
            reinitialize_move_generator => reinitialize_debroglie_mc
            call initialize_debroglie_mc(system,parameters,wig_lib)

        CASE ("SEPARATE_MC")

            move_proposal => separate_mc_step
            reinitialize_move_generator => reinitialize_separate_mc
            call initialize_separate_mc(system,parameters,wig_lib)
        
        CASE ("RDELTA_MC")

            move_proposal => rdelta_mc_step
            reinitialize_move_generator => reinitialize_rdelta_mc
            call initialize_rdelta_mc(system,parameters,wig_lib)
            
        CASE DEFAULT
            write(0,*) "Error: unknown move generator '"//f_param%move_generator//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_move_generator

!----------------------------------------------------------------------
! COMPUTE EDGEWORTH CORRECTION

	subroutine compute_EW(system,parameters)
		implicit none
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
        CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		INTEGER :: i
		REAL(wp) :: k2(NUM_AUX_SIM)
		REAL(wp) :: C6

		system%EW=0
        DO i=1,NUM_AUX_SIM
            k2(i)= polymers(i)%k2
        ENDDO

        system%EW = system%P**4*(system%k4 - 3*unbiased_product(k2,k2))
		system%EW = 1 + system%EW / 24._wp		

        C6 = system%k6 - 15._wp*unbiased_product(k2,polymers(:)%k4) + 30._wp*unbiased_product(k2,k2,k2)
        system%EW6 = system%EW - system%P**6 * C6 / 720._wp

	end subroutine compute_EW

END MODULE wb_forces