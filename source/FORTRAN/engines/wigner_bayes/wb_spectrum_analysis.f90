module wb_spectrum_analysis
  USE kinds
  USE wb_types
  USE nested_dictionaries
  USE atomic_units
  USE qtb_types, only: QTB_theta
  IMPLICIT NONE

  TYPE SPECTRUM_ANALYSIS_1D
    real(wp) :: omegacut
		INTEGER :: nom
		real(wp) :: domega
    real(wp) :: dt
    INTEGER :: n_rand
    INTEGER :: selector, i_traj

		real(wp), allocatable :: vfspec_aver(:)
    real(wp), allocatable :: ffspec_aver(:)
		real(wp), allocatable :: vvspec_aver(:)
		real(wp), allocatable :: vfspec(:)
    real(wp), allocatable :: ffspec(:)
		real(wp), allocatable :: vvspec(:)

		real(wp), allocatable :: veloc_store(:)
		real(wp), allocatable :: randforce_store(:)

  CONTAINS
    PROCEDURE :: initialize => initialize_spectrum_analysis
    PROCEDURE :: update => update_spectrum_analysis
    PROCEDURE :: write_average => write_ffspec_aver
  END TYPE

  TYPE(SPECTRUM_ANALYSIS_1D), SAVE :: spectrum_analyser

  PRIVATE
  PUBLIC :: spectrum_analyser

CONTAINS


  subroutine store_velocities(this,system)
      implicit none
      CLASS(SPECTRUM_ANALYSIS_1D), INTENt(inout) :: this
      CLASS(WIG_SYS_1D), INTENT(in) :: system
      INTEGER             :: i 

      this%veloc_store(this%selector) = system%P/system%mass
	end subroutine store_velocities

  subroutine store_randforce(this,system)
      implicit none
      CLASS(SPECTRUM_ANALYSIS_1D), INTENt(inout) :: this
      CLASS(WIG_SYS_1D), INTENT(in) :: system
      INTEGER             :: i 

      this%randforce_store(this%selector) = system%rand_force

	end subroutine store_randforce

  subroutine compute_vv_vf_ff_spec(this)
        implicit none
        CLASS(SPECTRUM_ANALYSIS_1D), INTENt(inout) :: this
        integer :: i, j, istep, ifail
        real(wp) :: fr(2*this%n_rand), fi(2*this%n_rand), &
                    vr(2*this%n_rand), vi(2*this%n_rand), &
                    work(2*this%n_rand)
        real(wp) :: norm
              

        norm=2._wp*this%dt
            
            fr = 0._wp
            fr(1:this%n_rand) = this%randforce_store(:)/this%dt
            fi(:) = 0._wp
            work(:) = 0._wp
            vr = 0._wp
            vr(1:this%n_rand) = this%veloc_store(:)
            vi(:) = 0._wp
    
            ifail=0
            call C06FCF(fr,fi,2*this%n_rand,work,ifail)    ! FFT
            ifail=0
            call C06FCF(vr,vi,2*this%n_rand,work,ifail)    ! FFT	 
    
    
            do istep = 1, this%n_rand
                this%vfspec(istep) = norm*(vr(istep)*fr(istep) + vi(istep)*fi(istep))
                this%vvspec(istep) = norm*(vr(istep)**2 + vi(istep)**2)
                this%ffspec(istep) = norm*(fr(istep)**2 + fi(istep)**2)

                ! this%vfspec(i,j,istep)  = ( this%vfspec(i,j,istep)	   &
                !         + 0.5_wp *this%dt * this%ffspec(i,j,istep) / this%mass(i) )    &
                !     / (1._wp + this%gamma0 * this%dt * 0.5_wp)
            enddo


    end subroutine compute_vv_vf_ff_spec

    subroutine average_vv_vf_ff_spec(this)
      implicit none   
      CLASS(SPECTRUM_ANALYSIS_1D), INTENT(inout) :: this

      this%i_traj=this%i_traj+1

      this%ffspec_aver = this%ffspec_aver + this%ffspec(:) 
      this%vvspec_aver = this%vvspec_aver + this%vvspec(:) 
      this%vfspec_aver = this%vfspec_aver + this%vfspec(:)

   
	end subroutine average_vv_vf_ff_spec

  subroutine initialize_spectrum_analysis(this,system,parameters,param_library)
    implicit none   
    CLASS(SPECTRUM_ANALYSIS_1D), INTENT(inout) :: this
    CLASS(WIG_SYS_1D), intent(inout) :: system
		CLASS(WIG_PARAM_1D), intent(inout) :: parameters
    TYPE(DICT_STRUCT), intent(in) :: param_library

    this%dt = parameters%dt

    this%i_traj = 0 ; this%selector = 0
    this%n_rand = parameters%fdt_stride

    this%omegacut = param_library%get("WIGNER_AUX/omegacut")

    this%domega      = pi/(this%n_rand*this%dt) !2._wp*pi/(this%n_rand*this%dt)
		this%nom         = nint(this%omegacut/this%domega)

    allocate( this%vfspec(this%n_rand) )
    allocate( this%vvspec(this%n_rand) )
    allocate( this%ffspec(this%n_rand) )
    allocate( this%randforce_store(this%n_rand) )
    allocate( this%veloc_store(this%n_rand) )
    !allocate( this%gamma_store(this%n_atoms,this%n_dim,this%n_rand) )
    allocate( this%vfspec_aver(this%n_rand)  )
    allocate( this%vvspec_aver(this%n_rand)  )
    allocate( this%ffspec_aver(this%n_rand)  )
    !allocate( this%gammaspec_aver(this%n_atoms,this%n_dim,this%n_rand)  )
   ! allocate( this%gammaspec(this%n_atoms,this%n_dim,this%n_rand)  )

    this%vvspec_aver = 0
    this%ffspec_aver = 0
    this%vfspec_aver = 0
   ! this%gammaspec_aver = 0

  end subroutine initialize_spectrum_analysis

  subroutine write_ffspec_aver(this,system,parameters,filename,restart_average)
    implicit none
    integer   :: l_step,u,i,j,c
    real(wp)  :: omega
    CLASS(SPECTRUM_ANALYSIS_1D), INTENT(inout) :: this
    CLASS(WIG_SYS_1D), intent(inout) :: system
    CLASS(WIG_PARAM_1D), intent(inout) :: parameters
    LOGICAL, INTENT(in), OPTIONAL :: restart_average
    CHARACTER(*), INTENT(in) :: filename
    REAL(wp):: mCvv_gammar

    this%ffspec_aver = this%ffspec_aver/real(this%i_traj,wp)
    this%vvspec_aver = this%vvspec_aver/real(this%i_traj,wp)
    this%vfspec_aver = this%vfspec_aver/real(this%i_traj,wp)
   ! this%gammaspec_aver = this%gammaspec_aver/real(this%i_traj,wp)

    open(newunit=u, file=output%working_directory//'/'//trim(filename))
    write(u,*) "#omega(cm^-1)    Cff    Cvv    sig2*Cvv/2theta    Cvf   theta(om,kT)"
    DO l_step = 1, this%nom
        omega = real(l_step,wp) * this%domega
        mCvv_gammar = 0.5_wp*this%vvspec_aver(l_step)*this%ffspec_aver(l_step)
        
        write(u,*) omega* cm1 , this%ffspec_aver(l_step) &
        , this%vvspec_aver(l_step) &
        , mCvv_gammar/QTB_theta(omega,parameters%temperature) &
        , this%vfspec_aver(l_step)
    ENDDO
    
    close(u)

    if(present(restart_average)) then
        if(restart_average) then
            this%ffspec_aver=0._wp
            this%vvspec_aver=0._wp
            this%vfspec_aver=0._wp
            this%i_traj=0
        endif
    endif

  end subroutine write_ffspec_aver

  subroutine update_spectrum_analysis(this,system,parameters)
    implicit none   
    CLASS(SPECTRUM_ANALYSIS_1D), INTENT(inout) :: this
    CLASS(WIG_SYS_1D), intent(inout) :: system
    CLASS(WIG_PARAM_1D), intent(inout) :: parameters

    this%selector = this%selector + 1

    call store_velocities(this,system)
    call store_randforce(this,system)

    IF(this%selector >= this%n_rand) THEN
      call compute_vv_vf_ff_spec(this)
      call average_vv_vf_ff_spec(this)
      this%selector=0
    ENDIF

  end subroutine update_spectrum_analysis


end module wb_spectrum_analysis