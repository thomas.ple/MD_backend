MODULE wigner_bayes
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE random
	USE wb_types
	USE wb_integrators
	USE classical_md, only: classical_MD_get_parameters
	USE wb_forces, only: compute_EW,compute_wigner_forces
	IMPLICIT NONE	

	TYPE(wIG_SYS_1D), save, target :: wig_system
	TYPE(WIG_PARAM_1D), save, target :: wig_parameters

	PROCEDURE(wig_sub), pointer :: wig_integrator

	PRIVATE :: wig_system, wig_parameters

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine wb_md_loop()
		IMPLICIT NONE
		INTEGER :: i,n_dof
		REAL(wp) :: k2_mean
		REAL :: time_start, time_finish
		

		IF(wig_parameters%only_auxiliary_simulation) THEN
			call compute_wigner_forces(wig_system,wig_parameters)
			write(*,*)
			write(*,*) "--------------------"
			write(*,*) "--------------------"
			write(*,*) "k2=",wig_system%k2
			write(*,*) "sqrt(var(k2))/k2=",wig_system%k2_var
			write(*,*) "--------------------"
			write(*,*) "--------------------"
			IF(wig_parameters%move_generator=="SEPARATE_MC") THEN
				write(*,*) "acc_ratio_delta=",wig_system%acc_ratio_delta
				write(*,*) "acc_ratio_staging=",wig_system%acc_ratio_staging
				write(*,*) "--------------------"
				write(*,*) "--------------------"			
			ENDIF
			write(*,*)
			return
		ENDIF


		if(wig_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",wig_parameters%n_steps_therm," steps &
						&of thermalization"
			call cpu_time(time_start)
			DO i=1,wig_parameters%n_steps_therm
				call wig_integrator(wig_system,wig_parameters)
				k2_mean=k2_mean + (wig_system%k2 - k2_mean)/REAL(i,wp)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			if(wig_parameters%compute_fdt) &
				call spectrum_analyser%write_average(wig_system,wig_parameters, &
									"wigner_spectra.thermalization.out",.TRUE.)
			! UPDATE sigma FROM k2_mean
			wig_parameters%sigma=SQRT(2._wp*wig_parameters%gamma0/k2_mean)

			call wb_write_restart_file(wig_system,wig_parameters &
						,wig_parameters%n_steps_prev+wig_parameters%n_steps_therm)
		endif

		write(*,*)
		write(*,*) "Starting ",wig_parameters%n_steps," steps &
						&of WiLD"
		call cpu_time(time_start)
		DO i=1,wig_parameters%n_steps

			wig_system%time=wig_system%time &
								+wig_parameters%dt

			call wig_integrator(wig_system,wig_parameters)
			call wig_compute_EW(wig_system,wig_parameters)

			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(wig_parameters%write_restart) then
				if(mod(i,wig_parameters%restart_stride)==0) &
					call wb_write_restart_file(wig_system,wig_parameters &
						,wig_parameters%n_steps_prev+i)
			endif

		ENDDO
		call cpu_time(time_finish)
		write(*,*) "Simulation done in ",time_finish-time_start,"s."

		if(wig_parameters%compute_fdt) &
				call spectrum_analyser%write_average(wig_system,wig_parameters, &
									"wigner_spectra.out")

		call wb_write_restart_file(wig_system,wig_parameters &
					,wig_parameters%n_steps_prev+wig_parameters%n_steps)	
		

	end subroutine wb_md_loop
!-----------------------------------------------------------------

	subroutine initialize_wb_MD(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart

		load_restart=.FALSE.
		wig_parameters%engine="wigner_bayes"

		call wb_initialize_restart_file(wig_system,wig_parameters,param_library)

		!ASSOCIATE classicla_MD_loop
		simulation_loop => wb_md_loop
		
		!GET ALGORITHM
		wig_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (wigner, default="nvt")
		wig_parameters%algorithm=to_lower_case(wig_parameters%algorithm)

		call wb_get_parameters(wig_system,wig_parameters,param_library)

		if(.not. wig_parameters%only_auxiliary_simulation) then
			call wb_get_integrator_parameters(wig_system,wig_parameters,param_library)	

			call wb_initialize_momenta(wig_system,wig_parameters)	
			
			! LOAD RESTART FILE IS NEEDED
			wig_parameters%n_steps_prev=0
			load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
			if(load_restart) then
				call wb_load_restart_file(wig_system,wig_parameters)
				wig_parameters%n_steps_therm=0
			endif
		endif

		call wb_initialize_output(wig_system,wig_parameters,param_library)

		if(wig_parameters%compute_fdt) &
			call spectrum_analyser%initialize(wig_system,wig_parameters,param_library)

		call wb_initialize_forces(wig_system,wig_parameters, param_library)

		if((.not. load_restart) .and. (.not. wig_parameters%only_auxiliary_simulation)) &
			call wb_thermalize_momenta(wig_system,wig_parameters)
		
		
	end subroutine initialize_wb_MD

	subroutine finalize_wb_MD()
		IMPLICIT NONE

		call wb_deallocate(wig_system,wig_parameters)

	end subroutine finalize_wb_MD

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine wb_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		parameters%temperature=param_library%get("PARAMETERS/temperature")
		if(parameters%temperature <= 0) STOP "Error: 'temperature' is not properly defined!"
		parameters%beta=1._wp/parameters%temperature

		system%time=0._wp
		system%mass=param_library%get("PARAMETERS/mass",default=Mprot)
		if(system%mass <= 0) STOP "Error: 'mass' is not properly defined!"

		system%X=param_library%get("PARAMETERS/xinit",default=0._wp)
		system%X_prev=system%X

		parameters%n_steps=param_library%get("PARAMETERS/n_steps")
		if(parameters%n_steps <= 0) STOP "Error: 'n_steps' is not properly defined!"
		parameters%n_steps_therm=param_library%get("PARAMETERS/n_steps_therm",default=0)
		if(parameters%n_steps_therm < 0) STOP "Error: 'n_steps_therm' is not properly defined!"

		parameters%dt=param_library%get("PARAMETERS/dt")
		if(parameters%dt <= 0) STOP "Error: 'dt' is not properly defined!"

		parameters%only_auxiliary_simulation=param_library%get(WIGNER_CAT//"/only_auxiliary_simulation" &
												,default=.FALSE.)
		parameters%noise_correction=param_library%get(WIGNER_CAT//"/noise_correction" &
												,default=.FALSE.)
		if(parameters%noise_correction) write(*,*) "Noise correction activated."

	end subroutine wb_get_parameters

	subroutine wb_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt")

			!classical initialization for the moment
			call randGauss(system%P)
			system%P=system%P*SQRT(system%mass*parameters%temperature)

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine wb_initialize_momenta

	subroutine wb_thermalize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		REAL(wp) :: sigma
		INTEGER :: INFO

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt")

			call randGauss(system%P)
			system%P=sqrt(system%k2inv)*system%P

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine wb_thermalize_momenta

!-----------------------------------------------------------------

	subroutine wb_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: i

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default="aboba")
		!! @input_file PARAMETERS/integrator (wigner, default="aboba")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt")
			
			parameters%gamma0=param_library%get("THERMOSTAT/damping")
			!! @input_file THERMOSTAT/damping (wigner)
			parameters%sigma=SQRT( &
				2._wp*parameters%gamma0*system%mass*parameters%temperature &
			)

			SELECT CASE(parameters%integrator_type)
			CASE("aboba")
				wig_integrator => wb_ABOBA
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT		

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine wb_get_integrator_parameters

!-----------------------------------------------------------------

	subroutine wb_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name
		INTEGER :: i_file, i
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%compute_fdt=.FALSE.

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("position","positions")
			!! @input_file OUTPUT/position (wigner)
				description="trajectory of positions"
				default_name="X.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%X,index=i_file,name="positions")

			CASE("momentum","momenta")		
			!! @input_file OUTPUT/momentum (wigner)	
				description="trajectory of momenta"
				default_name="P.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%P,index=i_file,name="momentum")
			
			CASE("ew","edgeworth")		
			!! @input_file OUTPUT/edgeworth (wigner)		
				description="trajectory of the edgeworth correction"
				default_name="EW.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%EW,index=i_file,name="EW order 4")
				!if(system%n_dof==1) &
					call output%add_column_to_file(system%EW6,index=i_file,name="EW order 6")
			
			CASE("k2_var")	
			!! @input_file OUTPUT/k2_var (wigner)		
				description="trajectory of the k2 and delta**2 variances"
				default_name="k2_var.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%k2_var,index=i_file,name="k2_var")
				call output%add_column_to_file(system%delta2_var,index=i_file,name="delta2_var")
			
			CASE("k2")		
			!! @input_file OUTPUT/k2 (wigner)		
				description="trajectory of k2"
				default_name="k2.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%k2,index=i_file,name="k2")
				call output%add_column_to_file(system%k2inv,index=i_file,name="k2 inverse")
			
			CASE("f_var")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of F variance"
				default_name="F_var.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%F_var,index=i_file,name="F_var")
			
			CASE("dyn_corr")
			!! @input_file OUTPUT/dyn_corr (wigner)	
				!if(system%n_dof==1) then
					description="trajectory of the correction for correlation functions"
					default_name="dyn_corr.traj"
					call output%create_file(dict,default_name,index=i_file)
					call output%files(i_file)%edit_description(description)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_column_to_file(system%beta_dU,index=i_file,name="beta*dU/dq")
					call output%add_column_to_file(system%dk2,index=i_file,name="dk2/dq")
					call output%add_column_to_file(system%dC4,index=i_file,name="dC4/dq")
					call output%add_column_to_file(system%C4_beta_dU,index=i_file,name="C4*beta*dU/dq")
					call output%add_column_to_file(system%C4,index=i_file,name="C4")
				!endif
			
			CASE("check_fdt")
			!! @input_file OUTPUT/print_system_info(wigner)
				parameters%compute_fdt = .TRUE.
				parameters%fdt_stride=dict%get("stride",default=2000)
			
			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

		SELECT CASE(parameters%move_generator)
		CASE("DE_BROGLIE_MC","SEPARATE_MC","RDELTA_MC")
			description="trajectory of acceptance ratio for the auxiliary Monte Carlo."
			call output%create_file("acc_ratio.traj",formatted=.TRUE.,description=description)
				call output%add_column_to_file(system%acc_ratio_delta,name="acc_ratio_delta")
				call output%add_column_to_file(system%acc_ratio_staging,name="acc_ratio_staging")
		END SELECT

	end subroutine wb_initialize_output

END MODULE wigner_bayes