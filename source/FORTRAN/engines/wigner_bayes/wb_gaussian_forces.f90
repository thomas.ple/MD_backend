MODULE wb_gaussian_forces
	USE kinds
    USE wb_types
    USE nested_dictionaries
	USE atomic_units
	IMPLICIT NONE

	CHARACTER(:), ALLOCATABLE, SAVE :: weight_file
	INTEGER, SAVE :: n_points
	REAL(wp), SAVE :: width_U, width_k2, width_k4
	REAL(wp), ALLOCATABLE, SAVE :: Z(:)
	REAL(wp), ALLOCATABLE, SAVE :: W_U(:), W_k2(:), W_k4(:)

	PRIVATE
	PUBLIC :: initialize_gaussian_forces, compute_gaussian_forces,compute_EW_gaussian

CONTAINS

	subroutine compute_gaussian_forces(system,parameters)
		IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
		REAL(wp) :: dU

		system%k2=predict(system%X,W_k2,width_k2)
		system%k2inv=1._wp/system%k2
		system%k4=predict(system%X,W_k4,width_k4)
		system%C4=system%k4 - 3*system%k2**2

		dU=predict_deriv(system%X,W_U,width_U)
		system%beta_dU=dU/parameters%temperature
		system%C4_beta_dU=system%beta_dU*system%C4

		system%dk2=predict_deriv(system%X,W_k2,width_k2)
		system%C=-0.5*system%dk2/(system%k2*system%mass)
		system%F=-system%beta_dU/(system%k2*system%mass) &
					-system%dk2/(system%mass*system%k2**2)

		system%dC4=predict_deriv(system%X,W_k4,width_k4) &
					-6*system%k2*system%dk2
	end subroutine compute_gaussian_forces

	subroutine compute_EW_gaussian(system,parameters)
		IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
        CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters

		system%EW = 1 + system%C4*system%P**4/24
	end subroutine compute_EW_gaussian


	subroutine initialize_gaussian_forces(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(WIG_SYS_1D), INTENT(inout) :: system
		CLASS(WIG_PARAM_1D), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: file_exists
		INTEGER :: u,tmp,i

		weight_file=trim(param_library%get(WIGNER_CAT//"/weight_file"))
		INQUIRE(file=weight_file, exist=file_exists)
		IF(.NOT. file_exists) THEN
			write(0,*) "Error: weight_file '"//weight_file//"' does not exist!"
			STOP "Execution stopped."
		ENDIF

		OPEN(newunit=u,file=weight_file, action="READ")
		READ(u,*) n_points,tmp
		READ(u,*) width_U, width_k2, width_k4

		ALLOCATE( &
			 Z(n_points) &
			,W_U(n_points) &
			,W_k2(n_points) &
			,W_k4(n_points) &
		)
		
		DO i=1,n_points
			READ(u,*) Z(i) &
					, W_U(i) &
					, W_k2(i) &
					, W_k4(i)
		ENDDO

		write(*,*) "gaussian forces : n_points=",n_points
		write(*,*) "gaussian forces : widths=", width_U*bohr, width_k2*bohr, width_k4*bohr, "angstrom"
		write(*,*) "gaussian forces : positions=", Z*bohr, "angstrom"
	
	end subroutine initialize_gaussian_forces

	function predict(x,weights,width) result(U)
		REAL(wp), intent(in) :: x, weights(:), width
		REAL(wp) :: U
		U=dot_product(weights,gauss_kernel(x,width))
	end function predict

	function predict_deriv(x,weights,width) result(U)
		REAL(wp), intent(in) :: x, weights(:), width
		REAL(wp) :: U
		U=dot_product(weights,dgauss_kernel(x,width))
	end function predict_deriv

	function gauss_kernel(x,width) result(g)
		REAL(wp), intent(in) :: x, width
		REAL(wp) :: g(n_points)
		g=exp(-((x-Z(:))/width)**2/2)
	end function gauss_kernel

	function dgauss_kernel(x,width) result(g)
		REAL(wp), intent(in) :: x, width
		REAL(wp) :: g(n_points)
		g=((Z(:)-x)/width**2)*exp(-((x-Z(:))/width)**2/2)
	end function dgauss_kernel


END MODULE wb_gaussian_forces