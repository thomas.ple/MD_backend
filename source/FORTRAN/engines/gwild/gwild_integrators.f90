MODULE gwild_integrators
  USE gwild_types
	USE random
  USE basic_types
	USE gwild_forces, only : compute_gwild_forces, initialize_auxiliary_simulation
	USE nested_dictionaries
	USE matrix_operations
	USE thermostats, only: apply_thermostat
  IMPLICIT NONE

	PRIVATE :: apply_A, apply_N

	PROCEDURE(gwild_sub), pointer :: gwild_compute_forces => null()

CONTAINS

    ! INTEGRATOR

	subroutine gwild_BAOAB(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), intent(inout) :: system
		CLASS(GWILD_PARAM), intent(inout) :: parameters
		INTEGER :: i,j,c
		REAL(wp) :: sigma, gamma_exp,R(system%n_atoms)

		system%P=system%P + 0.5*parameters%dt*system%Forces
		call gwild_update_P_mod_from_P(system,parameters)
		
		CALL apply_A(system,parameters,0.5_wp*parameters%dt)

		if(parameters%nose_control) &
			call apply_N(system,parameters,0.5_wp*parameters%dt)

		gamma_exp=exp(-parameters%gamma0*parameters%dt)
		sigma=SQRT( parameters%temperature*( 1._wp-gamma_exp*gamma_exp ) )
		gamma_exp=gamma_exp*exp(-system%nose*parameters%dt)

		do i=1,system%n_dim		
			call randGaussN(R)
			system%P(:,i)=gamma_exp*system%P(:,i) &
											+ sigma*R(:)
		enddo

		if(parameters%nose_control) &
			call apply_N(system,parameters,0.5_wp*parameters%dt)

		call gwild_update_P_mod_from_P(system,parameters)

		CALL apply_A(system,parameters,0.5_wp*parameters%dt)

		call gwild_compute_forces(system,parameters)
		
		system%P=system%P + 0.5*parameters%dt*system%Forces
		call gwild_update_P_mod_from_P(system,parameters)

	end subroutine gwild_BAOAB


	subroutine apply_A(system,parameters,tau)
		IMPLICIT NONE
		CLASS(GWILD_SYS), intent(inout) :: system
		CLASS(GWILD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i
		!!! WARNING: P_mod depends on X so that this integration is not exact !!!
		!!! possible solution: order 2 sympleptic integrator                  !!!
		!!!											 with statistical reweighting of Q            !!!
		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + tau*system%P_mod(:,i)/system%mass(:)
		enddo

	end subroutine apply_A

	subroutine apply_N(system,parameters,tau)
		IMPLICIT NONE
		CLASS(GWILD_SYS), intent(inout) :: system
		CLASS(GWILD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: temp_estimated

		temp_estimated=sum(system%P**2)
		system%nose=exp(-system%damping_nose*tau)*system%nose + (tau/system%M_nose) &
			*(temp_estimated - system%n_dof*parameters%temperature)

	end subroutine apply_N



!---------------------------------------------
! FORCES CALCULATORS

	subroutine gwild_initialize_forces(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dof
		
		! ASSIGN FORCES CALCULATOR	
		gwild_compute_forces => compute_gwild_forces
		call initialize_auxiliary_simulation(system,parameters,param_library)

	end subroutine gwild_initialize_forces

END MODULE gwild_integrators