MODULE gwild
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE random
	USE gwild_types
	USE gwild_integrators
	USE classical_md, only: classical_MD_get_parameters, classical_MD_initialize_dipole
	USE gwild_forces, only: compute_gwild_forces !,compute_EW
	USE thermostats, only : initialize_thermostat, finalize_thermostat, thermostat_end_thermalization
	IMPLICIT NONE	

	TYPE(GWILD_SYS), save, target :: gwild_system
	TYPE(GWILD_PARAM), save, target :: gwild_parameters

	PROCEDURE(gwild_sub), pointer :: gwild_integrator

	PRIVATE :: gwild_system, gwild_parameters

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine gwild_loop()
		IMPLICIT NONE
		INTEGER :: i,n_dof,n_dim,n_atoms,u
		REAL :: time_start, time_finish
	

		IF(gwild_parameters%only_auxiliary_simulation) THEN
			call compute_gwild_forces(gwild_system,gwild_parameters)
			call save_forces_results(gwild_system,gwild_parameters)
			RETURN
		ENDIF


		if(gwild_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",gwild_parameters%n_steps_therm," steps &
						&of thermalization"
			if(gwild_parameters%gamma0 /= gwild_parameters%gamma0_backup) &
				write(*,*) "damping for thermalization=",gwild_parameters%gamma0*THz,"THz"
			call cpu_time(time_start)
			DO i=1,gwild_parameters%n_steps_therm
				if(gwild_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				call gwild_integrator(gwild_system,gwild_parameters)
				call gwild_print_system_info(gwild_system,gwild_parameters,i)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			call gwild_write_restart_file(gwild_system,gwild_parameters,0)
		endif

		gwild_parameters%gamma0 = gwild_parameters%gamma0_backup
		write(*,*)
		write(*,*) "Starting ",gwild_parameters%n_steps," steps &
						&of g-WiLD"
		call cpu_time(time_start)
		DO i=1,gwild_parameters%n_steps

			if(gwild_parameters%check_exit_file()) then
				call gwild_write_restart_file(gwild_system,gwild_parameters &
						,gwild_parameters%n_steps_prev+i)
				write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
				RETURN
			endif

			gwild_system%time=gwild_system%time &
								+gwild_parameters%dt

			
			call gwild_integrator(gwild_system,gwild_parameters)
			
			! call compute_EW(gwild_system,gwild_parameters)

			if(gwild_parameters%compute_dipole) then				
				if(mod(i,gwild_parameters%compute_dipole_stride)==0) &
					call gwild_compute_dipole(gwild_system,gwild_parameters)
			endif	
			
			! call gwild_compute_temp_estimated(gwild_system,gwild_parameters)
			call gwild_print_system_info(gwild_system,gwild_parameters,i)

			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(gwild_parameters%write_restart) then
				if(mod(i,gwild_parameters%restart_stride)==0) &
					call gwild_write_restart_file(gwild_system,gwild_parameters &
						,gwild_parameters%n_steps_prev+i)
			endif

		ENDDO
		call cpu_time(time_finish)
		write(*,*) "Simulation done in ",time_finish-time_start,"s."

		call gwild_write_restart_file(gwild_system,gwild_parameters &
					,gwild_parameters%n_steps_prev+gwild_parameters%n_steps)	
		

	end subroutine gwild_loop
!-----------------------------------------------------------------

	subroutine initialize_gwild(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart,qtb_thermalization

		load_restart=.FALSE.
		gwild_parameters%engine="gwild"
		qtb_thermalization=.FALSE.

		call gwild_initialize_restart_file(gwild_system,gwild_parameters,param_library)

		!ASSOCIATE loop FUNCTION
		simulation_loop => gwild_loop

		!GET ALGORITHM
		gwild_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (gwild, default="nvt")
		gwild_parameters%algorithm=to_lower_case(gwild_parameters%algorithm)

		call gwild_get_parameters(gwild_system,gwild_parameters,param_library)
		if(.not. allocated(gwild_system%Forces)) &
				allocate(gwild_system%Forces(gwild_system%n_atoms,gwild_system%n_dim))

		IF(.not. gwild_parameters%only_auxiliary_simulation) THEN
			call gwild_get_integrator_parameters(gwild_system,gwild_parameters,param_library)	

			call gwild_initialize_momenta(gwild_system,gwild_parameters)	
			gwild_system%Forces=0._wp
			
			! LOAD RESTART FILE IS NEEDED
			gwild_parameters%n_steps_prev=0
			load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
			if(load_restart) then
				call gwild_load_restart_file(gwild_system,gwild_parameters)
				gwild_parameters%n_steps_therm=0
			endif

			qtb_thermalization=param_library%get(GWILD_CAT//"/qtb_thermalization", default=.FALSE.)
			if(qtb_thermalization) &
				call gwild_thermalize_with_QTB(gwild_system,gwild_parameters,param_library)
			
		ENDIF

		call gwild_initialize_output(gwild_system,gwild_parameters,param_library)

		call gwild_initialize_forces(gwild_system,gwild_parameters, param_library)

		IF(.not. gwild_parameters%only_auxiliary_simulation) THEN
			if(qtb_thermalization .AND. (.not. load_restart)) then
				gwild_system%P_mod=gwild_system%P
				call gwild_update_P_from_P_mod(gwild_system,gwild_parameters)
			else
				call gwild_update_P_mod_from_P(gwild_system,gwild_parameters)
			endif
			
			if(gwild_parameters%compute_dipole) then
				call classical_MD_initialize_dipole(gwild_system,gwild_parameters,param_library)
				call gwild_compute_dipole(gwild_system,gwild_parameters)
			endif
		ENDIF

	end subroutine initialize_gwild

	subroutine finalize_gwild()
		IMPLICIT NONE

		call gwild_deallocate(gwild_system,gwild_parameters)

	end subroutine finalize_gwild

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine gwild_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dof

		call classical_MD_get_parameters(system,parameters,param_library)

		! allocate(system%X_prev(system%n_atoms,system%n_dim))
		! system%X_prev=system%X

		n_dof=system%n_dof		
		
		allocate( &
			system%F_det(n_dof), &
			system%F_dU(n_dof), &
			system%F_phi(n_dof), &
			system%F_cl(n_dof), &
			system%Qinv(n_dof,n_dof), &
			! system%Qinv_mm(n_dof,n_dof), &
			system%Q(n_dof,n_dof), &
			system%sqrtQ(n_dof,n_dof), &
			system%QEigMat(n_dof,n_dof), &
			system%QinvEigVals(n_dof) &
		)
		system%F_det=0._wp
		system%F_dU=0._wp
		system%F_phi=0._wp
		system%F_cl=0._wp
		system%Qinv=0._wp
		system%Q=0._wp
		system%sqrtQ=0._wp
		system%QinvEigVals=0._wp
		system%QEigMat=0._wp

		parameters%nose_control=param_library%get(GWILD_CAT//"/use_nose_control" &
											,default=.FALSE.)
		system%nose=0				
		system%M_nose = param_library%get(GWILD_CAT//"/m_nose" &
											,default=1.0e6)
		system%damping_nose = param_library%get(GWILD_CAT//"/damping_nose" &
											,default=0.)

		parameters%only_auxiliary_simulation=param_library%get(GWILD_CAT//"/only_auxiliary_simulation" &
												,default=.FALSE.)
		parameters%apply_F_det=param_library%get(GWILD_CAT//"/apply_f_det" &
												,default=.TRUE.)

		system%lth2_det=product(parameters%beta/system%mass)**system%n_dim

		parameters%classical_wigner=param_library%get(GWILD_CAT//"/classical_wigner" &
											,default=.FALSE.)

	end subroutine gwild_get_parameters

	subroutine gwild_thermalize_with_QTB(system,parameters,param_library)
		USE classical_md_integrators, only : classical_MD_initialize_forces,class_MD_BAOAB
		USE classical_md_types, only: classical_MD_print_system_info,classical_MD_compute_kinetic_energy
		USE thermostats
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		REAL :: time_start, time_finish
		INTEGER :: i
		LOGICAL :: has_qtb_damping
		REAL(wp) :: damping_save,qtb_damping

		if(parameters%n_steps_therm > 0) then
			call param_library%store("THERMOSTAT/thermostat_type","qtb")

			has_qtb_damping=param_library%has_key(GWILD_CAT//"/qtb_damping")
			if(has_qtb_damping) then
				damping_save = param_library%get("THERMOSTAT/damping")
				qtb_damping  = param_library%get(GWILD_CAT//"/qtb_damping")
				call param_library%store("THERMOSTAT/damping",qtb_damping)
			endif

			call initialize_thermostat(system,parameters,param_library)
			call classical_MD_initialize_forces(system,parameters,param_library)
			
			write(*,*) "Starting ",gwild_parameters%n_steps_therm," steps &
						&of QTB thermalization"
			call cpu_time(time_start)
			DO i=1,parameters%n_steps_therm
				if(gwild_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				call class_MD_BAOAB(system,parameters)
				if(parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(system,parameters)
				call classical_md_print_system_info(system,parameters,i)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			parameters%n_steps_therm=0
			call finalize_thermostat(system,parameters)
			if(has_qtb_damping) then
				call param_library%store("THERMOSTAT/damping",damping_save)
			endif
			call param_library%store("THERMOSTAT/thermostat_type","langevin")
			call gwild_update_P_packed_from_P(system)
		endif
	end subroutine gwild_thermalize_with_QTB

	subroutine gwild_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i

		allocate(system%P(system%n_atoms,system%n_dim), &
				system%P_packed(system%n_atoms*system%n_dim))		
		allocate(system%P_mod(system%n_atoms,system%n_dim))
		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt")
			do i=1,system%n_dim
				call randGaussN(system%P(:,i))
				system%P(:,i)=system%P(:,i)*SQRT(parameters%temperature)
			enddo
			call gwild_update_P_packed_from_P(system)

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine gwild_initialize_momenta

!-----------------------------------------------------------------

	subroutine gwild_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: i
		CHARACTER(:), ALLOCATABLE :: default_integrator

		default_integrator = "baoab"

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default=default_integrator)
		!! @input_file PARAMETERS/integrator (gwild, default="aboba")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt")
			
			parameters%gamma0_backup=param_library%get("THERMOSTAT/damping")
			!! @input_file THERMOSTAT/damping (gwild)
			parameters%gamma0=param_library%get("THERMOSTAT/damping_thermalization" &
								,default=parameters%gamma0_backup)
			!! @input_file THERMOSTAT/damping (damping_thermalization)

			SELECT CASE(parameters%integrator_type)
			CASE("baoab")
				gwild_integrator => gwild_BAOAB
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT		

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine gwild_get_integrator_parameters

!-----------------------------------------------------------------

	subroutine gwild_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name
		INTEGER :: i_file, i,k
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%compute_EW = .FALSE.
		parameters%compute_dipole=.FALSE.

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("position","positions")
			!! @input_file OUTPUT/position (gwild)
				if(system%n_dim==1) then
					default_name="X.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X(:,1),index=i_file,name="positions")
				else
					default_name="position.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,name="positions")
				endif

			CASE("momentum","momenta")		
			!! @input_file OUTPUT/momentum (gwild)	
				description="trajectory of momenta"
				if(system%n_dim==1) then
					default_name="P.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")					
					call output%add_array_to_file(system%P_mod(:,1),index=i_file,name="momenta")
				else
					default_name="momentum.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%P_mod,system%element_symbols,index=i_file,name="momenta")
				endif

			CASE("p_tilde")		
				!! @input_file OUTPUT/p_tilde (gwild)	
					description="trajectory of p_tilde"
					if(system%n_dim==1) then
						default_name="P_tilde.traj"
						call output%create_file(dict,default_name,index=i_file)
	
						if(output%files(i_file)%has_time_column) &
							call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")					
						call output%add_array_to_file(system%P(:,1),index=i_file,name="p_tilde")
					else
						default_name="P_tilde.traj.xyz"
						call output%create_file(dict,default_name,index=i_file)
						call output%add_xyz_to_file(system%P,system%element_symbols,index=i_file,name="p_tilde")
					endif
			
			CASE("q_det")
				description="trajectory of Q determinant"
				default_name="Q_det.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%Qinv_det,index=i_file,name="Qinv_det")
				call output%add_column_to_file(system%sqrtQ_det,index=i_file,name="sqrtQ_det")
				call output%add_column_to_file(system%Q_det,index=i_file,name="Q_det")
			
			! CASE("ew","edgeworth")		
			! !! @input_file OUTPUT/edgeworth (gwild)		
			! 	parameters%compute_EW = .TRUE.
			! 	description="trajectory of the edgeworth correction"
			! 	default_name="EW.traj"
			! 	call output%create_file(dict,default_name,index=i_file)
			! 	call output%files(i_file)%edit_description(description)

			! 	if(output%files(i_file)%has_time_column) &
			! 		call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
			! 	call output%add_column_to_file(system%EW,index=i_file,name="EW order 4")
			! 	if(system%n_dof==1) &
			! 		call output%add_column_to_file(system%EW6,index=i_file,name="EW order 6")
			
			CASE("nose")		
			!! @input_file OUTPUT/nose (gwild)		
				description="trajectory of the nose control variable"
				default_name="nose.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%nose,index=i_file,name="nose")
				call output%add_column_to_file(parameters%gamma0,index=i_file,name="nose")
			
			CASE("q_inv")		
			!! @input_file OUTPUT/q_inv (gwild)		
				description="trajectory of Qinv (only the lower triangular part in col-major)"
				default_name="Qinv.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%Qinv(k:system%n_dof,k),index=i_file,name="Qinv")
				ENDDO
			
			! CASE("q_inv_mm")		
			! !! @input_file OUTPUT/q_inv (gwild)		
			! 	description="trajectory of Qinv_mm (only the lower triangular part in col-major)"
			! 	default_name="Qinv_mm.traj"
			! 	call output%create_file(dict,default_name,index=i_file)
			! 	call output%files(i_file)%edit_description(description)

			! 	if(output%files(i_file)%has_time_column) &
			! 		call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
			! 	DO k=1,system%n_dof
			! 		call output%add_array_to_file(system%Qinv_mm(k:system%n_dof,k),index=i_file,name="Qinv_mm")
			! 	ENDDO
			
			CASE("q_inv_eigvals")		
			!! @input_file OUTPUT/q_inv_eigvals (gwild)		
				description="trajectory of Qinv eigenvalues"
				default_name="Qinv_eigvals.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%QinvEigVals(:),index=i_file,name="Qinv_eigvals")
			
			CASE("q")		
			!! @input_file OUTPUT/q (gwild)		
				description="trajectory of Q (only the lower triangular part in col-major)"
				default_name="Q.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%Q(k:system%n_dof,k),index=i_file,name="Q")
				ENDDO

			CASE("f_det")	
			!! @input_file OUTPUT/f_det (gwild)			
				description="trajectory of F_det"
				default_name="F_det.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%F_det,index=i_file,name="F_det")
			
			CASE("f_du")	
				!! @input_file OUTPUT/f_du (gwild)			
					description="trajectory of F_dU "
					default_name="F_dU.traj"
					call output%create_file(dict,default_name,index=i_file)
					call output%files(i_file)%edit_description(description)
	
					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%F_dU,index=i_file,name="F_dU")
			
			CASE("f_phi")	
			!! @input_file OUTPUT/f_phi (gwild)			
				description="trajectory of F_phi"
				default_name="F_phi.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%F_phi,index=i_file,name="f_phi")
			
			CASE("f_classical")	
			!! @input_file OUTPUT/f_classical (gwild)			
				description="trajectory of classical force"
				default_name="F_classical.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%F_cl,index=i_file,name="F_cl")
			
			CASE("dipole","dipole_moment")	
			!! @input_file OUTPUT/dipole (gwild)			
				parameters%compute_dipole=.TRUE.
				parameters%compute_dipole_stride=dict%get("stride",default=1)
				description="trajectory of the dipole moment"
				default_name="dipole.traj"
				
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole,index=i_file,name="dipole")

				description="trajectory of the dipole moment velocity"
				default_name="dipole_velocity.traj"
				call dict%store("name",default_name)
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)
				

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole_velocity,index=i_file,name="dipole_velocity")
			
			CASE("print_system_info")
			!! @input_file OUTPUT/print_system_info(gwild)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
				parameters%keep_potential=.TRUE.
				parameters%print_temperature_stride=dict%get("stride",default=100)

			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO


		IF(parameters%compute_E_kin) THEN
			ALLOCATE(system%E_kin_mean(system%n_dof) &
							,system%E_kin(system%n_dof))
			system%E_kin_mean=0
			system%E_kin=0
			system%temp_estimated=0
			system%temp_k2=0
			system%E_kin_k2=0
			system%E_kin_k2_mean=0
			system%pressure_tensor_mean=0
			system%E_pot_mean=0
			system%E_pot=0
		ENDIF



	end subroutine gwild_initialize_output

END MODULE gwild
