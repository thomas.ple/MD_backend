MODULE gwild_types
	USE kinds
	USE classical_md_types
	USE nested_dictionaries
	USE file_handler, only: output
	USE atomic_units
	USE string_operations
  IMPLICIT NONE

	CHARACTER(*), PARAMETER :: GWILD_CAT="WIGNER_AUX"

  TYPE, EXTENDS(CLASS_MD_SYS) :: GWILD_SYS

		! REAL(wp), allocatable :: X_prev(:,:)
		REAL(wp), allocatable :: P_packed(:)
		REAL(wp), allocatable :: P_mod(:,:)

		! REAL(wp) :: EW
		! REAL(wp) :: EW6 !ONLY FOR 1D SYSTEMS

		REAL(wp), ALLOCATABLE :: F_det(:)
		REAL(wp), ALLOCATABLE :: F_dU(:)
		REAL(wp), ALLOCATABLE :: F_phi(:)
		REAL(wp), ALLOCATABLE :: F_cl(:)
		REAL(wp), ALLOCATABLE :: Q(:,:)
		REAL(wp), ALLOCATABLE :: Qinv(:,:)
		REAL(wp), ALLOCATABLE :: sqrtQ(:,:)
		REAL(wp), ALLOCATABLE :: sqrtQinv(:,:)
		! REAL(wp), ALLOCATABLE :: k4(:,:,:,:)

		REAL(wp), ALLOCATABLE :: QEigMat(:,:)
		REAL(wp), ALLOCATABLE :: QinvEigVals(:)

		REAL(wp) :: Q_det, sqrtQ_det, lth2_det,Qinv_det
		
		REAL(wp) :: temp_k2, E_kin_k2, E_kin_k2_mean
		REAL(wp) :: nose, M_nose, damping_nose

		! REAL(wp), ALLOCATABLE :: Qinv_mm(:,:)
	END TYPE

	TYPE, EXTENDS(CLASS_MD_PARAM) :: GWILD_PARAM
		REAL(wp) :: gamma0, gamma0_backup
		CHARACTER(:), ALLOCATABLE :: move_generator
		LOGICAL :: only_auxiliary_simulation
		LOGICAL :: compute_EW

		LOGICAL :: apply_F_det
		LOGICAL :: nose_control
		LOGICAL :: classical_wigner
	END TYPE	

	ABSTRACT INTERFACE
		subroutine gwild_sub(system,parameters)
			import :: GWILD_SYS, GWILD_PARAM
			IMPLICIT NONE
			CLASS(GWILD_SYS), intent(inout) :: system
			CLASS(GWILD_PARAM), intent(inout) :: parameters
		end subroutine gwild_sub

		subroutine gwild_sub_int(system,parameters,n)
			import :: GWILD_SYS, GWILD_PARAM
			IMPLICIT NONE
			CLASS(GWILD_SYS), intent(inout) :: system
			CLASS(GWILD_PARAM), intent(inout) :: parameters
			INTEGER, INTENT(in) :: n
		end subroutine gwild_sub_int
	END INTERFACE

CONTAINS

	subroutine gwild_update_P_from_P_packed(system)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system

		system%P=RESHAPE(system%P_packed,(/system%n_atoms,system%n_dim/))

	end subroutine gwild_update_P_from_P_packed

	subroutine gwild_update_P_packed_from_P(system)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system

		system%P_packed=RESHAPE(system%P,(/system%n_atoms*system%n_dim/))
		
	end subroutine gwild_update_P_packed_from_P

	subroutine gwild_update_P_mod_from_P(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), intent(inout) :: parameters
		INTEGER :: i

		! DO i=1,system%n_dim
		! 	system%P_mod(:,i)=system%P(:,i)*sqrt(parameters%beta/system%mass(:))
		! ENDDO

		system%P_packed=RESHAPE(system%P,(/system%n_atoms*system%n_dim/))
		system%P_mod=RESHAPE(matmul(system%sqrtQ,system%P_packed) &
													,(/system%n_atoms,system%n_dim/))
		DO i=1,system%n_dim
			system%P_mod(:,i)=system%P_mod(:,i)*sqrt(system%mass(:))
		ENDDO
		
	end subroutine gwild_update_P_mod_from_P

	subroutine gwild_update_P_from_P_mod(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), intent(inout) :: parameters
		INTEGER :: i
		REAL(wp), ALLOCATABLE :: sqrtQinv(:,:)

		DO i=1,system%n_dim
			system%P(:,i)=system%P_mod(:,i)/sqrt(system%mass(:))
		ENDDO

		allocate(sqrtQinv(system%n_dof,system%n_dof))
		sqrtQinv=0
		DO i=1,system%n_dof
			sqrtQinv(i,i)=sqrt(system%QinvEigVals(i))
		ENDDO
		sqrtQinv=gwild_matrix_from_eigen(system,sqrtQinv)

		system%P_packed=matmul(sqrtQinv, &
				RESHAPE(system%P,(/system%n_atoms*system%n_dim/)))
		system%P=RESHAPE(system%P_packed,(/system%n_atoms,system%n_dim/))	

		deallocate(sqrtQinv)				
		
	end subroutine gwild_update_P_from_P_mod

	subroutine gwild_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters

		call classical_MD_deallocate(system,parameters)
		!!! TO COMPLETE !!! 

	end subroutine gwild_deallocate

	! subroutine gwild_compute_temp_estimated(system,parameters)
	! 	IMPLICIT NONE
	! 	CLASS(GWILD_SYS), INTENT(inout) :: system
	! 	CLASS(GWILD_PARAM), INTENT(inout) :: parameters

	! 	system%temp_estimated=parameters%temperature*dot_product(system%P_packed &
	! 			,matmul(system%k2,system%P_packed))/real(system%n_dof,wp)
		
	! 	system%temp_natural=(system%P_packed &
	! 			,matmul(system%k2,system%P_packed))/real(system%n_dof,wp)

	! end subroutine gwild_compute_temp_estimated

	subroutine gwild_compute_kinetic_energy(system,parameters)
		IMPLICIT NONE
    CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j,n		

		n=0
		DO j=1,system%n_dim ; do i=1,system%n_atoms
			n=n+1
			system%E_kin(n)=0.5_wp*system%P(i,j)**2
		ENDDO ; ENDDO
		system%E_kin_k2=0
		DO j=1,system%n_dim ; do i=1,system%n_atoms
			system%E_kin_k2=system%E_kin_k2+system%P_mod(i,j)**2/(2._wp*system%mass(i))
		ENDDO ; ENDDO

	end subroutine gwild_compute_kinetic_energy

	subroutine gwild_update_mean_energies(system,parameters,n)
		IMPLICIT NONE
    CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n

		system%E_kin_mean=system%E_kin_mean+(system%E_kin-system%E_kin_mean)/real(n,wp)
		system%E_kin_k2_mean=system%E_kin_k2_mean+(system%E_kin_k2-system%E_kin_k2_mean)/real(n,wp)
		system%temp_estimated = SUM(system%E_kin_mean)*2._wp/system%n_dof
		system%temp_k2 = system%E_kin_k2_mean*2._wp/system%n_dof
		system%E_pot_mean = system%E_pot_mean +(system%E_pot - system%E_pot_mean)/real(n,wp)

	end subroutine gwild_update_mean_energies

	subroutine gwild_print_system_info(system,parameters,i)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: i		

		if(parameters%write_mean_energy_info) then
				call gwild_compute_kinetic_energy(system,parameters)

				call gwild_update_mean_energies(system,parameters &
							,parameters%counter_mean_energy_info)
				parameters%counter_mean_energy_info= &
						parameters%counter_mean_energy_info+1

				if(parameters%print_temperature_stride>0) then
					if(parameters%counter_mean_energy_info &
							>parameters%print_temperature_stride) then
						write(*,*)
						write(*,*) "----------------------------------------------"
						write(*,*) "STEP "//int_to_str(i)
						write(*,*) "SYSTEM INFO (last "//int_to_str(parameters%print_temperature_stride)//" steps):"
						write(*,*) "  temperature_natural: " &
												,system%temp_estimated*kelvin, "K"
						write(*,*) "  temperature_Q: " &
												,system%temp_k2*kelvin, "K"
						write(*,*) "  kinetic_energy_natural: " &
												,SUM(system%E_kin_mean)*kcalpermol, "kCal/mol"
						write(*,*) "  kinetic_energy_Q: " &
												,system%E_kin_k2_mean*kcalpermol, "kCal/mol"
						write(*,*) "  potential_energy: " &
												,system%E_pot_mean*kcalpermol, "kCal/mol"
						write(*,*)

						parameters%counter_mean_energy_info=1
						system%E_kin_k2_mean=0
						system%E_kin_mean=0
						system%temp_k2=0
						system%temp_estimated=0
						system%E_pot_mean=0
					endif				
				endif
			endif
	end subroutine gwild_print_system_info


	subroutine gwild_compute_dipole(system,parameters)
		IMPLICIT NONE
   	CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j

		system%dipole=0._wp
		system%dipole_velocity=0._wp	
		DO j=1,system%n_dim ; DO i=1,system%n_atoms
			system%dipole(j)=system%dipole(j) &
					+system%charges(i)*system%X(i,j)/system%n_atoms

			system%dipole_velocity(j)=system%dipole_velocity(j) &
					+system%charges(i)*system%P_mod(i,j)/(system%mass(i)*system%n_atoms)
		ENDDO ; ENDDO
	
	end subroutine gwild_compute_dipole

	function gwild_matrix_to_eigen(system,mat) result(mat_eig)
		IMPLICIT NONE
   	CLASS(GWILD_SYS), INTENT(in) :: system
		REAL(wp), INTENT(in):: mat(system%n_dof,system%n_dof)
		REAL(wp) :: mat_eig(system%n_dof,system%n_dof)

		mat_eig=matmul(transpose(system%QEigMat), &
                    matmul(mat,system%QEigMat))

	end function gwild_matrix_to_eigen

	function gwild_matrix_from_eigen(system,mat_eig) result(mat)
		IMPLICIT NONE
   	CLASS(GWILD_SYS), INTENT(in) :: system
		REAL(wp), INTENT(in):: mat_eig(system%n_dof,system%n_dof)
		REAL(wp) :: mat(system%n_dof,system%n_dof)

		mat=matmul(system%QEigMat, &
                matmul(mat_eig,transpose(system%QEigMat)))

	end function gwild_matrix_from_eigen

!----------------------------------------------------------
! RESTART FILE

	subroutine gwild_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_initialize_restart_file(system,parameters,param_library)
		
	end subroutine gwild_initialize_restart_file

	subroutine gwild_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(in) :: system
		CLASS(GWILD_PARAM), INTENT(in) :: parameters
		INTEGER, intent(in) :: n_steps
		INTEGER :: u

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			WRITE(u,*) parameters%engine
			WRITE(u,*) system%time
			WRITE(u,*) n_steps
			WRITE(u,*) system%X
			WRITE(u,*) system%P	
			WRITE(u,*) system%P_mod	
		close(u)

		write(*,*) "I/O : save config OK."

	end subroutine gwild_write_restart_file

	subroutine gwild_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u
		CHARACTER(100) :: engine

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			READ(u,*) engine
			if(engine/=parameters%engine) then
				write(0,*) "Error: tried to restart engine '",parameters%engine &
							,"' with a file from '",engine,"' !"
				STOP "Execution stopped"
			endif
			READ(u,*) system%time
			READ(u,*) parameters%n_steps_prev
			READ(u,*) system%X
			READ(u,*) system%P	
			READ(u,*) system%P_mod	
		close(u)

		write(*,*) "I/O : configuration retrieved successfully."

	end subroutine gwild_load_restart_file

!-------------------------------------------------------

	subroutine save_forces_results(system,parameters)
		IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(in) :: system
		CLASS(GWILD_PARAM), INTENT(in) :: parameters
		INTEGER :: u,u2, n_dim, n_dof, n_atoms, i,j

		n_dof=system%n_dof
		n_dim=system%n_dim
		n_atoms=system%n_atoms
		open(newunit=u,file=output%working_directory//"/Q.mat")
		open(newunit=u2,file=output%working_directory//"/Qinv.mat")
		write(*,*)
		write(*,*) "--------------------"
		write(*,*) "--------------------"
		if(n_dof == 1) then
			write(*,*) "Qinv=",system%Qinv,"Q=",system%Q
			write(u,*) system%Q
			write(u2,*) system%Qinv
		else
			DO i=1,n_dof
				write(u,*) REAL(system%Q(i,:))
				write(u2,*) REAL(system%Qinv(i,:))
			ENDDO
			write(*,*) "Q_x="
			DO i=1,n_atoms
				write(*,*) REAL(system%Q(i,1:n_atoms))
			ENDDO
			if(n_dim>1) then
				write(*,*)
				write(*,*) "Q_y="
				DO i=n_atoms+1,2*n_atoms
					write(*,*) REAL(system%Q(i,n_atoms+1:2*n_atoms))
				ENDDO
				if(n_dim>2) then
					write(*,*)
					write(*,*) "Q_z="
					DO i=2*n_atoms+1,3*n_atoms
						write(*,*) REAL(system%Q(i,2*n_atoms+1:3*n_atoms))
					ENDDO
				endif
			endif
		endif
		close(u)
		close(u2)

		open(newunit=u,file=output%working_directory//"/Forces.out")
		write(u,*) "#F_dU    F_phi    F_det    F_cl"
		DO i=1,system%n_dof
			write(u,*) system%F_dU(i), system%F_phi(i), system%F_det(i), system%F_cl(i)
		ENDDO
		close(u)

	!	write(*,*) "sqrt(var(k2))/k2=",system%k2_var!*RESHAPE(system%k2,(/n_dof*n_dof/))
		write(*,*) "--------------------"
		write(*,*) "--------------------"
		write(*,*)
	end subroutine save_forces_results




END MODULE gwild_types