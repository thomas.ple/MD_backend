MODULE gwild_forces
    USE kinds
    USE gwild_types
    USE rpmd_types
    USE nested_dictionaries
    USE string_operations
    USE atomic_units, only : convert_to_unit
    USE basic_types, only : Pot,dPot,get_pot_info
    USE matrix_operations, only : sym_mat_diag,fill_permutations,sym_mv
    USE random, only: RandGaussN
    USE unbiased_products, only: unbiased_product
    USE socket_potential, only :parallel_socket_pot_info
    USE timer_module
    IMPLICIT NONE

    INTEGER, SAVE :: NUM_AUX_SIM=3

    TYPE :: WIG_POLYMER_TYPE
        INTEGER :: n_beads

        REAL(wp), ALLOCATABLE :: X(:,:,:)
        REAL(wp), ALLOCATABLE :: EigV(:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:,:,:)

        REAL(wp), ALLOCATABLE :: F_beads(:,:,:)
        ! REAL(wp), ALLOCATABLE :: F_beads_prev(:,:,:)
        REAL(wp), ALLOCATABLE :: Pot_beads(:)
        ! REAL(wp), ALLOCATABLE :: Pot_beads_prev(:)

        ! REAL(wp), ALLOCATABLE :: Delta(:,:)
        REAL(wp), ALLOCATABLE :: Delta_packed(:)
        REAL(wp), ALLOCATABLE :: k1(:)
        REAL(wp), ALLOCATABLE :: k2_mm(:,:)

        ! REAL(wp), ALLOCATABLE :: X_prev(:,:,:)
        ! REAL(wp), ALLOCATABLE :: P_prev(:,:,:)
        ! REAL(wp), ALLOCATABLE :: Delta_prev(:,:)
        REAL(wp) :: MC_ener
        REAL(wp) :: MC_acc_ratio
        INTEGER :: MC_count_moves

        REAL(wp), ALLOCATABLE :: k2(:,:)
        ! REAL(wp), ALLOCATABLE :: k2_reweight(:,:)
		! REAL(wp), ALLOCATABLE :: k2inv(:,:)
		! REAL(wp), ALLOCATABLE :: k4(:,:,:,:)
		REAL(wp), ALLOCATABLE :: F(:)
		REAL(wp), ALLOCATABLE :: G(:,:,:)
        ! REAL(wp), ALLOCATABLE :: F_reweight(:)
		! REAL(wp), ALLOCATABLE :: G_reweight(:,:,:)

        !ONLY FOR 1D SYSTEMS
        REAL(wp) :: k6 
        REAL(wp) :: G4

        CHARACTER(:), ALLOCATABLE :: prev_config_filename
        INTEGER :: prev_config_unit,i_step_reweight
        REAL(wp) :: mean_weight_ratio

    END TYPE

    TYPE, EXTENDS(RPMD_PARAM) :: WIG_FORCE_PARAM
        REAL(wp), ALLOCATABLE :: mOmK(:,:)

        !PIOUD PARAMETERS
        ! REAL(wp), ALLOCATABLE :: mu(:,:,:)
        REAL(wp), ALLOCATABLE :: OUsig(:,:,:,:)
        REAL(wp), ALLOCATABLE :: expOmk(:,:,:,:)

        ! BAOAB PARAMETERS
        REAL(wp), ALLOCATABLE :: gammaExp(:,:)
        REAL(wp), ALLOCATABLE :: sigmaBAOAB(:,:)
        LOGICAL :: BAOAB_num

        REAL(wp), ALLOCATABLE :: F_cl(:)

        REAL(wp), ALLOCATABLE :: sigp2_inv(:)

        ! REAL(wp), ALLOCATABLE :: muX(:,:,:),muP(:,:,:)

        CHARACTER(:), ALLOCATABLE :: move_generator
        INTEGER :: n_samples

        REAL(wp), ALLOCATABLE :: sqrt_mass(:)
        LOGICAL :: spring_force
        LOGICAL :: compute_EW

        REAL(wp) :: delta_mass_factor, sqrt_delta_mass_factor

        LOGICAL :: verbose

        LOGICAL :: k2_smooth, G_smooth
        REAL(wp) :: k2_smooth_center
        REAL(wp) :: k2_smooth_width
        REAL(wp) , ALLOCATABLE :: k2_smooth_mat(:,:)

        REAL(wp), ALLOCATABLE :: memory_F(:)
		REAL(wp), ALLOCATABLE :: memory_C(:,:,:)
		REAL(wp), ALLOCATABLE :: memory_k2(:,:)
        REAL(wp), ALLOCATABLE :: memory_sqrtk2inv(:,:)
        REAL(wp) :: memory_sum_weight
        REAL(wp) :: memory_tau
        LOGICAL :: memory_smoothing

        LOGICAL :: MC_acceptance_test
        LOGICAL :: reweight_previous_configs
        INTEGER :: n_steps_reweight

        LOGICAL :: moment_matching
    END TYPE

    TYPE WORK_VARIABLES
        REAL(wp), ALLOCATABLE :: F(:)
        REAL(wp), ALLOCATABLE :: Delta2(:,:)
        REAL(wp), ALLOCATABLE :: Delta4(:,:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:),EigV(:)
        REAL(wp), ALLOCATABLE :: R(:), R1(:), R2(:)
        REAL(wp), ALLOCATABLE :: k2var(:,:),Fvar(:),Gvar(:,:,:)
    END TYPE WORK_VARIABLES

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), ALLOCATABLE, SAVE :: polymers
    TYPE(WIG_FORCE_PARAM), SAVE :: f_param
    TYPE(WORK_VARIABLES), DIMENSION(:), ALLOCATABLE, SAVE :: WORKS

    ABSTRACT INTERFACE
		subroutine move_proposal_type(system,polymer,move_accepted,WORK)
            IMPORT :: GWILD_SYS,WIG_POLYMER_TYPE,WORK_VARIABLES
			IMPLICIT NONE
            TYPE(GWILD_SYS), INTENT(IN) :: system
			TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
			LOGICAL, INTENT(OUT) :: move_accepted
            CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		end subroutine move_proposal_type

        subroutine update_beads_forces_type(q,polymer)
            IMPORT :: wp,WIG_POLYMER_TYPE
            implicit none
            TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
            REAL(wp), INTENT(in) :: q(:,:)
        end subroutine update_beads_forces_type
	END INTERFACE

    PROCEDURE(move_proposal_type), POINTER :: move_proposal => NULL()
    PROCEDURE(update_beads_forces_type), POINTER :: update_beads_forces => NULL()

    PRIVATE
    PUBLIC :: compute_gwild_forces, initialize_auxiliary_simulation! &
                ! ,compute_EW

CONTAINS

!--------------------------------------------------------------
! FORCE CALCULATIONS

    subroutine compute_gwild_forces(system,parameters)
        !$ USE OMP_LIB
        IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
        INTEGER :: i,j,k,INFO,i_thread,c,u,counter(f_param%n_threads)
        TYPE(timer_type) :: timer_full, timer_elem

        if(ANY(ISNAN(system%X))) STOP "ERROR: system diverged!"

        ! GET CLASSICAL FORCE     
        call get_pot_info(system%X,system%E_pot,system%Forces)
        system%F_cl=RESHAPE(system%Forces,(/ system%n_dof /))


        i_thread=1
        if(f_param%verbose) then
            write(*,*)
            write(*,*) "Starting g-WiLD forces computation..."
            !$ write(*,*) "(parallel computation)"
            call timer_full%start()
        endif

        counter=0
        !$OMP PARALLEL DO LASTPRIVATE(i_thread)
        DO i=1,NUM_AUX_SIM
            !$ i_thread = OMP_GET_THREAD_NUM()+1
            if(parameters%only_auxiliary_simulation) then
                counter(i_thread)=counter(i_thread)+1
                write(*,*) "  proc "//int_to_str(i_thread)//" run "//int_to_str(counter(i_thread))
            endif
            ! if(f_param%reweight_previous_configs) &
            !     call reweight_previous_configs(system,parameters,polymers(i),WORKS(i_thread))            
            call sample_forces(system,parameters,polymers(i),WORKS(i_thread))
            ! if(f_param%moment_matching) call moment_matching(system,parameters,polymers(i),WORKS(i_thread))
        ENDDO
        !$OMP END PARALLEL DO

        ! if(f_param%MC_acceptance_test) then
        !    ! system%acc_ratio = SUM(polymers(:)%MC_acc_ratio/REAL(NUM_AUX_SIM,wp))
        !     if(f_param%verbose) then
        !         write(*,*) "    mean acceptance ratio:", system%acc_ratio 
        !     endif
        ! endif

        ! COMPUTE FORCE ELEMENTS
        if(f_param%verbose) then
            write(*,*) "    Starting force elements calculation..."
            call timer_elem%start()
        endif
        CALL compute_forces_elements(system,parameters)         
        if(f_param%verbose) then
            write(*,*) "    force elements computed in",timer_elem%elapsed_time(),"s."
        endif  

        ! if(f_param%moment_matching) then
        !     system%Qinv_mm=0
        !     DO i=1,NUM_AUX_SIM
        !         system%Qinv_mm = system%Qinv_mm + polymers(i)%k2_mm/REAL(NUM_AUX_SIM,wp)
        !     ENDDO
        !     c=0
        !     DO j=1,system%n_dim; do i=1,system%n_atoms
        !         c=c+1
        !         DO k=1,system%n_dim
        !             system%Qinv_mm((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
        !                 =system%Qinv_mm((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
        !                     *sqrt(system%mass(i)*system%mass(:))/f_param%beta
        !         ENDDO
        !     ENDDO ; ENDDO
        ! else
        !     system%Qinv_mm = system%Qinv
        ! endif    
        
        if(f_param%verbose) then
            write(*,*) "g-WiLD forces computed in",timer_full%elapsed_time(),"s."
        endif    

        if(parameters%only_auxiliary_simulation) then
             c=0
            DO j=1,system%n_dim; do i=1,system%n_atoms
                c=c+1
                polymers(1)%F(c)=polymers(1)%F(c)*sqrt(system%mass(i))
                polymers(1)%G(:,:,c)=polymers(1)%G(:,:,c)*sqrt(system%mass(i))
            ENDDO ; ENDDO

            open(newunit=u,file=output%working_directory//"/k2.mat")
            write(u,*) "#mean value over "//int_to_str(NUM_AUX_SIM)//" runs"
            DO i=1,system%n_dof
                write(u,*) polymers(1)%k2(i,:)
            ENDDO
            close(u)
            open(newunit=u,file=output%working_directory//"/k2.mat.var")
            write(u,*) "#estimated variance of the mean over "//int_to_str(NUM_AUX_SIM)//" runs"
            DO i=1,system%n_dof
                write(u,*) WORKS(1)%k2var(i,:)/REAL(NUM_AUX_SIM,wp)
            ENDDO
            close(u)
            open(newunit=u,file=output%working_directory//"/F.vec")
            write(u,*) "#mean value over "//int_to_str(NUM_AUX_SIM)//" runs"
            DO i=1,system%n_dof
                write(u,*) polymers(1)%F(i)
            ENDDO
            close(u)
            open(newunit=u,file=output%working_directory//"/F.vec.var")
            write(u,*) "#estimated variance of the mean over "//int_to_str(NUM_AUX_SIM)//" runs"
            DO i=1,system%n_dof
                write(u,*) WORKS(1)%Fvar(i)/REAL(NUM_AUX_SIM,wp)
            ENDDO
            close(u)
            open(newunit=u,file=output%working_directory//"/G.tens")
            write(u,*) "#mean value over "//int_to_str(NUM_AUX_SIM)//" runs"
            DO i=1,system%n_dof
                DO j=1,system%n_dof
                    write(u,*) polymers(1)%G(j,:,i)
                ENDDO
                write(u,*)
            ENDDO
            close(u)
            open(newunit=u,file=output%working_directory//"/G.tens.var")
            write(u,*) "#estimated variance of the mean over "//int_to_str(NUM_AUX_SIM)//" runs"
            DO i=1,system%n_dof
                DO j=1,system%n_dof
                    write(u,*) WORKS(1)%Gvar(j,:,i)/REAL(NUM_AUX_SIM,wp)
                ENDDO
                write(u,*)
            ENDDO
            close(u)
        endif

    end subroutine compute_gwild_forces

    subroutine compute_forces_elements(system,parameters)
        IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: tmp,k2k2F,F(NUM_AUX_SIM),k2(NUM_AUX_SIM),G2(NUM_AUX_SIM),k4(NUM_AUX_SIM)
        INTEGER :: i,j,k,l,p1,p2,p3,c,is,INFO,maxlocdiff(2),ndof
        REAL(wp),ALLOCATABLE :: dQinv(:,:,:),dsqrtQ(:,:,:),sqrtEig(:),sqrtQinv(:,:)
        REAL(wp) :: Ns,tmpscal,mean_weight_ratio,weight_sum
        REAL(wp) :: k2mean,Fmean,Gmean

        ! COMPUTE MEAN Q
        ! system%Qinv=0
        ! DO i=1,NUM_AUX_SIM
        !     system%Qinv=system%Qinv+polymers(i)%k2/REAL(NUM_AUX_SIM, wp)
        ! ENDDO

        if(parameters%only_auxiliary_simulation) then
            ndof=system%n_dof
            if(.not.allocated(WORKS(1)%k2var)) then
                allocate(WORKS(1)%k2var(ndof,ndof) &
                        ,WORKS(1)%Fvar(ndof) &
                        ,WORKS(1)%Gvar(ndof,ndof,ndof) &
                )
            endif
            WORKS(1)%k2var=0
            WORKS(1)%Fvar=0
            WORKS(1)%Gvar=0
            DO j=1,ndof ; DO i=1,ndof
                k2mean=0
                DO k=1,NUM_AUX_SIM
                    k2mean=k2mean+polymers(k)%k2(i,j)
                ENDDO
                k2mean=k2mean/REAL(NUM_AUX_SIM,wp)
                DO k=1,NUM_AUX_SIM
                    WORKS(1)%k2var(i,j)=WORKS(1)%k2var(i,j)+(polymers(k)%k2(i,j)-k2mean)**2
                ENDDO
                WORKS(1)%k2var(i,j)=WORKS(1)%k2var(i,j)/REAL(NUM_AUX_SIM-1,wp)
            ENDDO ; ENDDO
            DO i=1,ndof
                Fmean=0
                DO k=1,NUM_AUX_SIM
                    Fmean=Fmean+polymers(k)%F(i)
                ENDDO
                Fmean=Fmean/REAL(NUM_AUX_SIM,wp)
                DO k=1,NUM_AUX_SIM
                    WORKS(1)%Fvar(i)=WORKS(1)%Fvar(i)+(polymers(k)%F(i)-Fmean)**2
                ENDDO
                WORKS(1)%Fvar(i)=WORKS(1)%Fvar(i)/REAL(NUM_AUX_SIM-1,wp)
            ENDDO
            DO l=1,ndof ; DO j=1,ndof ; DO i=1,ndof
                Gmean=0
                DO k=1,NUM_AUX_SIM
                    Gmean=Gmean+polymers(k)%G(i,j,l)
                ENDDO
                Gmean=Gmean/REAL(NUM_AUX_SIM,wp)
                DO k=1,NUM_AUX_SIM
                    WORKS(1)%Gvar(i,j,l)=WORKS(1)%Gvar(i,j,l)+(polymers(k)%G(i,j,l)-Gmean)**2
                ENDDO
                WORKS(1)%Gvar(i,j,l)=WORKS(1)%Gvar(i,j,l)/REAL(NUM_AUX_SIM-1,wp)
            ENDDO ; ENDDO ; ENDDO
        endif
        polymers(1)%k2=polymers(1)%k2/REAL(NUM_AUX_SIM, wp)
        polymers(1)%F=polymers(1)%F/REAL(NUM_AUX_SIM,wp)
        polymers(1)%G=polymers(1)%G/REAL(NUM_AUX_SIM,wp)
        DO i=2,NUM_AUX_SIM
            polymers(1)%k2=polymers(1)%k2+polymers(i)%k2/REAL(NUM_AUX_SIM,wp)
            polymers(1)%F=polymers(1)%F+polymers(i)%F/REAL(NUM_AUX_SIM,wp)
            polymers(1)%G=polymers(1)%G+polymers(i)%G/REAL(NUM_AUX_SIM,wp)
        ENDDO

        !transform k2 into Q
        c=0
        DO j=1,system%n_dim; do i=1,system%n_atoms
            c=c+1
            DO k=1,system%n_dim
                system%Qinv((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
                    =polymers(1)%k2((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
                        *sqrt(system%mass(i)*system%mass(:))/f_param%beta
            ENDDO
        ENDDO ; ENDDO
        ! same transformation for G
        DO l=1,system%n_dof
            c=0
            DO j=1,system%n_dim; do i=1,system%n_atoms
                c=c+1
                DO k=1,system%n_dim                
                    polymers(1)%G((k-1)*system%n_atoms+1:k*system%n_atoms,c,l) &
                        =polymers(1)%G((k-1)*system%n_atoms+1:k*system%n_atoms,c,l) &
                            *sqrt(system%mass(i)*system%mass(:))/f_param%beta
                ENDDO
            ENDDO ; ENDDO
        ENDDO
        ! NORMALIZE F AND G by sqrt(mass)
        c=0
        DO j=1,system%n_dim; do i=1,system%n_atoms
            c=c+1
            polymers(1)%F(c)=polymers(1)%F(c)/sqrt(system%mass(i))
            polymers(1)%G(:,:,c)=polymers(1)%G(:,:,c)/sqrt(system%mass(i))
        ENDDO ; ENDDO


        ! if(f_param%compute_EW) then
        !     ! COMPUTE MEAN k4 AND k6
        !     system%k4=0
        !     system%k6=0
        !     DO i=1,NUM_AUX_SIM
        !         system%k6=system%k6+polymers(i)%k6
        !         system%k4=system%k4+polymers(i)%k4
        !     ENDDO
        !     system%k6=system%k6/REAL(NUM_AUX_SIM, wp)
        !     system%k4=system%k4/REAL(NUM_AUX_SIM, wp)
        ! endif

        allocate(dQinv(system%n_dof,system%n_dof,system%n_dof) &
                ,dsqrtQ(system%n_dof,system%n_dof,system%n_dof) &
                ,sqrtEig(system%n_dof) &
                ,sqrtQinv(system%n_dof,system%n_dof))


        call sym_mat_diag(system%Qinv,system%QinvEigVals,system%QEigMat,INFO)
        IF(INFO/=0) STOP "Error: could not diagonalize Q^-1(q) !"
        system%sqrtQ=0
        sqrtQinv=0
        system%Q=0
        system%Qinv_det=1._wp
        sqrtEig=sqrt(system%QinvEigVals)
        DO i=1,system%n_dof
            sqrtQinv(i,i)=sqrtEig(i)
            system%sqrtQ(i,i)=1._wp/sqrtEig(i)
            system%Q=1._wp/system%QinvEigVals(i)
            system%Qinv_det=system%Qinv_det*system%QinvEigVals(i)
        ENDDO
        system%sqrtQ_det=1._wp/sqrt(system%Qinv_det)
        system%Q_det=1._wp/system%Qinv_det
        sqrtQinv = gwild_matrix_from_eigen(system,sqrtQinv) 
        system%sqrtQ = gwild_matrix_from_eigen(system,system%sqrtQ)
        system%Q = gwild_matrix_from_eigen(system,system%Q)

        system%F_dU=polymers(1)%F
        !COMPUTE d(Q^-1)
        DO k=1,system%n_dof
            dQinv(:,:,k)=polymers(1)%G(:,:,k)-system%Qinv(:,:)*polymers(1)%F(k)
        ENDDO
        !COMPUTE GEOMETRIC CONTRIBUTION (prop to d(detQ))
        system%F_det=0
        DO j=1,system%n_dof
            DO l=1,system%n_dof ; DO k=1,system%n_dof 
                system%F_det(j)=system%F_det(j)+system%Q(k,l)*dQinv(k,l,j)
            ENDDO ; ENDDO
            system%F_det(j)=-0.5_wp*system%F_det(j)
        ENDDO
        if(.not. parameters%classical_wigner) then
            system%F_det=matmul(system%sqrtQ,system%F_det)

            !COMPUTE d(Q^(1/2)) contribution
            DO k=1,system%n_dof
                dsqrtQ(:,:,k)=gwild_matrix_to_eigen(system,dQinv(:,:,k))
                DO j=1,system%n_dof ; DO i=1,system%n_dof
                    dsqrtQ(i,j,k)=-dsqrtQ(i,j,k) &
                        /((sqrtEig(i)+sqrtEig(j))*sqrtEig(i)*sqrtEig(j))
                ENDDO ; ENDDO 
                dsqrtQ(:,:,k) = gwild_matrix_from_eigen(system,dsqrtQ(:,:,k))      
            ENDDO
            system%F_phi=0
            DO j=1,system%n_dof ; DO i=1,system%n_dof
                system%F_phi(i)=system%F_phi(i)+dsqrtQ(i,j,j)
            ENDDO ; ENDDO

            !COMPUTE dU contribution
            system%F_dU = matmul(system%sqrtQ,system%F_dU)
        endif

        deallocate(dsqrtQ,dQinv)

        c=0
        DO j=1,system%n_dim; do i=1,system%n_atoms
            c=c+1
            system%F_cl(c)=system%F_cl(c)/sqrt(system%mass(i))
        ENDDO ; ENDDO
        !change of variable on the classical force (not the exact g-WiLD force)
        if(parameters%classical_wigner) then
            system%F_cl = matmul(system%Qinv,system%F_cl)
        else
            system%F_cl = matmul(sqrtQinv,system%F_cl)   
        endif              

        ! V=0
        ! DO l=1,system%n_dof; DO k=1,system%n_dof
        !     DO j=1,system%n_dof                
        !         V(j)=V(j)+polymers(1)%G(j,k,l)*system%k2inv(k,l)          
        !     ENDDO
        ! ENDDO ; ENDDO

        ! system%F_change_var = matmul(system%sqrtk2inv,2*system%F_change_var-V)
            

        !USE TRUE g-WiLD FORCE         
        WORKS(1)%F=system%F_dU
        if(parameters%apply_F_det) WORKS(1)%F=WORKS(1)%F+system%F_det
        if(.not. parameters%classical_wigner) WORKS(1)%F=WORKS(1)%F+system%F_phi
        system%Forces = RESHAPE(WORKS(1)%F,(/system%n_atoms,system%n_dim/))
        ! if(parameters%apply_F_det) then
        !     system%Forces = RESHAPE(system%F_dU &
        !                             + system%F_phi &
        !                             + system%F_det &
        !                     ,(/system%n_atoms,system%n_dim/) )
        ! else
        !     system%Forces = RESHAPE(system%F_dU &
        !                             + system%F_phi &
        !                     ,(/system%n_atoms,system%n_dim/) )
        ! endif

    end subroutine compute_forces_elements

!--------------------------------------------------------------
! SAMPLING LOOP
    subroutine sample_forces(system,parameters,polymer,WORK)
        IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: m,n_dof,ios
        INTEGER :: i,j,k,l
        LOGICAL :: move_accepted

        call reinitialize_polymer_config(system,polymer,WORK)

        !THERMALIZATION
        DO m=1,f_param%n_steps_therm
            CALL move_proposal(system,polymer,move_accepted,WORK)
        ENDDO

        ! if(f_param%MC_acceptance_test) &
        !     call compute_observables(system,polymer,WORK)

        !PRODUCTION
        DO m=1,f_param%n_steps

            !GENERATE A NEW CONFIG
            CALL move_proposal(system,polymer,move_accepted,WORK)
            ! if(move_accepted) then
                !COMPUTE OBSERVABLES
                call compute_observables(system,polymer,WORK)
            ! endif

            !UPDATE MEAN VALUES
            CALL update_mean_values(polymer,m,WORK)

        ENDDO

        n_dof=system%n_dof

        !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
        DO j=2,n_dof ; DO i=1,j-1
           polymer%k2(i,j)=polymer%k2(j,i)
        ENDDO ; ENDDO
        
        !symmetrize G
		do i=1,n_dof-1 ; do j=i+1,n_dof
			do k=1,n_dof
				polymer%G(i,j,k)=polymer%G(j,i,k)
			enddo
		enddo ; enddo
		
		!symmetrize k4
        ! if(f_param%compute_EW) then
        !     do l=1,n_dof ; do k=l,n_dof ; do j=k,n_dof ; do i=j,n_dof		
        !         call fill_permutations(polymer%k4,i,j,k,l)
        !     enddo ; enddo ; enddo ; enddo
        ! endif

        ! if(f_param%reweight_previous_configs .OR. f_param%moment_matching) then
        !     close(polymer%prev_config_unit,iostat=ios)
        ! endif

    end subroutine sample_forces

!-------------------------------------------------
! UTILITY SUBROUTINES

    subroutine compute_observables(system,polymer,WORK)
        IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j,k,l
        REAL(wp) :: config_weight
        
        IF(f_param%spring_force) THEN
            DO i=1,system%n_dim
                WORK%F(system%n_atoms*(i-1)+1:system%n_atoms*i) = &
                    f_param%sigp2_inv(:)/f_param%beta * ( &
                        polymer%X(1,:,i)+polymer%X(f_param%n_beads-1,:,i) &
                    )
            ENDDO
        ELSE
            WORK%F=RESHAPE( &
                    SUM(polymer%F_beads(1:f_param%n_beads-1,:,:)/real(f_param%n_beads,wp),dim=1) &
                ,(/system%n_dof/) )
        ENDIF


        ! if(f_param%reweight_previous_configs .OR. f_param%moment_matching) then
        !     config_weight=compute_config_weight(system%X,polymer%Pot_beads(0) &        
        !         ,polymer%Pot_beads(f_param%n_beads),polymer%X(f_param%n_beads,:,:) &
        !         ,system%X+polymer%X(1,:,:),system%X+polymer%X(f_param%n_beads-1,:,:))

        !     WRITE(polymer%prev_config_unit) config_weight,polymer%Pot_beads(0) &
        !             ,polymer%Pot_beads(f_param%n_beads) &
        !             ,polymer%X(f_param%n_beads,:,:),system%X+polymer%X(1,:,:) &
        !             ,system%X+polymer%X(f_param%n_beads-1,:,:)!,WORK%F
        ! endif

        WORK%F=WORK%F+ RESHAPE( ( polymer%F_beads(0,:,:)+ polymer%F_beads(f_param%n_beads,:,:)&
                            )/real(f_param%n_beads,wp) ,(/system%n_dof/) )

        polymer%Delta_packed=RESHAPE(2._wp*polymer%X(f_param%n_beads,:,:),(/system%n_dof/))

        !COMPUTE ONLY THE LOWER TRIANGULAR PART
		do j=1,system%n_dof ; do i=j,system%n_dof
			WORK%Delta2(i,j)=polymer%Delta_packed(i)*polymer%Delta_packed(j)
		enddo ; enddo

        ! if(f_param%compute_EW) then
        !     do l=1,system%n_dof ; do k=l,system%n_dof ;	do j=k,system%n_dof ; do i=j,system%n_dof
        !         WORK%Delta4(i,j,k,l)=polymer%Delta_packed(i)*polymer%Delta_packed(j) &
        !                         *polymer%Delta_packed(k)*polymer%Delta_packed(l)
        !     enddo ; enddo ; enddo ; enddo
        ! endif

    end subroutine compute_observables

    subroutine update_mean_values(polymer,count,WORK)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER, INTENT(in) :: count
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: k
        
        ! if(f_param%compute_EW) then
        !     polymer%k6=polymer%k6 + (polymer%Delta(1,1)**6-polymer%k6)/count
        !     polymer%k4=polymer%k4 + (WORK%Delta4-polymer%k4)/count	
        !     polymer%G4=polymer%G4 + (WORK%F(1)*WORK%Delta4(1,1,1,1)-polymer%G4)/count	
        ! endif	

        polymer%k2=polymer%k2 + (WORK%Delta2-polymer%k2)/count
		polymer%F=polymer%F + (WORK%F-polymer%F)/count        
		DO k=1,f_param%n_dof
			polymer%G(:,:,k)=polymer%G(:,:,k) &
                +(WORK%Delta2*WORK%F(k)-polymer%G(:,:,k))/count
		ENDDO
        ! polymer%k1 = polymer%k1 + (polymer%Delta_packed - polymer%k1)/count

    end subroutine update_mean_values
!--------------------------------------------------------------
! MOVE GENERATORS
    
    !-----------------------------------
    ! PIOUD steps
    subroutine PIOUD_step(system,polymer,move_accepted,WORK)
		IMPLICIT NONE
        TYPE(GWILD_SYS), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, INTENT(out) :: move_accepted
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		integer :: i,j,nu,k
        REAL(wp) :: EigX0(f_param%n_beads), EigV0(f_param%n_beads)

		nu=f_param%n_beads

        !CALL apply_forces(polymer,0.5_wp*f_param%dt)

        do j=1,system%n_dim ; do i=1,system%n_atoms

            !SAVE INITIAL VALUE TEMPORARILY
            EigV0=polymer%EigV(:,i,j)
            EigX0=polymer%EigX(:,i,j)

            !GENERATE RANDOM VECTORS
            CALL RandGaussN(WORK%R1)
            CALL RandGaussN(WORK%R2)

            !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
            polymer%EigV(:,i,j)=EigV0(:)*f_param%expOmk(:,1,1,i) &
                +EigX0(:)*f_param%expOmk(:,1,2,i) &
                +f_param%OUsig(:,1,1,i)*WORK%R1(:)

            polymer%EigX(1:nu,i,j)=EigV0(:)*f_param%expOmk(:,2,1,i) &
                +EigX0(:)*f_param%expOmk(:,2,2,i) &
                +f_param%OUsig(:,2,1,i)*WORK%R1(:)+f_param%OUsig(:,2,2,i)*WORK%R2(:)
            
            
            !TRANSFORM BACK IN COORDINATES
            ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(i)
            call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
            polymer%X(:,i,j)=polymer%X(:,i,j)/f_param%sqrt_mass(i)
        enddo ; enddo

        !APPLY FORCES	
        CALL update_beads_forces(system%X,polymer)        	
        CALL apply_forces(polymer,f_param%dt)

        move_accepted=.TRUE.

	end subroutine PIOUD_step

    !-----------------------------------
    ! BAOAB steps
    subroutine BAOAB_step(system,polymer,move_accepted,WORK)
		IMPLICIT NONE
        TYPE(GWILD_SYS), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, INTENT(out) :: move_accepted
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		integer :: i,j,nu,k
        REAL(wp) :: EigX0(f_param%n_beads), EigV0(f_param%n_beads)

		nu=f_param%n_beads

        !CALL apply_forces(polymer,0.5_wp*f_param%dt)

        do j=1,system%n_dim ; do i=1,system%n_atoms

            !SAVE INITIAL VALUE TEMPORARILY
            

            !HARMONIC PROPAGATION (BLOC A, dt/2)
            if(f_param%BAOAB_num) then
                polymer%EigX(:,i,j)=polymer%EigX(:,i,j)+polymer%EigV(:,i,j)*0.5*f_param%dt
            else
                EigV0=polymer%EigV(:,i,j)
                EigX0=polymer%EigX(:,i,j)

                polymer%EigX(:,i,j)=EigX0(:)*f_param%expOmk(:,1,1,i) &
                    +EigV0(:)*f_param%expOmk(:,1,2,i)
                polymer%EigV(:,i,j)=EigV0(:)*f_param%expOmk(:,2,2,i) &
                    +EigX0(:)*f_param%expOmk(:,2,1,i)
            endif

            ! OU PROPAGATION (BLOC O, dt)
            !GENERATE RANDOM VECTORS
            CALL RandGaussN(WORK%R1)
            !CALL RandGaussN(WORK%R2)
            
            EigV0(:)=polymer%EigV(:,i,j)*f_param%gammaExp(:,i) &
				        + f_param%sigmaBAOAB(:,i)*WORK%R1(:)


            !HARMONIC PROPAGATION (BLOC A, dt/2)
            if(f_param%BAOAB_num) then
                polymer%EigX(:,i,j)=polymer%EigX(:,i,j)+EigV0(:)*0.5*f_param%dt
                polymer%EigV(:,i,j)=EigV0(:)
            else
                EigX0(:)=polymer%EigX(:,i,j)

                polymer%EigX(:,i,j)=EigX0(:)*f_param%expOmk(:,1,1,i) &
                    +EigV0(:)*f_param%expOmk(:,1,2,i)
                polymer%EigV(:,i,j)=EigV0(:)*f_param%expOmk(:,2,2,i) &
                    +EigX0(:)*f_param%expOmk(:,2,1,i)
            endif

            !TRANSFORM BACK IN COORDINATES
            ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(j)
            call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
            polymer%X(:,i,j)=polymer%X(:,i,j)/f_param%sqrt_mass(i)
        enddo ; enddo

        !APPLY FORCES (BLOC B, dt)	
        CALL update_beads_forces(system%X,polymer)        	
        CALL apply_forces(polymer,f_param%dt)

        if(f_param%BAOAB_num) then
            do j=1,system%n_dim ; do i=1,system%n_atoms
                polymer%EigV(:,i,j)=polymer%EigV(:,i,j) &
                    -f_param%dt*polymer%EigX(:,i,j)*f_param%mOmK(:,i)**2
            enddo ; enddo
        endif

        move_accepted=.TRUE.

	end subroutine BAOAB_step

	subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i,j,nu
        REAL(wp) :: EigF(f_param%n_beads),F(f_param%n_beads)
       ! REAL(wp) :: Dnu(f_param%n_atoms,f_param%n_dim),Fdelta(f_param%n_atoms,f_param%n_dim)
       ! REAL(wp) :: sig

        nu=f_param%n_beads
        do j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
            !COMPUTE FORCE FOR NORMAL MODES
            F(1:nu-1)=polymer%F_beads(1:nu-1,i,j)/f_param%sqrt_mass(i)
            F(nu)=(polymer%F_beads(0,i,j)-polymer%F_beads(nu,i,j))/f_param%sqrt_mass(i)
            call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,F,1,0._8,EigF,1)

            polymer%EigV(:,i,j)=polymer%EigV(:,i,j)+tau*EigF(:)
        ENDDO ; ENDDO

	end subroutine apply_forces

    function compute_config_weight(q,pot0,potnu,delta,x1,xnu) result(Ener)
        implicit none
        REAL(wp), INTENT(in) :: q(:,:),delta(:,:),x1(:,:),xnu(:,:),pot0,potnu
        REAL(wp) :: Ener,sig
        INTEGER :: i,j
    
        Ener=f_param%dBeta*(pot0+potnu)
        DO j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
            sig=0.5_wp*f_param%sigp2_inv(i)
            Ener=Ener+sig*((q(i,j)-delta(i,j)-xnu(i,j))**2 &
                            +(q(i,j)+delta(i,j)-x1(i,j))**2)            
        ENDDO ; ENDDO

    end function compute_config_weight

!--------------------------------------------------------------
! VARIOUS INITIALIZATIONS

    subroutine reinitialize_polymer_config(system,polymer,WORK)
        IMPLICIT NONE
		CLASS(GWILD_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j

        polymer%k2=0
        ! if(f_param%compute_EW) then
        !     polymer%k4=0
        !     polymer%k6=0
        !     polymer%G4=0
        ! endif
        polymer%F=0
        polymer%G=0  
        ! polymer%k1=0      
 
        ! polymer%X_prev=polymer%X
        ! polymer%P_prev=polymer%P
        ! polymer%Delta_prev=polymer%Delta
       ! CALL update_beads_forces(system%X,polymer)
        ! polymer%F_beads_prev=polymer%F_beads
        ! polymer%Pot_beads_prev=polymer%Pot_beads

        ! if(f_param%reweight_previous_configs .OR. f_param%moment_matching) then
        !     polymer%i_step_reweight = polymer%i_step_reweight +1
        !     if(polymer%i_step_reweight>f_param%n_steps_reweight) polymer%i_step_reweight=1
        !     open(newunit=polymer%prev_config_unit &
        !             ,file=polymer%prev_config_filename//"_"//int_to_str(polymer%i_step_reweight) &
        !             ,form="unformatted")
        ! endif

       ! CALL apply_forces(polymer,0.5_wp*f_param%dt)

    end subroutine reinitialize_polymer_config

    subroutine initialize_auxiliary_simulation(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
        TYPE(DICT_STRUCT), pointer :: wig_lib
        INTEGER :: n_dof,n_beads,n_atoms,n_dim
        INTEGER :: i,j,k,n_therm_tmp,n_steps_init
        CHARACTER(:), ALLOCATABLE :: pot_name

        n_dof=system%n_dof
        f_param%n_dof=n_dof
        n_atoms=system%n_atoms
        f_param%n_atoms=n_atoms
        n_dim=system%n_dim
        f_param%n_dim=n_dim

        f_param%verbose = param_library%get(GWILD_CAT//"/verbose",default=.FALSE.)
        !! @input_file WIGNER_AUX/verbose (gwild, default=.FALSE.)

        NUM_AUX_SIM=param_library%get(GWILD_CAT//"/num_aux_sim",default=3)
        !! @input_file WIGNER_AUX/num_aux_sim (gwild, default=3)

        ! AT LEAST 3 AUXILIARY SIMULATIONS FOR DYNAMICS
        ! IF((.not. parameters%only_auxiliary_simulation) .and. NUM_AUX_SIM<3) &
        !     STOP "Error : 'num_aux_sim' must be >=3 for dynamics"
        allocate(polymers(NUM_AUX_SIM))

        wig_lib => param_library%get_child(GWILD_CAT)
        !GET NUMBER OF BEADS
        n_beads=wig_lib%get("n_beads")
        !! @input_file WIGNER_AUX/n_beads (gwild)
		if(n_beads<=0) STOP "Error: 'n_beads' of '"//GWILD_CAT//"' is not properly defined!"
			polymers(:)%n_beads=n_beads
			f_param%n_beads=n_beads
        
        !GET NUMBER OF AUXILIARY SIMULATION STEPS
        f_param%n_steps=wig_lib%get("n_steps")
        !! @input_file WIGNER_AUX/n_steps (gwild)
        if(f_param%n_steps<=0) &
            STOP "Error: 'n_steps' of '"//GWILD_CAT//"' is not properly defined!"

        f_param%n_steps_therm=wig_lib%get("n_steps_therm")
        !! @input_file WIGNER_AUX/n_steps_therm (gwild)
        if(f_param%n_steps_therm<0) &
            STOP "Error: 'n_steps_therm' of '"//GWILD_CAT//"' is not properly defined!"

        f_param%spring_force=wig_lib%get("spring_force",default=.FALSE.)
        !! @input_file WIGNER_AUX/spring_force (gwild, default=.FALSE.)

        f_param%k2_smooth=wig_lib%get("k2_smoothing",default=.FALSE.)
        !! @input_file WIGNER_AUX/k2_smoothing (gwild, default=.FALSE.)        
        f_param%k2_smooth_width=wig_lib%get("k2_smooth_width",default=0.03)
        !! @input_file WIGNER_AUX/k2_smooth_width (gwild, default=0.03)
        f_param%k2_smooth_center=wig_lib%get("k2_smooth_center",default=0.9)
        !! @input_file WIGNER_AUX/k2_smooth_center (gwild, default=0.9)
        if(f_param%k2_smooth) then
            ! write(*,*)
            ! write(*,*) "k2 smoothing of off-diagonal terms activated."
            ! write(*,*) "  using fermi function with threshold ",f_param%k2_smooth_center
            ! write(*,*) "  and transition width",f_param%k2_smooth_width
            ! f_param%G_smooth=wig_lib%get("g_smoothing",default=.TRUE.)
            write(*,*) "Warning: smoothing not implemented yet!"
        endif
      
        ! ALLOCATE(f_param%k2_smooth_mat(system%n_dof,system%n_dof))
  

        !INITIALIZE FORCES PARAMETERS
        allocate( &
            f_param%EigMat(n_beads,n_beads), &
            f_param%EigMatTr(n_beads,n_beads), &
            f_param%OmK(n_beads), &
            f_param%mOmK(n_beads,n_atoms), &
            f_param%gammaExp(n_beads,n_atoms), &
            f_param%sigmaBAOAB(n_beads,n_atoms), &
            f_param%sigp2_inv(n_atoms), &
            f_param%sqrt_mass(n_atoms) &
        )
        f_param%beta=parameters%beta
        f_param%dBeta=parameters%beta/REAL(f_param%n_beads,wp)
        f_param%sigp2_inv(:)=system%mass(:)/f_param%dBeta
        f_param%sqrt_mass=sqrt(system%mass)

        ! GET NUMBER OF THREADS        
        f_param%n_threads=1
        !$ f_param%n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

        !INITIALIZE WORK VARIABLES
        allocate(WORKS(f_param%n_threads))
        DO i=1,f_param%n_threads
            allocate( &
                WORKS(i)%F(n_dof), &
                WORKS(i)%Delta2(n_dof,n_dof), &
                WORKS(i)%R(n_atoms), &
                WORKS(i)%R1(n_beads), &
                WORKS(i)%R2(n_beads) &
            )
        ENDDO


        !INITIALIZE polymers
        DO i=1,NUM_AUX_SIM
            allocate( &
                polymers(i)%X(n_beads,n_atoms,n_dim), &
                polymers(i)%EigX(n_beads,n_atoms,n_dim), &
                polymers(i)%EigV(n_beads,n_atoms,n_dim), &
                ! polymers(i)%X_prev(0:n_beads,n_atoms,n_dim), &
                ! polymers(i)%P_prev(n_beads,n_atoms,n_dim), &
                polymers(i)%F_beads(0:n_beads,n_atoms,n_dim), &
                ! polymers(i)%F_beads_prev(0:n_beads,n_atoms,n_dim), &
                polymers(i)%Pot_beads(0:n_beads), &
                ! polymers(i)%Pot_beads_prev(0:n_beads), &
               ! polymers(i)%Delta(n_atoms,n_dim), &
                ! polymers(i)%Delta_prev(n_atoms,n_dim), &
                polymers(i)%Delta_packed(n_dof), &
                polymers(i)%k2(n_dof,n_dof), &
                ! polymers(i)%k1(n_dof), &
                ! polymers(i)%k2_reweight(n_dof,n_dof), &
                ! polymers(i)%k2inv(n_dof,n_dof), &
                polymers(i)%F(n_dof), &
                ! polymers(i)%F_reweight(n_dof), &
                polymers(i)%G(n_dof,n_dof,n_dof) &
                ! polymers(i)%G_reweight(n_dof,n_dof,n_dof) &
            )
            polymers(i)%Delta_packed=0._wp
            ! polymers(i)%Delta_prev=0._wp
            polymers(i)%X=0
            polymers(i)%EigX=0
            polymers(i)%EigV=0
            ! polymers(i)%P_prev=polymers(i)%P
            ! polymers(i)%X_prev=polymers(i)%X    
        ENDDO

        !  f_param%moment_matching=wig_lib%get("moment_matching",default=.FALSE.)
        ! !! @input_file WIGNER_AUX/moment_matching (gwild, default=.FALSE.)   
        ! if(f_param%moment_matching) then            
        !     DO i=1,NUM_AUX_SIM
        !         allocate(polymers(i)%k2_mm(n_dof,n_dof))
        !     ENDDO
        ! endif

        ! f_param%reweight_previous_configs=wig_lib%get("reweight_previous_configs",default=.FALSE.)
        ! if(f_param%reweight_previous_configs.OR.f_param%moment_matching) then
        !     DO i=1,NUM_AUX_SIM
        !         polymers(i)%prev_config_filename=output%working_directory//"/prev_config_"//int_to_str(i)
        !         polymers(i)%i_step_reweight=0
        !     ENDDO
        !     call EXECUTE_COMMAND_LINE("rm "//output%working_directory//"/prev_config_*")
        !     f_param%n_steps_reweight=1
        !     if(f_param%reweight_previous_configs) then
        !         f_param%n_steps_reweight=wig_lib%get("n_steps_reweight",default=1)                
        !     endif            
        ! endif

        ! f_param%compute_EW = parameters%compute_EW

        ! if(f_param%compute_EW) then
        !     DO i=1,f_param%n_threads
        !         allocate(WORKS(i)%Delta4(n_dof,n_dof,n_dof,n_dof))
        !     ENDDO
        !     DO i=1,NUM_AUX_SIM
        !         allocate(polymers(i)%k4(n_dof,n_dof,n_dof,n_dof))
        !     ENDDO            
        ! endif        

        call initialize_bead_forces(param_library)

        call initialize_move_generator(system,parameters,wig_lib)

        DO i=1,NUM_AUX_SIM
            call sample_momenta(polymers(i))
        ENDDO

        if(.not. parameters%only_auxiliary_simulation) then
            n_steps_init=wig_lib%get("n_steps_init",default=5000)
            !! @input_file WIGNER_AUX/n_steps_init (gwild, default=5000)
            n_therm_tmp = f_param%n_steps_therm
            f_param%n_steps_therm = n_steps_init
            write(*,*) "Initializing g-WiLD forces with",n_steps_init,"steps"
            call compute_gwild_forces(system,parameters)
            write(*,*) "g-WiLD forces initialization done."
            f_param%n_steps_therm = n_therm_tmp
        endif

    end subroutine initialize_auxiliary_simulation


    subroutine sample_momenta(polymer)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i,j,k

        DO k=1,f_param%n_dim ; DO j=1,f_param%n_atoms            
            call randGaussN(polymer%EigV(:,j,k))
            polymer%EigV(:,j,k)=polymer%EigV(:,j,k)/sqrt(f_param%dBeta)   !*f_param%sqrt_mass(j)    
        ENDDO  ; ENDDO
    end subroutine sample_momenta

    subroutine initialize_move_generator(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(GWILD_SYS), INTENT(inout) :: system
		CLASS(GWILD_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,j,k,nu,INFO
        REAL(wp) :: TRPMD_lambda, gamma0,gammak
        REAL(wp), ALLOCATABLE ::  TMP(:)

        nu=f_param%n_beads

        f_param%move_generator=wig_lib%get("move_generator" &
                    ,default="PIOUD")
        !! @input_file WIGNER_AUX/move_generator (wigner, default="PIOUD")
        f_param%move_generator=to_upper_case(f_param%move_generator)
        parameters%move_generator=f_param%move_generator

        SELECT CASE(f_param%move_generator)
        CASE ("PIOUD","BAOAB")

            !INITIALIZE PIOUD
            ! SHORTHANDS FOR MASS
            call get_polymer_masses(system,wig_lib)
            f_param%dt=wig_lib%get("dt",default=parameters%dt)
            !! @input_file WIGNER_AUX/dt (wigner, default= PARAMETERS/dt, if move_generator="PIOUD")

            !INITIALIZE DYNAMICAL MATRIX
            f_param%EigMat=0
            do i=1,nu-1
                f_param%EigMat(i,i)=2
                f_param%EigMat(i+1,i)=-1
                f_param%EigMat(i,i+1)=-1
            enddo
            f_param%EigMat(nu,nu)=2
            f_param%EigMat(1,nu)=-1
            f_param%EigMat(nu,1)=-1
            f_param%EigMat(nu-1,nu)=1
            f_param%EigMat(nu,nu-1)=1

            !SOLVE EIGENPROBLEM
            allocate(TMP(3*nu-1))
            CALL DSYEV('V','L',nu,f_param%EigMat,nu,f_param%Omk,TMP,3*nu-1,INFO)
            if(INFO/=0) then
                write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
                stop 
            endif
            deallocate(TMP)
            f_param%EigMatTr=transpose(f_param%EigMat)
            f_param%Omk=SQRT(f_param%Omk)/f_param%dBeta
            DO i=1,system%n_atoms
                f_param%mOmk(:,i)=f_param%Omk*sqrt(system%mass(i))/f_param%sqrt_mass(i) 
            ENDDO
            write(*,*)
            write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
            do i=1,nu
                write(*,*) i, f_param%Omk(i)*THz/(2*pi),"THz"
            enddo
            write(*,*)  
                

            allocate(f_param%expOmk(nu,2,2,system%n_atoms))      

            SELECT CASE(f_param%move_generator)
            CASE ("PIOUD")
                CALL initialize_PIOUD(system,wig_lib)
                move_proposal => PIOUD_step
            CASE ("BAOAB")
                move_proposal => BAOAB_step
                gamma0=wig_lib%get("baoab_damping",default=1._wp/f_param%dBeta)
                f_param%BAOAB_num=wig_lib%get("baoab_num",default=.FALSE.)
                TRPMD_lambda=wig_lib%get("trpmd_lambda",default=1._wp)
                DO j=1,f_param%n_atoms ; DO i=1,nu
                    gammak=max(TRPMD_lambda*f_param%mOmk(i,j) &
                                            ,gamma0)
                    f_param%gammaExp(i,j)=exp(-gammak*f_param%dt)
                    f_param%sigmaBAOAB(i,j)=sqrt((1-exp(-2*gammak*f_param%dt))/f_param%dBeta)
                    f_param%expOmk(i,1,1,j)=cos(f_param%mOmk(i,j)*0.5*f_param%dt)
                    f_param%expOmk(i,1,2,j)=sin(f_param%mOmk(i,j)*0.5*f_param%dt)/f_param%mOmk(i,j)
                    f_param%expOmk(i,2,1,j)=-sin(f_param%mOmk(i,j)*0.5*f_param%dt)*f_param%mOmk(i,j)
                    f_param%expOmk(i,2,2,j)=cos(f_param%mOmk(i,j)*0.5*f_param%dt)
                ENDDO ; ENDDO
            END SELECT

        CASE DEFAULT
            write(0,*) "Error: unknown move generator '"//f_param%move_generator//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_move_generator

    subroutine get_polymer_masses(system,wig_lib)
        IMPLICIT NONE
        CLASS(GWILD_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        TYPE(DICT_STRUCT), POINTER :: m_lib
        CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
        LOGICAL, ALLOCATABLE :: is_sub(:)
        REAL(wp) :: m_tmp
        INTEGER :: i
        CHARACTER(2) :: symbol

        
        if(.not. wig_lib%has_key("modified_masses")) then
            f_param%sqrt_mass=SQRT(system%mass)
            return
        endif

        m_lib => wig_lib%get_child("modified_masses")
        write(*,*)
        write(*,*) "g-WiLD auxiliary masses:"
        call dict_list_of_keys(m_lib,keys,is_sub)
        DO i=1,size(keys)
			if(is_sub(i)) CYCLE
            m_tmp=m_lib%get(keys(i))
            symbol=trim(keys(i))
            symbol(1:1)=to_upper_case(symbol(1:1))
            write(*,*) symbol," : ", real(m_tmp/Mprot),"amu"
        ENDDO
        write(*,*)
        do i=1, system%n_atoms
            f_param%sqrt_mass(i)=m_lib%get(trim(system%element_symbols(i)) &
                                            ,default=system%mass(i) )
            !if(f_param%sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", f_param%sqrt_mass(i)/Mprot," amu"                    
        enddo
        f_param%sqrt_mass=sqrt(f_param%sqrt_mass)

    end subroutine get_polymer_masses

    subroutine initialize_PIOUD(system,wig_lib)
        IMPLICIT NONE
        CLASS(GWILD_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,nu,j
		REAL(wp), ALLOCATABLE ::thetaInv(:,:,:,:),OUsig2(:,:,:,:)
		REAL(wp) :: Id(2,2)
        

        nu=f_param%n_beads

        Id=0
		Id(1,1)=1
		Id(2,2)=1

        allocate( &
            thetaInv(nu,2,2,system%n_atoms), &
            OUsig2(nu,2,2,system%n_atoms) &
        )
        if(allocated(f_param%OUsig)) deallocate(f_param%OUsig)
		allocate(f_param%OUsig(nu,2,2,system%n_atoms)) 
        if(allocated(f_param%expOmk)) deallocate(f_param%expOmk)
		allocate(f_param%expOmk(nu,2,2,system%n_atoms))              

        DO i=1,system%n_atoms
            f_param%expOmk(:,1,1,i)=exp(-f_param%mOmk(:,i)*f_param%dt) &
                                    *(1-f_param%mOmk(:,i)*f_param%dt)
            f_param%expOmk(:,1,2,i)=exp(-f_param%mOmk(:,i)*f_param%dt) &
                                    *(-f_param%dt*f_param%mOmk(:,i)**2)
            f_param%expOmk(:,2,1,i)=exp(-f_param%mOmk(:,i)*f_param%dt)*f_param%dt
            f_param%expOmk(:,2,2,i)=exp(-f_param%mOmk(:,i)*f_param%dt) &
                                    *(1+f_param%mOmk(:,i)*f_param%dt)

            thetaInv(:,1,1,i)=0._wp
            thetaInv(:,1,2,i)=-1._wp
            thetaInv(:,2,1,i)=1._wp/f_param%mOmk(:,i)**2
            thetaInv(:,2,2,i)=2._wp/f_param%mOmk(:,i)
        ENDDO

        !COMPUTE COVARIANCE MATRIX
        DO i=1,system%n_atoms
            OUsig2(:,1,1,i)=(1._wp- (1._wp-2._wp*f_param%dt*f_param%mOmk(:,i) &
                                +2._wp*(f_param%dt*f_param%mOmk(:,i))**2 &
                            )*exp(-2._wp*f_param%mOmk(:,i)*f_param%dt) &
                            )/f_param%dBeta
            OUsig2(:,2,2,i)=(1._wp- (1._wp+2._wp*f_param%dt*f_param%mOmk(:,i) &
                                +2._wp*(f_param%dt*f_param%mOmk(:,i))**2 &
                            )*exp(-2._wp*f_param%mOmk(:,i)*f_param%dt) &
                            )/(f_param%dBeta*f_param%mOmk(:,i)**2)
            OUsig2(:,2,1,i)=2._wp*f_param%mOmk(:,i)*(f_param%dt**2) &
                            *exp(-2._wp*f_param%mOmk(:,i)*f_param%dt)/f_param%dBeta
            OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

            !COMPUTE CHOLESKY DECOMPOSITION
            f_param%OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
            f_param%OUsig(:,1,2,i)=0
            f_param%OUsig(:,2,1,i)=OUsig2(:,2,1,i)/f_param%OUsig(:,1,1,i)
            f_param%OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-f_param%OUsig(:,2,1,i)**2)
        ENDDO

		!CHECK CHOLESKY DECOMPOSITION
		! write(*,*) "sum of squared errors on cholesky decomposition:"
		! do i=1,nu
		! 	write(*,*) i, sum( (matmul(f_param%OUsig(i,:,:),transpose(f_param%OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
		! enddo

    end subroutine initialize_PIOUD

    subroutine initialize_bead_forces(param_library)
        IMPLICIT NONE        
        TYPE(DICT_STRUCT), intent(in) :: param_library
        CHARACTER(:), ALLOCATABLE :: pot_name

        f_param%parallel_forces=.FALSE.
		pot_name=param_library%get("POTENTIAL/pot_name")
		if(to_upper_case(trim(pot_name))=="SOCKET") f_param%parallel_forces=.TRUE.

        IF(f_param%parallel_forces) THEN
            update_beads_forces => update_beads_forces_socket_parallel
        ELSE
            update_beads_forces => update_beads_forces_serial
        ENDIF

    end subroutine initialize_bead_forces

!----------------------------------------------------------------------

    subroutine update_beads_forces_serial(q,polymer)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q(:,:)
		INTEGER :: i,nu
        
        nu=f_param%n_beads
		do i=1,nu-1
            call get_pot_info(q+polymer%X(i,:,:),polymer%Pot_beads(i),polymer%F_beads(i,:,:))
		enddo
        call get_pot_info(q+polymer%X(nu,:,:),polymer%Pot_beads(0),polymer%F_beads(0,:,:))
        call get_pot_info(q-polymer%X(nu,:,:),polymer%Pot_beads(nu),polymer%F_beads(nu,:,:))

        polymer%F_beads(0,:,:)=0.5_wp*polymer%F_beads(0,:,:)
        polymer%F_beads(nu,:,:)=0.5_wp*polymer%F_beads(nu,:,:)
        polymer%Pot_beads(0)=0.5_wp*polymer%Pot_beads(0)
        polymer%Pot_beads(nu)=0.5_wp*polymer%Pot_beads(nu)
	end subroutine update_beads_forces_serial

    subroutine update_beads_forces_socket_parallel(q,polymer)
        !$ USE OMP_LIB
        implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q(:,:)
		INTEGER :: i,is,nu
        REAL(wp), ALLOCATABLE :: Xtmp(:,:,:)

        nu=f_param%n_beads
        is=1        
        !$ is=OMP_GET_THREAD_NUM()+1
        ALLOCATE(Xtmp(0:nu,f_param%n_atoms,f_param%n_dim))
        DO i=1,nu-1
            Xtmp(i,:,:) = q + polymer%X(i,:,:)
        ENDDO
        Xtmp(0,:,:) = q + polymer%X(nu,:,:)
        Xtmp(nu,:,:) = q - polymer%X(nu,:,:)
		CALL parallel_socket_pot_info(Xtmp,polymer%F_beads,polymer%Pot_beads,i_socket=is)
        polymer%F_beads(0,:,:)=0.5_wp*polymer%F_beads(0,:,:)
        polymer%F_beads(nu,:,:)=0.5_wp*polymer%F_beads(nu,:,:)
        polymer%Pot_beads(0)=0.5_wp*polymer%Pot_beads(0)
        polymer%Pot_beads(nu)=0.5_wp*polymer%Pot_beads(nu)

        DEALLOCATE(Xtmp)

	end subroutine update_beads_forces_socket_parallel

!----------------------------------------------------------------------
! COMPUTE EDGEWORTH CORRECTION

	! subroutine compute_EW(system,parameters)
	! 	implicit none
	! 	TYPE(GWILD_SYS), INTENT(inout) :: system
    !     CLASS(GWILD_PARAM), INTENT(inout) :: parameters
	! 	INTEGER :: j,k,l,m
	! 	REAL(wp), ALLOCATABLE, SAVE :: k2(:,:,:)
	! 	REAL(wp) :: C6        

    !     if(.not. f_param%compute_EW) then
    !         system%EW=1
    !         system%EW6=1
    !         return
    !     endif

	! 	system%EW=0

    !     if(parameters%classical_wigner) then
    !         system%P_packed = RESHAPE(system%P_mod, (/system%n_dof/))
    !     endif

	! 	if(.not. allocated(k2)) allocate(k2(system%n_dof,system%n_dof,2))
	! 	k2(:,:,1)= 0.5_wp*( polymers(1)%k2(:,:) + polymers(2)%k2(:,:) )
	! 	k2(:,:,2)= polymers(3)%k2(:,:)

	! 	do m=1,system%n_dof ; do l=1,system%n_dof ; do k=1,system%n_dof ; do j=1,system%n_dof
	
	! 		system%EW = system%EW + system%P_packed(j)*system%P_packed(k) &
    !                     *system%P_packed(l)*system%P_packed(m)*( &
    !                         system%k4(j,k,l,m)  - k2(j,l,1)*k2(k,m,2) &
    !                                             - k2(k,l,1)*k2(j,m,2) &
    !                                             - k2(j,k,1)*k2(l,m,2) &
    !                     )
														
	! 	enddo ; enddo ; enddo ; enddo

	! 	system%EW = 1 + system%EW / 24._wp

	! 	if(system%n_dof==1) then
	! 		C6 = system%k6 - 15._wp*polymers(1)%k2(1,1)*0.5_wp*(polymers(2)%k4(1,1,1,1)+polymers(3)%k4(1,1,1,1)) &
	! 			+30._wp*polymers(1)%k2(1,1)*polymers(2)%k2(1,1)*polymers(3)%k2(1,1)
	! 		system%EW6 = system%EW - (system%P_packed(1)**6) * C6 / 720._wp
	! 	endif

	! end subroutine compute_EW

!--------------------------------------------------
! REWEIGHT PREVIOUS CONFIGS

    ! subroutine reweight_previous_configs(system,parameters,polymer,WORK)
    !     IMPLICIT NONE
	! 	CLASS(GWILD_SYS), INTENT(inout) :: system
	! 	CLASS(GWILD_PARAM), INTENT(inout) :: parameters
    !     CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    !     CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
    !     INTEGER :: u,ios,nat,ndim,c,i,j,is,cloc,k
    !     REAL(wp), ALLOCATABLE :: Delta(:,:),Xnu(:,:),X1(:,:),F_delta(:,:)
    !     REAL(wp) :: weight_old,weight_new,pot0,potnu,weight_ratio,mean_weight_ratio,weight_sum
    !     REAL(wp) :: Pot0_old,Potnu_old,mean_ratios(f_param%n_steps_reweight),mean_ratio_min
    !     LOGICAL :: file_exist
    !     CHARACTER(:), ALLOCATABLE :: filename

    !     nat=system%n_atoms
    !     ndim=system%n_dim
    !     allocate(Delta(nat,ndim),Xnu(nat,ndim),X1(nat,ndim),F_delta(nat,ndim))
    !     polymer%k2_reweight=0
    !     polymer%G_reweight=0
    !     polymer%F_reweight=0

    !     c=0
    !     mean_ratios=0
    !     weight_sum=0
    !     polymer%mean_weight_ratio=0
    !     DO is=1,f_param%n_steps_reweight
    !         filename=polymer%prev_config_filename//"_"//int_to_str(is)
    !         INQUIRE(FILE=filename,EXIST=file_exist)
    !         if(.not. file_exist) CYCLE
    !         OPEN(newunit=u,file=filename,form="unformatted")
    !         cloc=0            
    !         DO
    !             READ(u,iostat=ios) weight_old,Pot0_old,Potnu_old,Delta,X1,Xnu,WORK%F
    !             if(ios/=0) EXIT
    !             c=c+1
    !             cloc=cloc+1
    !             call get_pot_info(system%X+0.5_wp*Delta,pot0,F_delta)
    !             WORK%F=WORK%F+0.5_wp*RESHAPE(F_delta,(/system%n_dof/)) /REAL(f_param%n_beads,wp)
    !             call get_pot_info(system%X-0.5_wp*Delta,potnu,F_delta)
    !             WORK%F=WORK%F+0.5_wp*RESHAPE(F_delta,(/system%n_dof/))/REAL(f_param%n_beads,wp)
    !             weight_new=compute_config_weight(system%X,0.5*pot0,0.5*potnu,Delta,X1,Xnu)
            
    !             weight_ratio=exp(-(weight_new-weight_old))
    !             !write(*,*) is, weight_ratio
    !             !write(*,*) polymer%prev_config_filename,c,weight_ratio,Pot0_old,0.5*Pot0
    !             weight_sum= weight_sum + weight_ratio
    !             polymer%mean_weight_ratio = polymer%mean_weight_ratio &
    !                 +(weight_ratio - polymer%mean_weight_ratio)/REAL(c,wp)
                
    !             mean_ratios(is)=mean_ratios(is) &
    !                 +(weight_ratio - mean_ratios(is))/REAL(cloc,wp)

    !             polymer%Delta_packed=RESHAPE(Delta,(/system%n_dof/))
    !             !COMPUTE ONLY THE LOWER TRIANGULAR PART
    !             do j=1,system%n_dof ; do i=j,system%n_dof
    !                 WORK%Delta2(i,j)=weight_ratio*polymer%Delta_packed(i)*polymer%Delta_packed(j)
    !                 polymer%k2_reweight(i,j) =  polymer%k2_reweight(i,j) &
    !                 ! + weight_ratio*polymer%Delta_packed(i)*polymer%Delta_packed(j)
    !                     + (WORK%Delta2(i,j) -polymer%k2_reweight(i,j))/REAL(c,wp)
    !             enddo ; enddo  

    !             DO k=1,f_param%n_dof
    !                 do j=1,system%n_dof ; do i=j,system%n_dof
    !                     polymer%G_reweight(i,j,k)=polymer%G_reweight(i,j,k) &
    !                         +(WORK%Delta2(i,j)*WORK%F(k)-polymer%G_reweight(i,j,k))/REAL(c,wp)
    !                 enddo ; enddo
    !             ENDDO  
    !             polymer%F_reweight=polymer%F_reweight &
    !                 + (weight_ratio*WORK%F - polymer%F_reweight)/REAL(c,wp)   
    !         ENDDO
    !         !polymer%mean_weight_ratio=weight_sum/REAL(c,wp)
            
    !         close(u)
    !     ENDDO
    !    ! if(f_param%verbose) &
    !         !write(*,*) mean_ratios
    !     !    write(*,*) "minimum mean ratio =",minval(mean_ratios)

    !     DO j=1,system%n_dof ; DO i=j+1,system%n_dof
    !         polymer%k2_reweight(j,i)=polymer%k2_reweight(i,j)
    !     ENDDO ; ENDDO
    !     DO k=1,f_param%n_dof
    !         DO j=1,system%n_dof ; DO i=j+1,system%n_dof
    !            polymer%G_reweight(j,i,k)=polymer%G_reweight(i,j,k)
    !         ENDDO ; ENDDO
    !     ENDDO 
    !     polymer%k2_reweight=polymer%k2_reweight/polymer%mean_weight_ratio
    !     polymer%G_reweight=polymer%G_reweight/polymer%mean_weight_ratio
    !     polymer%F_reweight=polymer%F_reweight/polymer%mean_weight_ratio

    ! end subroutine

    !  subroutine moment_matching(system,parameters,polymer,WORK)
    !     IMPLICIT NONE
	! 	CLASS(GWILD_SYS), INTENT(inout) :: system
	! 	CLASS(GWILD_PARAM), INTENT(inout) :: parameters
    !     CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    !     CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
    !      INTEGER :: u,ios,nat,ndim,c,i,j,is,cloc,k
    !     REAL(wp), ALLOCATABLE :: Delta(:),Xnu(:),X1(:),F_delta(:)
    !     REAL(wp) :: weight_old,weight_new,pot0,potnu,weight_ratio,mean_weight_ratio,weight_sum
    !     REAL(wp) :: Pot0_old,Potnu_old,mean_ratios(f_param%n_steps_reweight),mean_ratio_min
    !     LOGICAL :: file_exist
    !     CHARACTER(:), ALLOCATABLE :: filename,istepchar

    !     nat=system%n_atoms
    !     ndim=system%n_dim
    !     allocate(Delta(nat*ndim),Xnu(nat*ndim),X1(nat*ndim))
    !     polymer%k2_mm=0
    !     WORK%Delta2=0

    !     c=0
    !     filename=polymer%prev_config_filename
    !     istepchar=int_to_str(polymer%i_step_reweight)
    !     filename=filename//"_"//istepchar
    !     !write(0,*) polymer%i_step_reweight,int_to_str(polymer%i_step_reweight)
    !     INQUIRE(FILE=filename,EXIST=file_exist)
    !     !write(0,*) filename, file_exist
    !     if(.not. file_exist) STOP "Error: no config file to perform moment matching!"      
    !     OPEN(newunit=u,file=filename,form="unformatted")           
    !     DO
    !         READ(u,iostat=ios) weight_old,Pot0_old,Potnu_old,Delta,X1,Xnu!,WORK%F
    !         if(ios/=0) EXIT
    !         c=c+1
    !         write(*,*) polymer%k1
    !         Delta=2*Delta-polymer%k1
    !         do j=1,system%n_dof ; do i=j,system%n_dof
    !             WORK%Delta2(i,j)=Delta(i)*Delta(j)
    !         enddo ; enddo
    !         polymer%k2_mm=polymer%k2_mm +(WORK%Delta2 - polymer%k2_mm)/c
    !     ENDDO        
    !     close(u)
    !     DO j=1,system%n_dof ; DO i=j+1,system%n_dof
    !         polymer%k2_mm(j,i)=polymer%k2_mm(i,j)
    !     ENDDO ; ENDDO

    ! end subroutine moment_matching

END MODULE gwild_forces
