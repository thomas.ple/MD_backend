MODULE classical_mc
    USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE classical_md, ONLY: classical_MD_get_integrator_parameters, &
							classical_MD_initialize_FE_mean_force, &
							classical_MD_initialize_momenta, &
							classical_MD_get_parameters
	USE classical_mc_types
	USE classical_mc_moves
	USE classical_md_integrators, only: class_MD_compute_forces, &
										class_MD_potential_only
	USE classical_md_types, only: classical_MD_update_mean_force &
								, classical_MD_write_mean_force_info
	USE thermostats, only : finalize_thermostat
    IMPLICIT NONE

    TYPE(CLASS_MC_SYS) :: class_MC_system
    TYPE(CLASS_MC_PARAM) :: class_MC_parameters

    PROCEDURE(class_MC_sub), pointer :: move_proposal
	PROCEDURE(class_MC_fct_real), pointer :: compute_acc_probability

    PRIVATE :: class_MC_system,class_MC_parameters &
				,move_proposal,compute_acc_probability &
				,class_MC_update_mean_values

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine classical_mc_loop()
		IMPLICIT NONE
		INTEGER :: i

		if(class_MC_parameters%n_steps_therm > 0) then
			call class_MC_system%initialize_counters()
			write(*,*) "Starting ",class_MC_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,class_MC_parameters%n_steps_therm
				call class_MC_system%save_config()
				call move_proposal(class_MC_system,class_MC_parameters)
                call classical_MC_acceptance_test(class_MC_system,class_MC_parameters)
			ENDDO
			write(*,*) "Thermalization done."
            call class_MC_system%update_acc_ratio()
            write(*,*) "[acceptance ratio during thermalization = ", class_MC_system%acc_ratio,"]"
		endif  

		call class_MC_system%initialize_counters()
		write(*,*)
		write(*,*) "Starting ",class_MC_parameters%n_steps," steps &
						&of classical MC"
		DO i=1,class_MC_parameters%n_steps

			call class_MC_system%save_config()
			call move_proposal(class_MC_system,class_MC_parameters)
            call classical_MC_acceptance_test(class_MC_system,class_MC_parameters)

			if(class_MC_parameters%on_the_fly_mean_values) &
				call class_MC_update_mean_values(class_MC_system,class_MC_parameters,i)

			call output%write_all()

		ENDDO

		if(allocated(class_MC_system%FE_mean_force)) then
			write(*,*) "mean force=", class_MC_system%FE_mean_force!*bohr
			write(*,*) "std dev force=", class_MC_system%FE_std_dev_force!*bohr
			if(class_MC_parameters%write_mean_force_info) &
				call classical_MD_write_mean_force_info(class_MC_system,class_MC_parameters)
		endif
       
		write(*,*) "Simulation done."
        call class_MC_system%update_acc_ratio()
        write(*,*) "[acceptance ratio = ", class_MC_system%acc_ratio,"]"

	end subroutine classical_mc_loop

	subroutine classical_MC_acceptance_test(system,parameters)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: acc_probability, R
        
        acc_probability=compute_acc_probability(system,parameters)

        call random_number(R)
        if(R <= acc_probability) then
            system%n_acc_proposals=system%n_acc_proposals+1
        else
            call system%restore_prev_config()
        endif

        system%total_proposals=system%total_proposals+1

    end subroutine classical_MC_acceptance_test
!-----------------------------------------------------------------

    subroutine initialize_classical_MC(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library

		class_MC_parameters%engine="classical_mc"

		!ASSOCIATE classical_MC_loop
		simulation_loop => classical_MC_loop

		!GET ALGORITHM
		class_MC_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="boltzmann")
		!! @input_file PARAMETERS/algorithm (classical_MC, default="boltzmann")
		class_MC_parameters%algorithm=to_lower_case(class_MC_parameters%algorithm)

        class_MC_system%acc_ratio=0
        class_MC_system%n_acc_proposals=0
        class_MC_system%total_proposals=0

		call classical_MC_get_parameters(class_MC_system,class_MC_parameters,param_library)
		call classical_MC_get_algo_parameters(class_MC_system,class_MC_parameters,param_library)

		call classical_MC_initialize_output(class_MC_system,class_MC_parameters,param_library)

	end subroutine initialize_classical_MC

	subroutine finalize_classical_MC()
		IMPLICIT NONE

		if(class_MC_parameters%move_generator=="langevin") &
			call finalize_thermostat(class_MC_system,class_MC_parameters)

		call classical_MC_deallocate(class_MC_system,class_MC_parameters)
		
	end subroutine finalize_classical_MC

!-----------------------------------------------------------------

! GET PARAMETERS FROM LIBRARY
	subroutine classical_MC_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_get_parameters(system,parameters,param_library)

		allocate(system%X_prev(system%n_atoms,system%n_dim))
		system%X_prev=system%X

	end subroutine classical_MC_get_parameters

	subroutine classical_MC_get_algo_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: parameters_cat

		parameters_cat => param_library%get_child("PARAMETERS")

		SELECT CASE(parameters%algorithm)
		CASE ("boltzmann")

			!ASSOCIATE compute_acc_probability
			compute_acc_probability => classical_MC_Boltzmann_ratio
			system%MC_Energy = Pot(system%X)
		
		!SELECT MOVE GENERATOR
			parameters%move_generator=parameters_cat%get("mc_move_generator",default="gaussian")
			!! @input_file PARAMETERS/mc_move_generator (classical_MC, default="gaussian" if algorithm=boltzmann)
			parameters%move_generator=to_lower_case(parameters%move_generator)
			SELECT CASE(parameters%move_generator)
			CASE("gaussian")

				parameters%sigma_move=parameters_cat%get("mc_sigma_move",default=0.1/bohr)
				!! @input_file PARAMETERS/mc_sigma_move (classical_MC, default=0.1/bohr)
				if(parameters%sigma_move<=0) STOP "Error: 'sigma_move' is not properly defined!"

				!ASSOCIATE move_proposal
				move_proposal => classical_MC_gaussian_move
				write(*,*) "acceptance probability: Boltzmann"

			CASE DEFAULT
				write(0,*) "Error: unknown move generator '"//parameters%move_generator//"' !"
				STOP "Execution stopped"
			END SELECT

		CASE( "mean_force" )

			call classical_MD_initialize_FE_mean_force(system,parameters,param_library)
			allocate( system%X_collective_prev(parameters%collective_coord_size) )

			!MEAN VALUES ON THE FLY
			parameters%on_the_fly_mean_values = .TRUE.
			class_MC_update_mean_values => classical_MD_update_mean_force


		!SELECT MOVE GENERATOR
			parameters%move_generator=parameters_cat%get("mc_move_generator",default="langevin")
			!! @input_file PARAMETERS/mc_move_generator (classical_MC, default="langevin" if algorithm=mean force)
			parameters%move_generator=to_lower_case(parameters%move_generator)
			SELECT CASE(parameters%move_generator)
			CASE("gaussian")

				!ASSOCIATE move_proposal
				move_proposal => classical_MC_gaussian_move

				!ASSOCIATE compute_acc_probability
				compute_acc_probability => classical_MC_constrained_Boltzmann
				write(*,*) "acceptance probability: constrained Boltzmann"
        
        		system%MC_Energy=Pot(system%X) + 0.5_wp* sum( &
                parameters%FE_kappa(:)*(system%X_collective(:) - system%FE_Z(:))**2 )

				parameters%sigma_move=parameters_cat%get("mc_sigma_move",default=0.1/bohr)
				if(parameters%sigma_move<=0) STOP "Error: 'sigma_move' is not properly defined!"

			
			CASE("langevin")

				!ASSOCIATE move_proposal
				move_proposal => classical_MC_langevin_move

				!ASSOCIATE compute_acc_probability
				compute_acc_probability => classical_MC_constraint_only
				write(*,*) "acceptance probability: constraint only"

				!GET dt
				parameters%dt=parameters_cat%get("dt")
				!! @input_file PARAMETERS/dt (classical_MC, if move_generator=langevin)
				if(parameters%dt<=0) STOP "Error: 'dt' is not properly defined!"
				!GET INTEGRATOR PARAMETERS
				call classical_MD_get_integrator_parameters(system,parameters,param_library)

				!GET n_steps OF AUXILIARY LANGEVIN
				parameters%n_steps_aux_lgv=param_library%get("FREE_ENERGY/n_steps_aux_lgv")
				!! @input_file FREE_ENERGY/n_steps_aux_lgv (classical_MC)
				!GET momentum flip
				parameters%MF_momentum_flip=param_library%get("FREE_ENERGY/mf_momentum_flip",default=.TRUE.)
				!! @input_file FREE_ENERGY/mf_momentum_flip (classical_MC)
				if(.not. parameters%MF_momentum_flip) &
					write(*,*) "Warning: momentum flip deactivated, results could go wrong!"

				!INITIALIZE MOMENTA
				call classical_MD_initialize_momenta(system,parameters)
				allocate(system%P_prev(system%n_atoms,system%n_dim))

				!ALLOCATE Forces
				allocate(system%Forces(system%n_atoms,system%n_dim))
				allocate(system%Forces_prev(system%n_atoms,system%n_dim))
				class_MD_compute_forces => class_MD_potential_only
				system%Forces=-dPot(system%X)

				system%MC_Energy= 0.5_wp * sum( &
                parameters%FE_kappa(:)*( system%X_collective(:)-system%FE_Z(:) )**2 )

			CASE DEFAULT
				write(0,*) "Error: unknown move generator '"//parameters%move_generator//"' !"
				STOP "Execution stopped"
			END SELECT

		CASE DEFAULT
			write(0,*) "Error: unknown MC algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine classical_MC_get_algo_parameters
!-----------------------------------------------------------------

	subroutine classical_MC_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat, dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name
		INTEGER :: i_file, i
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%write_mean_force_info=.FALSE.

		output_cat => param_library%get_child("OUTPUT")

		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("position","positions")
			!! @input_file OUTPUT/position (classical_MC)
				description="MC series of positions"
				default_name="X.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%X(:,1),index=i_file,name="positions")

			
			CASE("fe_force")
			!! @input_file OUTPUT/fe_force (classical_MC)
				if(parameters%algorithm/="mean_force") CYCLE
				description="trajectory of free energy force"
				default_name="FE_force.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%FE_force(:),index=i_file,name="FE_force")

			CASE("fe_mean_force")	
			!! @input_file OUTPUT/fe_mean_force (classical_MC)
				if(parameters%algorithm/="mean_force") CYCLE

				description="evolution of free energy mean force"
				default_name="FE_mean_force.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%FE_mean_force(:),index=i_file,name="FE_mean_force")
			
			CASE("mean_force_info")	
			!! @input_file OUTPUT/mean_force_info (classical_MC)
				if(parameters%algorithm/="mean_force") CYCLE
				parameters%write_mean_force_info=.TRUE.
			
			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

	end subroutine classical_MC_initialize_output

!--------------------------------------------------------------

END MODULE classical_mc
