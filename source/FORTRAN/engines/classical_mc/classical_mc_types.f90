MODULE classical_mc_types
	USE kinds
    USE classical_md_types, ONLY:   CLASS_MD_PARAM, &
                                    CLASS_MD_SYS, &
                                    classical_MD_deallocate, &
                                    class_MD_sub_int
    IMPLICIT NONE

    TYPE, EXTENDS(CLASS_MD_SYS) :: CLASS_MC_SYS
        REAL(wp), allocatable :: X_prev(:,:)
        REAL(wp), allocatable :: P_prev(:,:)
        REAL(wp), allocatable :: Forces_prev(:,:)
        REAL(wp) :: MC_Energy, MC_Energy_prev

        REAL(wp) :: n_acc_proposals
        REAL(wp) :: total_proposals
        REAL(wp) :: acc_ratio

        REAL(wp), allocatable :: X_collective_prev(:)
    CONTAINS
        PROCEDURE :: update_acc_ratio => classical_MC_update_acc_ratio
        PROCEDURE :: save_config => classical_MC_save_config
        PROCEDURE :: restore_prev_config => classical_MC_restore_prev_config
        PROCEDURE :: initialize_counters => classical_MC_initialize_counters
    END TYPE

    TYPE, EXTENDS(CLASS_MD_PARAM) :: CLASS_MC_PARAM     
        CHARACTER(:), allocatable :: move_generator
        REAL(wp) :: sigma_move

        INTEGER :: n_steps_aux_lgv
        LOGICAL :: MF_momentum_flip
    END TYPE

    ABSTRACT INTERFACE
		subroutine class_MC_sub(system,parameters)
			import :: CLASS_MC_SYS, CLASS_MC_PARAM
			IMPLICIT NONE
			CLASS(CLASS_MC_SYS), intent(inout) :: system
			CLASS(CLASS_MC_PARAM), intent(inout) :: parameters
		end subroutine class_MC_sub

        function class_MC_fct_real(system,parameters) result(res)
			import :: CLASS_MC_SYS, CLASS_MC_PARAM, wp
			IMPLICIT NONE
			CLASS(CLASS_MC_SYS), intent(inout) :: system
			CLASS(CLASS_MC_PARAM), intent(inout) :: parameters
            REAL(wp) :: res
		end function class_MC_fct_real
	END INTERFACE

    PROCEDURE(class_MD_sub_int), pointer :: class_MC_update_mean_values

CONTAINS

	subroutine classical_MC_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters

		call classical_MD_deallocate(system,parameters)	

        if(allocated(system%X_prev)) &
            deallocate(system%X_prev)
        if(allocated(system%P_prev)) &
            deallocate(system%P_prev)

	end subroutine classical_MC_deallocate

    subroutine classical_MC_update_acc_ratio(system)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system

        system%acc_ratio=system%n_acc_proposals/system%total_proposals
    
    end subroutine classical_MC_update_acc_ratio

    subroutine classical_MC_initialize_counters(system)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system

        system%n_acc_proposals=0
        system%total_proposals=0   

    end subroutine classical_MC_initialize_counters

    subroutine classical_MC_save_config(system)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system

        system%X_prev=system%X
        if(allocated(system%P_prev)) &
            system%P_prev=system%P
        system%MC_Energy_prev=system%MC_Energy

        if(allocated(system%X_collective)) &
            system%X_collective_prev=system%X_collective
        if(allocated(system%Forces_prev)) &
            system%Forces_prev=system%Forces

    end subroutine classical_MC_save_config

    subroutine classical_MC_restore_prev_config(system)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system

        system%X=system%X_prev
        if(allocated(system%P_prev)) &
            system%P=system%P_prev
        system%MC_Energy=system%MC_Energy_prev

        if(allocated(system%X_collective)) &
            system%X_collective=system%X_collective_prev
        if(allocated(system%Forces_prev)) &
            system%Forces=system%Forces_prev

    end subroutine classical_MC_restore_prev_config


END MODULE classical_mc_types