MODULE classical_mc_moves
    USE kinds
    USE classical_mc_types
    USE classical_md_types, only : class_MD_update_collective_coord
    USE basic_types
    USE random
    USE classical_md, only : class_MD_integrator
    IMPLICIT NONE


CONTAINS

!----------------------------------------------
! MOVE GENERATORS

    subroutine classical_MC_gaussian_move(system,parameters)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
        REAL(wp), allocatable, save :: R(:)
        INTEGER :: i

        IF(.not. allocated(R)) allocate(R(system%n_atoms))

        do i=1,system%n_dim
            call randGaussN(R)
            system%X(:,i)=system%X(:,i) + parameters%sigma_move*R(:)
        enddo

    end subroutine classical_MC_gaussian_move

    subroutine classical_MC_langevin_move(system,parameters)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
        INTEGER :: i

        if(parameters%MF_momentum_flip) &
            system%P=-system%P

        do i=1,parameters%n_steps_aux_lgv            
            call class_MD_integrator(system,parameters)
        enddo

    end subroutine classical_MC_langevin_move

!----------------------------------------------
! ACCEPTANCE PROBABILITIES

    function classical_MC_Boltzmann_ratio(system,parameters) result(acc_probability)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: acc_probability
        
        system%MC_Energy=Pot(system%X)

        acc_probability=exp(-parameters%beta*(system%MC_Energy-system%MC_Energy_prev))

    end function classical_MC_Boltzmann_ratio

    function classical_MC_constrained_Boltzmann(system,parameters) result(acc_probability)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: acc_probability

        call class_MD_update_collective_coord(system,parameters)
        
        system%MC_Energy=Pot(system%X) + 0.5_wp* sum( &
                parameters%FE_kappa(:)*( system%X_collective(:)-system%FE_Z(:) )**2 )

        acc_probability=exp(-parameters%beta*(system%MC_Energy-system%MC_Energy_prev) )

    end function classical_MC_constrained_Boltzmann

    function classical_MC_constraint_only(system,parameters) result(acc_probability)
        IMPLICIT NONE
        CLASS(CLASS_MC_SYS), INTENT(inout) :: system
		CLASS(CLASS_MC_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: acc_probability

        call class_MD_update_collective_coord(system,parameters)
        
        system%MC_Energy= 0.5_wp * sum( &
                parameters%FE_kappa(:)*( system%X_collective(:)-system%FE_Z(:) )**2 )

        acc_probability=exp(-parameters%beta*(system%MC_Energy-system%MC_Energy_prev) )

    end function classical_MC_constraint_only

!------------------------------------------------


END MODULE classical_mc_moves