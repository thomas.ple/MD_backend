MODULE ring_polymer_md
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE file_handler
	USE random
	USE classical_md, ONLY:	classical_MD_get_parameters, &
							classical_MD_initialize_FE_mean_force, &
							classical_MD_initialize_dipole
	USE classical_md_types, ONLY: classical_MD_write_mean_force_info, &
																classical_MD_compute_dipole
	USE rpmd_types
	USE rpmd_integrators
	USE string_operations
	USE thermostats, only : initialize_thermostat, finalize_thermostat &
												, thermostat_end_thermalization
	IMPLICIT NONE	

	TYPE(POLYMER_TYPE), save, target :: RPMD_system
	TYPE(RPMD_PARAM), save, target :: RPMD_parameters

	PROCEDURE(RPMD_sub), pointer :: RPMD_integrator

	PRIVATE :: RPMD_system, RPMD_parameters, RPMD_integrator

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine RPMD_loop()
		IMPLICIT NONE
		INTEGER :: i
		REAL(wp) :: E_kin_sum

		! THERMALIZATION
		if(RPMD_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",RPMD_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,RPMD_parameters%n_steps_therm
				if(RPMD_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				call RPMD_integrator(RPMD_system,RPMD_parameters)

				! UPDATE CENTROID VARIABLE
				call RPMD_system%update_centroid()

				if(RPMD_parameters%compute_E_kin) &
					call RPMD_compute_kinetic_energy(RPMD_system,RPMD_parameters)
				call RPMD_print_system_info(RPMD_system,RPMD_parameters,i)
			ENDDO
			write(*,*) "Thermalization done."
			call RPMD_write_restart_file(RPMD_system,RPMD_parameters, 0)
			call thermostat_end_thermalization(RPMD_system,RPMD_parameters)
		endif

		write(*,*)
		write(*,*) "Starting ",RPMD_parameters%n_steps," steps &
						&of RPMD"
		! START PRODUCTION
		DO i=1,RPMD_parameters%n_steps
			! CHECK IF EXIT FILE
			if(RPMD_parameters%check_exit_file()) then
				call RPMD_write_restart_file(RPMD_system,RPMD_parameters &
								,RPMD_parameters%n_steps_prev+i)
				write(0,*) "I/O: EXIT file found. Writing RESTART and exiting properly."
				RETURN
			endif

			! UPDATE TIME
			RPMD_system%time=RPMD_system%time &
								+RPMD_parameters%dt

			! INTEGRATION STEP
			call RPMD_integrator(RPMD_system,RPMD_parameters)

			! UPDATE CENTROID VARIABLE
			call RPMD_system%update_centroid()

			! COMPUTE MEAN VALUES
			if(RPMD_parameters%on_the_fly_mean_values) &
				call RPMD_update_mean_values(RPMD_system,RPMD_parameters,i)		

			
			if(RPMD_parameters%compute_dipole) then
				
				if(mod(i,RPMD_parameters%compute_dipole_stride)==0) &
					call classical_MD_compute_dipole(RPMD_system,RPMD_parameters)
			endif	

			! COMPUTE KINETIC ENERGY
			if(RPMD_parameters%compute_E_kin) &
				call RPMD_compute_kinetic_energy(RPMD_system,RPMD_parameters)

			! COMPUTE MEAN ENERGIES AND PRINT INFO 
			call RPMD_print_system_info(RPMD_system,RPMD_parameters,i)
			! if(RPMD_parameters%write_mean_energy_info) then				
			! 	if(RPMD_parameters%print_temperature_stride>0) then
			! 		if(mod(i,RPMD_parameters%print_temperature_stride)==0) then
			! 			E_kin_sum=SUM(RPMD_system%E_kin_vir_mean)
			! 			write(*,*)
			! 			write(*,*) "----------------------------------------------"
			! 			write(*,*) "STEP "//int_to_str(i)
			! 			write(*,*) "SYSTEM INFO (last "//int_to_str(RPMD_parameters%print_temperature_stride)//" steps):"
			! 			write(*,*) "  temperature:" &
			! 									,RPMD_system%temp_estimated*kelvin, "K"
			! 			write(*,*) "  potential_energy:" &
			! 									,RPMD_system%E_pot_mean*kcalpermol, "kCal/mol"
			! 			write(*,*) "  kinetic_energy_virial:" &
			! 									,E_kin_sum*kcalpermol, "kCal/mol"
			! 			write(*,*) "  total_energy:" &
			! 									,(RPMD_system%E_pot_mean+E_kin_sum)*kcalpermol, "kCal/mol"
			! 		else
			! 			call RPMD_update_mean_energies(RPMD_system,RPMD_parameters &
			! 											,mod(i,RPMD_parameters%print_temperature_stride))
			! 		endif
			! 	else
			! 		call RPMD_update_mean_energies(RPMD_system,RPMD_parameters,i)
			! 	endif
			! endif

			! WRITE TRAJECTORY
			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(RPMD_parameters%write_restart) then
				if(mod(i,RPMD_parameters%restart_stride)==0) then
					call RPMD_write_restart_file(RPMD_system,RPMD_parameters &
									,RPMD_parameters%n_steps_prev+i)
				endif
			endif

		ENDDO

		call RPMD_write_restart_file(RPMD_system,RPMD_parameters &
									,RPMD_parameters%n_steps_prev+RPMD_parameters%n_steps)

		if(allocated(RPMD_system%FE_mean_force)) then
			write(*,*) "mean force=", RPMD_system%FE_mean_force*bohr
			write(*,*) "std dev force=", RPMD_system%FE_std_dev_force*bohr
			if(RPMD_parameters%write_mean_force_info) &
				call classical_MD_write_mean_force_info(RPMD_system,RPMD_parameters)
		endif
		if(RPMD_parameters%write_mean_energy_info) &
			call RPMD_write_mean_energy_info(RPMD_system,RPMD_parameters)
		write(*,*) "Simulation done."

	end subroutine RPMD_loop
!-----------------------------------------------------------------

	subroutine initialize_RPMD(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart

		RPMD_parameters%engine="rpmd"

		call RPMD_initialize_restart_file(RPMD_system,RPMD_parameters,param_library)

		!ASSOCIATE RPMD_loop
		simulation_loop => RPMD_loop

		!GET ALGORITHM
		RPMD_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (RPMD, default="nvt")
		RPMD_parameters%algorithm=to_lower_case(RPMD_parameters%algorithm)

		call RPMD_get_parameters(RPMD_system,RPMD_parameters,param_library)		
		call RPMD_initialize_momenta(RPMD_system,RPMD_parameters)
		call RPMD_initialize_normal_modes(RPMD_system,RPMD_parameters)

		! LOAD RESTART FILE IS NEEDED
		RPMD_parameters%n_steps_prev=0
		load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
		if(load_restart) then
			call RPMD_load_restart_file(RPMD_system,RPMD_parameters)
			RPMD_parameters%n_steps_therm=0
		endif

		if(RPMD_parameters%algorithm=="mean_force") &
			call RPMD_initialize_FE_mean_force(RPMD_system,RPMD_parameters,param_library)

		call RPMD_get_integrator_parameters(RPMD_system,RPMD_parameters,param_library)
		call RPMD_initialize_forces(RPMD_system,RPMD_parameters)

		call RPMD_initialize_output(RPMD_system,RPMD_parameters,param_library)

		call RPMD_write_frequencies(RPMD_system,RPMD_parameters)

		if(RPMD_parameters%compute_dipole) &
			 call classical_MD_initialize_dipole(RPMD_system,RPMD_parameters,param_library)
	end subroutine initialize_RPMD

	subroutine finalize_RPMD()
		IMPLICIT NONE

		if(RPMD_parameters%algorithm/="nve") &
			call finalize_thermostat(RPMD_system,RPMD_parameters)

		call RPMD_deallocate(RPMD_system,RPMD_parameters)	

	end subroutine finalize_RPMD

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine RPMD_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_beads,i
		CHARACTER(:), ALLOCATABLE :: pot_name

		call classical_MD_get_parameters(system,parameters,param_library)

		n_beads=param_library%get("PARAMETERS/n_beads")
		!! @input_file PARAMETERS/n_beads (RPMD)
		if(n_beads<=0) STOP "Error: 'n_beads' is not properly defined!"
			system%n_beads=n_beads
			parameters%n_beads=n_beads		

		allocate(system%X_beads(n_beads,system%n_atoms,system%n_dim))
		allocate(system%Epot_beads(n_beads))
		do i=1,n_beads
			system%X_beads(i,:,:)=system%X(:,:)
		enddo
		allocate(system%E_kin_prim(system%n_atoms),system%E_kin_vir(system%n_atoms))
		system%E_kin_prim=0._wp
		system%E_kin_vir=0._wp

		if(parameters%algorithm /= "nve") then
			parameters%dBeta=parameters%beta/system%n_beads
		endif

		parameters%parallel_forces=.FALSE.
		pot_name=param_library%get("POTENTIAL/pot_name")
		if(to_upper_case(trim(pot_name))=="SOCKET") parameters%parallel_forces=.TRUE.

		parameters%n_threads=1
		!GET OPENMP NUMBER OF THREADS
		!$ parameters%n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
		!$ write(*,*) "Parallel RPMD with n_threads=",parameters%n_threads

	end subroutine RPMD_get_parameters

	subroutine RPMD_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j

		allocate(system%P(system%n_atoms,system%n_dim))
		allocate(system%P_beads(system%n_beads,system%n_atoms,system%n_dim))

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt","mean_force")

			!initialize momenta
			do j=1,system%n_dim
				do i=1,system%n_atoms
					call randGaussN(system%P_beads(:,i,j))
					system%P_beads(:,i,j)=system%P_beads(:,i,j) &
							*SQRT(system%mass(i)/parameters%dBeta)
				enddo
			enddo
			call system%update_centroid()

		CASE("nve")
			system%P=0._wp
			system%P_beads=0._wp

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine RPMD_initialize_momenta

	subroutine RPMD_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default="baoab")
		!! @input_file PARAMETERS/integrator (RPMD, default="baoab")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt", "mean_force")

			call initialize_thermostat(system,parameters,param_library)

			SELECT CASE(parameters%integrator_type)
			CASE("baoab")
				RPMD_integrator => RPMD_BAOAB
			CASE("aboba")
				RPMD_integrator => RPMD_ABOBA
			CASE("pile-l")
				RPMD_integrator => RPMD_PILE_L
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT
			
		CASE("nve")
			SELECT CASE(parameters%integrator_type)
			CASE("velocity_verlet")
				RPMD_integrator => RPMD_velocity_verlet
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine RPMD_get_integrator_parameters


	subroutine RPMD_initialize_normal_modes(polymer,parameters)
		implicit none
		CLASS(POLYMER_TYPE), INTENT(inout) :: polymer
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		integer :: i,j,k,INFO,nu
		real(wp),allocatable :: WORK(:)
		
		nu=polymer%n_beads

		allocate(parameters%EigMat(nu,nu),parameters%EigMatTr(nu,nu) &
				, parameters%Omk(nu))
		allocate(polymer%EigX(nu,polymer%n_atoms,polymer%n_dim))
		allocate(polymer%EigP(nu,polymer%n_atoms,polymer%n_dim))

		allocate(WORK(3*nu-1))
		
		parameters%EigMat=0

		do i=1,nu-1
			parameters%EigMat(i,i)=2
			parameters%EigMat(i,i+1)=-1
			parameters%EigMat(i+1,i)=-1
		enddo
		parameters%EigMat(1,nu)=-1
		parameters%EigMat(nu,1)=-1
		parameters%EigMat(nu,nu)=2
		
		call DSYEV('V','U',nu,parameters%EigMat,nu,parameters%Omk,WORK,3*nu-1,INFO)		
		if(INFO/=0) then
			STOP "Error: could not find the polymer normal modes!"
		endif
		
		parameters%Omk(1)=0
		parameters%Omk(:)=sqrt(parameters%Omk(:))/parameters%dBeta
		
		!COMPUTE INVERSE TRANSFER MATRIX
		parameters%EigMatTr=transpose(parameters%EigMat)
		if(nu==1) write(*,*) "EigMat=",parameters%EigMat
		
		deallocate(WORK)
		
		
	end subroutine RPMD_initialize_normal_modes


!-------------------------------------------------------------------

	subroutine RPMD_initialize_FE_mean_force(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_initialize_FE_mean_force(system,parameters,param_library)
	!	parameters%FE_kappa=parameters%FE_kappa*system%n_beads

		RPMD_update_mean_values => RPMD_update_mean_force
		call RPMD_update_collective_coord(system,parameters)

		parameters%Omk=parameters%Omk/sqrt(real(system%n_beads,wp))

		write(*,*) "Mean force calculation => sampling at beta and not beta/nu !"

	end subroutine RPMD_initialize_FE_mean_force

!-------------------------------------------------------------------


	subroutine RPMD_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description, basename,default_name
		INTEGER :: i_file, i,j
		CHARACTER(3) :: i_char
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%compute_E_kin=.FALSE.
		parameters%write_mean_force_info=.FALSE.
		parameters%write_mean_energy_info=.FALSE.
		parameters%compute_dipole=.FALSE.

		! call output%create_file("X_1.traj",formatted)
		!  	call output%add_column_to_file(system%time,unit="ps")
		!  	call output%add_array_to_file(system%X_beads(1,:,1),unit="angstrom")

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("centroid_position")
			!! @input_file OUTPUT/centroid_position (RPMD)
				description="trajectory of the centroid position"				

				if(system%n_dim==1) then
					default_name="X_centroid.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X(:,1),index=i_file,name="centroid position")
				else
					default_name="centroid_position.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,name="centroid position")
				endif

				call output%files(i_file)%edit_description(description)
				

			CASE("centroid_momentum")
			!! @input_file OUTPUT/centroid_momentum (RPMD)
				description="trajectory of the centroid momentum"	

				if(system%n_dim==1) then
					default_name="P_centroid.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%P(:,1),index=i_file,name="centroid momentum")
				else
					default_name="centroid_momentum.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%P,system%element_symbols,index=i_file,name="centroid momentum")
				endif
				call output%files(i_file)%edit_description(description)

			CASE("e_kin_prim")
			!! @input_file OUTPUT/e_kin_prim (RPMD)
				description="trajectory of the primitive estimator of kinetic energy"
				parameters%compute_E_kin=.TRUE.
				default_name="E_kin_prim.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%E_kin_prim(:),index=i_file,name="primitive estimator of kinetic energy")
			
			CASE("e_kin_vir")
			!! @input_file OUTPUT/e_kin_vir (RPMD)
				description="trajectory of the virial estimator of kinetic energy"
				parameters%compute_E_kin=.TRUE.
				default_name="E_kin_vir.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%E_kin_vir(:),index=i_file,name="virial estimator of kinetic energy")

			
			CASE("potential_energy","e_pot")	
			!! @input_file OUTPUT/potential_energy (RPMD)
				description="trajectory of beads potential energy"
				default_name="potential_energy.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%Epot_beads(:),index=i_file,name="potential energy")
			
			CASE("beads_positions")
			!! @input_file OUTPUT/beads_positions (RPMD)
				description="trajectory of the bead "	

				if(system%n_dim==1) then			
					basename=dict%get("name",default="X_beads.traj.")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char)
						call output%create_file(dict,basename//i_char,index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						if(output%files(i_file)%has_time_column) &
							call output%add_column_to_file(system%time,unit="ps",name="time")
						call output%add_array_to_file(system%X_beads(j,:,1),name="position of bead "//i_char)
					ENDDO
				else
					basename=dict%get("name",default="beads_pos_")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char//".xyz")
						call output%create_file(dict,basename//i_char//".xyz",index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						call output%add_xyz_to_file(system%X_beads(j,:,:),system%element_symbols &
													,index=i_file,name="position of bead "//i_char)
					ENDDO
				endif
			
			CASE("forces")
			!! @input_file OUTPUT/beads_positions (RPMD)
				description="trajectory of the force on bead "	

				if(system%n_dim==1) then			
					basename=dict%get("name",default="Forces.traj.")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char)
						call output%create_file(dict,basename//i_char,index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						if(output%files(i_file)%has_time_column) &
							call output%add_column_to_file(system%time,unit="ps",name="time")
						call output%add_array_to_file(system%F_beads(j,:,1),name="force on bead "//i_char)
					ENDDO
				else
					basename=dict%get("name",default="beads_forces_")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char//".xyz")
						call output%create_file(dict,basename//i_char//".xyz",index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						call output%add_xyz_to_file(system%F_beads(j,:,:),system%element_symbols &
													,index=i_file,name="force on bead "//i_char)
					ENDDO
				endif

			CASE("dipole","dipole_moment")	
				parameters%compute_dipole=.TRUE.
				parameters%compute_dipole_stride=dict%get("stride",default=1)
			!! @input_file OUTPUT/dipole (classical_MD)
				description="trajectory of the dipole moment"
				default_name="dipole.traj"
				
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole,index=i_file,name="dipole")

				description="trajectory of the dipole moment velocity"
				default_name="dipole_velocity.traj"
				call dict%store("name",default_name)
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)
				

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole_velocity,index=i_file,name="dipole_velocity")

			CASE("mean_force_info")	
			!! @input_file OUTPUT/mean_force_info (RPMD)
				if(parameters%algorithm/="mean_force") CYCLE
				parameters%write_mean_force_info=.TRUE.
			
			CASE("mean_energy_info")	
			!! @input_file OUTPUT/mean_energy_info (RPMD)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
			
			CASE("print_system_info")	
			!! @input_file OUTPUT/print_system_info (RPMD)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
				parameters%print_temperature_stride=dict%get("stride",default=100)
				
			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

		if(parameters%write_mean_energy_info) then
			allocate(system%E_kin_prim_mean(system%n_atoms),system%E_kin_vir_mean(system%n_atoms))
			allocate(system%E_kin_prim_var(system%n_atoms),system%E_kin_vir_var(system%n_atoms))
			system%E_kin_prim_mean=0._wp
			system%E_kin_vir_mean=0._wp
			system%E_kin_prim_var=0._wp
			system%E_kin_vir_var=0._wp
		endif

	end subroutine RPMD_initialize_output

END MODULE ring_polymer_md
