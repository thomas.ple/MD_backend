MODULE rpmd_integrators
    USE rpmd_types
    USE random
    USE basic_types
	USE thermostats, ONLY : PI_thermostat_get_evolved_P, apply_PI_thermostat,fast_forward_langevin
	USE pi_lgv_thermostat, ONLY : apply_centroid_flip_ffl
	USE pi_qtb_thermostat, ONLY: pi_qtb_update_forces
	USE socket_potential, only :parallel_socket_pot_info
    IMPLICIT NONE

	PRIVATE :: apply_A,apply_B,apply_AOA
	
	PROCEDURE(RPMD_sub), pointer :: RPMD_compute_forces => null()

CONTAINS

    ! INTEGRATORS

	subroutine RPMD_velocity_verlet(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), intent(inout) :: parameters

		call apply_B(system,parameters,parameters%dt/2._wp)
		call apply_A(system,parameters,parameters%dt)

		call RPMD_compute_forces(system,parameters)
		call apply_B(system,parameters,parameters%dt/2._wp)

	end subroutine RPMD_velocity_verlet

	subroutine RPMD_BAOAB(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), intent(inout) :: parameters


		CALL apply_B(system,parameters,parameters%dt/2._wp)

		! CALL apply_A(system,parameters,parameters%dt/2._wp)
		! CALL apply_PI_thermostat(system,parameters)
		! CALL apply_A(system,parameters,parameters%dt/2._wp)
		CALL apply_AOA(system,parameters)

		call RPMD_compute_forces(system,parameters)

		CALL apply_B(system,parameters,parameters%dt/2._wp)	

	end subroutine RPMD_BAOAB

	subroutine RPMD_ABOBA(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), intent(inout) :: parameters

		CALL apply_A(system,parameters,parameters%dt)!/2._wp)		
		call RPMD_compute_forces(system,parameters)

		CALL apply_B(system,parameters,parameters%dt/2._wp)		
		CALL apply_PI_thermostat(system,parameters)
		CALL apply_B(system,parameters,parameters%dt/2._wp)	
	!	CALL apply_A(system,parameters,parameters%dt/2._wp)

	end subroutine RPMD_ABOBA

	subroutine RPMD_PILE_L(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), intent(inout) :: parameters

		CALL apply_B(system,parameters,parameters%dt/2._wp)		
		CALL apply_A(system,parameters,parameters%dt)

		call RPMD_compute_forces(system,parameters)
		CALL apply_B(system,parameters,parameters%dt/2._wp)	
		CALL apply_PI_thermostat(system,parameters)

	end subroutine RPMD_PILE_L

	subroutine apply_A(polymer,parameters,tau)
		!$ USE OMP_LIB
		IMPLICIT NONE		
		CLASS(POLYMER_TYPE), intent(inout) :: polymer
		CLASS(RPMD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp), ALLOCATABLE, SAVE :: EigX0(:,:), EigP0(:,:)
		INTEGER :: k,i,j,i_thread

		if(.not. allocated(EigX0)) then
			allocate(EigX0(polymer%n_beads,parameters%n_threads), EigP0(polymer%n_beads,parameters%n_threads))
		endif
		
		i_thread=1
	!	!$OMP PARALLEL FIRSTPRIVATE(i_thread,j,k,i)
	!		!$ i_thread = OMP_GET_THREAD_NUM()+1
	!	!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms

			EigP0(:,i_thread)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
			EigX0(:,i_thread)=matmul(parameters%EigMatTr,polymer%X_beads(:,j,k))

			polymer%EigX(1,j,k)=EigX0(1,i_thread)+tau*EigP0(1,i_thread)/polymer%mass(j)
			polymer%EigP(1,j,k)=EigP0(1,i_thread)

			do i=2,polymer%n_beads
				polymer%EigX(i,j,k)=EigX0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					+EigP0(i,i_thread)*sin(parameters%Omk(i)*tau)/(polymer%mass(j)*parameters%Omk(i))

				polymer%EigP(i,j,k)=EigP0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					-polymer%mass(j)*parameters%Omk(i)*EigX0(i,i_thread)*sin(parameters%Omk(i)*tau)
			enddo
			
			polymer%X_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigX(:,j,k))
			polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))

		enddo ; enddo
	!	!$OMP END DO
	!	!$OMP END PARALLEL

	end subroutine apply_A

	subroutine apply_B(polymer,parameters,tau)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: polymer
		CLASS(RPMD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,polymer%n_beads
			polymer%P_beads(i,:,:)=polymer%P_beads(i,:,:) + tau*polymer%F_beads(i,:,:)
		enddo
		
	end subroutine apply_B

	subroutine apply_AOA(polymer,parameters)
		!$ USE OMP_LIB
		implicit none
		CLASS(POLYMER_TYPE), intent(inout) :: polymer
		CLASS(RPMD_PARAM), intent(inout) :: parameters
		INTEGER :: i,j,k
		REAL(wp) :: tau
		REAL(wp) :: EigX0, EigP0
		REAL(wp), ALLOCATABLE :: Pc0(:,:)
		
		if(parameters%thermostat_type == "qtb") &
			call pi_qtb_update_forces(polymer,parameters)
		
    ALLOCATE(Pc0(polymer%n_atoms,polymer%n_dim))
		
		tau=parameters%dt/2._wp
		

		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms
			polymer%EigP(:,j,k)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
			polymer%EigX(:,j,k)=matmul(parameters%EigMatTr,polymer%X_beads(:,j,k))

			!APPLY A (dt/2)
			polymer%EigX(1,j,k)=polymer%EigX(1,j,k)+tau*polymer%EigP(1,j,k)/polymer%mass(j)

			do i=2,polymer%n_beads
				EigX0=polymer%EigX(i,j,k)
				EigP0=polymer%EigP(i,j,k)
				polymer%EigX(i,j,k)=EigX0*cos(parameters%Omk(i)*tau) &
					+EigP0*sin(parameters%Omk(i)*tau)/(polymer%mass(j)*parameters%Omk(i))

				polymer%EigP(i,j,k)=EigP0*cos(parameters%Omk(i)*tau) &
					-polymer%mass(j)*parameters%Omk(i)*EigX0*sin(parameters%Omk(i)*tau)
			enddo

			Pc0(j,k)=polymer%EigP(1,j,k)
			!APPLY O (dt)
			polymer%EigP(:,j,k)=PI_thermostat_get_evolved_P(polymer,parameters,j,k)
			! call RandGaussN(R)
			! EigP0(:,i_thread)=polymer%EigP(:,j,k)*parameters%RPMD_damping_exp(:) &
			! 						+ parameters%RPMD_sigma(:,j)*R(:)	
		enddo ; enddo

		if(fast_forward_langevin) call apply_centroid_flip_ffl(polymer,Pc0)
		DEALLOCATE(Pc0)

		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms
			!APPLY A (dt/2)
			polymer%EigX(1,j,k)=polymer%EigX(1,j,k)+tau*polymer%EigP(1,j,k)/polymer%mass(j)

			do i=2,polymer%n_beads
				EigX0=polymer%EigX(i,j,k)
				EigP0=polymer%EigP(i,j,k)
				polymer%EigX(i,j,k)=EigX0*cos(parameters%Omk(i)*tau) &
					+EigP0*sin(parameters%Omk(i)*tau)/(polymer%mass(j)*parameters%Omk(i))

				polymer%EigP(i,j,k)=EigP0*cos(parameters%Omk(i)*tau) &
					-polymer%mass(j)*parameters%Omk(i)*EigX0*sin(parameters%Omk(i)*tau)
			enddo

			polymer%X_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigX(:,j,k))
			polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))

		enddo ; enddo
		
	end subroutine apply_AOA

!------------------------------------------------------------------
! FORCES CALCULATORS

	subroutine RPMD_initialize_forces(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters

		! ASSIGN FORCES CALCULATOR	
		if(parameters%algorithm == "mean_force") then
			RPMD_compute_forces => RPMD_potential_constrained
		elseif(parameters%parallel_forces) then
			RPMD_compute_forces => RPMD_potential_only_socket_parallel
		else
			RPMD_compute_forces => RPMD_potential_only
		endif
		allocate(system%Forces(system%n_atoms,system%n_dim))
		allocate(system%F_beads(system%n_beads,system%n_atoms,system%n_dim))
		
		call RPMD_compute_forces(system,parameters)

	end subroutine RPMD_initialize_forces

	subroutine RPMD_potential_only(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i


		!$OMP PARALLEL DO PRIVATE(i) SCHEDULE(static)
		do i=1,system%n_beads
			call get_pot_info(system%X_beads(i,:,:),system%Epot_beads(i),system%F_beads(i,:,:))
		enddo
		!$OMP END PARALLEL DO

		system%E_pot=SUM(system%Epot_beads(:)/REAL(system%n_beads,wp))		

	end subroutine RPMD_potential_only	

	subroutine RPMD_potential_only_socket_parallel(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i

		! do i=1,system%n_beads
		! 	system%F_beads(i,:,:)=-dPot(system%X_beads(i,:,:))
		! enddo
		CALL parallel_socket_pot_info(system%X_beads,system%F_beads,system%Epot_beads)

		system%E_pot=0
		do i=1,system%n_beads
			system%E_pot=system%E_pot+system%Epot_beads(i)
		enddo
		system%E_pot=system%E_pot/REAL(system%n_beads,wp)

	end subroutine RPMD_potential_only_socket_parallel

	subroutine RPMD_potential_constrained(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), intent(inout) :: system
		CLASS(RPMD_PARAM), intent(inout) :: parameters
		INTEGER :: i

		call RPMD_update_collective_coord(system,parameters)

		do i=1,system%n_beads
			system%F_beads(i,:,:)=-dPot(system%X_beads(i,:,:))
		enddo
		system%F_beads=system%F_beads/system%n_beads

		do i=1,parameters%collective_coord_size
			system%F_beads(1,:,:)= system%F_beads(1,:,:) - parameters%FE_kappa(i) * &
				(system%X_collective(i) - system%FE_Z(i))*system%X_collective_grad(i,:,:)
		enddo

		system%E_pot=0
		do i=1,system%n_beads
			system%Epot_beads(i)=Pot(system%X_beads(i,:,:))
			system%E_pot=system%E_pot+system%Epot_beads(i)
		enddo
		system%E_pot=system%E_pot/REAL(system%n_beads,wp) &
			+0.5_wp*SUM(parameters%FE_kappa(:)*(system%X_collective(:) - system%FE_Z(:))**2)

	end subroutine RPMD_potential_constrained

END MODULE rpmd_integrators    
