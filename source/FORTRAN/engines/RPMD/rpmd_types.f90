MODULE rpmd_types
	USE kinds
	USE classical_md_types, only:	CLASS_MD_SYS, CLASS_MD_PARAM &
									,classical_MD_deallocate &
									,classical_MD_update_mean_force &
									,class_MD_update_collective_coord &
									,classical_md_initialize_restart_file
	USE atomic_units
	USE file_handler, only: output
	USE string_operations, only: int_to_str
    IMPLICIT NONE

  TYPE,EXTENDS(CLASS_MD_SYS) :: POLYMER_TYPE
		INTEGER :: n_beads
		
		REAL(wp), ALLOCATABLE :: X_beads(:,:,:)
		REAL(wp), ALLOCATABLE :: P_beads(:,:,:)

		REAL(wp), ALLOCATABLE :: EigX(:,:,:)
		REAL(wp), ALLOCATABLE :: EigP(:,:,:)

		REAL(wp), ALLOCATABLE :: F_beads(:,:,:)
		REAL(wp), ALLOCATABLE :: Epot_beads(:)

		REAL(wp), ALLOCATABLE :: E_kin_prim(:), E_kin_vir(:)
		REAL(wp), ALLOCATABLE :: E_kin_prim_mean(:), E_kin_vir_mean(:)
		REAL(wp), ALLOCATABLE :: E_kin_prim_var(:), E_kin_vir_var(:)

	CONTAINS
		PROCEDURE :: update_centroid => RPMD_update_centroid
		PROCEDURE :: write_frequencies => RPMD_write_frequencies

	END TYPE

	TYPE,EXTENDS(CLASS_MD_PARAM) :: RPMD_PARAM
		INTEGER :: n_beads

		REAL(wp) :: dBeta

		REAL(wp), ALLOCATABLE :: EigMat(:,:)
		REAL(wp), ALLOCATABLE :: EigMatTr(:,:)
		REAL(wp), ALLOCATABLE :: OmK(:)

		LOGICAL :: parallel_forces
	END TYPE

	ABSTRACT INTERFACE
		subroutine RPMD_sub(system,parameters)
			import :: POLYMER_TYPE, RPMD_PARAM
			IMPLICIT NONE
			CLASS(POLYMER_TYPE), intent(inout) :: system
			CLASS(RPMD_PARAM), intent(inout) :: parameters
		end subroutine RPMD_sub

		subroutine RPMD_sub_int(system,parameters,n)
			import :: POLYMER_TYPE, RPMD_PARAM
			IMPLICIT NONE
			CLASS(POLYMER_TYPE), intent(inout) :: system
			CLASS(RPMD_PARAM), intent(inout) :: parameters
			INTEGER, intent(in) :: n
		end subroutine RPMD_sub_int
	END INTERFACE

	PROCEDURE(RPMD_sub_int), pointer :: RPMD_update_mean_values

CONTAINS

	subroutine RPMD_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters

		call classical_MD_deallocate(system,parameters)
		
		if(allocated(system%X_beads)) &
			deallocate(	system%X_beads )
		if(allocated(system%P_beads)) &
			deallocate(	system%P_beads )
		if(allocated(system%F_beads)) &
			deallocate(	system%F_beads )
		if(allocated(system%EigX)) &
			deallocate(	system%EigX )
		if(allocated(system%EigP)) &
			deallocate(	system%EigP )
		if(allocated(parameters%EigMat)) &
			deallocate(	parameters%EigMat )
		if(allocated(parameters%EigMatTr)) &
			deallocate(	parameters%EigMatTr )
		if(allocated(parameters%OmK)) &
			deallocate(	parameters%OmK )
					
	end subroutine RPMD_deallocate

	subroutine RPMD_update_centroid(system)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system

		system%X=SUM(system%X_beads,DIM=1)/system%n_beads
		system%P=SUM(system%P_beads,DIM=1)/system%n_beads

	end subroutine RPMD_update_centroid

	subroutine RPMD_write_frequencies(system,parameters,unit)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in), optional :: unit
		integer :: u,i

		if(present(unit)) then
			u=unit
		else
			u=6
		endif

		write(u,*) "#POLYMER FREQUENCIES:"
		write(u,*) 1, 0._wp,"THz (infinity fs)"
		do i=2,system%n_beads
			write(u,*) i,parameters%Omk(i)*THz/(2*pi),"THz (",2*pi*fs/parameters%Omk(i),"fs)"
		enddo

	end subroutine RPMD_write_frequencies

!------------------------------------------------
	subroutine RPMD_compute_kinetic_energy(system,parameters)
		IMPLICIT NONE
    CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j,k,n
		REAL(wp) :: omega_nu2

		omega_nu2=0.5_wp/parameters%dbeta**2
		n=0
		system%E_kin_prim=0._wp
		system%E_kin_vir=0._wp

		DO j=1,system%n_dim ; do i=1,system%n_atoms
			n=n+1
			!PRIMITIVE ESTIMATOR
			DO k=1,system%n_beads-1
				system%E_kin_prim(i)=system%E_kin_prim(i) &
					+system%P_beads(k,i,j)**2/(2._wp*system%mass(i)) &
					-(system%mass(i)*omega_nu2)*(system%X_beads(k+1,i,j)-system%X_beads(k,i,j))**2
			ENDDO
			system%E_kin_prim(i)=system%E_kin_prim(i) &
					+system%P_beads(system%n_beads,i,j)**2/(2._wp*system%mass(i)) &
					-(system%mass(i)*omega_nu2)*(system%X_beads(1,i,j)-system%X_beads(system%n_beads,i,j))**2
			!system%E_kin_prim(n)=system%mass(i)*(system%E_kin_prim(n)+(system%X_beads(1,i,j)-system%X_beads(system%n_beads,i,j))**2)

			!VIRIAL ESTIMATOR
			system%E_kin_vir(i)=system%E_kin_vir(i)+SUM((system%X_beads(:,i,j)-system%X(i,j))*system%F_beads(:,i,j))
		ENDDO ; ENDDO

		!system%E_kin_prim=(0.5_wp/parameters%dbeta)*(1._8 - system%E_kin_prim/parameters%beta)
		system%E_kin_prim=system%E_kin_prim/real(system%n_beads,wp)

		if(parameters%thermostat_type=="qtb") then
			system%E_kin_vir=-0.5_wp*system%E_kin_vir/REAL(system%n_beads,wp)
			n=0
			DO j=1,system%n_dim ; DO i=1,system%n_atoms
				n=n+1
				system%E_kin_vir(i)=system%E_kin_vir(i)+system%P(i,j)**2/(2._wp*system%mass(i))
			ENDDO ; ENDDO
		else
			system%E_kin_vir=system%n_dim*0.5_wp/parameters%beta-0.5_wp*system%E_kin_vir/REAL(system%n_beads,wp)
		endif


	end subroutine RPMD_compute_kinetic_energy

	subroutine RPMD_print_system_info(system,parameters,i)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		REAL(wp) :: E_kin_sum
		INTEGER, INTENT(in) :: i

		if(parameters%write_mean_energy_info) then				
				call RPMD_update_mean_energies(system,parameters &
								,parameters%counter_mean_energy_info)
				parameters%counter_mean_energy_info= &
					parameters%counter_mean_energy_info+1

				E_kin_sum=SUM(system%E_kin_vir_mean)

				if(parameters%print_temperature_stride>0) then
					if(parameters%counter_mean_energy_info &
							>parameters%print_temperature_stride) then
						write(*,*)
						write(*,*) "----------------------------------------------"
						write(*,*) "STEP "//int_to_str(i)
						write(*,*) "SYSTEM INFO (last "//int_to_str(parameters%print_temperature_stride)//" steps):"
						write(*,*) "  temperature: " &
												,system%temp_estimated*kelvin, "K"
						write(*,*) "  potential_energy: " &
												,system%E_pot_mean*kcalpermol, "kCal/mol"
						write(*,*) "  kinetic_energy_virial: " &
												,E_kin_sum*kcalpermol, "kCal/mol"
						write(*,*) "  total_energy: " &
												,(system%E_pot_mean+E_kin_sum)*kcalpermol, "kCal/mol"
						write(*,*)

						parameters%counter_mean_energy_info=1
						system%E_kin_vir_mean=0
						system%temp_estimated=0
						system%E_pot_mean=0
					endif
				endif
				
			endif
	end subroutine RPMD_print_system_info

!------------------------------------------------
! COLLECTIVE COORDINATES

	subroutine RPMD_update_collective_coord(system,parameters)
		IMPLICIT NONE
        CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters

		system%X(:,:)=system%X_beads(1,:,:)

		call class_MD_update_collective_coord(system,parameters)

	end subroutine RPMD_update_collective_coord

!-------------------------------------------------
! UPDATE MEAN VALUES

	subroutine RPMD_update_mean_force(system,parameters,n)
        IMPLICIT NONE
        CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n

		system%X(:,:)=system%X_beads(1,:,:)

		call classical_MD_update_mean_force(system,parameters,n)
    
    end subroutine RPMD_update_mean_force

	subroutine RPMD_update_mean_energies(system,parameters,n)
        IMPLICIT NONE
        CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		REAL(wp) :: tmp(system%n_atoms)
		INTEGER, INTENT(in) :: n

		tmp(1)=system%E_pot-system%E_pot_mean
		system%E_pot_mean=system%E_pot_mean &
			+ (system%E_pot - system%E_pot_mean)/real(n,wp)
		system%E_pot_var=system%E_pot_var+tmp(1)*(system%E_pot-system%E_pot_mean)
		
		tmp=system%E_kin_vir-system%E_kin_vir_mean
		system%E_kin_vir_mean=system%E_kin_vir_mean &
			+ (system%E_kin_vir - system%E_kin_vir_mean)/real(n,wp)
		system%E_kin_vir_var=system%E_kin_vir_var+tmp(:)*(system%E_kin_vir-system%E_kin_vir_mean)
		
		tmp=system%E_kin_prim-system%E_kin_prim_mean
		system%E_kin_prim_mean=system%E_kin_prim_mean &
			+ (system%E_kin_prim - system%E_kin_prim_mean)/real(n,wp)
		system%E_kin_prim_var=system%E_kin_prim_var+tmp(:)*(system%E_kin_prim-system%E_kin_prim_mean)

		system%temp_estimated = SUM(system%E_kin_vir_mean)*2._wp/system%n_dof
    
    end subroutine RPMD_update_mean_energies

	subroutine RPMD_write_mean_energy_info(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u
		REAL(wp) :: n

		n=real(parameters%n_steps-1,wp)
		OPEN(newunit=u,file=output%working_directory//"/mean_energy_info.out")
			write(u,*) "# E_pot    E_kin_vir(:)    E_kin_prim(:)"
			write(u,*) "# var(E_pot)    var(E_kin_vir)(:)   var(E_kin_prim)(:)"
			write(u,*) system%E_pot_mean, system%E_kin_vir_mean(:), system%E_kin_prim_mean(:)
			write(u,*) system%E_pot_var/n, system%E_kin_vir_var(:)/n, system%E_kin_prim_var(:)/n
		CLOSE(u)		
    
    end subroutine RPMD_write_mean_energy_info

!-----------------------------------------------------------------
! RESTART FILE

	subroutine RPMD_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_initialize_restart_file(system,parameters,param_library)
		! parameters%restart_stride=param_library%get("PARAMETERS/save_stride",default=-1)
		! !! @input_file PARAMETERS/save_stride (classical_MD,default=-1)
		! if(parameters%restart_stride>0) then
		! 	parameters%write_restart=.TRUE.
		! else
		! 	parameters%write_restart=.FALSE.
		! endif
		
	end subroutine RPMD_initialize_restart_file


	subroutine RPMD_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER, intent(in) :: n_steps
		INTEGER :: u

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			WRITE(u,*) parameters%engine
			WRITE(u,*) system%time
			WRITE(u,*) n_steps
			WRITE(u,*) system%X_beads
			WRITE(u,*) system%P_beads		
		close(u)

		write(*,*) "I/O : save config OK."

	end subroutine RPMD_write_restart_file

	subroutine RPMD_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(POLYMER_TYPE), INTENT(inout) :: system
		CLASS(RPMD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,ios
		CHARACTER(100) :: engine

		open(NEWUNIT=u,file=output%working_directory//"/RESTART", STATUS="OLD", IOSTAT=ios)
		if(ios/=0) STOP "Error: could not open RESTART file"			

			READ(u,*) engine
			if(engine/=parameters%engine) then
				write(0,*) "Error: tried to restart engine '",parameters%engine &
							,"' with a file from '",engine,"' !"
				STOP "Execution stopped"
			endif
			READ(u,*) system%time
			READ(u,*) parameters%n_steps_prev
			READ(u,*) system%X_beads
			READ(u,*) system%P_beads

		close(u)

		call system%update_centroid

		write(*,*) "I/O : configuration retrieved successfully."

	end subroutine RPMD_load_restart_file

END MODULE rpmd_types
