MODULE wigner_integrators
  USE wigner_types
	USE random
  USE basic_types
	USE wigner_forces, only : compute_wigner_forces, initialize_auxiliary_simulation
	USE nested_dictionaries
	USE matrix_operations
	USE wigner_spectrum_analysis
	USE thermostats, only: apply_thermostat
  IMPLICIT NONE

	PRIVATE :: apply_A,apply_B

	PROCEDURE(wig_sub), pointer :: wig_compute_forces => null()

CONTAINS

    ! INTEGRATORS

	subroutine wigner_ABOBA(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters

		call wig_compute_forces(system,parameters)

		CALL apply_B(system,parameters,0.5_wp*parameters%dt)	

		CALL apply_O(system,parameters,parameters%dt)
		if(parameters%compute_fdt) &
			call spectrum_analyser%update(system,parameters)

		CALL apply_B(system,parameters,0.5_wp*parameters%dt)	

		CALL wigner_update_P_from_P_packed(system)
		CALL apply_A(system,parameters,parameters%dt)

	end subroutine wigner_ABOBA

	subroutine wigner_BAOAB_classical(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		INTEGER :: i,j,c
		REAL(wp) :: sigma(system%n_atoms,system%n_dim), gamma_exp,R(system%n_atoms)
		!LOGICAL :: mask(system%n_atoms,system%n_dim)

		! gamma_exp=exp(-parameters%gamma0*parameters%dt)
		! DO i=1,system%n_dim
		! 	! sigma(:,i)=SQRT( system%mass(:)*parameters%temperature &
		! 	! 								*( 1._wp-gamma_exp*gamma_exp ) )
		! 	sigma(:,i)=SQRT( parameters%temperature &
		! 									*( 1._wp-gamma_exp*gamma_exp ) )
		! ENDDO
		
		! if(parameters%noise_correction) then
		! 	c=0
		! 	DO j=1,system%n_dim ; DO i=1,system%n_atoms
		! 		c=c+1
		! 		sigma(i,j)=sigma(i,j)-sqrt(parameters%dt*system%F_var(c))
		! 		if(sigma(i,j)<0) then
		! 			write(0,*) "Warning: noise too large on atom",i,", direction",j
		! 			sigma(i,j)=0
		! 		endif
		! 	ENDDO ; ENDDO			
		! endif
		
		CALL apply_A_change(system,parameters,0.5_wp*parameters%dt)
		!CALL apply_thermostat(system,parameters)

		if(parameters%nose_control) &
			call apply_N(system,parameters,0.5_wp*parameters%dt)

		gamma_exp=exp(-parameters%gamma0*parameters%dt)
		sigma=SQRT( parameters%temperature*( 1._wp-gamma_exp*gamma_exp ) )
		gamma_exp=gamma_exp*exp(-system%nose*parameters%dt)

		do i=1,system%n_dim		
			call randGaussN(R)
			system%P(:,i)=gamma_exp*system%P(:,i) &
							+ sigma(:,i)*R(:)
		enddo

		if(parameters%nose_control) &
			call apply_N(system,parameters,0.5_wp*parameters%dt)

		call wigner_update_P_mod_from_P(system,parameters)

		CALL apply_A_change(system,parameters,0.5_wp*parameters%dt)

		call wig_compute_forces(system,parameters)
		system%P=system%P + parameters%dt*system%Forces

		call wigner_update_P_mod_from_P(system,parameters)

	end subroutine wigner_BAOAB_classical

	subroutine apply_A(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + tau*system%P(:,i)/system%mass(:)
		enddo

	end subroutine apply_A

	subroutine apply_A_change(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + tau*system%P_mod(:,i)/system%mass(:)
		enddo

	end subroutine apply_A_change

	subroutine apply_B(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: P_half(system%n_dof),F(system%n_dof)
		INTEGER :: k

		F=system%F_cl
		if(parameters%apply_dF) F=F+system%dF
		if(parameters%apply_V_force) F=F+system%V_force
		do k=1,system%n_dof
			system%W(k)=dot_product(system%P_packed(:),matmul(system%C(:,:,k),system%P_packed(:)))
		enddo

		if(parameters%apply_p2_force) then
			
			P_half(:)=system%P_packed(:)+0.5_wp*tau*(F(:)+system%W(:))

			do k=1,system%n_dof
				system%W(k)=dot_product(P_half(:),matmul(system%C(:,:,k),P_half(:)))
			enddo
			system%P_packed(:)=system%P_packed(:)+tau*(F(:)+system%W(:))
		else
			system%P_packed(:)=system%P_packed(:)+tau*F(:)
		endif
		
	end subroutine apply_B

	subroutine apply_O(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: R(system%n_dof),sigma(system%n_dof,system%n_dof)
		INTEGER :: i

		call RandGaussN(R)

		sigma=system%sqrtk2inv
		if(parameters%noise_correction) then
			DO i=1,system%n_dof
				sigma(i,i)=sigma(i,i)-sqrt(parameters%dt*system%F_var(i))
				if(sigma(i,i)<0) then
					sigma(i,i) = 0
					write(0,*) "noise too large on dof",i
				endif
			ENDDO
		endif

		system%rand_force = sqrt(1._wp - exp(-2._wp*tau*parameters%gamma0)) &
													*matmul(sigma,R)

		system%P_packed(:) = exp(-tau*parameters%gamma0)*system%P_packed(:) &
													+ system%rand_force(:)
	end subroutine apply_O

	subroutine apply_N(system,parameters,tau)
		IMPLICIT NONE
		CLASS(WIG_SYS), intent(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp) :: temp_estimated

		temp_estimated=sum(system%P**2)
		system%nose=exp(-system%damping_nose*tau)*system%nose + (tau/system%M_nose) &
			*(temp_estimated - system%n_dof*parameters%temperature)

	end subroutine apply_N



!---------------------------------------------
! FORCES CALCULATORS

	subroutine wigner_initialize_forces(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dof
		
		! ASSIGN FORCES CALCULATOR	
		wig_compute_forces => compute_wigner_forces
		call initialize_auxiliary_simulation(system,parameters,param_library)

	end subroutine wigner_initialize_forces

END MODULE wigner_integrators    
