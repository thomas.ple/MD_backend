MODULE wigner_md
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE random
	USE wigner_types
	USE wigner_integrators
	USE classical_md, only: classical_MD_get_parameters, classical_MD_initialize_dipole
	USE wigner_forces, only: compute_EW,compute_wigner_forces
	USE wigner_spectrum_analysis
	USE thermostats, only : initialize_thermostat, finalize_thermostat, thermostat_end_thermalization
	IMPLICIT NONE	

	TYPE(WIG_SYS), save, target :: wig_system
	TYPE(WIG_PARAM), save, target :: wig_parameters

	PROCEDURE(wig_sub), pointer :: wig_integrator

	PRIVATE :: wig_system, wig_parameters

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine wigner_md_loop()
		IMPLICIT NONE
		INTEGER :: i,n_dof,n_dim,n_atoms,u
		REAL :: time_start, time_finish
	

		IF(wig_parameters%only_auxiliary_simulation) THEN
			call compute_wigner_forces(wig_system,wig_parameters)
			call save_forces_results(wig_system,wig_parameters)
			RETURN
		ENDIF


		if(wig_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",wig_parameters%n_steps_therm," steps &
						&of thermalization"
			if(wig_parameters%gamma0 /= wig_parameters%gamma0_backup) &
				write(*,*) "damping for thermalization=",wig_parameters%gamma0*THz,"THz"
			call cpu_time(time_start)
			DO i=1,wig_parameters%n_steps_therm
				if(wig_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				call wig_integrator(wig_system,wig_parameters)
				call wigner_print_system_info(wig_system,wig_parameters,i)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			if(wig_parameters%compute_fdt) &
				call spectrum_analyser%write_average(wig_system,wig_parameters, &
									"wigner_spectra.thermalization.out",.TRUE.)

			call wigner_write_restart_file(wig_system,wig_parameters,0)
		endif

		wig_parameters%gamma0 = wig_parameters%gamma0_backup
		write(*,*)
		if(wig_parameters%classical_wigner) then
			write(*,*) "Starting ",wig_parameters%n_steps," steps &
							&of classical WiLD"
		else
			write(*,*) "Starting ",wig_parameters%n_steps," steps &
							&of WiLD"
		endif
		call cpu_time(time_start)
		DO i=1,wig_parameters%n_steps

			if(wig_parameters%check_exit_file()) then
				call wigner_write_restart_file(wig_system,wig_parameters &
						,wig_parameters%n_steps_prev+i)
				write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
				RETURN
			endif

			wig_system%time=wig_system%time &
								+wig_parameters%dt

			
			call wig_integrator(wig_system,wig_parameters)
			
			call compute_EW(wig_system,wig_parameters)

			if(wig_parameters%compute_dipole) then
				
				if(mod(i,wig_parameters%compute_dipole_stride)==0) &
					call wigner_compute_dipole(wig_system,wig_parameters)
			endif	
			
			! call wigner_compute_temp_estimated(wig_system,wig_parameters)
			call wigner_print_system_info(wig_system,wig_parameters,i)

			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(wig_parameters%write_restart) then
				if(mod(i,wig_parameters%restart_stride)==0) &
					call wigner_write_restart_file(wig_system,wig_parameters &
						,wig_parameters%n_steps_prev+i)
			endif

		ENDDO
		call cpu_time(time_finish)
		write(*,*) "Simulation done in ",time_finish-time_start,"s."

		if(wig_parameters%compute_fdt) &
			call spectrum_analyser%write_average(wig_system,wig_parameters,"wigner_spectra.out")

		call wigner_write_restart_file(wig_system,wig_parameters &
					,wig_parameters%n_steps_prev+wig_parameters%n_steps)	
		

	end subroutine wigner_md_loop
!-----------------------------------------------------------------

	subroutine initialize_wigner_MD(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart,qtb_thermalization

		load_restart=.FALSE.
		wig_parameters%engine="wigner_md"

		call wigner_initialize_restart_file(wig_system,wig_parameters,param_library)

		!ASSOCIATE classicla_MD_loop
		simulation_loop => wigner_md_loop

		!GET ALGORITHM
		wig_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (wigner, default="nvt")
		wig_parameters%algorithm=to_lower_case(wig_parameters%algorithm)

		call wigner_get_parameters(wig_system,wig_parameters,param_library)
		if(.not. allocated(wig_system%Forces)) &
				allocate(wig_system%Forces(wig_system%n_atoms,wig_system%n_dim))

		IF(.not. wig_parameters%only_auxiliary_simulation) THEN
			call wigner_get_integrator_parameters(wig_system,wig_parameters,param_library)	

			call wigner_initialize_momenta(wig_system,wig_parameters)	
			wig_system%Forces=0._wp
			
			! LOAD RESTART FILE IS NEEDED
			wig_parameters%n_steps_prev=0
			load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
			if(load_restart) then
				call wigner_load_restart_file(wig_system,wig_parameters)
				wig_parameters%n_steps_therm=0
			endif
		ENDIF

		IF(.not. wig_parameters%only_auxiliary_simulation) THEN

			qtb_thermalization=param_library%get(WIGNER_CAT//"/qtb_thermalization", default=.FALSE.)
			if(qtb_thermalization) &
				call wigner_thermalize_with_QTB(wig_system,wig_parameters,param_library)

			if(wig_parameters%classical_wigner) then			

				allocate(wig_system%P_mod(wig_system%n_atoms,wig_system%n_dim))
				call initialize_thermostat(wig_system,wig_parameters,param_library)
			endif

		ENDIF

		call wigner_initialize_output(wig_system,wig_parameters,param_library)

		if(wig_parameters%compute_fdt) &
			call spectrum_analyser%initialize(wig_system,wig_parameters,param_library)

		call wigner_initialize_forces(wig_system,wig_parameters, param_library)

		IF(.not. wig_parameters%only_auxiliary_simulation) THEN
			if(wig_parameters%classical_wigner) then
				if(qtb_thermalization) then
					wig_system%P_mod=wig_system%P
					call wigner_update_P_from_P_mod(wig_system,wig_parameters)
				else
				call wigner_update_P_mod_from_P(wig_system,wig_parameters)
				endif
			endif

			if(wig_parameters%compute_dipole) then
				call classical_MD_initialize_dipole(wig_system,wig_parameters,param_library)
				call wigner_compute_dipole(wig_system,wig_parameters)
			endif
		ENDIF
		! if((.not. load_restart) .and. (.not. wig_parameters%only_auxiliary_simulation)) &
		! 	call wigner_thermalize_momenta(wig_system,wig_parameters)

	end subroutine initialize_wigner_MD

	subroutine finalize_wigner_MD()
		IMPLICIT NONE

		call wigner_deallocate(wig_system,wig_parameters)

	end subroutine finalize_wigner_MD

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine wigner_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dof

		call classical_MD_get_parameters(system,parameters,param_library)

		allocate(system%X_prev(system%n_atoms,system%n_dim))
		system%X_prev=system%X

		n_dof=system%n_dof		
		
		allocate( &
			system%rand_force(n_dof), &
			system%V_force(n_dof), &
			system%dF(n_dof), &
			system%F_cl(n_dof), &
			system%F_var(n_dof), &
			system%W(n_dof), &
			system%C(n_dof,n_dof,n_dof), &
			system%k2(n_dof,n_dof), &
			system%sqrtk2inv(n_dof,n_dof), &
			system%k2inv(n_dof,n_dof), &
			system%k2_var(n_dof,n_dof), &
			system%k4(n_dof,n_dof,n_dof,n_dof), &
			system%k2EigMat(n_dof,n_dof), &
			system%k2EigVals(n_dof), &
			system%F_change_var(n_dof) &
		)
		system%k2=0._wp
		system%k4=0._wp
		system%V_force=0._wp
		system%dF=0._wp
		system%F_cl=0._wp
		system%F_change_var=0._wp
		system%k2inv=0._wp
		system%rand_force=0._wp
		system%sqrtk2inv=0._wp

		parameters%classical_wigner = param_library%get(WIGNER_CAT//"/classical_wigner" &
												,default=.FALSE.)
		if(parameters%classical_wigner) then
			allocate(system%sqrtk2(n_dof,n_dof))
			parameters%nose_control=param_library%get(WIGNER_CAT//"/use_nose_control" &
												,default=.FALSE.)
			system%nose=0				
			system%M_nose = param_library%get(WIGNER_CAT//"/m_nose" &
												,default=1.)
			system%damping_nose = param_library%get(WIGNER_CAT//"/damping_nose" &
												,default=0.)
		endif

		parameters%only_auxiliary_simulation=param_library%get(WIGNER_CAT//"/only_auxiliary_simulation" &
												,default=.FALSE.)
		parameters%apply_p2_force=param_library%get(WIGNER_CAT//"/apply_p2_force" &
												,default=.TRUE.)
		parameters%noise_correction=param_library%get(WIGNER_CAT//"/noise_correction" &
												,default=.FALSE.)
		
		parameters%apply_dF=param_library%get(WIGNER_CAT//"/apply_df" &
												,default=.TRUE.)
		parameters%apply_V_force=param_library%get(WIGNER_CAT//"/apply_v_force" &
												,default=.TRUE.)

		system%lth2_det=product(parameters%beta/system%mass)**system%n_dim

	end subroutine wigner_get_parameters

	subroutine wigner_thermalize_with_QTB(system,parameters,param_library)
		USE classical_md_integrators, only : classical_MD_initialize_forces,class_MD_BAOAB
		USE classical_md_types, only: classical_MD_print_system_info,classical_MD_compute_kinetic_energy
		USE thermostats
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		REAL :: time_start, time_finish
		INTEGER :: i
		LOGICAL :: has_qtb_damping
		REAL(wp) :: damping_save,qtb_damping

		if(parameters%n_steps_therm > 0) then
			call param_library%store("THERMOSTAT/thermostat_type","qtb")

			has_qtb_damping=param_library%has_key(WIGNER_CAT//"/qtb_damping")
			if(has_qtb_damping) then
				damping_save = param_library%get("THERMOSTAT/damping")
				qtb_damping  = param_library%get(WIGNER_CAT//"/qtb_damping")
				call param_library%store("THERMOSTAT/damping",qtb_damping)
			endif

			call initialize_thermostat(system,parameters,param_library)
			call classical_MD_initialize_forces(system,parameters, param_library)
			
			write(*,*) "Starting ",wig_parameters%n_steps_therm," steps &
						&of QTB thermalization"
			call cpu_time(time_start)
			DO i=1,parameters%n_steps_therm
				if(wig_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				call class_MD_BAOAB(system,parameters)
				if(parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(system,parameters)
				call classical_md_print_system_info(system,parameters,i)
			ENDDO
			call cpu_time(time_finish)
			write(*,*) "Thermalization done in ",time_finish-time_start,"s."

			parameters%n_steps_therm=0
			call finalize_thermostat(system,parameters)
			if(has_qtb_damping) then
				call param_library%store("THERMOSTAT/damping",damping_save)
			endif
			call param_library%store("THERMOSTAT/thermostat_type","langevin")
			call wigner_update_P_packed_from_P(system)
		endif
	end subroutine wigner_thermalize_with_QTB

	subroutine wigner_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER :: i

		allocate(system%P(system%n_atoms,system%n_dim), &
				system%P_packed(system%n_atoms*system%n_dim))		

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt")

			!classical initialization for the moment
			if(parameters%classical_wigner) then
				do i=1,system%n_dim
					call randGaussN(system%P(:,i))
					system%P(:,i)=system%P(:,i)*SQRT(parameters%temperature)
				enddo
			else
				do i=1,system%n_dim
					call randGaussN(system%P(:,i))
					system%P(:,i)=system%P(:,i)*SQRT(system%mass(:)*parameters%temperature)
				enddo
			endif
			call wigner_update_P_packed_from_P(system)

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine wigner_initialize_momenta

	subroutine wigner_thermalize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER :: INFO, i ,j

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt")

			system%P_packed=matmul(system%sqrtk2inv,system%P_packed)
			call wigner_update_P_from_P_packed(system)

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine wigner_thermalize_momenta

!-----------------------------------------------------------------

	subroutine wigner_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: i
		CHARACTER(:), ALLOCATABLE :: default_integrator

		if(parameters%classical_wigner) then
			default_integrator = "baoab"
		else
			default_integrator = "aboba"
		endif

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default=default_integrator)
		!! @input_file PARAMETERS/integrator (wigner, default="aboba")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt")
			
			parameters%gamma0_backup=param_library%get("THERMOSTAT/damping")
			!! @input_file THERMOSTAT/damping (wigner)
			parameters%gamma0=param_library%get("THERMOSTAT/damping_thermalization" &
								,default=parameters%gamma0_backup)

			SELECT CASE(parameters%integrator_type)
			CASE("aboba")
				wig_integrator => wigner_ABOBA
			CASE("baoab")
				wig_integrator => wigner_BAOAB_classical
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT		

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine wigner_get_integrator_parameters

!-----------------------------------------------------------------

	subroutine wigner_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name
		INTEGER :: i_file, i,k
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%compute_EW = .FALSE.
		parameters%compute_dipole=.FALSE.

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("position","positions")
			!! @input_file OUTPUT/position (wigner)
				if(system%n_dim==1) then
					default_name="X.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X(:,1),index=i_file,name="positions")
				else
					default_name="position.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,name="positions")
				endif

			CASE("momentum","momenta")		
			!! @input_file OUTPUT/momentum (wigner)	
				description="trajectory of momenta"
				if(system%n_dim==1) then
					default_name="P.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					
					if(parameters%classical_wigner) then
						call output%add_array_to_file(system%P_mod(:,1),index=i_file,name="momenta")
					else
						call output%add_array_to_file(system%P(:,1),index=i_file,name="momenta")
					endif
				else
					default_name="momentum.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					if(parameters%classical_wigner) then
						call output%add_xyz_to_file(system%P_mod,system%element_symbols,index=i_file,name="momenta")
					else
						call output%add_xyz_to_file(system%P,system%element_symbols,index=i_file,name="momenta")
					endif
				endif
			
			CASE("k2_det")
				description="trajectory of k2 determinant"
				default_name="k2_det.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%k2_det,index=i_file,name="k2_det")
				call output%add_column_to_file(system%sqrtk2inv_det,index=i_file,name="sqrtk2inv_det")

			
			CASE("ew","edgeworth")		
			!! @input_file OUTPUT/edgeworth (wigner)		
				parameters%compute_EW = .TRUE.
				description="trajectory of the edgeworth correction"
				default_name="EW.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%EW,index=i_file,name="EW order 4")
				if(system%n_dof==1) &
					call output%add_column_to_file(system%EW6,index=i_file,name="EW order 6")
			
			CASE("temperature")		
			!! @input_file OUTPUT/e_kin (wigner)		
				description="trajectory of the estimated temperature"
				default_name="temperature.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%temp_estimated,index=i_file,name="temp_estimated")
			
			CASE("nose")		
			!! @input_file OUTPUT/e_kin (wigner)		
				description="trajectory of the nose control variable"
				default_name="nose.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%nose,index=i_file,name="nose")
				call output%add_column_to_file(parameters%gamma0,index=i_file,name="nose")
			
			CASE("k2_var")	
			!! @input_file OUTPUT/k2_var (wigner)		
				description="trajectory of the k2 and delta**2 variances \n(only the lower triangular part in col-major)"
				default_name="k2_var.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%k2_var(k:system%n_dof,k),index=i_file,name="k2")
				ENDDO
			
			CASE("k2")		
			!! @input_file OUTPUT/k2 (wigner)		
				description="trajectory of k2 (only the lower triangular part in col-major)"
				default_name="k2.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%k2(k:system%n_dof,k),index=i_file,name="k2")
				ENDDO
			
			CASE("k2_eigvals")		
			!! @input_file OUTPUT/k2 (wigner)		
				description="trajectory of k2 eigenvalues"
				default_name="k2_eigvals.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%k2EigVals(:),index=i_file,name="k2_eigvals")
			
			CASE("k2_inv")		
			!! @input_file OUTPUT/k2 (wigner)		
				description="trajectory of k2 inverse (only the lower triangular part in col-major)"
				default_name="k2_inv.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dof
					call output%add_array_to_file(system%k2inv(k:system%n_dof,k),index=i_file,name="k2")
				ENDDO

			CASE("dforce")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of dF"
				default_name="dF.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dF,index=i_file,name="dF")
			
			CASE("f_change_var")	
				!! @input_file OUTPUT/f_var (wigner)			
					description="trajectory of F (change var)"
					default_name="F_change_var.traj"
					call output%create_file(dict,default_name,index=i_file)
					call output%files(i_file)%edit_description(description)
	
					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%F_change_var,index=i_file,name="dF")
			
			CASE("v_force")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of V force"
				default_name="V_force.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%V_force,index=i_file,name="v_f")

			CASE("f_clwild")	
			!! @input_file OUTPUT/f_var (wigner)	
				if(.not. parameters%classical_wigner)	CYCLE
				description="trajectory of force for the classical wigner"
				default_name="F_clwild.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_dim
					call output%add_array_to_file(system%Forces(:,k),index=i_file,name="F_clWiLD")
				ENDDO
			
			CASE("force")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of classical force"
				default_name="F_classical.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%F_cl,index=i_file,name="F_cl")
			
			CASE("p2_force")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of W (prop to p**2)"
				default_name="p2_force.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%W,index=i_file,name="W")
			
			CASE("rand_force")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of random force"
				default_name="F_rand.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%rand_force(:),index=i_file,name="F_rand")
			
			CASE("f_var")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of F variance"
				default_name="F_var.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%F_var,index=i_file,name="F_var")
			
			CASE("acceptance_ratio","acc_ratio")	
			!! @input_file OUTPUT/f_var (wigner)			
				description="trajectory of acceptance ratio"
				default_name="acc_ratio.traj"
				system%acc_ratio=0
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%acc_ratio,index=i_file,name="acc_ratio")
			
			CASE("dipole","dipole_moment")	
				parameters%compute_dipole=.TRUE.
				parameters%compute_dipole_stride=dict%get("stride",default=1)
			!! @input_file OUTPUT/dipole (classical_MD)
				description="trajectory of the dipole moment"
				default_name="dipole.traj"
				
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole,index=i_file,name="dipole")

				description="trajectory of the dipole moment velocity"
				default_name="dipole_velocity.traj"
				call dict%store("name",default_name)
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)
				

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole_velocity,index=i_file,name="dipole_velocity")
			
			CASE("dyn_corr")
			!! @input_file OUTPUT/dyn_corr (wigner)	
				if(system%n_dof==1) then
					parameters%compute_EW = .TRUE.
					description="trajectory of the correction for correlation functions"
					default_name="dyn_corr.traj"
					call output%create_file(dict,default_name,index=i_file)
					call output%files(i_file)%edit_description(description)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_column_to_file(system%beta_dU,index=i_file,name="beta*dU/dq")
					call output%add_column_to_file(system%dk2,index=i_file,name="dk2/dq")
					call output%add_column_to_file(system%dC4,index=i_file,name="dC4/dq")
					call output%add_column_to_file(system%C4_beta_dU,index=i_file,name="C4*beta*dU/dq")
					call output%add_column_to_file(system%C4,index=i_file,name="C4")
				endif
			
			CASE("print_system_info")
			!! @input_file OUTPUT/print_system_info(wigner)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
				parameters%keep_potential=.TRUE.
				parameters%print_temperature_stride=dict%get("stride",default=100)
			
			CASE("check_fdt")
			!! @input_file OUTPUT/print_system_info(wigner)
				parameters%compute_fdt = .TRUE.
				parameters%fdt_stride=dict%get("stride",default=2000)

			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO


		IF(parameters%compute_E_kin) THEN
			ALLOCATE(system%E_kin_mean(system%n_dof) &
							,system%E_kin(system%n_dof))
			system%E_kin_mean=0
			system%E_kin=0
			system%temp_estimated=0
			system%temp_k2=0
			system%E_kin_k2=0
			system%E_kin_k2_mean=0
			system%pressure_tensor_mean=0
			system%E_pot_mean=0
			system%E_pot=0
		ENDIF



	end subroutine wigner_initialize_output

END MODULE wigner_md