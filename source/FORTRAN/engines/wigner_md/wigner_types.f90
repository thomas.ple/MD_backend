MODULE wigner_types
	USE kinds
	USE classical_md_types
	USE nested_dictionaries
	USE file_handler, only: output
	USE atomic_units
	USE string_operations
  IMPLICIT NONE

	CHARACTER(*), PARAMETER :: WIGNER_CAT="WIGNER_AUX"

  TYPE, EXTENDS(CLASS_MD_SYS) :: WIG_SYS

		REAL(wp), allocatable :: X_prev(:,:)
		REAL(wp), allocatable :: P_packed(:)
		REAL(wp), allocatable :: P_mod(:,:)

		REAL(wp) :: EW
		REAL(wp) :: EW6 !ONLY FOR 1D SYSTEMS

		REAL(wp), ALLOCATABLE :: V_force(:)
		REAL(wp), ALLOCATABLE :: dF(:)
		REAL(wp), ALLOCATABLE :: F_cl(:)
		REAL(wp), ALLOCATABLE :: C(:,:,:)
		REAL(wp), ALLOCATABLE :: k2(:,:)
		REAL(wp), ALLOCATABLE :: sqrtk2inv(:,:)
		REAL(wp), ALLOCATABLE :: sqrtk2(:,:)
		REAL(wp), ALLOCATABLE :: k2inv(:,:)
		REAL(wp), ALLOCATABLE :: k4(:,:,:,:)
		REAL(wp), ALLOCATABLE :: k2_var(:,:)
		REAL(wp), ALLOCATABLE :: F_var(:)
		REAL(wp), ALLOCATABLE :: rand_force(:)
		REAL(wp), ALLOCATABLE :: F_change_var(:)

		REAL(wp), ALLOCATABLE :: k2EigMat(:,:)
		REAL(wp), ALLOCATABLE :: k2EigVals(:)
		REAL(wp), ALLOCATABLE :: W(:)

		REAL(wp) :: k2_det, sqrtk2inv_det, lth2_det
		
		!ONLY FOR 1D SYSTEMS
		REAL(wp) :: k6,beta_dU, dk2, dC4,C4_beta_dU,C4

		REAL(wp) :: temp_k2, E_kin_k2, E_kin_k2_mean

		REAL(wp) :: acc_ratio

		REAL(wp) :: nose, M_nose, damping_nose

	END TYPE

	TYPE, EXTENDS(CLASS_MD_PARAM) :: WIG_PARAM
		REAL(wp) :: gamma0, gamma0_backup
		CHARACTER(:), ALLOCATABLE :: move_generator
		LOGICAL :: only_auxiliary_simulation
		LOGICAL :: compute_EW

		LOGICAL :: compute_fdt
		INTEGER :: fdt_stride
		LOGICAL :: apply_p2_force
		LOGICAL :: apply_dF
		LOGICAL :: apply_V_force
		LOGICAL :: noise_correction

		LOGICAL :: classical_wigner
		LOGICAL :: nose_control
	END TYPE	

	ABSTRACT INTERFACE
		subroutine wig_sub(system,parameters)
			import :: WIG_SYS, WIG_PARAM
			IMPLICIT NONE
			CLASS(WIG_SYS), intent(inout) :: system
			CLASS(WIG_PARAM), intent(inout) :: parameters
		end subroutine wig_sub

		subroutine wig_sub_int(system,parameters,n)
			import :: WIG_SYS, WIG_PARAM
			IMPLICIT NONE
			CLASS(WIG_SYS), intent(inout) :: system
			CLASS(WIG_PARAM), intent(inout) :: parameters
			INTEGER, INTENT(in) :: n
		end subroutine wig_sub_int
	END INTERFACE

CONTAINS

	subroutine wigner_update_P_from_P_packed(system)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system

		system%P=RESHAPE(system%P_packed,(/system%n_atoms,system%n_dim/))

	end subroutine wigner_update_P_from_P_packed

	subroutine wigner_update_P_packed_from_P(system)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system

		system%P_packed=RESHAPE(system%P,(/system%n_atoms*system%n_dim/))
		
	end subroutine wigner_update_P_packed_from_P

	subroutine wigner_update_P_mod_from_P(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		INTEGER :: i

		! DO i=1,system%n_dim
		! 	system%P_mod(:,i)=system%P(:,i)*sqrt(parameters%beta/system%mass(:))
		! ENDDO

		system%P_packed=RESHAPE(system%P,(/system%n_atoms*system%n_dim/))
		system%P_mod=RESHAPE(matmul(system%sqrtk2inv,system%P_packed) &
													,(/system%n_atoms,system%n_dim/))
		DO i=1,system%n_dim
			system%P_mod(:,i)=system%P_mod(:,i)*sqrt(system%mass(:))
		ENDDO
		
	end subroutine wigner_update_P_mod_from_P

	subroutine wigner_update_P_from_P_mod(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), intent(inout) :: parameters
		INTEGER :: i

		DO i=1,system%n_dim
			system%P(:,i)=system%P_mod(:,i)/sqrt(system%mass(:))
		ENDDO

		system%P_packed=matmul(system%sqrtk2, &
				RESHAPE(system%P,(/system%n_atoms*system%n_dim/)))
		system%P=RESHAPE(system%P_packed,(/system%n_atoms,system%n_dim/))
		
										
		
	end subroutine wigner_update_P_from_P_mod

	subroutine wigner_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters

		call classical_MD_deallocate(system,parameters)
		!!! TO COMPLETE !!! 

	end subroutine wigner_deallocate

	! subroutine wigner_compute_temp_estimated(system,parameters)
	! 	IMPLICIT NONE
	! 	CLASS(WIG_SYS), INTENT(inout) :: system
	! 	CLASS(WIG_PARAM), INTENT(inout) :: parameters

	! 	system%temp_estimated=parameters%temperature*dot_product(system%P_packed &
	! 			,matmul(system%k2,system%P_packed))/real(system%n_dof,wp)
		
	! 	system%temp_natural=(system%P_packed &
	! 			,matmul(system%k2,system%P_packed))/real(system%n_dof,wp)

	! end subroutine wigner_compute_temp_estimated

	subroutine wigner_compute_kinetic_energy(system,parameters)
		IMPLICIT NONE
    CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j,n

		

		if(parameters%classical_wigner) then
			n=0
			DO j=1,system%n_dim ; do i=1,system%n_atoms
				n=n+1
				system%E_kin(n)=0.5_wp*system%P(i,j)**2
			ENDDO ; ENDDO
			system%E_kin_k2=0
			DO j=1,system%n_dim ; do i=1,system%n_atoms
				system%E_kin_k2=system%E_kin_k2+system%P_mod(i,j)**2/(2._wp*system%mass(i))
			ENDDO ; ENDDO
		else
			n=0
			DO j=1,system%n_dim ; do i=1,system%n_atoms
				n=n+1
				system%E_kin(n)=system%P(i,j)**2/(2._wp*system%mass(i))
			ENDDO ; ENDDO
			system%E_kin_k2 = parameters%temperature*0.5_wp*dot_product(system%P_packed &
				,matmul(system%k2,system%P_packed))
		endif

	end subroutine wigner_compute_kinetic_energy

	subroutine wigner_update_mean_energies(system,parameters,n)
		IMPLICIT NONE
    CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n

		system%E_kin_mean=system%E_kin_mean+(system%E_kin-system%E_kin_mean)/real(n,wp)
		system%E_kin_k2_mean=system%E_kin_k2_mean+(system%E_kin_k2-system%E_kin_k2_mean)/real(n,wp)
		system%temp_estimated = SUM(system%E_kin_mean)*2._wp/system%n_dof
		system%temp_k2 = system%E_kin_k2_mean*2._wp/system%n_dof
		system%E_pot_mean = system%E_pot_mean +(system%E_pot - system%E_pot_mean)/real(n,wp)

	end subroutine wigner_update_mean_energies

	subroutine wigner_print_system_info(system,parameters,i)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: i		

		if(parameters%write_mean_energy_info) then
				call wigner_compute_kinetic_energy(system,parameters)

				call wigner_update_mean_energies(system,parameters &
							,parameters%counter_mean_energy_info)
				parameters%counter_mean_energy_info= &
						parameters%counter_mean_energy_info+1

				if(parameters%print_temperature_stride>0) then
					if(parameters%counter_mean_energy_info &
							>parameters%print_temperature_stride) then
						write(*,*)
						write(*,*) "----------------------------------------------"
						write(*,*) "STEP "//int_to_str(i)
						write(*,*) "SYSTEM INFO (last "//int_to_str(parameters%print_temperature_stride)//" steps):"
						write(*,*) "  temperature_natural: " &
												,system%temp_estimated*kelvin, "K"
						write(*,*) "  temperature_k2: " &
												,system%temp_k2*kelvin, "K"
						write(*,*) "  kinetic_energy_natural: " &
												,SUM(system%E_kin_mean)*kcalpermol, "kCal/mol"
						write(*,*) "  kinetic_energy_k2: " &
												,system%E_kin_k2_mean*kcalpermol, "kCal/mol"
						write(*,*) "  potential_energy: " &
												,system%E_pot_mean*kcalpermol, "kCal/mol"
						write(*,*)

						parameters%counter_mean_energy_info=1
						system%E_kin_k2_mean=0
						system%E_kin_mean=0
						system%temp_k2=0
						system%temp_estimated=0
						system%E_pot_mean=0
					endif				
				endif
			endif
	end subroutine wigner_print_system_info


	subroutine wigner_compute_dipole(system,parameters)
		IMPLICIT NONE
   	CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j

		system%dipole=0._wp
		system%dipole_velocity=0._wp
		if(parameters%classical_wigner) then		
			DO j=1,system%n_dim ; DO i=1,system%n_atoms
				system%dipole(j)=system%dipole(j) &
						+system%charges(i)*system%X(i,j)/system%n_atoms

				system%dipole_velocity(j)=system%dipole_velocity(j) &
						+system%charges(i)*system%P_mod(i,j)/(system%mass(i)*system%n_atoms)
			ENDDO ; ENDDO
		else
			DO j=1,system%n_dim ; DO i=1,system%n_atoms
				system%dipole(j)=system%dipole(j) &
						+system%charges(i)*system%X(i,j)/system%n_atoms

				system%dipole_velocity(j)=system%dipole_velocity(j) &
						+system%charges(i)*system%P(i,j)/(system%mass(i)*system%n_atoms)
			ENDDO ; ENDDO
		endif
	
	end subroutine wigner_compute_dipole

!----------------------------------------------------------
! RESTART FILE

	subroutine wigner_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		call classical_MD_initialize_restart_file(system,parameters,param_library)
		
	end subroutine wigner_initialize_restart_file

	subroutine wigner_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(in) :: system
		CLASS(WIG_PARAM), INTENT(in) :: parameters
		INTEGER, intent(in) :: n_steps
		INTEGER :: u

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			WRITE(u,*) parameters%engine
			WRITE(u,*) system%time
			WRITE(u,*) n_steps
			WRITE(u,*) system%X
			WRITE(u,*) system%P		
		close(u)

		write(*,*) "I/O : save config OK."

	end subroutine wigner_write_restart_file

	subroutine wigner_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER :: u
		CHARACTER(100) :: engine

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			READ(u,*) engine
			if(engine/=parameters%engine) then
				write(0,*) "Error: tried to restart engine '",parameters%engine &
							,"' with a file from '",engine,"' !"
				STOP "Execution stopped"
			endif
			READ(u,*) system%time
			READ(u,*) parameters%n_steps_prev
			READ(u,*) system%X
			READ(u,*) system%P		
		close(u)

		write(*,*) "I/O : configuration retrieved successfully."

	end subroutine wigner_load_restart_file

!-------------------------------------------------------

	subroutine save_forces_results(system,parameters)
		IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(in) :: system
		CLASS(WIG_PARAM), INTENT(in) :: parameters
		INTEGER :: u,u2, n_dim, n_dof, n_atoms, i,j

		n_dof=system%n_dof
		n_dim=system%n_dim
		n_atoms=system%n_atoms
		open(newunit=u,file=output%working_directory//"/k2.mat")
		open(newunit=u2,file=output%working_directory//"/dk2.mat")
		write(*,*)
		write(*,*) "--------------------"
		write(*,*) "--------------------"
		if(n_dof == 1) then
			write(*,*) "k2=",system%k2
			write(u,*) system%k2
			write(u2,*) system%C
		else
			DO i=1,n_dof
				write(u,*) REAL(system%k2(i,:))
				DO j=1,n_dof
					write(u2,*) REAL(system%C(j,:,i))
				ENDDO
				write(u2,*)
			ENDDO
			write(*,*) "k2_x="
			DO i=1,n_atoms
				write(*,*) REAL(system%k2(i,1:n_atoms))
			ENDDO
			if(n_dim>1) then
				write(*,*)
				write(*,*) "k2_y="
				DO i=n_atoms+1,2*n_atoms
					write(*,*) REAL(system%k2(i,n_atoms+1:2*n_atoms))
				ENDDO
				if(n_dim>2) then
					write(*,*)
					write(*,*) "k2_z="
					DO i=2*n_atoms+1,3*n_atoms
						write(*,*) REAL(system%k2(i,2*n_atoms+1:3*n_atoms))
					ENDDO
				endif
			endif
		endif
		close(u)
	!	write(*,*) "sqrt(var(k2))/k2=",system%k2_var!*RESHAPE(system%k2,(/n_dof*n_dof/))
		write(*,*) "--------------------"
		write(*,*) "--------------------"
		write(*,*)
	end subroutine save_forces_results




END MODULE wigner_types