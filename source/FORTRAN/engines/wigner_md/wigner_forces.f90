MODULE wigner_forces
    USE kinds
    USE wigner_types
    USE rpmd_types
    USE nested_dictionaries
    USE string_operations
    USE atomic_units, only : convert_to_unit
    USE basic_types, only : Pot,dPot,get_pot_info
    USE matrix_operations, only : sym_mat_diag,fill_permutations,sym_mv
    USE random, only: RandGaussN
    USE unbiased_products, only: unbiased_product
    USE socket_potential, only :parallel_socket_pot_info
    USE timer_module
    IMPLICIT NONE

    INTEGER, SAVE :: NUM_AUX_SIM=3

    TYPE :: WIG_POLYMER_TYPE
        INTEGER :: n_beads

        REAL(wp), ALLOCATABLE :: X(:,:,:)
        REAL(wp), ALLOCATABLE :: P(:,:,:)
        REAL(wp), ALLOCATABLE :: F_beads(:,:,:)
        REAL(wp), ALLOCATABLE :: F_beads_prev(:,:,:)
        REAL(wp), ALLOCATABLE :: Pot_beads(:)
        REAL(wp), ALLOCATABLE :: Pot_beads_prev(:)

        REAL(wp), ALLOCATABLE :: Delta(:,:)
        REAL(wp), ALLOCATABLE :: Delta_packed(:)

        REAL(wp), ALLOCATABLE :: X_prev(:,:,:)
        REAL(wp), ALLOCATABLE :: P_prev(:,:,:)
        REAL(wp), ALLOCATABLE :: Delta_prev(:,:)
        REAL(wp) :: MC_ener
        REAL(wp) :: MC_acc_ratio
        INTEGER :: MC_count_moves

        REAL(wp), ALLOCATABLE :: k2(:,:)
        REAL(wp), ALLOCATABLE :: k2_reweight(:,:)
		REAL(wp), ALLOCATABLE :: k2inv(:,:)
		REAL(wp), ALLOCATABLE :: k4(:,:,:,:)
		REAL(wp), ALLOCATABLE :: F(:)
		REAL(wp), ALLOCATABLE :: G(:,:,:)
        REAL(wp), ALLOCATABLE :: F_reweight(:)
		REAL(wp), ALLOCATABLE :: G_reweight(:,:,:)
        REAL(wp), ALLOCATABLE :: dF(:)

        !ONLY FOR 1D SYSTEMS
        REAL(wp) :: k6 
        REAL(wp) :: G4

        LOGICAL :: PIOUD_noise

        CHARACTER(:), ALLOCATABLE :: prev_config_filename
        INTEGER :: prev_config_unit,i_step_reweight
        REAL(wp) :: mean_weight_ratio

        INTEGER :: HMC_count
    END TYPE

    TYPE, EXTENDS(RPMD_PARAM) :: WIG_FORCE_PARAM
        REAL(wp), ALLOCATABLE :: mu(:,:,:)
        REAL(wp), ALLOCATABLE :: OUsig(:,:,:,:)
        REAL(wp), ALLOCATABLE :: expOmk(:,:,:,:)

        REAL(wp), ALLOCATABLE :: F_cl(:)

        REAL(wp), ALLOCATABLE :: sigp2_inv(:)

        ! REAL(wp), ALLOCATABLE :: muX(:,:,:),muP(:,:,:)

        CHARACTER(:), ALLOCATABLE :: move_generator
        INTEGER :: n_samples

        REAL(wp), ALLOCATABLE :: sqrt_mass(:)
        LOGICAL :: spring_force
        LOGICAL :: compute_EW

        REAL(wp) :: delta_mass_factor, sqrt_delta_mass_factor

        LOGICAL :: verbose

        LOGICAL :: k2_smooth, G_smooth
        REAL(wp) :: k2_smooth_center
        REAL(wp) :: k2_smooth_width
        REAL(wp) , ALLOCATABLE :: k2_smooth_mat(:,:)

        REAL(wp), ALLOCATABLE :: memory_F(:)
		REAL(wp), ALLOCATABLE :: memory_C(:,:,:)
		REAL(wp), ALLOCATABLE :: memory_k2(:,:)
        REAL(wp), ALLOCATABLE :: memory_sqrtk2inv(:,:)
        REAL(wp) :: memory_sum_weight
        REAL(wp) :: memory_tau
        LOGICAL :: memory_smoothing

        LOGICAL :: MC_acceptance_test
        LOGICAL :: reweight_previous_configs
        INTEGER :: n_steps_reweight

        INTEGER :: HMC_refresh_p

    END TYPE

    TYPE WORK_VARIABLES
        REAL(wp), ALLOCATABLE :: F(:)
        REAL(wp), ALLOCATABLE :: Delta2(:,:)
        REAL(wp), ALLOCATABLE :: Delta4(:,:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:),EigV(:),tmp(:)
        REAL(wp), ALLOCATABLE :: R(:), R1(:), R2(:)
    END TYPE WORK_VARIABLES

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), ALLOCATABLE, SAVE :: polymers
    TYPE(WIG_FORCE_PARAM), SAVE :: f_param
    TYPE(WORK_VARIABLES), DIMENSION(:), ALLOCATABLE, SAVE :: WORKS

    ABSTRACT INTERFACE
		subroutine move_proposal_type(system,polymer,move_accepted,WORK)
            IMPORT :: WIG_SYS,WIG_POLYMER_TYPE,WORK_VARIABLES
			IMPLICIT NONE
            TYPE(WIG_SYS), INTENT(IN) :: system
			TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
			LOGICAL, INTENT(OUT) :: move_accepted
            CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		end subroutine move_proposal_type

        subroutine update_beads_forces_type(q,polymer)
            IMPORT :: wp,WIG_POLYMER_TYPE
            implicit none
            TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
            REAL(wp), INTENT(in) :: q(:,:)
        end subroutine update_beads_forces_type
	END INTERFACE

    PROCEDURE(move_proposal_type), POINTER :: move_proposal => NULL()
    PROCEDURE(update_beads_forces_type), POINTER :: update_beads_forces => NULL()

    PRIVATE
    PUBLIC :: compute_wigner_forces, initialize_auxiliary_simulation &
                ,compute_EW

CONTAINS

!--------------------------------------------------------------
! FORCE CALCULATIONS

    subroutine compute_wigner_forces(system,parameters)
        !$ USE OMP_LIB
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        INTEGER :: i,j,INFO,i_thread,c
        TYPE(timer_type) :: timer_full, timer_elem

        if(ANY(ISNAN(system%X))) STOP "ERROR: system diverged!"

        ! UPDATE X DEPENDENT PARAMETERS        
        call get_pot_info(system%X,system%E_pot,system%Forces)
        system%F_cl=RESHAPE(system%Forces,(/ system%n_dof /))
        ! DO j=1,system%n_dim ; DO i=1,system%n_atoms
        !     f_param%muX(:,i,j)=system%X(i,j)*f_param%mu(:,2,i)
        !     f_param%muP(:,i,j)=system%X(i,j)*f_param%mu(:,1,i)
        ! ENDDO ; ENDDO


        i_thread=1
        if(f_param%verbose) then
            write(*,*)
            write(*,*) "Starting WiLD forces computation..."
            !$ write(*,*) "(parallel computation)"
            call timer_full%start()
        endif

        !$OMP PARALLEL DO LASTPRIVATE(i_thread)
        DO i=1,NUM_AUX_SIM
            !$ i_thread = OMP_GET_THREAD_NUM()+1
            if(f_param%reweight_previous_configs) &
                call reweight_previous_configs(system,parameters,polymers(i),WORKS(i_thread))            
            call sample_forces(system,parameters,polymers(i),WORKS(i_thread))
        ENDDO
        !$OMP END PARALLEL DO

        if(f_param%MC_acceptance_test) then
            system%acc_ratio = SUM(polymers(:)%MC_acc_ratio/REAL(NUM_AUX_SIM,wp))
            if(f_param%verbose) then
                write(*,*) "    mean acceptance ratio:", system%acc_ratio 
            endif
        endif

        ! COMPUTE FORCE ELEMENTS
        if(f_param%verbose) then
            write(*,*) "    Starting force elements calculation..."
            call timer_elem%start()
        endif
        CALL compute_forces_elements(system,parameters)         
        if(f_param%verbose) then
            write(*,*) "    force elements computed in",timer_elem%elapsed_time(),"s."
        endif 

        if(f_param%memory_smoothing) call memory_smoothing(system,parameters)       
        
        system%X_prev=system%X   
        if(f_param%verbose) then
            write(*,*) "WiLD forces computed in",timer_full%elapsed_time(),"s."
        endif    

    end subroutine compute_wigner_forces

    subroutine compute_forces_elements(system,parameters)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: tmp,k2k2F,F(NUM_AUX_SIM),k2(NUM_AUX_SIM),G2(NUM_AUX_SIM),k4(NUM_AUX_SIM)
        INTEGER :: i,j,k,l,p1,p2,p3,c,is,INFO,maxlocdiff(2)
        REAL(wp),ALLOCATABLE,SAVE :: k2s(:,:,:),kik1ki(:,:,:), k2_work(:,:) &
                                    ,k1Gk1(:,:),k1F(:), V(:),k1kik1kik1(:,:)  !,k1kik1F(:,:)
        REAL(wp),ALLOCATABLE :: k2_reweight(:,:),k2_diff(:,:),dk2(:,:,:),dsqrtk2inv(:,:,:)
        REAL(wp) :: Ns,tmpscal,mean_weight_ratio,weight_sum

        ! COMPUTE MEAN k2
        system%k2=0
        DO i=1,NUM_AUX_SIM
            system%k2=system%k2+polymers(i)%k2/REAL(NUM_AUX_SIM, wp)
        ENDDO

        if(f_param%reweight_previous_configs) then
            allocate(k2_reweight(system%n_dof,system%n_dof) &
                    ,k2_diff(system%n_dof,system%n_dof))
            weight_sum=0
            DO i=1,NUM_AUX_SIM
                weight_sum=weight_sum+polymers(i)%mean_weight_ratio
            ENDDO
            mean_weight_ratio=weight_sum/REAL(NUM_AUX_SIM,wp)

            if(f_param%verbose) write(*,*) "   mean weight ratio=",mean_weight_ratio

            WORKS(1)%F=polymers(NUM_AUX_SIM)%F
            if(mean_weight_ratio>0.1) then
                k2_reweight=0
                DO i=1,NUM_AUX_SIM
                    polymers(i)%k2 = (polymers(i)%k2 + polymers(i)%mean_weight_ratio*polymers(i)%k2_reweight) &
                                        / (1+polymers(i)%mean_weight_ratio)
                    polymers(i)%G = (polymers(i)%G + polymers(i)%mean_weight_ratio*polymers(i)%G_reweight) &
                                         / (1+polymers(i)%mean_weight_ratio)
                    polymers(i)%F = (polymers(i)%F + polymers(i)%mean_weight_ratio*polymers(i)%F_reweight) &
                                         / (1+polymers(i)%mean_weight_ratio)
                    k2_reweight=k2_reweight + polymers(i)%k2/REAL(NUM_AUX_SIM,wp)
                ENDDO
            endif
             write(257,*) k2_reweight(1,1),system%k2(1,1),WORKS(1)%F(1),polymers(NUM_AUX_SIM)%F(1)
             FLUSH(257)
            if(mean_weight_ratio>0.1) system%k2=k2_reweight

        endif



        if(f_param%compute_EW) then
            ! COMPUTE MEAN k4 AND k6
            system%k4=0
            system%k6=0
            DO i=1,NUM_AUX_SIM
                system%k6=system%k6+polymers(i)%k6
                system%k4=system%k4+polymers(i)%k4
            ENDDO
            system%k6=system%k6/REAL(NUM_AUX_SIM, wp)
            system%k4=system%k4/REAL(NUM_AUX_SIM, wp)
        endif
        

        !----------------------------------------------------------------
        ! DO NOT COMPUTE THE OTHERS IF NOT ENOUGH TRAJECTORIES
        IF(NUM_AUX_SIM<3) RETURN ! SHOULD NOT BE ALLOWED FOR DYNAMICS
        !----------------------------------------------------------------

        system%k2_var=0
        ! COMPUTE VARIANCE OF ELEMENTS OF k2
        DO i=1,NUM_AUX_SIM
            system%k2_var=system%k2_var+(system%k2-polymers(i)%k2)**2/REAL(NUM_AUX_SIM,wp)
        ENDDO
        system%k2_var=system%k2_var/REAL(NUM_AUX_SIM-1,wp)


        f_param%k2_smooth_mat=1._wp
        if(f_param%k2_smooth) then
            call smooth_k2(system,parameters)   
            system%k2=system%k2*f_param%k2_smooth_mat
            DO i=1,NUM_AUX_SIM
                polymers(i)%k2=polymers(i)%k2*f_param%k2_smooth_mat
            ENDDO
           ! if(f_param%G_smooth) call smooth_G(system,parameters)
        endif

        !----------------------------------------------------------------
        ! COMPUTE DYNAMICAL CORRECTIONS IF 1 DOF

        if(system%n_dof==1) then
            
            DO i=1,NUM_AUX_SIM
                F(i)=polymers(i)%F(1)*f_param%beta
                k2(i)=polymers(i)%k2(1,1)
                if(f_param%compute_EW) &
                    k4(i)=polymers(i)%k4(1,1,1,1)
                G2(i)=polymers(i)%G(1,1,1)
            ENDDO

            system%beta_dU=-f_param%beta*SUM(F(:))/real(NUM_AUX_SIM,wp)
 
            system%dk2= SUM(G2(:))/real(NUM_AUX_SIM,wp) &
                        -unbiased_product(k2(:),F(:))

            k2k2F=unbiased_product(k2(:),k2(:),F(:))

            if(f_param%compute_EW) then
                system%C4=system%k4(1,1,1,1) - 3._wp*unbiased_product(k2(:),k2(:))
                system%C4_beta_dU=-unbiased_product(k4(:),F(:)) &
                                    +3._wp*k2k2F
                system%dC4= SUM(polymers(:)%G4)/real(NUM_AUX_SIM,wp) &
                            +system%C4_beta_dU + 3._wp*k2k2F &                        
                            -6._wp*unbiased_product(k2(:),G2(:))
            endif
        endif

        
       
        !----------------------------------------------------------------
        ! COMPUTE FORCES ELEMENTS (F AND C) WITH BIAS CORRECTION

        If(.NOT. allocated(k2s)) &
            allocate(k2s(f_param%n_dof,f_param%n_dof,NUM_AUX_SIM-1) &
                    ,kik1ki(f_param%n_dof,f_param%n_dof,NUM_AUX_SIM-1) &
                    ,k1kik1kik1(f_param%n_dof,f_param%n_dof) &
                    ,k1Gk1(f_param%n_dof,f_param%n_dof) &
                    ,k1F(f_param%n_dof) &
                   ! ,k1kik1F(f_param%n_dof,NUM_AUX_SIM-1) &
                    ,k2_work(f_param%n_dof,f_param%n_dof) &
                    ,V(f_param%n_dof) &
                )

        ! if(parameters%classical_wigner) then
        !     ! system%F_change_var=0
        !     ! DO i=1,NUM_AUX_SIM
        !     !     system%F_change_var = system%F_change_var + polymers(i)%F/REAL(NUM_AUX_SIM,wp)
        !     ! ENDDO
        !     ! system%Forces = RESHAPE(k1F, (/system%n_atoms,system%n_dim/))
        !     !system%F_change_var = k1F
        !     ! c=0
        !     ! DO j=1,system%n_dim; do i=1,system%n_atoms
        !     !     c=c+1
        !     !     system%F_change_var(c)=system%F_change_var(c)/system%mass(i)
        !     ! ENDDO ; ENDDO

        !     system%F_var=0
        !     DO p1=1,NUM_AUX_SIM
        !         system%F_var = system%F_var + (polymers(p1)%F - k1F)**2/REAL(NUM_AUX_SIM,wp)
        !     ENDDO
        !     system%F_var = system%F_var / REAL(NUM_AUX_SIM-1,wp)
        ! endif


        ! DIVIDE BY MASS
        DO is=1,NUM_AUX_SIM
            c=0
            DO j=1,system%n_dim ; DO i=1,system%n_atoms
                c=c+1
                polymers(is)%F(c)=polymers(is)%F(c)*f_param%beta/system%mass(i)
                !polymers(is)%dF(c)=polymers(is)%dF(c)/system%mass(i)
                polymers(is)%G(:,:,c)=polymers(is)%G(:,:,c)*f_param%beta/system%mass(i)* f_param%k2_smooth_mat
            ENDDO ; ENDDO            
        ENDDO
        Ns=real(NUM_AUX_SIM-1,wp)
        system%C=0
        V=0
        system%V_force=0
        system%dF=0

        ! !CHECK NEW ESTIMATOR VARIANCES
        ! k2_work=0
        ! k1F=0
        ! DO i=1,NUM_AUX_SIM
        !     k2_work(:,1)=k2_work(:,1)+polymers(i)%F/REAL(NUM_AUX_SIM,wp)
        !     k1F=k1F+polymers(i)%dF/REAL(NUM_AUX_SIM,wp)
        ! ENDDO
        ! k1Gk1=0
        ! V=0
        ! DO i=1,NUM_AUX_SIM
        !     k1Gk1(:,1)=k1Gk1(:,1)+(polymers(i)%F-k2_work(:,1))**2/REAL(NUM_AUX_SIM,wp)
        !     V=V+(polymers(i)%dF-k1F)**2/REAL(NUM_AUX_SIM,wp)
        ! ENDDO
       ! write(*,*) k1Gk1(:,1)/V

        ! COMPUTE FORCES ON UNBIASED PRODUCTS
        DO p1=1,NUM_AUX_SIM
            c=0
            k2_work=0
            !! STORE THE k2 THAT ARE NOT p1 AND COMPUTE THE MEAN VALUE
            DO p2=1,p1-1
                c=c+1
                k2s(:,:,c)=polymers(p2)%k2
                k2_work=k2_work+polymers(p2)%k2/Ns
            ENDDO
            DO p2=p1+1,NUM_AUX_SIM
                c=c+1
                k2s(:,:,c)=polymers(p2)%k2
                k2_work=k2_work+polymers(p2)%k2/Ns
            ENDDO
            !! COMPUTE k2inv ON THE NUM_AUX_SIM-1 SAMPLES     
            !WORKS(1)%Delta2=sym_mat_inverse(k2_work,INFO)
            !IF(INFO/=0) STOP "Error: could not compute k2inv !"

            call sym_mat_diag(k2_work,system%k2EigVals,system%k2EigMat,INFO)
            IF(INFO/=0) STOP "Error: could not diagonalize k2 !"
            if(ANY(system%k2EigVals<0)) write(0,*) "Error: k2 is not positive definite!"
            WORKS(1)%Delta2=0
            DO i=1,system%n_dof
               WORKS(1)%Delta2(i,i)=1._wp/system%k2EigVals(i)
            ENDDO
            WORKS(1)%Delta2 =matmul(system%k2EigMat,matmul(WORKS(1)%Delta2,transpose(system%k2EigMat)))
            !WORKS(1)%Delta2 = WORKS(1)%Delta2 *f_param%k2_smooth_mat

            !!COMPUTE <k2i k2inv k2i>
            kik1ki=0
            k1kik1kik1=0
            do is=1,NUM_AUX_SIM-1
                kik1ki(:,:,is)=matmul(WORKS(1)%Delta2, &
                                matmul(k2s(:,:,is),WORKS(1)%Delta2) )
                k1kik1kik1=k1kik1kik1 + matmul(kik1ki(:,:,is),k2s(:,:,is))/Ns
            enddo
            k1kik1kik1=matmul(k1kik1kik1,WORKS(1)%Delta2)/(Ns-1._wp)
            !!COMPUTE CORRECTED k2inv
            polymers(p1)%k2inv=(Ns/(Ns-1._wp))* WORKS(1)%Delta2 - k1kik1kik1
            !polymers(p1)%k2inv = polymers(p1)%k2inv*f_param%k2_smooth_mat

            if(.not. parameters%classical_wigner) then
                !!COMPUTE C
                k1Gk1 = WORKS(1)%Delta2*(Ns+1_wp)/(Ns-1._wp) - k1kik1kik1
                !k1Gk1 = k1Gk1 * f_param%k2_smooth_mat      
            ! k1F=matmul(WORKS(1)%Delta2,polymers(p1)%F)
            ! WORKS(1)%F=(k1F*(Ns+1_wp)-matmul(WORKS(1)%Delta2,matmul(kik1ki,k1F)))/(Ns-1._wp)
                WORKS(1)%F=matmul(k1Gk1,polymers(p1)%F)
                ! DO is=1,NUM_AUX_SIM-1
                !     k1Gk1=matmul(WORKS(1)%Delta2,matmul(k2s(:,:,is),WORKS(1)%Delta2)) * f_param%k2_smooth_mat
                !     k1kik1F(:,is)=matmul(k1Gk1,polymers(p1)%F)/(Ns*(Ns-1._wp))
                ! ENDDO

                DO j=1,system%n_dof                
                    DO k=1, system%n_dof
                        system%C(:,:,j)=system%C(:,:,j) &
                            -polymers(p1)%k2inv(k,j)*polymers(p1)%G(:,:,k)
                    ENDDO
                    system%C(:,:,j)=system%C(:,:,j)+k2_work*WORKS(1)%F(j)
                    ! DO is=1, NUM_AUX_SIM-1
                    !     system%C(:,:,j)=system%C(:,:,j)-k2s(:,:,is)*k1kik1F(j,is)
                    ! ENDDO
                ENDDO  
                DO is=1,NUM_AUX_SIM-1
                    k1Gk1 = kik1ki(:,:,is)! * f_param%k2_smooth_mat
                    WORKS(1)%F(:)=matmul(k1Gk1,polymers(p1)%F)/(Ns*(Ns-1._wp))
                    DO j=1,system%n_dof
                        system%C(:,:,j)=system%C(:,:,j)-k2s(:,:,is)*WORKS(1)%F(j)
                    ENDDO                
                ENDDO
            endif

            !COMPUTE V (OLD METHOD)
            V=0
            ! DO k=1,system%n_dof
            !     k1Gk1=matmul(WORKS(1)%Delta2,matmul(polymers(p1)%G(:,:,k),WORKS(1)%Delta2))/(Ns-1._wp)
            !     polymers(p1)%G(:,:,k)=k1Gk1*(Ns+2._wp) &  
            !         -matmul(WORKS(1)%Delta2,matmul(polymers(p1)%G(:,:,k),k1kik1kik1)) & 
            !         -matmul(k1kik1kik1,matmul(polymers(p1)%G(:,:,k),WORKS(1)%Delta2))
            !         ! -matmul(k1Gk1,matmul(kik1ki,WORKS(1)%Delta2)) &
            !         ! -matmul(WORKS(1)%Delta2,matmul(kik1ki,k1Gk1))
            !     k2_work=0
            !     DO is=1,NUM_AUX_SIM-1
            !         k2_work=k2_work  &
            !             +matmul(k2s(:,:,is),matmul(k1Gk1,k2s(:,:,is)))/Ns
            !     ENDDO
            !     polymers(p1)%G(:,:,k)=polymers(p1)%G(:,:,k) &
            !         -matmul(WORKS(1)%Delta2,matmul(k2_work,WORKS(1)%Delta2))
            ! ENDDO
            
            ! DO k=1,system%n_dof ; DO i=1,system%n_dof
            !     V(i)=V(i)-polymers(p1)%G(i,k,k)
            ! ENDDO ; ENDDO 
            
            !COMPUTE V (NEW METHOD)
            k1F=0
            tmpscal=-(Ns+2_wp)/(Ns-1._wp)
            WORKS(1)%Delta2=WORKS(1)%Delta2
            DO l=1,system%n_dof ; DO k=1,system%n_dof
               k1F(:)=k1F(:)+polymers(p1)%G(:,k,l)*WORKS(1)%Delta2(k,l)!*f_param%k2_smooth_mat(k,l)
            ENDDO ; ENDDO
            k1Gk1 = (-(Ns+2_wp)/(Ns-1._wp))*WORKS(1)%Delta2 + k1kik1kik1
           ! k1Gk1 = k1Gk1 * f_param%k2_smooth_mat
            V(:) = matmul(k1Gk1 , k1F)
            k1F=0
            DO l=1,system%n_dof ; DO k=1,system%n_dof
               k1F(:)=k1F(:)+polymers(p1)%G(:,k,l)*k1kik1kik1(k,l)!*f_param%k2_smooth_mat(k,l)
            ENDDO ; ENDDO
            k1Gk1 = WORKS(1)%Delta2!*f_param%k2_smooth_mat
            V(:)=V(:)+matmul(k1Gk1,k1F)

            ! k1F=0
            ! tmpscal=-(Ns+2_wp)/(Ns-1._wp)
            ! DO l=1,system%n_dof ; DO k=1,system%n_dof
            !    k1F(:)=k1F(:)+polymers(p1)%G(:,k,l)*( &
            !                     WORKS(1)%Delta2(k,l)*tmpscal &
            !                     + k1kik1kik1(k,l) )
            ! ENDDO ; ENDDO
            ! V(:)=matmul(WORKS(1)%Delta2,k1F)
            ! k1F=0
            ! DO l=1,system%n_dof ; DO k=1,system%n_dof
            !    k1F(:)=k1F(:)+polymers(p1)%G(:,k,l)*WORKS(1)%Delta2(k,l)
            ! ENDDO ; ENDDO
            ! V(:)=V(:)+matmul(k1kik1kik1,k1F)

            tmpscal=1._wp/(Ns*(Ns-1._wp))
            DO is=1,NUM_AUX_SIM-1
                k1F=0
                DO l=1,system%n_dof ; DO k=1,system%n_dof
                    k1F(:)=k1F(:)+polymers(p1)%G(:,k,l)*kik1ki(k,l,is)!*f_param%k2_smooth_mat(k,l)
                ENDDO ; ENDDO
                V(:)=V(:) + tmpscal*matmul(kik1ki(:,:,is),k1F)!*f_param%k2_smooth_mat,k1F)
            ENDDO

            !!COMPUTE F
            polymers(p1)%dF = matmul(polymers(p1)%k2inv,polymers(p1)%F)
            !polymers(p1)%F = V + polymers(p1)%dF
            system%V_force=system%V_force + (V + polymers(p1)%dF)/REAL(NUM_AUX_SIM,wp)  
            system%dF=system%dF + polymers(p1)%dF/REAL(NUM_AUX_SIM,wp)
        ENDDO

        system%dF = system%dF - system%F_cl

        if(.not. parameters%classical_wigner) then
            system%C=0.5_wp*system%C/REAL(NUM_AUX_SIM,wp)
            ! DO j=1,system%n_dof
            !     system%C(:,:,j) = system%C(:,:,j) * f_param%k2_smooth_mat(:,:)
            ! ENDDO
        endif
        
        !---------------------------------------------------
        ! COMPUTE MEAN k2 INVERSE AND SQRT(INVERSE)

        if(.not. parameters%classical_wigner) then
            call sym_mat_diag(system%k2,system%k2EigVals,system%k2EigMat,INFO)            
            IF(INFO/=0) STOP "Error: could not diagonalize k2 !"
            if(ANY(system%k2EigVals<0)) write(0,*) "Error: k2 is not positive definite!"
            system%k2inv=0
            system%sqrtk2inv=0
            system%k2_det=1._wp
            k2_work=0._wp
            DO i=1,system%n_dof
                system%k2_det=system%k2_det*system%k2EigVals(i)
                system%k2inv(i,i)=1._wp/system%k2EigVals(i)
                system%sqrtk2inv(i,i)=1._wp/sqrt(system%k2EigVals(i))
                !k2_work(i,i)=sqrt(system%k2EigVals(i))
            ENDDO
            system%k2_det=system%k2_det/system%lth2_det
            system%sqrtk2inv_det=1._wp/sqrt(system%k2_det)
            system%k2inv = matmul(system%k2EigMat,matmul(system%k2inv,transpose(system%k2EigMat)))        
            system%sqrtk2inv = matmul(system%k2EigMat,matmul(system%sqrtk2inv,transpose(system%k2EigMat)))
        ! k2_work = matmul(system%k2EigMat,matmul(k2_work,transpose(system%k2EigMat)))
        

    !    write(*,*)
     !   write(*,*) system%k2
      !  write(*,*) system%k2inv
       ! write(*,*) system%k2EigMat
        !write(*,*) matmul(system%k2,system%k2inv)

        ! COMPUTE THE CHOLESKY DECOMPOSITION OF k2 INVERSE
        ! system%sqrtk2inv=system%k2inv
        ! call DPOTRF('L',system%n_dof,system%sqrtk2inv,system%n_dof,INFO)            
        ! if(INFO/=0) then
        !     write(0,*) "Error code ",INFO,"while computing Cholesky decomposition of k2!"
        !     STOP "Execution stopped"
        ! endif
        ! !! WARNING : upper triangular elements of sigma are not necessary 0 (not referenced by LAPACK)
        ! DO j=2,system%n_dof ; DO i=1,j-1
        !     system%sqrtk2inv(i,j)=0._wp
        ! ENDDO ; ENDDO

            ! CORRECT SYSTEMATIC BIAS
            k1kik1kik1=0
            DO i=1,NUM_AUX_SIM
                k1kik1kik1=k1kik1kik1+matmul(polymers(i)%k2,matmul(system%k2inv,polymers(i)%k2))/REAL(NUM_AUX_SIM,wp)
            ENDDO
        endif

        !!! COMPUTE CHANGE OF VAR FORCES !!!
        if(parameters%classical_wigner) then
            ! COMPARE WITH ALTERNATIVE FORCE
            allocate(dk2(system%n_dof,system%n_dof,system%n_dof) &
                    ,dsqrtk2inv(system%n_dof,system%n_dof,system%n_dof))

            ! COMPUTE AND CORRECT SQRT(k2)
            system%F_change_var=0
            DO p1=1,NUM_AUX_SIM
                system%F_change_var=system%F_change_var  &
                    + polymers(p1)%F/REAL(NUM_AUX_SIM,wp)
            ENDDO
            polymers(1)%G=polymers(1)%G/REAL(NUM_AUX_SIM,wp)
            DO p1=2,NUM_AUX_SIM
                polymers(1)%G=polymers(1)%G  &
                    + polymers(p1)%G/REAL(NUM_AUX_SIM,wp)
            ENDDO

            c=0
            
            DO j=1,system%n_dim; do i=1,system%n_atoms
                c=c+1
                system%F_change_var(c)=system%F_change_var(c)*sqrt(system%mass(i))
                polymers(1)%G(:,:,c)=polymers(1)%G(:,:,c)*sqrt(system%mass(i))
                !TRANSFORM k2 to Qinv (adimensional)
                DO k=1,system%n_dim
                    system%k2((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
                        =system%k2((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
                            *sqrt(system%mass(i)*system%mass(:))/f_param%beta
                    DO l=1,system%n_dof
                        polymers(1)%G((k-1)*system%n_atoms+1:k*system%n_atoms,c,l) &
                            =polymers(1)%G((k-1)*system%n_atoms+1:k*system%n_atoms,c,l) &
                                *sqrt(system%mass(i)*system%mass(:))/f_param%beta
                    ENDDO
                ENDDO
            ENDDO ; ENDDO


            call sym_mat_diag(system%k2,system%k2EigVals,system%k2EigMat,INFO)
            IF(INFO/=0) STOP "Error: could not diagonalize Q^-1(q) !"
            system%sqrtk2=0
            system%sqrtk2inv=0
            system%k2inv=0
            system%k2_det=1._wp
            DO i=1,system%n_dof
                system%sqrtk2(i,i)=sqrt(system%k2EigVals(i))
                system%sqrtk2inv(i,i)=1._wp/sqrt(system%k2EigVals(i))
                system%k2inv=1._wp/system%k2EigVals(i)
                system%k2_det=system%k2_det*system%k2EigVals(i)
            ENDDO
            system%sqrtk2inv_det=1._wp/sqrt(system%k2_det)
            system%sqrtk2 = matmul(system%k2EigMat, &
                matmul(system%sqrtk2,transpose(system%k2EigMat)))
            system%sqrtk2inv = matmul(system%k2EigMat, &
                matmul(system%sqrtk2inv,transpose(system%k2EigMat)))
            system%k2inv = matmul(system%k2EigMat, &
                matmul(system%k2inv,transpose(system%k2EigMat)))
            ! system%sqrtk2 = ((8._wp*NUM_AUX_SIM - 9._wp)/(8._wp*(NUM_AUX_SIM-1._wp)))*system%sqrtk2 &
            ! +(1._wp/(8._wp*(NUM_AUX_SIM-1._wp)))*matmul(k1kik1kik1,system%sqrtk2inv)
            ! COMPUTE APPROXIMATE FORCE F = sqrt(k2/lth2).Fcl


            !COMPUTE d(Q^(1/2))
            V=sqrt(system%k2EigVals)
            DO k=1,system%n_dof
                dk2(:,:,k)=polymers(1)%G(:,:,k)-system%k2(:,:)*system%F_change_var(k)
                dsqrtk2inv(:,:,k)=matmul(transpose(system%k2EigMat), &
                    matmul(dk2(:,:,k),system%k2EigMat))
                DO j=1,system%n_dof ; DO i=1,system%n_dof
                    dsqrtk2inv(i,j,k)=-dsqrtk2inv(i,j,k)/((V(i)+V(j))*V(i)*V(j))
                ENDDO ; ENDDO 
                dsqrtk2inv(:,:,k) = matmul(system%k2EigMat, &
                    matmul(dsqrtk2inv(:,:,k),transpose(system%k2EigMat)))             
            ENDDO
            !COMPUTE GEOMETRIC CONTRIBUTION (prop to d(detQ))
            V=0
            DO j=1,system%n_dof
                DO l=1,system%n_dof ; DO k=1,system%n_dof 
                    V(j)=V(j)+system%k2inv(k,l)*dk2(k,l,j)
                ENDDO ; ENDDO
            ENDDO
            
            system%V_force=matmul(system%sqrtk2inv,-0.5_wp*V)

            !COMPUTE d(Q^(1/2)) contribution
            V=0
            DO j=1,system%n_dof ; DO i=1,system%n_dof
                V(i)=V(i)+dsqrtk2inv(i,j,j)
            ENDDO ; ENDDO
            !put g-WiLD force in V_force
            system%F_change_var=matmul(system%sqrtk2inv,system%F_change_var)
            write(77,*) system%F_change_var, system%V_force, V
            system%V_force = (system%V_force + V + system%F_change_var)/f_param%beta

            deallocate(dsqrtk2inv)

            !put WiLD force in F_change_var
            system%F_change_var=system%F_cl+system%dF+system%V_force

            ! !put g-WiLD force in V_force
            ! system%V_force=V
            c=0
            DO j=1,system%n_dim; do i=1,system%n_atoms
                c=c+1
                system%F_cl(c)=system%F_cl(c)/sqrt(system%mass(i))
                system%F_change_var(c)=system%F_change_var(c)/sqrt(system%mass(i))
            ENDDO ; ENDDO
            !change of variable on the classical force (not the exact g-WiLD force)
            system%F_cl = matmul(system%sqrtk2,system%F_cl)
            !change of variable on the WiLD force (not sure it is the g-WiLD force)   
            system%F_change_var = matmul(system%sqrtk2,system%F_change_var)                   

            ! V=0
            ! DO l=1,system%n_dof; DO k=1,system%n_dof
            !     DO j=1,system%n_dof                
            !         V(j)=V(j)+polymers(1)%G(j,k,l)*system%k2inv(k,l)          
            !     ENDDO
            ! ENDDO ; ENDDO

            ! system%F_change_var = matmul(system%sqrtk2inv,2*system%F_change_var-V)
            
            if(parameters%apply_V_force) then      
                !USE TRUE g-WiLD FORCE      
                system%Forces = RESHAPE(system%V_force, (/system%n_atoms,system%n_dim/))
            elseif(parameters%apply_df) then   
                !USE WiLD FORCE WITH CHANGE OF VARIABLE (not sure it is the g-WiLD force)       
                system%Forces = RESHAPE(system%F_change_var, (/system%n_atoms,system%n_dim/))
            else
                !USE CLASSICAL FORCE WITH CHANGE OF VARIABLE (not the exact g-WiLD force)
                system%Forces = RESHAPE(system%F_cl, (/system%n_atoms,system%n_dim/))
            endif 
        endif

        if(.not. parameters%classical_wigner) then
            system%sqrtk2inv = ((8._wp*NUM_AUX_SIM - 5._wp)/(8._wp*(NUM_AUX_SIM-1._wp)))*system%sqrtk2inv &
                -(3._wp/(8._wp*(NUM_AUX_SIM-1._wp)))*matmul(system%k2inv,matmul(k1kik1kik1,system%sqrtk2inv))        
        

            system%k2inv=(system%k2inv*REAL(NUM_AUX_SIM,wp) &
                -matmul(system%k2inv,matmul(k1kik1kik1,system%k2inv)))/REAL(NUM_AUX_SIM-1,wp)
            
            system%k2inv= system%k2inv!*f_param%k2_smooth_mat
            system%sqrtk2inv= system%sqrtk2inv!*f_param%k2_smooth_mat
        endif



    end subroutine compute_forces_elements

    subroutine memory_smoothing(system,parameters)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: mem_exp
        
        mem_exp = exp(-parameters%dt/f_param%memory_tau)

        f_param%memory_sum_weight = 1._wp + f_param%memory_sum_weight*mem_exp

        f_param%memory_k2 = f_param%memory_k2 * mem_exp + system%k2
        f_param%memory_sqrtk2inv = f_param%memory_sqrtk2inv * mem_exp + system%sqrtk2inv
        f_param%memory_C  = f_param%memory_C  * mem_exp + system%C

        system%sqrtk2inv = f_param%memory_sqrtk2inv / f_param%memory_sum_weight
        system%k2 = f_param%memory_k2 / f_param%memory_sum_weight
        system%C  = f_param%memory_C  / f_param%memory_sum_weight
    
    end subroutine memory_smoothing

    subroutine smooth_k2(system,parameters)
        CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_PARAM), INTENT(inout) :: parameters
        INTEGER :: i,j,k,c,n,l
        REAL(wp) :: smooth,x,mean_smooth,stdev_unbias
        ! reduce the importance of off-diagonal k2 terms where the variance is 
        ! the same or larger than the estimated value
        ! (cutoff using a fermi function)

        
        stdev_unbias=REAL(NUM_AUX_SIM-1,wp)/(NUM_AUX_SIM-1.5_wp)
        mean_smooth=0 ; c=0 ; n=0
        DO j=2,system%n_dof ; DO i=1,j-1
           ! x=sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j)) - f_param%k2_smooth_center
            !smooth=1._wp/(1._wp+exp(x/f_param%k2_smooth_width))
            smooth=1
            if(sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j)) > f_param%k2_smooth_center) &
                smooth = 0
            f_param%k2_smooth_mat(i,j)=smooth
            f_param%k2_smooth_mat(j,i)=smooth
            c=c+1
            mean_smooth=mean_smooth+(smooth-mean_smooth)/c ; 
            if(smooth<0.5) then
              !  write(*,*) "sigma/k2("//int_to_str(i)//","//int_to_str(j)//")=" &
               !                 ,sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j))
                n=n+1
            endif
            ! if(smooth<0.5 .and. f_param%verbose) &
            !     write(*,*) "Warning: hard smoothing on k2("//int_to_str(i)//","//int_to_str(j)//") &
            !                             & (",REAL(smooth),")"
            ! system%k2(i,j)=system%k2(i,j)*smooth
            ! system%k2(j,i)=system%k2(i,j)
            ! system%k2_var(i,j)=system%k2_var(i,j)*smooth*smooth
            ! system%k2_var(j,i)=system%k2_var(i,j)
            ! DO k=1,NUM_AUX_SIM
            !     polymers(k)%k2(i,j)=polymers(k)%k2(i,j)*smooth
            !     polymers(k)%k2(j,i)=polymers(k)%k2(i,j)
            !     DO l=1,system%n_dof
            !         polymers(k)%G(i,j,l)=polymers(k)%G(i,j,l)*smooth
            !         polymers(k)%G(j,i,l)=polymers(k)%G(i,j,l)
            !     ENDDO
            ! ENDDO
        ENDDO ; ENDDO

        if(mean_smooth<0.5 .AND. f_param%verbose) then
            write(*,*) "    Warning: hard smoothing on k2 => mean smoothing=",REAL(mean_smooth)
            write(*,*) "             number of smooth<0.5=",n,"/",c
        endif
    end subroutine smooth_k2

    subroutine smooth_G(system,parameters)
        CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_PARAM), INTENT(inout) :: parameters
        INTEGER :: i,j,k,c,n,l,is
        REAL(wp) :: smooth,x,mean_smooth,stdev_unbias
        REAL(wp) :: Gk_mean(system%n_dof,system%n_dof),Gk_var(system%n_dof,system%n_dof) &
                    ,delta(system%n_dof,system%n_dof)
        ! reduce the importance of off-diagonal G terms where the variance is 
        ! the same or larger than the estimated value
        ! (cutoff using a fermi function)


        
        stdev_unbias=REAL(NUM_AUX_SIM-1,wp)/(NUM_AUX_SIM-1.5_wp)
        mean_smooth=0 ; c=0 ; n=0
        DO k=1,system%n_dof
            Gk_mean=0._wp ; Gk_var = 0._wp
            DO is=1,NUM_AUX_SIM
                Gk_mean = Gk_mean + polymers(is)%G(:,:,k)/REAL(NUM_AUX_SIM,wp)
            ENDDO
            DO is=1,NUM_AUX_SIM
                Gk_var(:,:) = Gk_var(:,:) + (polymers(is)%G(:,:,k)-Gk_mean(:,:))**2/REAL(NUM_AUX_SIM,wp)
            ENDDO
            Gk_var= sqrt(Gk_var/(NUM_AUX_SIM-1.5_wp))

            delta=1._wp
            DO j=2,system%n_dof ; DO i=1,j-1
            ! x=sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j)) - f_param%k2_smooth_center
                !smooth=1._wp/(1._wp+exp(x/f_param%k2_smooth_width))
                if(Gk_var(i,j)/abs(Gk_mean(i,j)) > f_param%k2_smooth_center) then
                    delta(i,j)=0
                    delta(j,i)=0
                endif                
            ENDDO ; ENDDO
            DO is=1,NUM_AUX_SIM
                polymers(is)%G(:,:,k)=polymers(is)%G(:,:,k)*delta(:,:)
            ENDDO
        ENDDO
    end subroutine smooth_G
!--------------------------------------------------------------
! SAMPLING LOOP
    subroutine sample_forces(system,parameters,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: m,n_dof
        INTEGER :: i,j,k,l
        LOGICAL :: move_accepted

        call reinitialize_polymer_config(system,polymer,WORK)

        !THERMALIZATION
        polymer%PIOUD_noise = .FALSE.
        DO m=1,f_param%n_steps_therm
            !CALL move_proposal(system,polymer,move_accepted,WORK)
            CALL PIOUD_step(system,polymer,move_accepted,WORK)
        ENDDO

        if(f_param%MC_acceptance_test) &
            call compute_observables(system,polymer,WORK)

        !PRODUCTION
        polymer%PIOUD_noise = .TRUE.
        DO m=1,f_param%n_steps

            !GENERATE A NEW CONFIG
            CALL move_proposal(system,polymer,move_accepted,WORK)
            if(move_accepted) then
                !COMPUTE OBSERVABLES
                call compute_observables(system,polymer,WORK)
            endif

            !UPDATE MEAN VALUES
            CALL update_mean_values(polymer,m,WORK)

        ENDDO

        n_dof=system%n_dof

        !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
        DO j=2,n_dof ; DO i=1,j-1
           polymer%k2(i,j)=polymer%k2(j,i)
        ENDDO ; ENDDO
        
        !symmetrize G
		do i=1,n_dof-1 ; do j=i+1,n_dof
			do k=1,n_dof
				polymer%G(i,j,k)=polymer%G(j,i,k)
			enddo
		enddo ; enddo
		
		!symmetrize k4
        if(f_param%compute_EW) then
            do l=1,n_dof ; do k=l,n_dof ; do j=k,n_dof ; do i=j,n_dof		
                call fill_permutations(polymer%k4,i,j,k,l)
            enddo ; enddo ; enddo ; enddo
        endif

        if(f_param%reweight_previous_configs) &
            close(polymer%prev_config_unit)

    end subroutine sample_forces

!-------------------------------------------------
! UTILITY SUBROUTINES

    subroutine compute_observables(system,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j,k,l
        !REAL(wp), DIMENSION(:), POINTER :: Delta_packed
        REAL(wp) :: config_weight
        
        IF(f_param%spring_force) THEN
            DO i=1,system%n_dim
                WORK%F(system%n_atoms*(i-1)+1:system%n_atoms*i) = &
                    -f_param%sigp2_inv(:)/f_param%beta * ( &
                        2._wp*system%X(:,i)-polymer%X(1,:,i) &
                        -polymer%X(f_param%n_beads-1,:,i) &
                    )
            ENDDO
        ELSE
            WORK%F=RESHAPE( &
                    SUM(polymer%F_beads(1:f_param%n_beads-1,:,:)/real(f_param%n_beads,wp),dim=1) &
                ,(/system%n_dof/) )
        ENDIF


        if(f_param%reweight_previous_configs) then
            config_weight=compute_config_weight(system%X,polymer%Pot_beads(0) &
                ,polymer%Pot_beads(f_param%n_beads),polymer%Delta &
                ,polymer%X(1,:,:),polymer%X(f_param%n_beads-1,:,:))

            WRITE(polymer%prev_config_unit) config_weight,polymer%Pot_beads(0) &
                    ,polymer%Pot_beads(f_param%n_beads) &
                    ,polymer%Delta,polymer%X(1,:,:) &
                    ,polymer%X(f_param%n_beads-1,:,:),WORK%F
        endif

        WORK%F=WORK%F+ RESHAPE( ( polymer%F_beads(0,:,:)+ polymer%F_beads(f_param%n_beads,:,:)&
                            )/real(f_param%n_beads,wp) ,(/system%n_dof/) )

        polymer%Delta_packed=RESHAPE(polymer%Delta,(/system%n_dof/))
     !   Delta_packed => polymer%Delta

        !COMPUTE ONLY THE LOWER TRIANGULAR PART
		do j=1,system%n_dof ; do i=j,system%n_dof
			WORK%Delta2(i,j)=polymer%Delta_packed(i)*polymer%Delta_packed(j)
		enddo ; enddo

        if(f_param%compute_EW) then
            do l=1,system%n_dof ; do k=l,system%n_dof ;	do j=k,system%n_dof ; do i=j,system%n_dof
                WORK%Delta4(i,j,k,l)=polymer%Delta_packed(i)*polymer%Delta_packed(j) &
                                *polymer%Delta_packed(k)*polymer%Delta_packed(l)
            enddo ; enddo ; enddo ; enddo
        endif

        !WORK%dF = matmul(WORK%Delta2,system%F_cl)
    !     l=0
    !     DO j=1,system%n_dim; DO i=1,system%n_atoms
    !         l=l+1
    !         !WORK%dF(l)=system%mass(i)*WORK%dF(l)
    !         WORK%dF(l)=WORK%F(l)*f_param%beta/system%mass(i)
    !     ENDDO ; ENDDO
        
    !    ! WORK%dF = WORK%F - WORK%dF
    !     WORK%dF = WORK%dF -  sym_mv(WORK%Delta2,system%F_cl)

    end subroutine compute_observables

    subroutine update_mean_values(polymer,count,WORK)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER, INTENT(in) :: count
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: k
        
        if(f_param%compute_EW) then
            polymer%k6=polymer%k6 + (polymer%Delta(1,1)**6-polymer%k6)/count
            polymer%k4=polymer%k4 + (WORK%Delta4-polymer%k4)/count	
            polymer%G4=polymer%G4 + (WORK%F(1)*WORK%Delta4(1,1,1,1)-polymer%G4)/count	
        endif	

        polymer%k2=polymer%k2 + (WORK%Delta2-polymer%k2)/count
		polymer%F=polymer%F + (WORK%F-polymer%F)/count        
		DO k=1,f_param%n_dof
			polymer%G(:,:,k)=polymer%G(:,:,k) &
                +(WORK%Delta2*WORK%F(k)-polymer%G(:,:,k))/count
		ENDDO
        ! polymer%dF = polymer%dF + (WORK%dF-polymer%dF)/count    

    end subroutine update_mean_values
!--------------------------------------------------------------
! MOVE GENERATORS
    
    !-----------------------------------
    ! PIOUD steps
    subroutine PIOUD_step(system,polymer,move_accepted,WORK)
		IMPLICIT NONE
        TYPE(WIG_SYS), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, INTENT(out) :: move_accepted
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		integer :: i,j,nu,k

		nu=f_param%n_beads

        !CALL apply_forces(polymer,0.5_wp*f_param%dt)

            do j=1,system%n_dim ; do i=1,system%n_atoms
                !TRANSFORM IN NORMAL MODES	
                WORK%EigX(:)=polymer%X(1:nu,i,j)*f_param%sqrt_mass(i)
                WORK%EigX(nu)=f_param%sqrt_delta_mass_factor*WORK%EigX(nu)
                ! WORK%EigX=matmul(f_param%EigMatTr,WORK%EigX)
                call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigX,1,0._8,WORK%tmp,1)
                WORK%EigX=WORK%tmp
                WORK%EigV=polymer%P(1:nu,i,j)/f_param%sqrt_mass(i)
                WORK%EigV(nu)=WORK%EigV(nu)/f_param%sqrt_delta_mass_factor
                ! EigV=matmul(EigMatTr,EigV)
                call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigV,1,0._8,WORK%tmp,1)
                WORK%EigV=WORK%tmp	

                !GENERATE RANDOM VECTORS
                CALL RandGaussN(WORK%R1)
                CALL RandGaussN(WORK%R2)

                !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
                polymer%P(:,i,j)=WORK%EigV(:)*f_param%expOmk(:,1,1,i) &
                    +WORK%EigX(:)*f_param%expOmk(:,1,2,i) &
                    +f_param%OUsig(:,1,1,i)*WORK%R1(:) &
                    +system%X(i,j)*f_param%mu(:,1,i)

                polymer%X(1:nu,i,j)=WORK%EigV(:)*f_param%expOmk(:,2,1,i) &
                    +WORK%EigX(:)*f_param%expOmk(:,2,2,i) &
                    +f_param%OUsig(:,2,1,i)*WORK%R1(:)+f_param%OUsig(:,2,2,i)*WORK%R2(:) &
                    +system%X(i,j)*f_param%mu(:,2,i)
                
                
                !TRANSFORM BACK IN COORDINATES
                ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(i)
                call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%X(1:nu,i,j),1,0._8,WORK%tmp,1)
                polymer%X(1:nu,i,j)=WORK%tmp/f_param%sqrt_mass(i)
                polymer%X(nu,i,j)=polymer%X(nu,i,j)/f_param%sqrt_delta_mass_factor
                ! polymer%P(:,i)=matmul(f_param%EigMat,polymer%P(:,i))*f_param%sqrt_mass(i)
                call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%P(1:nu,i,j),1,0._8,WORK%tmp,1)
                polymer%P(1:nu,i,j)=WORK%tmp*f_param%sqrt_mass(i)
                polymer%P(nu,i,j)=f_param%sqrt_delta_mass_factor*polymer%P(nu,i,j)

            enddo ; enddo

            polymer%Delta(:,:)=polymer%X(nu,:,:)

            CALL update_beads_forces(system%X,polymer)
            !APPLY FORCES		
        CALL apply_forces(polymer,f_param%dt)

        if(f_param%MC_acceptance_test .AND. polymer%PIOUD_noise) then
            CALL acceptance_test(system%X,polymer,move_accepted)
        else
            move_accepted=.TRUE.
        endif

	end subroutine PIOUD_step

    subroutine HMC_step(system,polymer,move_accepted,WORK)
		IMPLICIT NONE
        TYPE(WIG_SYS), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, INTENT(out) :: move_accepted
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		integer :: i,j,nu,k
        REAL(wp) :: momk(f_param%n_beads)

		nu=f_param%n_beads

        ! write(*,*) 
        ! write(*,*) polymer%P

        CALL apply_forces(polymer,0.5_wp*f_param%dt)

            do j=1,system%n_dim ; do i=1,system%n_atoms

                mOmk(:)=f_param%Omk*sqrt(system%mass(i))/f_param%sqrt_mass(i) 

                !TRANSFORM IN NORMAL MODES	
                WORK%EigX(:)=polymer%X(1:nu,i,j)*f_param%sqrt_mass(i)
                WORK%EigX(nu)=f_param%sqrt_delta_mass_factor*WORK%EigX(nu)
                ! WORK%EigX=matmul(f_param%EigMatTr,WORK%EigX)
                call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigX,1,0._8,WORK%tmp,1)
                WORK%EigX=WORK%tmp
                WORK%EigV=polymer%P(1:nu,i,j)/f_param%sqrt_mass(i)
                WORK%EigV(nu)=WORK%EigV(nu)/f_param%sqrt_delta_mass_factor
                ! EigV=matmul(EigMatTr,EigV)
                call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigV,1,0._8,WORK%tmp,1)
                WORK%EigV=WORK%tmp

                !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
                polymer%P(:,i,j)=WORK%EigV(:)*cos(mOmk(:)*f_param%dt)&
                    -WORK%EigX(:)*mOmk(:)*sin(mOmk(:)*f_param%dt)
                

                polymer%X(1:nu,i,j)=WORK%EigV(:)*sin(mOmk(:)*f_param%dt)/mOmk(:) &
                                 +WORK%EigX(:)*cos(mOmk(:)*f_param%dt)
                
                
                !TRANSFORM BACK IN COORDINATES
                ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(i)
                call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%X(1:nu,i,j),1,0._8,WORK%tmp,1)
                polymer%X(1:nu,i,j)=WORK%tmp/f_param%sqrt_mass(i)
                polymer%X(nu,i,j)=polymer%X(nu,i,j)/f_param%sqrt_delta_mass_factor
                ! polymer%P(:,i)=matmul(f_param%EigMat,polymer%P(:,i))*f_param%sqrt_mass(i)
                call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%P(1:nu,i,j),1,0._8,WORK%tmp,1)
                polymer%P(1:nu,i,j)=WORK%tmp*f_param%sqrt_mass(i)
                polymer%P(nu,i,j)=f_param%sqrt_delta_mass_factor*polymer%P(nu,i,j)

            enddo ; enddo

            polymer%Delta(:,:)=polymer%X(nu,:,:)

            CALL update_beads_forces(system%X,polymer)
            !APPLY FORCES		
        CALL apply_forces(polymer,0.5_wp*f_param%dt)

        ! write(*,*) polymer%P

        polymer%HMC_count=polymer%HMC_count+1
        if(polymer%HMC_count>f_param%HMC_refresh_p) then
            polymer%HMC_count=1
            if(f_param%MC_acceptance_test) then
                CALL acceptance_test(system%X,polymer,move_accepted)
            else
                move_accepted=.TRUE.
            endif
            call sample_momenta(polymer)
        else
            move_accepted=.TRUE.
        endif

	end subroutine HMC_step

	subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i,j
       ! REAL(wp) :: Dnu(f_param%n_atoms,f_param%n_dim),Fdelta(f_param%n_atoms,f_param%n_dim)
       ! REAL(wp) :: sig

		do i=1,f_param%n_beads-1
			polymer%P(i,:,:)=polymer%P(i,:,:)+tau*polymer%F_beads(i,:,:)
		enddo
		polymer%P(f_param%n_beads,:,:)=polymer%P(f_param%n_beads,:,:) + 0.5_wp*tau* &
			(polymer%F_beads(0,:,:) - polymer%F_beads(f_param%n_beads,:,:))
        
        ! Dnu=polymer%X(1,:,:)-polymer%X(f_param%n_beads-1,:,:)
        ! Fdelta=0.5_wp*(polymer%F_beads(0,:,:) - polymer%F_beads(f_param%n_beads,:,:))
        ! DO j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
        !     sig=f_param%sigp2_inv(i)/f_param%dBeta
        !     Fdelta(i,j)=Fdelta(i,j)-0.5_wp*sig*(polymer%Delta(i,j)-Dnu(i,j))
        ! ENDDO ; ENDDO
        ! write(95,*) polymer%Delta, -Fdelta, f_param%dBeta,Mprot

	end subroutine apply_forces

    subroutine acceptance_test(q,polymer,move_accepted)
		implicit none
        REAL(wp), INTENT(in) :: q(:,:)
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, intent(inout) :: move_accepted
        REAL(wp) :: Ener,R        

        Ener=compute_MC_ener(q,polymer)

        polymer%MC_count_moves=polymer%MC_count_moves+1
        
        call random_number(R)
       ! write(*,*) R, (Ener-polymer%MC_ener)*THz,THz/f_param%Beta,exp(-f_param%dBeta*(Ener-polymer%MC_ener))
        if( R <= exp(-f_param%dBeta*(Ener-polymer%MC_ener)) ) then
           !write(*,*) polymer%MC_count_moves,Ener
            move_accepted=.TRUE.
            polymer%MC_ener=Ener
            polymer%X_prev=polymer%X
          !  polymer%P=-polymer%P
           ! polymer%P_prev=polymer%P            
            polymer%Delta_prev=polymer%Delta
            polymer%MC_acc_ratio=polymer%MC_acc_ratio &
                +(1._wp-polymer%MC_acc_ratio)/polymer%MC_count_moves
            polymer%F_beads_prev=polymer%F_beads
            polymer%Pot_beads_prev = polymer%Pot_beads
        else
            move_accepted=.FALSE.
            polymer%F_beads=polymer%F_beads_prev
            polymer%X=polymer%X_prev
            !polymer%P_prev=-polymer%P_prev
           ! polymer%P=polymer%P_prev            
            polymer%Delta=polymer%Delta_prev
            polymer%Pot_beads = polymer%Pot_beads_prev
            polymer%MC_acc_ratio=polymer%MC_acc_ratio &
                +(0._wp-polymer%MC_acc_ratio)/polymer%MC_count_moves
            
           ! polymer%HMC_count=f_param%HMC_refresh_p
        endif

    end subroutine acceptance_test

    function compute_MC_ener(q,polymer) result(Ener)
        implicit none
        REAL(wp), INTENT(in) :: q(:,:)
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        REAL(wp) :: Ener,sig, Epot, Ekin
        INTEGER :: i,j,k,nu

        nu=f_param%n_beads
        Epot=SUM(polymer%Pot_beads)
        Ekin=0._wp
        DO j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
            sig=0.5_wp*f_param%sigp2_inv(i)/f_param%dBeta
            DO k=1,nu-2
                Ekin=Ekin+sig*(polymer%X(k+1,i,j)-polymer%X(k,i,j))**2 &
                        + 0.5* (polymer%P(k,i,j)/f_param%sqrt_mass(i))**2
            ENDDO
            Ekin=Ekin+0.5*( (polymer%P(nu-1,i,j)/f_param%sqrt_mass(i))**2 &
                           + (polymer%P(nu,i,j)/(f_param%sqrt_delta_mass_factor*f_param%sqrt_mass(i)))**2)
            Ekin=Ekin+sig*((q(i,j)-0.5*polymer%Delta(i,j)-polymer%X(nu-1,i,j))**2 &
                            +(q(i,j)+0.5*polymer%Delta(i,j)-polymer%X(1,i,j))**2)            
        ENDDO ; ENDDO     
       ! write(*,*) "Epot per bead per dof",Epot /real(f_param%n_atoms*f_param%n_dim*nu)*THz
        !write(*,*) "Ekin per bead per dof",Ekin /real(f_param%n_atoms*f_param%n_dim*nu)*THz
        Ener = Epot + Ekin
        !write(*,*) Ener, Epot,Ekin

    end function compute_MC_ener

    function compute_config_weight(q,pot0,potnu,delta,x1,xnu) result(Ener)
        implicit none
        REAL(wp), INTENT(in) :: q(:,:),delta(:,:),x1(:,:),xnu(:,:),pot0,potnu
        REAL(wp) :: Ener,sig
        INTEGER :: i,j
    
        Ener=f_param%dBeta*(pot0+potnu)
        DO j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
            sig=0.5_wp*f_param%sigp2_inv(i)
            Ener=Ener+sig*((q(i,j)-0.5*delta(i,j)-xnu(i,j))**2 &
                            +(q(i,j)+0.5*delta(i,j)-x1(i,j))**2)            
        ENDDO ; ENDDO

    end function compute_config_weight

!--------------------------------------------------------------
! VARIOUS INITIALIZATIONS

    subroutine reinitialize_polymer_config(system,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j

        polymer%k2=0
        polymer%k2inv=0
        if(f_param%compute_EW) then
            polymer%k4=0
            polymer%k6=0
            polymer%G4=0
        endif
        polymer%F=0
        polymer%dF=0
        polymer%G=0        

        DO i=1,f_param%n_beads-1
            polymer%X(i,:,:)=polymer%X(i,:,:) + system%X(:,:)-system%X_prev(:,:)
        ENDDO       
        polymer%X_prev=polymer%X
        polymer%P_prev=polymer%P
        polymer%Delta_prev=polymer%Delta
        CALL update_beads_forces(system%X,polymer)
        polymer%F_beads_prev=polymer%F_beads
        polymer%Pot_beads_prev=polymer%Pot_beads

        if(f_param%MC_acceptance_test) then
            polymer%MC_ener=compute_MC_ener(system%X,polymer)
            polymer%MC_acc_ratio=0
            polymer%MC_count_moves=0
        endif

        if(f_param%move_generator=="HMC") then
            polymer%HMC_count=0
            call sample_momenta(polymer)
        endif 

        if(f_param%reweight_previous_configs) then
            polymer%i_step_reweight = polymer%i_step_reweight +1
            if(polymer%i_step_reweight>f_param%n_steps_reweight) polymer%i_step_reweight=1
            open(newunit=polymer%prev_config_unit &
                    ,file=polymer%prev_config_filename//"_"//int_to_str(polymer%i_step_reweight) &
                    ,form="unformatted")
        endif

        !CALL apply_forces(polymer,0.5_wp*f_param%dt)

    end subroutine reinitialize_polymer_config

    subroutine initialize_auxiliary_simulation(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
        TYPE(DICT_STRUCT), pointer :: wig_lib
        INTEGER :: n_dof,n_beads,n_atoms,n_dim
        INTEGER :: i,j,k,n_therm_tmp,n_steps_init
        CHARACTER(:), ALLOCATABLE :: pot_name

        n_dof=system%n_dof
        f_param%n_dof=n_dof
        n_atoms=system%n_atoms
        f_param%n_atoms=n_atoms
        n_dim=system%n_dim
        f_param%n_dim=n_dim

        f_param%verbose = param_library%get(WIGNER_CAT//"/verbose",default=.FALSE.)
        !! @input_file WIGNER_AUX/verbose (wigner, default=.FALSE.)

        NUM_AUX_SIM=param_library%get(WIGNER_CAT//"/num_aux_sim",default=3)
        !! @input_file WIGNER_AUX/num_aux_sim (wigner, default=3)

        ! AT LEAST 3 AUXILIARY SIMULATIONS FOR DYNAMICS
        IF((.not. parameters%only_auxiliary_simulation) .and. NUM_AUX_SIM<3) &
            STOP "Error : 'num_aux_sim' must be >=3 for dynamics"
        allocate(polymers(NUM_AUX_SIM))

        wig_lib => param_library%get_child(WIGNER_CAT)
        !GET NUMBER OF BEADS
        n_beads=wig_lib%get("n_beads")
        !! @input_file WIGNER_AUX/n_beads (wigner)
		if(n_beads<=0) STOP "Error: 'n_beads' of '"//WIGNER_CAT//"' is not properly defined!"
			polymers(:)%n_beads=n_beads
			f_param%n_beads=n_beads
        
        !GET NUMBER OF AUXILIARY SIMULATION STEPS
        f_param%n_steps=wig_lib%get("n_steps")
        !! @input_file WIGNER_AUX/n_steps (wigner)
        if(f_param%n_steps<=0) &
            STOP "Error: 'n_steps' of '"//WIGNER_CAT//"' is not properly defined!"

        f_param%n_steps_therm=wig_lib%get("n_steps_therm")
        !! @input_file WIGNER_AUX/n_steps_therm (wigner)
        if(f_param%n_steps_therm<0) &
            STOP "Error: 'n_steps_therm' of '"//WIGNER_CAT//"' is not properly defined!"

        f_param%spring_force=wig_lib%get("spring_force",default=.FALSE.)
        !! @input_file WIGNER_AUX/spring_force (wigner, default=.FALSE.)

        f_param%k2_smooth=wig_lib%get("k2_smoothing",default=.FALSE.)
        !! @input_file WIGNER_AUX/k2_smoothing (wigner, default=.FALSE.)        
        f_param%k2_smooth_width=wig_lib%get("k2_smooth_width",default=0.03)
        !! @input_file WIGNER_AUX/k2_smooth_width (wigner, default=0.03)
        f_param%k2_smooth_center=wig_lib%get("k2_smooth_center",default=0.9)
        !! @input_file WIGNER_AUX/k2_smooth_center (wigner, default=0.9)
        if(f_param%k2_smooth) then
            write(*,*)
            write(*,*) "k2 smoothing of off-diagonal terms activated."
            write(*,*) "  using fermi function with threshold ",f_param%k2_smooth_center
            write(*,*) "  and transition width",f_param%k2_smooth_width
            f_param%G_smooth=wig_lib%get("g_smoothing",default=.TRUE.)
        endif
       
        ALLOCATE(f_param%k2_smooth_mat(system%n_dof,system%n_dof))

        f_param%memory_smoothing=wig_lib%get("memory_smoothing",default=.FALSE.)
        !! @input_file WIGNER_AUX/memory_smoothing (wigner, default=.FALSE.)
        if(f_param%memory_smoothing) then
            f_param%memory_tau=wig_lib%get("memory_tau",default=5._wp*parameters%dt)
            !! @input_file WIGNER_AUX/memory_tau (wigner, default=5*dt)
            allocate(f_param%memory_C(n_dof,n_dof,n_dof) &
                    ,f_param%memory_F(n_dof) &
                    ,f_param%memory_k2(n_dof,n_dof) &
                    ,f_param%memory_sqrtk2inv(n_dof,n_dof) &
                )
            f_param%memory_C=0
            f_param%memory_F=0
            f_param%memory_k2=0
            f_param%memory_sqrtk2inv=0
            f_param%memory_sum_weight=0
            write(*,*)
            write(*,*) "memory smoothing activated with tau=",f_param%memory_tau*fs,"fs"
        endif

        f_param%MC_acceptance_test=wig_lib%get("mc_acceptance_test",default=.FALSE.)
        if(f_param%MC_acceptance_test) then
            DO i=1,NUM_AUX_SIM
                polymers(i)%MC_acc_ratio=0
                polymers(i)%MC_count_moves=0
            ENDDO
        endif
        !! @input_file WIGNER_AUX/spring_force (wigner, default=.FALSE.)

        f_param%reweight_previous_configs=wig_lib%get("reweight_previous_configs",default=.FALSE.)
        if(f_param%reweight_previous_configs) then
            call EXECUTE_COMMAND_LINE("rm "//output%working_directory//"/prev_config_*")
            f_param%n_steps_reweight=wig_lib%get("n_steps_reweight",default=1)
            polymers(:)%i_step_reweight=0
        endif

        !INITIALIZE FORCES PARAMETERS
        allocate( &
            f_param%EigMat(n_beads,n_beads), &
            f_param%EigMatTr(n_beads,n_beads), &
            f_param%OmK(n_beads), &
            f_param%sigp2_inv(n_atoms), &
            f_param%sqrt_mass(n_atoms) &
        )
        f_param%beta=parameters%beta
        f_param%dBeta=parameters%beta/REAL(f_param%n_beads,wp)
        f_param%sigp2_inv(:)=system%mass(:)/f_param%dBeta
        f_param%sqrt_mass=sqrt(system%mass)

        ! GET NUMBER OF THREADS        
        f_param%n_threads=1
        !$ f_param%n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

        !INITIALIZE WORK VARIABLES
        allocate(WORKS(f_param%n_threads))
        DO i=1,f_param%n_threads
            allocate( &
                WORKS(i)%F(n_dof), &
                ! WORKS(i)%dF(n_dof), &
                WORKS(i)%Delta2(n_dof,n_dof), &
                WORKS(i)%EigX(n_beads), &
                WORKS(i)%EigV(n_beads), &
                WORKS(i)%R(n_atoms), &
                WORKS(i)%R1(n_beads), &
                WORKS(i)%R2(n_beads), &
                WORKS(i)%tmp(n_beads) &
            )
        ENDDO


        !INITIALIZE polymers
        DO i=1,NUM_AUX_SIM
            allocate( &
                polymers(i)%X(0:n_beads,n_atoms,n_dim), &
                polymers(i)%P(n_beads,n_atoms,n_dim), &
                polymers(i)%X_prev(0:n_beads,n_atoms,n_dim), &
                polymers(i)%P_prev(n_beads,n_atoms,n_dim), &
                polymers(i)%F_beads(0:n_beads,n_atoms,n_dim), &
                polymers(i)%F_beads_prev(0:n_beads,n_atoms,n_dim), &
                polymers(i)%Pot_beads(0:n_beads), &
                polymers(i)%Pot_beads_prev(0:n_beads), &
                polymers(i)%Delta(n_atoms,n_dim), &
                polymers(i)%Delta_prev(n_atoms,n_dim), &
                polymers(i)%Delta_packed(n_dof), &
                polymers(i)%k2(n_dof,n_dof), &
                polymers(i)%k2_reweight(n_dof,n_dof), &
                polymers(i)%k2inv(n_dof,n_dof), &
                polymers(i)%F(n_dof), &
                polymers(i)%F_reweight(n_dof), &
                polymers(i)%dF(n_dof), &
                polymers(i)%G(n_dof,n_dof,n_dof), &
                polymers(i)%G_reweight(n_dof,n_dof,n_dof) &
            )
            polymers(i)%Delta=0._wp
            polymers(i)%Delta_prev=0._wp
            DO j=1,n_beads
                polymers(i)%X(j,:,:)=system%X(:,:)               
            ENDDO
            polymers(i)%P_prev=polymers(i)%P
            polymers(i)%X_prev=polymers(i)%X
            if(f_param%reweight_previous_configs) &
                polymers(i)%prev_config_filename=output%working_directory//"/prev_config_"//int_to_str(i)
        ENDDO

        f_param%compute_EW = parameters%compute_EW

        if(f_param%compute_EW) then
            DO i=1,f_param%n_threads
                allocate(WORKS(i)%Delta4(n_dof,n_dof,n_dof,n_dof))
            ENDDO
            DO i=1,NUM_AUX_SIM
                allocate(polymers(i)%k4(n_dof,n_dof,n_dof,n_dof))
            ENDDO            
        endif

        f_param%parallel_forces=.FALSE.
		pot_name=param_library%get("POTENTIAL/pot_name")
		if(to_upper_case(trim(pot_name))=="SOCKET") f_param%parallel_forces=.TRUE.

        call initialize_bead_forces()

        call initialize_move_generator(system,parameters,wig_lib)

        DO i=1,NUM_AUX_SIM
            call sample_momenta(polymers(i))
        ENDDO

        if(.not. parameters%only_auxiliary_simulation) then
            n_steps_init=wig_lib%get("n_steps_init",default=5000)
            !! @input_file WIGNER_AUX/n_steps_init (wigner, default=5000)
            n_therm_tmp = f_param%n_steps_therm
            f_param%n_steps_therm = n_steps_init
            write(*,*) "Initializing WiLD forces with",n_steps_init,"steps"
            call compute_wigner_forces(system,parameters)
            write(*,*) "WiLD forces initialization done."
            f_param%n_steps_therm = n_therm_tmp
        endif

    end subroutine initialize_auxiliary_simulation


    subroutine sample_momenta(polymer)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i,j,k

        DO k=1,f_param%n_dim ; DO j=1,f_param%n_atoms            
            call randGaussN(polymer%P(:,j,k))
            polymer%P(:,j,k)=polymer%P(:,j,k)*f_param%sqrt_mass(j)/sqrt(f_param%dBeta)       
        ENDDO  ; ENDDO


    end subroutine sample_momenta

    subroutine initialize_move_generator(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,j,k

        f_param%move_generator=wig_lib%get("move_generator" &
                    ,default="PIOUD")
        !! @input_file WIGNER_AUX/move_generator (wigner, default="PIOUD")
        f_param%move_generator=to_upper_case(f_param%move_generator)
        parameters%move_generator=f_param%move_generator

        SELECT CASE(f_param%move_generator)
        CASE ("PIOUD","HMC")

            !INITIALIZE PIOUD
            ! SHORTHANDS FOR MASS
            call get_polymer_masses(system,wig_lib)
            f_param%dt=wig_lib%get("dt",default=parameters%dt)
            !! @input_file WIGNER_AUX/dt (wigner, default= PARAMETERS/dt, if move_generator="PIOUD")
            f_param%delta_mass_factor=wig_lib%get("delta_mass_factor",default=0.25_wp)
            !! @input_file WIGNER_AUX/delta_mass_factor (wigner, default= 0.25, if move_generator="PIOUD")
            f_param%sqrt_delta_mass_factor=SQRT(f_param%delta_mass_factor)

            CALL initialize_PIOUD(system,wig_lib)

            DO i=1,NUM_AUX_SIM
                polymers(i)%X(f_param%n_beads,:,:)=polymers(i)%Delta
            ENDDO

            f_param%HMC_refresh_p=wig_lib%get("hmc_resample_stride",default=max(1,f_param%n_steps/10))
            DO i=1,NUM_AUX_SIM
                polymers(i)%HMC_count=0
            ENDDO

            SELECT CASE(f_param%move_generator)
            CASE ("PIOUD")
                move_proposal => PIOUD_step
            CASE("HMC")
                move_proposal => HMC_step
            END SELECT

        CASE DEFAULT
            write(0,*) "Error: unknown move generator '"//f_param%move_generator//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_move_generator

    subroutine get_polymer_masses(system,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        TYPE(DICT_STRUCT), POINTER :: m_lib
        CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
        LOGICAL, ALLOCATABLE :: is_sub(:)
        REAL(wp) :: m_tmp
        INTEGER :: i
        CHARACTER(2) :: symbol

        
        if(.not. wig_lib%has_key("modified_masses")) then
            f_param%sqrt_mass=SQRT(system%mass)
            return
        endif

        m_lib => wig_lib%get_child("modified_masses")
        write(*,*)
        write(*,*) "WiLD auxiliary masses:"
        call dict_list_of_keys(m_lib,keys,is_sub)
        DO i=1,size(keys)
			if(is_sub(i)) CYCLE
            m_tmp=m_lib%get(keys(i))
            symbol=trim(keys(i))
            symbol(1:1)=to_upper_case(symbol(1:1))
            write(*,*) symbol," : ", real(m_tmp/Mprot),"amu"
        ENDDO
        write(*,*)
        do i=1, system%n_atoms
            f_param%sqrt_mass(i)=m_lib%get(trim(system%element_symbols(i)) &
                                            ,default=system%mass(i) )
            !if(f_param%sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", f_param%sqrt_mass(i)/Mprot," amu"                    
        enddo
        f_param%sqrt_mass=sqrt(f_param%sqrt_mass)

    end subroutine get_polymer_masses

    subroutine initialize_PIOUD(system,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,nu,INFO,j
		REAL(wp), ALLOCATABLE :: TMP(:),thetaInv(:,:,:,:),OUsig2(:,:,:,:),mOmk(:,:)
		REAL(wp) :: Id(2,2), Om0
        

        nu=f_param%n_beads

        Id=0
		Id(1,1)=1
		Id(2,2)=1

		Om0=1._wp/f_param%dBeta
        allocate(TMP(3*nu-1),thetaInv(nu,2,2,system%n_atoms),OUsig2(nu,2,2,system%n_atoms),mOmk(nu,system%n_atoms))
		allocate( &
            f_param%mu(nu,2,system%n_atoms), &
            f_param%OUsig(nu,2,2,system%n_atoms), &
            f_param%expOmk(nu,2,2,system%n_atoms) &
            ! f_param%muX(nu,system%n_atoms,system%n_dim), &
            ! f_param%muP(nu,system%n_atoms,system%n_dim) &
        )

        !INITIALIZE DYNAMICAL MATRIX
		f_param%EigMat=0
		do i=1,nu-1
			f_param%EigMat(i,i)=2
			f_param%EigMat(i+1,i)=-1
			f_param%EigMat(i,i+1)=-1
		enddo
		f_param%EigMat(nu,nu)=0.5_wp/f_param%delta_mass_factor
		f_param%EigMat(nu-1,nu)=0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(nu,nu-1)=0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(1,nu)=-0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(nu,1)=-0.5_wp/f_param%sqrt_delta_mass_factor

        !SOLVE EIGENPROBLEM
		CALL DSYEV('V','L',nu,f_param%EigMat,nu,f_param%Omk,TMP,3*nu-1,INFO)
		if(INFO/=0) then
			write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
			stop 
		endif

        f_param%Omk=Om0*SQRT(f_param%Omk)
        DO i=1,system%n_atoms
           mOmk(:,i)=f_param%Omk*sqrt(system%mass(i))/f_param%sqrt_mass(i) 
        ENDDO
		write(*,*)
		write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
		do i=1,nu
			write(*,*) i, f_param%Omk(i)*THz/(2*pi),"THz"
		enddo
		write(*,*)

        f_param%EigMatTr=transpose(f_param%EigMat)

        DO i=1,system%n_atoms
            f_param%expOmk(:,1,1,i)=exp(-mOmk(:,i)*f_param%dt) &
                                    *(1-mOmk(:,i)*f_param%dt)
            f_param%expOmk(:,1,2,i)=exp(-mOmk(:,i)*f_param%dt) &
                                    *(-f_param%dt*mOmk(:,i)**2)
            f_param%expOmk(:,2,1,i)=exp(-mOmk(:,i)*f_param%dt)*f_param%dt
            f_param%expOmk(:,2,2,i)=exp(-mOmk(:,i)*f_param%dt) &
                                    *(1+mOmk(:,i)*f_param%dt)

            thetaInv(:,1,1,i)=0._wp
            thetaInv(:,1,2,i)=-1._wp
            thetaInv(:,2,1,i)=1._wp/mOmk(:,i)**2
            thetaInv(:,2,2,i)=2._wp/mOmk(:,i)
        ENDDO

        !COMPUTE MEAN VECTOR
        f_param%mu=0
        do j=1,system%n_atoms            
            f_param%mu(1,1,j)=Om0**2
            f_param%mu(nu-1,1,j)=Om0**2
            f_param%mu(:,1,j)=matmul(f_param%EigMatTr,f_param%mu(:,1,j))
            f_param%mu(:,2,j)=0._wp
        
            do i=1,nu
                thetaInv(i,:,:,j)=matmul((Id(:,:)-f_param%expOmk(i,:,:,j)),thetaInv(i,:,:,j))
                f_param%mu(i,:,j)=(system%mass(j)/f_param%sqrt_mass(j))*matmul(thetaInv(i,:,:,j),f_param%mu(i,:,j))
            enddo
        enddo

        !COMPUTE COVARIANCE MATRIX
        DO i=1,system%n_atoms
            OUsig2(:,1,1,i)=(1._wp- (1._wp-2._wp*f_param%dt*mOmk(:,i) &
                                +2._wp*(f_param%dt*mOmk(:,i))**2 &
                            )*exp(-2._wp*mOmk(:,i)*f_param%dt) &
                            )/f_param%dBeta
            OUsig2(:,2,2,i)=(1._wp- (1._wp+2._wp*f_param%dt*mOmk(:,i) &
                                +2._wp*(f_param%dt*mOmk(:,i))**2 &
                            )*exp(-2._wp*mOmk(:,i)*f_param%dt) &
                            )/(f_param%dBeta*mOmk(:,i)**2)
            OUsig2(:,2,1,i)=2._wp*mOmk(:,i)*(f_param%dt**2) &
                            *exp(-2._wp*mOmk(:,i)*f_param%dt)/f_param%dBeta
            OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

            !COMPUTE CHOLESKY DECOMPOSITION
            f_param%OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
            f_param%OUsig(:,1,2,i)=0
            f_param%OUsig(:,2,1,i)=OUsig2(:,2,1,i)/f_param%OUsig(:,1,1,i)
            f_param%OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-f_param%OUsig(:,2,1,i)**2)
        ENDDO

		!CHECK CHOLESKY DECOMPOSITION
		! write(*,*) "sum of squared errors on cholesky decomposition:"
		! do i=1,nu
		! 	write(*,*) i, sum( (matmul(f_param%OUsig(i,:,:),transpose(f_param%OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
		! enddo

    end subroutine initialize_PIOUD

    subroutine initialize_bead_forces()
        IMPLICIT NONE

        IF(f_param%parallel_forces) THEN
            update_beads_forces => update_beads_forces_socket_parallel
        ELSE
            update_beads_forces => update_beads_forces_serial
        ENDIF

    end subroutine initialize_bead_forces

!----------------------------------------------------------------------

    subroutine update_beads_forces_serial(q,polymer)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q(:,:)
		INTEGER :: i
        
		do i=1,f_param%n_beads-1
            call get_pot_info(polymer%X(i,:,:),polymer%Pot_beads(i),polymer%F_beads(i,:,:))
			!polymer%F_beads(i,:,:)=-dPot(polymer%X(i,:,:))
		enddo
        call get_pot_info(q+0.5_wp*polymer%Delta,polymer%Pot_beads(0),polymer%F_beads(0,:,:))
        call get_pot_info(q-0.5_wp*polymer%Delta,polymer%Pot_beads(f_param%n_beads) &
                                                ,polymer%F_beads(f_param%n_beads,:,:))
		!polymer%F_beads(0,:,:)=-0.5_wp*dPot(q+0.5_wp*polymer%Delta) 
        !polymer%F_beads(f_param%n_beads,:,:)=-0.5_wp*dPot(q-0.5_wp*polymer%Delta)
        polymer%F_beads(0,:,:)=0.5_wp*polymer%F_beads(0,:,:)
        polymer%F_beads(f_param%n_beads,:,:)=0.5_wp*polymer%F_beads(f_param%n_beads,:,:)
        polymer%Pot_beads(0)=0.5_wp*polymer%Pot_beads(0)
        polymer%Pot_beads(f_param%n_beads)=0.5_wp*polymer%Pot_beads(f_param%n_beads)
	end subroutine update_beads_forces_serial

    subroutine update_beads_forces_socket_parallel(q,polymer)
        !$ USE OMP_LIB
        implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q(:,:)
		INTEGER :: i,is

        is=1        
        !$ is=OMP_GET_THREAD_NUM()+1
        
        polymer%X(0,:,:) = q + 0.5_wp*polymer%Delta
        polymer%X(f_param%n_beads,:,:) = q - 0.5_wp*polymer%Delta
		CALL parallel_socket_pot_info(polymer%X,polymer%F_beads,polymer%Pot_beads,i_socket=is)

        if(f_param%move_generator=="PIOUD" &
            .OR. f_param%move_generator=="HMC") polymer%X(f_param%n_beads,:,:) = polymer%Delta


	end subroutine update_beads_forces_socket_parallel

!----------------------------------------------------------------------
! COMPUTE EDGEWORTH CORRECTION

	subroutine compute_EW(system,parameters)
		implicit none
		TYPE(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_PARAM), INTENT(inout) :: parameters
		INTEGER :: j,k,l,m
		REAL(wp), ALLOCATABLE, SAVE :: k2(:,:,:)
		REAL(wp) :: C6        

        if(.not. f_param%compute_EW) then
            system%EW=1
            system%EW6=1
            return
        endif

		system%EW=0

        if(parameters%classical_wigner) then
            system%P_packed = RESHAPE(system%P_mod, (/system%n_dof/))
        endif

		if(.not. allocated(k2)) allocate(k2(system%n_dof,system%n_dof,2))
		k2(:,:,1)= 0.5_wp*( polymers(1)%k2(:,:) + polymers(2)%k2(:,:) )
		k2(:,:,2)= polymers(3)%k2(:,:)

		do m=1,system%n_dof ; do l=1,system%n_dof ; do k=1,system%n_dof ; do j=1,system%n_dof
	
			system%EW = system%EW + system%P_packed(j)*system%P_packed(k) &
                        *system%P_packed(l)*system%P_packed(m)*( &
                            system%k4(j,k,l,m)  - k2(j,l,1)*k2(k,m,2) &
                                                - k2(k,l,1)*k2(j,m,2) &
                                                - k2(j,k,1)*k2(l,m,2) &
                        )
														
		enddo ; enddo ; enddo ; enddo

		system%EW = 1 + system%EW / 24._wp

		if(system%n_dof==1) then
			C6 = system%k6 - 15._wp*polymers(1)%k2(1,1)*0.5_wp*(polymers(2)%k4(1,1,1,1)+polymers(3)%k4(1,1,1,1)) &
				+30._wp*polymers(1)%k2(1,1)*polymers(2)%k2(1,1)*polymers(3)%k2(1,1)
			system%EW6 = system%EW - (system%P_packed(1)**6) * C6 / 720._wp
		endif

	end subroutine compute_EW

!--------------------------------------------------
! REWEIGHT PREVIOUS CONFIGS

    subroutine reweight_previous_configs(system,parameters,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: u,ios,nat,ndim,c,i,j,is,cloc,k
        REAL(wp), ALLOCATABLE :: Delta(:,:),Xnu(:,:),X1(:,:),F_delta(:,:)
        REAL(wp) :: weight_old,weight_new,pot0,potnu,weight_ratio,mean_weight_ratio,weight_sum
        REAL(wp) :: Pot0_old,Potnu_old,mean_ratios(f_param%n_steps_reweight),mean_ratio_min
        LOGICAL :: file_exist
        CHARACTER(:), ALLOCATABLE :: filename

        nat=system%n_atoms
        ndim=system%n_dim
        allocate(Delta(nat,ndim),Xnu(nat,ndim),X1(nat,ndim),F_delta(nat,ndim))
        polymer%k2_reweight=0
        polymer%G_reweight=0
        polymer%F_reweight=0

        c=0
        mean_ratios=0
        weight_sum=0
        polymer%mean_weight_ratio=0
        DO is=1,f_param%n_steps_reweight
            filename=polymer%prev_config_filename//"_"//int_to_str(is)
            INQUIRE(FILE=filename,EXIST=file_exist)
            if(.not. file_exist) CYCLE
            OPEN(newunit=u,file=filename,form="unformatted")
            cloc=0            
            DO
                READ(u,iostat=ios) weight_old,Pot0_old,Potnu_old,Delta,X1,Xnu,WORK%F
                if(ios/=0) EXIT
                c=c+1
                cloc=cloc+1
                call get_pot_info(system%X+0.5_wp*Delta,pot0,F_delta)
                WORK%F=WORK%F+0.5_wp*RESHAPE(F_delta,(/system%n_dof/)) /REAL(f_param%n_beads,wp)
                call get_pot_info(system%X-0.5_wp*Delta,potnu,F_delta)
                WORK%F=WORK%F+0.5_wp*RESHAPE(F_delta,(/system%n_dof/))/REAL(f_param%n_beads,wp)
                weight_new=compute_config_weight(system%X,0.5*pot0,0.5*potnu,Delta,X1,Xnu)
            
                weight_ratio=exp(-(weight_new-weight_old))
                !write(*,*) is, weight_ratio
                !write(*,*) polymer%prev_config_filename,c,weight_ratio,Pot0_old,0.5*Pot0
                weight_sum= weight_sum + weight_ratio
                polymer%mean_weight_ratio = polymer%mean_weight_ratio &
                    +(weight_ratio - polymer%mean_weight_ratio)/REAL(c,wp)
                
                mean_ratios(is)=mean_ratios(is) &
                    +(weight_ratio - mean_ratios(is))/REAL(cloc,wp)

                polymer%Delta_packed=RESHAPE(Delta,(/system%n_dof/))
                !COMPUTE ONLY THE LOWER TRIANGULAR PART
                do j=1,system%n_dof ; do i=j,system%n_dof
                    WORK%Delta2(i,j)=weight_ratio*polymer%Delta_packed(i)*polymer%Delta_packed(j)
                    polymer%k2_reweight(i,j) =  polymer%k2_reweight(i,j) &
                    ! + weight_ratio*polymer%Delta_packed(i)*polymer%Delta_packed(j)
                        + (WORK%Delta2(i,j) -polymer%k2_reweight(i,j))/REAL(c,wp)
                enddo ; enddo  

                DO k=1,f_param%n_dof
                    do j=1,system%n_dof ; do i=j,system%n_dof
                        polymer%G_reweight(i,j,k)=polymer%G_reweight(i,j,k) &
                            +(WORK%Delta2(i,j)*WORK%F(k)-polymer%G_reweight(i,j,k))/REAL(c,wp)
                    enddo ; enddo
                ENDDO  
                polymer%F_reweight=polymer%F_reweight &
                    + (weight_ratio*WORK%F - polymer%F_reweight)/REAL(c,wp)   
            ENDDO
            !polymer%mean_weight_ratio=weight_sum/REAL(c,wp)
            
            close(u)
        ENDDO
       ! if(f_param%verbose) &
            !write(*,*) mean_ratios
        !    write(*,*) "minimum mean ratio =",minval(mean_ratios)

        DO j=1,system%n_dof ; DO i=j+1,system%n_dof
            polymer%k2_reweight(j,i)=polymer%k2_reweight(i,j)
        ENDDO ; ENDDO
        DO k=1,f_param%n_dof
            DO j=1,system%n_dof ; DO i=j+1,system%n_dof
               polymer%G_reweight(j,i,k)=polymer%G_reweight(i,j,k)
            ENDDO ; ENDDO
        ENDDO 
        polymer%k2_reweight=polymer%k2_reweight/polymer%mean_weight_ratio
        polymer%G_reweight=polymer%G_reweight/polymer%mean_weight_ratio
        polymer%F_reweight=polymer%F_reweight/polymer%mean_weight_ratio

    end subroutine

END MODULE wigner_forces
