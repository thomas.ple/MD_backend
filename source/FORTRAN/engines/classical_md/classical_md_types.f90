MODULE classical_md_types
	USE kinds
	USE file_handler, only: output
	USE nested_dictionaries
	USE string_operations, only: int_to_str
	USE atomic_units
	USE basic_types, only: simulation_box
	USE matrix_operations, only: cross_product
    IMPLICIT NONE

    TYPE CLASS_MD_SYS
		INTEGER :: n_dim
		INTEGER :: n_atoms
		INTEGER :: n_dof

		REAL(wp) :: time

		REAL(wp), allocatable :: mass(:)
		REAL(wp), allocatable :: masskin(:)
		REAL(wp) :: total_mass
		REAL(wp), allocatable :: X(:,:)
		REAL(wp), allocatable :: P(:,:)
		CHARACTER(2), allocatable :: element_symbols(:)
		CHARACTER(2), allocatable :: element_set(:)
		TYPE(DICT_STRUCT), POINTER :: element_dict
		INTEGER :: n_elements

		REAL(wp), allocatable :: Forces(:,:)
		REAL(wp) :: E_pot
		REAL(wp) :: E_pot_mean
		REAL(wp) :: E_pot_var
		REAL(wp) :: temp_estimated
		REAL(wp), allocatable :: E_kin(:), E_kin_mean(:)
		REAL(wp) :: pressure_tensor(3,3),pressure_tensor_mean(3,3)

		REAL(wp), allocatable :: X_collective(:)
		REAL(wp), allocatable :: X_collective_grad(:,:,:)
	
		REAL(wp), allocatable :: FE_force(:)
		REAL(wp), allocatable :: FE_mean_force(:)
		REAL(wp), allocatable :: FE_mean_force2(:)
		REAL(wp), allocatable :: FE_std_dev_force(:)
		REAL(wp), allocatable :: FE_Z(:)

		REAL(wp), allocatable :: dipole(:), dipole_velocity(:)
		REAL(wp), allocatable :: charges(:)

	END TYPE

	TYPE CLASS_MD_PARAM
		CHARACTER(:), allocatable :: engine

		LOGICAL :: write_restart
		INTEGER :: restart_stride

		INTEGER :: n_dim
		INTEGER :: n_atoms
		INTEGER :: n_dof
		
		REAL(wp) :: temperature
		REAL(wp) :: beta

		INTEGER :: n_steps_prev
		INTEGER :: n_steps
		INTEGER :: n_steps_therm
		REAL(wp) :: dt

		LOGICAL :: mass_normalized

		INTEGER :: print_temperature_stride

		LOGICAL :: keep_potential

		CHARACTER(:), allocatable :: thermostat_type

		CHARACTER(:), allocatable :: algorithm
		CHARACTER(:), allocatable :: integrator_type

		CHARACTER(:), allocatable :: collective_coord
        INTEGER :: collective_coord_size
        
        REAL(wp), allocatable :: FE_kappa(:)
		LOGICAL :: write_mean_force_info
		LOGICAL :: on_the_fly_mean_values

		LOGICAL :: compute_E_kin
		LOGICAL :: write_mean_energy_info
		INTEGER :: counter_mean_energy_info

		INTEGER :: n_threads

		LOGICAL :: apply_eckart_conditions

		LOGICAL :: compute_dipole
		INTEGER :: compute_dipole_stride

		LOGICAL :: cmd_force

	CONTAINS
		PROCEDURE :: check_exit_file => classical_MD_check_exit_file
	END TYPE	

	ABSTRACT INTERFACE
		subroutine class_MD_sub(system,parameters)
			import :: CLASS_MD_SYS, CLASS_MD_PARAM
			IMPLICIT NONE
			CLASS(CLASS_MD_SYS), intent(inout) :: system
			CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		end subroutine class_MD_sub

		subroutine class_MD_sub_int(system,parameters,n)
			import :: CLASS_MD_SYS, CLASS_MD_PARAM
			IMPLICIT NONE
			CLASS(CLASS_MD_SYS), intent(inout) :: system
			CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
			INTEGER, INTENT(in) :: n
		end subroutine class_MD_sub_int
	END INTERFACE

	PROCEDURE(class_MD_sub), pointer :: class_MD_update_collective_coord
	PROCEDURE(class_MD_sub_int), pointer :: class_MD_update_mean_values

	PRIVATE
	PUBLIC :: CLASS_MD_SYS, CLASS_MD_PARAM &
				,classical_MD_deallocate &
				,classical_MD_initialize_collective_coord &
				,classical_MD_update_mean_force &
				,classical_MD_initialize_restart_file &
				,classical_MD_write_restart_file &
				,classical_MD_load_restart_file &
				,classical_MD_write_mean_force_info &
				,class_MD_update_collective_coord &
				,class_MD_update_mean_values &
				,class_MD_sub, class_MD_sub_int &
				,classical_MD_compute_kinetic_energy &
				,classical_MD_update_mean_energies &
				,classical_MD_write_mean_energy_info &
				,classical_MD_print_system_info &
				,classical_md_apply_eckart &
				,classical_MD_compute_dipole

CONTAINS

	subroutine classical_MD_deallocate(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters

		if(allocated(system%X)) &
			deallocate(	system%X )
		if(allocated(system%P)) &
			deallocate(	system%P )
		if(allocated(system%mass)) &
			deallocate(	system%mass )
		if(allocated(system%masskin)) &
			deallocate(	system%masskin )
		if(allocated(system%Forces)) &
			deallocate(	system%Forces )

	end subroutine classical_MD_deallocate

	subroutine classical_MD_compute_kinetic_energy(system,parameters)
		IMPLICIT NONE
        CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j,n,k
		REAL(wp) :: vol

		n=0
		DO j=1,system%n_dim ; do i=1,system%n_atoms
			n=n+1
			system%E_kin(n)=system%P(i,j)**2/(2._wp*system%masskin(i))
		ENDDO ; ENDDO

		system%pressure_tensor=0._wp
		if(parameters%n_dim==3) then
			vol=product(simulation_box%box_lengths)			
			DO i=1, system%n_dim ; DO j=1,system%n_dim
				DO k=1,system%n_atoms
					system%pressure_tensor(i,j)=system%pressure_tensor(i,j) &
								+ system%P(k,i)*system%P(k,j)/system%masskin(k) &
								+ system%Forces(k,i)*system%X(k,j)
				ENDDO
			ENDDO ; ENDDO
			system%pressure_tensor=system%pressure_tensor/vol
		endif

	end subroutine classical_MD_compute_kinetic_energy

	subroutine classical_MD_update_mean_energies(system,parameters,n)
		IMPLICIT NONE
        CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n
		INTEGER :: n_dof

		if(parameters%apply_eckart_conditions) then
			n_dof=system%n_dof-6
		else
			n_dof = system%n_dof
		endif
		system%E_kin_mean=system%E_kin_mean+(system%E_kin-system%E_kin_mean)/real(n,wp)
		system%E_pot_mean=system%E_pot_mean+(system%E_pot-system%E_pot_mean)/real(n,wp)
		system%temp_estimated = SUM(system%E_kin_mean)*2._wp/n_dof
		system%pressure_tensor_mean = system%pressure_tensor_mean &
					+(system%pressure_tensor - system%pressure_tensor_mean)/real(n,wp)


	end subroutine classical_MD_update_mean_energies

	subroutine classical_MD_write_mean_energy_info(system,parameters)
        IMPLICIT NONE
        CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u
		REAL(wp) :: n

		n=real(parameters%n_steps-1,wp)
		OPEN(newunit=u,file=output%working_directory//"/mean_energy_info.out")
			write(u,*) "# E_pot    E_kin(:)"
			write(u,*) system%E_pot_mean, system%E_kin_mean(:)
		CLOSE(u)		
    
  end subroutine classical_MD_write_mean_energy_info

	subroutine classical_MD_print_system_info(system,parameters,i)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: i
		INTEGER :: j

		if(parameters%write_mean_energy_info) then
			call classical_MD_update_mean_energies(system,parameters &
							,parameters%counter_mean_energy_info)
			parameters%counter_mean_energy_info= &
					parameters%counter_mean_energy_info+1

				if(parameters%print_temperature_stride>0) then
					if(parameters%counter_mean_energy_info &
							>parameters%print_temperature_stride) then
						write(*,*)
						write(*,*) "----------------------------------------------"
						write(*,*) "STEP "//int_to_str(i)
						write(*,*) "SYSTEM INFO (last "//int_to_str(parameters%print_temperature_stride)//" steps):"
						write(*,*) "  temperature: " &
												,system%temp_estimated*kelvin, "K"
						write(*,*) "  potential_energy: " &
												,system%E_pot_mean*kcalpermol, "kCal/mol"
						write(*,*) "  kinetic_energy: " &
												,SUM(system%E_kin_mean)*kcalpermol, "kCal/mol"
						write(*,*) "  total_energy: " &
												,(system%E_pot_mean+SUM(system%E_kin_mean))*kcalpermol, "kCal/mol"
						if(parameters%n_dim==3) then
							write(*,*) "  pressure tensor: "
							DO j=1,3
								write(*,*) system%pressure_tensor_mean(j,:)
							ENDDO
						endif
						write(*,*)
						parameters%counter_mean_energy_info=1
						system%E_kin_mean=0
						system%temp_estimated=0
						system%E_pot_mean=0
					endif
				endif
			endif
	end subroutine classical_md_print_system_info


!---------------------------------------------
! COLLECTIVE COORDINATES

	subroutine classical_MD_initialize_collective_coord(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		
		SELECT CASE(parameters%collective_coord)
		CASE("x1")

			!ASSOCIATE update_collective_coord
			class_MD_update_collective_coord => classical_MD_collective_x1
			parameters%collective_coord_size=system%n_atoms

		CASE DEFAULT
			write(0,*) "Error: unknown collective coordinate '"//parameters%collective_coord//"' !"
			STOP "Execution stopped"
		END SELECT

		!ALLOCATE COLLECTIVE VARIABLES
		allocate( 	system%X_collective(parameters%collective_coord_size), &
					system%X_collective_grad(parameters%collective_coord_size, &
											system%n_atoms,system%n_dim) )			
		call class_MD_update_collective_coord(system,parameters)

	end subroutine classical_MD_initialize_collective_coord

    subroutine classical_MD_collective_x1(system,parameters)
        IMPLICIT NONE
        CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i

        system%X_collective(:)=system%X(:,1)

		system%X_collective_grad=0
		do i=1,system%n_atoms
			system%X_collective_grad(i,i,1)=1
		enddo
    end subroutine classical_MD_collective_x1

!-------------------------------------------------------
! MEAN VALUES

    subroutine classical_MD_update_mean_force(system,parameters,n)
        IMPLICIT NONE
        CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n

		call class_MD_update_collective_coord(system,parameters)

        system%FE_force(:)=parameters%FE_kappa(:)*(system%X_collective(:) - system%FE_Z(:)) 
        
        system%FE_mean_force(:)=system%FE_mean_force &
            +(system%FE_force(:)-system%FE_mean_force)/real(n,wp)
        
        system%FE_mean_force2(:)=system%FE_mean_force2 &
            +(system%FE_force(:)**2-system%FE_mean_force2)/real(n,wp)

        system%FE_std_dev_force(:)=sqrt( (system%FE_mean_force2-system%FE_mean_force**2) &
                                        /real(n,wp) )
    
    end subroutine classical_MD_update_mean_force
!----------------------------------------------------------

	subroutine classical_MD_compute_dipole(system,parameters)
		IMPLICIT NONE
    CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i,j
		REAL(wp), allocatable :: m(:)

		allocate(m(system%n_atoms))
		if(parameters%mass_normalized) then
			m=sqrt(system%mass)
		else
			m=system%mass
		endif

		system%dipole=0._wp
		system%dipole_velocity=0._wp
		DO j=1,system%n_dim ; DO i=1,system%n_atoms
			system%dipole(j)=system%dipole(j) &
					+system%charges(i)*system%X(i,j)/system%n_atoms

			system%dipole_velocity(j)=system%dipole_velocity(j) &
					+system%charges(i)*system%P(i,j)/(m(i)*system%n_atoms)
		ENDDO ; ENDDO

		deallocate(m)
	
	end subroutine classical_MD_compute_dipole
!----------------------------------------------------------
! RESTART FILE

	subroutine classical_MD_initialize_restart_file(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		parameters%restart_stride=param_library%get("PARAMETERS/save_stride",default=-1)
		!! @input_file PARAMETERS/save_stride (classical_MD,default=-1)
		if(parameters%restart_stride>0) then
			parameters%write_restart=.TRUE.
		else
			parameters%write_restart=.FALSE.
		endif
		
	end subroutine classical_MD_initialize_restart_file


	subroutine classical_MD_write_restart_file(system,parameters,n_steps)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(in) :: system
		CLASS(CLASS_MD_PARAM), INTENT(in) :: parameters
		INTEGER, intent(in) :: n_steps
		INTEGER :: u

		open(NEWUNIT=u,file=output%working_directory//"/RESTART")
			WRITE(u,*) parameters%engine
			WRITE(u,*) system%time
			WRITE(u,*) n_steps
			WRITE(u,*) system%X
			WRITE(u,*) system%P		
		close(u)

		write(*,*) "I/O : save config OK."

	end subroutine classical_MD_write_restart_file

	subroutine classical_MD_load_restart_file(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,ios
		CHARACTER(100) :: engine

		open(NEWUNIT=u,file=output%working_directory//"/RESTART", STATUS="OLD", IOSTAT=ios)
		if(ios/=0) STOP "Error: could not open RESTART file"			

			READ(u,*) engine
			if(engine/=parameters%engine) then
				write(0,*) "Error: tried to restart engine '",parameters%engine &
							,"' with a file from '",engine,"' !"
				STOP "Execution stopped"
			endif
			READ(u,*) system%time
			READ(u,*) parameters%n_steps_prev
			READ(u,*) system%X
			READ(u,*) system%P	

		close(u)

		write(*,*) "I/O : configuration retrieved successfully."

	end subroutine classical_MD_load_restart_file

	function classical_MD_check_exit_file(self) RESULT(file_exist)
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: self
		LOGICAL :: file_exist

		INQUIRE(FILE=output%base_directory//"/EXIT",EXIST=file_exist)

	end function classical_MD_check_exit_file

!--------------------------------------------------

	subroutine classical_MD_write_mean_force_info(system,parameters)
		USE basic_types, only:Pot,dPot
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(in) :: system
		CLASS(CLASS_MD_PARAM), INTENT(in) :: parameters
		INTEGER :: u
		REAL(wp) :: E
		REAL(wp), allocatable :: X(:,:),F(:,:)
		LOGICAL :: write_Pot

		write_Pot=.FALSE.
		if(system%n_dim==1 .AND. parameters%collective_coord=="x1") then
			allocate(X(system%n_atoms,1),F(system%n_atoms,1))
			X(:,1)=system%FE_Z(:)
			E=Pot(X)
			F=dPot(X)
			write_Pot=.TRUE.
		endif

		open(NEWUNIT=u,file=output%working_directory//"/mean_force.info")
			if(write_Pot) then
				WRITE(u,*) system%FE_Z(:),system%FE_mean_force(:),E,F(:,1)
			else
				WRITE(u,*) system%FE_Z(:),system%FE_mean_force(:)
			endif
		close(u)

		write(*,*) "I/O : mean_force.info -> OK."

	end subroutine classical_MD_write_mean_force_info

!---------------------------------------------------------------
SUBROUTINE classical_md_apply_eckart( system,parameters )
		!! ADAPTED FROM PaPIM !!

    !> @brief Application of Eckart's conditions to the sampled phase space point i.e. 
    !> centering the molecule within its center of mass and removing all translational
    !> and rotational components of velocity.
    !> 
    !> @detail This subroutine calls external "lapack" procedure "dsysv" for solving a set of linear 
    !> equations. Failure with compiling this subroutine my be due to linking of this 
    !> external subroutine. Please check the "lapack" linking fags in the Makefile.
    !>
    IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(INOUT) :: system
		CLASS(CLASS_MD_PARAM), INTENT(INOUT) :: parameters

    !! Local variables
    INTEGER :: i_atom
    INTEGER :: num_of_atoms
    INTEGER :: i_index
    INTEGER :: j_index
    INTEGER, DIMENSION(3) :: ipiv
    INTEGER :: error_info
    REAL( wp ) :: r2
    REAL( wp ) :: reduced_mass
    REAL( wp ), DIMENSION(3) :: center_of_mass
    REAL( wp ), DIMENSION(3) :: cm_velocity
    REAL( wp ), DIMENSION(3) :: total_angular_momentum
    REAL( wp ), DIMENSION(3) :: bond
    REAL( wp ), DIMENSION(3) :: work
    REAL( wp ), DIMENSION(3,3) :: moment_of_inertia
		REAL(wp) :: total_mass_inv

		if(parameters%mass_normalized) &
			STOP "Error: Eckart conditions not compatible with mass normalized momenta"

		if(system%n_dim < 3) return
    !--------------------------------------------------------------------------

    num_of_atoms = system%n_atoms
		total_mass_inv = 1._wp / system%total_mass

    center_of_mass = 0.0_wp
    cm_velocity = 0.0_wp
    DO i_atom = 1, num_of_atoms
        center_of_mass = center_of_mass + system%X(i_atom,:) * &
                                         &system%mass(i_atom)
        cm_velocity = cm_velocity + system%P(i_atom,:)
    END DO
    center_of_mass = center_of_mass * total_mass_inv
    cm_velocity = cm_velocity * total_mass_inv
    DO i_atom = 1, num_of_atoms
        system%X(i_atom,:) = system%X(i_atom,:) - &
                                &center_of_mass
        system%P(i_atom,:) = system%P(i_atom,:) - &
                                &cm_velocity * system%mass(i_atom)
    END DO


    IF( num_of_atoms > 2 ) THEN

        total_angular_momentum = 0.0_wp
        moment_of_inertia = 0.0_wp
        DO i_atom = 1, num_of_atoms
            total_angular_momentum = total_angular_momentum + cross_product( &
                                    system%X(i_atom,:), system%P(i_atom,:) )
            r2 = DOT_PRODUCT( system%X(i_atom,:), system%X(i_atom,:) )
            DO i_index = 1, 3
                DO j_index = i_index, 3
                    IF( i_index == j_index ) THEN
                        moment_of_inertia(i_index,j_index) = moment_of_inertia(i_index,j_index) + &
                            &system%mass(i_atom) * (r2 - system%X(i_atom,i_index) ** 2)
                    ELSE
                        moment_of_inertia(i_index,j_index) = moment_of_inertia(i_index,j_index) - &
                            &system%mass(i_atom) * system%X(i_atom,i_index) * &
                            &system%X(i_atom,j_index)
                        moment_of_inertia(j_index,i_index) = moment_of_inertia(i_index,j_index)
                    END IF
                END DO
            END DO
        END DO

        !! Optionally save the total angular momentum
        !IF( save_cm_prop ) trajectory%angular_momentum = total_angular_momentum

        !! Call to external library subroutine
        !! Lapack subroutine for solving the system of linear equations. The input "variable total angular
        !! momentum" takes the output value of "total nagular velocity" or "omega" of the system
        CALL dsysv( 'u', 3, 1, moment_of_inertia, 3, ipiv, total_angular_momentum, 3, work, 3, error_info )
        !! Call to external library subroutine
        IF( error_info /= 0 ) STOP  "Error in eckart_conditions. Matrix Inversion Problem." 

        DO i_atom = 1, num_of_atoms
            system%P(i_atom,:) = system%P(i_atom,:) - &
                &system%mass(i_atom) * cross_product( total_angular_momentum, &
                &system%X(i_atom,:) )
        END DO

    ELSE IF( num_of_atoms == 2 ) THEN

        bond = system%X(1,:) - system%X(2,:)
        r2 = DOT_PRODUCT( bond, bond )

        reduced_mass = system%mass(1) * system%mass(2) * total_mass_inv

        system%X(1,:) = bond * system%mass(2) * total_mass_inv
        system%X(2,:) = -bond * system%mass(1) * total_mass_inv
 
        total_angular_momentum = cross_product( bond, system%P(1,:) )

        !! Optionally save the total angular momentum
        !IF( save_cm_prop ) trajectory%angular_momentum = total_angular_momentum

        total_angular_momentum = total_angular_momentum / ( r2 * reduced_mass )

        total_angular_momentum = cross_product( total_angular_momentum, bond )
 
        system%P(1,:) = system%P(1,:) - reduced_mass * &
                                       &total_angular_momentum
        system%P(2,:) = -system%P(1,:)

    END IF


  END SUBROUTINE classical_md_apply_eckart

END MODULE classical_md_types