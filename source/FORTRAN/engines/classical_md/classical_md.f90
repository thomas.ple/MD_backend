MODULE classical_md
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE random
	USE classical_md_types
	USE classical_md_integrators
	USE thermostats, only : initialize_thermostat, finalize_thermostat, thermostat_end_thermalization
	USE periodic_table
	USE matrix_operations, only: sym_mat_diag
	IMPLICIT NONE	

	TYPE(CLASS_MD_SYS), save, target :: class_MD_system
	TYPE(CLASS_MD_PARAM), save, target :: class_MD_parameters

	PROCEDURE(class_MD_sub), pointer :: class_MD_integrator

	PRIVATE :: class_MD_system, class_MD_parameters, class_MD_update_mean_values

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine classical_md_loop()
		IMPLICIT NONE
		INTEGER :: i

		if(simulation_box%use_pbc) &
			call simulation_box%apply_pbc(class_MD_system%X)

		if(class_MD_parameters%n_steps_therm > 0) then
			write(*,*) "Starting ",class_MD_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,class_MD_parameters%n_steps_therm
				if(class_MD_parameters%check_exit_file()) then
					!call classical_MD_write_restart_file(class_MD_system,class_MD_parameters,0)
					write(0,*) "I/O: EXIT file found during thermalization. Exiting properly."
					RETURN
				endif
				
				call class_MD_integrator(class_MD_system,class_MD_parameters)

				if(simulation_box%use_pbc) &
					call simulation_box%apply_pbc(class_MD_system%X)
				if(class_MD_parameters%apply_eckart_conditions) &
					call classical_md_apply_eckart(class_MD_system,class_MD_parameters)
				! COMPUTE KINETIC ENERGY
				if(class_MD_parameters%compute_E_kin) &
					call classical_MD_compute_kinetic_energy(class_MD_system,class_MD_parameters)
				call classical_md_print_system_info(class_MD_system,class_MD_parameters,i)
			ENDDO
			write(*,*) "Thermalization done."
			call classical_MD_write_restart_file(class_MD_system,class_MD_parameters, 0)
			call thermostat_end_thermalization(class_MD_system,class_MD_parameters)
		endif

		write(*,*)
		if(class_MD_parameters%cmd_force) then
			write(*,*) "Starting ",class_MD_parameters%n_steps," steps &
						&of Centroid MD"
		else
			write(*,*) "Starting ",class_MD_parameters%n_steps," steps &
						&of classical MD"
		endif
		DO i=1,class_MD_parameters%n_steps
			! CHECK IF EXIT FILE
			if(class_MD_parameters%check_exit_file()) then
				call classical_MD_write_restart_file(class_MD_system,class_MD_parameters &
					,class_MD_parameters%n_steps_prev+i)
				write(0,*) "I/O: EXIT file found. Writing RESTART and exiting properly."
				RETURN
			endif

			! UPDATE TIME
			class_MD_system%time=class_MD_system%time &
								+class_MD_parameters%dt

			! INTEGRATION STEP
			call class_MD_integrator(class_MD_system,class_MD_parameters)

			! APPLY PERIODIC BOUNDARIES CONDITIONS
			if(simulation_box%use_pbc) &
				call simulation_box%apply_pbc(class_MD_system%X)

			! APPLY ECKART CONDITIONS TO REMOVE CENTER OF MASS MOTION 
			! AND GLOBAL ROTATIONS
			if(class_MD_parameters%apply_eckart_conditions) &
				call classical_md_apply_eckart(class_MD_system,class_MD_parameters)	

			! COMPUTE MEAN VALUES
			if(class_MD_parameters%on_the_fly_mean_values) &
				call class_MD_update_mean_values(class_MD_system,class_MD_parameters,i)
			
			! COMPUTE KINETIC ENERGY
			if(class_MD_parameters%compute_E_kin) &
				call classical_MD_compute_kinetic_energy(class_MD_system,class_MD_parameters)
			
			if(class_MD_parameters%compute_dipole) then
				if(mod(i,class_MD_parameters%compute_dipole_stride)==0) &
					call classical_MD_compute_dipole(class_MD_system,class_MD_parameters)
			endif
			
			! COMPUTE MEAN ENERGIES AND PRINT INFO 
			call classical_md_print_system_info(class_MD_system,class_MD_parameters,i)
			
			! WRITE TRAJECTORY
			call output%write_all()

			!WRITE RESTART FILE IF NEEDED
			if(class_MD_parameters%write_restart) then
				if(mod(i,class_MD_parameters%restart_stride)==0) then
					call classical_MD_write_restart_file(class_MD_system,class_MD_parameters &
						,class_MD_parameters%n_steps_prev+i)
				endif
			endif

		ENDDO
		write(*,*) "Simulation done."

		!WRITE RESTART FILE
		call classical_MD_write_restart_file(class_MD_system,class_MD_parameters &
					,class_MD_parameters%n_steps_prev+class_MD_parameters%n_steps)

		if(allocated(class_MD_system%FE_mean_force)) then
			write(*,*) "mean force=", class_MD_system%FE_mean_force*bohr
			write(*,*) "std dev force=", class_MD_system%FE_std_dev_force*bohr
			if(class_MD_parameters%write_mean_force_info) &
				call classical_MD_write_mean_force_info(class_MD_system,class_MD_parameters)
		endif
		if(class_MD_parameters%write_mean_energy_info) &
			call classical_MD_write_mean_energy_info(class_MD_system,class_MD_parameters)
		write(*,*) "Simulation done."

	end subroutine classical_md_loop

	subroutine classical_md_phonons()
		IMPLICIT NONE
		REAL(wp), ALLOCATABLE :: hess(:,:),eigvals(:),eigMat(:,:)
		INTEGER :: n_dof,n_atoms,n_dim,c,i,j,k,INFO,ufreq,uvec

		n_dof=class_MD_system%n_dof
		n_atoms=class_MD_system%n_atoms
		n_dim=class_MD_system%n_dim
		allocate(hess(n_dof,n_dof))

		WRITE(*,*) "HARMONIC FREQUENCIES:"

		call get_pot_info(class_MD_system%X,class_MD_system%E_pot &
												,class_MD_system%Forces &
												,hessian=hess)

    !COMPUTE MASS WEIGHTED HESSIAN
		c=0
		DO j=1,n_dim; do i=1,n_atoms
				c=c+1
				DO k=1,n_dim
						hess((k-1)*n_atoms+1:k*n_atoms,c) &
								=hess((k-1)*n_atoms+1:k*n_atoms,c) &
										/sqrt(class_MD_system%mass(i)*class_MD_system%mass(:))
				ENDDO
		ENDDO ; ENDDO

		allocate(eigvals(n_dof),eigMat(n_dof,n_dof))

		!DIAGONALIZE HESSIAN
    call sym_mat_diag(hess,eigvals,eigMat,INFO)            
    IF(INFO/=0) STOP "Error: could not diagonalize mass weighted hessian !"

		open(newunit=ufreq,file=output%working_directory//"/frequencies.out")
		open(newunit=uvec,file=output%working_directory//"/normal_modes.out")
		DO k=1,n_dof
			if(eigvals(k)<0) then
				write(*,*) k,-sqrt(-eigvals(k))*cm1,"cm^-1"
				write(ufreq,*) k,-sqrt(-eigvals(k))*cm1
			else
				write(*,*) k,sqrt(eigvals(k))*cm1,"cm^-1"
				write(ufreq,*) k, sqrt(eigvals(k))*cm1
			endif
			write(uvec,*) eigMat(k,:)
		ENDDO
		close(ufreq)
		close(uvec)
		
	
	end subroutine classical_md_phonons
!-----------------------------------------------------------------

	subroutine initialize_classical_MD(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart

		class_MD_parameters%engine="classical_md"

		call classical_MD_initialize_restart_file(class_MD_system,class_MD_parameters,param_library)		

		!GET ALGORITHM
		class_MD_parameters%algorithm=param_library%get("PARAMETERS/algorithm" &
										, default="nvt")
		!! @input_file PARAMETERS/algorithm (classical_MD, default="nvt")
		class_MD_parameters%algorithm=to_lower_case(class_MD_parameters%algorithm)

		!ASSOCIATE classical_MD_loop
		SELECT CASE(class_MD_parameters%algorithm)
		CASE("phonon","phonons")
			simulation_loop => classical_md_phonons
		CASE DEFAULT
			simulation_loop => classical_MD_loop
		END SELECT

		! GET PARAMETERS
		call classical_MD_get_parameters(class_MD_system,class_MD_parameters,param_library)
		call classical_MD_initialize_momenta(class_MD_system,class_MD_parameters)

		call classical_MD_get_integrator_parameters(class_MD_system,class_MD_parameters,param_library)

		! LOAD RESTART FILE IS NEEDED
		class_MD_parameters%n_steps_prev=0
		load_restart=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
		!! @input_file PARAMETERS/continue_simulation (classical_MD, default=.FALSE.)
		if(load_restart) then
			call classical_MD_load_restart_file(class_MD_system,class_MD_parameters)
			class_MD_parameters%n_steps_therm=0
		endif
		
		!INITIALIZE MEAN FORCE CALCULATION
		if(class_MD_parameters%algorithm=="mean_force") &
			call classical_MD_initialize_FE_mean_force(class_MD_system,class_MD_parameters,param_library)

		!INITIALIZE FORCES
		call classical_MD_initialize_forces(class_MD_system,class_MD_parameters,param_library)
		
		call classical_MD_initialize_output(class_MD_system,class_MD_parameters,param_library)

		if(class_MD_parameters%compute_dipole) &
			 call classical_MD_initialize_dipole(class_MD_system,class_MD_parameters,param_library)

	end subroutine initialize_classical_MD

	subroutine finalize_classical_MD()
		IMPLICIT NONE

		if(class_MD_parameters%algorithm/="nve") &
			call finalize_thermostat(class_MD_system,class_MD_parameters)

		call classical_MD_deallocate(class_MD_system,class_MD_parameters)

	end subroutine finalize_classical_MD

!-----------------------------------------------------------------

	! GET PARAMETERS FROM LIBRARY
	subroutine classical_MD_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dim,n_atoms,i,j,spos,length,n_atoms_of
		TYPE(DICT_STRUCT), pointer :: parameters_cat
		REAL(wp), allocatable :: X0_default(:)
		INTEGER, allocatable:: indices(:),element_indices(:)
		CHARACTER(:), ALLOCATABLE :: xyz_file,elements
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)

		parameters%on_the_fly_mean_values = .FALSE.

		parameters_cat => param_library%get_child("PARAMETERS")

		!GET n_dim AND n_atoms
		n_dim=parameters_cat%get("n_dim", default=n_dim_default)
		!! @input_file PARAMETERS/n_dim (classical_MD, default=n_dim_default)
		if(n_dim<=0) STOP "Error: 'n_dim' is not properly defined!"
		system%n_dim=n_dim
		parameters%n_dim=n_dim

		n_atoms=parameters_cat%get("n_atoms")
		!! @input_file PARAMETERS/n_atoms (classical_MD)
		if(n_atoms<=0) STOP "Error: 'n_atoms' is not properly defined!"
		system%n_atoms=n_atoms
		parameters%n_atoms=n_atoms
		
		system%n_dof=n_atoms*n_dim
		parameters%n_dof=system%n_dof		

		!GET INITIAL POSITION
		allocate(X0_default(n_atoms*n_dim))
		X0_default=0.0_wp
		X0_default=parameters_cat%get("xinit",default=X0_default)
		!! @input_file PARAMETERS/xinit (classical_MD,default=0.)		
		if(size(X0_default) /= n_dim*n_atoms) &
			STOP "Error: specified 'xinit' size does not match 'n_dim*n_atoms'"	
		allocate(system%X(n_atoms,n_dim))
		system%X=RESHAPE(X0_default,(/n_atoms,n_dim/))
			

		!GET mass
		allocate(system%mass(n_atoms))
		if(parameters_cat%has_key("mass")) then
			system%mass=parameters_cat%get("mass")
			!! @input_file PARAMETERS/mass (classical_MD,default=1 amu)	
			if(size(system%mass) /= n_atoms) &
				STOP "Error: specified 'mass' size does not match 'n_atoms'"
			do i=1,n_atoms
				if(system%mass(i)<=0) STOP "Error: 'mass' is not properly defined!"
			enddo		
		else
			write(0,*) "Warning: 'mass' not specified, using default of 1 a.m.u. for all dimensions."
			system%mass=Mprot
			call param_library%store("PARAMETERS/mass",system%mass)
		endif
		system%total_mass=SUM(system%mass)

		parameters%mass_normalized=param_library%get("THERMOSTAT/mass_normalized",default=.FALSE.)
		allocate(system%masskin(n_atoms))
		if(parameters%mass_normalized) then
			system%masskin(:)=1.
		else
			system%masskin=system%mass
		endif
		
		system%element_dict => param_library%get_child("ELEMENTS")		
		call dict_list_of_keys(system%element_dict,keys)
		system%n_elements=size(keys)
		allocate(system%element_set(system%n_elements))
		allocate(system%element_symbols(n_atoms))
		DO i=1,system%n_elements
			system%element_set(i)=system%element_dict%get(keys(i)//"/symbol")
			n_atoms_of=system%element_dict%get(keys(i)//"/n")
			indices=system%element_dict%get(keys(i)//"/indices")
			DO j=1,n_atoms_of
				system%element_symbols(indices(j))=system%element_set(i)
			ENDDO
		ENDDO

		! allocate(system%element_symbols(n_atoms), indices(n_atoms))
		! system%element_symbols="Xx"
		! if(parameters_cat%has_key("element_index")) then
		! 	indices=parameters_cat%get("element_index")
		! 	if(size(indices) /= n_atoms) &
		! 		STOP "Error: specified 'element_index' size does not match 'n_atoms'"
		! 	elements=""
		! 	system%n_elements=0
		! 	DO i=1,n_atoms
		! 		system%element_symbols(i)=get_atomic_symbol(indices(i))
		! 		if(index(elements,trim(system%element_symbols(i)))==0) then
		! 			system%n_elements=system%n_elements+1
		! 			elements=elements//"_"//trim(system%element_symbols(i))
		! 		endif
		! 	ENDDO
		! 	allocate(system%element_set(system%n_elements),element_indices(system%n_atoms))	
		! 	DO i=1,system%n_elements
		! 		length=len(elements)
		! 		spos=index(elements,"_",BACK=.TRUE.)
		! 		system%element_set(i)=elements(spos+1:length)
		! 		call system%element_dict%add_child(system%element_set(i))
		! 		if(spos>1) elements=elements(1:spos-1)
		! 		n_atoms_of=0
		! 		DO j=1,system%n_atoms
		! 			if(system%element_set(i) == system%element_symbols(j)) then
		! 				n_atoms_of=n_atoms_of+1
		! 				element_indices(n_atoms_of) = j 
		! 			endif
		! 		ENDDO
		! 		call system%element_dict%store(system%element_set(i)//"/n",n_atoms_of)
		! 		call system%element_dict%store(system%element_set(i)//"/indices",element_indices(1:n_atoms_of))
		! 	ENDDO
		! 	write(*,*) "n_elements= "//int_to_str(system%n_elements)//" -> ",system%element_set//" "
		! else
		! 	system%n_elements=1
		! 	allocate(system%element_set(system%n_elements))
		! 	allocate(element_indices(system%n_atoms))	
		! 	system%element_set(1)="Xx"
		! 	DO j=1,system%n_atoms ; element_indices(j)=j ; ENDDO
		! 	call system%element_dict%add_child(system%element_set(1))
		! 	call system%element_dict%store(system%element_set(1)//"/n",system%n_atoms)
		! 	call system%element_dict%store(system%element_set(1)//"/indices",element_indices(1:system%n_atoms))
		! endif
		call system%element_dict%print()

		allocate(system%dipole(system%n_dim) &
						,system%dipole_velocity(system%n_dim) )

		system%time=0._wp
		system%E_pot=0._wp
		system%E_pot_mean=0._wp
		system%E_pot_var=0._wp
		system%temp_estimated = 0._wp
		system%pressure_tensor_mean=0
		parameters%counter_mean_energy_info=1

		parameters%print_temperature_stride=100
		parameters%compute_dipole_stride=1
		parameters%compute_dipole=.FALSE.

		!SAMPLING PARAMETERS
		SELECT CASE(parameters%engine)
		CASE("classical_mc","schrodinger")

		CASE DEFAULT
			parameters%dt=parameters_cat%get("dt")
			!! @input_file PARAMETERS/dt (classical_MD)	
			if(parameters%dt<=0) STOP "Error: 'dt' is not properly defined!"
		END SELECT
		parameters%n_steps=parameters_cat%get("n_steps")
		!! @input_file PARAMETERS/n_steps (classical_MD)	
		if(parameters%n_steps<=0) STOP "Error: 'n_steps' is not properly defined!"
		parameters%n_steps_therm=parameters_cat%get("n_steps_therm",default=0)
		!! @input_file PARAMETERS/n_steps_therm (classical_MD, default=0)
		if(parameters%n_steps_therm<0) STOP "Error: 'n_steps_therm' is not properly defined!"	

		if(parameters%algorithm /= "nve") then

			parameters%temperature=parameters_cat%get("temperature") !WHICH IS ALREADY CONVERTED TO kbT
			!! @input_file PARAMETERS/temperature (classical_MD, if algorithm /= "nve")
			if(parameters%temperature<0) STOP "Error: 'temperature' is not properly defined!"
			parameters%beta=1._wp/parameters%temperature

		endif

		parameters%apply_eckart_conditions = parameters_cat%get("apply_eckart_conditions",default=.FALSE.)

		if(parameters%engine=="classical_md") then
			parameters%cmd_force=parameters_cat%get("use_cmd_force",default=.FALSE.)
		endif

	end subroutine classical_MD_get_parameters

	subroutine classical_MD_initialize_momenta(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		INTEGER :: i

		allocate(system%P(system%n_atoms,system%n_dim))		

		!initialize momenta
		SELECT CASE(parameters%algorithm)
		CASE("nvt","mean_force")

			do i=1,system%n_dim
				call randGaussN(system%P(:,i))
				system%P(:,i)=system%P(:,i)*SQRT(system%masskin(:)*parameters%temperature)
			enddo

		CASE("nve","phonons")
			system%P=0._wp

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

	end subroutine classical_MD_initialize_momenta

!-----------------------------------------------------------------

	subroutine classical_MD_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: parameters_cat

		parameters_cat => param_library%get_child("PARAMETERS")

		SELECT CASE(parameters%algorithm)
		CASE("nvt" ,"mean_force")

			parameters%integrator_type=parameters_cat%get("integrator" &
										, default="baoab")
			!! @input_file PARAMETERS/integrator (classical_MD, default="baoab" for algorithms "nvt","mean_force")
			parameters%integrator_type=to_lower_case(parameters%integrator_type)
			
			call initialize_thermostat(system,parameters,param_library)

			SELECT CASE(parameters%integrator_type)
			CASE("baoab")
				class_MD_integrator => class_MD_BAOAB
			CASE("aboba")
				class_MD_integrator => class_MD_ABOBA
			CASE("stochastic_verlet")
				class_MD_integrator => class_MD_stochastic_verlet
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT		
			
		CASE("nve")
			parameters%integrator_type=parameters_cat%get("integrator" &
										, default="velocity_verlet")
			!! @input_file PARAMETERS/integrator (classical_MD, default="velocity_verlet" for algorithms "nve")
			parameters%integrator_type=to_lower_case(parameters%integrator_type)

			SELECT CASE(parameters%integrator_type)
			CASE("euler")
				class_MD_integrator => class_MD_euler
			CASE("velocity_verlet")
				class_MD_integrator => class_MD_velocity_verlet
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT
		
		CASE("phonons")
			parameters%integrator_type="none"
		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine classical_MD_get_integrator_parameters

	subroutine classical_MD_initialize_FE_mean_force(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: free_energy_cat

		free_energy_cat => param_library%get_child("FREE_ENERGY")

		!SELECT COLLECTIVE COORDINATE
			parameters%collective_coord=free_energy_cat%get("collective_coord",default="x1")
			!! @input_file FREE_ENERGY/collective_coord (classical_MD, default="x1")
			parameters%collective_coord=to_lower_case(parameters%collective_coord)
			call classical_MD_initialize_collective_coord(system,parameters)
			allocate(	system%FE_Z(parameters%collective_coord_size), &
						parameters%FE_kappa(parameters%collective_coord_size)	)		

		!INITIALIZE MEAN_FORCE
			allocate(	system%FE_force(parameters%collective_coord_size), &
						system%FE_mean_force(parameters%collective_coord_size), &
						system%FE_mean_force2(parameters%collective_coord_size), &
						system%FE_std_dev_force(parameters%collective_coord_size) )
			system%FE_force(:)=0._wp
			system%FE_mean_force(:)=0._wp
			system%FE_mean_force2(:)=0._wp
			system%FE_std_dev_force(:)=0._wp

		!GET FORCE CENTER
			system%FE_Z=free_energy_cat%get("mf_center_coord")
			!! @input_file FREE_ENERGY/mf_center_coord (classical_MD)
			if(size(system%FE_Z) /= parameters%collective_coord_size) then
				write(0,*) "Error: 'MF_center_coord' size does not match selected &
					& collective coordinate of size",parameters%collective_coord_size," !"
				STOP "Execution stopped."
			endif

		!GET kappa
			parameters%FE_kappa=free_energy_cat%get("mf_kappa")
			!! @input_file FREE_ENERGY/mf_kappa (classical_MD)
			if(size(parameters%FE_kappa) /= parameters%collective_coord_size) then
				write(0,*) "Error: 'MF_kappa' size does not match selected &
					& collective coordinate of size",parameters%collective_coord_size," !"
				STOP "Execution stopped."
			endif
			IF( ANY(parameters%FE_kappa <= 0) ) &
				STOP "Error: 'MF_kappa' must be positive!"
		
		! ASSIGN update_mean_values
			class_MD_update_mean_values => classical_MD_update_mean_force
			parameters%on_the_fly_mean_values = .TRUE.

	end subroutine classical_MD_initialize_FE_mean_force

	subroutine classical_MD_initialize_dipole(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), POINTER :: charge_lib
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)
		REAL(wp) :: charge_tmp
		INTEGER :: i
		CHARACTER(2) :: symbol

		allocate(system%charges(system%n_atoms))
        
		if(.not. param_library%has_key("PARAMETERS/charges")) then
				STOP "Error: all charges must be specified in order to compute the dipole moment"
		endif

		charge_lib => param_library%get_child("PARAMETERS/charges")
		write(*,*)
		write(*,*) "Charges:"
		call dict_list_of_keys(charge_lib,keys,is_sub)
		DO i=1,size(keys)
			if(is_sub(i)) CYCLE
			charge_tmp=charge_lib%get(keys(i))
			symbol=trim(keys(i))
			symbol(1:1)=to_upper_case(symbol(1:1))
			write(0,*) symbol," : ", charge_tmp,"au"
		ENDDO
		write(*,*)
		do i=1, system%n_atoms
				system%charges(i)=charge_lib%get(trim(system%element_symbols(i)),default=-1001._wp)
				if(system%charges(i)<=-1000._wp) then
					write(0,*) "Error: incorrect charge for element "//trim(system%element_symbols(i))
					STOP "Execution stopped."
				endif             
		enddo

		!system%dipole=0
		!system%dipole_velocity=0
		call classical_MD_compute_dipole(system,parameters)

	
	end subroutine classical_MD_initialize_dipole

!-----------------------------------------------------------------

	subroutine classical_MD_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description,default_name,unit_tmp
		INTEGER :: i_file, i
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		parameters%write_mean_force_info=.FALSE.
		parameters%keep_potential=.FALSE.
		parameters%write_mean_energy_info=.FALSE.
		parameters%compute_E_kin=.FALSE.
		parameters%compute_dipole=.FALSE.

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)			
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("position","positions")
			!! @input_file OUTPUT/position (classical_MD)
				description="trajectory of positions"

				if(system%n_dim==1) then
					default_name="X.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X(:,1),index=i_file,name="positions")
				else
					default_name="position.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,name="positions")
				endif

				call output%files(i_file)%edit_description(description)

			CASE("momentum","momenta")
			!! @input_file OUTPUT/momentum (classical_MD)			
				description="trajectory of momenta"
				if(system%n_dim==1) then
					default_name="P.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%P(:,1),index=i_file,name="momenta")
				else
					default_name="momentum.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%P,system%element_symbols,index=i_file,name="momenta")
				endif
				call output%files(i_file)%edit_description(description)

			CASE("force","forces")
				!! @input_file OUTPUT/momentum (classical_MD)			
					description="trajectory of the force"
					if(system%n_dim==1) then
						default_name="Force.traj"
						call output%create_file(dict,default_name,index=i_file)
	
						if(output%files(i_file)%has_time_column) &
							call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
						call output%add_array_to_file(system%Forces(:,1),index=i_file,name="Force")
					else
						default_name="Force.traj.xyz"
						call output%create_file(dict,default_name,index=i_file)
						call output%add_xyz_to_file(system%Forces,system%element_symbols,index=i_file,name="Force")
					endif
					call output%files(i_file)%edit_description(description)
			
			CASE("pos+mom","position+momentum","positions+momenta")
			!! @input_file OUTPUT/momentum (classical_MD)			
				description="trajectory of positions and momenta"
				if(system%n_dim==1) then
					default_name="trajectory.out"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					unit_tmp=dict%get("x_unit",default="au")
					call output%add_array_to_file(system%P(:,1),index=i_file,unit=unit_tmp,name="position")
					unit_tmp=dict%get("p_unit",default="au")
					call output%add_array_to_file(system%X(:,1),index=i_file,unit=unit_tmp,name="momenta")
				else
					default_name="pos+mom.traj.xyz"
					call output%create_file(dict,default_name,index=i_file)
					unit_tmp=dict%get("x_unit",default="au")
					call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,unit=unit_tmp,name="positions")
					unit_tmp=dict%get("p_unit",default="au")
					call output%add_xyz_to_file(system%P,system%element_symbols,index=i_file,unit=unit_tmp,name="momenta")
				endif
			
			CASE("fe_force")
			!! @input_file OUTPUT/fe_force (classical_MD)
				if(parameters%algorithm/="mean_force") CYCLE

				description="trajectory of free energy force"
				default_name="FE_force.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%FE_force(:),index=i_file,name="FE_force")

			CASE("fe_mean_force")	
			!! @input_file OUTPUT/fe_mean_force (classical_MD)
				if(parameters%algorithm/="mean_force") CYCLE

				description="evolution of free energy mean force"
				default_name="FE_mean_force.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%FE_mean_force(:),index=i_file,name="FE_mean_force")

			CASE("mean_force_info")	
			!! @input_file OUTPUT/mean_force_info (classical_MD)
				if(parameters%algorithm/="mean_force") CYCLE
				parameters%write_mean_force_info=.TRUE.
			
			CASE("potential_energy","e_pot")	
			!! @input_file OUTPUT/potential_energy (classical_MD)
				description="trajectory of potential energy"
				default_name="potential_energy.traj"
				parameters%keep_potential=.TRUE.
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%E_pot,index=i_file,name="potential energy")
			
			CASE("dipole","dipole_moment")	
				parameters%compute_dipole=.TRUE.
				parameters%compute_dipole_stride=dict%get("stride",default=1)
			!! @input_file OUTPUT/dipole (classical_MD)
				description="trajectory of the dipole moment"
				default_name="dipole.traj"
				
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole,index=i_file,name="dipole")

				description="trajectory of the dipole moment velocity"
				default_name="dipole_velocity.traj"
				call dict%store("name",default_name)
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)
				

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_array_to_file(system%dipole_velocity,index=i_file,name="dipole_velocity")
			
			CASE("mean_energy_info")
			!! @input_file OUTPUT/mean_e_kin(classical_MD)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
				parameters%keep_potential=.TRUE.
			
			CASE("print_system_info")
			!! @input_file OUTPUT/mean_e_kin(classical_MD)
				parameters%write_mean_energy_info=.TRUE.
				parameters%compute_E_kin=.TRUE.
				parameters%keep_potential=.TRUE.
				parameters%print_temperature_stride=dict%get("stride",default=100)
			
			! CASE("position_xyz","positions_xyz")
			! !! @input_file OUTPUT/position (classical_MD)
			! 	description="trajectory of positions in xyz format"
			! 	default_name="pos.xyz"
			! 	call output%create_file(dict,default_name,index=i_file)
			! 	call output%files(i_file)%edit_description(description)

			! 	call output%add_xyz_to_file(system%X,system%element_symbols,index=i_file,name="positions xyz")

			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

		if(parameters%compute_E_kin) then
			allocate(system%E_kin(system%n_dof))
			system%E_kin=0._wp
		endif
		if(parameters%write_mean_energy_info) then
			allocate(system%E_kin_mean(system%n_dof))
			system%E_kin_mean=0._wp
		endif

	end subroutine classical_MD_initialize_output

END MODULE classical_md