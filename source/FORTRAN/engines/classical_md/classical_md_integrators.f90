MODULE classical_md_integrators
    USE classical_md_types
		USE random
		USE nested_dictionaries
    USE thermostats, only : apply_thermostat
    USE basic_types
		USE atomic_units
		USE centroid_forces, only: compute_cmd_forces, initialize_auxiliary_simulation
    IMPLICIT NONE

	PRIVATE :: apply_A,apply_B

	PROCEDURE(class_MD_sub), pointer :: class_MD_compute_forces => null()

CONTAINS

    ! INTEGRATORS

	subroutine class_MD_velocity_verlet(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		REAL(wp) :: dt
		REAL(wp), allocatable, save :: Forces_old(:,:)
		INTEGER :: i
		REAL(wp), allocatable :: m(:)

		allocate(m(system%n_atoms))
		if(parameters%mass_normalized) then
			m=sqrt(system%mass)
		else
			m=system%mass
		endif
		
		! if(.not. allocated(Forces_old)) &
		! 	allocate(Forces_old(size(system%Forces,1),size(system%Forces,2)))
		! Forces_old=system%Forces
		! dt=parameters%dt

		! write(*,*) "BEFORE MOVING FORCES, DX"
		! DO i=1,system%n_atoms
		! 	write(*,'(i3.3,6E15.7)') i,system%Forces(i,:), (dt*system%P(i,:) &
		! 			+ 0.5_wp*dt*dt*system%Forces(i,:))/system%mass(i)
		! enddo
		
		! do i=1,system%n_dim
		! 	system%X(:,i)=system%X(:,i) + (dt*system%P(:,i) &
		! 			+ 0.5_wp*dt*dt*system%Forces(:,i))/system%mass(:)
		! enddo	

		! ! write(*,*) "BEFORE CALLING FORCES"
		! ! DO i=1,system%n_atoms
		! ! 	write(*,'(i3.3,6E15.7)') i,system%Forces(i,:), system%X(i,:)*bohr
		! ! enddo

		! call class_MD_compute_forces(system,parameters)
		! !system%Forces=-dPot(system%X)

		! system%P = system%P + 0.5_wp*dt*(Forces_old+system%Forces)

		system%P=system%P + 0.5_wp*parameters%dt*system%Forces
		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + parameters%dt*system%P(:,i)/m(:)
		enddo
		call class_MD_compute_forces(system,parameters)
		system%P=system%P + 0.5_wp*parameters%dt*system%Forces
		

	end subroutine class_MD_velocity_verlet

	subroutine class_MD_euler(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		INTEGER :: i
		REAL(wp), allocatable :: m(:)

		allocate(m(system%n_atoms))
		if(parameters%mass_normalized) then
			m=sqrt(system%mass)
		else
			m=system%mass
		endif

		call class_MD_compute_forces(system,parameters)
		!system%Forces=-dPot(system%X)
		do i=1,system%n_dim
			system%X(:,i) = system%X(:,i) + parameters%dt*system%P(:,i)/m(:)
			system%P(:,i) = system%P(:,i) + parameters%dt*system%Forces(:,i)
		enddo

	end subroutine class_MD_euler

	subroutine class_MD_BAOAB(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters

		CALL apply_B(system,parameters,0.5_wp*parameters%dt)

		CALL apply_A(system,parameters,parameters%dt/2._wp)
		CALL apply_thermostat(system,parameters)
		CALL apply_A(system,parameters,parameters%dt/2._wp)

		call class_MD_compute_forces(system,parameters)
	!	system%Forces=-dPot(system%X)

		CALL apply_B(system,parameters,0.5_wp*parameters%dt)	

	end subroutine class_MD_BAOAB

	subroutine class_MD_ABOBA(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters

	!	CALL apply_A(system,parameters,parameters%dt/2._wp)
		call class_MD_compute_forces(system,parameters)
	!	system%Forces=-dPot(system%X)
		CALL apply_B(system,parameters,parameters%dt/2._wp)		
		CALL apply_thermostat(system,parameters)
		CALL apply_B(system,parameters,parameters%dt/2._wp)	
		CALL apply_A(system,parameters,parameters%dt)

	end subroutine class_MD_ABOBA

	subroutine class_MD_stochastic_verlet(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters

		CALL apply_thermostat(system,parameters)
		call class_MD_velocity_verlet(system,parameters)	
		CALL apply_thermostat(system,parameters)

	end subroutine class_MD_stochastic_verlet

	subroutine apply_A(system,parameters,tau)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i
		REAL(wp), allocatable :: m(:)

		allocate(m(system%n_atoms))
		if(parameters%mass_normalized) then
			m=sqrt(system%mass)
		else
			m=system%mass
		endif

		do i=1,system%n_dim
			system%X(:,i)=system%X(:,i) + tau*system%P(:,i)/m(:)
		enddo

	end subroutine apply_A

	subroutine apply_B(system,parameters,tau)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau

		system%P=system%P + tau*system%Forces
		
	end subroutine apply_B

!---------------------------------------------
! FORCES CALCULATORS

	subroutine classical_MD_initialize_forces(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(CLASS_MD_PARAM), INTENT(inout) :: parameters
		CLASS(DICT_STRUCT), INTENT(in) :: param_library
		! ASSIGN FORCES CALCULATOR	
		if(parameters%cmd_force) then
			class_MD_compute_forces => CMD_potential
			call initialize_auxiliary_simulation(system,parameters,param_library)
		elseif(parameters%algorithm == "mean_force") then
			class_MD_compute_forces => class_MD_potential_constrained
		else
			class_MD_compute_forces => class_MD_potential_only
		endif
		if(.not. allocated(system%Forces)) &
			allocate(system%Forces(system%n_atoms,system%n_dim))
		

		call class_MD_compute_forces(system,parameters)

	end subroutine classical_MD_initialize_forces

	subroutine class_MD_potential_only(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		INTEGER :: i

		call get_pot_info(system%X,system%E_pot,system%Forces)
		! DO i=1,system%n_atoms
		! 	write(*,'(i3.3,6E15.7)') i,system%Forces(i,:), system%X(i,:)*bohr
		! enddo
		!if(parameters%keep_potential) system%E_pot=Pot(system%X)
		!system%Forces=-dPot(system%X)
		if(parameters%mass_normalized) then
			DO i=1,system%n_dim
				system%Forces(:,i)=system%Forces(:,i)/sqrt(system%mass(:))
			ENDDO
		endif

	end subroutine class_MD_potential_only

	subroutine CMD_potential(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		INTEGER :: i

		call compute_cmd_forces(SYSTEM,parameters)

		if(parameters%mass_normalized) then
			DO i=1,system%n_dim
				system%Forces(:,i)=system%Forces(:,i)/sqrt(system%mass(:))
			ENDDO
		endif

	end subroutine CMD_potential

	subroutine class_MD_potential_constrained(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), intent(inout) :: system
		CLASS(CLASS_MD_PARAM), intent(inout) :: parameters
		INTEGER :: i

		call class_MD_update_collective_coord(system,parameters)

		if(parameters%keep_potential) system%E_pot=Pot(system%X) &
				+0.5_wp*SUM(parameters%FE_kappa(:)*(system%X_collective(:) - system%FE_Z(:))**2)
		
		system%Forces=-dPot(system%X)
		do i=1,parameters%collective_coord_size
			system%Forces= system%Forces - parameters%FE_kappa(i) * &
				(system%X_collective(i) - system%FE_Z(i))*system%X_collective_grad(i,:,:)
		enddo

		if(parameters%mass_normalized) then
			DO i=1,system%n_dim
				system%Forces(:,i)=system%Forces(:,i)/sqrt(system%mass(:))
			ENDDO
		endif

	end subroutine class_MD_potential_constrained

END MODULE classical_md_integrators    
