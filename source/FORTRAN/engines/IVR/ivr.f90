MODULE ivr
	USE kinds
	USE basic_types
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE string_operations
	USE matrix_operations
	USE random
	USE classical_md
	USE classical_md_types
	USE rpmd_types
	USE ring_polymer_md
	USE rpmd_integrators
	use fk_initial
	IMPLICIT NONE	

	TYPE, EXTENDS(CLASS_MD_PARAM) :: IVR_PARAM     
		CHARACTER(:), allocatable :: X_file, P_file, W_file
		CHARACTER(:), allocatable :: generator
		INTEGER :: X_unit, P_unit, W_unit
		REAL(wp) :: X_mult,P_mult,W_mult

		LOGICAL :: add_weight, formatted_input
		INTEGER :: n_skip
		LOGICAL :: generate_LGA,generate_PIMD,generate_FK
		INTEGER :: LGA_counter, LGA_n_draws

		REAL(wp), ALLOCATABLE :: P0(:,:)
		REAL(wp), ALLOCATABLE :: X0(:,:)
		REAL(wp), ALLOCATABLE :: Cpp(:,:,:)
		REAL(wp), ALLOCATABLE :: Ec(:,:,:)
		REAL(wp), ALLOCATABLE :: Epot(:)
		
		INTEGER :: xinit_file_unit, pinit_file_unit
		INTEGER :: isample,n_samples
		INTEGER :: write_stride
	END TYPE

	ABSTRACT INTERFACE
		function IVR_res_logical(system,parameters) result(iostat)
			import :: CLASS_MD_SYS, IVR_PARAM
			IMPLICIT NONE
			CLASS(CLASS_MD_SYS), intent(inout) :: system
			CLASS(IVR_PARAM), intent(inout) :: parameters
			LOGICAL :: iostat
		end function IVR_res_logical
	END INTERFACE

	TYPE(CLASS_MD_SYS), save, target :: ivr_system
	TYPE(IVR_PARAM), save, target :: ivr_parameters
	TYPE(RPMD_PARAM), save,target :: PI_param
	TYPE(POLYMER_TYPE), save,target :: PI_sys 
	PROCEDURE(IVR_res_logical), pointer :: ivr_get_next_state => null()

	PRIVATE
	PUBLIC :: initialize_IVR, finalize_IVR

CONTAINS

!-----------------------------------------------------------------
! MAIN LOOP
	subroutine ivr_loop()
		IMPLICIT NONE
		INTEGER :: i,n

		ivr_parameters%isample=1
		write(*,*)
		DO WHILE( ivr_get_next_state(ivr_system,ivr_parameters) )	
			if(mod(ivr_parameters%isample,ivr_parameters%write_stride)==0) then		
				write(*,'(A)') ACHAR(13)//"IVR : "//int_to_str(ivr_parameters%isample)//" initial conditions"
				call ivr_write_results(ivr_system,ivr_parameters)
			endif
			DO i=1,ivr_parameters%n_steps
				call ivr_update_observables(ivr_system,ivr_parameters,ivr_parameters%isample,i)
				call class_MD_integrator(ivr_system,ivr_parameters)
			ENDDO
			ivr_parameters%isample=ivr_parameters%isample+1
		ENDDO
		write(*,*)
		call ivr_write_results(ivr_system,ivr_parameters)

	end subroutine ivr_loop
!-----------------------------------------------------------------

	subroutine initialize_IVR(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: load_restart
		INTEGER :: i

		ivr_parameters%engine="ivr"
		ivr_parameters%algorithm="nve"

		!ASSOCIATE classical_MD_loop
		simulation_loop => ivr_loop

		! GET PARAMETERS
		call classical_MD_get_parameters(ivr_system,ivr_parameters,param_library)
		allocate(ivr_system%P(ivr_system%n_atoms,ivr_system%n_dim))
		call classical_MD_get_integrator_parameters(ivr_system,ivr_parameters,param_library)
		!INITIALIZE FORCES
		call classical_MD_initialize_forces(ivr_system,ivr_parameters,param_library)

		ivr_parameters%generator=to_upper_case(TRIM( &
								param_library%get("IVR/generator",default="LGA") ))
		ivr_parameters%generate_PIMD = .FALSE.
		ivr_parameters%generate_LGA=.FALSE.
		ivr_parameters%generate_FK=.FALSE.
		SELECT CASE(ivr_parameters%generator)
		CASE("LGA")
			ivr_parameters%generate_PIMD = .TRUE.
			ivr_parameters%generate_LGA=.TRUE.
		CASE("FK_LPI")
			ivr_parameters%generate_PIMD = .TRUE.
			ivr_parameters%generate_FK=.TRUE.
		CASE("FILE")

		CASE("FILE+LGA")
			ivr_parameters%generate_LGA=.TRUE.
		CASE DEFAULT
			write(0,*) "Error: unknown generator '"//ivr_parameters%generator//"'."
			STOP "Execution stopped."
		END SELECT

		if(ivr_parameters%generate_PIMD) then
			PI_param%algorithm="nvt"
			call RPMD_get_parameters(PI_sys,PI_param,param_library)
			call RPMD_initialize_momenta(PI_sys,PI_param)
			call RPMD_initialize_normal_modes(PI_sys,PI_param)

			PI_param%dt=param_library%get("IVR/dt_pimd",default=ivr_parameters%dt)
			PI_param%n_steps=param_library%get("IVR/n_steps_between_samples")
			PI_param%n_steps_therm=param_library%get("IVR/n_steps_thermalization_pimd",default=PI_param%n_steps)
			ivr_parameters%n_samples=param_library%get("IVR/n_samples")
			ivr_parameters%write_stride=param_library%get("IVR/write_stride",default=1)

			call initialize_thermostat(PI_sys,PI_param,param_library)
			call RPMD_initialize_forces(PI_sys,PI_param)

			write(*,*) "Thermalization: "//int_to_str(PI_param%n_steps_therm)//" steps of PIMD."
			DO i=1,PI_param%n_steps_therm
				call RPMD_BAOAB(PI_sys,PI_param)
			ENDDO
		endif

		ivr_parameters%X_mult=atomic_units_get_multiplier( &
				trim(param_library%get("IVR/x_unit",default="angstrom")) )
		write(*,*) "X_unit=",ivr_parameters%X_mult
		ivr_parameters%P_mult=atomic_units_get_multiplier( &
				trim(param_library%get("IVR/p_unit",default="amu*angstrom/fs")) )
		ivr_parameters%W_mult=atomic_units_get_multiplier( &
				trim(param_library%get("IVR/w_unit",default="au")) )

		ivr_parameters%n_steps_therm=0

		call ivr_initialize_results(ivr_system,ivr_parameters)
		if(ivr_parameters%generate_PIMD) then
			call ivr_initialize_LGA(ivr_system,ivr_parameters,param_library)
			if(ivr_parameters%generate_FK) call initialize_fk(param_library)
			ivr_get_next_state => ivr_get_next_state_PIMD
		else
			call ivr_open_trajectory_files(ivr_system,ivr_parameters,param_library)
		endif

	end subroutine initialize_IVR

	subroutine finalize_IVR()
		IMPLICIT NONE

		call classical_MD_deallocate(ivr_system,ivr_parameters)

		call ivr_close_trajectory_files(ivr_system,ivr_parameters)

	end subroutine finalize_IVR

!-----------------------------------------------------------------

	subroutine ivr_initialize_results(system,parameters)
		IMPLICIT NONE 
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		INTEGER :: n_atoms,n_dim,n_steps

		n_dim=system%n_dim
		n_atoms=system%n_atoms
		n_steps=parameters%n_steps
		allocate(parameters%P0(n_atoms,n_dim) &
				,parameters%X0(n_atoms,n_dim) &
				,parameters%Cpp(n_atoms,n_dim,n_steps) &
				,parameters%Ec(n_atoms,n_dim,n_steps) &
				,parameters%Epot(n_steps)	)
		parameters%Cpp=0._wp
		parameters%Ec=0._wp
		parameters%Epot=0._wp

	end subroutine ivr_initialize_results

	subroutine ivr_open_trajectory_files(system,parameters,param_library)
		IMPLICIT NONE 
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: file_exists
		CHARACTER(:), ALLOCATABLE :: access,form,input_unit

		!GET n_skip
		parameters%n_skip=param_library%get("IVR/stride",default=1)
		

		!GET FILE NAMES
		parameters%X_file=trim(param_library%get("IVR/x_file" &
					,default=output%working_directory//"/X.traj"))
		parameters%P_file=trim(param_library%get("IVR/p_file" &
					,default=output%working_directory//"/P.traj"))
		parameters%W_file=trim(param_library%get("IVR/w_file" &
								,default="NONE"))
		
		!CHECK IF FILES EXIST
		INQUIRE(file=parameters%X_file,exist=file_exists)
		if(.not. file_exists) then
			write(0,*) "Error: file "//parameters%X_file//" does not exist!"
			STOP "Execution stopped."
		endif

		if(parameters%generate_LGA) then
			call ivr_initialize_LGA(system,parameters,param_library)
		else
			INQUIRE(file=parameters%P_file,exist=file_exists)
			if(.not. file_exists) then
				write(0,*) "Error: file "//parameters%P_file//" does not exist!"
				STOP "Execution stopped."
			endif
		endif

		if(parameters%W_file /= "NONE") then
			parameters%add_weight=.TRUE.
			INQUIRE(file=parameters%W_file,exist=file_exists)
			if(.not. file_exists) then
				write(0,*) "Error: file "//parameters%W_file//" does not exist!"
				STOP "Execution stopped."
			endif
		endif

		!OPEN FILES
		parameters%formatted_input=param_library%get("IVR/formatted_input",default=.FALSE.)
		if(parameters%formatted_input) then
			form="formatted"
			access="sequential"
			ivr_get_next_state => ivr_get_next_state_formatted
		else
			form="unformatted"
			access="stream"
			ivr_get_next_state => ivr_get_next_state_unformatted
		endif
		open(newunit=parameters%X_unit, file=parameters%X_file, form=form, access=access)
		if(.not.parameters%generate_LGA) &
			open(newunit=parameters%P_unit, file=parameters%P_file, form=form, access=access)
		if(parameters%add_weight) &
			open(newunit=parameters%W_unit, file=parameters%W_file, form=form, access=access)


	end subroutine ivr_open_trajectory_files

	subroutine ivr_initialize_LGA(system,parameters,param_library)
		IMPLICIT NONE 
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		LOGICAL :: file_exists

		if(parameters%generate_LGA) then
			write(*,*) "Generating momentum with Local Gaussian Approximation"
		elseif(parameters%generate_FK) then
		  write(*,*) "Generating initial conditions with FK-LPI"
		endif
		parameters%temperature=param_library%get("PARAMETERS/temperature") !WHICH IS ALREADY CONVERTED TO kbT
		!! @input_file PARAMETERS/temperature (classical_MD, if algorithm /= "nve")
		if(parameters%temperature<=0) STOP "Error: 'temperature' is not properly defined!"
		parameters%beta=1._wp/parameters%temperature

		INQUIRE(file=output%working_directory//"/X_init.out",exist=file_exists)
		if(file_exists) then
			OPEN(newunit=parameters%xinit_file_unit &
			,file=output%working_directory//"/X_init.out")
			CLOSE(parameters%xinit_file_unit,STATUS="DELETE")
		endif
		OPEN(newunit=parameters%xinit_file_unit &
			,file=output%working_directory//"/X_init.out" &
			,form="unformatted" &
			,access="stream")
		
		INQUIRE(file=output%working_directory//"/P_init.out",exist=file_exists)
		if(file_exists) then
			OPEN(newunit=parameters%pinit_file_unit &
			,file=output%working_directory//"/P_init.out")
			CLOSE(parameters%pinit_file_unit,STATUS="DELETE")
		endif
		OPEN(newunit=parameters%pinit_file_unit &
			,file=output%working_directory//"/P_init.out" &
			,form="unformatted" &
			,access="stream")
		

		parameters%LGA_n_draws=param_library%get("IVR/LGA_n_draws_from_x",default=1)
		parameters%LGA_counter = parameters%LGA_n_draws
		if(parameters%LGA_n_draws>1) then
			write(*,*) "LGA: "//int_to_str(parameters%LGA_n_draws)//" momentum draws for each position."
		endif
	end subroutine ivr_initialize_LGA

	subroutine ivr_close_trajectory_files(system,parameters)
		IMPLICIT NONE 
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters

		close(parameters%X_unit)
		if(parameters%generate_LGA) then
			close(parameters%pinit_file_unit)
			close(parameters%xinit_file_unit)
		else
			close(parameters%P_unit)	
		endif	
		if(parameters%add_weight) close(parameters%W_unit)

	end subroutine ivr_close_trajectory_files

	subroutine ivr_update_observables(system,parameters,n,t)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		INTEGER, INTENT(in) :: n,t
		REAL(wp) :: n_re
		INTEGER :: i

		n_re=real(n,wp)

		parameters%Cpp(:,:,t)=parameters%Cpp(:,:,t) &
			+ (parameters%P0(:,:)*system%P(:,:) &
				-parameters%Cpp(:,:,t))/n_re
		
		DO i=1,system%n_dim
			parameters%Ec(:,i,t)=parameters%Ec(:,i,t) &
				+( system%P(:,i)**2/(2._wp*system%mass(:)) &
					-parameters%Ec(:,i,t) )/n_re
		ENDDO

		parameters%Epot(t)=parameters%Epot(t) &
			+( Pot(system%X)-parameters%Epot(t) )/n_re

	end subroutine ivr_update_observables

	function ivr_get_next_state_formatted(system,parameters) result(iostat)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		INTEGER :: ios,i
		LOGICAL :: iostat
		LOGICAL :: read_now

		iostat=.FALSE.

		read_now = .TRUE.
		if(parameters%generate_LGA) then
			! parameters%LGA_counter=parameters%LGA_counter+1
			if(parameters%LGA_counter<parameters%LGA_n_draws) then
				read_now=.FALSE.
				parameters%LGA_counter=parameters%LGA_counter+1
			else
				parameters%LGA_counter=1
			endif
		endif

		!GET NEW START POSITION
		IF(read_now) then
			IF(parameters%n_skip-1>0) THEN
				DO i=1,parameters%n_skip-1
					READ(parameters%X_unit,*,iostat=ios) parameters%X0(:,:)
				ENDDO
			ENDIF
			READ(parameters%X_unit,*,iostat=ios) parameters%X0(:,:)
			if(ios/=0) return
			parameters%X0=parameters%X0/parameters%X_mult
		ENDIF
		system%X=parameters%X0

		IF(parameters%generate_LGA) then
			call ivr_generate_momentum_LGA(system,parameters)
		ELSE
			IF(parameters%n_skip-1>0) THEN
				DO i=1,parameters%n_skip-1
					READ(parameters%P_unit,*,iostat=ios)
				ENDDO
			ENDIF
			READ(parameters%P_unit,*,iostat=ios) system%P(:,:)
			if(ios/=0) return
			system%P=system%P/parameters%P_mult
		ENDIF		

		if(parameters%add_weight) then
			IF(read_now) then
				IF(parameters%n_skip-1>0) THEN
					DO i=1,parameters%n_skip-1
						READ(parameters%W_unit,*,iostat=ios)
					ENDDO
				ENDIF
				READ(parameters%W_unit,*,iostat=ios) parameters%P0(:,:)
				if(ios/=0) return
				!CONVERT TO ATOMIC UNITS
				parameters%P0=parameters%P0/parameters%W_mult
			ENDIF
		else
			parameters%P0=system%P
		endif
		
		IF(.not. parameters%generate_LGA) then
			call get_pot_info(system%X,system%E_pot,system%Forces)
		endif

		iostat=.TRUE.

	end function ivr_get_next_state_formatted

	function ivr_get_next_state_unformatted(system,parameters) result(iostat)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		INTEGER :: ios,i
		LOGICAL :: iostat,read_now

		iostat=.FALSE.

		read_now = .TRUE.
		if(parameters%generate_LGA) then
			! parameters%LGA_counter=parameters%LGA_counter+1
			if(parameters%LGA_counter<parameters%LGA_n_draws) then
				read_now=.FALSE.
				parameters%LGA_counter=parameters%LGA_counter+1
			else
				parameters%LGA_counter=1
			endif
		endif

		IF(read_now) then
			IF(parameters%n_skip-1>0) THEN
				DO i=1,parameters%n_skip-1
					READ(parameters%X_unit,iostat=ios) parameters%X0(:,:)
				ENDDO
			ENDIF
			READ(parameters%X_unit,iostat=ios) parameters%X0(:,:)
			if(ios/=0) return
			parameters%X0=parameters%X0/parameters%X_mult
		ENDIF
		system%X=parameters%X0

		IF(parameters%generate_LGA) then
			call ivr_generate_momentum_LGA(system,parameters)
		ELSE
			IF(parameters%n_skip-1>0) THEN
				DO i=1,parameters%n_skip-1
					READ(parameters%P_unit,iostat=ios) system%P(:,:)
				ENDDO
			ENDIF
			READ(parameters%P_unit,iostat=ios) system%P(:,:)
			if(ios/=0) return
			system%P=system%P/parameters%P_mult
		ENDIF

		if(parameters%add_weight) then
			IF(read_now) then
				IF(parameters%n_skip-1>0) THEN
					DO i=1,parameters%n_skip-1
						READ(parameters%W_unit,iostat=ios) parameters%P0(:,:)
					ENDDO
				ENDIF
				READ(parameters%W_unit,iostat=ios) parameters%P0(:,:)
				if(ios/=0) return
				!CONVERT TO ATOMIC UNITS
				parameters%P0=parameters%P0/parameters%W_mult
			ENDIF
		else
			parameters%P0=system%P
		endif

		IF(.not. parameters%generate_LGA) then
			call get_pot_info(system%X,system%E_pot,system%Forces)
		endif			

		iostat=.TRUE.		

	end function ivr_get_next_state_unformatted

	function ivr_get_next_state_PIMD(system,parameters) result(iostat)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		INTEGER :: ios,i,ibead
		REAL :: R
		LOGICAL :: iostat
		LOGICAL :: evolve_polymer
		REAL(wp), ALLOCATABLE :: Xc(:,:)

		iostat=.FALSE.
		if(parameters%isample>parameters%n_samples) return

		evolve_polymer = .TRUE.
		if(parameters%generate_LGA .OR. parameters%generate_FK) then
			! parameters%LGA_counter=parameters%LGA_counter+1
			if(parameters%LGA_counter<parameters%LGA_n_draws) then
				evolve_polymer=.FALSE.
				parameters%LGA_counter=parameters%LGA_counter+1
			else
				parameters%LGA_counter=1
			endif
		endif

		!GET NEW START POSITION
		IF(evolve_polymer) then
			DO i=1,PI_param%n_steps
				call RPMD_BAOAB(PI_sys,PI_param)
			ENDDO				
		ENDIF
		

		if(parameters%generate_LGA) then
			IF(evolve_polymer) then
				call random_number(R)
				ibead=floor(PI_sys%n_beads*R)+1
				parameters%X0=PI_sys%X_beads(ibead,:,:)
			ENDIF
			system%X=parameters%X0
			call ivr_generate_momentum_LGA(system,parameters)
			parameters%P0=system%P
		else
			ALLOCATE(Xc(PI_sys%n_atoms,PI_sys%n_dim))
			Xc(:,:)=0._wp
			DO i=1,PI_sys%n_beads
				Xc(:,:)=Xc(:,:)+PI_sys%X_beads(i,:,:)
			ENDDO
			Xc(:,:)=Xc(:,:)/REAL(PI_sys%n_beads,wp)
			call generate_fk_initial(Xc,parameters%X0,parameters%P0,reuse_om2=(.NOT. evolve_polymer))			
			system%P=parameters%P0
		endif

		write(parameters%xinit_file_unit) system%X*parameters%X_mult
		write(parameters%pinit_file_unit) system%P*parameters%P_mult !*bohr/(Mprot*fs)

		iostat=.TRUE.

	end function ivr_get_next_state_PIMD

	subroutine ivr_write_results(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		INTEGER :: t,u1,u2,u3,u4,i,j
		REAL(wp) :: time
		REAL(wp), ALLOCATABLE :: mCvv(:,:,:)

		OPEN(newunit=u1,file=output%working_directory//"/Cpp_ivr.out")
		OPEN(newunit=u4,file=output%working_directory//"/mCvv_ivr.out")
		OPEN(newunit=u2,file=output%working_directory//"/Ec_ivr.out")
		OPEN(newunit=u3,file=output%working_directory//"/Epot_ivr.out")

		allocate(mCvv(system%n_atoms,system%n_dim,parameters%n_steps))
		DO j=1,system%n_dim; DO i=1,system%n_atoms
			mCvv(i,j,:)=parameters%Cpp(i,j,:)/(2._wp*system%mass(i))
		ENDDO ; ENDDO

		DO t=1,parameters%n_steps
			time=(t-1)*parameters%dt*fs
			write(u1,*) time,parameters%Cpp(:,:,t)*parameters%P_mult**2
			write(u4,*) time,mCvv(:,:,t)
			write(u2,*) time,parameters%Ec(:,:,t)
			write(u3,*) time,parameters%Epot(t)
		ENDDO

		close(u1)
		close(u2)
		close(u3)
		close(u4)

	end subroutine ivr_write_results

	subroutine ivr_generate_momentum_LGA(system,parameters)
		IMPLICIT NONE
		CLASS(CLASS_MD_SYS), INTENT(inout) :: system
		CLASS(IVR_PARAM), INTENT(inout) :: parameters
		REAL(wp), ALLOCATABLE :: hess(:,:),eigvals(:),eigMat(:,:),Peig(:)
		REAL(wp) :: omega2,u,Q,k2
		INTEGER :: c, i,j,k,INFO

		allocate(hess(system%n_dof,system%n_dof))

		call get_pot_info(system%X,system%E_pot,system%Forces &
												,hessian=hess)

    !COMPUTE MASS WEIGHTED HESSIAN
		c=0
		DO j=1,system%n_dim; do i=1,system%n_atoms
				c=c+1
				DO k=1,system%n_dim
						hess((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
								=hess((k-1)*system%n_atoms+1:k*system%n_atoms,c) &
										/sqrt(system%mass(i)*system%mass(:))
				ENDDO
		ENDDO ; ENDDO

		allocate(eigvals(system%n_dof),eigMat(system%n_dof,system%n_dof))

		!DIAGONALIZE HESSIAN
    call sym_mat_diag(hess,eigvals,eigMat,INFO)            
    IF(INFO/=0) STOP "Error: could not diagonalize mass weighted hessian !"

		deallocate(hess)
		allocate(Peig(system%n_dof))
		!COMPUTE QUANTUM CORRECTION TO KINETIC ENERGY IN NORMAL MODES
		call randGaussN(Peig)
		 DO i=1,system%n_dof
		 	omega2=eigvals(i)
		 	if(omega2>=0) then 
		 		! HARMONIC WHEN REAL FREQUENCY
		 		u=0.5_wp*parameters%beta*sqrt(omega2)
		 		Q=u/tanh(u)
		 	else 
		 		! LIU & MILLER ANSATZ WHEN IMAGINARY FREQUENCY
		 		u=0.5_wp*parameters%beta*sqrt(-omega2)
        if(u<0.5*pi) then
          Q=u/tanh(u)
        else
          Q=tanh(u)/u
        endif
		 	endif
			Peig(i)=Peig(i)*sqrt(Q/parameters%beta)
		 ENDDO

		! !!! TESTING !!!
		!u=(1500/cm1)*0.5*parameters%beta
		!Q=u/tanh(u)
		!Peig=Peig*sqrt(Q/parameters%beta)
		!!!!!!!!!!!!!!!!!!!!!!!
!		 k2=1.03174281e-01 -1.68054204e-03*system%X(1,1) &
!		 	  +4.14265413e-05*system%X(1,1)**2 &
!		 		-9.93042094e-07*system%X(1,1)**3 &
!		 		+2.99170861e-08*system%X(1,1)**4 &
!		 		-9.68121238e-10*system%X(1,1)**5
!		 system%P=RESHAPE(Peig/sqrt(k2), (/system%n_atoms,system%n_dim/))
		!!! TESTING !!!

		! TRANSFORM BACK
		system%P = RESHAPE(matmul(EigMat,Peig) &
							,(/system%n_atoms,system%n_dim/) )
		DO i=1,system%n_dim
			system%P(:,i)=system%P(:,i)*sqrt(system%mass(:))
		ENDDO
			
	end subroutine ivr_generate_momentum_LGA

END MODULE ivr
