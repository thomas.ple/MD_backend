module fk_initial
  use kinds
  USE nested_dictionaries
  USE random, only: RandGaussN
  USE basic_types
  USE matrix_operations
  USE atomic_units
  implicit none
  INTEGER, SAVE :: nat,ndim, ndof, maxiter,nsteps_om2
  REAL(wp), ALLOCATABLE, SAVE :: Om2(:,:),mass(:)
  REAL(wp), ALLOCATABLE, SAVE :: OmEig(:),EigMat(:,:),LEig(:)
  !REAL(wp), ALLOCATABLE, SAVE :: A2inv(:,:)
  REAL(wp), SAVE :: kT,conv_thr
  LOGICAL, SAVE :: verbose


  PRIVATE
  PUBLIC :: initialize_fk, generate_fk_initial
contains

  subroutine initialize_fk(param_library)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    INTEGER :: i,j,c

    nat=param_library%get("PARAMETERS/n_atoms")
    ndim=param_library%get("PARAMETERS/n_dim")
    ndof=nat*ndim
    mass=param_library%get("PARAMETERS/mass")
    kT=param_library%get("PARAMETERS/temperature")
    maxiter=param_library%get("IVR/max_iter_fk",DEFAULT=20)
    conv_thr=param_library%get("IVR/conv_thr_fk",DEFAULT=0.02)
    nsteps_om2=param_library%get("IVR/n_steps_om2_fk",DEFAULT=100)
    verbose=param_library%get("IVR/verbose_fk",DEFAULT=.FALSE.)

    allocate(Om2(ndof,ndof),LEig(ndof))
    allocate(OmEig(ndof),EigMat(ndof,ndof))
    !allocate(A2inv(ndof,ndof))
    Om2(:,:)=0._wp
    OmEig(:)=0._wp
    EigMat(:,:)=0._wp
    !A2inv(:,:)=0._wp
    ! initialize L to the classical limit (deBroglie length/sqrt(12) )
    c=0
    DO j=1,ndim ; DO i=1,nat
      c=c+1
      LEig(c)=1/SQRT(12*kT)
     ! A2inv(c,c)=12*kT*mass(i)
      EigMat(c,c)=1
    ENDDO ; ENDDO 

    write(*,*) "FK initialization done."
    
  end subroutine initialize_fk

  subroutine generate_fk_initial(Xc,X0,P0,reuse_om2)
    implicit none
    REAL(wp), INTENT(in) :: Xc(:,:)
    LOGICAL, INTENT(in), OPTIONAL :: reuse_om2
    REAL(wp), INTENT(inout) :: X0(:,:),P0(:,:)
    REAL(wp), ALLOCATABLE :: xEig(:),pEig(:)
    REAL(wp) :: u0
    INTEGER :: i,j
    LOGICAL :: reuse_om2_

    reuse_om2_=.FALSE.
    if(PRESENT(reuse_om2)) reuse_om2_=reuse_om2

    if(.NOT. reuse_om2_) THEN
      call compute_Om2(Xc)
    ENDIF

    ALLOCATE(xEig(ndof),pEig(ndof))
    call randGaussN(xEig)
    call randGaussN(pEig)
    DO i=1,ndof
      xEig(i)=xEig(i)*LEig(i)
      u0=0.5*OmEig(i)/kT
      pEig(i)=pEig(i)*sqrt(kT*u0/tanh(u0))
      !write(*,*) OmEig(i)*cm1,kT*u0/tanh(u0)*kelvin
    ENDDO

    P0(:,:) = RESHAPE(matmul(EigMat,pEig),(/nat,ndim/) )
    X0(:,:) = RESHAPE(matmul(EigMat,xEig),(/nat,ndim/) )
    DO i=1,ndim
      X0(:,i)=Xc(:,i) + X0(:,i)/sqrt(mass(:))
			P0(:,i)=P0(:,i)*sqrt(mass(:))
		ENDDO  
    
  end subroutine generate_fk_initial

  subroutine compute_Om2(Xc)
    implicit none
    REAL(wp), INTENT(in) :: Xc(:,:)
    INTEGER :: i,j,iconv
    REAL(wp) :: LEigOld(ndof)    
    
    iconv = maxiter
    DO i=1,maxiter
      LEigOld=LEig
      call sample_Om2(Xc)
      call update_eigenvalues()
      !write(*,*) LeigOld*bohr,Leig*bohr,OmEig*cm1
      if(maxval(abs(LEigOld-LEig)/LEigOld)<=conv_thr) THEN
        iconv=i
        EXIT
      ENDIF
    ENDDO

    IF(verbose) THEN
      IF(iconv<maxiter) then
        write(*,*) "FK: converged Om2 in ",iconv,"steps"
      else
        write(*,*) "FK Warning: Om2 did not converged to precision ",conv_thr,"in ",maxiter,"steps"
      endif
    ENDIF

  end subroutine compute_Om2

  subroutine sample_Om2(Xc)
    implicit none
    REAL(wp), INTENT(in) :: Xc(:,:)
    REAL(wp), ALLOCATABLE :: z(:)!,dU(:)
    REAL(wp), ALLOCATABLE :: hess(:,:),delta(:,:)
    INTEGER :: istep,i,j,k,c

    ALLOCATE(z(ndof),hess(ndof,ndof))
    ALLOCATE(delta(nat,ndim))
    !ALLOCATE(dU(ndof))

    Om2(:,:)=0
    DO istep=1,nsteps_om2
      !sample z in eigenspace and convert to coordinates
      call RandGaussN(z)
      z=MATMUL(EigMat,z*LEig)
      c=0
      DO j=1,ndim ; DO i=1,nat
        c=c+1
        z(c)=z(c)/sqrt(mass(i))
        delta(i,j)=z(c)
      ENDDO ; ENDDO
      
      hess=compute_hessian(Xc+delta)

      ! dU(:)=RESHAPE(dPot(Xc+delta), (/ndof/) )
      ! DO j=1,ndof ; DO i=1,ndof
      !   hess(i,j)=z(i)*dU(j)
      ! enddo ; enddo

      Om2(:,:)=Om2(:,:)+hess(:,:)
            
    ENDDO
    Om2(:,:)=Om2(:,:)/REAL(nsteps_om2,wp)   
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!
    !ONLY if gradient sampling
    !  Om2=matmul(A2inv,Om2)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!

    !mass normalization
    c=0
    DO j=1,ndim; do i=1,nat
        c=c+1
        DO k=1,ndim
          Om2((k-1)*nat+1:k*nat,c) &
                =Om2((k-1)*nat+1:k*nat,c) &
                    /sqrt(mass(i)*mass(:))
        ENDDO
    ENDDO ; ENDDO

    !symmetrize Om2
    Om2=0.5_wp*(Om2+transpose(Om2))

  end subroutine sample_Om2

  subroutine update_eigenvalues()
    implicit none
    INTEGER :: INFO,i,j,k,c

    ! A2inv(:,:)=0._wp

    call sym_mat_diag(Om2,OmEig,EigMat,INFO)
    IF(INFO/=0) STOP "Error: could not diagonalize Om2(xc) !"
    DO i=1,ndof
      if(OmEig(i)<=0) STOP "Error: Found a negative eigenvalue of Om2(xc) !"
      OmEig(i)=sqrt(OmEig(i))
      LEig(i)=sqrt((kT/OmEig(i)**2)*( OmEig(i)/(2*kT*tanh(0.5*OmEig(i)/kT))  - 1._wp))
      ! A2inv(i,i)=1._wp/Leig(i)**2
    ENDDO

    ! A2inv=matmul(EigMat, matmul(A2inv,transpose(EigMat)))
    ! c=0
    ! DO j=1,ndim; do i=1,nat
    !     c=c+1
    !     DO k=1,ndim
    !       A2inv((k-1)*nat+1:k*nat,c) &
    !             =A2inv((k-1)*nat+1:k*nat,c) &
    !                 *sqrt(mass(i)*mass(:))
    !     ENDDO
    ! ENDDO ; ENDDO


  end subroutine update_eigenvalues

end module fk_initial