MODULE schrodinger
	USE kinds
	USE nested_dictionaries
	USE classical_md_types
	USE basic_types
	USE classical_md, only: classical_MD_get_parameters
	USE atomic_units, only: convert_to_atomic_units, &
							atomic_units_get_multiplier, &
							bohr, Mprot, fs
	USE file_handler, only: output
	!USE solve_schro2d, only: solve_2D
	IMPLICIT NONE

	CHARACTER(*), PARAMETER :: SCHRO_CAT="SCHRODINGER"

	TYPE, EXTENDS(CLASS_MD_SYS) :: SCHRO_SYS
		REAL(wp), ALLOCATABLE :: X_grid(:)
		REAL(wp), ALLOCATABLE :: Y_grid(:)
		REAL(wp), ALLOCATABLE :: Px_grid(:)
		REAL(wp), ALLOCATABLE :: Py_grid(:)
		REAL(wp), ALLOCATABLE :: V(:,:)
		REAL(wp), ALLOCATABLE :: Psi(:,:,:), Ener(:)

		REAL(wp) :: partition, E0
		REAL(wp), ALLOCATABLE :: stat_fac(:)

		REAL(wp), ALLOCATABLE :: Wig(:,:)
		REAL(wp), ALLOCATABLE :: Wig_margX(:), Wig_margPx(:)
		REAL(wp), ALLOCATABLE :: Wig_margY(:), Wig_margPy(:)
	END TYPE

	TYPE, EXTENDS(CLASS_MD_PARAM) :: SCHRO_PARAM     
		INTEGER :: nev,nconv

		REAL(wp) :: dx(2), dp(2)
		REAL(wp) :: x_min, x_max, p_max, py_max,y_min,y_max
		INTEGER :: p_steps, x_steps
		REAL(wp) :: x_unit, p_unit

		REAL(wp) :: spectrum_unit
		REAL(wp) :: omega_max, domega
		INTEGER :: om_steps
		REAL(wp) :: damp
		INTEGER :: n_cumulants
		LOGICAL :: compute_wigner
	END TYPE

	TYPE(SCHRO_SYS) :: schro_system
  TYPE(SCHRO_PARAM) :: schro_parameters

	PRIVATE
	PUBLIC :: initialize_schrodinger, finalize_schrodinger

CONTAINS

	subroutine schrodinger_compute_all()
		IMPLICIT NONE
		LOGICAL :: file_exists, solve_decide
		CHARACTER :: solve_decide_char

		inquire(file=output%working_directory//"/Energies.out",exist=file_exists)
		if(file_exists) inquire(file=output%working_directory//"/Psi.out",exist=file_exists)

		solve_decide=.TRUE.
		! if(file_exists) then
		! 	write(*,*)
		! 	write(*,*) "Found Energies and Psi files. Solving will overwrite them."
		! 	write(*,*) "Solve Schrodinger anyway ?(y/n)"
		! 	read(*,*) solve_decide_char
		! 	if(solve_decide_char/="y") then
		! 		call retrieve_Psi(schro_system,schro_parameters)
		!		solve_decide=.FALSE.
		! 	endif
		! endif
		if(solve_decide) then
			if(schro_system%n_atoms==1) then
				call solve_1D(schro_system%V(:,1),schro_system%Ener,schro_system%Psi(:,:,1) &
							,schro_parameters%dx(1),schro_system%mass(1),schro_parameters%nev)
			else
        STOP "schrodinger 2D not available without ARPACK: checkout the branch schrodinger+ARPACK"
			!	call solve_2D(schro_parameters%x_steps,schro_system%X_grid,schro_system%Y_grid &
			!					,schro_system%V,schro_parameters%nconv,schro_system%Ener,schro_system%Psi &
			!					,schro_parameters%dx,schro_parameters%nev,schro_system%mass)
			endif
		endif

		!COMPUTE PARTITION FUNCTION AND THERMAL WEIGHTS
		CALL compute_and_write_schrodinger_results(schro_system,schro_parameters)

		if(schro_system%n_atoms==1) then
			if(schro_parameters%compute_wigner) &
				CALL compute_and_write_wigner_function_1D(schro_system,schro_parameters)
				
			CALL compute_and_write_PIM_approx(schro_system,schro_parameters)			

			CALL compute_and_write_spectrum(schro_system,schro_parameters)
		else
        STOP "schrodinger 2D not available without ARPACK: checkout the branch schrodinger+ARPACK"
			!CALL compute_and_write_wigner_function_2D(schro_system,schro_parameters)
		endif

	end subroutine schrodinger_compute_all

	subroutine initialize_schrodinger(param_library)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library

		!ASSOCIATE classical_MD_loop
		simulation_loop => schrodinger_compute_all
		schro_parameters%engine = "schrodinger"
		schro_parameters%algorithm = "schrodinger"
		
		if(.NOT. param_library%has_key("OUTPUT")) then
		  STOP "Error: OUTPUT category must be present in the input file!"
		endif

		call schrodinger_get_parameters(schro_system,schro_parameters,param_library)

	end subroutine initialize_schrodinger

	subroutine schrodinger_get_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: schro_lib
		CHARACTER(:), ALLOCATABLE :: unit

		call classical_MD_get_parameters(system,parameters,param_library)
		!if(system%n_atoms/=1 .or. system%n_dim/=1) &
		!	STOP "Error: schrodinger only implemented for n_atoms=n_dim=1"

		schro_lib => param_library%get_child(SCHRO_CAT)
		! GET OUTPUT UNITS
		unit=schro_lib%get("x_output_unit",default="angstrom")
		!! @input_file SCHRODINGER/x_output_unit (schrodinger, default="angstrom")
		parameters%x_unit=atomic_units_get_multiplier(unit)
		unit=schro_lib%get("p_output_unit",default="amu*angstrom/fs")
		!! @input_file SCHRODINGER/p_output_unit (schrodinger, default="amu*angstrom/fs")
		parameters%p_unit=atomic_units_get_multiplier(unit)
		unit=schro_lib%get("spectrum_output_unit",default="cm-1")
		!! @input_file SCHRODINGER/spectrum_output_unit (schrodinger, default="cm-1")
		parameters%spectrum_unit=atomic_units_get_multiplier(unit)

		parameters%nev=schro_lib%get("nev")
		!! @input_file SCHRODINGER/nev (schrodinger)
		if(parameters%nev<=0) STOP "Error: 'nev' is not properly defined!"
		parameters%nconv=parameters%nev

		if(system%n_dim/=1) &
			STOP "Error: schrodinger only implemented for n_dim=1 and n_atoms=1,2"

		SELECT CASE(system%n_atoms)
		CASE(2)
        STOP "schrodinger 2D not available without ARPACK: checkout the branch schrodinger+ARPACK"
      !call initialize_2D(system,parameters,schro_lib)	
		CASE(1)	
			call initialize_1D(system,parameters,schro_lib)		
		CASE default
			STOP "Error: schrodinger only implemented for n_dim=1 and n_atoms=1,2"
		END SELECT

		!GET SPECTRUM PARAMETERS
		parameters%omega_max=schro_lib%get("omega_max")
		!! @input_file SCHRODINGER/omega_max (schrodinger)
		if(parameters%omega_max<=0) STOP "Error: 'omega_max' is not properly defined!"
		parameters%domega=schro_lib%get("domega")
		!! @input_file SCHRODINGER/domega (schrodinger)
		if(parameters%domega<=0) STOP "Error: 'domega' is not properly defined!"
		parameters%damp=schro_lib%get("spectrum_gamma" &
						,default=2*parameters%domega)
		!! @input_file SCHRODINGER/spectrum_gamma (schrodinger, default=2*domega)
		if(parameters%damp<=0) STOP "Error: 'spectrum_gamma' is not properly defined!"

	end subroutine schrodinger_get_parameters

	subroutine initialize_1D(system,parameters,schro_lib)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: schro_lib
		REAL(wp) :: tmp
		INTEGER :: i,u,x_steps,p_steps

		!GET GRID PARAMETERS
		parameters%x_min=schro_lib%get("x_min")
		!! @input_file SCHRODINGER/x_min (schrodinger)
		parameters%x_max=schro_lib%get("x_max")
		!! @input_file SCHRODINGER/x_max (schrodinger)
		if(parameters%x_min>=parameters%x_max) then
			tmp=parameters%x_min
			parameters%x_min=parameters%x_max
			parameters%x_max=tmp
		endif
		x_steps=schro_lib%get("x_steps",default=500)
		!! @input_file SCHRODINGER/x_steps (schrodinger, default=500)
		if(x_steps<=0) STOP "Error: 'x_steps' is not properly defined!"
		parameters%x_steps=x_steps
		parameters%dx=(parameters%x_max-parameters%x_min)/(x_steps-1)

		allocate(system%X_grid(x_steps),system%V(x_steps,1))
		system%X_grid(:)=(/ (parameters%x_min+(i-1)*parameters%dx(1), i = 1,x_steps) /)
		!WRITE POTENTIAL
		OPEN(newunit=u,file=output%working_directory//"/potential.out")
		DO i=1,x_steps
			system%X(:,:)=system%X_grid(i)
			system%V(i,1) = Pot(system%X)
			write(u,*) system%X_grid(i)*parameters%x_unit, system%V(i,1)
		ENDDO
		CLOSE(u)
		allocate(system%Psi(x_steps,parameters%nev,1), system%Ener(x_steps))
		system%Psi=0._wp ; system%Ener=0._wp

		parameters%p_max=schro_lib%get("p_max")	
		!! @input_file SCHRODINGER/p_max (schrodinger)	
		p_steps=schro_lib%get("p_steps",default=500)
		!! @input_file SCHRODINGER/p_steps (schrodinger, default=500)
		if(p_steps<=0) STOP "Error: 'p_steps' is not properly defined!"

		parameters%p_steps=p_steps
		parameters%dp = 2*parameters%p_max / (parameters%p_steps-1)
		allocate(system%Px_grid(p_steps))
		system%Px_grid(:)=(/ (-parameters%p_max+(i-1)*parameters%dp(1), i = 1,p_steps) /)

		allocate(system%Wig(x_steps,p_steps))
		allocate(system%Wig_margX(x_steps),system%Wig_margPx(p_steps))

		parameters%n_cumulants=schro_lib%get("n_cumulants",default=3)

		parameters%compute_wigner=schro_lib%get("compute_wigner",default=.TRUE.)

	end subroutine initialize_1D

	subroutine initialize_2D(system,parameters,schro_lib)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: schro_lib
		REAL(wp) :: tmp
		INTEGER :: i,j,u,x_steps,p_steps

		!GET GRID PARAMETERS
		parameters%x_min=schro_lib%get("x_min")
		parameters%x_max=schro_lib%get("x_max")
		if(parameters%x_min>parameters%x_max) then
			tmp=parameters%x_min
			parameters%x_min=parameters%x_max
			parameters%x_max=tmp
		endif
		parameters%y_min=schro_lib%get("y_min")
		!! @input_file SCHRODINGER/y_min (schrodinger 2D)
		parameters%y_max=schro_lib%get("y_max")
		!! @input_file SCHRODINGER/y_max (schrodinger 2D)
		if(parameters%y_min>parameters%y_max) then
			tmp=parameters%y_min
			parameters%y_min=parameters%y_max
			parameters%y_max=tmp
		endif


		x_steps=schro_lib%get("x_steps",default=500)
		!! @input_file SCHRODINGER/x_steps (schrodinger, default=500)
		if(x_steps<=0) STOP "Error: 'x_steps' is not properly defined!"
		parameters%x_steps=x_steps
		parameters%dx(1)=(parameters%x_max-parameters%x_min)/(x_steps-1)
		parameters%dx(2)=(parameters%y_max-parameters%y_min)/(x_steps-1)

		allocate(system%X_grid(x_steps),system%Y_grid(x_steps),system%V(x_steps,x_steps))
		system%X_grid(:)=(/ (parameters%x_min+(i-1)*parameters%dx(1), i = 1,x_steps) /)
		system%Y_grid(:)=(/ (parameters%y_min+(i-1)*parameters%dx(2), i = 1,x_steps) /)
		!WRITE POTENTIAL
		
		OPEN(newunit=u,file=output%working_directory//"/potential.out")
		DO i=1,x_steps ; DO j=1,x_steps
			system%X(1,1)=system%X_grid(i)
			system%X(2,1)=system%Y_grid(j)
			system%V(i,j) = Pot(system%X)
			write(u,*) system%X_grid(i)*parameters%x_unit, system%Y_grid(j)*parameters%x_unit, system%V(i,j)
		ENDDO ; write(u,*) ; ENDDO
		CLOSE(u)
		allocate(system%Psi(x_steps,x_steps,parameters%nev), system%Ener(x_steps))
		system%Psi=0._wp ; system%Ener=0._wp

		parameters%p_max=schro_lib%get("p_max")		
		parameters%py_max=schro_lib%get("py_max",default=parameters%p_max)
		!! @input_file SCHRODINGER/py_max (schrodinger 2D)	
		p_steps=schro_lib%get("p_steps",default=500)
		!! @input_file SCHRODINGER/p_steps (schrodinger, default=500)
		if(p_steps<=0) STOP "Error: 'p_steps' is not properly defined!"

		parameters%p_steps=p_steps
		parameters%dp(1) = 2*parameters%p_max / (parameters%p_steps-1)
		allocate(system%Px_grid(p_steps))
		system%Px_grid(:)=(/ (-parameters%p_max+(i-1)*parameters%dp(1), i = 1,p_steps) /)

		parameters%dp(2) = 2*parameters%py_max / (parameters%p_steps-1)
		allocate(system%Py_grid(p_steps))
		system%Py_grid(:)=(/ (-parameters%py_max+(i-1)*parameters%dp(2), i = 1,p_steps) /)

		allocate(system%Wig_margX(x_steps),system%Wig_margPx(p_steps))
		allocate(system%Wig_margY(x_steps),system%Wig_margPy(p_steps))

	end subroutine initialize_2D

	subroutine finalize_schrodinger()
		IMPLICIT NONE

		!! TO DO

	end subroutine finalize_schrodinger

!-----------------------------------------------

	subroutine compute_and_write_schrodinger_results(system,parameters)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,k,l,nconv

		system%E0=system%Ener(1)
		nconv=parameters%nconv
		allocate(system%stat_fac(1:nconv))
		system%stat_fac(1:nconv)=exp(-(system%Ener(1:nconv)-system%E0)*parameters%beta)

		system%partition=SUM( system%stat_fac(1:nconv) ) 
		system%stat_fac(1:nconv) = system%stat_fac(1:nconv)/system%partition
		
		!WRITE ENERGIES AND PSIs
		open(newunit=u,file=output%working_directory//"/Energies.out")	
		do k=1,nconv
			write(u,*) k, system%Ener(k), system%stat_fac(k)
		enddo
		close(u)
		write(*,*) "Energies written to: "//output%working_directory//"/Energies.out"
		
		
		open(newunit=u,file=output%working_directory//"/Psi.out")
		if(system%n_atoms==1) then
			do k=1,parameters%x_steps
				write(u,*) system%X_grid(k)*parameters%x_unit, system%Psi(k,1:nconv,1)
			enddo
		else
			do k=1,parameters%x_steps ; do l=1,parameters%x_steps
				write(u,*) system%X_grid(k)*parameters%x_unit,system%Y_grid(l)*parameters%x_unit, system%Psi(k,l,1:nconv)
			enddo ; write(u,*) ; enddo
		endif
		close(u)
		write(*,*) "Wavefunctions written to: "//output%working_directory//"/Psi.out"

	end subroutine compute_and_write_schrodinger_results

	subroutine compute_and_write_PIM_approx(system,parameters)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		REAL(wp), ALLOCATABLE :: rhoc(:), rho(:), fact2n(:),ew(:),d(:),dc(:),EW0(:,:),NLHA(:,:)
		real(wp), allocatable :: PsiT(:,:),moments(:,:),cumulants(:,:),margP(:,:),wigX(:)
		REAL(wp) :: dx,dD,delta,tmp,sign,p,dp
		INTEGER :: nconv,i,j,k,Nx,ncumul,m,u,u2,u3

		nconv=parameters%nev
		dx=parameters%dx(1)
		dD=2*parameters%dx(1)
		dp=parameters%dp(1)*parameters%p_unit
		Nx=parameters%x_steps
		ncumul=parameters%n_cumulants

		allocate(moments(Nx,ncumul))
		allocate(PsiT(nconv,Nx))
		PsiT=transpose(system%Psi(:,1:nconv,1))

		ALLOCATE(rhoc(0:Nx-1),rho(Nx))
		ALLOCATE(wigX(Nx))
		write(*,*)
		write(*,*) 'Starting to compute PIM approximation'
		rho=0
		wigX=0
		moments=0
		DO i=1,Nx
			wigX(i) = SUM(system%stat_fac(1:nconv)*PsiT(:,i)**2)
			rhoc=0
			DO j=0,i-1
				if(i+j>Nx) exit
				delta=j*dD
				rhoc(j)=SUM(system%stat_fac(1:nconv)*PsiT(:,i-j)*PsiT(:,i+j))
				DO k=1,ncumul
					moments(i,k)=moments(i,k)+dD*rhoc(j)*(delta**(2*k))
				ENDDO
			ENDDO
			rho(i)=2*(SUM(rhoc*dD)-0.5*rhoc(0)*dD-0.5*rhoc(Nx-1)*dD)
			! DO j=0,i-1
			! 	if(i+j>Nx) exit
			! 	delta=j*dD
			! 	DO k=1,ncumul
			! 		moments(i,k)=moments(i,k)+dD*(rhoc(j)/rho(i))*(delta**(2*k))
			! 	ENDDO
			! ENDDO
			DO k=1,ncumul
				moments(i,k)=2*(moments(i,k)-0.5*dD*(rhoc(Nx-1))*((Nx-1)*dD)**(2*k))/rho(i)
			ENDDO
		ENDDO
		wigX = wigX / sum(wigX*dx*parameters%x_unit)

		deallocate(PsiT)
		deallocate(rhoc)

		write(*,*) '   computing cumulants.'
		allocate(cumulants(Nx,ncumul))
		cumulants(:,1)=moments(:,1)
		DO k=2,ncumul
			cumulants(:,k)=moments(:,k)
			DO m=1,k-1
				tmp=nCr(2*k-1,2*m-1)
				cumulants(:,k)=cumulants(:,k)-tmp*cumulants(:,m)*moments(:,k-m)
			ENDDO			
			! DO i=1,Nx
			! 	if(abs(system%X_grid(i))*bohr>0.6) &
			! 		cumulants(i,k)=0._wp
			! ENDDO
		ENDDO

		open(newunit=u,file=output%working_directory//"/cumulants.out")	
		open(newunit=u2,file=output%working_directory//"/moments.out")	
		open(newunit=u3,file=output%working_directory//"/e_beta_U.out")	
		do i=1,Nx
			write(u,*) system%X_grid(i)*parameters%x_unit, cumulants(i,:)
			write(u2,*) system%X_grid(i)*parameters%x_unit, moments(i,:)
			write(u3,*) system%X_grid(i)*parameters%x_unit, rho(i)
		enddo
		close(u)
		close(u2)
		close(u3)
		write(*,*) "cumulants written to: "//output%working_directory//"/cumulants.out"
		write(*,*) "moments written to: "//output%working_directory//"/moments.out"

		deallocate(moments)

		allocate(fact2n(ncumul))
		DO k=1,ncumul
			fact2n(k)=factorial(2*k)
		ENDDO

		write(*,*) '   computing P marginal up to EW'//int_to_str(2*ncumul)//'.'
		allocate(margP(parameters%p_steps,ncumul),ew(Nx),d(Nx),dc(Nx))

		open(newunit=u,file=output%working_directory//"/Wigner_EW0_ECMA.out")			
		DO i=1,parameters%x_steps
			p=system%Px_grid(i)
			dc(:)=exp(-0.5_wp*cumulants(i,1)*system%Px_grid(:)**2)/sqrt(2*pi/cumulants(i,1))/parameters%p_unit			
			do j=1,parameters%p_steps
				write(u,*) system%X_grid(i)*parameters%x_unit &
									,system%Px_grid(j)*parameters%p_unit &
									,wigX(i)*dc(j)
			enddo
			write(u,*)
		ENDDO	
		
		DO i=1,parameters%p_steps
			p=system%Px_grid(i)
			d(:)=exp(-0.5_wp*cumulants(:,1)*p**2)*rho(:)	
			margP(i,1) = SUM(d*dx) -0.5*d(1)*dx - 0.5*d(Nx)*dx
			sign=1
			ew(:)=1._wp
			DO k=2,ncumul
				ew(:)=ew(:)+sign*cumulants(:,k)*(p**(2*k))/fact2n(k)
				margP(i,k)=SUM(d*ew*dx)-0.5*d(1)*ew(1)*dx-0.5*d(Nx)*ew(Nx)*dx
				sign=-sign
			ENDDO
		ENDDO
		close(u)

		DO k=1,ncumul
			margP(:,k)=margP(:,k)/(SUM(margP(:,k)*dp) &
									-0.5*margP(1,k)*dp-0.5*margP(parameters%p_steps,k)*dp)
		ENDDO

		open(newunit=u,file=output%working_directory//"/Wigner_EWn_margP.out")	
		DO i=1,parameters%p_steps
			write(u,*) system%Px_grid(i)*parameters%p_unit, margP(i,:)
		ENDDO
		close(u)
		write(*,*) "P marginal with EW approx written to: "//output%working_directory//"/Wigner_EWn_margP.out"

	end subroutine compute_and_write_PIM_approx

	pure function nCr(n,k)
		INTEGER ,INTENT(in):: n,k
		REAL(wp) :: nCr
		INTEGER :: i

		nCr=1
		if(k==0) return
		DO i=0,k-1
			nCr=nCr*REAL(n-i,wp)/REAL(k-i,wp)
		ENDDO

	end function nCr

	function factorial(n)
		INTEGER :: n
		REAL(wp) :: factorial
		INTEGER :: i

		if(n<0) STOP "Error: fact(n) only defined for n>=0"
		factorial=1
		if(n<=1) return
		DO i=2,n
			factorial=factorial*REAL(i,wp)
		ENDDO

	end function factorial

	subroutine compute_and_write_wigner_function_1D(system,parameters)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,i,j,k
		INTEGER :: nconv, xstep,pstep,delta_step
		REAL(wp) :: Wig_tmp
		real(wp), allocatable :: PsiT(:,:),WigT(:,:)

		nconv=parameters%nev
		allocate(PsiT(nconv,parameters%x_steps),WigT(parameters%p_steps,parameters%x_steps))
		PsiT=transpose(system%Psi(:,1:nconv,1))
		WigT=0
		write(*,*)
		write(*,*) "starting to compute wigner function"
		!$OMP PARALLEL DO PRIVATE(i,k,j,Wig_tmp)
		do i=1,parameters%x_steps			
			Wig_tmp=sum(system%stat_fac(1:nconv)*PsiT(:,i)**2)/2
			!system%Wig(i,:)=system%Wig(i,:)+Wig_tmp
			WigT(:,i)=Wig_tmp
			do k=1,i-1
				!if(i-k>0 .and. i+k<=parameters%x_steps) then
				if(i+k>parameters%x_steps) exit
					Wig_tmp=sum(system%stat_fac(1:nconv) &
						*PsiT(:,i-k)*PsiT(:,i+k))
					do j=1,parameters%p_steps
						WigT(j,i)=WigT(j,i) &
							+cos(system%Px_grid(j)*(2*k)*parameters%dx(1))*Wig_tmp
					enddo
				!endif
			enddo
		enddo
		!$OMP END PARALLEL DO

		system%Wig=transpose(WigT)
		!NORMALIZE
		system%Wig=system%Wig &
			/( SUM(system%Wig)*parameters%dx(1)*parameters%x_unit &
				*parameters%dp(1)*parameters%p_unit )
		write(*,*) "wigner function done"
		write(*,*)

		!COMPUTE MARGINALS
		system%Wig_margX=0._wp
		system%Wig_margPx=0._wp
		DO i=1,parameters%x_steps
			system%Wig_margX(i)=SUM(system%Wig(i,:))*parameters%dp(1)*parameters%p_unit
		enddo
		DO i=1,parameters%p_steps
			system%Wig_margPx(i)=SUM(system%Wig(:,i))*parameters%dx(1)*parameters%x_unit
		enddo

		!WRITE TO FILES
		xstep=parameters%x_steps
		pstep=parameters%p_steps
		open(newunit=u,file=output%working_directory//"/Wigner.out")
		do i=1,xstep ; do j=1,pstep
			write(u,*) 	system%X_grid(i)*parameters%x_unit &
						, system%Px_grid(j)*parameters%p_unit &
						, system%Wig(i,j)
		enddo ; write(u,*) ; enddo
		close(u)
		write(*,*) "2D wigner density written to: "//output%working_directory//"/Wigner.out"
		
		open(newunit=u,file=output%working_directory//"/Wigner.out.grid")
		write(u,*) "#",parameters%x_min*parameters%x_unit &
						, parameters%x_max*parameters%x_unit &
						, xstep
		write(u,*) "#",-parameters%p_max*parameters%p_unit &
						, parameters%p_max*parameters%p_unit &
						, pstep
		do i=1,xstep
			write(u,*) system%Wig(i,:)
		enddo
		close(u)
		write(*,*) "2D wigner grid written to: "//output%working_directory//"/Wigner.out.grid"

		open(newunit=u,file=output%working_directory//"/Wigner_margX.out")
		do i=1,xstep
			write(u,*) system%X_grid(i)*parameters%x_unit, system%Wig_margX(i)
		enddo
		close(u)
		write(*,*) "Wigner X marginal written to: "//output%working_directory//"/Wigner_margX.out"

		open(newunit=u,file=output%working_directory//"/Wigner_margP.out")
		do i=1,pstep
			write(u,*) system%Px_grid(i)*parameters%p_unit, system%Wig_margPx(i)
		enddo
		close(u)
		write(*,*) "Wigner P marginal written to: "//output%working_directory//"/Wigner_margP.out"

	end subroutine compute_and_write_wigner_function_1D

	subroutine compute_and_write_wigner_function_2D(system,parameters)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,uy,i,j,k,l,px,py
		INTEGER :: nconv, xstep,pstep
		REAL(wp) :: Wig_tmp,time0,timef,pfact
		REAL(wp), ALLOCATABLE :: Psi2(:,:,:)	

		nconv=parameters%nconv
		xstep=parameters%x_steps
		pstep=parameters%p_steps

		allocate(Psi2(size(system%Psi,1),size(system%Psi,2),size(system%Psi,3)))
		allocate(system%Wig(xstep,xstep))
		Psi2=system%Psi**2

		!COMPUTE MARGINALS
		system%Wig_margX=0._wp
		system%Wig_margY=0._wp
		DO i=1,nconv
			system%Wig_margX=system%Wig_margX + system%stat_fac(i)*sum(Psi2(:,:,i),dim=2)
			system%Wig_margY=system%Wig_margY + system%stat_fac(i)*sum(Psi2(:,:,i),dim=1)
			system%Wig=system%Wig + system%stat_fac(i)*Psi2(:,:,i)
		ENDDO		

		!NORMALIZATION
		system%Wig_margX=system%Wig_margX/(SUM(system%Wig_margX)*parameters%dx(1)*parameters%x_unit)
		system%Wig_margY=system%Wig_margY/(SUM(system%Wig_margX)*parameters%dx(2)*parameters%x_unit)
		system%Wig=system%Wig/(SUM(system%Wig)*parameters%dx(1)*parameters%dx(2)*parameters%x_unit**2)

		!WRITE TO FILES
		open(newunit=u,file=output%working_directory//"/Wigner_margX.out")
		open(newunit=uy,file=output%working_directory//"/Wigner_margY.out")
		do i=1,xstep
			write(u,*) system%X_grid(i)*parameters%x_unit, system%Wig_margX(i)
			write(uy,*) system%Y_grid(i)*parameters%x_unit, system%Wig_margY(i)
		enddo
		close(u)
		close(uy)
		write(*,*) "Wigner X marginal written to: "//output%working_directory//"/Wigner_margX.out"
		write(*,*) "Wigner Y marginal written to: "//output%working_directory//"/Wigner_margY.out"

		open(newunit=u,file=output%working_directory//"/Wigner_margXY.out")
		do i=1,xstep ; do j=1,xstep
			write(u,*) system%X_grid(i)*parameters%x_unit &
						,system%Y_grid(j)*parameters%x_unit &
						,system%Wig(i,j)
		enddo ; write(u,*) ; enddo
		close(u)



	end subroutine compute_and_write_wigner_function_2D

	subroutine compute_and_write_spectrum(system,parameters)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,i,j,u2,u3
		REAL(wp), ALLOCATABLE :: Omega(:), theta(:),kernel(:),spectrum(:),time(:),gxx(:)
		REAL(wp) :: omnm,xnm2,pic,dt,damp
		INTEGER :: Fstep

		open(newunit=u,file=output%working_directory//"/xnm2.out")
		open(newunit=u2,file=output%working_directory//"/spectrum_pics.out")
		Fstep=int(parameters%omega_max/parameters%domega)+1
		allocate(omega(Fstep),theta(Fstep))
		allocate(kernel(Fstep),spectrum(Fstep))
		allocate(time(Fstep/2),gxx(Fstep/2))
		dt=2._wp*pi/(2*parameters%omega_max)
		write(*,*) "gxx dt=",dt*fs,"fs"
		spectrum=0
		gxx=0
		omega(:)=(/ (i*parameters%domega, i=1,Fstep) /)
		time(:)=(/ ((i-1)*dt, i=1,Fstep/2) /)
		do i = 1,parameters%nev-1 ; do j = i+1, parameters%nev
			! if (i==j) CYCLE
			omnm=system%Ener(j)-system%Ener(i)
			xnm2=abs(sum(system%x_grid(:)*system%psi(:,i,1)*system%psi(:,j,1)))**2
			write(u,*) i,j,xnm2
			pic=pi*(system%stat_fac(i)+system%stat_fac(j))*xnm2 &
					*(system%mass(1)*omnm)**2
			
			damp=parameters%damp
			if(omnm*cm1>1800.) damp=2._wp*damp
			  
			!kernel(:)=2*omega(:)**2 &
			!     /((omega(:)**2-omnm**2)**2+(omega(:)*damp)**2)/pi
			kernel(:)=exp(-0.5*((omega(:)-omnm)/damp)**2) &
			          /(damp*sqrt(2._wp*pi))
			spectrum(:)=spectrum(:)+pic*kernel(:)

			gxx(:)=gxx(:)+2*exp(-0.5_wp*parameters%beta*(system%Ener(i)+system%Ener(j)))/system%partition &
										*xnm2*cos(omnm*time(:))

			 !&
					!*(parameters%p_unit)**2
			!if(pic > 1.0e-8) &
				write(u2,*) j-1,i-1,omnm*parameters%spectrum_unit, pic,pic/(2._wp*system%mass(1))*kcalpermol			
		enddo ; enddo 
		close(u)
		close(u2)
		write(*,*) "xnm2 written to: "//output%working_directory//"/xnm2.out"

		open(newunit=u,file=output%working_directory//"/spectrum.out")
		do i=1,Fstep
			write(u,*) omega(i)*parameters%spectrum_unit,spectrum(i)!*(parameters%p_unit)**2
		enddo
		close(u)
		write(*,*) "Spectrum written to: "//output%working_directory//"/spectrum.out"

		open(newunit=u,file=output%working_directory//"/spectrum_kubo.out")
		do i=1,Fstep
			write(u,*) omega(i)*parameters%spectrum_unit,spectrum(i)*tanh(0.5*omega(i)*parameters%beta)/(0.5*omega(i)*parameters%beta)!*(parameters%p_unit)**2
		enddo
		close(u)
		write(*,*) "Kubo-transformed Spectrum written to: "//output%working_directory//"/spectrum_kubo.out"

		open(newunit=u,file=output%working_directory//"/gxx.out")
		do i=1,Fstep/2
			write(u,*) time(i)*fs,gxx(i)
		enddo
		close(u)
		write(*,*) "gxx written to: "//output%working_directory//"/gxx.out"

	end subroutine compute_and_write_spectrum

	subroutine retrieve_Psi(system,parameters)
		IMPLICIT NONE
		CLASS(SCHRO_SYS), INTENT(inout) :: system
		CLASS(SCHRO_PARAM), INTENT(inout) :: parameters
		INTEGER :: u,k,nconv

		!!! TO DO !!! 

	end subroutine retrieve_Psi

!-----------------------------------------------
! 1D schrodinger solver

	subroutine solve_1D(V,E,Psi,dx,mass,nev)
		real(wp), intent(in) :: V(:),dx,mass
		real(wp), intent(inout) :: E(:), Psi(:,:)
		integer,intent(in) :: nev
		integer :: i,INFO,n,nconv
		integer, allocatable :: ISUPPZ(:),IWORK(:)
		real(wp) :: tmp
		real(wp) :: SD(size(V)), WORK(18*size(V)),Eigvals(size(V))
		LOGICAL :: TRYRAC
		!use atomic units with hbar=1
		!requires LAPACK
		tmp=0
		n=size(V)
		allocate(ISUPPZ(2*nev),IWORK(10*n))
		do i=1,n
			E(i)=1/(mass*dx**2)+V(i)
		enddo
		
		SD(:)=-1/(2*mass*dx**2)

		TRYRAC=.TRUE.

		write(*,*) "starting diagonalization"
		!call dsteqr('I',n,E,SD,Psi,n,WORK,INFO)		
		call dstemr('V','I',n,E,SD,tmp,tmp,1,nev,nconv,Eigvals,Psi,n,nev &
									,ISUPPZ,TRYRAC,WORK,18*n,IWORK,10*n,INFO)
		if(INFO /= 0) then
			write(0,*) "ERREUR dsteqr:",INFO
			stop
		end if
		write(*,*) "diagonalization done."

		write(*,*) "found",nconv,"eigenvalues."
		E=0
		E(1:nconv)=Eigvals(1:nconv)
		
	end subroutine solve_1D

END MODULE schrodinger
