module gxx_module
  USE kinds
  USE ring_polymer_md
  USE string_operations
  USE random
	use gxx_types
	use gxx_integrators
  IMPLICIT NONE

  TYPE(gxx_SYS), save, target :: gxx_system
	TYPE(gxx_PARAM), save, target :: gxx_parameters

  PROCEDURE(gxx_sub), pointer :: gxx_integrator

	PRIVATE
  PUBLIC :: initialize_gxx

CONTAINS

  subroutine gxx_loop()
		IMPLICIT NONE
    INTEGER :: i,u

    if(gxx_parameters%n_steps_therm > 0) then
			
			write(*,*) "Starting ",gxx_parameters%n_steps_therm," steps &
						&of thermalization"
			DO i=1,gxx_parameters%n_steps_therm

        call gxx_integrator(gxx_system,gxx_parameters)

			ENDDO
			write(*,*) "Thermalization done."
    ENDIF

		gxx_system%gxx_mean=0._wp
    write(*,*) "Starting ",gxx_parameters%n_steps," steps &
						&for gxx at time t=",REAL(gxx_parameters%gxx_t*fs),"fs"
		DO i=1,gxx_parameters%n_steps
			gxx_system%time=gxx_system%time &
							+gxx_parameters%dt
			write(*,'(A)',advance="no") ACHAR(13)//"step : "//int_to_str(i)

			call gxx_integrator(gxx_system,gxx_parameters)

			call update_gxx(gxx_system,gxx_parameters,i)
			! WRITE TRAJECTORY
			call output%write_all()
		ENDDO

		OPEN(newunit=u,file=output%working_directory//"/gxx.out")
		write(u,*) gxx_parameters%gxx_t*fs,gxx_system%gxx_mean
		CLOSE(u)

		write(*,*)
		write(*,*) "Simulation done."    

  end subroutine gxx_loop

  subroutine initialize_gxx(param_library)
    IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library

    gxx_parameters%engine="gxx"

		!ASSOCIATE classical_MD_loop
		simulation_loop => gxx_loop

		!GET ALGORITHM
		gxx_parameters%algorithm="nvt"

		! GET PARAMETERS
    call gxx_get_parameters(gxx_system,gxx_parameters,param_library)
		call RPMD_initialize_momenta(gxx_system,gxx_parameters)

		call gxx_initialize_normal_modes(gxx_system,gxx_parameters) 

		call gxx_get_integrator_parameters(gxx_system,gxx_parameters,param_library)	

    

		call gxx_initialize_forces(gxx_system,gxx_parameters,param_library)

		call gxx_initialize_output(gxx_system,gxx_parameters,param_library)

  end subroutine initialize_gxx

  subroutine gxx_get_parameters(system,parameters,param_library)
    implicit none
		CLASS(gxx_SYS), INTENT(inout) :: system
		CLASS(gxx_PARAM), INTENT(inout) :: parameters
    TYPE(DICT_STRUCT), intent(in) :: param_library

    call RPMD_get_parameters(system,parameters,param_library)

    if(system%n_dof /= 1 ) STOP "Error: gxx only implemented for n_dof=1"
		if(system%n_beads < 4) STOP "Error: n_beads must be >=4 for gxx"

    parameters%gxx_t = param_library%get("parameters/gxx_time")
		parameters%gxx_dt = parameters%gxx_t/REAL(system%n_beads-1,wp)
		parameters%gxx_dt2 = parameters%gxx_dt*parameters%gxx_dt
		parameters%dBeta=parameters%beta/REAL(system%n_beads-1,wp)
    parameters%tauc2=(parameters%gxx_t**2+parameters%beta**2/4._wp) &
                          /REAL(system%n_beads-1,wp)**2
    parameters%tauc=SQRT(parameters%tauc2)

		system%n_k2=system%n_dof*(system%n_beads-2)
		parameters%n_k2=system%n_k2
    allocate(system%k2(system%n_k2,system%n_k2))
		system%k2=0._wp
		allocate(system%df(system%n_k2))
		system%df=0._wp
		allocate(system%x1xnu(system%n_atoms,system%n_dim))
		system%x1xnu=0._wp
		allocate(system%gxx_mean(system%n_atoms,system%n_dim))
		system%gxx_mean=0._wp
		allocate(system%gxx_num_mean(system%n_atoms,system%n_dim))
		system%gxx_num_mean=0._wp

		
		system%k2_weight_mean=0._wp

		parameters%use_k2_free=param_library%get(GXX_CAT//"/use_k2_free",default=.TRUE.)

  end subroutine gxx_get_parameters

  subroutine gxx_initialize_normal_modes(polymer,parameters)
		implicit none
		CLASS(gxx_SYS), INTENT(inout) :: polymer
		CLASS(gxx_PARAM), INTENT(inout) :: parameters
		integer :: i,j,k,INFO,nu
		real(wp):: pref,Q2
		real(wp),allocatable :: WORK(:)
		
		call gxx_compute_k2_free(polymer,parameters)
		
		nu=polymer%n_beads

		allocate(parameters%EigMat(nu,nu),parameters%EigMatTr(nu,nu) &
				, parameters%Omk(nu))
		allocate(polymer%EigX(nu,polymer%n_atoms,polymer%n_dim))
		allocate(polymer%EigP(nu,polymer%n_atoms,polymer%n_dim))

		allocate(WORK(3*nu-1))
		
		parameters%EigMat=0

		do i=1,nu-1
			parameters%EigMat(i,i)=2/parameters%tauc2
			parameters%EigMat(i,i+1)=-1/parameters%tauc2
			parameters%EigMat(i+1,i)=-1/parameters%tauc2
		enddo
		parameters%EigMat(1,1)=1/parameters%tauc2
		parameters%EigMat(nu,nu)=1/parameters%tauc2

		if(parameters%use_k2_free) then
			pref=2*parameters%gxx_dt2/(parameters%dBeta*parameters%tauc2**2)
			do i=2,nu-1; do j=2,nu-1
				Q2=pref*polymer%Q2_free(i-1,j-1)
				parameters%EigMat(i,j)=parameters%EigMat(i,j)+4*Q2
				parameters%EigMat(i,j+1)=parameters%EigMat(i,j+1)-2*Q2
				parameters%EigMat(i,j-1)=parameters%EigMat(i,j-1)-2*Q2
				parameters%EigMat(i+1,j)=parameters%EigMat(i+1,j)-2*Q2
				parameters%EigMat(i-1,j)=parameters%EigMat(i-1,j)-2*Q2
				parameters%EigMat(i+1,j+1)=parameters%EigMat(i+1,j+1)+Q2
				parameters%EigMat(i+1,j-1)=parameters%EigMat(i+1,j-1)+Q2
				parameters%EigMat(i-1,j+1)=parameters%EigMat(i-1,j+1)+Q2
				parameters%EigMat(i-1,j-1)=parameters%EigMat(i-1,j-1)+Q2
			enddo ; enddo
		endif
		
		call DSYEV('V','U',nu,parameters%EigMat,nu,parameters%Omk,WORK,3*nu-1,INFO)		
		if(INFO/=0) then
			STOP "Error: could not find the polymer normal modes!"
		endif
		
		parameters%Omk(1)=0
		parameters%Omk(:)=sqrt(parameters%Omk(:))
		
		!COMPUTE INVERSE TRANSFER MATRIX
		parameters%EigMatTr=transpose(parameters%EigMat)
		if(nu==1) write(*,*) "EigMat=",parameters%EigMat
		
		deallocate(WORK)		

		DO k=1,polymer%n_dim; DO j=1,polymer%n_atoms
			polymer%EigP(:,j,k)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
			polymer%EigX(:,j,k)=matmul(parameters%EigMatTr,polymer%X_beads(:,j,k))
		ENDDO ; ENDDO
		
	end subroutine gxx_initialize_normal_modes

	subroutine gxx_compute_k2_free(system,parameters)
		implicit none
		CLASS(gxx_SYS), INTENT(inout) :: system
		CLASS(gxx_PARAM), INTENT(inout) :: parameters
		real(wp),allocatable :: WORK(:),EigMat(:,:),omk2(:)
		INTEGER :: i,j,c, info,nu,u,k

		nu=system%n_beads-2
		allocate(system%Q2_free(nu,nu) &
						,system%k2_free(system%n_k2,system%n_k2))
		

		allocate(EigMat(nu,nu) &
							,omk2(nu) &
							,WORK(3*nu-1))
		
		EigMat=0
		do i=1,nu-1
			EigMat(i,i)=2
			EigMat(i,i+1)=-1
			EigMat(i+1,i)=-1
		enddo
		EigMat(nu,nu)=2

		call DSYEV('V','U',nu,EigMat,nu,omk2,WORK,3*nu-1,INFO)		
		if(INFO/=0) then
			STOP "Error: could not find the polymer normal modes!"
		endif
		
		system%Q2_free=0._wp
		DO i=1,nu
			system%Q2_free(i,i)=4._wp*parameters%tauc2/(parameters%dBeta*omk2(i))
		ENDDO
		system%Q2_free=matmul(EigMat, &
                matmul(system%Q2_free,transpose(EigMat)))
		
		deallocate(omk2, EigMat,WORK)

		system%k2_free=0._wp
		c=0
		DO j=1,system%n_dim; DO i=1,system%n_atoms
			c=c+1
			system%k2_free((c-1)*nu+1:c*nu,(c-1)*nu+1:c*nu)=system%Q2_free(:,:)/system%mass(i)
		ENDDO ; ENDDO

		OPEN(newunit=u,file=output%working_directory//"/Q2_free.out")
		DO k=1,nu
			write(u,*) system%Q2_free(k,:)
		ENDDO
		CLOSE(u)
		
	end subroutine gxx_compute_k2_free

	subroutine gxx_get_integrator_parameters(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(gxx_SYS), INTENT(inout) :: system
		CLASS(gxx_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		parameters%integrator_type=param_library%get("PARAMETERS/integrator", default="baoab")
		!! @input_file PARAMETERS/integrator (gxx, default="baoab")
		parameters%integrator_type=to_lower_case(parameters%integrator_type)

		SELECT CASE(parameters%algorithm)
		CASE("nvt")

			call initialize_thermostat(system,parameters,param_library)

			SELECT CASE(parameters%integrator_type)
			CASE("baoab")
				gxx_integrator => gxx_BAOAB
			CASE("aboba")
				gxx_integrator => gxx_ABOBA
			CASE("pile-l")
				gxx_integrator => gxx_PILE_L
			CASE DEFAULT
				write(0,*) "Error: unknown integrator '"//parameters%integrator_type//"' &
							&for algorithm '"//parameters%algorithm//"' !"
				STOP "Execution stopped"
			END SELECT

		CASE DEFAULT
			write(0,*) "Error: unknown algorithm '"//parameters%algorithm//"' !"
			STOP "Execution stopped"
		END SELECT

		write(*,*) "algorithm: "//parameters%algorithm//", INTEGRATOR: "//parameters%integrator_type

	end subroutine gxx_get_integrator_parameters

	subroutine gxx_initialize_output(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(gxx_SYS), INTENT(inout) :: system
		CLASS(gxx_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), pointer :: output_cat,dict
		CHARACTER(:), ALLOCATABLE :: current_file, description, basename,default_name
		INTEGER :: i_file, i,j,k
		CHARACTER(3) :: i_char
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)

		! call output%create_file("X_1.traj",formatted)
		!  	call output%add_column_to_file(system%time,unit="ps")
		!  	call output%add_array_to_file(system%X_beads(1,:,1),unit="angstrom")

		output_cat => param_library%get_child("OUTPUT")
		call dict_list_of_keys(output_cat,keys,is_sub)
		DO i=1,size(keys)
			if(.not. is_sub(i)) CYCLE
			current_file=keys(i)
			dict => param_library%get_child("OUTPUT/"//current_file)
			SELECT CASE(to_lower_case(trim(current_file)))

			CASE("bead0")
			!! @input_file OUTPUT/bead0 (gxx)
				description="trajectory of the first bead (time 0)"				

				if(system%n_dim==1) then
					default_name="bead0.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X_beads(1,:,1),index=i_file,name="bead0")
				else
					default_name="bead0.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X_beads(1,:,:),system%element_symbols,index=i_file,name="bead0")
				endif

				call output%files(i_file)%edit_description(description)
			
			CASE("beadnu")
			!! @input_file OUTPUT/beadnu (gxx)
				description="trajectory of the first bead (time 0)"				

				if(system%n_dim==1) then
					default_name="beadnu.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%X_beads(system%n_beads,:,1),index=i_file,name="beadnu")
				else
					default_name="beadnu.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%X_beads(system%n_beads,:,:),system%element_symbols,index=i_file,name="beadnu")
				endif

				call output%files(i_file)%edit_description(description)
			
			CASE("beads_positions")
			!! @input_file OUTPUT/beads_positions (gxx)
				description="trajectory of the bead "	

				if(system%n_dim==1) then			
					basename=dict%get("name",default="X_beads.traj.")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char)
						call output%create_file(dict,basename//i_char,index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						if(output%files(i_file)%has_time_column) &
							call output%add_column_to_file(system%time,unit="ps",name="time")
						call output%add_array_to_file(system%X_beads(j,:,1),name="position of bead "//i_char)
					ENDDO
				else
					basename=dict%get("name",default="beads_pos_")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char//".xyz")
						call output%create_file(dict,basename//i_char//".xyz",index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						call output%add_xyz_to_file(system%X_beads(j,:,:),system%element_symbols &
													,index=i_file,name="position of bead "//i_char)
					ENDDO
				endif
			
			CASE("gxx_mean")
			!! @input_file OUTPUT/gxx_mean (gxx)
				description="mean gxx"				

				if(system%n_dim==1) then
					default_name="gxx_mean.traj"
					call output%create_file(dict,default_name,index=i_file)

					if(output%files(i_file)%has_time_column) &
						call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
					call output%add_array_to_file(system%gxx_mean(:,1),index=i_file,name="gxx")
				else
					default_name="gxx_mean.xyz"
					call output%create_file(dict,default_name,index=i_file)
					call output%add_xyz_to_file(system%gxx_mean(:,:),system%element_symbols,index=i_file,name="gxx")
				endif
			
			CASE("k2_weight")
			!! @input_file OUTPUT/gxx_mean (gxx)
				description="k2 weight (exp(-dt^2*(dF*k2*dF))"				

				default_name="k2_weight.traj"
				call output%create_file(dict,default_name,index=i_file)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				call output%add_column_to_file(system%k2_weight,index=i_file,name="k2_weight")
				call output%add_column_to_file(system%k2_weight_free,index=i_file,name="k2_weight_free")

				call output%files(i_file)%edit_description(description)
			
			CASE("k2")		
			!! @input_file OUTPUT/k2 (gxx)		
				description="trajectory of k2 (only the lower triangular part in col-major)"
				default_name="k2.traj"
				call output%create_file(dict,default_name,index=i_file)
				call output%files(i_file)%edit_description(description)

				if(output%files(i_file)%has_time_column) &
					call output%add_column_to_file(system%time,unit="ps",index=i_file,name="time")
				DO k=1,system%n_k2
					call output%add_array_to_file(system%k2(k:system%n_k2,k),index=i_file,name="k2")
				ENDDO
			
			CASE("forces")
			!! @input_file OUTPUT/beads_positions (gxx)
				description="trajectory of the force on bead "	

				if(system%n_dim==1) then			
					basename=dict%get("name",default="Forces.traj.")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char)
						call output%create_file(dict,basename//i_char,index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						if(output%files(i_file)%has_time_column) &
							call output%add_column_to_file(system%time,unit="ps",name="time")
						call output%add_array_to_file(system%F_beads(j,:,1),name="force on bead "//i_char)
					ENDDO
				else
					basename=dict%get("name",default="beads_forces_")					
					DO j=1,system%n_beads
						write(i_char,'(i3.3)') j
						call dict%store("name",basename//i_char//".xyz")
						call output%create_file(dict,basename//i_char//".xyz",index=i_file)
						call output%files(i_file)%edit_description(description//i_char)
						
						call output%add_xyz_to_file(system%F_beads(j,:,:),system%element_symbols &
													,index=i_file,name="force on bead "//i_char)
					ENDDO
				endif
			
			CASE("print_system_info")	
			!! @input_file OUTPUT/print_system_info (gxx)
				parameters%write_mean_energy_info=.TRUE.
				parameters%print_temperature_stride=dict%get("stride",default=100)
				
			CASE DEFAULT
				write(0,*) "Warning: file '"//trim(current_file)//"' not supported by "//parameters%engine
			END SELECT
		ENDDO

	end subroutine gxx_initialize_output

end module gxx_module