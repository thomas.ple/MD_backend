MODULE gxx_integrators
	USE gxx_types
	USE random
	USE basic_types
	USE gxx_forces
	USE thermostats, ONLY : PI_thermostat_get_evolved_P, apply_PI_thermostat
	USE socket_potential, only :parallel_socket_pot_info

  IMPLICIT NONE

	PRIVATE :: apply_A,apply_B,apply_AOA

CONTAINS

    ! INTEGRATORS

	subroutine gxx_velocity_verlet(system,parameters)
		IMPLICIT NONE
		CLASS(gxx_SYS), intent(inout) :: system
		CLASS(gxx_PARAM), intent(inout) :: parameters

		call apply_B(system,parameters,parameters%dt/2._wp)
		call apply_A(system,parameters,parameters%dt)

		call gxx_compute_forces(system,parameters)
		call apply_B(system,parameters,parameters%dt/2._wp)

	end subroutine gxx_velocity_verlet

	subroutine gxx_BAOAB(system,parameters)
		IMPLICIT NONE
		CLASS(gxx_SYS), intent(inout) :: system
		CLASS(gxx_PARAM), intent(inout) :: parameters


		CALL apply_B(system,parameters,parameters%dt/2._wp)

		! CALL apply_A(system,parameters,parameters%dt/2._wp)
		! CALL apply_PI_thermostat(system,parameters)
		! CALL apply_A(system,parameters,parameters%dt/2._wp)
		CALL apply_AOA(system,parameters)

		call gxx_compute_forces(system,parameters)

		CALL apply_B(system,parameters,parameters%dt/2._wp)	

	end subroutine gxx_BAOAB

	subroutine gxx_ABOBA(system,parameters)
		IMPLICIT NONE
		CLASS(gxx_SYS), intent(inout) :: system
		CLASS(gxx_PARAM), intent(inout) :: parameters

		CALL apply_A(system,parameters,parameters%dt)!/2._wp)		
		call gxx_compute_forces(system,parameters)

		CALL apply_B(system,parameters,parameters%dt/2._wp)		
		CALL apply_PI_thermostat(system,parameters)
		CALL apply_B(system,parameters,parameters%dt/2._wp)	
	!	CALL apply_A(system,parameters,parameters%dt/2._wp)

	end subroutine gxx_ABOBA

	subroutine gxx_PILE_L(system,parameters)
		IMPLICIT NONE
	  CLASS(gxx_SYS), intent(inout) :: system
		CLASS(gxx_PARAM), intent(inout) :: parameters

		CALL apply_B(system,parameters,parameters%dt/2._wp)		
		CALL apply_A(system,parameters,parameters%dt)

		call gxx_compute_forces(system,parameters)
		CALL apply_B(system,parameters,parameters%dt/2._wp)	
		CALL apply_PI_thermostat(system,parameters)

	end subroutine gxx_PILE_L

	subroutine apply_A(polymer,parameters,tau)
		!$ USE OMP_LIB
		IMPLICIT NONE		
		CLASS(gxx_SYS), intent(inout) :: polymer
		CLASS(gxx_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		REAL(wp), ALLOCATABLE, SAVE :: EigX0(:,:), EigP0(:,:)
		INTEGER :: k,i,j,i_thread

		if(.not. allocated(EigX0)) then
			allocate(EigX0(polymer%n_beads,parameters%n_threads), EigP0(polymer%n_beads,parameters%n_threads))
		endif
		
		i_thread=1
	!	!$OMP PARALLEL FIRSTPRIVATE(i_thread,j,k,i)
	!		!$ i_thread = OMP_GET_THREAD_NUM()+1
	!	!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms

			EigP0(:,i_thread)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
			EigX0(:,i_thread)=matmul(parameters%EigMatTr,polymer%X_beads(:,j,k))

			polymer%EigX(1,j,k)=EigX0(1,i_thread)+tau*EigP0(1,i_thread)/polymer%mass(j)
			polymer%EigP(1,j,k)=EigP0(1,i_thread)

			do i=2,polymer%n_beads
				polymer%EigX(i,j,k)=EigX0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					+EigP0(i,i_thread)*sin(parameters%Omk(i)*tau)/(polymer%mass(j)*parameters%Omk(i))

				polymer%EigP(i,j,k)=EigP0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					-polymer%mass(j)*parameters%Omk(i)*EigX0(i,i_thread)*sin(parameters%Omk(i)*tau)
			enddo
			
			polymer%X_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigX(:,j,k))
			polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))

		enddo ; enddo
	!	!$OMP END DO
	!	!$OMP END PARALLEL

	end subroutine apply_A

	subroutine apply_B(polymer,parameters,tau)
		IMPLICIT NONE
		CLASS(gxx_SYS), intent(inout) :: polymer
		CLASS(gxx_PARAM), intent(inout) :: parameters
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,polymer%n_beads
			polymer%P_beads(i,:,:)=polymer%P_beads(i,:,:) + tau*polymer%F_beads(i,:,:)
		enddo
		
	end subroutine apply_B

	subroutine apply_AOA(polymer,parameters)
		!$ USE OMP_LIB
		implicit none
		CLASS(gxx_SYS), intent(inout) :: polymer
		CLASS(gxx_PARAM), intent(inout) :: parameters
		INTEGER :: i,j,k,i_thread
		REAL(wp) :: tau
		REAL(wp), ALLOCATABLE, SAVE :: EigX0(:,:), EigP0(:,:)

		if(.not. allocated(EigX0)) &
			allocate(EigX0(polymer%n_beads,parameters%n_threads) &
					,EigP0(polymer%n_beads,parameters%n_threads) )

		tau=parameters%dt/2._wp
		
		i_thread=1
	!	!$OMP PARALLEL FIRSTPRIVATE(i_thread,j,k,i)
	!		!$ i_thread = OMP_GET_THREAD_NUM()+1
	!	!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)
		do k=1,polymer%n_dim ; do j=1,polymer%n_atoms
			!!$ write(*,*) i_thread

			EigP0(:,i_thread)=matmul(parameters%EigMatTr,polymer%P_beads(:,j,k))
			EigX0(:,i_thread)=matmul(parameters%EigMatTr,polymer%X_beads(:,j,k))

			!APPLY A (dt/2)
			polymer%EigX(1,j,k)=EigX0(1,i_thread)+tau*EigP0(1,i_thread)/polymer%mass(j)
			polymer%EigP(1,j,k)=EigP0(1,i_thread)

			do i=2,polymer%n_beads
				polymer%EigX(i,j,k)=EigX0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					+EigP0(i,i_thread)*sin(parameters%Omk(i)*tau)/(polymer%mass(j)*parameters%Omk(i))

				polymer%EigP(i,j,k)=EigP0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					-polymer%mass(j)*parameters%Omk(i)*EigX0(i,i_thread)*sin(parameters%Omk(i)*tau)
			enddo

			!APPLY O (dt)
			EigP0(:,i_thread)=PI_thermostat_get_evolved_P(polymer,parameters,j,k)
			! call RandGaussN(R)
			! EigP0(:,i_thread)=polymer%EigP(:,j,k)*parameters%RPMD_damping_exp(:) &
			! 						+ parameters%RPMD_sigma(:,j)*R(:)
			EigX0(:,i_thread)=polymer%EigX(:,j,k)			

			!APPLY A (dt/2)
			polymer%EigX(1,j,k)=EigX0(1,i_thread)+tau*EigP0(1,i_thread)/polymer%mass(j)
			polymer%EigP(1,j,k)=EigP0(1,i_thread)

			do i=2,polymer%n_beads
				polymer%EigX(i,j,k)=EigX0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					+EigP0(i,i_thread)*sin(parameters%Omk(i)*tau)/(polymer%mass(j)*parameters%Omk(i))

				polymer%EigP(i,j,k)=EigP0(i,i_thread)*cos(parameters%Omk(i)*tau) &
					-polymer%mass(j)*parameters%Omk(i)*EigX0(i,i_thread)*sin(parameters%Omk(i)*tau)
			enddo

			polymer%X_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigX(:,j,k))
			polymer%P_beads(:,j,k)=matmul(parameters%EigMat,polymer%EigP(:,j,k))

		enddo ; enddo
	!	!$OMP END DO
	!	!$OMP END PARALLEL
		
	end subroutine apply_AOA

!------------------------------------------------------------------
! FORCES CALCULATORS

	subroutine gxx_initialize_forces(system,parameters,param_library)
		IMPLICIT NONE
		CLASS(gxx_SYS), INTENT(inout) :: system
		CLASS(gxx_PARAM), INTENT(inout) :: parameters
		TYPE(DICT_STRUCT), intent(in) :: param_library

		allocate(system%Forces(system%n_atoms,system%n_dim))
		allocate(system%F_beads(system%n_beads,system%n_atoms,system%n_dim))
		allocate(system%F_cl(system%n_beads,system%n_atoms,system%n_dim))
		
		call initialize_auxiliary_simulation(system,parameters,param_library)
		call gxx_compute_forces(system,parameters)

	end subroutine gxx_initialize_forces

END MODULE gxx_integrators    
