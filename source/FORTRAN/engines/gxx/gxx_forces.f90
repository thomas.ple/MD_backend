MODULE gxx_forces
    USE kinds
    USE gxx_types
    USE rpmd_types
    USE nested_dictionaries
    USE string_operations
    USE atomic_units, only : convert_to_unit
    USE basic_types, only : Pot,dPot,get_pot_info
    USE matrix_operations, only : sym_mat_diag,fill_permutations,sym_mv
    USE random, only: RandGaussN
    USE unbiased_products, only: unbiased_product
    USE socket_potential, only :parallel_socket_pot_info
    USE timer_module
    IMPLICIT NONE

    INTEGER, SAVE :: NUM_AUX_SIM=3

    TYPE :: WIG_POLYMER_TYPE
        INTEGER :: n_beads

        REAL(wp), ALLOCATABLE :: X(:,:,:)
        REAL(wp), ALLOCATABLE :: EigV(:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:,:,:)

        REAL(wp), ALLOCATABLE :: F_beads_plus(:,:,:)
        REAL(wp), ALLOCATABLE :: F_beads_minus(:,:,:)

        ! REAL(wp), ALLOCATABLE :: F_beads_prev(:,:,:)
        REAL(wp), ALLOCATABLE :: Pot_beads(:)
        ! REAL(wp), ALLOCATABLE :: Pot_beads_prev(:)

        ! REAL(wp), ALLOCATABLE :: k1(:)
        ! REAL(wp), ALLOCATABLE :: k2_mm(:,:)

        ! REAL(wp), ALLOCATABLE :: X_prev(:,:,:)
        ! REAL(wp), ALLOCATABLE :: P_prev(:,:,:)
        ! REAL(wp), ALLOCATABLE :: Delta_prev(:,:)
        REAL(wp) :: MC_ener
        REAL(wp) :: MC_acc_ratio
        INTEGER :: MC_count_moves

        REAL(wp), ALLOCATABLE :: Delta_packed(:)
        REAL(wp), ALLOCATABLE :: k2(:,:)
        ! REAL(wp), ALLOCATABLE :: k2_reweight(:,:)
		! REAL(wp), ALLOCATABLE :: k2inv(:,:)
		! REAL(wp), ALLOCATABLE :: k4(:,:,:,:)
		REAL(wp), ALLOCATABLE :: F(:,:,:)
		! REAL(wp), ALLOCATABLE :: G(:,:,:)
        ! REAL(wp), ALLOCATABLE :: F_reweight(:)
		! REAL(wp), ALLOCATABLE :: G_reweight(:,:,:)

        !ONLY FOR 1D SYSTEMS
        REAL(wp) :: k6 
        REAL(wp) :: G4

        CHARACTER(:), ALLOCATABLE :: prev_config_filename
        INTEGER :: prev_config_unit,i_step_reweight
        REAL(wp) :: mean_weight_ratio

    END TYPE

    TYPE, EXTENDS(RPMD_PARAM) :: WIG_FORCE_PARAM
        INTEGER :: n_k2

        REAL(wp), ALLOCATABLE :: mOmK(:,:)

        !PIOUD PARAMETERS
        ! REAL(wp), ALLOCATABLE :: mu(:,:,:)
        REAL(wp), ALLOCATABLE :: OUsig(:,:,:,:)
        REAL(wp), ALLOCATABLE :: expOmk(:,:,:,:)

        ! BAOAB PARAMETERS
        REAL(wp), ALLOCATABLE :: gammaExp(:,:)
        REAL(wp), ALLOCATABLE :: sigmaBAOAB(:,:)
        LOGICAL :: BAOAB_num

        REAL(wp), ALLOCATABLE :: F_cl(:)

        REAL(wp), ALLOCATABLE :: sigp2_inv(:)

        ! REAL(wp), ALLOCATABLE :: muX(:,:,:),muP(:,:,:)

        CHARACTER(:), ALLOCATABLE :: move_generator
        INTEGER :: n_samples

        REAL(wp), ALLOCATABLE :: sqrt_mass(:)
        LOGICAL :: compute_EW

        REAL(wp) :: delta_mass_factor, sqrt_delta_mass_factor

        LOGICAL :: verbose

        LOGICAL :: k2_smooth, G_smooth
        REAL(wp) :: k2_smooth_center
        REAL(wp) :: k2_smooth_width
        REAL(wp) , ALLOCATABLE :: k2_smooth_mat(:,:)

        REAL(wp), ALLOCATABLE :: memory_F(:)
		REAL(wp), ALLOCATABLE :: memory_C(:,:,:)
		REAL(wp), ALLOCATABLE :: memory_k2(:,:)
        REAL(wp), ALLOCATABLE :: memory_sqrtk2inv(:,:)
        REAL(wp) :: memory_sum_weight
        REAL(wp) :: memory_tau
        LOGICAL :: memory_smoothing

        LOGICAL :: MC_acceptance_test
        LOGICAL :: reweight_previous_configs
        INTEGER :: n_steps_reweight

        LOGICAL :: moment_matching

    END TYPE

    TYPE WORK_VARIABLES
        REAL(wp), ALLOCATABLE :: F(:)
        REAL(wp), ALLOCATABLE :: Delta2(:,:)
        REAL(wp), ALLOCATABLE :: Delta4(:,:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:),EigV(:)
        REAL(wp), ALLOCATABLE :: R(:), R1(:), R2(:)
    END TYPE WORK_VARIABLES

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), ALLOCATABLE, SAVE :: polymers
    TYPE(WIG_FORCE_PARAM), SAVE :: f_param
    TYPE(WORK_VARIABLES), DIMENSION(:), ALLOCATABLE, SAVE :: WORKS

    ABSTRACT INTERFACE
		subroutine move_proposal_type(system,polymer,move_accepted,WORK)
            IMPORT :: gxx_SYS,WIG_POLYMER_TYPE,WORK_VARIABLES
			IMPLICIT NONE
            TYPE(gxx_SYS), INTENT(IN) :: system
			TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
			LOGICAL, INTENT(OUT) :: move_accepted
            CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		end subroutine move_proposal_type

        subroutine update_beads_forces_type(q,polymer)
            IMPORT :: wp,WIG_POLYMER_TYPE
            implicit none
            TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
            REAL(wp), INTENT(in) :: q(:,:,:)
        end subroutine update_beads_forces_type
	END INTERFACE

    PROCEDURE(move_proposal_type), POINTER :: move_proposal => NULL()
    PROCEDURE(update_beads_forces_type), POINTER :: update_beads_forces => NULL()

    PRIVATE
    PUBLIC :: gxx_compute_forces, initialize_auxiliary_simulation! &
                ! ,compute_EW

CONTAINS

!--------------------------------------------------------------
! FORCE CALCULATIONS

    subroutine gxx_compute_forces(system,parameters)
      !$ USE OMP_LIB
      IMPLICIT NONE
      CLASS(gxx_SYS), INTENT(inout) :: system
      CLASS(gxx_PARAM), INTENT(inout) :: parameters
      INTEGER :: i,j,k,INFO,i_thread,c
      TYPE(timer_type) :: timer_full, timer_elem

      if(ANY(ISNAN(system%X_beads))) STOP "ERROR: system diverged!"

      ! GET CLASSICAL FORCE
      system%Forces=0
      DO i=1,system%n_beads
        call get_pot_info(system%X_beads(i,:,:),system%Epot_beads(i),system%F_cl(i,:,:))
        system%Forces=system%Forces+system%F_cl(i,:,:)/system%n_beads
      ENDDO

      i_thread=1
      if(f_param%verbose) then
          write(*,*)
          write(*,*) "Starting gxx forces computation..."
          !$ write(*,*) "(parallel computation)"
          call timer_full%start()
      endif

      
      !$OMP PARALLEL DO LASTPRIVATE(i_thread)
      DO i=1,NUM_AUX_SIM
          !$ i_thread = OMP_GET_THREAD_NUM()+1
          ! if(f_param%reweight_previous_configs) &
          !     call reweight_previous_configs(system,parameters,polymers(i),WORKS(i_thread))            
          call sample_forces(system,parameters,polymers(i),WORKS(i_thread))
          ! if(f_param%moment_matching) call moment_matching(system,parameters,polymers(i),WORKS(i_thread))
      ENDDO
      !$OMP END PARALLEL DO

      ! if(f_param%MC_acceptance_test) then
      !    ! system%acc_ratio = SUM(polymers(:)%MC_acc_ratio/REAL(NUM_AUX_SIM,wp))
      !     if(f_param%verbose) then
      !         write(*,*) "    mean acceptance ratio:", system%acc_ratio 
      !     endif
      ! endif

      ! COMPUTE FORCE ELEMENTS
      if(f_param%verbose) then
          write(*,*) "    Starting force elements calculation..."
          call timer_elem%start()
      endif
      CALL compute_forces_elements(system,parameters)         
      if(f_param%verbose) then
          write(*,*) "    force elements computed in",timer_elem%elapsed_time(),"s."
      endif      
      
      if(f_param%verbose) then
          write(*,*) "g-WiLD forces computed in",timer_full%elapsed_time(),"s."
      endif    

    end subroutine gxx_compute_forces

    subroutine compute_forces_elements(system,parameters)
      IMPLICIT NONE
      CLASS(gxx_SYS), INTENT(inout) :: system
      CLASS(gxx_PARAM), INTENT(inout) :: parameters
      REAL(wp) :: tmp,k2k2F,F(NUM_AUX_SIM),k2(NUM_AUX_SIM),G2(NUM_AUX_SIM),k4(NUM_AUX_SIM)
      INTEGER :: i,j,k,l,p1,p2,p3,c,is,INFO,maxlocdiff(2)
      REAL(wp),ALLOCATABLE :: dQinv(:,:,:),dsqrtQ(:,:,:),sqrtEig(:),sqrtQinv(:,:)
      REAL(wp) :: Ns,tmpscal,mean_weight_ratio,weight_sum

      ! COMPUTE MEAN Q
      system%k2=0
      system%F_beads=0
      system%F_beads(1,:,:)=0.5*system%F_cl(1,:,:)
      system%F_beads(system%n_beads,:,:)=0.5*system%F_cl(system%n_beads,:,:)
      DO i=1,NUM_AUX_SIM
          system%k2=system%k2+polymers(i)%k2/REAL(NUM_AUX_SIM, wp)
          DO j=1,f_param%n_beads
            system%F_beads(1+j,:,:)=system%F_beads(1+j,:,:)+polymers(i)%F(j,:,:)/REAL(NUM_AUX_SIM, wp)
          ENDDO
      ENDDO

    end subroutine compute_forces_elements

!--------------------------------------------------------------
! SAMPLING LOOP
    subroutine sample_forces(system,parameters,polymer,WORK)
        IMPLICIT NONE
        CLASS(gxx_SYS), INTENT(inout) :: system
        CLASS(gxx_PARAM), INTENT(inout) :: parameters
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: m,n_dof,ios
        INTEGER :: i,j,k,l
        LOGICAL :: move_accepted

        call reinitialize_polymer_config(system,polymer,WORK)

        !THERMALIZATION
        DO m=1,f_param%n_steps_therm
            CALL move_proposal(system,polymer,move_accepted,WORK)
        ENDDO

        ! if(f_param%MC_acceptance_test) &
        !     call compute_observables(system,polymer,WORK)

        !PRODUCTION
        DO m=1,f_param%n_steps

            !GENERATE A NEW CONFIG
            CALL move_proposal(system,polymer,move_accepted,WORK)
            ! if(move_accepted) then
                !COMPUTE OBSERVABLES
                call compute_observables(system,polymer,WORK)
            ! endif

            !UPDATE MEAN VALUES
            CALL update_mean_values(polymer,m,WORK)

        ENDDO

        !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
        DO j=2,f_param%n_k2 ; DO i=1,j-1
           polymer%k2(i,j)=polymer%k2(j,i)
        ENDDO ; ENDDO
        

    end subroutine sample_forces

!-------------------------------------------------
! UTILITY SUBROUTINES

    subroutine compute_observables(system,polymer,WORK)
        IMPLICIT NONE
		    CLASS(gxx_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j,k,l
        REAL(wp) :: config_weight

        polymer%Delta_packed=RESHAPE(2*polymer%X(:,:,:),(/f_param%n_k2/))

        !COMPUTE ONLY THE LOWER TRIANGULAR PART
        do j=1,f_param%n_k2 ; do i=j,f_param%n_k2
          WORK%Delta2(i,j)=polymer%Delta_packed(i)*polymer%Delta_packed(j)
        enddo ; enddo

        ! if(f_param%compute_EW) then
        !     do l=1,system%n_dof ; do k=l,system%n_dof ;	do j=k,system%n_dof ; do i=j,system%n_dof
        !         WORK%Delta4(i,j,k,l)=polymer%Delta_packed(i)*polymer%Delta_packed(j) &
        !                         *polymer%Delta_packed(k)*polymer%Delta_packed(l)
        !     enddo ; enddo ; enddo ; enddo
        ! endif

    end subroutine compute_observables

    subroutine update_mean_values(polymer,count,WORK)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER, INTENT(in) :: count
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: k

        polymer%k2=polymer%k2 + (WORK%Delta2-polymer%k2)/count
        polymer%F=polymer%F + (0.5*(polymer%F_beads_plus+polymer%F_beads_minus)-polymer%F)/count        

    end subroutine update_mean_values
!--------------------------------------------------------------
! MOVE GENERATORS
    
    !-----------------------------------
    ! PIOUD steps
    subroutine PIOUD_step(system,polymer,move_accepted,WORK)
		  IMPLICIT NONE
      TYPE(gxx_SYS), INTENT(IN) :: system
		  TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
      LOGICAL, INTENT(out) :: move_accepted
      CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		  integer :: i,j,nu,k
      REAL(wp) :: EigX0(f_param%n_beads), EigV0(f_param%n_beads)

		  nu=f_param%n_beads

      !CALL apply_forces(polymer,0.5_wp*f_param%dt)

      do j=1,system%n_dim ; do i=1,system%n_atoms

          !SAVE INITIAL VALUE TEMPORARILY
          EigV0=polymer%EigV(:,i,j)
          EigX0=polymer%EigX(:,i,j)

          !GENERATE RANDOM VECTORS
          CALL RandGaussN(WORK%R1)
          CALL RandGaussN(WORK%R2)

          !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
          polymer%EigV(:,i,j)=EigV0(:)*f_param%expOmk(:,1,1,i) &
              +EigX0(:)*f_param%expOmk(:,1,2,i) &
              +f_param%OUsig(:,1,1,i)*WORK%R1(:)

          polymer%EigX(1:nu,i,j)=EigV0(:)*f_param%expOmk(:,2,1,i) &
              +EigX0(:)*f_param%expOmk(:,2,2,i) &
              +f_param%OUsig(:,2,1,i)*WORK%R1(:)+f_param%OUsig(:,2,2,i)*WORK%R2(:)
          
          
          !TRANSFORM BACK IN COORDINATES
          ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(i)
          call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
          polymer%X(:,i,j)=polymer%X(:,i,j)/f_param%sqrt_mass(i)
      enddo ; enddo

      !APPLY FORCES	
      CALL update_beads_forces(system%X_beads(2:system%n_beads-1,:,:),polymer)        	
      CALL apply_forces(polymer,f_param%dt)

      move_accepted=.TRUE.

	  end subroutine PIOUD_step

    !-----------------------------------
    ! BAOAB steps
    subroutine BAOAB_step(system,polymer,move_accepted,WORK)
		  IMPLICIT NONE
      TYPE(gxx_SYS), INTENT(IN) :: system
		  TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
      LOGICAL, INTENT(out) :: move_accepted
      CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
      integer :: i,j,nu,k
      REAL(wp) :: EigX0(f_param%n_beads), EigV0(f_param%n_beads)

	  	nu=f_param%n_beads

      !CALL apply_forces(polymer,0.5_wp*f_param%dt)

      do j=1,system%n_dim ; do i=1,system%n_atoms
        

        !HARMONIC PROPAGATION (BLOC A, dt/2)
        if(f_param%BAOAB_num) then
            polymer%EigX(:,i,j)=polymer%EigX(:,i,j)+polymer%EigV(:,i,j)*0.5*f_param%dt
        else
            EigV0=polymer%EigV(:,i,j)
            EigX0=polymer%EigX(:,i,j)

            polymer%EigX(:,i,j)=EigX0(:)*f_param%expOmk(:,1,1,i) &
                +EigV0(:)*f_param%expOmk(:,1,2,i)
            polymer%EigV(:,i,j)=EigV0(:)*f_param%expOmk(:,2,2,i) &
                +EigX0(:)*f_param%expOmk(:,2,1,i)
        endif

        ! OU PROPAGATION (BLOC O, dt)
        !GENERATE RANDOM VECTORS
        CALL RandGaussN(WORK%R1)
        !CALL RandGaussN(WORK%R2)
        
        EigV0(:)=polymer%EigV(:,i,j)*f_param%gammaExp(:,i) &
            + f_param%sigmaBAOAB(:,i)*WORK%R1(:)


        !HARMONIC PROPAGATION (BLOC A, dt/2)
        if(f_param%BAOAB_num) then
            polymer%EigX(:,i,j)=polymer%EigX(:,i,j)+EigV0(:)*0.5*f_param%dt
            polymer%EigV(:,i,j)=EigV0(:)
        else
            EigX0(:)=polymer%EigX(:,i,j)

            polymer%EigX(:,i,j)=EigX0(:)*f_param%expOmk(:,1,1,i) &
                +EigV0(:)*f_param%expOmk(:,1,2,i)
            polymer%EigV(:,i,j)=EigV0(:)*f_param%expOmk(:,2,2,i) &
                +EigX0(:)*f_param%expOmk(:,2,1,i)
        endif

        !TRANSFORM BACK IN COORDINATES
        ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(j)
        call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
        polymer%X(:,i,j)=polymer%X(:,i,j)/f_param%sqrt_mass(i)
      enddo ; enddo

      !APPLY FORCES (BLOC B, dt)	
      CALL update_beads_forces(system%X_beads(2:system%n_beads-1,:,:),polymer)        	
      CALL apply_forces(polymer,f_param%dt)

      if(f_param%BAOAB_num) then
          do j=1,system%n_dim ; do i=1,system%n_atoms
              polymer%EigV(:,i,j)=polymer%EigV(:,i,j) &
                  -f_param%dt*polymer%EigX(:,i,j)*f_param%mOmK(:,i)**2
          enddo ; enddo
      endif

      move_accepted=.TRUE.

	  end subroutine BAOAB_step

    subroutine apply_forces(polymer,tau)
      implicit none
      TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
      REAL(wp), INTENT(in) :: tau
      INTEGER :: i,j,nu
      REAL(wp) :: EigF(f_param%n_beads),F(f_param%n_beads)
      ! REAL(wp) :: Dnu(f_param%n_atoms,f_param%n_dim),Fdelta(f_param%n_atoms,f_param%n_dim)
      ! REAL(wp) :: sig

      nu=f_param%n_beads
      do j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
          !COMPUTE FORCE FOR NORMAL MODES
          F(:)=0.5*(polymer%F_beads_plus(:,i,j)-polymer%F_beads_minus(:,i,j))/f_param%sqrt_mass(i)
          call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,F,1,0._8,EigF,1)

          polymer%EigV(:,i,j)=polymer%EigV(:,i,j)+tau*EigF(:)
      ENDDO ; ENDDO

    end subroutine apply_forces

    function compute_config_weight(q,pot0,potnu,delta,x1,xnu) result(Ener)
        implicit none
        REAL(wp), INTENT(in) :: q(:,:),delta(:,:),x1(:,:),xnu(:,:),pot0,potnu
        REAL(wp) :: Ener,sig
        INTEGER :: i,j
    
        Ener=f_param%dBeta*(pot0+potnu)
        DO j=1,f_param%n_dim ; DO i=1,f_param%n_atoms
            sig=0.5_wp*f_param%sigp2_inv(i)
            Ener=Ener+sig*((q(i,j)-delta(i,j)-xnu(i,j))**2 &
                            +(q(i,j)+delta(i,j)-x1(i,j))**2)            
        ENDDO ; ENDDO

    end function compute_config_weight

!--------------------------------------------------------------
! VARIOUS INITIALIZATIONS

    subroutine reinitialize_polymer_config(system,polymer,WORK)
        IMPLICIT NONE
		    CLASS(gxx_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j

        polymer%k2=0
        ! if(f_param%compute_EW) then
        !     polymer%k4=0
        !     polymer%k6=0
        !     polymer%G4=0
        ! endif
        polymer%F=0
        ! polymer%G=0  
        ! polymer%k1=0      
 
        ! polymer%X_prev=polymer%X
        ! polymer%P_prev=polymer%P
        ! polymer%Delta_prev=polymer%Delta
       ! CALL update_beads_forces(system%X,polymer)
        ! polymer%F_beads_prev=polymer%F_beads
        ! polymer%Pot_beads_prev=polymer%Pot_beads

        ! if(f_param%reweight_previous_configs .OR. f_param%moment_matching) then
        !     polymer%i_step_reweight = polymer%i_step_reweight +1
        !     if(polymer%i_step_reweight>f_param%n_steps_reweight) polymer%i_step_reweight=1
        !     open(newunit=polymer%prev_config_unit &
        !             ,file=polymer%prev_config_filename//"_"//int_to_str(polymer%i_step_reweight) &
        !             ,form="unformatted")
        ! endif

       ! CALL apply_forces(polymer,0.5_wp*f_param%dt)

    end subroutine reinitialize_polymer_config

    subroutine initialize_auxiliary_simulation(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(gxx_SYS), INTENT(inout) :: system
		    CLASS(gxx_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
        TYPE(DICT_STRUCT), pointer :: wig_lib
        INTEGER :: n_dof,n_beads,n_atoms,n_dim,n_k2
        INTEGER :: i,j,k,n_therm_tmp,n_steps_init
        CHARACTER(:), ALLOCATABLE :: pot_name

        n_dof=system%n_dof
        f_param%n_dof=n_dof
        n_atoms=system%n_atoms
        f_param%n_atoms=n_atoms
        n_dim=system%n_dim
        f_param%n_dim=n_dim

        f_param%verbose = param_library%get(GXX_CAT//"/verbose",default=.FALSE.)
        !! @input_file WIGNER_AUX/verbose (gwild, default=.FALSE.)

        NUM_AUX_SIM=param_library%get(GXX_CAT//"/num_aux_sim",default=3)
        !! @input_file WIGNER_AUX/num_aux_sim (gwild, default=3)

        ! AT LEAST 3 AUXILIARY SIMULATIONS FOR DYNAMICS
        ! IF((.not. parameters%only_auxiliary_simulation) .and. NUM_AUX_SIM<3) &
        !     STOP "Error : 'num_aux_sim' must be >=3 for dynamics"
        allocate(polymers(NUM_AUX_SIM))

        wig_lib => param_library%get_child(GXX_CAT)
        !GET NUMBER OF BEADS
        n_beads=system%n_beads-2
        polymers(:)%n_beads=n_beads
        f_param%n_beads=n_beads

        n_k2=n_beads*n_dof
        f_param%n_k2 = n_k2
        
        !GET NUMBER OF AUXILIARY SIMULATION STEPS
        f_param%n_steps=wig_lib%get("n_steps")
        !! @input_file WIGNER_AUX/n_steps (gwild)
        if(f_param%n_steps<=0) &
            STOP "Error: 'n_steps' of '"//GXX_CAT//"' is not properly defined!"

        f_param%n_steps_therm=wig_lib%get("n_steps_therm")
        !! @input_file WIGNER_AUX/n_steps_therm (gwild)
        if(f_param%n_steps_therm<0) &
            STOP "Error: 'n_steps_therm' of '"//GXX_CAT//"' is not properly defined!"

        f_param%k2_smooth=wig_lib%get("k2_smoothing",default=.FALSE.)
        !! @input_file WIGNER_AUX/k2_smoothing (gwild, default=.FALSE.)        
        f_param%k2_smooth_width=wig_lib%get("k2_smooth_width",default=0.03)
        !! @input_file WIGNER_AUX/k2_smooth_width (gwild, default=0.03)
        f_param%k2_smooth_center=wig_lib%get("k2_smooth_center",default=0.9)
        !! @input_file WIGNER_AUX/k2_smooth_center (gwild, default=0.9)
        if(f_param%k2_smooth) then
            ! write(*,*)
            ! write(*,*) "k2 smoothing of off-diagonal terms activated."
            ! write(*,*) "  using fermi function with threshold ",f_param%k2_smooth_center
            ! write(*,*) "  and transition width",f_param%k2_smooth_width
            ! f_param%G_smooth=wig_lib%get("g_smoothing",default=.TRUE.)
            write(*,*) "Warning: smoothing not implemented yet!"
        endif
      
        ! ALLOCATE(f_param%k2_smooth_mat(system%n_dof,system%n_dof))
  

        !INITIALIZE FORCES PARAMETERS
        allocate( &
            f_param%EigMat(n_beads,n_beads), &
            f_param%EigMatTr(n_beads,n_beads), &
            f_param%OmK(n_beads), &
            f_param%mOmK(n_beads,n_atoms), &
            f_param%gammaExp(n_beads,n_atoms), &
            f_param%sigmaBAOAB(n_beads,n_atoms), &
            f_param%sigp2_inv(n_atoms), &
            f_param%sqrt_mass(n_atoms) &
        )
        f_param%beta=parameters%beta
        f_param%dBeta=parameters%dBeta
        f_param%sigp2_inv(:)=parameters%dBeta*system%mass(:)/parameters%tauc2
        f_param%sqrt_mass=sqrt(system%mass)

        ! GET NUMBER OF THREADS        
        f_param%n_threads=1
        !$ f_param%n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

        !INITIALIZE WORK VARIABLES
        allocate(WORKS(f_param%n_threads))
        DO i=1,f_param%n_threads
            allocate( &
                ! WORKS(i)%F(n_dof), &
                WORKS(i)%Delta2(n_k2,n_k2), &
                WORKS(i)%R(n_atoms), &
                WORKS(i)%R1(n_beads), &
                WORKS(i)%R2(n_beads) &
            )
        ENDDO


        !INITIALIZE polymers
        DO i=1,NUM_AUX_SIM
            allocate( &
                polymers(i)%X(n_beads,n_atoms,n_dim), &
                polymers(i)%EigX(n_beads,n_atoms,n_dim), &
                polymers(i)%EigV(n_beads,n_atoms,n_dim), &
                ! polymers(i)%X_prev(0:n_beads,n_atoms,n_dim), &
                ! polymers(i)%P_prev(n_beads,n_atoms,n_dim), &
                polymers(i)%F_beads_plus(n_beads,n_atoms,n_dim), &
                polymers(i)%F_beads_minus(n_beads,n_atoms,n_dim), &
                ! polymers(i)%F_beads_prev(0:n_beads,n_atoms,n_dim), &
                polymers(i)%Pot_beads(n_beads), &
                ! polymers(i)%Pot_beads_prev(0:n_beads), &
               ! polymers(i)%Delta(n_atoms,n_dim), &
                ! polymers(i)%Delta_prev(n_atoms,n_dim), &
                polymers(i)%Delta_packed(n_k2), &
                polymers(i)%k2(n_k2,n_k2), &
                ! polymers(i)%k1(n_dof), &
                ! polymers(i)%k2_reweight(n_dof,n_dof), &
                ! polymers(i)%k2inv(n_dof,n_dof), &
                polymers(i)%F(n_beads,n_atoms,n_dim) &
                ! polymers(i)%F_reweight(n_dof), &
                ! polymers(i)%G(n_dof,n_dof,n_dof), &
                ! polymers(i)%G_reweight(n_dof,n_dof,n_dof) &
            )
            polymers(i)%Delta_packed=0._wp
            ! polymers(i)%Delta_prev=0._wp
            polymers(i)%X=0
            polymers(i)%EigX=0
            polymers(i)%EigV=0
            polymers(i)%F=0
            ! polymers(i)%P_prev=polymers(i)%P
            ! polymers(i)%X_prev=polymers(i)%X    
        ENDDO

        call initialize_bead_forces(param_library)

        call initialize_move_generator(system,parameters,wig_lib)

        DO i=1,NUM_AUX_SIM
          call sample_momenta(polymers(i))
        ENDDO

        ! if(.not. parameters%only_auxiliary_simulation) then
            n_steps_init=wig_lib%get("n_steps_init",default=5000)
            !! @input_file WIGNER_AUX/n_steps_init (gwild, default=5000)
            n_therm_tmp = f_param%n_steps_therm
            f_param%n_steps_therm = n_steps_init
            write(*,*) "Initializing g-WiLD forces with",n_steps_init,"steps"
            call gxx_compute_forces(system,parameters)
            write(*,*) "g-WiLD forces initialization done."
            f_param%n_steps_therm = n_therm_tmp
        ! endif

    end subroutine initialize_auxiliary_simulation


    subroutine sample_momenta(polymer)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER :: i,j,k

        DO k=1,f_param%n_dim ; DO j=1,f_param%n_atoms            
            call randGaussN(polymer%EigV(:,j,k))
            polymer%EigV(:,j,k)=polymer%EigV(:,j,k)/sqrt(f_param%dBeta)   !*f_param%sqrt_mass(j)    
        ENDDO  ; ENDDO
    end subroutine sample_momenta

    subroutine initialize_move_generator(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(gxx_SYS), INTENT(inout) :: system
		    CLASS(gxx_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,j,k,nu,INFO
        REAL(wp) :: TRPMD_lambda, gamma0,gammak
        REAL(wp), ALLOCATABLE ::  TMP(:)

        nu=f_param%n_beads

        f_param%move_generator=wig_lib%get("move_generator" &
                    ,default="BAOAB")
        !! @input_file WIGNER_AUX/move_generator (wigner, default="PIOUD")
        f_param%move_generator=to_upper_case(f_param%move_generator)
        ! parameters%move_generator=f_param%move_generator

        SELECT CASE(f_param%move_generator)
        CASE ("PIOUD","BAOAB")

            !INITIALIZE PIOUD
            ! SHORTHANDS FOR MASS
            call get_polymer_masses(system,wig_lib)
            f_param%dt=wig_lib%get("dt",default=parameters%dt)
            !! @input_file WIGNER_AUX/dt (wigner, default= PARAMETERS/dt, if move_generator="PIOUD")

            !INITIALIZE DYNAMICAL MATRIX
            f_param%EigMat=0
            do i=1,nu-1
                f_param%EigMat(i,i)=2
                f_param%EigMat(i+1,i)=-1
                f_param%EigMat(i,i+1)=-1
            enddo
            f_param%EigMat(nu,nu)=2

            !SOLVE EIGENPROBLEM
            allocate(TMP(3*nu-1))
            CALL DSYEV('V','L',nu,f_param%EigMat,nu,f_param%Omk,TMP,3*nu-1,INFO)
            if(INFO/=0) then
                write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
                stop 
            endif
            deallocate(TMP)
            f_param%EigMatTr=transpose(f_param%EigMat)
            f_param%Omk=SQRT(f_param%Omk)/parameters%tauc
            DO i=1,system%n_atoms
                f_param%mOmk(:,i)=f_param%Omk*sqrt(system%mass(i))/f_param%sqrt_mass(i) 
            ENDDO
            write(*,*)
            write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
            do i=1,nu
                write(*,*) i, f_param%Omk(i)*THz/(2*pi),"THz"
            enddo
            write(*,*)  
                

            allocate(f_param%expOmk(nu,2,2,system%n_atoms))      

            SELECT CASE(f_param%move_generator)
            CASE ("PIOUD")
                CALL initialize_PIOUD(system,wig_lib)
                move_proposal => PIOUD_step
            CASE ("BAOAB")
                move_proposal => BAOAB_step
                gamma0=wig_lib%get("baoab_damping",default=1._wp/f_param%dBeta)
                f_param%BAOAB_num=wig_lib%get("baoab_num",default=.FALSE.)
                TRPMD_lambda=wig_lib%get("trpmd_lambda",default=1._wp)
                DO j=1,f_param%n_atoms ; DO i=1,nu
                    gammak=max(TRPMD_lambda*f_param%mOmk(i,j) &
                                            ,gamma0)
                    f_param%gammaExp(i,j)=exp(-gammak*f_param%dt)
                    f_param%sigmaBAOAB(i,j)=sqrt((1-exp(-2*gammak*f_param%dt))/f_param%dBeta)
                    f_param%expOmk(i,1,1,j)=cos(f_param%mOmk(i,j)*0.5*f_param%dt)
                    f_param%expOmk(i,1,2,j)=sin(f_param%mOmk(i,j)*0.5*f_param%dt)/f_param%mOmk(i,j)
                    f_param%expOmk(i,2,1,j)=-sin(f_param%mOmk(i,j)*0.5*f_param%dt)*f_param%mOmk(i,j)
                    f_param%expOmk(i,2,2,j)=cos(f_param%mOmk(i,j)*0.5*f_param%dt)
                ENDDO ; ENDDO
            END SELECT

        CASE DEFAULT
            write(0,*) "Error: unknown move generator '"//f_param%move_generator//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_move_generator

    subroutine get_polymer_masses(system,wig_lib)
        IMPLICIT NONE
        CLASS(gxx_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        TYPE(DICT_STRUCT), POINTER :: m_lib
        CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
        LOGICAL, ALLOCATABLE :: is_sub(:)
        REAL(wp) :: m_tmp
        INTEGER :: i
        CHARACTER(2) :: symbol

        
        if(.not. wig_lib%has_key("modified_masses")) then
            f_param%sqrt_mass=SQRT(system%mass)
            return
        endif

        m_lib => wig_lib%get_child("modified_masses")
        write(*,*)
        write(*,*) "g-WiLD auxiliary masses:"
        call dict_list_of_keys(m_lib,keys,is_sub)
        DO i=1,size(keys)
			if(is_sub(i)) CYCLE
            m_tmp=m_lib%get(keys(i))
            symbol=trim(keys(i))
            symbol(1:1)=to_upper_case(symbol(1:1))
            write(*,*) symbol," : ", real(m_tmp/Mprot),"amu"
        ENDDO
        write(*,*)
        do i=1, system%n_atoms
            f_param%sqrt_mass(i)=m_lib%get(trim(system%element_symbols(i)) &
                                            ,default=system%mass(i) )
            !if(f_param%sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", f_param%sqrt_mass(i)/Mprot," amu"                    
        enddo
        f_param%sqrt_mass=sqrt(f_param%sqrt_mass)

    end subroutine get_polymer_masses

    subroutine initialize_PIOUD(system,wig_lib)
        IMPLICIT NONE
        CLASS(gxx_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,nu,j
        REAL(wp), ALLOCATABLE ::thetaInv(:,:,:,:),OUsig2(:,:,:,:)
        REAL(wp) :: Id(2,2)
        

        nu=f_param%n_beads

        Id=0
        Id(1,1)=1
        Id(2,2)=1

        allocate( &
            thetaInv(nu,2,2,system%n_atoms), &
            OUsig2(nu,2,2,system%n_atoms) &
        )
        if(allocated(f_param%OUsig)) deallocate(f_param%OUsig)
		allocate(f_param%OUsig(nu,2,2,system%n_atoms)) 
        if(allocated(f_param%expOmk)) deallocate(f_param%expOmk)
		allocate(f_param%expOmk(nu,2,2,system%n_atoms))              

        DO i=1,system%n_atoms
            f_param%expOmk(:,1,1,i)=exp(-f_param%mOmk(:,i)*f_param%dt) &
                                    *(1-f_param%mOmk(:,i)*f_param%dt)
            f_param%expOmk(:,1,2,i)=exp(-f_param%mOmk(:,i)*f_param%dt) &
                                    *(-f_param%dt*f_param%mOmk(:,i)**2)
            f_param%expOmk(:,2,1,i)=exp(-f_param%mOmk(:,i)*f_param%dt)*f_param%dt
            f_param%expOmk(:,2,2,i)=exp(-f_param%mOmk(:,i)*f_param%dt) &
                                    *(1+f_param%mOmk(:,i)*f_param%dt)

            thetaInv(:,1,1,i)=0._wp
            thetaInv(:,1,2,i)=-1._wp
            thetaInv(:,2,1,i)=1._wp/f_param%mOmk(:,i)**2
            thetaInv(:,2,2,i)=2._wp/f_param%mOmk(:,i)
        ENDDO

        !COMPUTE COVARIANCE MATRIX
        DO i=1,system%n_atoms
            OUsig2(:,1,1,i)=(1._wp- (1._wp-2._wp*f_param%dt*f_param%mOmk(:,i) &
                                +2._wp*(f_param%dt*f_param%mOmk(:,i))**2 &
                            )*exp(-2._wp*f_param%mOmk(:,i)*f_param%dt) &
                            )/f_param%dBeta
            OUsig2(:,2,2,i)=(1._wp- (1._wp+2._wp*f_param%dt*f_param%mOmk(:,i) &
                                +2._wp*(f_param%dt*f_param%mOmk(:,i))**2 &
                            )*exp(-2._wp*f_param%mOmk(:,i)*f_param%dt) &
                            )/(f_param%dBeta*f_param%mOmk(:,i)**2)
            OUsig2(:,2,1,i)=2._wp*f_param%mOmk(:,i)*(f_param%dt**2) &
                            *exp(-2._wp*f_param%mOmk(:,i)*f_param%dt)/f_param%dBeta
            OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

            !COMPUTE CHOLESKY DECOMPOSITION
            f_param%OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
            f_param%OUsig(:,1,2,i)=0
            f_param%OUsig(:,2,1,i)=OUsig2(:,2,1,i)/f_param%OUsig(:,1,1,i)
            f_param%OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-f_param%OUsig(:,2,1,i)**2)
        ENDDO

		!CHECK CHOLESKY DECOMPOSITION
		! write(*,*) "sum of squared errors on cholesky decomposition:"
		! do i=1,nu
		! 	write(*,*) i, sum( (matmul(f_param%OUsig(i,:,:),transpose(f_param%OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
		! enddo

    end subroutine initialize_PIOUD

    subroutine initialize_bead_forces(param_library)
        IMPLICIT NONE        
        TYPE(DICT_STRUCT), intent(in) :: param_library
        CHARACTER(:), ALLOCATABLE :: pot_name

        f_param%parallel_forces=.FALSE.
		    pot_name=param_library%get("POTENTIAL/pot_name")
		    if(to_upper_case(trim(pot_name))=="SOCKET") f_param%parallel_forces=.TRUE.

        IF(f_param%parallel_forces) THEN
            update_beads_forces => update_beads_forces_socket_parallel
        ELSE
            update_beads_forces => update_beads_forces_serial
        ENDIF

    end subroutine initialize_bead_forces

!----------------------------------------------------------------------

    subroutine update_beads_forces_serial(q,polymer)
      implicit none
      TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
      REAL(wp), INTENT(in) :: q(:,:,:)
      INTEGER :: i,nu
      REAL(wp) :: Pot
          
        nu=f_param%n_beads
        ! write(75,*) polymer%X(:,:,:)
        do i=1,nu          
          call get_pot_info(q(i,:,:)+polymer%X(i,:,:),polymer%Pot_beads(i),polymer%F_beads_plus(i,:,:))
          call get_pot_info(q(i,:,:)-polymer%X(i,:,:),Pot,polymer%F_beads_minus(i,:,:))
          polymer%Pot_beads(i)=0.5*(polymer%Pot_beads(i)+Pot)
        enddo
    end subroutine update_beads_forces_serial

    subroutine update_beads_forces_socket_parallel(q,polymer)
        !$ USE OMP_LIB
        implicit none
        TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        REAL(wp), INTENT(in) :: q(:,:,:)
        INTEGER :: i,is,nu
        REAL(wp), ALLOCATABLE :: Xtmp(:,:,:),Pot(:)

        nu=f_param%n_beads
        is=1        
        !$ is=OMP_GET_THREAD_NUM()+1
        ALLOCATE(Xtmp(nu,f_param%n_atoms,f_param%n_dim))
        ALLOCATE(Pot(nu))
        DO i=1,nu
            Xtmp(i,:,:) = q(i,:,:) + polymer%X(i,:,:)
        ENDDO
		    CALL parallel_socket_pot_info(Xtmp,polymer%F_beads_plus,polymer%Pot_beads,i_socket=is)
        DO i=1,nu
            Xtmp(i,:,:) = q(i,:,:) - polymer%X(i,:,:)
        ENDDO
		    CALL parallel_socket_pot_info(Xtmp,polymer%F_beads_minus,Pot,i_socket=is)
        polymer%Pot_beads=0.5*(polymer%Pot_beads + Pot)

        DEALLOCATE(Xtmp)
        DEALLOCATE(Pot)

	end subroutine update_beads_forces_socket_parallel

END MODULE gxx_forces
