module gxx_types
  USE kinds
  USE rpmd_types
  USE string_operations
  USE random
  IMPLICIT NONE

  TYPE, EXTENDS(POLYMER_TYPE) :: gxx_SYS
    REAL(wp), allocatable :: k2(:,:)
    REAL(wp), allocatable :: x1xnu(:,:), gxx_mean(:,:), gxx_num_mean(:,:)
    REAL(wp), allocatable :: F_cl(:,:,:)
    REAL(wp), allocatable :: dF(:)
    REAL(wp) :: k2_weight,k2_weight_mean,k2_weight_free
    INTEGER :: n_k2
    REAL(wp), allocatable :: k2_free(:,:),Q2_free(:,:)
  END TYPE

  TYPE, EXTENDS(RPMD_PARAM) :: gxx_PARAM
    REAL(wp) :: gxx_t, gxx_dt, gxx_dt2
    REAL(wp) :: tauc, tauc2
    INTEGER :: n_k2
    LOGICAL :: use_k2_free
  END TYPE

  ABSTRACT INTERFACE
		subroutine gxx_sub(system,parameters)
			import :: gxx_SYS, gxx_PARAM
			IMPLICIT NONE
			CLASS(gxx_SYS), intent(inout) :: system
			CLASS(gxx_PARAM), intent(inout) :: parameters
		end subroutine gxx_sub
	END INTERFACE

  CHARACTER(*), PARAMETER :: GXX_CAT="WIGNER_AUX"

CONTAINS

  subroutine update_gxx(system,parameters,istep)
    CLASS(gxx_SYS) :: system
    CLASS(gxx_PARAM) :: parameters
    INTEGER, INTENT(in) :: istep
    INTEGER :: c, i,j,k
    REAL(wp) :: dx(system%n_k2)

    c=0
    DO j=1,system%n_dim; DO i=1,system%n_atoms
      DO k=2,system%n_beads-1
        c=c+1
        dx(c)=(system%mass(i)/parameters%tauc2)*( 2*system%X_beads(k,i,j) &
                -system%X_beads(k+1,i,j)-system%X_beads(k-1,i,j) &
              )
        system%dF(c) =  system%F_cl(k,i,j) + dx(c)
      ENDDO
    ENDDO ; ENDDO

    system%k2_weight_free=dot_product(dx,matmul(system%k2_free,dx))

    system%k2_weight=dot_product(system%dF,matmul(system%k2,system%dF))
    if(parameters%use_k2_free) then
      system%k2_weight=exp(-parameters%gxx_dt2*(system%k2_weight-system%k2_weight_free))
    else 
      system%k2_weight=exp(-parameters%gxx_dt2*system%k2_weight)
    endif
    system%k2_weight_free=dot_product(system%dF,matmul(system%k2_free,system%dF))
    system%k2_weight_free=exp(-parameters%gxx_dt2*system%k2_weight_free)

    system%k2_weight_mean = system%k2_weight_mean + ( &
        system%k2_weight - system%k2_weight_mean  &   
      )/REAL(istep,wp)

    system%x1xnu(:,:)=system%X_beads(1,:,:)*system%X_beads(system%n_beads,:,:)
    system%gxx_num_mean(:,:) = system%gxx_num_mean(:,:) + ( &
        system%x1xnu(:,:)*system%k2_weight - system%gxx_num_mean(:,:) & 
      )/REAL(istep,wp)
    
    system%gxx_mean = system%gxx_num_mean/system%k2_weight_mean
  end subroutine update_gxx

end module gxx_types