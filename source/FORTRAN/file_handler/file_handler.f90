MODULE file_handler
	USE kinds
	USE trajectory_files	
	USE nested_dictionaries
	IMPLICIT NONE

	CHARACTER(*), PARAMETER :: DEFAULT_NAME="default_output"
	CHARACTER(*), PARAMETER :: DESCRIPTION_FILE_NAME="output_description.info"

    type OUTPUT_TYPE
		CHARACTER(:), allocatable :: name
		INTEGER :: n_files_max=200
		INTEGER :: n_files=0
		TYPE(TRAJ_FILE), allocatable :: files(:)
		CHARACTER(:), allocatable :: working_directory
		CHARACTER(:), allocatable :: base_directory
	CONTAINS
		PROCEDURE :: add_file => output_add_file
		PROCEDURE, PRIVATE :: create_file_from_val => output_create_file_from_val
		PROCEDURE, PRIVATE :: create_file_from_dict => output_create_file_from_dict
		GENERIC :: create_file => create_file_from_val,create_file_from_dict
		PROCEDURE :: write_all => output_write_all_files 
		PROCEDURE :: write_file => output_write_file
		PROCEDURE :: add_column_to_file => output_add_column_to_file
		PROCEDURE :: add_array_to_file => output_add_array_to_file
		PROCEDURE :: add_xyz_to_file => output_add_xyz_to_file
		PROCEDURE :: get_file => output_get_file
		PROCEDURE :: get_file_index => output_get_file_index
		PROCEDURE :: set_working_directory => output_set_working_directory
		PROCEDURE :: open_files => output_open_files
		PROCEDURE :: close_files => output_close_files
		PROCEDURE :: destroy => output_destroy
		PROCEDURE :: write_description_file => output_write_description_file
	end type OUTPUT_TYPE

	type(OUTPUT_TYPE), SAVE :: output

	PRIVATE
	PUBLIC :: OUTPUT_TYPE, output

CONTAINS

	subroutine output_open_files(output,append)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		LOGICAL, INTENT(in), OPTIONAL :: append
		INTEGER :: i

		if(output%n_files<=0) then
			write(0,*) "Warning: no file in output!"
			return
		endif

		do i=1,output%n_files
			call output%files(i)%open(output%working_directory,append)
		enddo
	end subroutine output_open_files

	subroutine output_close_files(output)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		INTEGER :: i

		if(output%n_files<=0) then
			!write(0,*) "Warning: no file in output!"
			return
		endif

		do i=1,output%n_files
			call output%files(i)%close()
		enddo
	end subroutine output_close_files

	subroutine output_destroy(output)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		INTEGER :: i

		if(output%n_files>0) then
			do i=1,output%n_files
				call output%files(i)%destroy()
			enddo
		endif

		deallocate(output%files)

		write(*,*) "Output correctly closed."

	end subroutine output_destroy

	subroutine output_set_working_directory(output,base_dir,file_dir)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		CHARACTER(*), INTENT(in) :: base_dir,file_dir
		LOGICAL :: dir_exists, search_dir
		integer :: dir_stop, dir_start
		CHARACTER(:), allocatable :: subdir,full_dir,directory

		directory=base_dir//"/"//file_dir

		if(.not. allocated(output%files)) then
			allocate(output%files(output%n_files_max))
		endif

		full_dir=trim(directory)
		subdir=""
		dir_start=1
		dir_stop=INDEX(full_dir,"/")-1
		search_dir=.TRUE.
		do while(search_dir)
			if(dir_stop==0) then
				subdir=subdir//"/"
			else
				if(dir_stop<0) then
					dir_stop=len(full_dir)
					search_dir=.FALSE.
				endif
				subdir=full_dir(:dir_stop)
				INQUIRE(file=subdir,exist=dir_exists)
				if(.not. dir_exists) then
					call system("mkdir "//subdir)
				endif
			endif
			dir_start=dir_start+dir_stop+1
			dir_stop=INDEX(full_dir(dir_start:),"/")-1
		enddo

		output%base_directory=trim(base_dir)
		output%working_directory=trim(directory)

	end subroutine output_set_working_directory

	subroutine output_create_file_from_val(output,name,formatted,stride,unit,description,index)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		CHARACTER(*), INTENT(in) :: name
		LOGICAL, INTENT(in), OPTIONAL :: formatted
		INTEGER, INTENT(in), OPTIONAL :: stride
		INTEGER, INTENT(out), OPTIONAL :: index
		CHARACTER(*), INTENT(in), OPTIONAL :: unit
		CHARACTER(*), INTENT(in), OPTIONAL :: description
		TYPE(TRAJ_FILE) :: file

		call file%init(name,formatted,stride,unit,description)
		call output%add_file(file,index)

	end subroutine output_create_file_from_val

	subroutine output_create_file_from_dict(output,dict,default_name,index)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		CHARACTER(*), INTENT(in) :: default_name
		CLASS(DICT_STRUCT), INTENT(in) :: dict
		INTEGER, INTENT(out), OPTIONAL :: index
		TYPE(TRAJ_FILE) :: file

		call file%init(dict,default_name)
		call output%add_file(file,index)

	end subroutine output_create_file_from_dict

	subroutine output_add_file(output,file,index)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		TYPE(TRAJ_FILE), INTENT(INOUT) :: file
		INTEGER, INTENT(out), OPTIONAL :: index
		INTEGER :: i_check

		if(.not. allocated(output%files)) then
			output%working_directory="./"
			allocate(output%files(output%n_files_max))
		endif

		i_check=output%get_file_index(file%name)
		if(i_check > 0) then
			write(0,*) "Error: file '"//file%name//"' already exists!"
			STOP "Execution stopped."
		endif

		output%n_files=output%n_files+1
		output%files(output%n_files) = file
		if(present(index)) index=output%n_files

	end subroutine output_add_file

	subroutine output_add_column_to_file(output,value,unit,file,name,index)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		REAL(wp), target :: value
		CHARACTER(*), INTENT(in), OPTIONAL :: unit
		CHARACTER(*), INTENT(in), OPTIONAL :: file
		CHARACTER(*), INTENT(in), OPTIONAL :: name
		INTEGER, INTENT(in), OPTIONAL :: index
		INTEGER :: i_file

		if(present(index) .or. present(file)) then
			i_file = output%get_file_index(file,index)
			if(i_file<=0) then
				if(present(index)) then
					write(0,*) "Error: could not find file at index ", index
				elseif(present(file)) then
					write(0,*) "Error: could not find file : "//file
				endif
				STOP "Execution stopped."
			endif
			call output%files(i_file)%add_column(value,unit,name=name)
		else
			if(output%n_files > 0) then
				call output%files(output%n_files)%add_column(value,unit,name=name)
			else
				STOP "Error: no file in output!"
			endif
		endif
	end subroutine output_add_column_to_file

	subroutine output_add_array_to_file(output,value,unit,index,file,name)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		REAL(wp), target :: value(:)
		CHARACTER(*), INTENT(in), OPTIONAL :: unit
		CHARACTER(*), INTENT(in), OPTIONAL :: file
		CHARACTER(*), INTENT(in), OPTIONAL :: name
		INTEGER, INTENT(in), OPTIONAL :: index
		INTEGER :: i_file

		if(present(index) .or. present(file)) then
			i_file = output%get_file_index(file,index)
			if(i_file<=0) then
				if(present(index)) then
					write(0,*) "Error: could not find file at index ", index
				elseif(present(file)) then
					write(0,*) "Error: could not find file : "//file
				endif
				STOP "Execution stopped."
			endif
			call output%files(i_file)%add_array(value,unit,name=name)
		else
			if(output%n_files > 0) then
				call output%files(output%n_files)%add_array(value,name=unit)
			else
				STOP "Error: no file in output!"
			endif
		endif
	end subroutine output_add_array_to_file

	subroutine output_add_xyz_to_file(output,value,symbols,unit,index,file,name)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		REAL(wp), target :: value(:,:)
		CHARACTER(2), target :: symbols(:)
		CHARACTER(*), INTENT(in), OPTIONAL :: unit
		CHARACTER(*), INTENT(in), OPTIONAL :: file
		CHARACTER(*), INTENT(in), OPTIONAL :: name
		INTEGER, INTENT(in), OPTIONAL :: index
		INTEGER :: i_file

		if(present(index) .or. present(file)) then
			i_file = output%get_file_index(file,index)
			if(i_file<=0) then
				if(present(index)) then
					write(0,*) "Error: could not find file at index ", index
				elseif(present(file)) then
					write(0,*) "Error: could not find file : "//file
				endif
				STOP "Execution stopped."
			endif
			call output%files(i_file)%add_xyz(value,symbols,unit,name=name)
		else
			if(output%n_files > 0) then
				call output%files(output%n_files)%add_xyz(value,symbols,name=name)
			else
				STOP "Error: no file in output!"
			endif
		endif
	end subroutine output_add_xyz_to_file

	function output_get_file(output,name,index) result(file)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		INTEGER, INTENT(in), OPTIONAL :: index
		CHARACTER(*), INTENT(in), OPTIONAL :: name
		TYPE(TRAJ_FILE) :: file
		INTEGER :: i_file

		i_file=output%get_file_index(name,index)
		if(i_file<=0) then
			if(present(index)) then
				write(0,*) "Error: could not find file at index ", index
			elseif(present(name)) then
				write(0,*) "Error: could not find file : "//name
			endif
			STOP "Execution stopped."
		endif
		file = output%files(i_file)

	end function output_get_file

	function output_get_file_index(output,name,index) result(i_file)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		CHARACTER(*), INTENT(in), OPTIONAL :: name
		INTEGER, INTENT(in), OPTIONAL :: index
		INTEGER :: i_file
		LOGICAL :: found_file
		INTEGER :: i

		if(present(index)) then
			if(index>0 .and. index<=output%n_files) then
				i_file=index
			else
				i_file=0
			endif
		elseif(present(name)) then
			found_file=.FALSE.
			i_file=0
			do i=1,output%n_files
				if(output%files(i)%name==trim(name)) then
					if(found_file) &
						STOP "Error: cannot have two files with the same name!"
					i_file=i
					found_file=.TRUE.
				endif
			enddo
		else
			STOP "Error: no data passed to find the file!"
		endif
	end function output_get_file_index
		

	subroutine output_write_all_files(output)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		INTEGER :: i

		do i=1,output%n_files
			call output%files(i)%write_line()
			!write(*,*) output%files(i)%get_line()
		enddo

	end subroutine output_write_all_files

	subroutine output_write_file(output,name)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		CHARACTER(*), INTENT(in) :: name
		INTEGER :: i

		i=output%get_file_index(name)
		if (i>0) then
			call output%files(i)%write_line()
			!write(*,*) output%files(i)%get_line()
		else
			write(0,*) "Error: '"//trim(name)//"' does not exist!"
			STOP "Execution stopped."
		endif

	end subroutine output_write_file

	subroutine output_write_description_file(output,clear)
		IMPLICIT NONE
		CLASS(OUTPUT_TYPE) :: output
		LOGICAL, INTENT(in), OPTIONAL :: clear
		INTEGER :: i,u
		CHARACTER(:), allocatable :: pos
		TYPE(TRAJ_FILE) :: file

		if(output%n_files<=0) RETURN

		pos="APPEND"
		if(present(clear)) then
			if(clear) pos="REWIND"
		endif

		open(file=output%working_directory//"/"//DESCRIPTION_FILE_NAME, &
				newunit=u, position=pos)

		DO i=1,output%n_files
			file=output%get_file(index=i)
			CALL file%print_description(u)
			WRITE(u,*)
		ENDDO

		close(u)

	end subroutine output_write_description_file
	

END MODULE file_handler