MODULE trajectory_files
	USE kinds
	USE vectors
	USE atomic_units
	USE nested_dictionaries
	IMPLICIT NONE

	LOGICAL :: DEFAULT_FORMATTED=.TRUE.
	CHARACTER(*), PARAMETER :: DEFAULT_POS="REWIND"
	CHARACTER(*), PARAMETER :: DEFAULT_UNIT="au"
	LOGICAL :: DEFAULT_TIME_COL=.FALSE.
	INTEGER :: DEFAULT_STRIDE=1

	INTEGER, PARAMETER :: max_columns = 15

	type, extends(VECTOR_DATA) :: FILE_COL
		LOGICAL :: is_array
		REAL(wp), pointer :: data_scalar => null()
		REAL(wp), dimension(:), pointer :: data_array => null()
		REAL(wp) :: output_unit=1._wp
		CHARACTER(:), ALLOCATABLE :: unit_str
		CHARACTER(:), ALLOCATABLE :: name
	end type

	type, extends(VECTOR_DATA) :: XYZ_DATA
		REAL(wp), dimension(:,:), pointer :: data_array => null()
		CHARACTER(2), dimension(:), pointer :: symbols => null()
		INTEGER :: n_atoms
		REAL(wp) :: output_unit=1._wp
		CHARACTER(:), ALLOCATABLE :: unit_str
		CHARACTER(:), ALLOCATABLE :: name
		CHARACTER(:), ALLOCATABLE :: n_at_char
	end type

	type, extends(VECTOR) :: TRAJ_FILE
		CHARACTER(:), ALLOCATABLE :: name
		INTEGER :: unit
		INTEGER :: n_total_columns
		LOGICAL :: formatted
		CHARACTER(:), ALLOCATABLE :: format
		CHARACTER(:), ALLOCATABLE :: access
		LOGICAL :: is_open=.FALSE.
		CHARACTER(:), ALLOCATABLE :: in_directory
		INTEGER :: stride,stride_count
		CHARACTER(:), ALLOCATABLE :: default_unit
		CHARACTER(:), ALLOCATABLE :: description
		LOGICAL :: has_time_column
		LOGICAL :: is_xyz_file
	CONTAINS
		PROCEDURE,PRIVATE :: init_from_val => traj_file_init_from_val
		PROCEDURE,PRIVATE :: init_from_dict => traj_file_init_from_dict
		GENERIC :: init => init_from_val ,init_from_dict
		PROCEDURE :: get_column => traj_file_get_column
		PROCEDURE :: get_line => traj_file_get_line
		PROCEDURE :: add_column => traj_file_add_column
		PROCEDURE :: add_array => traj_file_add_array
		PROCEDURE :: add_xyz => traj_file_add_xyz
		PROCEDURE :: write_line => traj_file_write_line
		PROCEDURE :: open => traj_file_open
		PROCEDURE :: close => traj_file_close
		PROCEDURE :: destroy => traj_file_destroy
		PROCEDURE :: rename => traj_file_rename
		PROCEDURE :: n_col => traj_file_n_col
		PROCEDURE :: print_description => traj_file_print_description
		PROCEDURE :: edit_description => traj_file_edit_description
		PROCEDURE :: update_format => traj_file_update_format
	end type

	PRIVATE
	PUBLIC :: 	TRAJ_FILE, &
                FILE_COL, &
				DEFAULT_FORMATTED, &
				DEFAULT_POS, &
				DEFAULT_UNIT, &
				DEFAULT_TIME_COL, &
				DEFAULT_STRIDE
CONTAINS

	subroutine traj_file_open(file,directory,append)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		CHARACTER(*), INTENT(in),OPTIONAL :: directory
		CHARACTER(:), ALLOCATABLE :: fullname
		LOGICAL,INTENT(in), OPTIONAL :: append
		INTEGER :: ios
		CHARACTER(:), allocatable :: file_pos

		if(file%n_total_columns<=0) then
			write(0,*) "Warning: file '"//file%name//"' is empty and will not be opened."
			RETURN
		endif

		file_pos=DEFAULT_POS
		if(present(append)) then
			if(append) then
				file_pos="APPEND"
			else
				file_pos="REWIND"
			endif
		endif

		if(present(directory)) then
			file%in_directory=directory//"/"
			fullname=directory//"/"//trim(file%name)
		else
			file%in_directory="./"
			fullname=trim(file%name)
		endif

		open(NEWUNIT=file%unit,file=fullname, access=file%access &
				,position=file_pos, form=file%format, iostat=ios)
		
		if(ios/=0) then
			write(0,*) "Error: could not open '"//fullname//"' properly!"
			STOP "Execution stopped"
		endif

		file%is_open=.TRUE.
		file%in_directory=directory

		write(*,*) "file '"//fullname//"' correctly opened."

	end subroutine traj_file_open

	subroutine traj_file_close(file)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER :: ios

		if(file%is_open) then
			close(file%unit,iostat=ios)
			if(ios/=0) then
				write(0,*) "Error: could not close '"//file%in_directory//file%name//"' properly!"
			else
				write(*,*) "file '"//file%in_directory//"/"//file%name//"' &
					&correctly closed."
			endif
			file%is_open=.FALSE.
		endif

	end subroutine traj_file_close

	subroutine traj_file_init_from_val(file, name,formatted,stride,unit,description)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		CHARACTER(*), INTENT(in) :: name
		LOGICAL, INTENT(in), OPTIONAL :: formatted
		INTEGER,INTENT(in), OPTIONAL :: stride
		CHARACTER(*), INTENT(in), OPTIONAL :: unit
		CHARACTER(*), INTENT(in), OPTIONAL :: description


		file%is_xyz_file=.FALSE.

		if(present(formatted)) then
			call file%update_format(formatted)
		else
			call file%update_format(DEFAULT_FORMATTED)
		endif

		if(present(stride)) then
			file%stride=stride
		else
			file%stride=DEFAULT_STRIDE
		endif
		file%stride_count=0

		file%default_unit=DEFAULT_UNIT
		if(present(unit)) file%default_unit=unit

		file%description=""
		if(present(description)) file%description=description
		
		

		call vector_create(file)
		file%name=trim(name)
	!	file%unit=last_unit
	!	last_unit=last_unit+1
		file%n_total_columns=0
		file%is_open=.FALSE.
		file%in_directory="."
		file%has_time_column=DEFAULT_TIME_COL

	end subroutine traj_file_init_from_val

	subroutine traj_file_init_from_dict(file,dict,default_name)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		CLASS(DICT_STRUCT),INTENT(IN) :: dict		
		CHARACTER(*), INTENT(in) :: default_name

		file%is_xyz_file=.FALSE.

		file%name=dict%get("name",default=default_name)
		call dict%store("name",file%name)

		call file%update_format(dict%get("formatted" &
								,default=DEFAULT_FORMATTED))
		file%stride=dict%get("stride",default=DEFAULT_STRIDE)
		file%stride_count=0		
		file%default_unit=dict%get("unit",default=DEFAULT_UNIT)
		file%description=dict%get("description",default="")
		file%has_time_column=dict%get("time_column",default=DEFAULT_TIME_COL)

		call vector_create(file)
		file%n_total_columns=0
		file%is_open=.FALSE.
		file%in_directory="."		

	end subroutine traj_file_init_from_dict

	subroutine traj_file_rename(file,name)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		CHARACTER(*), INTENT(in) :: name

		if(trim(name) == "") then
			write(0,*) "Warning: cannot rename a file to an empty string!"
		else
			write(*,*) "Renaming file '"//file%name//"' to '"//trim(name)//"'."
			file%name=trim(name)
		endif

	end subroutine traj_file_rename

	subroutine  traj_file_edit_description(file,description)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		CHARACTER(*), INTENT(in) :: description

		file%description=description

	end subroutine traj_file_edit_description


	subroutine traj_file_destroy(file)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		
	!	write(*,*) "closing file '"//file%name//"'."
		call file%close()
		call vector_destroy(file)

	end subroutine traj_file_destroy

	subroutine traj_file_update_format(file,formatted)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		LOGICAL, intent(in) :: formatted

		file%formatted=formatted
		if(formatted) then
			file%format="formatted"
			file%access="sequential"
		else
			file%format="unformatted"
			file%access="stream"
		endif

	end subroutine traj_file_update_format

	subroutine traj_file_add_column(file,value,unit,name)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		REAL(wp), target :: value
		TYPE(FILE_COL) :: new_col
		CHARACTER(*), intent(in), OPTIONAL :: unit
		CHARACTER(*), intent(in), OPTIONAL :: name

		if(vector_is_empty(file)) &
            STOP "Error: file has not been initialized!"
		if(file%is_xyz_file) &
			STOP "Error: can have only 1 data in xyz file!"

		if(present(unit)) then
			new_col%output_unit=atomic_units_get_multiplier(unit)
			new_col%unit_str=unit
		else
			new_col%output_unit=atomic_units_get_multiplier(file%default_unit)
			new_col%unit_str=file%default_unit
		endif

		new_col%name="SCALAR"
		if(present(name)) new_col%name=trim(name)
		
		new_col%data_scalar => value
		new_col%is_array=.FALSE.

		call vector_append(file,new_col)
		file%n_total_columns=file%n_total_columns+1

	end subroutine traj_file_add_column

	subroutine traj_file_add_array(file,value,unit,name)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		REAL(wp), INTENT(in), target :: value(:)
		TYPE(FILE_COL) :: new_col
		CHARACTER(*), intent(in), OPTIONAL :: unit
		CHARACTER(*), intent(in), OPTIONAL :: name

		if(vector_is_empty(file)) &
            STOP "Error: file has not been initialized!"
		if(file%is_xyz_file) &
			STOP "Error: can have only 1 data in xyz file!"

		if(present(unit)) then
			new_col%output_unit=atomic_units_get_multiplier(unit)
			new_col%unit_str=unit
		else
			new_col%output_unit=atomic_units_get_multiplier(file%default_unit)
			new_col%unit_str=file%default_unit
		endif

		new_col%name="ARRAY"
		if(present(name)) new_col%name=trim(name)
		
		new_col%data_array => value
		new_col%is_array=.TRUE.		

		call vector_append(file,new_col)
		file%n_total_columns=file%n_total_columns+size(value)

	end subroutine traj_file_add_array

	subroutine traj_file_add_xyz(file,value,symbols,unit,name)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		REAL(wp), INTENT(in), target :: value(:,:)
		CHARACTER(2), INTENT(in), target :: symbols(:)
		TYPE(XYZ_DATA) :: new_col
		CHARACTER(*), intent(in), OPTIONAL :: unit
		CHARACTER(*), intent(in), OPTIONAL :: name
		CHARACTER(100) :: ncol_char

		if(vector_is_empty(file)) &
            STOP "Error: file has not been initialized!"
		! if(vector_size(file) > 0) &
		! 	STOP "Error: can have only 1 data in xyz file!"

		if(present(unit)) then
			new_col%output_unit=atomic_units_get_multiplier(unit)
			new_col%unit_str=unit
		else
			new_col%output_unit=atomic_units_get_multiplier(file%default_unit)
			new_col%unit_str=file%default_unit
		endif

		new_col%name="XYZ DATA"
		if(present(name)) new_col%name=trim(name)
		
		new_col%data_array => value	
		new_col%symbols => symbols	
		new_col%n_atoms=size(value,1)
		
		write(ncol_char,*) new_col%n_atoms
		new_col%n_at_char=trim(adjustl(ncol_char))


		call vector_append(file,new_col)
		file%n_total_columns=file%n_total_columns+size(value)

		file%is_xyz_file=.TRUE.
		call traj_file_update_format(file,formatted=.TRUE.)

	end subroutine traj_file_add_xyz

	function traj_file_n_col(file) result(n_col)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER :: n_col

		n_col=vector_size(file)
	end function traj_file_n_col

	function traj_file_get_column(file,i_col) result(col)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER, INTENT(in) :: i_col
		CLASS(FILE_COL), pointer :: col
		CLASS(VECTOR_DATA), pointer :: vec_data

		if(i_col<=0 .or. i_col > vector_size(file)) then
			col => null()
		else
			vec_data => vector_at(file,i_col)
			SELECT TYPE(vec_data)
			CLASS is (FILE_COL)
				col => vec_data
			CLASS DEFAULT
				STOP "Error: type error in trajectory_files!"
			END SELECT
		endif

	end function traj_file_get_column

	function traj_file_get_xyz_data(file,icol) result(col)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		CLASS(XYZ_DATA), pointer :: col
		CLASS(VECTOR_DATA), pointer :: vec_data
		INTEGER, INTENT(in), OPTIONAL :: icol
		INTEGER :: i

		i=1
		if(present(icol)) i=icol

		if(.NOT. file%is_xyz_file) &
			STOP "Error: file is not xyz format!"
		vec_data => vector_at(file,i)
		SELECT TYPE(vec_data)
		CLASS is (XYZ_DATA)
			col => vec_data
		CLASS DEFAULT
			STOP "Error: type error in trajectory_files!"
		END SELECT

	end function traj_file_get_xyz_data

	function traj_file_get_line(file) result(line)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER :: n,i,j
		CHARACTER(:), ALLOCATABLE :: line
		CLASS(FILE_COL), pointer :: col
		CHARACTER(50) :: elem

		n=vector_size(file)
		do i=1,n
			col => file%get_column(i)
			if(col%is_array) then
				do j=1,size(col%data_array)
					write(elem,*) col%data_array(j)*col%output_unit
					line=line//"  "//trim(elem)
				enddo
			else
				write(elem,*) col%data_scalar*col%output_unit
				line=line//"  "//trim(elem)
			endif
		enddo

	end function traj_file_get_line

	subroutine traj_file_write_line(file)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file

		if(file%is_open) then

			file%stride_count=file%stride_count+1
			if(file%stride_count >= file%stride) then
				file%stride_count=0
				if(file%is_xyz_file) then
					call traj_file_write_line_xyz(file)
				else
					if(file%formatted) then
						call traj_file_write_line_formatted(file)
					else
						call traj_file_write_line_unformatted(file)
					endif
				endif
			!	FLUSH(file%unit)
			endif

		endif

	end subroutine traj_file_write_line

	subroutine traj_file_write_line_formatted(file)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER :: n,i,j
		CLASS(FILE_COL), pointer :: col
	!	CHARACTER(100) :: ncol_char

		n=vector_size(file)
		do i=1,n-1
			col => file%get_column(i)
			if(col%is_array) then
				do j=1,size(col%data_array)
					write(file%unit,'(E15.7)',ADVANCE="no") col%data_array(j)*col%output_unit
				enddo
				! write(ncol_char,*) size(col%data_array)
				! write(file%unit,'('//trim(ncol_char)//'E15.7)', ADVANCE="no") col%data_array(:)*col%output_unit
			else
				write(file%unit,'(E15.7)',ADVANCE="no")  col%data_scalar*col%output_unit
			endif
		enddo
		col => file%get_column(n)
		if(col%is_array) then
			do j=1,size(col%data_array)-1
				write(file%unit,'(E15.7)',ADVANCE="no") col%data_array(j)*col%output_unit
			enddo
			write(file%unit,'(E15.7)') col%data_array(size(col%data_array))*col%output_unit
			! write(ncol_char,*) size(col%data_array)
			! write(file%unit,'('//trim(ncol_char)//'E15.7)') col%data_array(:)*col%output_unit
		else
			write(file%unit,'(E15.7)')  col%data_scalar*col%output_unit
		endif

	end subroutine traj_file_write_line_formatted

	subroutine traj_file_write_line_unformatted(file)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER :: n,i
		CLASS(FILE_COL), pointer :: col

		n=vector_size(file)
		do i=1,n
			col => file%get_column(i)
			if(col%is_array) then
				write(file%unit) col%data_array(:)*col%output_unit
			else
				write(file%unit)  col%data_scalar*col%output_unit
			endif
		enddo

	end subroutine traj_file_write_line_unformatted

	subroutine traj_file_write_line_xyz(file)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER :: n,i,j
		CLASS(XYZ_DATA), pointer :: col
		CHARACTER(100) :: ncol_char
		REAL(wp), allocatable :: dat(:,:)

		
		col => traj_file_get_xyz_data(file,1)

		n=vector_size(file)
		allocate(dat(col%n_atoms,3*n))
		dat=0
		dat(:,1:3)=col%data_array(:,1:3)*col%output_unit

		DO i=2,n
			col => traj_file_get_xyz_data(file,i)
			dat(:,3*(i-1)+1:3*i)=col%data_array(:,1:3)*col%output_unit
		ENDDO
		write(ncol_char,*) 3*n

		write(file%unit,'(A)') col%n_at_char
		write(file%unit,*)
		DO i=1,col%n_atoms
			write(file%unit,'(A,'//TRIM(ncol_char)//'E15.7)') "    "//col%symbols(i),dat(i,:)
		ENDDO		

	end subroutine traj_file_write_line_xyz

	subroutine  traj_file_print_description(file,unit)
		IMPLICIT NONE
		CLASS(TRAJ_FILE) :: file
		INTEGER, INTENT(in), OPTIONAL :: unit
		INTEGER :: u,i,i_col
		CLASS(FILE_COL), pointer :: col
		CLASS(XYZ_DATA), pointer :: xyz_dat

		if(file%n_total_columns<=0) RETURN

		u=5
		if(present(unit)) u=unit

		WRITE(u,'(A)') file%name
		WRITE(u,'(A)') "{"
		WRITE(u,'(A)') '"'//file%description//'"'
		WRITE(u,'(4X,A)') "MODE: "//file%format//", "//file%access
		WRITE(u,'(4X,A,i4)') "STRIDE: ", file%stride
		
		IF(file%is_xyz_file) THEN
			xyz_dat => traj_file_get_xyz_data(file)
			WRITE(u,'(4X,A)') "XYZ FILE: "//xyz_dat%name//"{"//xyz_dat%unit_str//"}"
		ELSE
			WRITE(u,'(4X,A,i2)') "N_COLUMNS: ",file%n_total_columns
			i_col=1
			DO i=1,vector_size(file)
				col => file%get_column(i)
				if(col%is_array) then
					WRITE(u,"(8X,i2.2,A,i2.2,A)") i_col,"-",i_col+size(col%data_array)-1 &
												," : "//col%name//"{"//col%unit_str//"}"
					i_col=i_col+size(col%data_array)
				else
					WRITE(u,"(8X,i2.2,A)") i_col," : "//col%name//"{"//col%unit_str//"}"
					i_col=i_col+1
				endif
			ENDDO			
		ENDIF
		WRITE(u,'(A)') "}"

	end subroutine traj_file_print_description


END MODULE trajectory_files
