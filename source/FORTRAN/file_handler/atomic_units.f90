module atomic_units
	USE kinds
	USE string_operations
	USE nested_dictionaries
	implicit none
	
	real(wp), parameter :: eV     = 27.211_wp       ! Hartree to eV
	real(wp), parameter :: kcalperMol     = 627.5094736_wp       ! Hartree to kcal/mol
	real(wp), parameter :: bohr   = 0.52917721_wp    ! Bohr to Angstrom
	real(wp), parameter :: Mprot    = 1836.15        ! proton mass
!	real(wp), parameter :: hbar   = 1._wp           ! Planck's constant
	real(wp), parameter :: fs     = 2.4188843e-2_wp ! AU time to femtoseconds
	real(wp), parameter :: kelvin = 3.15774e5_wp    ! Hartree to Kelvin
	real(wp), parameter :: THz = 1000/fs		! AU frequency to THz
	real(wp), parameter :: nNewton = 82.387_wp		! AU Force to nNewton 
	real(wp), parameter :: cm1=219471.52_wp  ! Hartree to cm-1 
	real(wp), parameter :: gmol_Afs=bohr/(Mprot*fs) !AU momentum to (g/mol).A/fs
	real(wp), parameter :: kbar=294210.2648438959 ! Hartree/bohr**3 to kbar

	TYPE(DICT_STRUCT) :: dict_atomic_units

	PRIVATE :: unit_from_string, dict_atomic_units

CONTAINS

	subroutine atomic_units_initialize()
		IMPLICIT NONE

		call dict_atomic_units%init()
		call dict_atomic_units%store("1",1._wp)
		call dict_atomic_units%store("au",1._wp)		
		call dict_atomic_units%store("ev",eV)
		call dict_atomic_units%store("kcalpermol",kcalperMol)
		call dict_atomic_units%store("angstrom",bohr)
		call dict_atomic_units%store("bohr",1._wp/bohr)
		call dict_atomic_units%store("amu",1._wp/Mprot)
		call dict_atomic_units%store("femtoseconds",fs)
		call dict_atomic_units%store("fs",fs)
		call dict_atomic_units%store("picoseconds",fs/1000._wp)
		call dict_atomic_units%store("ps",fs/1000._wp)
		call dict_atomic_units%store("kelvin",kelvin)
		call dict_atomic_units%store("k",kelvin)
		call dict_atomic_units%store("thz",THz)
		call dict_atomic_units%store("tradhz",THz/(2*pi))
		call dict_atomic_units%store("cm-1",cm1)

	end subroutine atomic_units_initialize

	function convert_to_atomic_units(value,unit) RESULT(converted_val)
		IMPLICIT NONE
		REAL(wp), intent(in) :: value
		CHARACTER(*), intent(in) :: unit
		REAL(wp) :: converted_val
		REAL(wp) :: multiplier		

		multiplier=atomic_units_get_multiplier(unit)	
		converted_val=value/multiplier

	end function convert_to_atomic_units

	function convert_to_unit(value,unit) RESULT(converted_val)
		IMPLICIT NONE
		REAL(wp), intent(in) :: value
		CHARACTER(*), intent(in) :: unit
		REAL(wp) :: converted_val
		REAL(wp) :: multiplier		

		multiplier=atomic_units_get_multiplier(unit)	
		converted_val=value*multiplier

	end function convert_to_unit

	function atomic_units_get_multiplier(unit_string_in) result(multiplier)
		IMPLICIT NONE
		CHARACTER(*), intent(in) :: unit_string_in
		REAL(wp) :: multiplier

		REAL(wp) :: tmp_unit, tmp_power, tmp_power_read
		CHARACTER(:), ALLOCATABLE :: unit_string,unit_substring
		INTEGER :: i,unit_start,unit_stop,power_start,power_stop
		CHARACTER(1) :: current_char
		LOGICAL :: in_power

		unit_string=to_lower_case(trim(unit_string_in))

		multiplier=1._wp
		if(unit_string=="") RETURN

		unit_start=1
		unit_stop=0
		in_power=.FALSE.
		power_start=0
		power_stop=0
		tmp_power=1._wp
		do i=1,len(unit_string)

			current_char=unit_string(i:i)	
			!write(*,*) current_char
			SELECT CASE(current_char)
			CASE("^")

				if(in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif
				in_power=.TRUE.
				unit_stop=i-1
				if( unit_stop-unit_start < 0 ) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

			CASE("{")

				if(.not. in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				if(i+1 >= len(unit_string))	then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				power_start=i+1

			CASE("}")

				if(.not. in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				in_power=.FALSE.
				power_stop=i-1

				if(power_stop-power_start < 0) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				else
					READ(unit_string(power_start:power_stop),*) tmp_power_read
					tmp_power=tmp_power*tmp_power_read
				endif

				unit_substring=unit_string(unit_start:unit_stop)
				tmp_unit=unit_from_string(unit_substring)

				multiplier=multiplier*(tmp_unit**tmp_power)

				power_start=0
				power_stop=0
				unit_start=i+1
				unit_stop=0
			
			CASE('*')
				if(in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				if(unit_start==i) then
					unit_start=i+1
					tmp_power=1._wp
					CYCLE
				endif

				unit_stop=i-1
				if( unit_stop-unit_start < 0 ) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				unit_substring=unit_string(unit_start:unit_stop)
				tmp_unit=unit_from_string(unit_substring)
				multiplier=multiplier*(tmp_unit**tmp_power)

				unit_start=i+1
				unit_stop=0
				tmp_power=1._wp
			
			CASE('/')
				if(in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				if(unit_start==i) then
					unit_start=i+1
					tmp_power=-1._wp
					CYCLE
				endif

				unit_stop=i-1
				if( unit_stop-unit_start < 0 ) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				unit_substring=unit_string(unit_start:unit_stop)
				tmp_unit=unit_from_string(unit_substring)
				multiplier=multiplier*(tmp_unit**tmp_power)

				unit_start=i+1
				unit_stop=0
				tmp_power=-1._wp

			CASE DEFAULT
				if(i+1 > len(unit_string)) then
					if(in_power) then
						write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
						STOP "Execution stopped"
					endif

					unit_stop=i
					if( unit_stop-unit_start < 0 ) then
						write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
						STOP "Execution stopped"
					endif

					unit_substring=unit_string(unit_start:unit_stop)
					tmp_unit=unit_from_string(unit_substring)
					multiplier=multiplier*(tmp_unit**tmp_power)
				endif
			END SELECT
		enddo

	end function atomic_units_get_multiplier

	function unit_from_string(unit_string) RESULT(unit)
		IMPLICIT NONE
		CHARACTER(*), intent(in) :: unit_string
		REAL(wp) :: unit

		! CLASS(DICT_DATA), pointer :: data

		! data => dict_get_key(dict_atomic_units, unit_string)
		! SELECT TYPE(data)
		! CLASS is (REAL_CONTAINER)
		! 	unit=data%value
		! CLASS DEFAULT
		! 	STOP "Error: type error in atomic_units dictionary!"
		! END SELECT
		unit=dict_atomic_units%get(unit_string)
	end function unit_from_string

end module atomic_units

