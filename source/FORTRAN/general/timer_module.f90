MODULE timer_module
  IMPLICIT NONE

  TYPE timer_type
    integer :: start_time(8)=-1
    integer :: end_time(8)=-1
    integer :: last_elapsed_time=-1
  CONTAINS
    PROCEDURE :: start => timer_start
    PROCEDURE :: elapsed_time => timer_elapsed_time
  END TYPE timer_type
  

  PRIVATE
  PUBLIC :: timer_type

CONTAINS

  SUBROUTINE timer_start(self)
    IMPLICIT NONE
    CLASS(timer_type) :: self

    call date_and_time(values=self%start_time)

  END SUBROUTINE timer_start

  FUNCTION timer_elapsed_time(self)
    IMPLICIT NONE
    CLASS(timer_type) :: self
    REAL :: timer_elapsed_time

    call date_and_time(values=self%end_time)

    timer_elapsed_time = (self%end_time(8) - self%start_time(8))/1000.
    timer_elapsed_time = timer_elapsed_time + (self%end_time(7) - self%start_time(7))
    timer_elapsed_time = timer_elapsed_time + (self%end_time(6) - self%start_time(6))*60.
    timer_elapsed_time = timer_elapsed_time + (self%end_time(5) - self%start_time(5))*3600.

    self%last_elapsed_time = timer_elapsed_time

  END FUNCTION timer_elapsed_time

END MODULE timer_module