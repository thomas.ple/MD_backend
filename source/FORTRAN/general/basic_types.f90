MODULE basic_types
!! define interfaces for potential calls
!! must be USEd when using potential or forces
	USE kinds
	USE atomic_units
	IMPLICIT NONE

	INTEGER, PARAMETER :: n_dim_default=1

	abstract interface
		subroutine sub_no_param()
			IMPLICIT NONE
		end subroutine sub_no_param
	end interface

	abstract interface
		function pot_type(X) result(U)
		!! abstract interface for potential function
			import :: wp
			IMPLICIT NONE
			REAL(wp),INTENT(in)  :: X(:,:)
				!! positions of the atoms
			REAL(wp) :: U
		end function pot_type
	end interface
	
	abstract interface
		function dpot_type(X) result(dU)
		!! abstract interface for potential derivative function
			import :: wp
			IMPLICIT NONE
			REAL(wp),INTENT(in)  :: X(:,:)
				!! positions of the atoms
			REAL(wp)	:: dU(size(X,1),size(X,2))
		end function dpot_type
	end interface

	abstract interface
			function hessian_type(X) result(H)
		!! abstract interface for potential hessian
			import :: wp
			implicit none
			real(wp),intent(in)  :: X(:,:)		
				!! positions of the atoms
			real(wp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		end function hessian_type
	end interface

	abstract interface
		subroutine get_pot_info_type(X,Pot,Forces,hessian,vir)
		!! abstract interface for all-in-one potential info (e.g. for ab initio potentials)
			import :: wp			
			IMPLICIT NONE
			real(wp), intent(in)  :: X(:,:)
			real(wp), intent(out) :: Forces(size(X,1),size(X,2))
			real(wp), intent(out) :: Pot
			real(wp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
			real(wp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
		end subroutine get_pot_info_type
	end interface

	TYPE SIMULATION_BOX_TYPE
		INTEGER :: n_dim
		REAL(wp) :: a,b,c
		REAL(wp) :: box_lengths(3)
		REAL(wp), ALLOCATABLE :: hcell(:,:) , hcell_inv(:,:)
		LOGICAL :: use_pbc
	CONTAINS
		PROCEDURE :: apply_pbc => simulation_box_apply_pbc
	END TYPE SIMULATION_BOX_TYPE

	TYPE(SIMULATION_BOX_TYPE), SAVE :: simulation_box

	PROCEDURE(sub_no_param), pointer :: simulation_loop => null()
		!! pointer to the subroutine that launches the simulation
	PROCEDURE(pot_type), POINTER :: Pot => null()
		!! pointer to the potential function
	PROCEDURE(dpot_type), POINTER :: dPot => null()
		!! pointer to the potential derivative function
	PROCEDURE(hessian_type), POINTER :: compute_hessian => null()
		!! pointer to the potential derivative function
	PROCEDURE(get_pot_info_type), POINTER :: get_pot_info => null()
		!! pointer to the all-in-one potential function

CONTAINS

	subroutine simulation_box_apply_pbc(self,X)
		IMPLICIT NONE
		CLASS(SIMULATION_BOX_TYPE) :: self
		REAL(wp), INTENT(inout) :: X(:,:)
		REAL(wp), ALLOCATABLE, SAVE :: q(:,:)
		INTEGER :: i
		
		IF(.NOT. ALLOCATED(q)) &
			ALLOCATE(q(size(X,2),size(X,1)))

		q=matmul(self%hcell_inv,transpose(X))
		q=q-int(q)
		X=transpose(matmul(self%hcell,q))

	end subroutine simulation_box_apply_pbc
END MODULE basic_types
