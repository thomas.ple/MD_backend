MODULE kinds
!! Definition of kinds and constants
  IMPLICIT NONE
  
  !KINDS
  INTEGER, PARAMETER, PUBLIC :: sp = 4
    !! simple precision
  INTEGER, PARAMETER, PUBLIC :: dp = 8
    !! double precision
  INTEGER, PARAMETER, PUBLIC :: wp = dp
    !! working precision

	CHARACTER(*), PARAMETER :: null_type="NULL" &
                            ,int_type="INTEGER",real_type="REAL" &
                            ,int_array_type="INTEGER_ARRAY",real_array_type="REAL_ARRAY" &
                            ,logical_type="LOGICAL",string_type="STRING" &
                            ,logical_array_type="LOGICAL_ARRAY"
      !! define data_type key strings

  !CONSTANTS
  REAL(wp), PARAMETER :: pi=acos(-1._wp)
    !! Pi constant
  REAL(wp), PARAMETER :: hbar = 1._wp 
    !! Planck constant

END MODULE kinds