PROGRAM MDbackend
	use kinds
	use nested_dictionaries
	use random
	use input_handler
	use basic_types
	use engine_dispatcher
	use potentials
	use file_handler
	use matrix_operations, only: ut3x3_mat_inverse
	use trajectory_files, only: DEFAULT_TIME_COL &
								,DEFAULT_FORMATTED &
								,DEFAULT_STRIDE
	use atomic_units
	use string_operations
	use periodic_table
	use timer_module
	!$ USE OMP_LIB
	IMPLICIT NONE

	TYPE(DICT_STRUCT) :: param_library	

	REAL(wp) :: R, timeStart, timeFinish
	CHARACTER(2) :: continue_arg
	INTEGER :: i, nargs,u
	CHARACTER(:), allocatable :: input_file
	CHARACTER(:), allocatable :: n_proc
	CHARACTER(:), allocatable :: current_arg

	LOGICAL :: n_proc_specified=.FALSE.
	LOGICAL :: continue_simulation=.FALSE.
	LOGICAL :: print_parameters=.FALSE.

	CHARACTER(:), allocatable :: command_line
	CHARACTER (LEN = 256), allocatable :: fields(:)
    CHARACTER (LEN = 32), allocatable  :: field_types(:)
	INTEGER :: nfields
  LOGICAL :: atomic_simulation
	!$ INTEGER :: n_threads
	TYPE(timer_type) :: timer

!-------- RANDOM SEED --------
	call set_random_seed()
	write(*,*)
	write(*,*) "****** TEST RANDOM SEED ******"
	do i=1,10
		call random_number(R)
		write(*,*) R
	enddo
	write(*,*) "******************************"
	write(*,*)
	
!-------- INITIALIZATION --------

	call param_library%init()

	call atomic_units_initialize()

	nargs=command_argument_count()
	if(nargs <= 0) STOP "Error: at least the input file must be &
		&provided as command argument."

	call get_command(length=i)
	allocate(CHARACTER(len=i) :: command_line)
	call get_command(command_line)

	allocate(fields(nargs+1),field_types(nargs+1))
	CALL split_line( command_line, nfields, fields, field_types )

	if(trim(field_types(2)) /= STRING_TYPE) &
		STOP "Error: first argument should be the input file!"
	input_file=trim(fields(2))

	!PARSE COMMAND ARGUMENTS
	DO i=3,nfields
		current_arg=trim(fields(i))
		
		SELECT CASE(trim(field_types(i)))
		CASE(STRING_TYPE)
			SELECT CASE(current_arg)
			CASE("-c","-C")
				continue_simulation=.TRUE.
				write(*,*) "continue simulation"
				CYCLE
			CASE("--print-parameters","-p")
				print_parameters=.TRUE.
				CYCLE
			END SELECT
		CASE(INT_TYPE)
			if(.not. n_proc_specified) then
				n_proc_specified=.TRUE.
				n_proc=current_arg
				write(*,*) "proc "//n_proc
				CYCLE
			endif
		END SELECT

		write(0,*) "Warning: unrecognized argument: "//current_arg
	ENDDO

	write(*,*)
	write(*,*) "### Initialization ###"

	! PARSE INPUT FILE AND FILL LIBRARY
	call parse_input_file(input_file,param_library)
	!STORE continue_simulation
	call param_library%store("PARAMETERS/continue_simulation",continue_simulation)

	!GET OPENMP NUMBER OF THREADS
	!$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
	!$ write(*,*) "OpenMP max number of threads=",n_threads
	!$ call omp_set_num_threads(n_threads)

	!CHECK IF XYZ FILE IS GIVEN AS INPUT AND EXTRACT DATA
	atomic_simulation = param_library%get("PARAMETERS/atomic_simulation",default=.TRUE.)
  if(atomic_simulation) then
		call check_xyz_input(param_library)
		
		call initialize_simulation_box(param_library)
	else
		write(*,*) "Warning: key PARAMETERS/n_atoms not found, skipping xyz and cell initialization."
	endif

	!INITIALIZE OUTPUT
	if(param_library%has_key("OUTPUT")) then
		if(n_proc_specified) then
			call initialize_general_output(param_library,input_file,n_proc)
		else
			call initialize_general_output(param_library,input_file)
		endif
	else
		write(*,*) "Warning: OUTPUT category not found, skipping output initialization."
	endif

	!INITIALIZE POTENTIAL AND ENGINE
	if(param_library%has_key("POTENTIAL")) then
		call initialize_potential(param_library)
	else
		write(*,*) "Warning: POTENTIAL category not found, skipping potential initialization."
	endif
	call initialize_engine(param_library)

	if(.not. associated(simulation_loop)) &
		STOP "Error: simulation loop not initialized!"
	

	if(print_parameters) call param_library%print()		

	if(param_library%has_key("OUTPUT")) then
		call output%open_files(append=continue_simulation)
		call output%write_description_file(clear=.TRUE.)

		OPEN(newunit=u,file=output%working_directory//"/input_data.in")
			CALL param_library%print(unit=u)
		CLOSE(u)
	endif

	!SIMULATION START (simulation_loop is defined by the engine)
	write(*,*)
	write(*,*) "### starting simulation ###"
	call timer%start()
	call cpu_time(timeStart)

	call simulation_loop()

	call cpu_time(timeFinish)
	write(*,*)
	write(*,*) "Job done in ", timer%elapsed_time(),"s"
	write(*,*) "CPU time ", timeFinish-timeStart,"s"

	!FINALIZATION
	write(*,*)
	write(*,*) "### finalization ###"
	if(param_library%has_key("OUTPUT")) call output%destroy()
	call finalize_engine()
	if(param_library%has_key("POTENTIAL")) call finalize_potential()
	call param_library%destroy()


CONTAINS

	subroutine initialize_general_output(param_library,input_file,n_proc)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		CHARACTER(*), intent(in) :: input_file
		CHARACTER(*), intent(in), optional :: n_proc
		CHARACTER(:), allocatable :: outdir,file_dir, abs_input,engine
		CHARACTER(255) :: cwd
		LOGICAL :: file_exist

		if(.not. param_library%has_key("OUTPUT")) call param_library%add_child("OUTPUT")

		CALL GETCWD(cwd)
		abs_input=trim(input_file)
		if(abs_input(1:1)/="/") &
			abs_input=trim(cwd)//"/"//abs_input
		
		write(*,*) abs_input

		outdir=param_library%get("PARAMETERS/output_dir")
		!! @input_file PARAMETERS/output_dir (main)
		outdir=trim(outdir)
		if(present(n_proc)) then
			file_dir=param_library%get("PARAMETERS/file_dir",default="proc")
			!! @input_file PARAMETERS/file_dir (main, default="proc", if present(n_proc))
			file_dir=trim(file_dir)//"_"//trim(n_proc)
		else
			file_dir=""
		endif

		INQUIRE(FILE=outdir//"/EXIT",EXIST=file_exist)
		IF(file_exist) &
			call system("rm "//outdir//"/EXIT")
		call output%set_working_directory(outdir,file_dir)

		engine=to_lower_case(trim(param_library%get("PARAMETERS/engine")))
		if(engine=="ivr") then
			call system("cp "//abs_input//" "//outdir//"/input_data_ivr.in")
		else
			call system("cp "//abs_input//" "//outdir//"/input_data.in")
		endif

		DEFAULT_FORMATTED=param_library%get("OUTPUT/formatted_by_default",default=DEFAULT_FORMATTED)
		!! @input_file OUTPUT/formatted_by_default (main, default=DEFAULT_FORMATTED)
		DEFAULT_TIME_COL=param_library%get("OUTPUT/time_column_by_default",default=DEFAULT_TIME_COL)
		!! @input_file OUTPUT/time_column_by_default (main, default=DEFAULT_TIME_COL)
		DEFAULT_STRIDE=param_library%get("OUTPUT/stride_by_default",default=DEFAULT_STRIDE)
		!! @input_file OUTPUT/stride_by_default (main, default=DEFAULT_STRIDE)

	end subroutine initialize_general_output


	subroutine check_xyz_input(param_library)
	!! CHECK IF XYZ FILE IS GIVEN AS INPUT AND EXTRACT DATA
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		CHARACTER(:), allocatable :: xyz_file,xyz_unit
		INTEGER :: n_atoms
		REAL(wp), allocatable :: X(:,:), mass(:)
		REAL(wp) :: unit
		LOGICAL :: num_is_indicated,tinker_xyz, blank_line
		INTEGER :: i,j,n_elements,length,spos,n_atoms_of
		CHARACTER(2), ALLOCATABLE :: element_symbols(:),element_set(:)
		INTEGER, ALLOCATABLE :: indices(:),element_indices(:)
		TYPE(DICT_STRUCT), POINTER :: parameters_cat
		CHARACTER(:), ALLOCATABLE :: elements

		call init_periodic_table()

		parameters_cat => param_library%get_child("PARAMETERS")

		xyz_file=parameters_cat%get("xyz_file", default="null")
		if(xyz_file/="null") then

			xyz_unit=parameters_cat%get("xyz_unit", default="au")
			unit=atomic_units_get_multiplier(xyz_unit)	

			blank_line=.TRUE.
			num_is_indicated=parameters_cat%get("xyz_num_indicated", default=.FALSE.)	
			tinker_xyz=parameters_cat%get("tinker_xyz", default=.FALSE.)	
			if(tinker_xyz) then
				num_is_indicated=.TRUE. ; blank_line=.FALSE.
			endif

			call get_info_from_xyz_file(xyz_file,n_atoms,X,mass,num_is_indicated,blank_line,indices=indices)
			call parameters_cat%store("mass", mass*Mprot)
			call parameters_cat%store("xinit", RESHAPE(X, (/ n_atoms*3 /))/unit )
			call parameters_cat%store("n_atoms",n_atoms)
			call parameters_cat%store("n_dim",3)
			call parameters_cat%store("element_index",indices)	
		endif

		n_atoms=parameters_cat%get("n_atoms")

		call param_library%add_child("ELEMENTS")
		if(allocated(indices)) deallocate(indices)
		allocate(element_symbols(n_atoms), indices(n_atoms))
		element_symbols="Xx"
		if(parameters_cat%has_key("element_index")) then
			indices=parameters_cat%get("element_index")
			if(size(indices) /= n_atoms) &
				STOP "Error: specified 'element_index' size does not match 'n_atoms'"
			elements=""
			n_elements=0
			DO i=1,n_atoms
				element_symbols(i)=get_atomic_symbol(indices(i))
				if(index(elements,trim(element_symbols(i)))==0) then
					n_elements=n_elements+1
					elements=elements//"_"//trim(element_symbols(i))
				endif
			ENDDO
			allocate(element_set(n_elements),element_indices(n_atoms))	
			DO i=1,n_elements
				length=len(elements)
				spos=index(elements,"_",BACK=.TRUE.)
				element_set(i)=elements(spos+1:length)
				call param_library%add_child("ELEMENTS/"//element_set(i))
				if(spos>1) elements=elements(1:spos-1)
				n_atoms_of=0
				DO j=1,n_atoms
					if(element_set(i) == element_symbols(j)) then
						n_atoms_of=n_atoms_of+1
						element_indices(n_atoms_of) = j 
					endif
				ENDDO
				call param_library%store("ELEMENTS/"//element_set(i)//"/n",n_atoms_of)
				call param_library%store("ELEMENTS/"//element_set(i)//"/symbol",element_set(i))
				call param_library%store("ELEMENTS/"//element_set(i)//"/indices",element_indices(1:n_atoms_of))
			ENDDO
			write(*,*) "n_elements= "//int_to_str(n_elements)//" -> ",element_set//" "
		else
			n_elements=1
			allocate(element_set(n_elements))
			allocate(element_indices(n_atoms))	
			element_set(1)="Xx"
			DO j=1,n_atoms ; element_indices(j)=j ; ENDDO
			call param_library%add_child("ELEMENTS/"//element_set(1))
			call param_library%store("ELEMENTS/"//element_set(1)//"/n",n_atoms)
			call param_library%store("ELEMENTS/"//element_set(1)//"/symbol",element_set(1))
			call param_library%store("ELEMENTS/"//element_set(1)//"/indices",element_indices(1:n_atoms))
		endif
	
	end subroutine check_xyz_input

	subroutine initialize_simulation_box(param_library)
	!! INITIALIZE PERIODIC BOUNDARIES AND SIMULATION BOX
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: n_dim, i,j,INFO
		REAL(wp), allocatable :: box_abc(:),hcell(:),hcellin(:,:)
		INTEGER, allocatable :: ipiv(:)
		if(.NOT. param_library%has_key("PARAMETERS/n_dim")) return

		n_dim=param_library%get("PARAMETERS/n_dim")
		if(n_dim/=3) then
			write(*,*) "Warning: PARAMETERS/n_dim must be 3 for PBC! Ignoring box input."
			simulation_box%use_pbc=.FALSE.
			return
		endif

		simulation_box%use_pbc=param_library%get("BOUNDARIES/periodic_boundaries", default=.FALSE.)
		!if(.NOT. simulation_box%use_pbc) return
		
		allocate(simulation_box%hcell(n_dim,n_dim))
		allocate(simulation_box%hcell_inv(n_dim,n_dim))
		simulation_box%n_dim=n_dim
		
		if(param_library%has_key("BOUNDARIES/box_abc")) then
			box_abc=param_library%get("BOUNDARIES/box_abc")
			simulation_box%a=box_abc(1)
			if(n_dim>1) simulation_box%b=box_abc(2)
			if(n_dim>2) simulation_box%c=box_abc(3)
		else
			simulation_box%a=param_library%get("BOUNDARIES/box_a",default=1.)
			simulation_box%b=param_library%get("BOUNDARIES/box_b",default=simulation_box%a)
			simulation_box%c=param_library%get("BOUNDARIES/box_c",default=simulation_box%a)
		endif
		
		if(param_library%has_key("BOUNDARIES/hcell")) then
			hcell=param_library%get("BOUNDARIES/hcell")
			if(size(hcell)/=n_dim*n_dim) STOP "Error: BOUNDARIES/hcell does not match n_dim!"
			simulation_box%hcell=RESHAPE(hcell,(/n_dim,n_dim/))
			!DO i=1,n_dim ; DO j=i+1,n_dim
			!	if(simulation_box%hcell(j,i)/=0) STOP "Error: BOUNDARIES/hcell must be triangular superior!"
			!ENDDO; ENDDO
			simulation_box%a=SQRT(SUM(simulation_box%hcell(:,1)**2))
			if(n_dim>1) simulation_box%b=SQRT(SUM(simulation_box%hcell(:,2)**2))
			if(n_dim>2) simulation_box%c=SQRT(SUM(simulation_box%hcell(:,3)**2))
		else
			simulation_box%hcell=0
			simulation_box%hcell(1,1)=simulation_box%a
			simulation_box%hcell(2,2)=simulation_box%b
			simulation_box%hcell(3,3)=simulation_box%c
		endif

		simulation_box%box_lengths(1) = simulation_box%a
		simulation_box%box_lengths(2) = simulation_box%b
		simulation_box%box_lengths(3) = simulation_box%c

		!write(*,*) simulation_box%a*bohr, simulation_box%b*bohr, simulation_box%c*bohr
		write(*,*) "CELL MATRIX   | CELL MATRIX INVERSE"
		!simulation_box%hcell_inv=ut3x3_mat_inverse(simulation_box%hcell)
	  allocate(hcellin(n_dim,n_dim),ipiv(n_dim)) ; hcellin=simulation_box%hcell
    simulation_box%hcell_inv=0._wp
    DO i=1,n_dim
      simulation_box%hcell_inv(i,i)=1._wp
    ENDDO
    CALL DGESV(n_dim,n_dim,hcellin,n_dim,ipiv,simulation_box%hcell_inv,n_dim,INFO)
    do i=1,n_dim
			!simulation_box%hcell_inv(i,i)=1._wp/simulation_box%hcell(i,i)
			write(*,*) simulation_box%hcell(i,:)*bohr, "  |  ", simulation_box%hcell_inv(i,:)/bohr
		enddo
	
	end subroutine initialize_simulation_box

END PROGRAM MDbackend
