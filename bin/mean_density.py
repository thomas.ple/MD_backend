#!/usr/bin/env python3
import util.post_traj as pt
from util.atomic_units import au
import argparse
import glob
import os

parser=argparse.ArgumentParser(description='Compute the mean spectrum of a set of trajectories.')
parser.add_argument('-o', '--outFile',type=str ,default='mean_density.out'
				,help='name of the file containing the density.')
parser.add_argument('-tf', '--trajFile',type=str ,default=None, nargs="*"
				,help='name of the trajectory file.')
parser.add_argument('-wf', '--weightFile',type=str ,default=None
				,help='name of the weight file.')
parser.add_argument('-wr', '--weightRank',type=int ,default=0
				,help='rank in the weight file.')
parser.add_argument('-r', '--rank', nargs="*", type=int ,default=0
				,help='rank in the trajectory file.')
parser.add_argument('-s', '--skip', type=int ,default=0
				,help='rank in the trajectory file.')
parser.add_argument('-nb', '--nbins',type=int ,default=500
				,help='number of bins in the histogram.')
parser.add_argument('-t','--timeCol',action='store_true'
				,help='specify that the first column of the trajectory is time.')
parser.add_argument('-b','--binary',action='store_true'
				,help='specify that the trajectory file is binary.')
parser.add_argument('-u', '--unit',type=str ,default=None, nargs="*"
				,help='unit of the input trajectory.')
parser.add_argument('-xr', '--xrange',type=float , nargs=2, default=None
				,help='min max values of first traj')
parser.add_argument('-yr', '--yrange',type=float , nargs=2, default=None
				,help='min max values of second traj')



def mean_density(args):

	unit=1.
	unit2=unit
	if args.unit:
		unit=au.get_multiplier(args.unit[0])
		if len(args.trajFile) >= 2:
			unit2=au.get_multiplier(args.unit[1])
		else:
			unit2=unit
	
	#PARAMETERS
	traj_file=None
	traj_file_2=None
	if args.trajFile:
		traj_file=args.trajFile[0]	
		if len(args.trajFile) >= 2:
			traj_file_2=args.trajFile[1]
			print(traj_file_2)

	pattern="proc"
	binary=args.binary
	if binary:
		n_dof=int(input("Enter number of dof:"))
	else:
		n_dof=1
	nbins=args.nbins
	
	rank=0
	rank_2=0
	if args.rank:
		rank=args.rank[0]
		if len(args.rank) >= 2:
			rank_2=args.rank[1]

	if binary:
		print("binary file.")
	outfile=args.outFile
	
	if args.timeCol:
		padd=1
	else:
		padd=0
	

	#GET DIRECTORIES MATCHING filedir
	list_dirs=sorted(glob.glob("./"+pattern+"_*"))
	if len(list_dirs) <= 0:
		print("Error: could not find directories matching the pattern '"+pattern+"_*' !")
		return
	
	if traj_file is None:
		list_files=sorted(glob.glob(list_dirs[0]+"/X*.traj*"))
		if len(list_files) > 0:
			traj_file=os.path.basename(list_files[0])
			print("\n Auto-search found trajectory file '"+traj_file+"'.")
			print("If you want to process another trajectory file, relaunch the program using the flag '-tf FILENAME'.\n")
		else:
			list_files=sorted(glob.glob(list_dirs[0]+"/*.traj*"))
			if len(list_files) > 0:
				traj_file=os.path.basename(list_files[0])
				print("\n Auto-search found trajectory file '"+traj_file+"'.")
				print("If you want to process another trajectory file, relaunch the program using the flag '-tf FILENAME'.\n")
			else:
				print("\n Auto-search could not find a trajectory file. Please specify one using the flag '-tf FILENAME'.\n")
				return
	print("trajectory file: "+traj_file)
	if traj_file_2 is not None:
		print("trajectory file 2: "+traj_file_2)

	#COMPUTE SPECTRUM FOR EACH DIMENSION
	#compute mean spectrum
	files=[]
	files_2=[]
	#for dir in list_dirs:
	#	files.extend(glob.glob(dir+"/"+traj_file))
	#	files.extend(glob.glob(dir+"/"+traj_file_2))
	#print(files[0])
	rank_weights=0
	if args.weightFile is not None:
		rank_weights=args.weightRank
		if binary:
			n_col_weights=int(input("Enter number of columns in weight file:"))
		else:
			n_col_weights=1


	trajs=[]
	trajs_2=[]
	weights=[]
	for dir in list_dirs:
		files=sorted(glob.glob(dir+"/"+traj_file))
		weight_traj=None
		if args.weightFile is not None:
			weight=dir+"/"+args.weightFile
			if os.path.isfile(weight):
				weight_traj=pt.get_traj_from_file(weight,ncol=n_col_weights,binary=binary,rank=rank_weights,skip=args.skip)
		for filename in files:
			#filename=dir+"/"+traj_file
			if os.path.isfile(filename): 
				string = "\t"+filename
				if weight_traj is not None:
					string+="   < "+args.weightFile
				print(string)
				traj=pt.get_traj_from_file(filename,ncol=n_dof+padd,binary=binary,rank=rank+padd,unit=unit,skip=args.skip)
				trajs.append(traj)
				weights.append(weight_traj)
				
		if traj_file_2 is not None:
			files_2=sorted(glob.glob(dir+"/"+traj_file_2))
			for filename in files_2:
				#filename=dir+"/"+traj_file
				if os.path.isfile(filename): 
					string = "\t"+filename
					if weight_traj is not None:
						string+="   < "+args.weightFile
					print(string)
					traj=pt.get_traj_from_file(filename,ncol=n_dof+padd,binary=binary,rank=rank_2+padd,unit=unit2,skip=args.skip)
					trajs_2.append(traj)
					
	if len(weights)==0:
		weights=None
	
	if len(trajs) > 0:
		if len(trajs_2) > 0:
			density=pt.mean_density_from_trajs(trajs,trajs2=trajs_2,nbins=nbins,weights=weights,xrange=args.xrange,yrange=args.yrange)
		else:
			density=pt.mean_density_from_trajs(trajs,nbins=nbins,weights=weights,xrange=args.xrange)
	else:
		print("Error: no file corresponding to '"+pattern+"_*/"+traj_file+"' found!")
		return

	#WRITE SPECTRA
	density.write_for_gnuplot(outfile)
	print("Density written to '"+outfile+"'.")


if __name__ == '__main__':
	args=parser.parse_args()
	mean_density(args)
