import numpy as np
import post_traj as pt
import numba

def clean_line(line):
  lineclean=line.strip()
  #skip comments
  if lineclean.startswith("#"):
	  return None
  #split line
  parsed_line=lineclean.split()
  #remove empty strings
  parsed_line=[x for x in parsed_line if x]
  if parsed_line:
    return parsed_line
  else:
    return None

def get_xyz_info(filename):
  atoms=[]
  with open(filename,"r") as f:
    n_atoms=int(f.readline())
    for line in f:
      parsed_line=clean_line(line)
      if parsed_line is None:
        continue
      #print(parsed_line[0])
      if len(parsed_line)==1:
        if int(parsed_line[0]) != n_atoms:
          print("Error: number of atoms cannot change!")
          raise ValueError
        else:
          break	
      atoms.append(parsed_line[0])

  if len(atoms) != n_atoms:
    print("Error: number of atoms does not match data!")
    print(len(atoms),n_atoms)
    raise ValueError
	
  return atoms


def read_xyz_step(pf,n_atoms,positions):
  read_nat=False
  nat_read=0
  if positions.shape != (n_atoms,3):
    print("Error: incorrect position array shape!")
    print(positions.shape,(n_atoms,3))
    raise ValueError
  while True:
    line=pf.readline()
    if not line:
      return True
    parsed_line=clean_line(line)
    if parsed_line is None:
			  continue
    if not read_nat:
      #read number of atoms
      nat_new=int(parsed_line[0])
      if nat_new != n_atoms:
        print("Error: number of atoms cannot change!")
        print(nat_new,n_atoms)
        raise ValueError
      read_nat=True
      continue
		
    for i in range(3):
      positions[nat_read,i]=float(parsed_line[i+1])
    nat_read+=1
    if(nat_read>=n_atoms):
      break
  return False	

def get_n_xyz_steps(pf,N,n_atoms):
  positions=np.zeros((n_atoms,3,N))
  eof=False
  for i in range(N):
    eof=read_xyz_step(pf,n_atoms,positions[:,:,i])
    if eof:
      return None
  return positions

# def mean_spectrum(positions,dt):
#   N=positions.shape[2]
#   n_atoms=positions.shape[0]
#   n_dof=3*n_atoms
#   ft=np.fft.ifft(positions,2*N,axis=2,norm="ortho")
#   CC_mean=np.mean(np.real(ft*np.conj(ft)),axis=(0,1))
#   CC_mean*=2*dt
#   return pt.Spectrum(CC_mean,dt,np.pi/(N*dt))

def mean_spectrum(positions,dt,computeArray,*args,**kwargs):
  n_atoms=positions.shape[0]
  counter=0
  N=positions.shape[2]
  # spectrum_mean=pt.spectrum_from_traj(positions[0,0,:],dt=dt)
  # spectrum_mean.values[:]=0
  CC_mean=np.zeros((3,2*N))
  for i in range(n_atoms):
    if computeArray[i]:
      counter+=1
      for j in range(3):
        ft=np.fft.ifft(positions[i,j,:],2*N,norm="ortho")
        CC_mean[j,:]+=np.real(ft*np.conj(ft))
        # spectrum=pt.spectrum_from_traj(positions[i,j,:],dt=dt)
        # spectrum_mean.values+=spectrum.values/n_dof 
  # return spectrum_mean
  spectra=[]
  for j in range(3):
    spectra.append(pt.Spectrum(CC_mean[j,:]*2*dt/counter,dt,np.pi/(N*dt)))
  return spectra

def mean_spectrum_dipole_moment(positions,dt,charges,*args,**kwargs):
  N=positions.shape[2]
  CC=np.zeros((3,2*N))
  spectra=[]
  for i in range(3):
    moment=np.sum(positions[:,i,:]*charges[:,np.newaxis],axis=0)  
    ft=np.fft.ifft(moment,2*N,norm="ortho")
    CC=np.real(ft*np.conj(ft))/3.
    spectra.append(pt.Spectrum(CC*2*dt,dt,np.pi/(N*dt))) 
  return spectra

def mean_spectrum_xyz(filename,dt,block_size,charges=None,skip=0,species="*"):
  pf=open(filename,"r")
  atoms=get_xyz_info(filename)
  n_atoms=len(atoms)
  atoms_set=set(atoms)
  dipole_moment=False
  computeArray=[True for i in range(n_atoms)]
  print(f"n_atoms={n_atoms}, species={atoms_set}")
  if charges is not None:
    if charges.shape[0] != n_atoms:
      print("Error: number of atoms does not match charges!")
      raise ValueError
    print("Dipole moment spectrum.")
    dipole_moment=True
    compute_spectrum=mean_spectrum_dipole_moment
  else:
    if species!="*":
      computeArray=[atoms[i]==species for i in range(n_atoms)]
      #print(computeArray)
    compute_spectrum=mean_spectrum
  
  if skip > 0:
    skippos=np.zeros((n_atoms,3))
    print(f"Skipping {skip} frames.")
    for i in range(skip):
      read_xyz_step(pf,n_atoms,skippos)

  positions=get_n_xyz_steps(pf,block_size,n_atoms)
  if positions is None:
    print("Error: block_size > total n_steps")
    return None
  c=1
  print(f"block {c}")
  spectra_mean=compute_spectrum(positions,dt,computeArray=computeArray,charges=charges)
  # if dipole_moment:
  #   spectrum_mean=mean_spectrum_dipole_moment(positions,charges,dt)
  # else:
  #   spectrum_mean=mean_spectrum(positions,dt)
  
  while True:
    positions=get_n_xyz_steps(pf,block_size,n_atoms)
    if positions is None:
      break
    c+=1
    print(f"block {c}")
    spectra=compute_spectrum(positions,dt,computeArray=computeArray,charges=charges)
    for j in range(3):
      spectra_mean[j].values+=(spectra[j].values-spectra_mean[j].values)/c
  print(f"{c} blocks read.")
  return spectra_mean
    
  

