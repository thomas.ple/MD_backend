import numpy as np
import xml.etree.ElementTree as ET
import sys
import argparse
import glob

posthr=1.e-15
kbar=294210.2648438959

"""
parser=argparse.ArgumentParser(description="Analyze a Quantum Espresso .xml file")
parser.add_argument("filename",type=str, help="path to the xml file to analyze.")
args = parser.parse_args()

filename=args.filename
if filename[-4:].lower() != ".xml":
  print("Error: file name must end with '.xml'")
  raise ValueError
"""

def parse_qexml(filename):
  # parse a QE xml output file and return the root
  if filename[-4:].lower() != ".xml":
    print("Error: file name must end with '.xml'")
    raise ValueError
  pw=ET.parse(filename).getroot()
  return pw

def get_output(pw):
   return pw.find('output')

def get_step(pw,istep):
   return pw.findall('step')[istep-1]

def get_n_step(pw):
  return len(pw.findall('step'))

def get_n_atoms(step):
  # extract number of atoms
  return int(step.find("atomic_structure").get("nat"))

def get_positions(step,get_species=False):
  # extract atomic positions (in bohr) and array of species symbols
  nat=int(step.find("atomic_structure").get("nat"))
  posdat=step.find("atomic_structure/atomic_positions")
  positions=np.zeros((nat,3))
  species=np.zeros(nat,'U2')
  for i,atom in enumerate(posdat):
  	species[i]=atom.get("name")
  	positions[i,:]=np.fromstring(atom.text,sep=' ')
  
  if get_species:
    return positions, species
  else:
    return positions

def get_crystal_positions(step,get_species=False):
  # extract atomic positions (in crystal units)  and array of species symbols
  pos,species=get_positions(step,True)
  cell,cellinv=get_cell(step,True)
  nat=len(species)
  cryspos=np.zeros_like(pos)  
  for i in range(nat):
    cryspos[i,:]= cellinv @ pos[i,:]
  if get_species:
    return cryspos, species
  else:
    return cryspos

def get_stress(step):
  # extract stress tensor (in Ha/A^3) and pressure (in kbar)
  stress=np.fromstring(step.find("stress").text,sep=" ").reshape((3,3))
  pressure=np.trace(stress)*kbar/3
  return stress, pressure

def get_etot(step):
  # extract total potential energy (in Ha)
  return float(step.find("total_energy/etot").text)

def get_forces(step):
  # extract forces (in Ha/bohr)
  nat=get_n_atoms(step)
  return np.fromstring(step.find("forces").text,sep=" ").reshape((nat,3))

def get_cell(step,get_inverse=False,get_volume=False):
  # extract cell vectors (in bohr)
  # |ax  bx  cx|
  # |ay  by  cy|
  # |az  bz  cz|
  cell=np.zeros((3,3)) ; celldm=np.zeros(6)
  for i,vec in enumerate(step.find("atomic_structure/cell")):
    cell[:,i]=np.fromstring(vec.text,sep=' ')
  cell[np.abs(cell)<posthr]=0.
  output=(cell,)
  if get_inverse:
    output+=(np.linalg.inv(cell),)
  if get_volume:
    vol=np.dot(cell[:,0],np.cross(cell[:,1],cell[:,2]))
    output+=(vol,)
  if len(output)==1:
    return cell
  else:
    return output

def get_celldm(step,ibrav):
  cell=get_cell(step)
  return get_celldm_from_cell(ibrav,cell)

def get_ecut(step):
  # extract ecutwfc and ecutrho (in Ha)
  return float(step.find("basis_set/ecutwfc").text), float(step.find("basis_set/ecutrho").text)

def extract_step(pw,istep):
  step=pw.findall('step')[istep-1]
  data={}

  #GET NUMBER OF ATOMS
  nat=get_n_atoms(step)
  data["nat"]=nat

  #GET CELL DIMENSION
  data["cell"]=get_cell(step)

  #GET TOTAL ENERGY AND FORCES
  data["etot"]=get_etot(step)
  data["forces"]=get_forces(step)
  data["forcetot"]=np.sum(data["forces"])

  data["stress"],data["pressure"]=get_stress(step)
  data["positions"],data["species"]=get_positions(step,get_species=True)


def extract_output_section(pw):
  output=pw.find("output")
  #output=pw.findall("step")[-1]
  data={}
  
  data["cputime"]=float(pw.find("cputime").text)

  #GET NUMBER OF ATOMS
  nat=get_n_atoms(output)
  data["nat"]=nat
  
  data["ecutwfc"],data["ecutrho"]=get_ecut(output)

  #GET CELL DIMENSIONS
  data["cell"]=get_cell(output)

  #GET TOTAL ENERGY AND FORCES
  data["etot"]=get_etot(output)
  data["forces"]=get_forces(output)
  data["forcetot"]=np.sum(data["forces"])

  data["stress"],data["pressure"]=get_stress(output)
  
  data["positions"],data["species"]=get_positions(output,get_species=True)  
  return data

def write_xyz_frame(pf,species,positions,comment=''):
  nat=len(species)
  pf.write(f'{nat}\n')
  pf.write(f'{comment}\n')
  for i in range(nat):
    pf.write(f'{species[i]}\t{positions[i,0]:.8f}\t{positions[i,1]:.8f}\t{positions[i,2]:.8f}\n')
    
def write_pw_atomic_pos_crystal(pf,species,crysPos,moveSpec=None):
  nat=len(species)
  pf.write('ATOMIC_POSITIONS (crystal)\n')
  if moveSpec is None:
    for i in range(nat):
      pf.write(f'{species[i]:<2}    {crysPos[i,0]:.9f}  {crysPos[i,1]:.9f}  {crysPos[i,2]:.9f}\n')
  elif isinstance(moveSpec,dict):
    for i in range(nat):
      pf.write(f'{species[i]:<2}    {crysPos[i,0]:.9f}  {crysPos[i,1]:.9f}  {crysPos[i,2]:.9f}  {moveSpec[species[i]]}\n')
  else:
    for i in range(nat):
      pf.write(f'{species[i]:<2}    {crysPos[i,0]:.9f}  {crysPos[i,1]:.9f}  {crysPos[i,2]:.9f}  {moveSpec[i]}\n')

# see description at https://www.quantum-espresso.org/Doc/INPUT_PW.html#idm199
def get_celldm_from_cell(ibrav,cell):
  celldm=np.zeros(6)
  if ibrav==0:
    celldm[0]=np.linalg.norm(cell[:,0])
    celldm[1]=np.linalg.norm(cell[:,1])
    celldm[2]=np.linalg.norm(cell[:,2])
    celldm[3]=np.dot(cell[:,1],cell[:,2])/(celldm[1]*celldm[2])
    celldm[4]=np.dot(cell[:,0],cell[:,2])/(celldm[0]*celldm[2])
    celldm[5]=np.dot(cell[:,0],cell[:,1])/(celldm[0]*celldm[1])
  elif ibrav==1:
    # cubic P (sc)
    celldm[0]=cell[0,0]
  elif ibrav==2:
    # cubic F (fcc)
    celldm[0]=np.sqrt(2.)*np.linalg.norm(cell[:,0])
  elif ibrav==3 or ibrav==-3:
    # cubic I (bcc)
    celldm[0]=2.*np.linalg.norm(cell[:,0])/np.sqrt(3.)
  elif ibrav==4 or ibrav==6:
    # Hexagonal/Trigonal P or Tetragonal P (st)
    celldm[0]=cell[0,0]
    celldm[2]=cell[2,2]/cell[0,0]
  elif ibrav==5 or ibrav==-5:
    # Trigonal R (3fold axis c or 3fold axis <111>)
    celldm[0]=np.linalg.norm(cell[:,0])
    c=np.linalg.norm(cell[:,2])
    celldm[3]=np.dot(cell[:,0],cell[:,2])/(celldm[0]*c)
  elif ibrav==7:
    # Tetragonal I (bct)
    celldm[0]=2.*cell[0,0]
    celldm[2]=cell[2,2]/cell[0,0]
  elif ibrav==8:
    # Orthorhombic P
    celldm[0]=cell[0,0]
    celldm[1]=cell[1,1]/cell[0,0]
    celldm[2]=cell[2,2]/cell[0,0]
  elif ibrav==9 or ibrav==-9:
    # Orthorhombic base-centered(bco)
    celldm[0]=2.*cell[0,0]
    celldm[1]=cell[1,1]/cell[0,0]
    celldm[2]=cell[2,2]/celldm[0]
  elif ibrav==91:
    # Orthorhombic one-face base-centered A-type
    celldm[0]=cell[0,0]
    celldm[1]=2.*cell[1,1]/celldm[0]
    celldm[2]=2.*cell[2,2]/celldm[0]
  elif ibrav==10 or ibrav==11:
    # Orthorhombic face-centered or body-centered
    celldm[0]=2.*cell[0,0]
    celldm[1]=cell[1,1]/cell[0,0]
    celldm[2]=cell[2,2]/cell[0,0]
  elif ibrav==12:
    # Monoclinic P, unique axis c
    celldm[0]=cell[0,0]
    celldm[2]=cell[2,2]/cell[0,0]
    b=np.linalg.norm(cell[:,1])
    celldm[1]=b/celldm[0]
    celldm[3]=cell[0,1]/b
  elif ibrav==-12:
    # Monoclinic P, unique axis b
    celldm[0]=cell[0,0]
    celldm[1]=cell[1,1]/cell[0,0]
    c=np.linalg.norm(cell[:,2])
    celldm[2]=c/celldm[0]
    celldm[4]=cell[0,2]/c
  elif ibrav==13:
    # Monoclinic base-centered (unique axis c)
    celldm[0]=2.*cell[0,0]
    celldm[2]=cell[2,2]/cell[0,0]
    b=np.linalg.norm(cell[:,1])
    celldm[1]=b/celldm[0]
    celldm[3]=cell[0,1]/b
  elif ibrav==-13:
    # Monoclinic base-centered (unique axis b)
    celldm[0]=2.*cell[0,0]
    celldm[1]=cell[1,1]/cell[0,0]
    c=np.linalg.norm(cell[:,2])
    celldm[2]=c/celldm[0]
    celldm[4]=cell[0,2]/c
  elif ibrav==14:
    # Triclinic
    celldm[0]=cell[0,0]
    b=np.linalg.norm(cell[:,1])
    celldm[1]=b/celldm[0]
    celldm[5]=cell[0,1]/b
    c=np.linalg.norm(cell[:,2])
    celldm[2]=c/celldm[0]
    celldm[4]=cell[0,2]/c
    celldm[3]=np.dot(cell[:,1],cell[:,2])/(b*c)
  else:
    print(f"Error: unknown ibrav={ibrav}")
  
  return celldm
	
