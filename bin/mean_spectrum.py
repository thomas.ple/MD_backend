#!/usr/bin/env python3
import util.post_traj as pt
from util.input_parser import parse_input
from util.atomic_units import au
import argparse
import glob
import os

parser=argparse.ArgumentParser(description='Compute the mean spectrum of a set of trajectories.')
parser.add_argument('-i','--inputFile',type=str, default=None
				,help='the path to the input file.')
parser.add_argument('-o', '--outFile',type=str ,default='spectrum_mean.out'
				,help='name of the file containing the spectrum.')
parser.add_argument('-tf', '--trajFile',type=str ,default=None
				,help='name of the trajectory file.')
parser.add_argument('-dt',type=float ,default=None
				,help='time step of the series (in fs).')
parser.add_argument('-k','--kubo',action='store_true'
				,help='specify that the spectrum must be inverse kubo transformed.')
parser.add_argument('--piqtbc',action='store_true'
				,help='specify that the spectrum must be inverse piqtb-c transformed.')
parser.add_argument('--piqtbst',action='store_true'
				,help='specify that the spectrum must be inverse piqtb-st transformed.')
parser.add_argument('-t','--timeCol',action='store_true'
				,help='specify that the first column of the trajectory is time.')
parser.add_argument('-b','--binary',action='store_true'
				,help='specify that the trajectory file is binary.')
parser.add_argument('--unit', type=float, default=au.cm1
				,help='unit of the output.')
parser.add_argument('--gsmooth', type=float, default=-1.
				,help='spectrum smoothing coefficient (in units of the output, by default=cm-1)')
parser.add_argument('--cutoff', type=float, default=-1.
				,help='frequency cutoff (in units of the output, by default=cm-1)')

parser.add_argument('--tcf',action='store_true'
				,help='also save the time correlation function.')
parser.add_argument('--unittcf', type=float, default=au.ps
				,help='unit of the output.')
parser.add_argument('--cutofftcf', type=float, default=-1.
				,help='frequency cutoff (in units of the output, by default=cm-1)')

parser.add_argument('--imag',action='store_true'
				,help='save the imaginary parts.')

def get_input_data(args):
	default_input="input_data.in"
	param = lambda:None

	## find input file
	if args.inputFile is None:
		if os.path.isfile(default_input):
			input_file=default_input
			print("\n Auto-search found input file '"+input_file+"'.")
		else:
			list_input=glob.glob("*.in")
			if len(list_input) > 0:
				input_file=list_input[0]
			else:
				print("\n Auto-search could not find an input file.")
				input_file=None
	elif not os.path.isfile(args.inputFile):
		print("\n Input file '"+args.inputFile+"' does not exist!")
		input_file = None
	else:
		input_file=args.inputFile
	
	param.piqtb_c=False
	param.piqtb_st=False
	if input_file is not None: ## input file found: get values
		print("input file: "+input_file)
		input_data=parse_input(input_file)
		#print(input_data)
		
		param.engine=input_data.get("parameters/engine",default="classical_md")
		if param.engine.lower() in ["rpmd","cmd","centroid_md"]:
			param.kubo=True
			if param.engine.lower() == "rpmd":
				is_piqtb = input_data.get("thermostat/thermostat_type",default="white")
				if is_piqtb=="qtb":
					param.kubo=True
					is_centroid_only = input_data.get("thermostat/piqtb_centroid_only",default=False)
					is_piqtb_st = input_data.get("thermostat/piqtb_beads_same_temperature",default=False)
					if is_centroid_only:
						param.piqtb_c = True
					elif is_piqtb_st:
						param.piqtb_st = True
		else:
			param.kubo=args.kubo
			param.piqtb_c=args.piqtbc
			param.piqtb_st=args.piqtbst
		param.binary=not input_data.get("output/formatted_by_default",default=True)
		param.pattern=input_data.get("parameters/file_dir")
		param.n_atoms=input_data.get("parameters/n_atoms")		
		param.dt=input_data.get("parameters/dt")	
		if param.kubo or param.piqtb_c or param.piqtb_st:
			param.temperature=input_data.get("parameters/temperature")*au.kelvin
		
		if param.piqtb_c or param.piqtb_st:
			param.nu=int(input_data.get("parameters/n_beads",default=1))
			
	else: ## input file not found: ask for values
		param.binary=True
		param.piqtb_c = args.piqtbc
		param.piqtb_st = args.piqtbst
		param.kubo=args.kubo
		param.pattern=input("Enter directory pattern (ex: proc): ")
		param.n_atoms=int(input("Enter number of degrees of freedom: "))
		if args.dt is None:
			param.dt=float(input("Enter time step dt (in fs): "))/au.fs
		if param.kubo or param.piqtb_c:
			param.temperature=float(input("Enter temperature (in K): "))
		
		if param.piqtb_c:
			param.nu=int(input("Enter number of beads: "))

	## overwrite dt if specified in command line
	if args.dt is not None:
		param.dt=args.dt/au.fs
	
	return param

def mean_spectrum(args):

	#GET DATA FROM INPUT FILE	
	param=get_input_data(args)
	print("n_atoms= "+str(param.n_atoms))
	print("dt= "+str(param.dt*au.fs)+" fs")	
	#PARAMETERS
	traj_file=args.trajFile
	binary=args.binary or param.binary
	if binary:
		print("binary file.")
	outfile=args.outFile

	unit=args.unit
	kubo=args.kubo or param.kubo
	if param.piqtb_c:
		print("inverse piqtb-c transform.")
		print("temperature= "+str(param.temperature)+" K")
		print("n_beads= "+str(param.nu))
	elif param.piqtb_st:
		print("inverse piqtb-st transform.")
		print("temperature= "+str(param.temperature)+" K")
		print("n_beads= "+str(param.nu))
	
	if kubo:
		print("inverse kubo transform.")
		print("temperature= "+str(param.temperature)+" K")
	gsmooth=args.gsmooth
	cutoff=args.cutoff
	
	if args.timeCol:
		padd=1
	else:
		padd=0
	

	#GET DIRECTORIES MATCHING filedir
	list_dirs=sorted(glob.glob("./"+param.pattern+"_*"))
	if len(list_dirs) <= 0:
		print("Error: could not find directories matching the pattern '"+param.pattern+"_*' !")
		return
	
	if traj_file is None:
		list_files=glob.glob(list_dirs[0]+"/P*.traj")
		if len(list_files) > 0:
			traj_file=os.path.basename(list_files[0])
			print("\n Auto-search found trajectory file '"+traj_file+"'.")
			print("If you want to process another trajectory file, relaunch the program using the flag '-tf FILENAME'.\n")
		else:
			list_files=glob.glob(list_dirs[0]+"/*.traj")
			if len(list_files) > 0:
				traj_file=os.path.basename(list_files[0])
				print("\n Auto-search found trajectory file '"+traj_file+"'.")
				print("If you want to process another trajectory file, relaunch the program using the flag '-tf FILENAME'.\n")
			else:
				print("\n Auto-search could not find a trajectory file. Please specify one using the flag '-tf FILENAME'.\n")
				return
	print("trajectory file: "+traj_file)


	#COMPUTE SPECTRUM FOR EACH DIMENSION
	#compute mean spectrum
	# trajs=[]
	# for i in range(param.n_atoms):
	# 	trajs.append([])
	
	# for dir in list_dirs:
	# 	filename=dir+"/"+traj_file
	# 	if os.path.isfile(filename): 
	# 		print("\t"+filename)
	# 		traj=pt.get_traj_from_file(filename,ncol=param.n_atoms+padd,binary=binary)
	# 		for i in range(param.n_atoms):
	# 			trajs[i].append(traj[:,i])
	
	# all_same_len=True
	# n_steps=len(trajs[0][0])
	# for traj in trajs[0]:
	# 	n_loc=len(traj)
	# 	if n_loc != n_steps:
	# 		all_same_len=False
	# 		if n_loc < n_steps:
	# 			n_steps = n_loc
	# if not all_same_len:
	# 	n_steps -= n_steps % 100
	# 	print("Warning: trajectories not all the same length...")
	# 	print("cutting at the shortest, n_steps="+str(n_steps))
	# else:
	# 	n_steps=None

	files=[]
	list_n_steps=[]
	for dir in list_dirs:
		filename=dir+"/"+traj_file
		if os.path.isfile(filename): 
			print("\t"+filename)
			files.append(filename)
			list_n_steps.append(pt.get_traj_from_file(filename,ncol=param.n_atoms+padd,binary=binary).shape[0])
	n_steps=min(list_n_steps)

			
	# spectra=[]
	# for rank in range(param.n_atoms):
	# 	print("DIMENSION "+str(rank+1))
	# 	if len(trajs) > 0:
	# 		spec = pt.mean_spectrum_from_trajs(trajs[rank],dt=param.dt,n_steps=n_steps)
	# 		spec.smooth(gsmooth/unit)
	# 		if kubo:
	# 			spec.inverse_kubo(param.temperature)
	# 		spectra.append(spec)
	# 	else:
	# 		print("Error: no file corresponding to '"+param.pattern+"_*/"+traj_file+"' found!")
	# 		return

	spectra=[]
	for rank in range(param.n_atoms):
		print("DIMENSION "+str(rank+1))
		if len(files) > 0:
			spec = pt.mean_spectrum_from_files(files,rank=rank+padd,binary=binary
								,ncol=param.n_atoms+padd,dt=param.dt,n_steps=n_steps)
			spec.smooth(gsmooth/unit)
			if param.piqtb_c:
				spec.inverse_piqtb_c(param.temperature,param.nu)
			elif param.piqtb_st:
				spec.inverse_piqtb_st(param.temperature,param.nu)
			
			if kubo:
				spec.inverse_kubo(param.temperature)
			spectra.append(spec)
		else:
			print("Error: no file corresponding to '"+param.pattern+"_*/"+traj_file+"' found!")
			return

	#WRITE SPECTRA
	pt.write_spectra_to_file(spectra,outfile,unit=unit,cutoff=cutoff,imag=args.imag)
	if args.tcf:
		pt.write_all_tcf(spectra,outfile+".tcf",unit=args.unittcf,cutoff=args.cutofftcf,imag=args.imag)
	print("Spectra written to '"+outfile+"'.")


if __name__ == '__main__':
	args=parser.parse_args()
	mean_spectrum(args)
