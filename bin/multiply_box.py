#!/usr/bin/env python3
import numpy as np
import argparse

parser=argparse.ArgumentParser(description='Write a xyz box multiplied in any direction.')
parser.add_argument('-i', '--inputFile',type=str, required=True
				,help='name of the xyz file to multiply.')
parser.add_argument('-o', '--outputFile',type=str, required=True
				,help='name of the output file.')
parser.add_argument('-b', '--boxFile',type=str, required=True
				,help='name of the file containing the cell matrix (format: ax ay az bx by bz cx cy cz)')
parser.add_argument('-n',type=int,nargs=3, default=(1,1,1)
				,help='number of multiplication in directions x y z')
parser.add_argument('-ci','--crystalIn',action='store_true'
				,help='specify that the input is in crystal unit.')
parser.add_argument('-co','--crystalOut',action='store_true'
				,help='specify that the output should be in crystal unit.')
parser.add_argument('-wh','--writehcell',action='store_true'
				,help='specify that the output should be in crystal unit.')



def write_xyz(pf,species,pos,offset=[0.,0.,0.]):
  for i in range(len(species)):
    pf.write(f"{species[i]}\t{pos[i,0]+offset[0]:.8f}\t{pos[i,1]+offset[1]:.8f}\t{pos[i,2]+offset[2]:.8f}\n")

def offset_xyz(pos,offset):
  tmp=np.copy(pos)
  for i in range(3):
    tmp[:,i]+=offset[i]
  return tmp

def convert_to_crystal(hcellinv,cartesian):
  return np.dot(hcellinv,cartesian.T).T

def convert_to_cartesian(hcell,crystal):
  return convert_to_crystal(hcell,crystal)

def treatline(line): 
  spl=line.split() 
  at=spl[0] 
  x=np.zeros(3) 
  for i in range(3): 
    x[i]=float(spl[i+1]) 
  return at,x

def multiply_box(fileIn,fileOut,fileBox,Nx,Ny,Nz
                  ,crystalIn=False,crystalOut=False,write_hcell=True):
  #get cell matrix and compute its inverse
  hcell=np.reshape(np.loadtxt(fileBox),(3,3),order='F')
  hcellinv=np.linalg.inv(hcell)

  box_crystal=np.zeros((3,3))
  box_crystal[0,0]=Nx
  box_crystal[1,1]=Ny
  box_crystal[2,2]=Nz
  box_new=convert_to_cartesian(hcell,box_crystal).T

  cont=True
  iframe=0
  po=open(fileOut,"w")
  with open(fileIn,"r") as pf:
    while cont:
      #First word of first line must be the number of atoms
      line=pf.readline()
      if not line:
        break
      Nat=int(line.split()[0])      
      iframe+=1
      Nat_new=Nat*Nx*Ny*Nz
      species=[]
      pos=np.zeros((Nat,3))
      print("frame ",iframe,"Nat=",Nat,"Nat_new=",Nat_new)
      #skip second line (comment)
      pf.readline()
      #start reading the positions
      for i in range(Nat):
        line=pf.readline()
        at,x=treatline(line)
        species.append(at)
        pos[i,:]=x     
      
      #offset in crystal basis
      if crystalIn:
        crystal=np.copy(pos)
      else:
        crystal=convert_to_crystal(hcellinv,pos)

      po.write(f"{Nat_new}")
      if crystalOut:
        po.write(" (crystal units)")
      po.write("\n")
      if write_hcell:
        po.write("#hcell: ")
        for j in range(3):
          for i in range(3):
            po.write(f"    {box_new[i,j]:.8f}")
      po.write("\n")
      #pf.write("#hcell: "+str(np.reshape(box_new,(9,),order='F'))[1:-1]+"\n")
      for xi in range(Nx):
        for yi in range(Ny):
          for zi in range(Nz):
            crystal_new=offset_xyz(crystal,[xi,yi,zi])
            if crystalOut:
              write_xyz(po,species,crystal_new)
            else:
              write_xyz(po,species,convert_to_cartesian(hcell,crystal_new))
    po.close()       
    crystal_string=""    
    if crystalOut:
      crystal_string=" (crystal units)"
    print(f"{Nx}x{Ny}x{Nz} box written to '"+fileOut+"'"+crystal_string)

if __name__ == '__main__':
  args=parser.parse_args()
  multiply_box(args.inputFile,args.outputFile,args.boxFile,*args.n
                ,args.crystalIn,args.crystalOut,args.writehcell)

#with open(filename,"r") as f:
#  lines=f.readlines()

#Nat=int(lines[0].split()[0])
#print(Nat)
#species=[]
#pos=[]
#for line in lines[1:]:
#  at,x=treatline(line)
#  species.append(at)
#  pos.append(x)
#print(species)
#Nat_new=Nat*Nx*Ny*Nz
#print("Nat_new=",Nat_new)


#with open(fileout,"w") as pf:
#    pf.write(f"{Nat_new}\n")
#    for xi in range(Nx):
#      for yi in range(Ny):
#        for zi in range(Nz):
#          offset=[xi*abox,yi*bbox,zi*cbox]
#          write_xyz(pf,species,pos,offset)

