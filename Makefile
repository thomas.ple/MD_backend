#!/usr/bin/make

#main building variables
DSRC    = source/FORTRAN
DOBJ    = bin/.OBJ/
DMOD    = bin/.MOD/
DEXE    = bin/
LIBS    =  -lsockets  -L./source/sockets/ 
FC      = ifort
OPTSC   = -c -O3 -module bin/.MOD
OPTSL   = -mkl -module bin/.MOD
VPATH   = $(DSRC) $(DOBJ) $(DMOD)
MKDIRS  = $(DOBJ) $(DMOD) $(DEXE)
LCEXES  = $(shell echo $(EXES) | tr '[:upper:]' '[:lower:]')
EXESPO  = $(addsuffix .o,$(LCEXES))
EXESOBJ = $(addprefix $(DOBJ),$(EXESPO))

#auxiliary variables
COTEXT  = "Compiling $(<F)"
LITEXT  = "Assembling $@"

#building rules
$(DEXE)md_backend.x: $(MKDIRS) $(DOBJ)main.o \
	$(DOBJ)fastfourtrans.o
	@rm -f $(filter-out $(DOBJ)main.o,$(EXESOBJ))
	@echo $(LITEXT)
	@$(FC) $(DOBJ)*.o -o $@ $(LIBS) $(OPTSL) 
EXES := $(EXES) md_backend.x

#compiling rules
$(DOBJ)main.o: source/FORTRAN/main.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)random.o \
	$(DOBJ)input_handler.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)engine_dispatcher.o \
	$(DOBJ)potentials.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)trajectory_files.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)string_operations.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)basic_types.o: source/FORTRAN/general/basic_types.f90 \
	$(DOBJ)kinds.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)matrix_operations.o: source/FORTRAN/general/matrix_operations.f90 \
	$(DOBJ)kinds.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)kinds.o: source/FORTRAN/general/kinds.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)random.o: source/FORTRAN/general/random.f90 \
	$(DOBJ)kinds.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)string_operations.o: source/FORTRAN/general/string_operations.f90 \
	$(DOBJ)kinds.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)unbiased_products.o: source/FORTRAN/general/unbiased_products.f90 \
	$(DOBJ)kinds.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)fastfourtrans.o: source/FORTRAN/general/FastFourTrans.f
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)muller_potential.o: source/FORTRAN/potentials/muller_potential.f90 \
	$(DOBJ)kinds.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)potentials.o: source/FORTRAN/potentials/potentials.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)socket_potential.o \
	$(DOBJ)model_potentials.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)model_potentials.o: source/FORTRAN/potentials/model_potentials.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)muller_potential.o \
	$(DOBJ)atomic_units.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)socket_potential.o: source/FORTRAN/potentials/socket_potential.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)matrix_operations.o \
	$(DOBJ)basic_types.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)engine_dispatcher.o: source/FORTRAN/engines/engine_dispatcher.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)classical_mc.o \
	$(DOBJ)ring_polymer_md.o \
	$(DOBJ)wigner_md.o \
	$(DOBJ)wigner_bayes.o \
	$(DOBJ)centroid_md.o \
	$(DOBJ)schrodinger.o \
	$(DOBJ)ivr.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)ring_polymer_md.o: source/FORTRAN/engines/RPMD/ring_polymer_md.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)random.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)rpmd_integrators.o \
	$(DOBJ)thermostats.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)rpmd_integrators.o: source/FORTRAN/engines/RPMD/rpmd_integrators.f90 \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)random.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)thermostats.o \
	$(DOBJ)pi_qtb_thermostat.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)rpmd_types.o: source/FORTRAN/engines/RPMD/rpmd_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)qtb_thermostat.o: source/FORTRAN/engines/thermostats/qtb_thermostat.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)random.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)qtb_types.o \
	$(DOBJ)file_handler.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)thermostats.o: source/FORTRAN/engines/thermostats/thermostats.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)qtb_thermostat.o \
	$(DOBJ)lgv_thermostat.o \
	$(DOBJ)pi_lgv_thermostat.o \
	$(DOBJ)pi_qtb_thermostat.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)qtb_types.o: source/FORTRAN/engines/thermostats/qtb_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)random.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)matrix_operations.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)pi_lgv_thermostat.o: source/FORTRAN/engines/thermostats/pi_lgv_thermostat.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)random.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)pi_qtb_thermostat.o: source/FORTRAN/engines/thermostats/pi_qtb_thermostat.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)random.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)pi_lgv_thermostat.o \
	$(DOBJ)qtb_types.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)lgv_thermostat.o: source/FORTRAN/engines/thermostats/lgv_thermostat.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)random.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)schrodinger.o: source/FORTRAN/engines/schrodinger/schrodinger.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wigner_bayes.o: source/FORTRAN/engines/wigner_bayes/wigner_bayes.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)wb_types.o \
	$(DOBJ)wb_integrators.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)wb_forces.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wb_forces.o: source/FORTRAN/engines/wigner_bayes/wb_forces.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)wb_types.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)matrix_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)unbiased_products.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wb_gaussian_forces.o: source/FORTRAN/engines/wigner_bayes/wb_gaussian_forces.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)wb_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wb_types.o: source/FORTRAN/engines/wigner_bayes/wb_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)file_handler.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wb_integrators.o: source/FORTRAN/engines/wigner_bayes/wb_integrators.f90 \
	$(DOBJ)wb_types.o \
	$(DOBJ)random.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)wb_forces.o \
	$(DOBJ)wb_gaussian_forces.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)cmd_integrators.o: source/FORTRAN/engines/CMD/cmd_integrators.f90 \
	$(DOBJ)cmd_types.o \
	$(DOBJ)random.o \
	$(DOBJ)thermostats.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)cmd_forces.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)cmd_forces.o: source/FORTRAN/engines/CMD/cmd_forces.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)cmd_types.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)matrix_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)file_handler.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)cmd_types.o: source/FORTRAN/engines/CMD/cmd_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)centroid_md.o: source/FORTRAN/engines/CMD/centroid_md.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)cmd_types.o \
	$(DOBJ)cmd_integrators.o \
	$(DOBJ)thermostats.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)cmd_forces.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)ivr.o: source/FORTRAN/engines/IVR/ivr.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)classical_md_types.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)classical_md_integrators.o: source/FORTRAN/engines/classical_md/classical_md_integrators.f90 \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)random.o \
	$(DOBJ)thermostats.o \
	$(DOBJ)basic_types.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)classical_md.o: source/FORTRAN/engines/classical_md/classical_md.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)classical_md_integrators.o \
	$(DOBJ)thermostats.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)classical_md_types.o: source/FORTRAN/engines/classical_md/classical_md_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)basic_types.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wigner_forces.o: source/FORTRAN/engines/wigner_md/wigner_forces.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)wigner_types.o \
	$(DOBJ)rpmd_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)matrix_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)unbiased_products.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wigner_types.o: source/FORTRAN/engines/wigner_md/wigner_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)file_handler.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wigner_md.o: source/FORTRAN/engines/wigner_md/wigner_md.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)random.o \
	$(DOBJ)wigner_types.o \
	$(DOBJ)wigner_integrators.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)wigner_forces.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)wigner_integrators.o: source/FORTRAN/engines/wigner_md/wigner_integrators.f90 \
	$(DOBJ)wigner_types.o \
	$(DOBJ)random.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)wigner_forces.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)classical_mc.o: source/FORTRAN/engines/classical_mc/classical_mc.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)classical_md.o \
	$(DOBJ)classical_mc_types.o \
	$(DOBJ)classical_mc_moves.o \
	$(DOBJ)classical_md_integrators.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)thermostats.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)classical_mc_types.o: source/FORTRAN/engines/classical_mc/classical_mc_types.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)classical_md_types.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)classical_mc_moves.o: source/FORTRAN/engines/classical_mc/classical_mc_moves.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)classical_mc_types.o \
	$(DOBJ)classical_md_types.o \
	$(DOBJ)basic_types.o \
	$(DOBJ)random.o \
	$(DOBJ)classical_md.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)linkedlist.o: source/FORTRAN/data_structures/linkedlist.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)nested_dictionaries.o: source/FORTRAN/data_structures/nested_dictionaries.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)linkedlist.o \
	$(DOBJ)string_operations.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)vectors.o: source/FORTRAN/data_structures/vectors.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)trajectory_files.o: source/FORTRAN/file_handler/trajectory_files.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)vectors.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)input_handler.o: source/FORTRAN/file_handler/input_handler.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)nested_dictionaries.o \
	$(DOBJ)atomic_units.o \
	$(DOBJ)file_handler.o \
	$(DOBJ)trajectory_files.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)atomic_units.o: source/FORTRAN/file_handler/atomic_units.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)string_operations.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

$(DOBJ)file_handler.o: source/FORTRAN/file_handler/file_handler.f90 \
	$(DOBJ)kinds.o \
	$(DOBJ)trajectory_files.o \
	$(DOBJ)nested_dictionaries.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC) -Isource/sockets  $< -o $@

#phony auxiliary rules
.PHONY : $(MKDIRS)
$(MKDIRS):
	@mkdir -p $@
.PHONY : cleanobj
cleanobj:
	@echo deleting objects
	@rm -fr $(DOBJ)
.PHONY : cleanmod
cleanmod:
	@echo deleting mods
	@rm -fr $(DMOD)
.PHONY : cleanexe
cleanexe:
	@echo deleting exes
	@rm -f $(addprefix $(DEXE),$(EXES))
.PHONY : clean
clean: cleanobj cleanmod
.PHONY : cleanall
cleanall: clean cleanexe
