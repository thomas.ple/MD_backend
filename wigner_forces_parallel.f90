MODULE wigner_forces
    USE kinds
    USE wigner_types
    USE rpmd_types
    USE nested_dictionaries
    USE string_operations
    USE atomic_units, only : convert_to_unit
    USE basic_types, only : Pot,dPot
    USE matrix_operations, only : sym_mat_diag,fill_permutations
    USE random, only: RandGaussN
    USE unbiased_products, only: unbiased_product
    USE socket_potential, only :parallel_socket_pot_info
    USE timer_module
    IMPLICIT NONE

    INTEGER, SAVE :: NUM_AUX_SIM=3

    TYPE :: WIG_POLYMER_TYPE
        INTEGER :: n_beads

        REAL(wp), ALLOCATABLE :: X(:,:,:)
        REAL(wp), ALLOCATABLE :: P(:,:,:)
        REAL(wp), ALLOCATABLE :: F_beads(:,:,:)
        REAL(wp), ALLOCATABLE :: Pot_beads(:)

        REAL(wp), ALLOCATABLE :: Delta(:,:)
        REAL(wp), ALLOCATABLE :: Delta_packed(:)

        REAL(wp), ALLOCATABLE :: k2(:,:)
		REAL(wp), ALLOCATABLE :: k2inv(:,:)
		REAL(wp), ALLOCATABLE :: k4(:,:,:,:)
		REAL(wp), ALLOCATABLE :: F(:)
		REAL(wp), ALLOCATABLE :: G(:,:,:)
        REAL(wp), ALLOCATABLE :: C(:,:,:)

        !ONLY FOR 1D SYSTEMS
        REAL(wp) :: k6 
        REAL(wp) :: G4
    END TYPE

    TYPE, EXTENDS(RPMD_PARAM) :: WIG_FORCE_PARAM
        REAL(wp), ALLOCATABLE :: mu(:,:,:)
        REAL(wp), ALLOCATABLE :: OUsig(:,:,:,:)
        REAL(wp), ALLOCATABLE :: expOmk(:,:,:,:)

        REAL(wp), ALLOCATABLE :: sigp2_inv(:)

        ! REAL(wp), ALLOCATABLE :: muX(:,:,:),muP(:,:,:)

        CHARACTER(:), ALLOCATABLE :: move_generator
        INTEGER :: n_samples

        REAL(wp), ALLOCATABLE :: sqrt_mass(:)
        LOGICAL :: spring_force
        LOGICAL :: compute_EW

        REAL(wp) :: delta_mass_factor, sqrt_delta_mass_factor

        INTEGER :: n_threads
        LOGICAL :: verbose

        LOGICAL :: k2_smooth
        REAL(wp) :: k2_smooth_center
        REAL(wp) :: k2_smooth_width
        REAL(wp) , ALLOCATABLE :: k2_smooth_mat(:,:)

        REAL(wp), ALLOCATABLE :: memory_F(:)
		REAL(wp), ALLOCATABLE :: memory_C(:,:,:)
		REAL(wp), ALLOCATABLE :: memory_k2(:,:)
        REAL(wp), ALLOCATABLE :: memory_sqrtk2inv(:,:)
        REAL(wp) :: memory_sum_weight
        REAL(wp) :: memory_tau
        LOGICAL :: memory_smoothing


    END TYPE

    TYPE WORK_VARIABLES
        REAL(wp), ALLOCATABLE :: V(:)
        REAL(wp), ALLOCATABLE :: F(:)
        REAL(wp), ALLOCATABLE :: Delta2(:,:)
        REAL(wp), ALLOCATABLE :: Delta4(:,:,:,:)
        REAL(wp), ALLOCATABLE :: EigX(:),EigV(:),tmp(:)
        REAL(wp), ALLOCATABLE :: R(:), R1(:), R2(:)
        REAL(wp),ALLOCATABLE :: k2s(:,:,:),kik1ki(:,:,:), k2_work(:,:) &
                                    ,k1Gk1(:,:),k1F(:),k1kik1kik1(:,:)
        REAL(wp), ALLOCATABLE :: k2EigMat(:,:)
		REAL(wp), ALLOCATABLE :: k2EigVals(:)
    END TYPE WORK_VARIABLES

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), ALLOCATABLE, SAVE :: polymers
    TYPE(WIG_FORCE_PARAM), SAVE :: f_param
    TYPE(WORK_VARIABLES), DIMENSION(:), ALLOCATABLE, SAVE :: WORKS

    ABSTRACT INTERFACE
		subroutine move_proposal_type(system,polymer,move_accepted,WORK)
            IMPORT :: WIG_SYS,WIG_POLYMER_TYPE,WORK_VARIABLES
			IMPLICIT NONE
            TYPE(WIG_SYS), INTENT(IN) :: system
			TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
			LOGICAL, INTENT(OUT) :: move_accepted
            CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		end subroutine move_proposal_type

        subroutine update_beads_forces_type(q,polymer)
            IMPORT :: wp,WIG_POLYMER_TYPE
            implicit none
            TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
            REAL(wp), INTENT(in) :: q(:,:)
        end subroutine update_beads_forces_type
	END INTERFACE

    PROCEDURE(move_proposal_type), POINTER :: move_proposal => NULL()
    PROCEDURE(update_beads_forces_type), POINTER :: update_beads_forces => NULL()

    PRIVATE
    PUBLIC :: compute_wigner_forces, initialize_auxiliary_simulation &
                ,compute_EW

CONTAINS

!--------------------------------------------------------------
! FORCE CALCULATIONS

    subroutine compute_wigner_forces(system,parameters)
        !$ USE OMP_LIB
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        INTEGER :: i,j,INFO,i_thread
        TYPE(timer_type) :: timer_full, timer_elem

        if(ANY(ISNAN(system%X))) STOP "ERROR: system diverged!"

        ! UPDATE X DEPENDENT PARAMETERS
        ! DO j=1,system%n_dim ; DO i=1,system%n_atoms
        !     f_param%muX(:,i,j)=system%X(i,j)*f_param%mu(:,2,i)
        !     f_param%muP(:,i,j)=system%X(i,j)*f_param%mu(:,1,i)
        ! ENDDO ; ENDDO

        i_thread=1
        if(f_param%verbose) then
            write(*,*)
            write(*,*) "Starting WiLD forces computation..."
            !$ write(*,*) "(parallel computation)"
            call timer_full%start()
        endif

        !$OMP PARALLEL DO LASTPRIVATE(i_thread)
        DO i=1,NUM_AUX_SIM
            !$ i_thread = OMP_GET_THREAD_NUM()+1
            call sample_forces(system,parameters,polymers(i),WORKS(i_thread))
        ENDDO
        !$OMP END PARALLEL DO


        ! COMPUTE FORCE ELEMENTS
        if(f_param%verbose) then
            write(*,*) "    Starting force elements calculation..."
            call timer_elem%start()
        endif
        CALL compute_forces_elements(system,parameters)         
        if(f_param%verbose) then
            write(*,*) "    force elements computed in",timer_elem%elapsed_time(),"s."
        endif 

        if(f_param%memory_smoothing) call memory_smoothing(system,parameters)       
        
        system%X_prev=system%X   
        if(f_param%verbose) then
            write(*,*) "WiLD forces computed in",timer_full%elapsed_time(),"s."
        endif    

    end subroutine compute_wigner_forces

    subroutine compute_forces_elements(system,parameters)
        !$ USE OMP_LIB
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: tmp,k2k2F,F(NUM_AUX_SIM),k2(NUM_AUX_SIM),G2(NUM_AUX_SIM),k4(NUM_AUX_SIM)
        INTEGER :: i,j,k,l,p1,p2,p3,c,is,INFO,i_thread
        !,k1kik1F(:,:)
        REAL(wp), ALLOCATABLE, SAVE :: C_loc(:,:,:)
        REAL(wp) :: Ns,tmpscal


        ! COMPUTE MEAN k2
        system%k2=0
        DO i=1,NUM_AUX_SIM
            system%k2=system%k2+polymers(i)%k2/REAL(NUM_AUX_SIM, wp)
        ENDDO

        if(f_param%compute_EW) then
            ! COMPUTE MEAN k4 AND k6
            system%k4=0
            system%k6=0
            DO i=1,NUM_AUX_SIM
                system%k6=system%k6+polymers(i)%k6
                system%k4=system%k4+polymers(i)%k4
            ENDDO
            system%k6=system%k6/REAL(NUM_AUX_SIM, wp)
            system%k4=system%k4/REAL(NUM_AUX_SIM, wp)
        endif
        

        !----------------------------------------------------------------
        ! DO NOT COMPUTE THE OTHERS IF NOT ENOUGH TRAJECTORIES
        IF(NUM_AUX_SIM<3) RETURN ! SHOULD NOT BE ALLOWED FOR DYNAMICS
        !----------------------------------------------------------------

        system%k2_var=0
        ! COMPUTE VARIANCE OF ELEMENTS OF k2
        DO i=1,NUM_AUX_SIM
            system%k2_var=system%k2_var+(system%k2-polymers(i)%k2)**2/REAL(NUM_AUX_SIM,wp)
        ENDDO
        system%k2_var=system%k2_var/REAL(NUM_AUX_SIM-1,wp)

        f_param%k2_smooth_mat=1._wp
        if(f_param%k2_smooth) call smooth_k2(system,parameters)        

        !----------------------------------------------------------------
        ! COMPUTE DYNAMICAL CORRECTIONS IF 1 DOF

        if(system%n_dof==1) then
            
            DO i=1,NUM_AUX_SIM
                F(i)=polymers(i)%F(1)
                k2(i)=polymers(i)%k2(1,1)
                if(f_param%compute_EW) &
                    k4(i)=polymers(i)%k4(1,1,1,1)
                G2(i)=polymers(i)%G(1,1,1)
            ENDDO

            system%beta_dU=-SUM(F(:))/real(NUM_AUX_SIM,wp)
 
            system%dk2= SUM(G2(:))/real(NUM_AUX_SIM,wp) &
                        -unbiased_product(k2(:),F(:))

            k2k2F=unbiased_product(k2(:),k2(:),F(:))

            if(f_param%compute_EW) then
                system%C4=system%k4(1,1,1,1) - 3._wp*unbiased_product(k2(:),k2(:))
                system%C4_beta_dU=-unbiased_product(k4(:),F(:)) &
                                    +3._wp*k2k2F
                system%dC4= SUM(polymers(:)%G4)/real(NUM_AUX_SIM,wp) &
                            +system%C4_beta_dU + 3._wp*k2k2F &                        
                            -6._wp*unbiased_product(k2(:),G2(:))
            endif
        endif
       
        !----------------------------------------------------------------
        ! COMPUTE FORCES ELEMENTS (F AND C) WITH BIAS CORRECTION

        ! If(.NOT. allocated(k2s)) &
        !     allocate(k2s(f_param%n_dof,f_param%n_dof,NUM_AUX_SIM-1) &
        !             ,kik1ki(f_param%n_dof,f_param%n_dof,NUM_AUX_SIM-1) &
        !             ,k1kik1kik1(f_param%n_dof,f_param%n_dof) &
        !             ,k1Gk1(f_param%n_dof,f_param%n_dof) &
        !             ,k1F(f_param%n_dof) &
        !            ! ,k1kik1F(f_param%n_dof,NUM_AUX_SIM-1) &
        !             ,k2_work(f_param%n_dof,f_param%n_dof) &
        !             ,V(f_param%n_dof) &
        !         )
        ! If(.NOT. allocated(C_loc)) &
        !      allocate(C_loc(f_param%n_dof,f_param%n_dof,f_param%n_dof))

        ! DIVIDE BY MASS
        DO is=1,NUM_AUX_SIM
            c=0
            DO j=1,system%n_dim ; DO i=1,system%n_atoms
                c=c+1
                polymers(is)%F(c)=polymers(is)%F(c)/system%mass(i)
                polymers(is)%G(:,:,c)=polymers(is)%G(:,:,c)/system%mass(i)
            ENDDO ; ENDDO            
        ENDDO
        Ns=real(NUM_AUX_SIM-1,wp)
        ! C_loc=0
        system%C=0
        system%F=0

        ! COMPUTE FORCES ON UNBIASED PRODUCTS
        i_thread=1
        !! !$OMP PARALLEL DO LASTPRIVATE(i,j,k,l,p1,p2,p3,c,is,INFO,i_thread,tmpscal) !! REDUCTION(+:C_loc)    
        DO p1=1,NUM_AUX_SIM
            !$ i_thread = OMP_GET_THREAD_NUM()+1
            c=0
            WORKS(i_thread)%k2_work=0
            !! STORE THE k2 THAT ARE NOT p1 AND COMPUTE THE MEAN VALUE
            DO p2=1,p1-1
                c=c+1
                WORKS(i_thread)%k2s(:,:,c)=polymers(p2)%k2
                WORKS(i_thread)%k2_work=WORKS(i_thread)%k2_work+polymers(p2)%k2/Ns
            ENDDO
            DO p2=p1+1,NUM_AUX_SIM
                c=c+1
                WORKS(i_thread)%k2s(:,:,c)=polymers(p2)%k2
                WORKS(i_thread)%k2_work=WORKS(i_thread)%k2_work+polymers(p2)%k2/Ns
            ENDDO
            !! COMPUTE k2inv ON THE NUM_AUX_SIM-1 SAMPLES     
            !WORKS(1)%Delta2=sym_mat_inverse(k2_work,INFO)
            !IF(INFO/=0) STOP "Error: could not compute k2inv !"

            call sym_mat_diag(WORKS(i_thread)%k2_work,WORKS(i_thread)%k2EigVals,WORKS(i_thread)%k2EigMat,INFO)
            IF(INFO/=0) STOP "Error: could not diagonalize k2 !"
            if(ANY(WORKS(i_thread)%k2EigVals<0)) write(0,*) "Error: k2 is not positive definite!"
            WORKS(i_thread)%Delta2=0
            DO i=1,system%n_dof
               WORKS(i_thread)%Delta2(i,i)=1._wp/WORKS(i_thread)%k2EigVals(i)
            ENDDO
            WORKS(i_thread)%Delta2 =matmul(WORKS(i_thread)%k2EigMat,matmul(WORKS(1)%Delta2,transpose(WORKS(i_thread)%k2EigMat)))
            !WORKS(1)%Delta2 = WORKS(1)%Delta2 *f_param%k2_smooth_mat

            !!COMPUTE <k2i k2inv k2i>
            WORKS(i_thread)%kik1ki=0
            WORKS(i_thread)%k1kik1kik1=0
            do is=1,NUM_AUX_SIM-1
                WORKS(i_thread)%kik1ki(:,:,is)=matmul(WORKS(i_thread)%Delta2, &
                                matmul(WORKS(i_thread)%k2s(:,:,is),WORKS(i_thread)%Delta2) )
                WORKS(i_thread)%k1kik1kik1=WORKS(i_thread)%k1kik1kik1 &
                         + matmul(WORKS(i_thread)%kik1ki(:,:,is),WORKS(i_thread)%k2s(:,:,is))/Ns
            enddo
            WORKS(i_thread)%k1kik1kik1=matmul(WORKS(i_thread)%k1kik1kik1,WORKS(i_thread)%Delta2)/(Ns-1._wp)
            !!COMPUTE CORRECTED k2inv
            polymers(p1)%k2inv=(Ns/(Ns-1._wp))* WORKS(i_thread)%Delta2 - WORKS(i_thread)%k1kik1kik1
           ! polymers(p1)%k2inv = polymers(p1)%k2inv*f_param%k2_smooth_mat

            !COMPUTE V (OLD METHOD)
            WORKS(i_thread)%V=0
            ! DO k=1,system%n_dof
            !     k1Gk1=matmul(WORKS(1)%Delta2,matmul(polymers(p1)%G(:,:,k),WORKS(1)%Delta2))/(Ns-1._wp)
            !     polymers(p1)%G(:,:,k)=k1Gk1*(Ns+2._wp) &  
            !         -matmul(WORKS(1)%Delta2,matmul(polymers(p1)%G(:,:,k),k1kik1kik1)) & 
            !         -matmul(k1kik1kik1,matmul(polymers(p1)%G(:,:,k),WORKS(1)%Delta2))
            !         ! -matmul(k1Gk1,matmul(kik1ki,WORKS(1)%Delta2)) &
            !         ! -matmul(WORKS(1)%Delta2,matmul(kik1ki,k1Gk1))
            !     k2_work=0
            !     DO is=1,NUM_AUX_SIM-1
            !         k2_work=k2_work  &
            !             +matmul(k2s(:,:,is),matmul(k1Gk1,k2s(:,:,is)))/Ns
            !     ENDDO
            !     polymers(p1)%G(:,:,k)=polymers(p1)%G(:,:,k) &
            !         -matmul(WORKS(1)%Delta2,matmul(k2_work,WORKS(1)%Delta2))
            ! ENDDO
            
            ! DO k=1,system%n_dof ; DO i=1,system%n_dof
            !     V(i)=V(i)-polymers(p1)%G(i,k,k)
            ! ENDDO ; ENDDO 
            
            !COMPUTE V (NEW METHOD)
            WORKS(i_thread)%k1F=0
            tmpscal=-(Ns+2_wp)/(Ns-1._wp)
            DO k=1,system%n_dof ; DO l=1,system%n_dof
               WORKS(i_thread)%k1F(:)=WORKS(i_thread)%k1F(:)+polymers(p1)%G(k,:,l)*( &
                                WORKS(i_thread)%Delta2(k,l)*tmpscal &
                                + WORKS(i_thread)%k1kik1kik1(k,l) )
            ENDDO ; ENDDO
            WORKS(i_thread)%V(:)=matmul(WORKS(i_thread)%Delta2,WORKS(i_thread)%k1F)
            WORKS(i_thread)%k1F=0
            DO k=1,system%n_dof ; DO l=1,system%n_dof
               WORKS(i_thread)%k1F(:)=WORKS(i_thread)%k1F(:)+polymers(p1)%G(k,:,l)*WORKS(i_thread)%Delta2(k,l)
            ENDDO ; ENDDO
            WORKS(i_thread)%V(:)=WORKS(i_thread)%V(:)+matmul(WORKS(i_thread)%k1kik1kik1,WORKS(i_thread)%k1F)

            tmpscal=1._wp/(Ns*(Ns-1._wp))
            WORKS(i_thread)%k1F=0
            DO is=1,NUM_AUX_SIM-1
                DO k=1,system%n_dof ; DO l=1,system%n_dof
                    WORKS(i_thread)%k1F(:)=WORKS(i_thread)%k1F(:)+polymers(p1)%G(k,:,l)*WORKS(i_thread)%kik1ki(k,l,is)
                ENDDO ; ENDDO
                WORKS(i_thread)%V(:)=WORKS(i_thread)%V(:) + tmpscal*matmul(WORKS(i_thread)%kik1ki(:,:,is),WORKS(i_thread)%k1F)
            ENDDO

            !!COMPUTE F
            polymers(p1)%F = WORKS(i_thread)%V + 2._wp*matmul(polymers(p1)%k2inv,polymers(p1)%F)  


            !!COMPUTE C
            WORKS(i_thread)%k1Gk1 = WORKS(i_thread)%Delta2*(Ns+1_wp)/(Ns-1._wp) - WORKS(i_thread)%k1kik1kik1
            WORKS(i_thread)%k1Gk1 = WORKS(i_thread)%k1Gk1 * f_param%k2_smooth_mat      
           ! k1F=matmul(WORKS(1)%Delta2,polymers(p1)%F)
           ! WORKS(1)%F=(k1F*(Ns+1_wp)-matmul(WORKS(1)%Delta2,matmul(kik1ki,k1F)))/(Ns-1._wp)
            WORKS(i_thread)%F=matmul(WORKS(i_thread)%k1Gk1,polymers(p1)%F)
            ! DO is=1,NUM_AUX_SIM-1
            !     k1Gk1=matmul(WORKS(1)%Delta2,matmul(k2s(:,:,is),WORKS(1)%Delta2)) * f_param%k2_smooth_mat
            !     k1kik1F(:,is)=matmul(k1Gk1,polymers(p1)%F)/(Ns*(Ns-1._wp))
            ! ENDDO

            polymers(p1)%C=0
            DO j=1,system%n_dof                
                DO k=1, system%n_dof
                    polymers(p1)%C(:,:,j)=polymers(p1)%C(:,:,j) &
                        +(-1*(polymers(p1)%k2inv(j,k)*f_param%k2_smooth_mat(j,k))*polymers(p1)%G(:,:,k))
                ENDDO
                polymers(p1)%C(:,:,j)=polymers(p1)%C(:,:,j)+WORKS(i_thread)%k2_work*WORKS(i_thread)%F(j)
                ! DO is=1, NUM_AUX_SIM-1
                !     system%C(:,:,j)=system%C(:,:,j)-k2s(:,:,is)*k1kik1F(j,is)
                ! ENDDO
            ENDDO  
            DO is=1,NUM_AUX_SIM-1
                WORKS(i_thread)%k1Gk1 = WORKS(i_thread)%kik1ki(:,:,is) * f_param%k2_smooth_mat
                WORKS(i_thread)%F(:)=matmul(WORKS(i_thread)%k1Gk1,polymers(p1)%F)/(Ns*(Ns-1._wp))
                DO j=1,system%n_dof
                    polymers(p1)%C(:,:,j)=polymers(p1)%C(:,:,j)+(-1*WORKS(i_thread)%k2s(:,:,is)*WORKS(i_thread)%F(j))
                ENDDO                
            ENDDO          
        ENDDO
        !!!$OMP END PARALLEL DO

        DO p1=1,NUM_AUX_SIM
            system%F = system%F + polymers(p1)%F/REAL(NUM_AUX_SIM,wp)
            system%C=system%C + polymers(p1)%C
        ENDDO

        system%C=0.5_wp*system%C/REAL(NUM_AUX_SIM,wp)!C_loc/REAL(NUM_AUX_SIM,wp)
        DO j=1,system%n_dof
            system%C(:,:,j) = system%C(:,:,j) * f_param%k2_smooth_mat(:,:)
        ENDDO

        system%F_var=0
        DO p1=1,NUM_AUX_SIM
            system%F_var = system%F_var + (polymers(p1)%F - system%F)**2/REAL(NUM_AUX_SIM,wp)
        ENDDO
        system%F_var = system%F_var / REAL(NUM_AUX_SIM-1,wp)


        !---------------------------------------------------
        ! COMPUTE MEAN k2 INVERSE AND SQRT(INVERSE)

        call sym_mat_diag(system%k2,system%k2EigVals,system%k2EigMat,INFO)            
        IF(INFO/=0) STOP "Error: could not diagonalize k2 !"
        if(ANY(system%k2EigVals<0)) write(0,*) "Error: k2 is not positive definite!"
        system%k2inv=0
        system%sqrtk2inv=0
        DO i=1,system%n_dof
            system%k2inv(i,i)=1._wp/system%k2EigVals(i)
            system%sqrtk2inv(i,i)=1._wp/sqrt(system%k2EigVals(i))
        ENDDO
        system%k2inv = matmul(system%k2EigMat,matmul(system%k2inv,transpose(system%k2EigMat)))        
        system%sqrtk2inv = matmul(system%k2EigMat,matmul(system%sqrtk2inv,transpose(system%k2EigMat)))


    !    write(*,*)
     !   write(*,*) system%k2
      !  write(*,*) system%k2inv
       ! write(*,*) system%k2EigMat
        !write(*,*) matmul(system%k2,system%k2inv)

        ! COMPUTE THE CHOLESKY DECOMPOSITION OF k2 INVERSE
        ! system%sqrtk2inv=system%k2inv
        ! call DPOTRF('L',system%n_dof,system%sqrtk2inv,system%n_dof,INFO)            
        ! if(INFO/=0) then
        !     write(0,*) "Error code ",INFO,"while computing Cholesky decomposition of k2!"
        !     STOP "Execution stopped"
        ! endif
        ! !! WARNING : upper triangular elements of sigma are not necessary 0 (not referenced by LAPACK)
        ! DO j=2,system%n_dof ; DO i=1,j-1
        !     system%sqrtk2inv(i,j)=0._wp
        ! ENDDO ; ENDDO

        ! CORRECT SYSTEMATIC BIAS
        WORKS(1)%k1kik1kik1=0
        DO i=1,NUM_AUX_SIM
            WORKS(1)%k1kik1kik1=WORKS(1)%k1kik1kik1+ &
                matmul(polymers(i)%k2,matmul(system%k2inv,polymers(i)%k2))/REAL(NUM_AUX_SIM,wp)
        ENDDO
        system%sqrtk2inv = ((8._wp*NUM_AUX_SIM - 5._wp)/(8._wp*(NUM_AUX_SIM-1._wp)))*system%sqrtk2inv &
            -(3._wp/(8._wp*(NUM_AUX_SIM-1._wp)))*matmul(system%k2inv,matmul(WORKS(1)%k1kik1kik1,system%sqrtk2inv))

        system%k2inv=(system%k2inv*REAL(NUM_AUX_SIM,wp) &
            -matmul(system%k2inv,matmul(WORKS(1)%k1kik1kik1,system%k2inv)))/REAL(NUM_AUX_SIM-1,wp)
        
        system%k2inv= system%k2inv*f_param%k2_smooth_mat
        system%sqrtk2inv= system%sqrtk2inv*f_param%k2_smooth_mat

    end subroutine compute_forces_elements

    subroutine memory_smoothing(system,parameters)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        REAL(wp) :: mem_exp
        
        mem_exp = exp(-parameters%dt/f_param%memory_tau)

        f_param%memory_sum_weight = 1._wp + f_param%memory_sum_weight*mem_exp

        f_param%memory_k2 = f_param%memory_k2 * mem_exp + system%k2
        f_param%memory_sqrtk2inv = f_param%memory_sqrtk2inv * mem_exp + system%sqrtk2inv
        f_param%memory_C  = f_param%memory_C  * mem_exp + system%C
        f_param%memory_F  = f_param%memory_F  * mem_exp + system%F

        system%sqrtk2inv = f_param%memory_sqrtk2inv / f_param%memory_sum_weight
        system%k2 = f_param%memory_k2 / f_param%memory_sum_weight
        system%C  = f_param%memory_C  / f_param%memory_sum_weight
        !system%F  = f_param%memory_F  / f_param%memory_sum_weight
    
    end subroutine memory_smoothing

    subroutine smooth_k2(system,parameters)
        CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_PARAM), INTENT(inout) :: parameters
        INTEGER :: i,j,k,c,n,l
        REAL(wp) :: smooth,x,mean_smooth,stdev_unbias
        ! reduce the importance of off-diagonal k2 terms where the variance is 
        ! the same or larger than the estimated value
        ! (cutoff using a fermi function)

        
        stdev_unbias=REAL(NUM_AUX_SIM-1,wp)/(NUM_AUX_SIM-1.5_wp)
        mean_smooth=0 ; c=0 ; n=0
        DO j=2,system%n_dof ; DO i=1,j-1
           ! x=sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j)) - f_param%k2_smooth_center
            !smooth=1._wp/(1._wp+exp(x/f_param%k2_smooth_width))
            smooth=1
            if(sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j)) > f_param%k2_smooth_center) &
                smooth = 0
            f_param%k2_smooth_mat(i,j)=smooth
            f_param%k2_smooth_mat(j,i)=f_param%k2_smooth_mat(i,j)
            c=c+1
            mean_smooth=mean_smooth+(smooth-mean_smooth)/c ; 
            if(smooth<0.5) then
              !  write(*,*) "sigma/k2("//int_to_str(i)//","//int_to_str(j)//")=" &
               !                 ,sqrt(system%k2_var(i,j)*stdev_unbias)/abs(system%k2(i,j))
                n=n+1
            endif
            ! if(smooth<0.5 .and. f_param%verbose) &
            !     write(*,*) "Warning: hard smoothing on k2("//int_to_str(i)//","//int_to_str(j)//") &
            !                             & (",REAL(smooth),")"
            ! system%k2(i,j)=system%k2(i,j)*smooth
            ! system%k2(j,i)=system%k2(i,j)
            ! system%k2_var(i,j)=system%k2_var(i,j)*smooth*smooth
            ! system%k2_var(j,i)=system%k2_var(i,j)
            ! DO k=1,NUM_AUX_SIM
            !     polymers(k)%k2(i,j)=polymers(k)%k2(i,j)*smooth
            !     polymers(k)%k2(j,i)=polymers(k)%k2(i,j)
            !     DO l=1,system%n_dof
            !         polymers(k)%G(i,j,l)=polymers(k)%G(i,j,l)*smooth
            !         polymers(k)%G(j,i,l)=polymers(k)%G(i,j,l)
            !     ENDDO
            ! ENDDO
        ENDDO ; ENDDO

        if(mean_smooth<0.5) then
            write(*,*) "    Warning: hard smoothing on k2 => mean smoothing=",REAL(mean_smooth)
            write(*,*) "             number of smooth<0.5=",n
        endif
    end subroutine smooth_k2

!--------------------------------------------------------------
! SAMPLING LOOP
    subroutine sample_forces(system,parameters,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: m,n_dof
        INTEGER :: i,j,k,l
        LOGICAL :: move_accepted

        call reinitialize_polymer_config(system,polymer,WORK)

        !THERMALIZATION
        DO m=1,f_param%n_steps_therm
            CALL move_proposal(system,polymer,move_accepted,WORK)
        ENDDO

        !PRODUCTION
        DO m=1,f_param%n_steps

            !GENERATE A NEW CONFIG
            CALL move_proposal(system,polymer,move_accepted,WORK)
            !COMPUTE OBSERVABLES
            call compute_observables(system,polymer,WORK)

            !UPDATE MEAN VALUES
            CALL update_mean_values(polymer,m,WORK)

        ENDDO

        n_dof=system%n_dof

        !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
        DO j=2,n_dof ; DO i=1,j-1
           polymer%k2(i,j)=polymer%k2(j,i)
        ENDDO ; ENDDO
        
        !symmetrize G
		do i=1,n_dof-1 ; do j=i+1,n_dof
			do k=1,n_dof
				polymer%G(i,j,k)=polymer%G(j,i,k)
			enddo
		enddo ; enddo
		
		!symmetrize k4
        if(f_param%compute_EW) then
            do l=1,n_dof ; do k=l,n_dof ; do j=k,n_dof ; do i=j,n_dof		
                call fill_permutations(polymer%k4,i,j,k,l)
            enddo ; enddo ; enddo ; enddo
        endif

    end subroutine sample_forces

!-------------------------------------------------
! UTILITY SUBROUTINES

    subroutine compute_observables(system,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j,k,l
        REAL(wp), DIMENSION(:), POINTER :: Delta_packed
        
        IF(f_param%spring_force) THEN
            DO i=1,system%n_dim
                WORK%F(system%n_atoms*(i-1)+1:system%n_atoms*i) = &
                    -f_param%sigp2_inv(:) * ( &
                        2._wp*system%X(:,i)-polymer%X(1,:,i) &
                        -polymer%X(f_param%n_beads-1,:,i) &
                    )+f_param%dBeta * ( &
                        polymer%F_beads(0,:,i)+ polymer%F_beads(f_param%n_beads,:,i)&
                    )
            ENDDO
        ELSE
            WORK%F=f_param%beta*RESHAPE( &
                    SUM(polymer%F_beads/real(f_param%n_beads,wp),dim=1) &
                ,(/system%n_dof/) )
        ENDIF

        polymer%Delta_packed=RESHAPE(polymer%Delta,(/system%n_dof/))
     !   Delta_packed => polymer%Delta

        !COMPUTE ONLY THE LOWER TRIANGULAR PART
		do j=1,system%n_dof ; do i=j,system%n_dof
			WORK%Delta2(i,j)=polymer%Delta_packed(i)*polymer%Delta_packed(j)
		enddo ; enddo

        if(f_param%compute_EW) then
            do l=1,system%n_dof ; do k=l,system%n_dof ;	do j=k,system%n_dof ; do i=j,system%n_dof
                WORK%Delta4(i,j,k,l)=polymer%Delta_packed(i)*polymer%Delta_packed(j) &
                                *polymer%Delta_packed(k)*polymer%Delta_packed(l)
            enddo ; enddo ; enddo ; enddo
        endif

    end subroutine compute_observables

    subroutine update_mean_values(polymer,count,WORK)
        IMPLICIT NONE
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        INTEGER, INTENT(in) :: count
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: k
        
        if(f_param%compute_EW) then
            polymer%k6=polymer%k6 + (polymer%Delta(1,1)**6-polymer%k6)/count
            polymer%k4=polymer%k4 + (WORK%Delta4-polymer%k4)/count	
            polymer%G4=polymer%G4 + (WORK%F(1)*WORK%Delta4(1,1,1,1)-polymer%G4)/count	
        endif	

        polymer%k2=polymer%k2 + (WORK%Delta2-polymer%k2)/count
		polymer%F=polymer%F + (WORK%F-polymer%F)/count        
		DO k=1,f_param%n_dof
			polymer%G(:,:,k)=polymer%G(:,:,k) &
                +(WORK%Delta2*WORK%F(k)-polymer%G(:,:,k))/count
		ENDDO        

    end subroutine update_mean_values
!--------------------------------------------------------------
! MOVE GENERATORS
    
    !-----------------------------------
    ! PIOUD steps
    subroutine PIOUD_step(system,polymer,move_accepted,WORK)
		IMPLICIT NONE
        TYPE(WIG_SYS), INTENT(IN) :: system
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
        LOGICAL, INTENT(out) :: move_accepted
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		integer :: i,j,nu

		nu=f_param%n_beads

		do j=1,system%n_dim ; do i=1,system%n_atoms
			!TRANSFORM IN NORMAL MODES	
            WORK%EigX(:)=polymer%X(1:nu,i,j)*f_param%sqrt_mass(i)
            WORK%EigX(nu)=f_param%sqrt_delta_mass_factor*WORK%EigX(nu)
            ! WORK%EigX=matmul(f_param%EigMatTr,WORK%EigX)
            call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigX,1,0._8,WORK%tmp,1)
            WORK%EigX=WORK%tmp
            WORK%EigV=polymer%P(1:nu,i,j)/f_param%sqrt_mass(i)
            WORK%EigV(nu)=WORK%EigV(nu)/f_param%sqrt_delta_mass_factor
            ! EigV=matmul(EigMatTr,EigV)
            call DGEMV('N',nu,nu,1._8,f_param%EigMatTr,nu,WORK%EigV,1,0._8,WORK%tmp,1)
            WORK%EigV=WORK%tmp	

			!GENERATE RANDOM VECTORS
			CALL RandGaussN(WORK%R1)
			CALL RandGaussN(WORK%R2)

			!PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
			polymer%P(:,i,j)=WORK%EigV(:)*f_param%expOmk(:,1,1,i) &
                +WORK%EigX(:)*f_param%expOmk(:,1,2,i) &
                +f_param%OUsig(:,1,1,i)*WORK%R1(:) &
                +system%X(i,j)*f_param%mu(:,1,i)

			polymer%X(1:nu,i,j)=WORK%EigV(:)*f_param%expOmk(:,2,1,i) &
                +WORK%EigX(:)*f_param%expOmk(:,2,2,i) &
                +f_param%OUsig(:,2,1,i)*WORK%R1(:)+f_param%OUsig(:,2,2,i)*WORK%R2(:) &
                +system%X(i,j)*f_param%mu(:,2,i)
			
			!TRANSFORM BACK IN COORDINATES
            ! polymer%X(1:nu,i,j)=matmul(f_param%EigMat,polymer%X(1:nu,i,j))/f_param%sqrt_mass(i)
            call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%X(1:nu,i,j),1,0._8,WORK%tmp,1)
            polymer%X(1:nu,i,j)=WORK%tmp/f_param%sqrt_mass(i)
            polymer%X(nu,i,j)=polymer%X(nu,i,j)/f_param%sqrt_delta_mass_factor
            ! polymer%P(:,i)=matmul(f_param%EigMat,polymer%P(:,i))*f_param%sqrt_mass(i)
            call DGEMV('N',nu,nu,1._8,f_param%EigMat,nu,polymer%P(1:nu,i,j),1,0._8,WORK%tmp,1)
            polymer%P(1:nu,i,j)=WORK%tmp*f_param%sqrt_mass(i)
            polymer%P(nu,i,j)=f_param%sqrt_delta_mass_factor*polymer%P(nu,i,j)

		enddo ; enddo

		polymer%Delta(:,:)=polymer%X(nu,:,:)

        CALL update_beads_forces(system%X,polymer)
		!APPLY FORCES		
		CALL apply_forces(polymer,f_param%dt)

        move_accepted=.TRUE.

	end subroutine PIOUD_step

	subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: tau
		INTEGER :: i

		do i=1,f_param%n_beads-1
			polymer%P(i,:,:)=polymer%P(i,:,:)+tau*polymer%F_beads(i,:,:)
		enddo
		polymer%P(f_param%n_beads,:,:)=polymer%P(f_param%n_beads,:,:) + 0.5_wp*tau* &
			(polymer%F_beads(0,:,:) - polymer%F_beads(f_param%n_beads,:,:))
	end subroutine apply_forces
!--------------------------------------------------------------
! VARIOUS INITIALIZATIONS

    subroutine reinitialize_polymer_config(system,polymer,WORK)
        IMPLICIT NONE
		CLASS(WIG_SYS), INTENT(inout) :: system
        CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
        CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
        INTEGER :: i,j

        polymer%k2=0
        polymer%k2inv=0
        if(f_param%compute_EW) then
            polymer%k4=0
            polymer%k6=0
            polymer%G4=0
        endif
        polymer%F=0
        polymer%G=0

        DO i=1,f_param%n_beads-1
            polymer%X(i,:,:)=polymer%X(i,:,:) + system%X(:,:)-system%X_prev(:,:)
        ENDDO       
        CALL update_beads_forces(system%X,polymer)

        CALL apply_forces(polymer,0.5_wp*f_param%dt)

    end subroutine reinitialize_polymer_config

    subroutine initialize_auxiliary_simulation(system,parameters,param_library)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: param_library
        TYPE(DICT_STRUCT), pointer :: wig_lib
        INTEGER :: n_dof,n_beads,n_atoms,n_dim
        INTEGER :: i,j,k,n_therm_tmp,n_steps_init
        CHARACTER(:), ALLOCATABLE :: pot_name

        n_dof=system%n_dof
        f_param%n_dof=n_dof
        n_atoms=system%n_atoms
        f_param%n_atoms=n_atoms
        n_dim=system%n_dim
        f_param%n_dim=n_dim

        f_param%verbose = param_library%get(WIGNER_CAT//"/verbose",default=.FALSE.)
        !! @input_file WIGNER_AUX/verbose (wigner, default=.FALSE.)

        NUM_AUX_SIM=param_library%get(WIGNER_CAT//"/num_aux_sim",default=3)
        !! @input_file WIGNER_AUX/num_aux_sim (wigner, default=3)

        ! AT LEAST 3 AUXILIARY SIMULATIONS FOR DYNAMICS
        IF((.not. parameters%only_auxiliary_simulation) .and. NUM_AUX_SIM<3) &
            STOP "Error : 'num_aux_sim' must be >=3 for dynamics"
        allocate(polymers(NUM_AUX_SIM))

        wig_lib => param_library%get_child(WIGNER_CAT)
        !GET NUMBER OF BEADS
        n_beads=wig_lib%get("n_beads")
        !! @input_file WIGNER_AUX/n_beads (wigner)
		if(n_beads<=0) STOP "Error: 'n_beads' of '"//WIGNER_CAT//"' is not properly defined!"
			polymers(:)%n_beads=n_beads
			f_param%n_beads=n_beads
        
        !GET NUMBER OF AUXILIARY SIMULATION STEPS
        f_param%n_steps=wig_lib%get("n_steps")
        !! @input_file WIGNER_AUX/n_steps (wigner)
        if(f_param%n_steps<=0) &
            STOP "Error: 'n_steps' of '"//WIGNER_CAT//"' is not properly defined!"

        f_param%n_steps_therm=wig_lib%get("n_steps_therm")
        !! @input_file WIGNER_AUX/n_steps_therm (wigner)
        if(f_param%n_steps_therm<0) &
            STOP "Error: 'n_steps_therm' of '"//WIGNER_CAT//"' is not properly defined!"

        f_param%spring_force=wig_lib%get("spring_force",default=.FALSE.)
        !! @input_file WIGNER_AUX/spring_force (wigner, default=.FALSE.)

        f_param%k2_smooth=wig_lib%get("k2_smoothing",default=.FALSE.)
        !! @input_file WIGNER_AUX/k2_smoothing (wigner, default=.FALSE.)        
        f_param%k2_smooth_width=wig_lib%get("k2_smooth_width",default=0.03)
        !! @input_file WIGNER_AUX/k2_smooth_width (wigner, default=0.03)
        f_param%k2_smooth_center=wig_lib%get("k2_smooth_center",default=0.9)
        !! @input_file WIGNER_AUX/k2_smooth_center (wigner, default=0.9)
        if(f_param%k2_smooth) then
            write(*,*)
            write(*,*) "k2 smoothing of off-diagonal terms activated."
            write(*,*) "  using fermi function with threshold ",f_param%k2_smooth_center
            write(*,*) "  and transition width",f_param%k2_smooth_width
        endif
        ALLOCATE(f_param%k2_smooth_mat(system%n_dof,system%n_dof))

        f_param%memory_smoothing=wig_lib%get("memory_smoothing",default=.FALSE.)
        !! @input_file WIGNER_AUX/memory_smoothing (wigner, default=.FALSE.)
        if(f_param%memory_smoothing) then
            f_param%memory_tau=wig_lib%get("memory_tau",default=5._wp*parameters%dt)
            !! @input_file WIGNER_AUX/memory_tau (wigner, default=5*dt)
            allocate(f_param%memory_C(n_dof,n_dof,n_dof) &
                    ,f_param%memory_F(n_dof) &
                    ,f_param%memory_k2(n_dof,n_dof) &
                    ,f_param%memory_sqrtk2inv(n_dof,n_dof) &
                )
            f_param%memory_C=0
            f_param%memory_F=0
            f_param%memory_k2=0
            f_param%memory_sqrtk2inv=0
            f_param%memory_sum_weight=0
            write(*,*)
            write(*,*) "memory smoothing activated with tau=",f_param%memory_tau*fs,"fs"
        endif

        !INITIALIZE FORCES PARAMETERS
        allocate( &
            f_param%EigMat(n_beads,n_beads), &
            f_param%EigMatTr(n_beads,n_beads), &
            f_param%OmK(n_beads), &
            f_param%sigp2_inv(n_atoms) &
        )
        f_param%beta=parameters%beta
        f_param%dBeta=parameters%beta/REAL(f_param%n_beads,wp)
        f_param%sigp2_inv(:)=system%mass(:)/f_param%dBeta

        ! GET NUMBER OF THREADS        
        f_param%n_threads=1
        !$ f_param%n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

        !INITIALIZE WORK VARIABLES
        allocate(WORKS(f_param%n_threads))
        DO i=1,f_param%n_threads
            allocate( &
                WORKS(i)%F(n_dof), &
                WORKS(i)%Delta2(n_dof,n_dof), &
                WORKS(i)%EigX(n_beads), &
                WORKS(i)%EigV(n_beads), &
                WORKS(i)%R(n_atoms), &
                WORKS(i)%R1(n_beads), &
                WORKS(i)%R2(n_beads), &
                WORKS(i)%tmp(n_beads) &
            )
            allocate( &
                WORKS(i)%k2s(n_dof,n_dof,NUM_AUX_SIM-1) &
                ,WORKS(i)%kik1ki(n_dof,n_dof,NUM_AUX_SIM-1) &
                ,WORKS(i)%k1kik1kik1(n_dof,n_dof) &
                ,WORKS(i)%k1Gk1(n_dof,n_dof) &
                ,WORKS(i)%k1F(n_dof) &
                ! ,k1kik1F(f_param%n_dof,NUM_AUX_SIM-1) &
                ,WORKS(i)%k2_work(n_dof,n_dof) &
                ,WORKS(i)%V(n_dof) &
            )
            allocate(WORKS(i)%k2EigMat(n_dof,n_dof),WORKS(i)%k2EigVals(n_dof))
        ENDDO


        !INITIALIZE polymers
        DO i=1,NUM_AUX_SIM
            allocate( &
                polymers(i)%X(0:n_beads,n_atoms,n_dim), &
                polymers(i)%P(n_beads,n_atoms,n_dim), &
                polymers(i)%F_beads(0:n_beads,n_atoms,n_dim), &
                polymers(i)%Pot_beads(0:n_beads), &
                polymers(i)%Delta(n_atoms,n_dim), &
                polymers(i)%Delta_packed(n_dof), &
                polymers(i)%k2(n_dof,n_dof), &
                polymers(i)%k2inv(n_dof,n_dof), &
                polymers(i)%F(n_dof), &
                polymers(i)%G(n_dof,n_dof,n_dof), &
                polymers(i)%C(n_dof,n_dof,n_dof) &
            )
            polymers(i)%Delta=0._wp
            DO j=1,n_beads
                polymers(i)%X(j,:,:)=system%X(:,:)
                DO k=1,n_dim
                    call randGaussN(polymers(i)%P(j,:,k))
                    polymers(i)%P(j,:,k)=polymers(i)%P(j,:,k)*SQRT(f_param%sigp2_inv(:))
                ENDDO
            ENDDO
        ENDDO

        f_param%compute_EW = parameters%compute_EW

        if(f_param%compute_EW) then
            DO i=1,f_param%n_threads
                allocate(WORKS(i)%Delta4(n_dof,n_dof,n_dof,n_dof))
            ENDDO
            DO i=1,NUM_AUX_SIM
                allocate(polymers(i)%k4(n_dof,n_dof,n_dof,n_dof))
            ENDDO            
        endif

        f_param%parallel_forces=.FALSE.
		pot_name=param_library%get("POTENTIAL/pot_name")
		if(to_upper_case(trim(pot_name))=="SOCKET") f_param%parallel_forces=.TRUE.

        call initialize_bead_forces()

        call initialize_move_generator(system,parameters,wig_lib)



        if(.not. parameters%only_auxiliary_simulation) then
            n_steps_init=wig_lib%get("n_steps_init",default=5000)
            !! @input_file WIGNER_AUX/n_steps_init (wigner, default=5000)
            n_therm_tmp = f_param%n_steps_therm
            f_param%n_steps_therm = n_steps_init
            write(*,*) "Initializing WiLD forces with",n_steps_init,"steps"
            call compute_wigner_forces(system,parameters)
            write(*,*) "WiLD forces initialization done."
            f_param%n_steps_therm = n_therm_tmp
        endif

    end subroutine initialize_auxiliary_simulation

    subroutine initialize_move_generator(system,parameters,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
		CLASS(WIG_PARAM), INTENT(inout) :: parameters
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,j,k

        f_param%move_generator=wig_lib%get("move_generator" &
                    ,default="PIOUD")
        !! @input_file WIGNER_AUX/move_generator (wigner, default="PIOUD")
        f_param%move_generator=to_upper_case(f_param%move_generator)
        parameters%move_generator=f_param%move_generator

        SELECT CASE(f_param%move_generator)
        CASE ("PIOUD")

            move_proposal => PIOUD_step
            !INITIALIZE PIOUD
            ! SHORTHANDS FOR MASS
            allocate(f_param%sqrt_mass(system%n_atoms))
            call get_polymer_masses(system,wig_lib)
            f_param%dt=wig_lib%get("dt",default=parameters%dt)
            !! @input_file WIGNER_AUX/dt (wigner, default= PARAMETERS/dt, if move_generator="PIOUD")
            f_param%delta_mass_factor=wig_lib%get("delta_mass_factor",default=0.25_wp)
            !! @input_file WIGNER_AUX/delta_mass_factor (wigner, default= 0.25, if move_generator="PIOUD")
            f_param%sqrt_delta_mass_factor=SQRT(f_param%delta_mass_factor)
            CALL initialize_PIOUD(system,wig_lib)
            DO i=1,NUM_AUX_SIM
                polymers(i)%X(f_param%n_beads,:,:)=polymers(i)%Delta
            ENDDO

        CASE DEFAULT
            write(0,*) "Error: unknown move generator '"//f_param%move_generator//"' !"
            STOP "Execution stopped."
        END SELECT

    end subroutine initialize_move_generator

    subroutine get_polymer_masses(system,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        TYPE(DICT_STRUCT), POINTER :: m_lib
        CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
        LOGICAL, ALLOCATABLE :: is_sub(:)
        REAL(wp) :: m_tmp
        INTEGER :: i
        CHARACTER(2) :: symbol

        
        if(.not. wig_lib%has_key("modified_masses")) then
            f_param%sqrt_mass=SQRT(system%mass)
            return
        endif

        m_lib => wig_lib%get_child("modified_masses")
        write(*,*)
        write(*,*) "WiLD auxiliary masses:"
        call dict_list_of_keys(m_lib,keys,is_sub)
        DO i=1,size(keys)
			if(is_sub(i)) CYCLE
            m_tmp=m_lib%get(keys(i))
            symbol=trim(keys(i))
            symbol(1:1)=to_upper_case(symbol(1:1))
            write(*,*) symbol," : ", real(m_tmp/Mprot),"amu"
        ENDDO
        write(*,*)
        do i=1, system%n_atoms
            f_param%sqrt_mass(i)=m_lib%get(trim(system%element_symbols(i)) &
                                            ,default=system%mass(i) )
            !if(f_param%sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", f_param%sqrt_mass(i)/Mprot," amu"                    
        enddo
        f_param%sqrt_mass=sqrt(f_param%sqrt_mass)

    end subroutine get_polymer_masses

    subroutine initialize_PIOUD(system,wig_lib)
        IMPLICIT NONE
        CLASS(WIG_SYS), INTENT(inout) :: system
        TYPE(DICT_STRUCT), intent(in) :: wig_lib
        INTEGER :: i,nu,INFO,j
		REAL(wp), ALLOCATABLE :: TMP(:),thetaInv(:,:,:,:),OUsig2(:,:,:,:),mOmk(:,:)
		REAL(wp) :: Id(2,2), Om0
        

        nu=f_param%n_beads

        Id=0
		Id(1,1)=1
		Id(2,2)=1

		Om0=1._wp/f_param%dBeta
        allocate(TMP(3*nu-1),thetaInv(nu,2,2,system%n_atoms),OUsig2(nu,2,2,system%n_atoms),mOmk(nu,system%n_atoms))
		allocate( &
            f_param%mu(nu,2,system%n_atoms), &
            f_param%OUsig(nu,2,2,system%n_atoms), &
            f_param%expOmk(nu,2,2,system%n_atoms), &
            ! f_param%muX(nu,system%n_atoms,system%n_dim), &
            ! f_param%muP(nu,system%n_atoms,system%n_dim) &
        )

        !INITIALIZE DYNAMICAL MATRIX
		f_param%EigMat=0
		do i=1,nu-1
			f_param%EigMat(i,i)=2
			f_param%EigMat(i+1,i)=-1
			f_param%EigMat(i,i+1)=-1
		enddo
		f_param%EigMat(nu,nu)=0.5_wp/f_param%delta_mass_factor
		f_param%EigMat(nu-1,nu)=0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(nu,nu-1)=0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(1,nu)=-0.5_wp/f_param%sqrt_delta_mass_factor
		f_param%EigMat(nu,1)=-0.5_wp/f_param%sqrt_delta_mass_factor

        !SOLVE EIGENPROBLEM
		CALL DSYEV('V','L',nu,f_param%EigMat,nu,f_param%Omk,TMP,3*nu-1,INFO)
		if(INFO/=0) then
			write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
			stop 
		endif

        f_param%Omk=Om0*SQRT(f_param%Omk)
        DO i=1,system%n_atoms
           mOmk(:,i)=f_param%Omk*sqrt(system%mass(i))/f_param%sqrt_mass(i) 
        ENDDO
		write(*,*)
		write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
		do i=1,nu
			write(*,*) i, convert_to_unit(f_param%Omk(i),"THz"),"THz"
		enddo
		write(*,*)

        f_param%EigMatTr=transpose(f_param%EigMat)

        DO i=1,system%n_atoms
            f_param%expOmk(:,1,1,i)=exp(-mOmk(:,i)*f_param%dt) &
                                    *(1-mOmk(:,i)*f_param%dt)
            f_param%expOmk(:,1,2,i)=exp(-mOmk(:,i)*f_param%dt) &
                                    *(-f_param%dt*mOmk(:,i)**2)
            f_param%expOmk(:,2,1,i)=exp(-mOmk(:,i)*f_param%dt)*f_param%dt
            f_param%expOmk(:,2,2,i)=exp(-mOmk(:,i)*f_param%dt) &
                                    *(1+mOmk(:,i)*f_param%dt)

            thetaInv(:,1,1,i)=0._wp
            thetaInv(:,1,2,i)=-1._wp
            thetaInv(:,2,1,i)=1._wp/mOmk(:,i)**2
            thetaInv(:,2,2,i)=2._wp/mOmk(:,i)
        ENDDO

        !COMPUTE MEAN VECTOR
        f_param%mu=0
        do j=1,system%n_atoms            
            f_param%mu(1,1,j)=Om0**2
            f_param%mu(nu-1,1,j)=Om0**2
            f_param%mu(:,1,j)=matmul(f_param%EigMatTr,f_param%mu(:,1,j))
            f_param%mu(:,2,j)=0._wp
        
            do i=1,nu
                thetaInv(i,:,:,j)=matmul((Id(:,:)-f_param%expOmk(i,:,:,j)),thetaInv(i,:,:,j))
                f_param%mu(i,:,j)=(system%mass(j)/f_param%sqrt_mass(j))*matmul(thetaInv(i,:,:,j),f_param%mu(i,:,j))
            enddo
        enddo

        !COMPUTE COVARIANCE MATRIX
        DO i=1,system%n_atoms
            OUsig2(:,1,1,i)=(1._wp- (1._wp-2._wp*f_param%dt*mOmk(:,i) &
                                +2._wp*(f_param%dt*mOmk(:,i))**2 &
                            )*exp(-2._wp*mOmk(:,i)*f_param%dt) &
                            )/f_param%dBeta
            OUsig2(:,2,2,i)=(1._wp- (1._wp+2._wp*f_param%dt*mOmk(:,i) &
                                +2._wp*(f_param%dt*mOmk(:,i))**2 &
                            )*exp(-2._wp*mOmk(:,i)*f_param%dt) &
                            )/(f_param%dBeta*mOmk(:,i)**2)
            OUsig2(:,2,1,i)=2._wp*mOmk(:,i)*(f_param%dt**2) &
                            *exp(-2._wp*mOmk(:,i)*f_param%dt)/f_param%dBeta
            OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

            !COMPUTE CHOLESKY DECOMPOSITION
            f_param%OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
            f_param%OUsig(:,1,2,i)=0
            f_param%OUsig(:,2,1,i)=OUsig2(:,2,1,i)/f_param%OUsig(:,1,1,i)
            f_param%OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-f_param%OUsig(:,2,1,i)**2)
        ENDDO

		!CHECK CHOLESKY DECOMPOSITION
		! write(*,*) "sum of squared errors on cholesky decomposition:"
		! do i=1,nu
		! 	write(*,*) i, sum( (matmul(f_param%OUsig(i,:,:),transpose(f_param%OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
		! enddo

    end subroutine initialize_PIOUD

    subroutine initialize_bead_forces()
        IMPLICIT NONE

        IF(f_param%parallel_forces) THEN
            update_beads_forces => update_beads_forces_socket_parallel
        ELSE
            update_beads_forces => update_beads_forces_serial
        ENDIF

    end subroutine initialize_bead_forces

!----------------------------------------------------------------------

    subroutine update_beads_forces_serial(q,polymer)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q(:,:)
		INTEGER :: i
        
		do i=1,f_param%n_beads-1
			polymer%F_beads(i,:,:)=-dPot(polymer%X(i,:,:))
		enddo
		polymer%F_beads(0,:,:)=-0.5_wp*dPot(q+0.5_wp*polymer%Delta) 
        polymer%F_beads(f_param%n_beads,:,:)=-0.5_wp*dPot(q-0.5_wp*polymer%Delta)
	end subroutine update_beads_forces_serial

    subroutine update_beads_forces_socket_parallel(q,polymer)
        !$ USE OMP_LIB
        implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(wp), INTENT(in) :: q(:,:)
		INTEGER :: i,is

        is=1        
        !$ is=OMP_GET_THREAD_NUM()+1
        
        polymer%X(0,:,:) = q + 0.5_wp*polymer%Delta
        polymer%X(f_param%n_beads,:,:) = q - 0.5_wp*polymer%Delta
		CALL parallel_socket_pot_info(polymer%X,polymer%F_beads,polymer%Pot_beads,i_socket=is)

        if(f_param%move_generator=="PIOUD") polymer%X(f_param%n_beads,:,:) = polymer%Delta


	end subroutine update_beads_forces_socket_parallel

!----------------------------------------------------------------------
! COMPUTE EDGEWORTH CORRECTION

	subroutine compute_EW(system)
		implicit none
		TYPE(WIG_SYS), INTENT(inout) :: system
		INTEGER :: j,k,l,m
		REAL(wp), ALLOCATABLE, SAVE :: k2(:,:,:)
		REAL(wp) :: C6        

        if(.not. f_param%compute_EW) then
            system%EW=1
            system%EW6=1
            return
        endif

		system%EW=0

		if(.not. allocated(k2)) allocate(k2(system%n_dof,system%n_dof,2))
		k2(:,:,1)= 0.5_wp*( polymers(1)%k2(:,:) + polymers(2)%k2(:,:) )
		k2(:,:,2)= polymers(3)%k2(:,:)

		do m=1,system%n_dof ; do l=1,system%n_dof ; do k=1,system%n_dof ; do j=1,system%n_dof
	
			system%EW = system%EW + system%P_packed(j)*system%P_packed(k) &
                        *system%P_packed(l)*system%P_packed(m)*( &
                            system%k4(j,k,l,m)  - k2(j,l,1)*k2(k,m,2) &
                                                - k2(k,l,1)*k2(j,m,2) &
                                                - k2(j,k,1)*k2(l,m,2) &
                        )
														
		enddo ; enddo ; enddo ; enddo

		system%EW = 1 + system%EW / 24._wp

		if(system%n_dof==1) then
			C6 = system%k6 - 15._wp*polymers(1)%k2(1,1)*0.5_wp*(polymers(2)%k4(1,1,1,1)+polymers(3)%k4(1,1,1,1)) &
				+30._wp*polymers(1)%k2(1,1)*polymers(2)%k2(1,1)*polymers(3)%k2(1,1)
			system%EW6 = system%EW - (system%P_packed(1)**6) * C6 / 720._wp
		endif

	end subroutine compute_EW

END MODULE wigner_forces